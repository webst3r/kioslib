unit uClienteLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormClienteLista = class(TUniForm)
    UniDBGrid1: TUniDBGrid;
    dsCabe: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    btOpciones: TUniBitBtn;
    btFicha: TUniBitBtn;
    btReserva: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    btSalir: TUniBitBtn;
    procedure UniDBGrid1DblClick(Sender: TObject);
    procedure btFichaClick(Sender: TObject);
    procedure btReservaClick(Sender: TObject);


  private
    procedure ProAbrirCabe(zEmpresa, zCliente: Integer);
    { Private declarations }

  public
    { Public declarations }

  vTipo : String;

  vInicioForm : Boolean;

  vAreaNegocio, vActuacion, vCliente, vTipoCliente:Integer;
  vFechaAlta, vParar:String;

  vNomCabe,
  vNomKeyCabe1,vNomKeyCabe2,
  vWhereCabe,vOrderCabe : String;
  vFiltroEspecial : String;

  vFechaDesde : TDate;

  vOEmpresa, vOCliente : Integer;

  vSwNou              : Boolean;
  vCodiNou, vCodiVell : Integer;
  vSwBusca,  vSwTFacturacion, vSwTPago   : Boolean;

  vImpoTotLin, vValorMovi : Double;
  vRegisH,   vRegisC    : Integer;

  end;

function FormClienteLista: TFormClienteLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uCliente, uDMCliente, uClienteFicha;

function FormClienteLista: TFormClienteLista;
begin
  Result := TFormClienteLista(DMppal.GetFormInstance(TFormClienteLista));

end;


procedure TFormClienteLista.UniDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ProAbrirCabe  (vOEmpresa, DMCliente.sqlCabeID_CLIENTE.AsInteger);
  FormClienteFicha.Parent          := FormCliente.tabFichaCliente;
  FormCliente.pcDetalle.ActivePage := FormCliente.tabFichaCliente;
end;

procedure TFormClienteLista.btFichaClick(Sender: TObject);
begin
  FormCliente.RutAbrirFichaClie;
end;

procedure TFormClienteLista.btReservaClick(Sender: TObject);
begin
  FormCliente.RutAbrirReservaClie;
end;

procedure TFormClienteLista.ProAbrirCabe  (zEmpresa,zCliente:Integer);
var
  I : Integer;
begin
  vWhereCabe := ' Where ID_CLIENTE = ' + IntToStr(zCliente);
  vFiltroEspecial := '';
  vOrderCabe := ' Order by ID_CLIENTE ';
 // if FormPrincipal.cbClienteNombre.Checked then
 // vOrderCabe      := ' Order by NOMBRE ';

  DMCliente.sqlCabeS.Close;
  DMCliente.sqlCabeS.sql.text := Copy(DMCliente.sqlCabe.sql.text,1,
                    pos('WHERE', Uppercase(DMCliente.sqlCabe.sql.text))-1)
                 + vWhereCabe + vFiltroEspecial + vOrderCabe;
  DMCliente.sqlCabeS.Open;
  DMCliente.sqlCabeS.First;

  FormClienteFicha.pCodi.Text := IntToStr(DMCliente.sqlCabeSID_CLIENTE.AsInteger);

end;


end.
