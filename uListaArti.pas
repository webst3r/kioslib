unit uListaArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormListaArti = class(TUniForm)
    tmSession: TUniTimer;
    dsListaArticulo: TDataSource;
    GridListaArti: TUniDBGrid;
    btFicha: TUniBitBtn;
    btConsulta: TUniBitBtn;
    edBuscar: TUniEdit;
    UniLabel9: TUniLabel;
    btNuevoArticulo: TUniBitBtn;
    btHistorico: TUniBitBtn;
    procedure GridListaArtiDblClick(Sender: TObject);
    procedure btFichaClick(Sender: TObject);
    procedure btConsultaClick(Sender: TObject);
    procedure edBuscarChange(Sender: TObject);
    procedure btNuevoArticuloClick(Sender: TObject);
    procedure btHistoricoClick(Sender: TObject);
    procedure GridListaArtiColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure edBuscarKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);


  private


    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    procedure RutFiltrarArti(vBusqueda : String; vEditorial : Integer);
    procedure RutAbrirHistoListaArti;
  end;

function FormListaArti: TFormListaArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuArti, uFichaArti,
  uDMHistoArti, uMensajes;

function FormListaArti: TFormListaArti;
begin
  Result := TFormListaArti(DMppal.GetFormInstance(TFormListaArti));

end;

procedure TFormListaArti.btConsultaClick(Sender: TObject);
begin
  DMppal.swConsArti := self.Name;
  FormMenuArti.RutAbrirConsArti;
end;

procedure TFormListaArti.btFichaClick(Sender: TObject);
begin
  DMMenuArti.swCreando := false;
  FormMenuArti.RutAbrirFichaArti('listaArti');
  FormManteArti.RutHabitlitarBTs(False);
  FormManteArti.RutShow;
  DMppal.swArtiFicha := self.Name;
end;


procedure TFormListaArti.btHistoricoClick(Sender: TObject);
begin
  RutAbrirHistoListaArti;
end;

procedure TFormListaArti.RutAbrirHistoListaArti;
begin
  FormMenu.RutAbrirHistoArtiTablas(DMMenuArti.sqlListaArticulo.FieldByName('ID_ARTICULO').AsInteger,
                                    DMMenuArti.sqlListaArticulo.FieldByName('BARRAS').AsString,
                                    DMMenuArti.sqlListaArticulo.FieldByName('ADENDUM').AsString,
                                    DMMenuArti.sqlListaArticulo.FieldByName('DESCRIPCION').AsString,
                                    'articulos');
  DMppal.swHisArti := self.Name;
end;

procedure TFormListaArti.RutFiltrarArti(vBusqueda : String; vEditorial : Integer);
var
  vWhere,vAnd, vOr : String;
begin
//abrir tablas
  //sqlArticulos.Close;
  //sqlArticulos.Open;
  vWhere := 'WHERE A.EDITORIAL > 0';
  if vEditorial <> 0 then   vWhere := 'WHERE A.EDITORIAL = ' + IntToStr(vEditorial);

  vOr   := '';
  if vBusqueda <> '' then
  begin
    //vWhere := 'WHERE FA.ID_ARTICULO LIKE '      + QuotedStr('%'+vBusqueda+'%');
    vOr   := vOr + ' AND (UPPER(A.BARRAS)    LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ' OR UPPER(A.ADENDUM)     LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ' OR UPPER(A.DESCRIPCION) LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ')'
  end;


  with DMMenuArti.sqlListaArticulo do
  begin
    Close;
    DMMenuArti.sqlListaArticulo.SQL.Text := Copy(SQL.Text, 1,
                pos('WHERE A.EDITORIAL', Uppercase(SQL.Text))-1)
              + vWhere
              + vOr
              + ' ORDER BY 1';


    Open;
  end;
end;

procedure TFormListaArti.btNuevoArticuloClick(Sender: TObject);
begin
  FormMenuArti.RutAbrirFichaArti('nuevoArti');
  FormManteArti.btNuevoArticuloClick(nil);
end;

procedure TFormListaArti.edBuscarChange(Sender: TObject);
begin
  if length(edBuscar.Text) >= 5 then RutFiltrarArti(edBuscar.Text,0);
end;

procedure TFormListaArti.edBuscarKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (length(edBuscar.Text) < 5) then RutFiltrarArti(edbuscar.Text,0); //para identificar cuando es un enter
end;

procedure TFormListaArti.GridListaArtiColumnSort(Column: TUniDBGridColumn;
  Direction: Boolean);
begin
  DMppal.SortColumnSQL(DMMenuArti.sqlListaArticulo, Column.FieldName, Direction);
end;

procedure TFormListaArti.GridListaArtiDblClick(Sender: TObject);
begin
  btFichaClick(nil);
end;

end.
