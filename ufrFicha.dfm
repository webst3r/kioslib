object FormfrFicha: TFormfrFicha
  Left = 0
  Top = 0
  ClientHeight = 600
  ClientWidth = 890
  Caption = ''
  BorderStyle = bsNone
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  OnBeforeShow = UniFormBeforeShow
  PixelsPerInch = 96
  TextHeight = 13
  object UniURLFrame1: TUniURLFrame
    Left = 0
    Top = 0
    Width = 890
    Height = 566
    Hint = ''
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ParentColor = False
    Color = clBtnFace
    ExplicitHeight = 664
  end
  object btCerrarFR: TUniButton
    Left = 0
    Top = 566
    Width = 890
    Height = 34
    Hint = ''
    Caption = 'Cerrar'
    Anchors = [akLeft, akRight, akBottom]
    Align = alBottom
    TabOrder = 1
    OnClick = btCerrarFRClick
    ExplicitTop = 664
  end
end
