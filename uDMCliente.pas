unit uDMCliente;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMCliente = class(TDataModule)
    sqlCabe: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    sqlCabeID_CLIENTE: TIntegerField;
    sqlCabeNOMBRE: TStringField;
    sqlCabeNIF: TStringField;
    sqlCabeID_DIRECCION: TIntegerField;
    sqlCabeNOMBRE2: TStringField;
    sqlCabeDIRECCION: TStringField;
    sqlCabeDIRECCION2: TStringField;
    sqlCabePOBLACION: TStringField;
    sqlCabeCPOSTAL: TStringField;
    sqlCabeCPROVINCIA: TStringField;
    sqlCabePROVINCIA: TStringField;
    sqlCabeCONTACTO1NOMBRE: TStringField;
    sqlCabeCONTACTO1CARGO: TStringField;
    sqlCabeCONTACTO2NOMBRE: TStringField;
    sqlCabeCONTACTO2CARGO: TStringField;
    sqlCabeTELEFONO1: TStringField;
    sqlCabeTELEFONO2: TStringField;
    sqlCabeMOVIL: TStringField;
    sqlCabeFAX: TStringField;
    sqlCabeEMAIL: TStringField;
    sqlCabeWEB: TStringField;
    sqlCabeMENSAJEAVISO: TStringField;
    sqlCabeOBSERVACIONES: TMemoField;
    sqlCabeBANCO: TStringField;
    sqlCabeAGENCIA: TStringField;
    sqlCabeDC: TStringField;
    sqlCabeCUENTABANCO: TStringField;
    sqlCabeTDTO: TIntegerField;
    sqlCabeTIVACLI: TSmallintField;
    sqlCabeTPAGO: TIntegerField;
    sqlCabeDIAFIJO1: TSmallintField;
    sqlCabeDIAFIJO2: TSmallintField;
    sqlCabeDIAFIJO3: TSmallintField;
    sqlCabeSWBLOQUEO: TSmallintField;
    sqlCabeSWTFACTURACION: TSmallintField;
    sqlCabeSWAGRUARTIFRA: TSmallintField;
    sqlCabeREFEPROVEEDOR: TStringField;
    sqlCabeTRANSPORTE: TSmallintField;
    sqlCabeSWALTABAJA: TSmallintField;
    sqlCabeFECHABAJA: TDateField;
    sqlCabeFECHAALTA: TDateField;
    sqlCabeHORAALTA: TTimeField;
    sqlCabeFECHAULTI: TDateField;
    sqlCabeHORAULTI: TTimeField;
    sqlCabeUSUULTI: TStringField;
    sqlCabeNOTAULTI: TStringField;
    sqlPoblacionS: TFDQuery;
    sqlTFacturacio: TFDQuery;
    FDQuery3: TFDQuery;
    sqlCLIARTI1: TFDQuery;
    sqlTPago: TFDQuery;
    sqlReservaS: TFDQuery;
    sqlCLIEARTIS: TFDQuery;
    sqlCabeS: TFDQuery;
    sqlPoblacionSCODIGO: TIntegerField;
    sqlPoblacionSPOSTAL: TIntegerField;
    sqlPoblacionSPOBLACION: TStringField;
    sqlPoblacionSCPROV: TIntegerField;
    sqlPoblacionSPROVINCIA: TStringField;
    sqlPoblacionSCPOSTAL: TStringField;
    sqlPoblacionSCPAIS: TStringField;
    sqlTFacturacioTTIPO: TStringField;
    sqlTFacturacioNORDEN: TIntegerField;
    sqlTFacturacioDESCRIPCION: TStringField;
    sqlCabeSID_CLIENTE: TIntegerField;
    sqlCabeSNOMBRE: TStringField;
    sqlCabeSDIRECCION: TStringField;
    sqlCabeSPOBLACION: TStringField;
    sqlCabeSPROVINCIA: TStringField;
    sqlCabeSCPOSTAL: TStringField;
    sqlCabeSCPROVINCIA: TStringField;
    sqlCabeSNIF: TStringField;
    sqlCabeSTELEFONO1: TStringField;
    sqlCabeSTELEFONO2: TStringField;
    sqlCabeSMOVIL: TStringField;
    sqlCabeSFAX: TStringField;
    sqlUpdate: TFDQuery;
    sqlTPagoID_TPAGO: TIntegerField;
    sqlTPagoDESCRIPCION: TStringField;
    sqlTPagoTEFECTO: TSmallintField;
    sqlTPagoEFECTOS: TSmallintField;
    sqlTPagoFRECUENCIA: TSmallintField;
    sqlTPagoDIASPRIMERVTO: TSmallintField;
    sqlCLIEARTISID_CLIARTI: TIntegerField;
    sqlCLIEARTISID_CLIENTE: TIntegerField;
    sqlCLIEARTISNOMBRE: TStringField;
    sqlCLIEARTISID_ARTICULO: TIntegerField;
    sqlCLIEARTISDESCRIPCION: TStringField;
    sqlCLIEARTISBARRAS: TStringField;
    sqlCLIEARTISCANTIDAD: TSingleField;
    sqlCLIEARTISD1: TSmallintField;
    sqlCLIEARTISD2: TSmallintField;
    sqlCLIEARTISD3: TSmallintField;
    sqlCLIEARTISD4: TSmallintField;
    sqlCLIEARTISD5: TSmallintField;
    sqlCLIEARTISD6: TSmallintField;
    sqlCLIEARTISD7: TSmallintField;
    sqlCLIEARTISDF: TSmallintField;
    sqlCLIEARTISRESERVAEJEMPLAR: TIntegerField;
    sqlCLIEARTISDESDEFECHA: TDateField;
    sqlCLIEARTISHASTAFECHA: TDateField;
    sqlCLIEARTISDESDEFECHAEX: TDateField;
    sqlCLIEARTISHASTAFECHAEX: TDateField;
    sqlCLIEARTISSWALTABAJA: TSmallintField;
    sqlCLIEARTISFECHABAJA: TDateField;
    sqlCLIEARTISFECHAALTA: TDateField;
    sqlCLIEARTISHORAALTA: TTimeField;
    sqlCLIEARTISFECHAULTI: TDateField;
    sqlCLIEARTISHORAULTI: TTimeField;
    sqlCLIEARTISUSUULTI: TStringField;
    sqlCLIEARTISNOTAULTI: TStringField;
    sqlCLIEARTISOBSERVACIONES: TStringField;
    sqlReservaSBARRAS18: TStringField;
    sqlReservaSID_CLIENTE: TIntegerField;
    sqlReservaSID_ARTICULO: TIntegerField;
    sqlReservaSADENDUM: TStringField;
    sqlReservaSID_HISARTIRE: TIntegerField;
    sqlReservaSSWSITUACION: TSmallintField;
    sqlReservaSFECHARESERVA: TDateField;
    sqlReservaSFECHAENTREGA: TDateField;
    sqlReservaSCANTIRESERVA: TSingleField;
    sqlReservaSCANTIENTREGA: TSingleField;
    sqlReservaSCANTIRECI: TSingleField;
    sqlReservaSCANTIAPARTADA: TSingleField;
    sqlReservaSBARRAS: TStringField;
    sqlReservaSADENDUM_1: TStringField;
    sqlReservaSDESCRIPCION: TStringField;
    sqlReservaSNOMBRE: TStringField;
    sqlReservaSNIF: TStringField;
    sqlReservaSTELEFONO1: TStringField;
    sqlCLIARTI1ID_CLIARTI: TIntegerField;
    sqlCLIARTI1ID_CLIENTE: TIntegerField;
    sqlCLIARTI1NOMBRE: TStringField;
    sqlCLIARTI1ID_ARTICULO: TIntegerField;
    sqlCLIARTI1DESCRIPCION: TStringField;
    sqlCLIARTI1BARRAS: TStringField;
    sqlCLIARTI1CANTIDAD: TSingleField;
    sqlCLIARTI1D1: TSmallintField;
    sqlCLIARTI1D2: TSmallintField;
    sqlCLIARTI1D3: TSmallintField;
    sqlCLIARTI1D4: TSmallintField;
    sqlCLIARTI1D5: TSmallintField;
    sqlCLIARTI1D6: TSmallintField;
    sqlCLIARTI1D7: TSmallintField;
    sqlCLIARTI1DF: TSmallintField;
    sqlCLIARTI1RESERVAEJEMPLAR: TIntegerField;
    sqlCLIARTI1DESDEFECHA: TDateField;
    sqlCLIARTI1HASTAFECHA: TDateField;
    sqlCLIARTI1DESDEFECHAEX: TDateField;
    sqlCLIARTI1HASTAFECHAEX: TDateField;
    sqlCLIARTI1SWALTABAJA: TSmallintField;
    sqlCLIARTI1FECHABAJA: TDateField;
    sqlCLIARTI1FECHAALTA: TDateField;
    sqlCLIARTI1HORAALTA: TTimeField;
    sqlCLIARTI1FECHAULTI: TDateField;
    sqlCLIARTI1HORAULTI: TTimeField;
    sqlCLIARTI1USUULTI: TStringField;
    sqlCLIARTI1NOTAULTI: TStringField;
    sqlCLIARTI1OBSERVACIONES: TStringField;
    sqlCLIARTI1SWAVISAR: TSmallintField;
    sqlCLIARTI1FECHAGENERADAULTI: TDateField;

  private

    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    procedure RutAbrirTablas;

    end;

function DMCliente: TDMCliente;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal;

function DMCliente: TDMCliente;
begin
  Result := TDMCliente(DMppal.GetModuleInstance(TDMCliente));
end;


procedure TDMCliente.RutAbrirTablas;
begin
  sqlCabe.Close;
  sqlCabe.Open();

  sqlPoblacionS.Close;
  sqlPoblacionS.Open();

  sqlTFacturacio.Close;
  sqlTFacturacio.Open();

  sqlTPago.Close;
  sqlTPago.Open();

  sqlCLIEARTIS.Close;
  sqlCLIEARTIS.Open();

  sqlReservaS.Close;
  sqlReservas.Open();
end;

initialization
  RegisterModuleClass(TDMCliente);





end.


