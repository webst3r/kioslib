object DMCliente: TDMCliente
  OldCreateOrder = False
  Height = 653
  Width = 978
  object sqlCabe: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FMCLIENTES'
      'where ID_CLIENTE is not null'
      'Order by ID_CLIENTE')
    Left = 40
    Top = 112
    object sqlCabeID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlCabeNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlCabeDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeDIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlCabePOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabeCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabePROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlCabeCONTACTO1NOMBRE: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlCabeCONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlCabeTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlCabeEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlCabeWEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlCabeMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlCabeBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object sqlCabeAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object sqlCabeDC: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object sqlCabeCUENTABANCO: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object sqlCabeTDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object sqlCabeTIVACLI: TSmallintField
      FieldName = 'TIVACLI'
      Origin = 'TIVACLI'
    end
    object sqlCabeTPAGO: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object sqlCabeDIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlCabeDIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlCabeDIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlCabeSWBLOQUEO: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object sqlCabeSWTFACTURACION: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object sqlCabeSWAGRUARTIFRA: TSmallintField
      FieldName = 'SWAGRUARTIFRA'
      Origin = 'SWAGRUARTIFRA'
    end
    object sqlCabeREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlCabeTRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlCabeSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCabeFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCabeFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCabeHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCabeFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCabeHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCabeUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCabeNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 127
    Top = 4
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 210
    Top = 4
  end
  object sqlPoblacionS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select A.CODIGO,A.POSTAL,A.POBLACION,A.CPROV,B.PROVINCIA,A.CPOST' +
        'AL,A.CPAIS'
      'from  FPOBLA A'
      'left outer join FPROVI B on (A.CPROV = B.CPROVI)'
      'Order by CPOSTAL,POBLACION')
    Left = 96
    Top = 120
    object sqlPoblacionSCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPoblacionSPOSTAL: TIntegerField
      FieldName = 'POSTAL'
      Origin = 'POSTAL'
    end
    object sqlPoblacionSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 30
    end
    object sqlPoblacionSCPROV: TIntegerField
      FieldName = 'CPROV'
      Origin = 'CPROV'
    end
    object sqlPoblacionSPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object sqlPoblacionSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlPoblacionSCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      Size = 3
    end
  end
  object sqlTFacturacio: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select TTIPO, NORDEN, DESCRIPCION from FTIPOSI'
      'where TTIPO = '#39'TFACTURACI'#39
      'Order by NORDEN')
    Left = 176
    Top = 120
    object sqlTFacturacioTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTFacturacioNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTFacturacioDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object FDQuery3: TFDQuery
    Connection = DMppal.FDConnection1
    Left = 112
    Top = 328
  end
  object sqlCLIARTI1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select P.ID_CLIARTI, P.ID_CLIENTE,C.NOMBRE, P.ID_ARTICULO,A.DESC' +
        'RIPCION,A.BARRAS,'
      ' P.CANTIDAD, P.D1, P.D2, P.D3, P.D4, P.D5, P.D6, P.D7, P.DF,'
      'P.RESERVAEJEMPLAR,'
      'P.DESDEFECHA, P.HASTAFECHA, P.DESDEFECHAEX, P.HASTAFECHAEX,'
      
        'P.SWALTABAJA, P.FECHABAJA, P.FECHAALTA, P.HORAALTA, P.FECHAULTI,' +
        ' P.HORAULTI,'
      'P.USUULTI, P.NOTAULTI,P.OBSERVACIONES,P.SWAVISAR,'
      'P.FECHAGENERADAULTI'
      ' from FVCLIARTI P'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = P.ID_ARTICULO)'
      ' left outer join FMCLIENTES C on (C.ID_CLIENTE  = P.ID_CLIENTE)'
      ''
      'where P.ID_CLIARTI =:ID_CLIARTI'
      'Order by P.ID_CLIENTE,P.ID_CLIARTI')
    Left = 104
    Top = 272
    ParamData = <
      item
        Name = 'ID_CLIARTI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlCLIARTI1ID_CLIARTI: TIntegerField
      FieldName = 'ID_CLIARTI'
      Origin = 'ID_CLIARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCLIARTI1ID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlCLIARTI1NOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlCLIARTI1ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlCLIARTI1DESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlCLIARTI1BARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlCLIARTI1CANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlCLIARTI1D1: TSmallintField
      FieldName = 'D1'
      Origin = 'D1'
    end
    object sqlCLIARTI1D2: TSmallintField
      FieldName = 'D2'
      Origin = 'D2'
    end
    object sqlCLIARTI1D3: TSmallintField
      FieldName = 'D3'
      Origin = 'D3'
    end
    object sqlCLIARTI1D4: TSmallintField
      FieldName = 'D4'
      Origin = 'D4'
    end
    object sqlCLIARTI1D5: TSmallintField
      FieldName = 'D5'
      Origin = 'D5'
    end
    object sqlCLIARTI1D6: TSmallintField
      FieldName = 'D6'
      Origin = 'D6'
    end
    object sqlCLIARTI1D7: TSmallintField
      FieldName = 'D7'
      Origin = 'D7'
    end
    object sqlCLIARTI1DF: TSmallintField
      FieldName = 'DF'
      Origin = 'DF'
    end
    object sqlCLIARTI1RESERVAEJEMPLAR: TIntegerField
      FieldName = 'RESERVAEJEMPLAR'
      Origin = 'RESERVAEJEMPLAR'
    end
    object sqlCLIARTI1DESDEFECHA: TDateField
      FieldName = 'DESDEFECHA'
      Origin = 'DESDEFECHA'
    end
    object sqlCLIARTI1HASTAFECHA: TDateField
      FieldName = 'HASTAFECHA'
      Origin = 'HASTAFECHA'
    end
    object sqlCLIARTI1DESDEFECHAEX: TDateField
      FieldName = 'DESDEFECHAEX'
      Origin = 'DESDEFECHAEX'
    end
    object sqlCLIARTI1HASTAFECHAEX: TDateField
      FieldName = 'HASTAFECHAEX'
      Origin = 'HASTAFECHAEX'
    end
    object sqlCLIARTI1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCLIARTI1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCLIARTI1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCLIARTI1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCLIARTI1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCLIARTI1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCLIARTI1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCLIARTI1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlCLIARTI1OBSERVACIONES: TStringField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      Size = 50
    end
    object sqlCLIARTI1SWAVISAR: TSmallintField
      FieldName = 'SWAVISAR'
      Origin = 'SWAVISAR'
    end
    object sqlCLIARTI1FECHAGENERADAULTI: TDateField
      FieldName = 'FECHAGENERADAULTI'
      Origin = 'FECHAGENERADAULTI'
    end
  end
  object sqlTPago: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select ID_TPAGO, DESCRIPCION, TEFECTO, EFECTOS, FRECUENCIA, DIAS' +
        'PRIMERVTO from FTPAGO'
      'where ID_TPAGO is not null'
      'Order by ID_TPAGO')
    Left = 232
    Top = 128
    object sqlTPagoID_TPAGO: TIntegerField
      FieldName = 'ID_TPAGO'
      Origin = 'ID_TPAGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTPagoDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlTPagoTEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlTPagoEFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlTPagoFRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlTPagoDIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
  end
  object sqlReservaS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select R.BARRAS18, R.ID_CLIENTE, R.ID_ARTICULO, R.ADENDUM, R.ID_' +
        'HISARTIRE, R.SWSITUACION,'
      
        'R.FECHARESERVA, R.FECHAENTREGA, R.CANTIRESERVA, R.CANTIENTREGA, ' +
        'R.CANTIRECI, R.CANTIAPARTADA,'
      'A.BARRAS, A.ADENDUM, A.DESCRIPCION,'
      'C.NOMBRE, C.NIF, C.TELEFONO1'
      ' From FVRESERVA R'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = R.ID_ARTICULO)'
      ' left outer join FMCLIENTES C on (C.ID_CLIENTE = R.ID_CLIENTE)'
      ' Where R.BARRAS18 is not null'
      ' Order by R.BARRAS18')
    Left = 128
    Top = 504
    object sqlReservaSBARRAS18: TStringField
      FieldName = 'BARRAS18'
      Origin = 'BARRAS18'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 18
    end
    object sqlReservaSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlReservaSID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlReservaSADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlReservaSID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlReservaSSWSITUACION: TSmallintField
      FieldName = 'SWSITUACION'
      Origin = 'SWSITUACION'
    end
    object sqlReservaSFECHARESERVA: TDateField
      FieldName = 'FECHARESERVA'
      Origin = 'FECHARESERVA'
    end
    object sqlReservaSFECHAENTREGA: TDateField
      FieldName = 'FECHAENTREGA'
      Origin = 'FECHAENTREGA'
    end
    object sqlReservaSCANTIRESERVA: TSingleField
      FieldName = 'CANTIRESERVA'
      Origin = 'CANTIRESERVA'
    end
    object sqlReservaSCANTIENTREGA: TSingleField
      FieldName = 'CANTIENTREGA'
      Origin = 'CANTIENTREGA'
    end
    object sqlReservaSCANTIRECI: TSingleField
      FieldName = 'CANTIRECI'
      Origin = 'CANTIRECI'
    end
    object sqlReservaSCANTIAPARTADA: TSingleField
      FieldName = 'CANTIAPARTADA'
      Origin = 'CANTIAPARTADA'
    end
    object sqlReservaSBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlReservaSADENDUM_1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ADENDUM_1'
      Origin = 'ADENDUM'
      ProviderFlags = []
      ReadOnly = True
      Size = 5
    end
    object sqlReservaSDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlReservaSNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlReservaSNIF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NIF'
      Origin = 'NIF'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
    object sqlReservaSTELEFONO1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
  end
  object sqlCLIEARTIS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select P.ID_CLIARTI, P.ID_CLIENTE,C.NOMBRE, P.ID_ARTICULO,A.DESC' +
        'RIPCION,A.BARRAS,'
      ' P.CANTIDAD, P.D1, P.D2, P.D3, P.D4, P.D5, P.D6, P.D7, P.DF,'
      'P.RESERVAEJEMPLAR,'
      'P.DESDEFECHA, P.HASTAFECHA, P.DESDEFECHAEX, P.HASTAFECHAEX,'
      
        'P.SWALTABAJA, P.FECHABAJA, P.FECHAALTA, P.HORAALTA, P.FECHAULTI,' +
        ' P.HORAULTI,'
      'P.USUULTI, P.NOTAULTI,P.OBSERVACIONES'
      ' from FVCLIARTI P'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = P.ID_ARTICULO)'
      ' left outer join FMCLIENTES C on (C.ID_CLIENTE  = P.ID_CLIENTE)'
      ''
      'where P.ID_CLIARTI is not null'
      'Order by P.ID_CLIENTE,P.ID_CLIARTI')
    Left = 48
    Top = 504
    object sqlCLIEARTISID_CLIARTI: TIntegerField
      FieldName = 'ID_CLIARTI'
      Origin = 'ID_CLIARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCLIEARTISID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlCLIEARTISNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlCLIEARTISID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlCLIEARTISDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlCLIEARTISBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlCLIEARTISCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlCLIEARTISD1: TSmallintField
      FieldName = 'D1'
      Origin = 'D1'
    end
    object sqlCLIEARTISD2: TSmallintField
      FieldName = 'D2'
      Origin = 'D2'
    end
    object sqlCLIEARTISD3: TSmallintField
      FieldName = 'D3'
      Origin = 'D3'
    end
    object sqlCLIEARTISD4: TSmallintField
      FieldName = 'D4'
      Origin = 'D4'
    end
    object sqlCLIEARTISD5: TSmallintField
      FieldName = 'D5'
      Origin = 'D5'
    end
    object sqlCLIEARTISD6: TSmallintField
      FieldName = 'D6'
      Origin = 'D6'
    end
    object sqlCLIEARTISD7: TSmallintField
      FieldName = 'D7'
      Origin = 'D7'
    end
    object sqlCLIEARTISDF: TSmallintField
      FieldName = 'DF'
      Origin = 'DF'
    end
    object sqlCLIEARTISRESERVAEJEMPLAR: TIntegerField
      FieldName = 'RESERVAEJEMPLAR'
      Origin = 'RESERVAEJEMPLAR'
    end
    object sqlCLIEARTISDESDEFECHA: TDateField
      FieldName = 'DESDEFECHA'
      Origin = 'DESDEFECHA'
    end
    object sqlCLIEARTISHASTAFECHA: TDateField
      FieldName = 'HASTAFECHA'
      Origin = 'HASTAFECHA'
    end
    object sqlCLIEARTISDESDEFECHAEX: TDateField
      FieldName = 'DESDEFECHAEX'
      Origin = 'DESDEFECHAEX'
    end
    object sqlCLIEARTISHASTAFECHAEX: TDateField
      FieldName = 'HASTAFECHAEX'
      Origin = 'HASTAFECHAEX'
    end
    object sqlCLIEARTISSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCLIEARTISFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCLIEARTISFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCLIEARTISHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCLIEARTISFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCLIEARTISHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCLIEARTISUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCLIEARTISNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlCLIEARTISOBSERVACIONES: TStringField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      Size = 50
    end
  end
  object sqlCabeS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_CLIENTE,A.NOMBRE,A.DIRECCION,A.POBLACION,P.PROVINCI' +
        'A,A.CPOSTAL,A.CPROVINCIA,'
      '   A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL, A.FAX'
      'From FMCLIENTES A'
      ' left outer join'
      ' FPROVI P on (A.CPROVINCIA = P.CPROVI)'
      'where ID_CLIENTE is not null'
      'Order by ID_CLIENTE')
    Left = 32
    Top = 200
    object sqlCabeSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabeSPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object sqlCabeSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabeSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeSFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
  end
  object sqlUpdate: TFDQuery
    Left = 303
    Top = 4
  end
end
