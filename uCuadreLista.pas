unit uCuadreLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormCuadreLista = class(TUniForm)
    gridListaFra: TUniDBGrid;
    dsFraProveS: TDataSource;
    dsProveS: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel1: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel3: TUniLabel;
    UniLabel2: TUniLabel;
    edDesdeFecha: TUniDateTimePicker;
    edHastaFecha: TUniDateTimePicker;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    lbText: TUniLabel;
    btConsultaAbiertos: TUniBitBtn;
    btConsultaCerrados: TUniBitBtn;
    btConsultaTodos: TUniBitBtn;
    btNuevaDevolucion: TUniBitBtn;
    edIdProvee: TUniEdit;
    cbProvee: TUniDBLookupComboBox;
    UniLabel4: TUniLabel;
    btTodasDistri: TUniBitBtn;
    procedure cbProveeCloseUp(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure btConsultaAbiertosClick(Sender: TObject);
    procedure btConsultaCerradosClick(Sender: TObject);
    procedure btConsultaTodosClick(Sender: TObject);
    procedure btTodasDistriClick(Sender: TObject);
    procedure gridListaFraDblClick(Sender: TObject);



  private


    { Private declarations }

  public
    { Public declarations }


  end;

function FormCuadreLista: TFormCuadreLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMCuadre, uCuadreMenu, uCuadreFicha,
  uMensajes;

function FormCuadreLista: TFormCuadreLista;
begin
  Result := TFormCuadreLista(DMppal.GetFormInstance(TFormCuadreLista));

end;

procedure TFormCuadreLista.btConsultaAbiertosClick(Sender: TObject);
begin
  DMCuadre.RutAbrirFraProveS(edIdProvee.Text,0,edDesdeFecha.DateTime,edHastaFecha.DateTime,True);
  lbText.Caption := 'En Curso';
end;

procedure TFormCuadreLista.btConsultaCerradosClick(Sender: TObject);
begin
  DMCuadre.RutAbrirFraProveS(edIdProvee.Text,1,edDesdeFecha.DateTime,edHastaFecha.DateTime,True);
  lbText.Caption := 'Cerradas';
end;

procedure TFormCuadreLista.btConsultaTodosClick(Sender: TObject);
begin
  DMCuadre.RutAbrirFraProveS(edIdProvee.Text,-1,edDesdeFecha.DateTime,edHastaFecha.DateTime,True);
  lbText.Caption := 'Todas';
end;

procedure TFormCuadreLista.btTodasDistriClick(Sender: TObject);
begin
  edIdProvee.Text := '0';
  btConsultaTodosClick(nil);
end;

procedure TFormCuadreLista.cbProveeCloseUp(Sender: TObject);
begin
  edIdProvee.Text := DMCuadre.sqlProveSID_PROVEEDOR.AsString;
  btConsultaTodosClick(nil);
end;

procedure TFormCuadreLista.gridListaFraDblClick(Sender: TObject);
begin
  FormCuadreMenu.RutAbrirFicha(DMCuadre.sqlFraProveSID_FRAPROVE.AsInteger);
  DMppal.swCuadreFicha := self.Name;
end;

procedure TFormCuadreLista.UniFormCreate(Sender: TObject);
begin
  //edDesdeFecha.DateTime := now - 30;
  edDesdeFecha.DateTime := now -365;
  edHastaFecha.DateTime := now + 15;
end;

end.
