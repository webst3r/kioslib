unit uDevolLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, Vcl.StdCtrls, Vcl.Buttons,
  uniDBText, uniImageList, Vcl.Imaging.pngimage;

type
  TFormDevolLista = class(TUniForm)
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel1: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    dsCabeS: TDataSource;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    pcControl: TUniPageControl;
    tabGrid: TUniTabSheet;
    gridListaDevolucion: TUniDBGrid;
    dsProve1: TDataSource;
    plRecalcularPendiente: TUniPanel;
    tabSeleccionLineas: TUniTabSheet;
    PnlSeleDevol: TUniPanel;
    RgFechaAviDevo: TUniDBRadioGroup;
    GbxSelDevoluFecha: TUniGroupBox;
    pFechaOri: TUniDBDateTimePicker;
    LblDiaOri: TUniLabel;
    LblSelDevolucion: TUniLabel;
    LbFechaOri: TUniLabel;
    UniLabel8: TUniLabel;
    BtnCacelaSeleccion: TUniButton;
    BtSeleccionar: TUniButton;
    dsCabe1: TDataSource;
    edDesdeFecha: TUniDateTimePicker;
    edHastaFecha: TUniDateTimePicker;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    lbText: TUniLabel;
    btPaquetes: TUniBitBtn;
    btFichaAgrupada: TUniBitBtn;
    btConsultaAbiertos: TUniBitBtn;
    btConsultaCerrados: TUniBitBtn;
    btConsultaTodos: TUniBitBtn;
    btFichaIndividual: TUniBitBtn;
    btEjecutar: TUniBitBtn;
    UniLabel2: TUniLabel;
    edBusqueda: TUniEdit;
    btNuevaDevolucion: TUniBitBtn;
    UniLabel5: TUniLabel;
    edBusquedaArti: TUniEdit;
    lbNumeroArti: TUniLabel;
    procedure gridListaDevolucionDblClick(Sender: TObject);
    procedure btFichaAgrupadaClick(Sender: TObject);
    procedure btFichaIndividualClick(Sender: TObject);
    procedure btSalirClick(Sender: TObject);
    procedure btConsultaCerradosClick(Sender: TObject);
    procedure btConsultaAbiertosClick(Sender: TObject);
    procedure gridListaDevolucionFieldImage(const Column: TUniDBGridColumn;
      const AField: TField; var OutImage: TGraphic; var DoNotDispose: Boolean;
      var ATransparent: TUniTransparentOption);
    procedure btConsultaTodosClick(Sender: TObject);
    procedure gridListaDevolucionColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure UniFormCreate(Sender: TObject);
    procedure btNuevaDevolucionClick(Sender: TObject);
    procedure btPaquetesClick(Sender: TObject);
    procedure btEjecutarClick(Sender: TObject);
    procedure edBusquedaChange(Sender: TObject);
    procedure edBusquedaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edBusquedaArtiChange(Sender: TObject);
    procedure edBusquedaArtiKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edBusquedaArtiEnter(Sender: TObject);


  private



    { Private declarations }

  public
    { Public declarations }
    procedure RutNuevaDevolucion(vTipo : Integer);
    procedure RutAlbaranClick(vCabe: String; swDevolucion : Integer);
    procedure RutFichaAgrupadaClick(vCabe: String);
    procedure RutBuscarArti;


  end;

function FormDevolLista: TFormDevolLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMDevol, uDevolMenu, uDevolFicha, uDevolNuevo, uDevolPaquetes;

function FormDevolLista: TFormDevolLista;
begin
  Result := TFormDevolLista(DMppal.GetFormInstance(TFormDevolLista));

end;

procedure TFormDevolLista.btNuevaDevolucionClick(Sender: TObject);
begin
  //if FormDevolMenu.swDevoAlbaran = 0 then DMDevolucion.RutAbrirTablasNuevo;//aqui al venir de la lista sqlcabe1 aun no esta abierto
 // DMDevolucion.RutAbrirTablasNuevo;//abre sqlDevolNuevo
  FormDevolLista.RutNuevaDevolucion(1);
end;

procedure TFormDevolLista.RutNuevaDevolucion(vTipo : Integer);
begin

  DMDevolucion.RutAbrirTablasNuevo;
  DMDevolucion.RutNuevaCabe(0);
  FormDevolNuevo.RutInicioForm(False,vTipo);
  FormDevolNuevo.ShowModal();


end;


procedure TFormDevolLista.btFichaIndividualClick(Sender: TObject);
begin
  if DMDevolucion.sqlCabeS.RecordCount <> 0 then
  begin
    RutAlbaranClick(' = ' + IntToStr(DMDevolucion.sqlCabeSIDSTOCABE.AsInteger),DMDevolucion.sqlCabeSSWDEVOLUCION.AsInteger);
    DMDevolucion.RutScrollCabe1;
  end;
  DMppal.swDevolucionFicha := self.Name;
end;


procedure TFormDevolLista.RutAlbaranClick(vCabe : String; swDevolucion : Integer);
begin

  FormDevolMenu.RutAbrirAlbaranTab;
//  DMDevolucion.RutAbrirAlbaran(vIDCabe,DMDevolucion.sqlCabeSSWTIPODOCU.AsInteger,edDesdeFecha.DateTime,edHastaFecha.DateTime,False);
  //se pasa = 2 o 22 por que cuando cerramos o abrimos una devolucion el registro se va de la tabla,
  // por que si la query es de abiertas y cerramos una devolucion al refrescar, el registro cerrado ya no aparece ne la query de abiertos
  //entonces ya no podemos pasar el DMDevolucion.sqlCabeSSWTIPODOCU.AsInteger por que ya no es el q tenemos seleccionado
  //pero si tenemos el idCabe
  DMDevolucion.RutAbrirAlbaran(vCabe,' = 2 or SWTIPODOCU = 22 ', swDevolucion,edDesdeFecha.DateTime,edHastaFecha.DateTime,true);
//  DMDevolucion.RutAbrirHisArtiCabe1;
  FormDevolFicha.pBarrasA.SetFocus;
  FormDevolFicha.swUnico := true;

end;

procedure TFormDevolLista.btFichaAgrupadaClick(Sender: TObject);
begin
  RutFichaAgrupadaClick(' > 0 ');
  DMppal.swDevolucionFicha := self.Name;
  DMDevolucion.RutScrollCabe1;
end;

procedure TFormDevolLista.RutFichaAgrupadaClick(vCabe : String);
begin

  FormDevolMenu.RutAbrirAlbaranTab;
  DMDevolucion.RutAbrirAlbaran(vCabe,'= 22 ', 1,edDesdeFecha.DateTime,edHastaFecha.DateTime,true);
  FormDevolFicha.swUnico := false;
  FormDevolFicha.pBarrasA.SetFocus;

end;

procedure TFormDevolLista.btSalirClick(Sender: TObject);
begin
  if FormDevolMenu.swDevoAlbaran = 1 then FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabAlbaranDevolucion
  else
  begin
    FormDevolMenu.swDevoLista := 0;
    FormMenu.swMenuBts := False;

    self.Close;
  end;

end;

procedure TFormDevolLista.gridListaDevolucionColumnSort(
  Column: TUniDBGridColumn; Direction: Boolean);
begin
  DMppal.SortColumnSQL(DMDevolucion.sqlCabeS, Column.FieldName, Direction);
end;

procedure TFormDevolLista.gridListaDevolucionDblClick(Sender: TObject);
var
  vWhere : String;
begin
 // DMDevolucion.RutAbrirCabe1;
  btFichaIndividualClick(nil);
end;



procedure TFormDevolLista.gridListaDevolucionFieldImage(
  const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
  var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(AField.FieldName, 'SWDEVOLUCION') then
  begin
    DoNotDispose := True; // we provide an static image so do not free it.
//    if DMDevolucion.sqlCabeSSWDEVOLUCION.AsString = '1' then  OutImage := imgAbierto.Picture.Graphic;

    if DMDevolucion.sqlCabeSSWDEVOLUCION.AsString = '1' then  OutImage := FormMenu.imgAbierto.Picture.Graphic;
    if DMDevolucion.sqlCabeSSWDEVOLUCION.AsString = '3' then  OutImage := FormMenu.ImgCerrado.Picture.Graphic;
{
    if AField.Value = 2 then  OutImage := UniImage2.Picture.Graphic;
    if AField.Value = 3 then  OutImage := UniImage3.Picture.Graphic;
    if AField.Value = 4 then  OutImage := UniImage4.Picture.Graphic;
}
//    OutImage := UniNativeImageList1.Images[14].Graphic; // UniImage1.Picture.Graphic
  end;

  if SameText(AField.FieldName, 'SWTIPODOCU') then
  begin
    DoNotDispose := True; // we provide an static image so do not free it.
    if DMDevolucion.sqlCabeSSWTIPODOCU.AsString = '22' then  OutImage := FormMenu.imgAgrupada.Picture.Graphic;
    if DMDevolucion.sqlCabeSSWTIPODOCU.AsString = '2'  then  OutImage := FormMenu.ImgIndividual.Picture.Graphic;

  end;

end;

procedure TFormDevolLista.btConsultaAbiertosClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirCabeS(1,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,'');
  lbText.Caption := 'En Curso';
  lbNumeroArti.Caption := '';
end;

procedure TFormDevolLista.btConsultaCerradosClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirCabeS(3,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,'');
  lbText.Caption := 'Cerradas';
  lbNumeroArti.Caption := '';
end;

procedure TFormDevolLista.btConsultaTodosClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirCabeS(0,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,'');
  lbText.Caption := 'Todas';
  lbNumeroArti.Caption := '';
end;

procedure TFormDevolLista.btEjecutarClick(Sender: TObject);
begin
  btConsultaAbiertosClick(nil);
end;

procedure TFormDevolLista.UniFormCreate(Sender: TObject);
begin
  edDesdeFecha.DateTime := now - 30;
  edHastaFecha.DateTime := now + 15;

  with GridListaDevolucion do
  begin
    Columns[0].Width := 78;
    Columns[1].Width := 80;
    Columns[2].Width := 225;
    Columns[3].Width := 95;
    Columns[4].Width := 40;
    Columns[5].Width := 80;
    Columns[6].Width := 80;
    Columns[7].Width := 64;
    Columns[8].Width := 50;

  end;

end;

procedure TFormDevolLista.btPaquetesClick(Sender: TObject);
begin
//  DMDevolucion.RutAbrirPaquetes(DMDevolucion.sqlCabeSID_PROVEEDOR.AsInteger,DMDevolucion.sqlCabeSIDSTOCABE.AsInteger);
  FormDevolPaquetes.RutInicioForm(DMDevolucion.sqlCabeSTOTALPAQUETES.AsString,DateToStr(DMDevolucion.sqlCabeSFECHA.AsDateTime)
                                  ,DMDevolucion.sqlCabeSID_PROVEEDOR.AsInteger,DMDevolucion.sqlCabeSIDSTOCABE.AsInteger);
  FormDevolPaquetes.ShowModal;
end;



procedure TFormDevolLista.edBusquedaChange(Sender: TObject);
begin
  if (length(edBusqueda.Text) >= 5) then
  begin
    DMDevolucion.swBusquedaArti := False;
    DMDevolucion.RutAbrirCabeS(0,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,edBusqueda.Text);
    lbText.Caption := 'Todas';
  end;
end;

procedure TFormDevolLista.edBusquedaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (length(edBusqueda.Text) <= 5) then
  begin
    DMDevolucion.swBusquedaArti := False;
    DMDevolucion.RutAbrirCabeS(0,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,edBusqueda.Text);
    lbText.Caption := 'Todas';
  end;
end;


procedure TFormDevolLista.edBusquedaArtiChange(Sender: TObject);
begin
//  if (length(edBusqueda.Text) >= 3) then
//  begin
    RutBuscarArti;
//  end;
end;

procedure TFormDevolLista.edBusquedaArtiEnter(Sender: TObject);
begin
  DMppal.swConsArti := self.Name;
  FormMenu.RutAbrirConsArti;
end;

procedure TFormDevolLista.edBusquedaArtiKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  if (Key = 13) and (length(edBusqueda.Text) <= 3) then
//  begin
    RutBuscarArti;
//  end;
end;

procedure TFormDevolLista.RutBuscarArti;
begin
  DMDevolucion.swBusquedaArti := True;
  //edBusqueda.Text := DMppal.sqlArticulosID_ARTICULO.AsString;
  DMDevolucion.RutAbrirCabeS(0,edDesdeFecha.DateTime,edHastaFecha.DateTime,True,lbNumeroArti.Caption);
  lbText.Caption := 'Articulo';
end;

end.

