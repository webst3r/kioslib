object FormDevolMenu: TFormDevolMenu
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1100
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalle: TUniPageControl
    Left = 0
    Top = 0
    Width = 1100
    Height = 615
    Hint = ''
    ShowHint = True
    ActivePage = tabListaDevolucion
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tabListaDevolucion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabListaDevolucion'
      Layout = 'fit'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
    object tabAlbaranDevolucion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabAlbaranDevolucion'
      Layout = 'fit'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
    object tabNuevaDevolucion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabNuevaDevolucion'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
  end
end
