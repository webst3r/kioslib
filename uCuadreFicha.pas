unit uCuadreFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormCuadreFicha = class(TUniForm)
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel9: TUniLabel;
    pcModo: TUniPageControl;
    tabPorDevolucion: TUniTabSheet;
    tabPorArticulos: TUniTabSheet;
    UniDBGrid1: TUniDBGrid;
    UniDBGrid2: TUniDBGrid;
    edDesdeCargo: TUniDBDateTimePicker;
    edHastaCargo: TUniDBDateTimePicker;
    edDesdeAbono: TUniDBDateTimePicker;
    edHastaAbono: TUniDBDateTimePicker;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    rgOpcion: TUniRadioGroup;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    dsFraProve1: TDataSource;
    dsHistoP: TDataSource;
    dsAlbaAbonoP: TDataSource;
    procedure rgOpcionClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);



  private

    { Private declarations }

  public
    { Public declarations }


  end;

function FormCuadreFicha: TFormCuadreFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu
  , uDMCuadre, uCuadreMenu, uCuadreLista;

function FormCuadreFicha: TFormCuadreFicha;
begin
  Result := TFormCuadreFicha(DMppal.GetFormInstance(TFormCuadreFicha));

end;

procedure TFormCuadreFicha.rgOpcionClick(Sender: TObject);
begin
  pcModo.ActivePageIndex := rgOpcion.ItemIndex;
end;

procedure TFormCuadreFicha.UniFormCreate(Sender: TObject);
begin
  rgOpcionClick(nil);
end;

end.
