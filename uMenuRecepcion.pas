unit uMenuRecepcion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText;

type
  TFormMenuRecepcion = class(TUniForm)
    tmSession: TUniTimer;
    pcDetalle: TUniPageControl;
    tabListaRecepcion: TUniTabSheet;
    tabAlbaranRecepcion: TUniTabSheet;
    tabReservas: TUniTabSheet;
    UniPanel1: TUniPanel;
    btBuscar: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    btNuevoAlbaran: TUniBitBtn;
    btCerrarAlba: TUniBitBtn;
    btRefrescarImagenes: TUniBitBtn;
    btExcelBuscar: TUniBitBtn;
    btModificarTarifas: TUniBitBtn;
    btGrabarTarifas: TUniBitBtn;
    btVerUltiCompra: TUniBitBtn;
    btConfigurar: TUniBitBtn;
    btReservas: TUniBitBtn;
    btListarDocu: TUniBitBtn;
    btSalir: TUniBitBtn;
    UniPanel2: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniDBText1: TUniDBText;
    UniDBText2: TUniDBText;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    dsListaRecepcion1: TDataSource;
    procedure UniBitBtn1Click(Sender: TObject);
    procedure UniBitBtn3Click(Sender: TObject);
    procedure UniBitBtn4Click(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }

    swListaRecepcion, swFichaRecepcion : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    procedure RutAbrirFichaRecepcion;
    procedure RutAbrirListaRecepcion;

  end;

function FormMenuRecepcion: TFormMenuRecepcion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uRecepcionLista, uRecepcionAlbaran, uRecepcionReserva, uDMRecepcion;

function FormMenuRecepcion: TFormMenuRecepcion;
begin
  Result := TFormMenuRecepcion(DMppal.GetFormInstance(TFormMenuRecepcion));

end;

procedure TFormMenuRecepcion.RutAbrirFichaRecepcion;
begin
  pcDetalle.ActivePage := tabAlbaranRecepcion;
  if pcDetalle.ActivePage = tabAlbaranRecepcion then
  begin
    if swFichaRecepcion = 0 then
    begin
      FormAlbaranRecepcion.Parent := tabAlbaranRecepcion;
      FormAlbaranRecepcion.Show();
      swFichaRecepcion := 1;
    end;
  end;
end;

procedure TFormMenuRecepcion.RutAbrirListaRecepcion;
begin
  pcDetalle.ActivePage := tabListaRecepcion;
  if pcDetalle.ActivePage = tabListaRecepcion then
  begin
    if swListaRecepcion = 0 then
    begin
      FormListaRecepcion.Parent := tabListaRecepcion;
      FormListaRecepcion.Show();
      swListaRecepcion := 1;
    end;
  end;
end;

procedure TFormMenuRecepcion.UniBitBtn1Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaRecepcion;
end;


procedure TFormMenuRecepcion.UniBitBtn3Click(Sender: TObject);
begin


    if DMMenuRecepcion.sqlListaRecepcion.RecordCount = 0  then exit;

    DMMenuRecepcion.RutAbrirHisArti(DMMenuRecepcion.sqlListaRecepcionIDSTOCABE.AsString);

    pcDetalle.ActivePage := tabAlbaranRecepcion;

    FormAlbaranRecepcion.Parent := tabAlbaranRecepcion;
    FormAlbaranRecepcion.Align  := alClient;
    FormAlbaranRecepcion.Show();
    DMppal.RutInicioForm;



end;

procedure TFormMenuRecepcion.UniBitBtn4Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabReservas;

  FormReservas.Parent := tabReservas;
  FormReservas.Align := alClient;
  FormReservas.Show();
  DMppal.RutInicioForm;
  DMMenuRecepcion.RutAbrirReservasRecepcion;
end;


end.
