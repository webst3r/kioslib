unit uDevolFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox;

type
  TFormDevolFicha = class(TUniForm)
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    UniBitBtn29: TUniBitBtn;
    btModificar: TUniBitBtn;
    UniBitBtn31: TUniBitBtn;
    UniBitBtn17: TUniBitBtn;
    UniBitBtn22: TUniBitBtn;
    UniBitBtn23: TUniBitBtn;
    UniBitBtn32: TUniBitBtn;
    btLista: TUniBitBtn;
    btOpciones: TUniBitBtn;
    dsLinea: TDataSource;
    dsCabe1: TDataSource;
    dsHistoAnteF: TDataSource;
    dsHistoAnteFT: TDataSource;
    dsHistoDevolver: TDataSource;
    dsHistoDevolverProve: TDataSource;
    dsCabeSCap: TDataSource;
    dsProveS: TDataSource;
    UniPageControl3: TUniPageControl;
    dsCabeNuevo: TDataSource;
    dsLineaTotal: TDataSource;
    TabSheet6: TUniTabSheet;
    pnlCBarras: TUniPanel;
    UniLabel9: TUniLabel;
    pBarrasA: TUniEdit;
    pAdendumEntrar: TUniDBEdit;
    UniLabel10: TUniLabel;
    pCantidad: TUniDBEdit;
    UniLabel11: TUniLabel;
    BtnRestarCanti: TUniBitBtn;
    pGraDescripcion: TUniDBEdit;
    UniLabel59: TUniLabel;
    UniLabel39: TUniLabel;
    UniLabel40: TUniLabel;
    pPrecio: TUniDBEdit;
    pPreu: TUniDBEdit;
    UniDBEdit37: TUniDBEdit;
    pPreu2: TUniDBEdit;
    btMasInfo: TUniBitBtn;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    UniLabel37: TUniLabel;
    UniDBEdit30: TUniDBEdit;
    paStock: TUniDBEdit;
    pMerma: TUniDBEdit;
    UniDBEdit33: TUniDBEdit;
    UniDBEdit34: TUniDBEdit;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    pFeAbono: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    btArriba: TUniBitBtn;
    btAnularLinea: TUniButton;
    UniEdit1: TUniDBEdit;
    UniLabel4: TUniLabel;
    btAbajoTodo: TUniBitBtn;
    btAbajo: TUniBitBtn;
    btArribaTodo: TUniBitBtn;
    edMensajes: TUniEdit;
    UniEdit2: TUniEdit;
    BtnSumarCanti: TUniBitBtn;
    btBuscarArti: TUniBitBtn;
    UniDBText1: TUniDBText;
    pnlTotales: TUniPanel;
    pMargen1: TUniDBEdit;
    pMargen2: TUniDBEdit;
    UniLabel41: TUniLabel;
    pTIva1: TUniDBEdit;
    pTIva2: TUniDBEdit;
    UniDBEdit47: TUniDBEdit;
    UniDBEdit48: TUniDBEdit;
    UniDBEdit49: TUniDBEdit;
    UniDBEdit50: TUniDBEdit;
    UniLabel43: TUniLabel;
    UniLabel44: TUniLabel;
    UniLabel45: TUniLabel;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel46: TUniLabel;
    UniLabel47: TUniLabel;
    UniDBEdit55: TUniDBEdit;
    UniDBEdit56: TUniDBEdit;
    UniDBEdit57: TUniDBEdit;
    UniDBEdit58: TUniDBEdit;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniDBEdit59: TUniDBEdit;
    UniDBEdit60: TUniDBEdit;
    UniDBEdit61: TUniDBEdit;
    UniDBEdit62: TUniDBEdit;
    UniLabel50: TUniLabel;
    UniLabel51: TUniLabel;
    UniLabel52: TUniLabel;
    UniLabel53: TUniLabel;
    UniLabel54: TUniLabel;
    UniDBText5: TUniDBText;
    UniDBText6: TUniDBText;
    UniDBText7: TUniDBText;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniDBText8: TUniDBText;
    UniDBText9: TUniDBText;
    GridPreSeleccio: TUniDBGrid;
    UniPanel2: TUniPanel;
    UniLabel3: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    lbPaquetes: TUniLabel;
    UniPanel1: TUniPanel;
    UniPanel3: TUniPanel;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniPanel4: TUniPanel;
    gridCabe1: TUniDBGrid;
    pnlNavigator: TUniPanel;
    btSumarPaquete: TUniBitBtn;
    btRestarPaquete: TUniBitBtn;
    UniPanel5: TUniPanel;
    btHistoArti: TUniBitBtn;
    btVerFicha: TUniBitBtn;
    pEncarte2: TUniDBEdit;
    pEncarte1: TUniDBEdit;
    UniLabel42: TUniLabel;
    btVerUltiCompra: TUniBitBtn;
    btPaquetes: TUniBitBtn;
    btModificarDesc: TUniButton;
    procedure btListaClick(Sender: TObject);
    procedure btOpcionesClick(Sender: TObject);
    procedure btMasInfoClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure btCerrarFichaClick(Sender: TObject);
    procedure pBarrasAExit(Sender: TObject);
    procedure GridPreSeleccioColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure pBarrasAEnter(Sender: TObject);
    procedure BtnSumarCantiClick(Sender: TObject);
    procedure BtnRestarCantiClick(Sender: TObject);
    procedure pCantidadExit(Sender: TObject);
    procedure pCantidadEnter(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure btAnularLineaClick(Sender: TObject);
    procedure pAdendumEntrarEnter(Sender: TObject);
    procedure pAdendumEntrarExit(Sender: TObject);
    procedure btAbajoTodoClick(Sender: TObject);
    procedure btAbajoClick(Sender: TObject);
    procedure btArribaClick(Sender: TObject);
    procedure btArribaTodoClick(Sender: TObject);
    procedure btRestarPaqueteClick(Sender: TObject);
    procedure btBuscarArtiClick(Sender: TObject);
    procedure btSumarPaqueteClick(Sender: TObject);
    procedure pPreuExit(Sender: TObject);
    procedure pPreu2Exit(Sender: TObject);
    procedure pTIva1Exit(Sender: TObject);
    procedure pTIva1Enter(Sender: TObject);
    procedure pTIva2Exit(Sender: TObject);
    procedure pTIva2Enter(Sender: TObject);
    procedure btVerFichaClick(Sender: TObject);
    procedure btHistoArtiClick(Sender: TObject);
    procedure pCantidadChange(Sender: TObject);
    procedure UniEdit2Enter(Sender: TObject);
    procedure btVerUltiCompraClick(Sender: TObject);
    procedure btPaquetesClick(Sender: TObject);
    procedure pPreuEnter(Sender: TObject);
    procedure pPreu2Enter(Sender: TObject);
    procedure pMargen1Enter(Sender: TObject);
    procedure pMargen2Enter(Sender: TObject);
    procedure UniDBEdit51Enter(Sender: TObject);
    procedure pPreuKeyPress(Sender: TObject; var Key: Char);
    procedure btModificarDescClick(Sender: TObject);


  private
    procedure RutLeerProveedor(vID: Integer);
    function CheckString(const str: string): boolean;

    procedure ProLlegirArticle(vIdArti: Integer);






    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;
    vTIVA, vTIVA2 : Integer;
    vAden : String;
    vIDProve : Integer;
    swClicPnl : Boolean;
     swUnico : Boolean;

     swExiste :Integer ;

     procedure RutMensajeArticulo(vTipoMensaje : Integer);
     procedure RutMensajeDistri;
     procedure RutAnularLinea;
     function RutFormatearAdendum(vAdendum: String; vTBarras: Integer): String;
     procedure RutHistoArtiClick;
  end;

function FormDevolFicha: TFormDevolFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDevolLista,  uDMDevol, uDevolNuevo, uDevolMenu,
  ufrFicha, uConstVar, uListaArti, uConsArti, uMensajes, uMenuArti,
  uDMHistoArti, uFichaArti, uDMUltiCompra, uUltiCompra, uDevolPaquetes,
  uModificarDesc, uDMModificarDesc;

function FormDevolFicha: TFormDevolFicha;
begin
  Result := TFormDevolFicha(DMppal.GetFormInstance(TFormDevolFicha));

end;

procedure TFormDevolFicha.btImprimirClick(Sender: TObject);
begin
  FormfrFicha.vModifica    := False;
  FormfrFicha.InvNum       := '1';
  FormfrFicha.vTipoInforme := 'iDevol';
  FormfrFicha.vExportPDF   := False;
  FormfrFicha.ShowModal;

  DMDevolucion.RutCalcularCamposInforme;

end;

procedure TFormDevolFicha.btListaClick(Sender: TObject);
begin
  FormDevolMenu.RutAbrirLista;
end;

procedure TFormDevolFicha.btOpcionesClick(Sender: TObject);
begin
   UniPanel27.Visible := not unipanel27.visible;
end;

procedure TFormDevolFicha.btPaquetesClick(Sender: TObject);
begin
  FormDevolPaquetes.RutInicioForm(DMDevolucion.sqlCabe1TOTALPAQUETES.AsString,DateToStr(DMDevolucion.sqlCabe1FECHA.AsDateTime)
                                  ,DMDevolucion.sqlCabe1ID_PROVEEDOR.AsInteger,DMDevolucion.sqlCabe1IDSTOCABE.AsInteger);
  FormDevolPaquetes.ShowModal;
end;

procedure TFormDevolFicha.btRestarPaqueteClick(Sender: TObject);
begin
  if DMDevolucion.sqlCabe1PAQUETE.AsInteger = 1 then exit;

  DMDevolucion.RutCambiarPaquete(DMDevolucion.sqlCabe1PAQUETE.AsInteger - 1);
{  if DMDevolucion.sqlCabe1.RecordCount <> 0 then
  begin
    DMDevolucion.sqlCabe1.Edit;
    DMDevolucion.sqlCabe1PAQUETE.AsInteger := DMDevolucion.sqlCabe1PAQUETE.AsInteger - 1;
    DMDevolucion.sqlCabe1.Post;

    DMDevolucion.sqlCabe1.Refresh;
    DMDevolucion.RutScrollCabe1;
  end;}
end;

procedure TFormDevolFicha.btSumarPaqueteClick(Sender: TObject);
begin
  DMDevolucion.RutCambiarPaquete(DMDevolucion.sqlCabe1PAQUETE.AsInteger + 1);
{  if DMDevolucion.sqlCabe1.RecordCount <> 0 then
  begin
    DMDevolucion.sqlCabe1.Edit;
    DMDevolucion.sqlCabe1PAQUETE.AsInteger := DMDevolucion.sqlCabe1PAQUETE.AsInteger + 1;
    DMDevolucion.sqlCabe1.Post;

    DMDevolucion.sqlCabe1.Refresh;
    DMDevolucion.RutScrollCabe1;
  end;
  }
end;

procedure TFormDevolFicha.btVerFichaClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount > 0 then
  begin
    DMMenuArti.swCreando := false;
    DMMenuArti.RutAbrirFichaArticulos(DMDevolucion.sqlLineaID_ARTICULO.AsString);
    FormMenuArti.RutAbrirDevolArti;
    FormManteArti.RutHabitlitarBTs(False);
    //para saber donde volver atras en caso de no venir de la lista
    //FormManteArti.vStringOrigen := 'fichaDevol';
    DMppal.swArtiFicha := self.Name;
    FormManteArti.RutShow;

    DMppal.swArtiFicha := self.Name;
  end;
end;

procedure TFormDevolFicha.btVerUltiCompraClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount = 0 then exit;

  DMUltiCompra.RutAbrirUltiCompra(DMDevolucion.sqlLineaID_ARTICULO.AsInteger);
  FormUltiCompra.ShowModal();
end;

procedure TFormDevolFicha.GridPreSeleccioColumnSummary(
  Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'nomrep') then
  begin
    if Column.AuxValue = NULL then Column.AuxValue:='';
    Column.AuxValue    := 'Total';
  end else
  begin
    if Column.AuxValue = NULL then Column.AuxValue:=0.0;
    Column.AuxValue    := Column.AuxValue + Column.Field.AsFloat;
  end;
end;

procedure TFormDevolFicha.pAdendumEntrarEnter(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clLime;
end;

procedure TFormDevolFicha.pAdendumEntrarExit(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clWindow;

  ProLlegirArticle(DMDevolucion.sqlLineaID_ARTICULO.AsInteger);

  vAden := pAdendumEntrar.Text;
  vAden := RutFormatearAdendum(vAden, DMDevolucion.sqlArticuloTBARRAS.asInteger);
  if (StrToIntDef(pAdendumEntrar.Text,0) > 0) and (DMDevolucion.sqlLinea.RecordCount > 0) then
  begin
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaADENDUM.AsString := vAden;
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

  end;


end;

procedure TFormDevolFicha.ProLlegirArticle (vIdArti: Integer);
begin

  DMDevolucion.sqlArticulo.Close;
  DMDevolucion.sqlArticulo.Params.ParamByName('ID_ARTICULO').AsInteger := vIdArti;
  DMDevolucion.sqlArticulo.Open;
end;

procedure TFormDevolFicha.pBarrasAEnter(Sender: TObject);
begin
  (sender as TUniEdit).Color := clLime;
  pBarrasA.Text  := '';

end;

function TFormDevolFicha.CheckString(const str: string): boolean;
var i: integer;
begin
 result := false;
 for i := 1 to Length(str) do
  if CharInSet(str[i], ['a'..'z', 'A'..'Z']) then
  begin
   result := true;
   exit;
  end;
end;


procedure TFormDevolFicha.pBarrasAExit(Sender: TObject);
begin
  pBarrasA.Color := clWindow;




  if pBarrasA.Text > '' then
  begin
    if CheckString(pBarrasA.Text) = true then
    begin
      FormMenu.RutAbrirConsArti;
      FormConsArti.RutInicioShow;
      FormConsArti.vIdEditorial := 0;
      DMppal.swConsArti := self.Name;
      DMppal.RutConsArti(pBarrasA.Text,0);
    end else
    begin
      DMppal.RutSepararCodigoBarras(pBarrasA.Text);




      DMDevolucion.RutLeerCodigoBarras;
      if DMDevolucion.swRepetido then exit;

      if DMDevolucion.sqlLinea.Recordcount > 0 then
      begin
        DMDevolucion.sqlLinea.DisableControls;
        GridPreSeleccio.DataSource := nil;
        DMDevolucion.sqlLinea.RefreshRecord();
        DMDevolucion.sqlLineaTotal.Refresh;
        DMDevolucion.RutComprobarPaquete;

        GridPreSeleccio.DataSource := dsLinea;
        DMDevolucion.sqlLinea.EnableControls;
      end;


      DMDevolucion.RutLocateLinea(DMppal.sqlHisArtiDev1.FieldByName('ID_HISARTI').AsInteger);







//  DMDevolucion.sqlCabe1.Refresh; //para actualizar el cabe1 el totalDevueltos
//sobra el campo totalDevueltos, esta en DevolLista totalPaquetes/UltimoPaquete
    end;
  end;
end;

procedure TFormDevolFicha.RutMensajeDistri;
begin
  if swUnico then
  begin
    FormMensajes.RutInicioForm(1, 'FORMDEVOLFICHA', 'ACEPTAR','La Distribuidora del articulo no existe en la devolucion');
    FormMensajes.ShowModal();
    pBarrasA.SetFocus;
  end else
  begin
    RutLeerProveedor(DMppal.vID);
    FormMensajes.RutInicioForm(2, 'FORMDEVOLFICHA', 'ACEPTARCANC','La Distribuidora del articulo no existe en la devolucion, quieres a�adirla?');
    FormMensajes.ShowModal();
    pCantidad.SetFocus;

  end;


end;


procedure TFormDevolFicha.RutLeerProveedor(vID:Integer);
begin

  with DMppal.sqlProveedor2 do
  begin
    Close;
    SQL.Text := 'Select * ' +
                'From FMPROVE ' +
                'Where ID_PROVEEDOR = ' + IntToStr(vID);


    Open;
  end;

end;



procedure TFormDevolFicha.RutMensajeArticulo(vTipoMensaje : Integer);
begin
    if vTipomensaje = 1 then
    begin
      edMensajes.Text := 'Correcto';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 3 then
    begin
      edMensajes.Text := 'No has Comprado el Articulo';
      edMensajes.Color := clYellow;
    end;

    if vTipoMensaje = 4 then
    begin
      edMensajes.Text := '�No existe ninguna distribuidora para este articulo!';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 5 then
    begin
      edMensajes.Text := '�Este Articulo no existe!';
      edMensajes.Color := clRed;
      FormMensajes.RutInicioForm(3, 'FORMDEVOLFICHA', 'ACEPTARCANC','�Quieres Crear un Nuevo Articulo?');
      FormMensajes.ShowModal();


    end;

    if vTipoMensaje = 6 then
    begin
      edMensajes.Text := 'Devoluci�n Cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 7 then
    begin
      edMensajes.Text := 'Devoluci�n abierta';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 8  then
    begin
      edMensajes.Text := 'No se permite anular la linea si esta la devoluci�n cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 9  then
    begin
      edMensajes.Text := 'Se han Actualizado los precios';
      edMensajes.Color := clYellow;
    end;

    if vTipoMensaje = 10  then
    begin
      edMensajes.Text := 'Art�culo dado de alta automaticamente en tu Base de Datos';
      edMensajes.Color := clYellow;
    end;
end;



procedure TFormDevolFicha.pCantidadChange(Sender: TObject);
begin
  if pCantidad.Text = '' then
  begin
    pCantidad.Text := IntToStr(0);
  end;

  if (StrToIntDef(pCantidad.Text,0) > 0) and (DMDevolucion.sqlLinea.RecordCount > 0) then
  begin
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaCANTIDAD.AsInteger := StrToInt(pCantidad.Text);
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

    DMDevolucion.sqlLineaTotal.Refresh;
    DMDevolucion.RutComprobarPaquete;
  end;

  pBarrasA.SetFocus;
end;

procedure TFormDevolFicha.pCantidadEnter(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clLime;
  pCantidad.SelectAll;
end;



procedure TFormDevolFicha.pCantidadExit(Sender: TObject);
begin


  (sender as TUniDBEdit).Color := clWindow;
  


end;

procedure TFormDevolFicha.pMargen1Enter(Sender: TObject);
begin
  pMargen1.SelectAll;
end;

procedure TFormDevolFicha.pMargen2Enter(Sender: TObject);
begin
  pMargen1.SelectAll;
end;

procedure TFormDevolFicha.pPreu2Enter(Sender: TObject);
begin
  pPreu2.SelectAll;
end;

procedure TFormDevolFicha.pPreu2Exit(Sender: TObject);
begin

  DMDevolucion.sqlLinea.FieldByName('PRECIOCOMPRA2').AsFloat := DMDevolucion.sqlLinea.FieldByName('PRECIOVENTA2').AsFloat;
  Dmppal.ProSortidaPreus (1);
end;

procedure TFormDevolFicha.pPreuEnter(Sender: TObject);
begin
  pPreu.SelectAll;
end;

procedure TFormDevolFicha.pPreuExit(Sender: TObject);
begin
  DMppal.ProSortidaPreus (2);
end;

procedure TFormDevolFicha.pPreuKeyPress(Sender: TObject; var Key: Char);
begin
  if ((Uppercase(key)='.')) then key:=',';
end;

procedure TFormDevolFicha.pTIva1Enter(Sender: TObject);
begin
  pTIva1.SelectAll;
  vTIVA := DMDevolucion.sqlLinea.FieldByName('TIVA').AsInteger;
end;

procedure TFormDevolFicha.pTIva1Exit(Sender: TObject);
begin
  //if Dmppal.RutEdiInse(DMDevolucion.sqlLinea) = False Then Exit;
  if vTIVA <> DMDevolucion.sqlLinea.FieldByName('TIVA').AsInteger Then
  begin

    Dmppal.RutHisArtiLin_CargaIVA_A  (DMDevolucion.sqlLinea,'TIVA','TPCIVA','TPCRE', 1);
    Dmppal.ProSortidaPreus (1);   // 'PRECIOVENTA'          pPreu.SetFocus;
//a  RutCalculaLinHisto_N      (DMDevoluA.cdsHistoAnte, 'CANTIENALBA', 2, 0);
//b  RutDevolu_CalculoVendidos (DMDevoluA.cdsHistoAnte, 'COMPRAHISTO');
  //   DMDevolucion.RutDevolu_CalculoVendidos_A (DMDevolucion.sqlLinea, FormDevoluA.cbRestoVentas1.Checked);
  end;
end;

procedure TFormDevolFicha.pTIva2Enter(Sender: TObject);
begin
 vTIVA2 := DMDevolucion.sqlLineaTIVA2.AsInteger;
end;

procedure TFormDevolFicha.pTIva2Exit(Sender: TObject);
begin

  if vTIVA2 <> DMDevolucion.sqlLinea.FieldByName('TIVA2').AsInteger Then
  begin
     Dmppal.RutHisArtiLin_CargaIVA_A  (DMDevolucion.sqlLinea,'TIVA2','TPCIVA2','TPCRE2', 1);
     Dmppal.ProSortidaPreus (1);   // 'PRECIOVENTA'          pPreu.SetFocus;
//a  RutCalculaLinHisto_N      (DMDevoluA.cdsHistoAnte, 'CANTIENALBA', 2, 0);
//b  RutDevolu_CalculoVendidos (DMDevoluA.cdsHistoAnte, 'COMPRAHISTO');
     //RutDevolu_CalculoVendidos_A (DMDevoluA.cdsHistoAnte, FormDevoluA.cbRestoVentas1.Checked);
  end;
end;

procedure TFormDevolFicha.btAbajoClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Next;
end;

procedure TFormDevolFicha.btAbajoTodoClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.last;

end;

procedure TFormDevolFicha.btArribaClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Prior;
end;

procedure TFormDevolFicha.btArribaTodoClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.First;
end;

procedure TFormDevolFicha.btCerrarFichaClick(Sender: TObject);
begin
//  if FormDevolucion.swDevoAlbaran = 1 then FormDevolucion.pcDetalle.ActivePage := FormDevolucion.tabAlbaranDevolucion;
  FormDevolMenu.swDevoAlbaran := 0;

  self.Close;
end;


procedure TFormDevolFicha.btHistoArtiClick(Sender: TObject);
begin
  RutHistoArtiClick;
end;

procedure TFormDevolFicha.RutHistoArtiClick;
begin
  if (DMDevolucion.sqlLinea.RecordCount <> 0)
  then FormMenu.RutAbrirHistoArtiTablas(DMDevolucion.sqlLineaID_ARTICULO.AsInteger,
                                    DMDevolucion.sqlLineaBARRAS.AsString,
                                    DMDevolucion.sqlLineaADENDUM.AsString,
                                    DMDevolucion.sqlLineaDESCRIPCION.AsString,
                                    'devolucion');
  DMppal.swHisArti := self.Name;
end;

procedure TFormDevolFicha.btAnularLineaClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount = 0 then exit;
  FormMensajes.RutInicioForm(4, 'FORMDEVOLFICHA', 'ACEPTARCANC','Esta seguro que quiere anular la linea?');
  FormMensajes.ShowModal();

end;
procedure TFormDevolFicha.RutAnularLinea;
begin
  if (DMDevolucion.sqlCabe1.FieldByName('SWCERRADO').AsInteger = 1)
  or (DMDevolucion.sqlCabe1.FieldByName('SWDEVOLUCION').AsInteger > 2) Then
      begin
           FormMensajes.RutInicioForm(4, 'FORMDEVOLFICHA', 'ACEPTAR','No se permite anular la linea si esta la devoluci�n cerrada');
           FormMensajes.ShowModal();
      end       //'No se permite anular linea si esta cerrada')
     else  DMDevolucion.sqlLinea.Delete;
  DMDevolucion.sqlLineaTotal.Refresh;
  DMDevolucion.RutComprobarPaquete;
end;
procedure TFormDevolFicha.UniDBEdit51Enter(Sender: TObject);
begin
  UniDBEdit51.SelectAll;
end;

procedure TFormDevolFicha.UniEdit2Enter(Sender: TObject);
begin
  pBarrasA.SetFocus;
end;

procedure TFormDevolFicha.UniFormCreate(Sender: TObject);
begin
  swClicPnl := True;
  DMppal.swCrear := false;
  DMDevolucion.swRepetido := false;
  pnlTotales.Height := 0;

  with gridCabe1 do
  begin
    Columns[0].Width := 160;
    Columns[1].Width := 70;
    Columns[2].Width := 64;
    Columns[3].Width := 75;
    Columns[4].Width := 64;
  end;
end;

Function TFormDevolFicha.RutFormatearAdendum  (vAdendum:String; vTBarras:Integer):String;
var
  vAden : Integer;
begin
//  Result := '';

  if (vTBarras = 7) or
     (vTBarras = 9) or
     (vTBarras = 15) then
  begin
      vAden := StrToIntDef(vAdendum,0);
      Result := FormatFloat('00', vAden);
      exit;
  end;

  if (vTBarras = 13) then
  begin
    if vAdendum = '' then
    begin
      result := '';
    end else
    begin
      vAden := StrToIntDef(vAdendum,0);
//      Result := FormatFloat('#####', vAden);
      Result := FormatFloat('00000', vAden);
    end;
    exit;
  end;

  if (vTBarras = 18) then
  begin

      vAden := StrToIntDef(vAdendum,0);
      Result := FormatFloat('00000', vAden);
      exit;
  end;


end;

procedure TFormDevolFicha.btMasInfoClick(Sender: TObject);
var
  vHeight : integer;
begin
  vHeight := 71;
  if swClicPnl then
  begin
    pnlTotales.Height := vHeight;
    GridPreSeleccio.Height := GridPreSeleccio.Height - vHeight;
  end
  else
  begin
    pnlTotales.Height := 0;
    GridPreSeleccio.Height := GridPreSeleccio.Height + vHeight;
  end;
  swClicPnl := not swClicPnl;
end;



procedure TFormDevolFicha.btModificarDescClick(Sender: TObject);
begin
  DMModificarDesc.RutAbrirArti(DMDevolucion.sqlLineaID_ARTICULO.AsInteger);
  ProLlegirArticle(DMDevolucion.sqlLineaID_ARTICULO.AsInteger);
  FormEditarDesc.ShowModal();
end;

procedure TFormDevolFicha.BtnRestarCantiClick(Sender: TObject);
begin
  if (DMDevolucion.sqlLinea.RecordCount > 0) and (DMDevolucion.sqlLineaDEVUELTOS.AsInteger > 1) then
  begin
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaCANTIDAD.AsInteger := DMDevolucion.sqlLineaCANTIDAD.AsInteger - 1;
    DMDevolucion.sqlLineaDEVUELTOS.AsInteger := DMDevolucion.sqlLineaDEVUELTOS.AsInteger - 1;
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

    DMDevolucion.sqlLineaTotal.Refresh;
    DMDevolucion.RutComprobarPaquete;
  end;
end;

procedure TFormDevolFicha.BtnSumarCantiClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount > 0 then
  begin

    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaCANTIDAD.AsInteger := DMDevolucion.sqlLineaCANTIDAD.AsInteger + 1;
    DMDevolucion.sqlLineaDEVUELTOS.AsInteger := DMDevolucion.sqlLineaDEVUELTOS.AsInteger + 1;
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

    DMDevolucion.sqlLineaTotal.Refresh;
    DMDevolucion.RutComprobarPaquete;
  end;
end;

procedure TFormDevolFicha.btBuscarArtiClick(Sender: TObject);
begin
  DMppal.swConsArti := self.Name;
  FormMenu.RutAbrirConsArti;
  FormConsArti.RutInicioShow;
  FormConsArti.vIdEditorial := 0;
end;


end.
