unit uMenuArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormMenuArti = class(TUniForm)
    tmSession: TUniTimer;
    pcDetalle: TUniPageControl;
    tabListaArti: TUniTabSheet;
    tabFichaArti: TUniTabSheet;
    tabConsArti: TUniTabSheet;
    UniPanel1: TUniPanel;
    UniButton1: TUniButton;
    UniButton2: TUniButton;
    UniButton3: TUniButton;



  private


    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swFicha, swLista, swConsulta : Integer;
    procedure RutAbrirDevolArti;
    procedure RutAbrirConsArti;
    procedure RutAbrirFichaArti(vOrigen : String);
    procedure RutAbrirListaArti;
  end;

function FormMenuArti: TFormMenuArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uListaArti, uFichaArti, uConsArti;

function FormMenuArti: TFormMenuArti;
begin
  Result := TFormMenuArti(DMppal.GetFormInstance(TFormMenuArti));

end;


procedure TFormMenuArti.RutAbrirListaArti;
begin
  pcDetalle.ActivePage := tabListaArti;

  if swLista = 0 then
  begin
    FormListaArti.Parent := tabListaArti;
    FormListaArti.Align := alClient;
    FormListaArti.Show();
    swLista := 1;

    DMMenuArti.RutAbrirListaArticulos;
  end;


  DMPpal.RutCapturaGrid(FormListaArti.Name, FormListaArti.GridListaArti, nil, 0, 0, 0);

end;
procedure TFormMenuArti.RutAbrirDevolArti;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;


  FormMenuArti.Parent := FormMenu.tabArticulos;
  FormMenuArti.Align  := alClient;
  FormMenuArti.Show();

  FormMenuArti.pcDetalle.ActivePage := FormMenuArti.tabFichaArti;


  if FormMenu.swFicha = 0 then
  begin
    FormManteArti.Parent := FormMenuArti.tabFichaArti;
    FormManteArti.Align := alClient;
    FormManteArti.Show();

    FormMenu.swFicha := 1;

  end;

  FormManteArti.RutShow;

  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;




end;
procedure TFormMenuArti.RutAbrirFichaArti(vOrigen : String);
begin


  if swFicha = 0 then
  begin
    FormManteArti.Parent := tabFichaArti;
    FormManteArti.Align := alClient;
    FormManteArti.Show();
    DMppal.RutInicioForm;
    swFicha := 1;


  end;

  if pcDetalle.ActivePage = tabListaArti     then DMMenuArti.RutAbrirFichaArticulos(DMMenuArti.sqlListaArticuloID_ARTICULO.AsString)
  else if pcDetalle.ActivePage = tabConsArti then DMMenuArti.RutAbrirFichaArticulos(DMMenuArti.sqlArtiLinkID_ARTICULO.AsString);

   pcDetalle.ActivePage := tabFichaArti;

  //if swFicha = 1 then FormManteArti.vStringOrigen := vOrigen; //si no se contorla con el if se abrira sin control todo el formulario

  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;

end;

procedure TFormMenuArti.RutAbrirConsArti;
begin
  FormMenu.RutAbrirConsArti;
  FormConsArti.RutInicioShow;
  FormConsArti.vIdEditorial := 0;

  {pcDetalle.ActivePage := tabConsArti;

  if swConsulta = 0 then
  begin
    FormConsArti.Parent := tabConsArti;
    FormConsArti.Align := alClient;
    FormConsArti.Show();
    swConsulta := 1;
    //DMppal.RutInicioForm;

  end;
 // DMMenuArti.RutAbrirConsArti;
 }
end;



end.

