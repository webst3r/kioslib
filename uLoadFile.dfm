object FormLoadFile: TFormLoadFile
  Left = 0
  Top = 0
  ClientHeight = 100
  ClientWidth = 300
  Caption = 'FormLoadFile'
  BorderStyle = bsNone
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnlDropbox: TUniPanel
    Left = 0
    Top = 0
    Width = 300
    Height = 100
    Hint = ''
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    BorderStyle = ubsNone
    Caption = ''
    Color = 16755200
    object mmoFiles: TUniMemo
      AlignWithMargins = True
      Left = 8
      Top = 8
      Width = 284
      Height = 84
      Hint = ''
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Margins.Bottom = 8
      Visible = False
      BorderStyle = ubsNone
      Align = alClient
      Anchors = [akLeft, akTop, akRight, akBottom]
      ReadOnly = True
      Color = 16755200
      TabOrder = 1
      WordWrap = False
    end
    object hfrDropbox: TUniHTMLFrame
      Left = 83
      Top = 50
      Width = 125
      Height = 97
      Hint = ''
      HTML.Strings = (
        '<script type="text/javascript">'
        ''
        
          '// set vars (RUN = DoInit, ajaxtarget = Target for ajaxRequests,' +
          ' dropbox = dropbox Element for events): '
        'var RUN = false;'
        '//!!! var ajaxtarget = fraMultiUpload.hfrDropbox; '
        
          '//!!! var dropbox = document.getElementById(fraMultiUpload.pnlDr' +
          'opbox.id);'
        ''
        ''
        'function dragEnter(evt) {'
        '  evt.stopPropagation();'
        '  evt.preventDefault();'
        '}'
        ''
        'function dragExit(evt) {'
        '  evt.stopPropagation();'
        '  evt.preventDefault();'
        '}'
        ''
        'function dragOver(evt) {'
        '  evt.stopPropagation();'
        '  evt.preventDefault();'
        '}'
        ''
        'function drop(evt) {'
        '  evt.stopPropagation();'
        '  evt.preventDefault();'
        '  // get Files and Count:'
        '  var files = evt.dataTransfer.files;'
        '  var count = files.length;'
        '  // Only call the handler if 1 or more files was dropped:'
        '  if (count > 0)'
        '    handleFiles(files);'
        '}'
        ''
        'function initEventHandlers() {'
        '  // set EventHandlers for dropbox for drag and drop:'
        '  dropbox.addEventListener("dragenter", dragEnter, false);'
        '  dropbox.addEventListener("dragexit",  dragExit,  false);'
        '  dropbox.addEventListener("dragover",  dragOver,  false);'
        '  dropbox.addEventListener("drop",      drop,      false);'
        '}'
        ''
        ''
        'function handleFiles(files) {'
        ' '
        '  // for each file to upload: '
        '  for (var i = 0, f; f = files[i]; i++) {'
        ''
        '     // init FileReader for this file:'
        '     var reader = new FileReader();'
        ''
        '     // notify the upload start:'
        
          '     ajaxRequest(ajaxtarget, "uploadingfile", ["filename="+f.nam' +
          'e, "filesize="+f.size, "filetype="+f.type]);     '
        '     '
        '     // set event handler "reader.onprogress":'
        '     reader.onprogress = (function(f) {'
        '     '#9#9'return function(e) {'
        '     '#9#9#9' // notify file upload progress (e is a Progressevent):'
        
          '     '#9#9#9' var percentLoaded = Math.round((e.loaded / e.total) * 1' +
          '00);'
        
          '      '#9#9' ajaxRequest(ajaxtarget, "uploadingprogress", ["filename' +
          '="+f.name, "filesize="+f.size, "filetype="+f.type, "loaded="+e.l' +
          'oaded, "total="+e.total, "percent="+percentLoaded]);     '#9#9
        '     '#9#9'}'
        '     })(f);'
        ''
        '     // set event handler "reader.onload":'
        '     reader.onload = (function(f) { '
        '        return function(e) {'
        
          '           // send to server e.target.result , filename, filesiz' +
          'e and filetype: '
        
          '           ajaxRequest(ajaxtarget, "uploadedfile", ["filename="+' +
          'f.name, "filesize="+f.size, "filetype="+f.type, "content="+e.tar' +
          'get.result]);'
        '        };'
        '     })(f);'
        '    '
        '     //start read operation for this file:'
        '     reader.readAsDataURL(f);'
        '     '
        '  }'
        '  '
        '}'
        ''
        ''
        'if (RUN == true) {'
        '  initEventHandlers();'
        '};'
        ''
        '</script>')
      OnAjaxEvent = hfrDropboxAjaxEvent
    end
    object lblDropbox: TUniLabel
      AlignWithMargins = True
      Left = 8
      Top = 8
      Width = 195
      Height = 20
      Hint = ''
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Margins.Bottom = 8
      TextConversion = txtHTML
      AutoSize = False
      Caption = 'Arrastrar ficheros zona azul'
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsBold]
      TabOrder = 3
    end
    object btSalirUpload: TUniBitBtn
      Left = 259
      Top = 47
      Width = 82
      Height = 36
      Hint = ''
      Visible = False
      Caption = 'Salir'
      TabStop = False
      TabOrder = 4
      ImageIndex = 0
    end
    object btBorrarFile: TUniBitBtn
      Left = 214
      Top = 3
      Width = 82
      Height = 38
      Hint = ''
      Glyph.Data = {
        16020000424D160200000000000076000000280000001A0000001A0000000100
        040000000000A001000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777788888888
        8888888887777700000077791111111111111111187777000000777911111111
        1111111118777700000077791111111111111111187777000000777911111111
        1111111118777700000077779999999999999999977777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000}
      Caption = 'Fichero'
      TabStop = False
      TabOrder = 5
      Default = True
      ImageIndex = 0
      OnClick = btBorrarFileClick
    end
  end
  object tmrDone: TUniTimer
    Interval = 1750
    Enabled = False
    RunOnce = True
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      '   '
      '}')
    OnTimer = tmrDoneTimer
    Left = 24
    Top = 56
  end
  object tmrUpdateFrameName: TUniTimer
    Interval = 750
    RunOnce = True
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      '   '
      '}')
    OnTimer = tmrUpdateFrameNameTimer
    Left = 16
    Top = 40
  end
end
