unit uDevolucionLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, Vcl.StdCtrls, Vcl.Buttons,
  uniDBText;

type
  TFormDevolucionLista = class(TUniForm)
    UniPanel1: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel1: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    pcControl: TUniPageControl;
    tabGrid: TUniTabSheet;
    wwDBGrid1: TUniDBGrid;
    tabOpciones: TUniTabSheet;
    UniContainerPanel1: TUniContainerPanel;
    BtnBuscaDistribui: TUniBitBtn;
    pDistriCodi: TUniDBEdit;
    UniLabel2: TUniLabel;
    pDistriNom: TUniDBEdit;
    btcambiardistribuidora: TUniBitBtn;
    BitBtn3: TUniBitBtn;
    PageControl3: TUniPageControl;
    TabSheet8: TUniTabSheet;
    TabSheet9: TUniTabSheet;
    TabSheet15: TUniTabSheet;
    TabSheet16: TUniTabSheet;
    UniContainerPanel2: TUniContainerPanel;
    UniLabel3: TUniLabel;
    wwDBEdit2: TUniDBEdit;
    wwDBEdit12: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    pDistriData: TUniDBDateTimePicker;
    DBText2: TUniDBText;
    Fecha: TUniLabel;
    UniLabel14: TUniLabel;
    wwDBGrid3: TUniDBGrid;
    UniPanel2: TUniPanel;
    UniLabel15: TUniLabel;
    BtnDevoluAfegir: TUniBitBtn;
    BtnDevoluNova: TUniBitBtn;
    plRecalcularPendiente: TUniPanel;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    edNuevaIDDevolucion: TUniDBEdit;
    UniLabel18: TUniLabel;
    edContraCambiarID: TUniDBEdit;
    DBText10: TUniDBText;
    btCambiarIdDevolucion: TUniBitBtn;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    btNuevaDistribuidora: TUniBitBtn;
    wwDBEdit46: TUniDBEdit;
    wwDBEdit45: TUniDBEdit;
    UniLabel19: TUniLabel;
    BitBtn4: TUniBitBtn;
    BitBtn5: TUniBitBtn;
    LabelProcesoDistri: TUniLabel;
    UniPanel4: TUniPanel;
    PnlSeleDevolE: TUniPanel;
    UniLabel6: TUniLabel;
    wwDBEdit5: TUniDBEdit;
    UniLabel7: TUniLabel;
    DBText1: TUniDBText;
    wwDBDateTimePicker2: TUniDBDateTimePicker;
    PnlSeleDevolC: TUniPanel;
    RgFechaAviDevoC: TUniDBRadioGroup;
    UniGroupBox1: TUniGroupBox;
    UniLabel8: TUniLabel;
    pFechaDevolu: TUniDBDateTimePicker;
    UniGroupBox2: TUniGroupBox;
    BtnBuscaDocuNou: TUniBitBtn;
    UniLabel9: TUniLabel;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    wwDBEdit14: TUniDBEdit;
    UniLabel10: TUniLabel;
    cbRestoVentas: TUniCheckBox;
    UniPanel3: TUniPanel;
    UniLabel11: TUniLabel;
    DBMemo1: TUniDBMemo;
    UniLabel12: TUniLabel;
    pFechaCargoCrea: TUniDBDateTimePicker;
    UniLabel13: TUniLabel;
    pFechaAbonoCrea: TUniDBDateTimePicker;
    BtnGrabarCabe: TUniBitBtn;
    BtnCancelarCabe: TUniBitBtn;
    BtnReactivar: TUniBitBtn;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormDevolucionLista: TFormDevolucionLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDevolucion;

function FormDevolucionLista: TFormDevolucionLista;
begin
  Result := TFormDevolucionLista(DMppal.GetFormInstance(TFormDevolucionLista));

end;


end.
