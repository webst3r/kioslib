object DMUltiCompra: TDMUltiCompra
  OldCreateOrder = False
  Height = 653
  Width = 978
  object sqlUltiCompra: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select First 5 H.ID_ARTICULO, H.ADENDUM, H.FECHA'
      ', A.BARRAS'
      ', sum(H.entradas) as Entradas'
      ''
      ', (Select SUM(Coalesce(H1.Salidas,0)) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      
        '   and H1.adendum = H.Adendum and ((H1.clave = '#39'51'#39') or (H1.clav' +
        'e = '#39'53'#39'))'
      '   and H1.Fecha >= H.FECHA) as Ventas'
      ''
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum and H1.clave = '#39'54'#39
      '   and H1.Fecha >= H.FECHA) as Devoluciones'
      ''
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum and H1.clave = '#39'55'#39
      '   and H1.Fecha >= H.FECHA) as Merma'
      ''
      ', (Select SUM(H1.Entradas - H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum'
      '   and H1.Fecha >= H.FECHA) as Stock'
      ''
      'from FVHISARTI H'
      'left join FMARTICULO A on(A.ID_ARTICULO = H.ID_ARTICULO)'
      ''
      'WHERE H.ID_ARTICULO = 0'
      'and H.clave = '#39'01'#39
      ''
      'group by 1,2,3,4'
      'order by 1,3 desc,2')
    Left = 56
    Top = 64
    object sqlUltiCompraID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlUltiCompraADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlUltiCompraFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlUltiCompraENTRADAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraVENTAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAS'
      Origin = 'VENTAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraDEVOLUCIONES: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLUCIONES'
      Origin = 'DEVOLUCIONES'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraMERMA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMA'
      Origin = 'MERMA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraSTOCK: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'STOCK'
      Origin = 'STOCK'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
  end
end
