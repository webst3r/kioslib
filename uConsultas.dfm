object FormConsultas: TFormConsultas
  Left = 0
  Top = 0
  ClientHeight = 726
  ClientWidth = 1177
  Caption = 'FormConsultas'
  OnShow = UniFormShow
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pcConsultas: TUniPageControl
    Left = 0
    Top = 55
    Width = 1177
    Height = 671
    Hint = ''
    ActivePage = tabTiemposTrabajos
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tabTiemposTrabajos: TUniTabSheet
      Hint = ''
      Caption = 'Tiempos por Trabajos'
      object UniDBNavigator2: TUniDBNavigator
        Left = 909
        Top = 6
        Width = 249
        Height = 25
        Hint = ''
        DataSource = dsSumTiempo
        TabOrder = 0
      end
      object gridSumTiempo: TUniDBGrid
        Left = 3
        Top = 37
        Width = 1155
        Height = 598
        Hint = ''
        DataSource = dsSumTiempo
        WebOptions.Paged = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        TabOrder = 1
        Columns = <
          item
            FieldName = 'NOMBRECLIENTE'
            Title.Caption = 'Cliente'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'ID'
            Title.Caption = 'Id'
            Width = 35
          end
          item
            FieldName = 'DESCRIPCION_TRABAJO'
            Title.Caption = 'Descripcion'
            Width = 266
          end
          item
            FieldName = 'PERSONA'
            Title.Caption = 'Usuario'
            Width = 58
            Sortable = True
          end
          item
            FieldName = 'FECHAINICIO'
            Title.Caption = 'Inicio'
            Width = 71
            Sortable = True
          end
          item
            FieldName = 'FECHAFIN'
            Title.Caption = 'Fin'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'TOTAL_TIEMPO'
            Title.Caption = 'Total'
            Width = 108
            Sortable = True
          end>
      end
    end
    object tabTiemposClientes: TUniTabSheet
      Hint = ''
      Caption = 'Tiempos por Clientes'
      object UniDBNavigator3: TUniDBNavigator
        Left = 909
        Top = 6
        Width = 249
        Height = 25
        Hint = ''
        DataSource = dsTiempoCliente
        TabOrder = 0
      end
      object gridTiempoCliente: TUniDBGrid
        Left = 3
        Top = 37
        Width = 1155
        Height = 598
        Hint = ''
        DataSource = dsTiempoCliente
        WebOptions.Paged = False
        LoadMask.Message = 'Loading data...'
        TabOrder = 1
        Columns = <
          item
            FieldName = 'NOMBRECLIENTE'
            Title.Caption = 'Cliente'
            Width = 64
            ReadOnly = True
            Sortable = True
          end
          item
            FieldName = 'PERSONA'
            Title.Caption = 'Usuario'
            Width = 244
            ReadOnly = True
            Sortable = True
          end
          item
            FieldName = 'HORAS_CAMPO'
            Title.Caption = 'Horas'
            Width = 118
            ReadOnly = True
            Sortable = True
          end>
      end
    end
    object tabTodosTiempos: TUniTabSheet
      Hint = ''
      Caption = 'Todos los Tiempos'
      object UniDBNavigator4: TUniDBNavigator
        Left = 909
        Top = 6
        Width = 249
        Height = 25
        Hint = ''
        DataSource = dsTodosTiempos
        TabOrder = 0
      end
      object gridTodosTiempos: TUniDBGrid
        Left = 3
        Top = 37
        Width = 1155
        Height = 598
        Hint = ''
        DataSource = dsTodosTiempos
        WebOptions.Paged = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        TabOrder = 1
        Columns = <
          item
            FieldName = 'ID'
            Title.Caption = 'ID'
            Width = 64
            Visible = False
          end
          item
            FieldName = 'ID_TRABAJO'
            Title.Caption = 'Trabajo'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'ID_PERSONA'
            Title.Caption = 'Persona'
            Width = 70
            Sortable = True
          end
          item
            FieldName = 'NOMBRECLIENTE'
            Title.Caption = 'Cliente'
            Width = 121
            ReadOnly = True
            Sortable = True
          end
          item
            FieldName = 'DESCRIPCIONTRABAJO'
            Title.Caption = 'Descripcion Trabajo'
            Width = 98
            ReadOnly = True
            DisplayMemo = True
          end
          item
            FieldName = 'DESCRIPCION_HTML'
            Title.Caption = 'Descripcion Tarea'
            Width = 89
            DisplayMemo = True
          end
          item
            FieldName = 'FECHA'
            Title.Caption = 'Fecha'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'PARADA'
            Title.Caption = 'Parada'
            Width = 64
            Visible = False
            Sortable = True
          end
          item
            FieldName = 'HORA_INICIO'
            Title.Caption = 'Inicio'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'HORA_FIN'
            Title.Caption = 'Fin'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'HORAS_TEMPORAL'
            Title.Caption = 'Tiempo Total'
            Width = 118
            ReadOnly = True
            Sortable = True
          end>
      end
    end
  end
  object UniPanel2: TUniPanel
    Left = 0
    Top = 0
    Width = 1177
    Height = 55
    Hint = ''
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Caption = ''
    object tmDesdeTiempoTrabajos: TUniDateTimePicker
      Left = 65
      Top = 10
      Width = 88
      Hint = ''
      DateTime = 43339.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm'
      TabOrder = 1
    end
    object tmHastaTiempoTrabajos: TUniDateTimePicker
      Left = 199
      Top = 10
      Width = 88
      Hint = ''
      DateTime = 43339.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm'
      TabOrder = 2
    end
    object UniLabel16: TUniLabel
      Left = 29
      Top = 14
      Width = 30
      Height = 13
      Hint = ''
      Caption = 'Desde'
      TabOrder = 3
    end
    object UniLabel17: TUniLabel
      Left = 166
      Top = 14
      Width = 28
      Height = 13
      Hint = ''
      Caption = 'Hasta'
      TabOrder = 4
    end
    object btFiltrarTiemposTrabajos: TUniButton
      Left = 295
      Top = 10
      Width = 75
      Height = 22
      Hint = ''
      Caption = 'Filtrar'
      TabOrder = 5
      OnClick = btFiltrarTiemposTrabajosClick
    end
    object cbTodasFechas: TUniCheckBox
      Left = 65
      Top = 33
      Width = 117
      Height = 17
      Hint = ''
      Caption = 'Todas las Fechas'
      TabOrder = 6
      OnClick = cbTodasFechasClick
    end
  end
  object dsTrabajos: TDataSource
    Left = 8
    Top = 760
  end
  object dsTiempo: TDataSource
    Left = 72
    Top = 760
  end
  object dsEmpresa: TDataSource
    Left = 8
    Top = 816
  end
  object dsSumTiempo: TDataSource
    Left = 128
    Top = 760
  end
  object dsTiempoCliente: TDataSource
    Left = 72
    Top = 808
  end
  object dsTodosTiempos: TDataSource
    Left = 120
    Top = 808
  end
  object dsEmpresa2: TDataSource
    Left = 8
    Top = 708
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object DataSource1: TDataSource
    DataSet = DMppal.sqlUpdate
    Left = 176
    Top = 752
  end
end
