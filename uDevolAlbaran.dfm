object FormDevolucionAlbaran: TFormDevolucionAlbaran
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 687
  ClientWidth = 997
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBtsLista: TUniPanel
    Left = 0
    Top = 0
    Width = 997
    Height = 50
    Hint = ''
    Visible = False
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    BorderStyle = ubsSingle
    Caption = ''
    Color = clBtnShadow
    object BtnDevolEnCurso: TUniBitBtn
      Left = 244
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888800000088888008888888888800000088880FF000000000080000008880
        F0080FFFFFFF08000000880F0FF00F00000F0800000080F0F0080FFFFFFF0800
        0000880F0FF00F00000F0800000080F0F0080FFFFFFF08000000880F0FB00F00
        F0000800000080F0FBFB0FFFF0F088000000880FBFBF0FFFF0088800000080FB
        FBFB00000088880000008800BFBFBFBF088888000000888800FBFBF088888800
        000088888800B808888888000000888888880088888888000000888888888888
        888888000000888888888888888888000000}
      Caption = ''
      TabOrder = 1
      ImageIndex = 13
    end
    object BtnDevolEnviadas: TUniBitBtn
      Left = 305
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 2
    end
    object spTodas: TUniBitBtn
      Left = 366
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77770000000000000007FFFFFFFFFFFFFFF00000F000F00F000F70910BB30E60
        330070910BB30E60330070910BB30E60330070910BB30E60330070910BB30E60
        330070910BB30E603300709100000E603300709100FF0E6000007000010F0E60
        0F00700F0100000000007700F00700FF00777770000770000077}
      Caption = ''
      TabOrder = 3
    end
    object BtnDocumentoCabe: TUniBitBtn
      Left = 427
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888000000080000000000000008000000080F8FFFFFFFFFFF0800000008089
        9FFF899998F08000000080F98FFFF8888FF08000000080FFFFFFFFFFFFF08000
        000080F7F447844447F08000000080F8F888F88888F08000000080F8F7788777
        78F08000000080F7F747847747F08000000080FFFFFFF8888FF0800000008080
        0007FFFFFFF08000000080F8FF8FFFFFFFF08000000080866666F88888808000
        000080E767EEEEEEEE708000000080E8E7EEEEEEEE8080000000800000000000
        000080000000}
      Caption = ''
      TabOrder = 4
    end
    object UniLabel85: TUniLabel
      Left = 504
      Top = 310
      Width = 54
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Documento'
      TabOrder = 5
    end
    object edDocumentoProve: TUniDBEdit
      Left = 496
      Top = 323
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 6
    end
    object spVerDocumentoProveedor: TUniBitBtn
      Left = 657
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000F0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0087FFFFFFFFFFFF0B3087FFFFFFFFFFF0BB0087FF
        FFFFFFFF0BB3008FFFFFFFFFF0BBB008FFFFFFFFF00BBB007FFFFFFF00BBB007
        FFF0FFFFF00BBB007FF0FFFFFFF00BB007F0FFFFFFFFF00B0070FFFFFFFFFFF0
        00F0FFFFFFFFFFFFFFF0}
      Caption = ''
      TabOrder = 7
      ImageIndex = 13
    end
    object btEtiquetasDocumento: TUniBitBtn
      Left = 718
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42050000424D4205000000000000360000002800000016000000130000000100
        1800000000000C05000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FFFFFF000000000000000000000000FFFFFFC0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C00000000000000000000000000000000000000000000000C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C0000000C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0008000008000C0C0
        C00000FF0000FFC0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C00000
        00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0000000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFC0C0C0C0
        C0C0808080000000000000808080808080808080808080808080808080808080
        8080808080808080808080808080800000000000008080800000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000FFFFFFFFFFFF00008080
        80000000000000808080808080808080000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000FFFFFF000000
        000000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000
        0000FFFFFF000000000000000000000000000000000000000000000000FFFFFF
        000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000000000FFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF000000FFFFFF0000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000
        000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000}
      Caption = ''
      TabOrder = 8
    end
    object btInicioConfiguracion: TUniBitBtn
      Left = 786
      Top = 318
      Width = 34
      Height = 27
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000013000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        3333333000003243342222224433333000003224422222222243333000003222
        222AAAAA222433300000322222A33333A222433000003222223333333A224330
        00003222222333333A44433000003AAAAAAA3333333333300000333333333333
        3333333000003333333333334444443000003A444333333A2222243000003A22
        43333333A2222430000033A22433333442222430000033A22244444222222430
        0000333A2222222222AA243000003333AA222222AA33A3300000333333AAAAAA
        333333300000333333333333333333300000}
      Caption = ''
      TabOrder = 9
    end
    object BtnPreProve: TUniBitBtn
      Left = 838
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000007777777777777777770000007777777777770007770000007444
        4400000006007700000074FFFF08880600080700000074F008000060EE070700
        000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
        00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
        080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
        000074F444F444F444F477000000744444444444444477000000777777777777
        777777000000777777777777777777000000}
      Caption = ''
      TabOrder = 10
    end
    object UniDateTimePicker1: TUniDateTimePicker
      Left = 103
      Top = 310
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 11
    end
    object UniDateTimePicker2: TUniDateTimePicker
      Left = 103
      Top = 331
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 12
    end
    object UniLabel86: TUniLabel
      Left = 103
      Top = 296
      Width = 43
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'En Curso'
      TabOrder = 13
    end
    object UniLabel87: TUniLabel
      Left = 68
      Top = 310
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 14
    end
    object UniLabel88: TUniLabel
      Left = 68
      Top = 331
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 15
    end
    object UniPanel24: TUniPanel
      Left = 56
      Top = 2
      Width = 522
      Height = 45
      Hint = ''
      ShowHint = True
      ParentShowHint = False
      TabOrder = 16
      BorderStyle = ubsNone
      ShowCaption = False
      Caption = 'Programa'
      Color = clBtnShadow
      object UniBitBtn24: TUniBitBtn
        Left = 6
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Nuevo'
        ShowHint = True
        Caption = ''
        TabOrder = 1
        IconAlign = iaTop
        Images = ImgNativeList
        ImageIndex = 0
      end
      object UniBitBtn29: TUniBitBtn
        Left = 69
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Anular'
        ShowHint = True
        Caption = ''
        TabOrder = 2
        IconAlign = iaTop
        Images = ImgNativeList
        ImageIndex = 7
      end
      object btModificar: TUniBitBtn
        Left = 132
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Modificar'
        ShowHint = True
        Caption = ''
        TabOrder = 3
        IconAlign = iaTop
        Images = ImgNativeList
        ImageIndex = 4
      end
      object UniBitBtn31: TUniBitBtn
        Left = 194
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Cerrar'
        ShowHint = True
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        Images = ImgNativeList
        ImageIndex = 9
      end
    end
    object UniPanel26: TUniPanel
      Left = 588
      Top = 2
      Width = 357
      Height = 45
      Hint = ''
      ShowHint = True
      ParentShowHint = False
      TabOrder = 17
      BorderStyle = ubsNone
      ShowCaption = False
      Caption = 'Opcional'
      Color = clBtnShadow
      object UniPanel27: TUniPanel
        Left = 3
        Top = 0
        Width = 231
        Height = 45
        Hint = ''
        Visible = False
        ShowHint = True
        TabOrder = 1
        BorderStyle = ubsNone
        ShowCaption = False
        Caption = 'Ficha'
        Color = clActiveBorder
        object UniBitBtn17: TUniBitBtn
          Left = 3
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Consulta Devoluciones'
          ShowHint = True
          Caption = ''
          TabOrder = 1
          IconAlign = iaTop
          Images = ImgNativeList
          ImageIndex = 12
        end
        object UniBitBtn22: TUniBitBtn
          Left = 65
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Nueva Devolucion'
          ShowHint = True
          Caption = ''
          TabOrder = 2
          IconAlign = iaTop
          Images = ImgNativeList
          ImageIndex = 2
        end
        object UniBitBtn23: TUniBitBtn
          Left = 121
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Buscar'
          ShowHint = True
          Caption = ''
          TabOrder = 3
          IconAlign = iaTop
          Images = ImgNativeList
          ImageIndex = 1
        end
        object UniBitBtn32: TUniBitBtn
          Left = 177
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Imprimir'
          ShowHint = True
          Caption = ''
          TabOrder = 4
          IconAlign = iaTop
          Images = ImgNativeList
          ImageIndex = 8
        end
      end
      object btOpciones: TUniBitBtn
        Left = 322
        Top = 5
        Width = 35
        Height = 35
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        Caption = ''
        TabOrder = 2
        Transparency = toFuchsia
        Images = ImgNativeList
        ImageIndex = 11
        OnClick = btOpcionesClick
      end
    end
    object btLista: TUniBitBtn
      AlignWithMargins = True
      Left = 3
      Top = 2
      Width = 50
      Height = 45
      Hint = ''
      Margins.Bottom = 0
      ShowHint = True
      Caption = 'Lista'
      TabOrder = 18
      IconAlign = iaTop
      Images = ImgNativeList
      ImageIndex = 4
      OnClick = btListaClick
    end
    object btCerrarFicha: TUniBitBtn
      Left = 952
      Top = 7
      Width = 35
      Height = 35
      Hint = 'Salir'
      ShowHint = True
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C21E0000C21E00000000000000000000D9E5F1C3D2E3
        0E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10
        120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E
        1012B9BEEC7075C47277C77175C66F73C47074C66F73C66F72C76F72C76F71C8
        6F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71
        C97072C78A7DA30E1012CDC9E57975BA7473C36873C55C7CB65A7EB8567AB95B
        75CC5E75CC5F76C1607AB6586FD25973C85B75C35F75C3636FCE656FCB6973C7
        6D7BBC6F7AC5696CCE6B66DF6F70C50E1012C6CEE37378B66F74C36671C95D78
        BD5977BA5F7CC15E70C56A7AC4697BB36F84A6687AB8697EAE6A80A96D80AA6F
        79B8727DB96E7BAE6D81A07182AD6E78BA6B6DC96E70C80E1012BDD2E16C7AB4
        6974C4646ECE6074C46276C3687AC66A72C6BEC6EDDAE4F4DEECF1E0E8F4E0ED
        F2E0EFF1E1EEF2E0E7F4DFE9F5DBE8F3DBF1EFDAEEF2CEDDF59299D26E70C80E
        1012B4D6E0647CB36374C5616DD36270CA6670C76B71C5706EC0D3D1EFF3F4FB
        F9FEF7FFF9FFFEFDF8FDFEF6FBFEF9F8F8FEF6FAFFF1FBFCEDFEEEE9FBF3EDFA
        FC929BB96E70C80E1012B0D3E2617BB66275C46270CB6475C16675BE6D77C16E
        70C2BCBFEADBE2F1DEEAEFE5DFF3E5E3F1E4E5F0E2E4F1E1DDF4E1E0F3DEE2F3
        ECFAF7F0FCFBF2F8FE9795B96E70C80E1012B4D0E6647AB96475C16573C26378
        B0798DC39CAEE27079C76E77C26B78B56B7EA87276BE727AB56F79AF707AB272
        74BF7578B98086B6CFDCE4F0F9FEF1F3FD9F99C06E70C80E1012B8CCE66776B9
        6874BD6D7ABB7890B1A9C4DBC6E1F56879C35F6FC56376C85D76BD6171CB6176
        C3667CC46276C1626DC86B74C47781BECBDAE8F1FAFEF4F4FEA198C06E70C80E
        1012BBC7E46B72B4797EBF747EADB7CDD4E4FDFECFEAF4687BB46074C35F76C8
        5A76C25F7DBE5578AE5C81B3597CB4637BC76479BC7186B6C8E0E3F0FEFEF4F9
        FCA29EB76E70C80E1012BBCBE56E77AF7A7FB8B1B3D4ECF6F9EFFBF8EAF4F8C3
        C9E9C8D1F1C1CFEEBFD3EBC6D5ECC3D7E7C1D9E8BFD6EAA8B7E57180B87687B2
        CBDFE3F1FEFEF4F9FDA29DB96E70C80E1012B9CFDF7481B0BABFEBF3EFFFF6F6
        F9F5F6F4FBFCFAF9F6FEF5F7FDEFF5F8F0FBF4F4FAFAF4FCF7F1FCFAEEF8FFCB
        CFEC757AB07D86B3CDDCE6F1FBFEF3F5FEA199BF6E70C80E1012B6D6DE6F82A7
        B8BDE5F4EDFFFCF6FCFFFBF9FFFDF8FFF7FEFFFCF9FFFEEDFFFEDCFFFEF0FFFB
        EAFEFCF3FEF9FFDDD2ED827AB48682B8D1D8EAF2F6FFF3EFFF9F93C86E70C80E
        1012B8D2E56C7AB3767CBCB8B7E5EBF2FBF0FBFBEBF8F6C5D6E2B9D0E5B7D5E4
        B1D7E5C0CEE7C1D2E6C0D1E7C0CEE9AAAEE67576BD8586BED3D8E8F9F8FFFBF3
        FFAA97C16E72C50E1012B8CCE6717CBD6F73BF7D7BC4BEC5E7E7F1FBD8E5EF6D
        78AB6876B56477B4647EB36078B55F7BAD6481B05B76AC697BC76676BA7989BC
        C7DEE7EDFCFFF0F8FF9C9BBB6E72C50E1012BCCEE46D76BB7073C66B6AC18088
        C1B3BEE6D1DDF4737AC36F75C96E76C66572B8607BC55877B55F80B95978B65E
        75C56076BC6F86B9BAD9E1E3FCFEE9FBFF97A0BC6E72C50E1012BDD2E26C79B5
        6B73C46B72CF6777BD7082C2A6B7EB656BC47071CD7070C67476C05E68CD6272
        CA6072C46474C76872D26872C77885C6C3DAE9E9FBFFEDF8FF9599BD6E72C50E
        1012BFD8DE6E80AC6979BC6073C65D7BBA5D7EB45F7EB36475B7868CC88F93C2
        8A8EAE9087C79590C58E8BBB918BBD9386C8978AC49C92BFD7D7DEF9F7FAFCF7
        FEAA9DBD6E72C50E1012C1D5DD6D7CAE6D7BC45B6EC65979BC5B7EB75A7CAF6B
        7FBAC9D3F1E8F0FFF2F9FEF6F4FEF3F3FAF5F5F9F9F6FCF8EDFEFAEFFFF7EFFD
        FDFDF6FCFDF6FFF8FAA89CB76E72C50E1012C1CEDF6E77B56D76CB6170D45E7B
        C6597BB95D7EB3677BB5C9D6EFECF7FFEDF8FAF4FEFDF4FEF6F5FEF4F7FEF9F9
        F8FEF9F8FFF9F9FBF7FDEEFAFDF3F7F7F99F9BB76C70C30E1012C6CAE87372CA
        6865D46161DD6170D15E72C56278C16874C2A7ADDDB7BEDABBC4D4AAC9D6ABCC
        D3ACCDD3AFCAD5B1C0DDB0BEDDADBDDCAEC8D8ABC4D8A8BBDF7B87CD7175C80E
        1012BDC6E26F70BA7470D06B68D6666EC46673BE6B7AC06C74C37379C26C75B0
        7684AE637ABA657DB2677EB0697BB46B73C46B73C46974BF6B82B46279B36679
        C76773D76E72C50E1012C3D4DC747CAE6F71B97372C96D75B7717DB7717FB76D
        76BE6F78BF6B78B46B7DAA6A77BE6C7BB56E7CB1717AB47273C37476C36B72B6
        697DA5697FAE687ABC5F6BC46F73C6C4D0E7D6EEE7C0D2D9C2CCE1C4C8E7C3CE
        E3C1D0E2BFD0E1BCC8E5BAC6E7B9CAE5B6CEE1C5C7E6C7CCE3CACEE1CCCCE2CD
        C6E6CDC8E6C9CBE2C7D8DCC2D6DFC1D2E2BCC8E4B7BBEED7E3F4}
      Caption = ''
      TabOrder = 19
      IconAlign = iaTop
      OnClick = btCerrarFichaClick
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 50
    Width = 997
    Height = 110
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    BorderStyle = ubsNone
    Caption = ''
    object pnlNavigator: TUniPanel
      Left = 0
      Top = 0
      Width = 36
      Height = 110
      Hint = ''
      Visible = False
      ShowHint = True
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 1
      Caption = ''
      object btNuevo: TUniBitBtn
        Left = 0
        Top = -1
        Width = 37
        Height = 29
        Hint = ''
        ShowHint = True
        Glyph.Data = {
          16020000424D160200000000000076000000280000001A0000001A0000000100
          040000000000A001000000000000000000001000000000000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          77777777777777000000777777777778888877777777770000007777777777FA
          AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
          AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
          AAAA87777777770000007777777777FAAAAA877777777700000077778888888A
          AAAA8888888777000000777FAAAAAAAAAAAAAAAAAAA877000000777FAAAAAAAA
          AAAAAAAAAAA877000000777FAAAAAAAAAAAAAAAAAAA877000000777FAAAAAAAA
          AAAAAAAAAAA8770000007777FFFFFFFAAAAA8FFFFFF7770000007777777777FA
          AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
          AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
          AAAA87777777770000007777777777FAAAAA877777777700000077777777777F
          FFFF777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000}
        Caption = ''
        TabOrder = 1
        OnClick = btNuevoClick
      end
      object btBorrarCS: TUniBitBtn
        Tag = 99
        Left = 0
        Top = 30
        Width = 37
        Height = 29
        Hint = ''
        ShowHint = True
        Glyph.Data = {
          16020000424D160200000000000076000000280000001A0000001A0000000100
          040000000000A001000000000000000000001000000000000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777788888888
          8888888887777700000077791111111111111111187777000000777911111111
          1111111118777700000077791111111111111111187777000000777911111111
          1111111118777700000077779999999999999999977777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000777777777777
          7777777777777700000077777777777777777777777777000000}
        Caption = ''
        TabOrder = 2
        ImageIndex = 30
      end
    end
    object UniDBGrid1: TUniDBGrid
      Left = 36
      Top = 0
      Width = 953
      Height = 110
      Hint = ''
      ShowHint = True
      DataSource = dsCabe1
      WebOptions.Paged = False
      WebOptions.FetchAll = True
      LoadMask.Message = 'Loading data...'
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 2
      Columns = <
        item
          FieldName = 'IDSTOCABE'
          Title.Caption = 'IDSTOCABE'
          Width = 64
        end
        item
          FieldName = 'SWTIPODOCU'
          Title.Caption = 'SWTIPODOCU'
          Width = 73
        end
        item
          FieldName = 'NDOCSTOCK'
          Title.Caption = 'NDOCSTOCK'
          Width = 66
        end
        item
          FieldName = 'FECHA'
          Title.Caption = 'FECHA'
          Width = 64
        end
        item
          FieldName = 'NALMACEN'
          Title.Caption = 'NALMACEN'
          Width = 64
        end
        item
          FieldName = 'ID_PROVEEDOR'
          Title.Caption = 'ID_PROVEEDOR'
          Width = 82
        end
        item
          FieldName = 'FABRICANTE'
          Title.Caption = 'FABRICANTE'
          Width = 124
        end
        item
          FieldName = 'SWDOCTOPROVE'
          Title.Caption = 'SWDOCTOPROVE'
          Width = 89
        end
        item
          FieldName = 'DOCTOPROVE'
          Title.Caption = 'DOCTOPROVE'
          Width = 124
        end
        item
          FieldName = 'DOCTOPROVEFECHA'
          Title.Caption = 'DOCTOPROVEFECHA'
          Width = 106
        end
        item
          FieldName = 'FECHACARGO'
          Title.Caption = 'FECHACARGO'
          Width = 73
        end
        item
          FieldName = 'FECHAABONO'
          Title.Caption = 'FECHAABONO'
          Width = 73
        end
        item
          FieldName = 'NOMBRE'
          Title.Caption = 'NOMBRE'
          Width = 244
        end
        item
          FieldName = 'NIF'
          Title.Caption = 'NIF'
          Width = 94
        end
        item
          FieldName = 'OBSERVACIONES'
          Title.Caption = 'OBSERVACIONES'
          Width = 88
        end
        item
          FieldName = 'PAQUETE'
          Title.Caption = 'PAQUETE'
          Width = 64
        end
        item
          FieldName = 'SWDEVOLUCION'
          Title.Caption = 'SWDEVOLUCION'
          Width = 85
        end
        item
          FieldName = 'SWCERRADO'
          Title.Caption = 'SWCERRADO'
          Width = 69
        end
        item
          FieldName = 'SWFRATIENDA'
          Title.Caption = 'SWFRATIENDA'
          Width = 77
        end
        item
          FieldName = 'SEMANA'
          Title.Caption = 'SEMANA'
          Width = 64
        end
        item
          FieldName = 'MAXPAQUETE'
          Title.Caption = 'MAXPAQUETE'
          Width = 71
        end>
    end
  end
  object UniPageControl3: TUniPageControl
    Left = 0
    Top = 160
    Width = 997
    Height = 527
    Hint = ''
    ShowHint = True
    ActivePage = TabSheet6
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    ExplicitHeight = 478
    object TabSheet6: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'Entrada'
      ExplicitHeight = 450
      object UniPanel5: TUniPanel
        Left = 0
        Top = 0
        Width = 989
        Height = 158
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        object UniLabel9: TUniLabel
          Left = 12
          Top = 21
          Width = 65
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'C.Barras [F5]'
          TabOrder = 9
        end
        object pBarrasA: TUniEdit
          Left = 79
          Top = 16
          Width = 231
          Height = 27
          Hint = ''
          ShowHint = True
          Text = ''
          TabOrder = 0
          OnExit = pBarrasAExit
        end
        object pAdendumEntrar: TUniDBEdit
          Left = 595
          Top = 17
          Width = 77
          Height = 26
          Hint = ''
          ShowHint = True
          DataField = 'ADENDUM'
          DataSource = dsLinea
          TabOrder = 2
        end
        object UniLabel10: TUniLabel
          Left = 617
          Top = 2
          Width = 41
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Num[F7]'
          TabOrder = 10
        end
        object pCantidad: TUniDBEdit
          Left = 172
          Top = 62
          Width = 57
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 5
        end
        object UniLabel11: TUniLabel
          Left = 175
          Top = 45
          Width = 43
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cantidad'
          TabOrder = 11
        end
        object BtnSumarCanti: TUniBitBtn
          Left = 246
          Top = 62
          Width = 55
          Height = 39
          Hint = ''
          ShowHint = True
          Caption = '+'
          ParentFont = False
          Font.Height = -24
          TabOrder = 12
        end
        object BtnRestarCanti: TUniBitBtn
          Left = 305
          Top = 62
          Width = 55
          Height = 39
          Hint = ''
          ShowHint = True
          Caption = '-'
          ParentFont = False
          Font.Height = -32
          TabOrder = 13
        end
        object pGraDescripcion: TUniDBEdit
          Left = 316
          Top = 17
          Width = 273
          Height = 26
          Hint = ''
          ShowHint = True
          DataField = 'DESCRIPCION'
          DataSource = dsLinea
          TabOrder = 1
        end
        object UniLabel59: TUniLabel
          Left = 405
          Top = 2
          Width = 54
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Descripcion'
          TabOrder = 14
        end
        object UniLabel39: TUniLabel
          Left = 12
          Top = 45
          Width = 45
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total PVP'
          TabOrder = 15
        end
        object UniLabel40: TUniLabel
          Left = 77
          Top = 45
          Width = 30
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'P.V.P.'
          TabOrder = 16
        end
        object pPrecio: TUniDBEdit
          Left = 9
          Top = 62
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PrecioTotal'
          DataSource = dsLinea
          TabOrder = 3
        end
        object pPreu: TUniDBEdit
          Left = 75
          Top = 81
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOVENTA'
          DataSource = dsLinea
          TabOrder = 7
        end
        object UniDBEdit37: TUniDBEdit
          Left = 9
          Top = 82
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOMPRA2'
          DataSource = dsLinea
          TabOrder = 6
        end
        object pPreu2: TUniDBEdit
          Left = 75
          Top = 62
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOVENTA2'
          DataSource = dsLinea
          TabOrder = 4
        end
        object btMasInfo: TUniBitBtn
          Left = 625
          Top = 115
          Width = 45
          Height = 35
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 17
          Images = ImgNativeList
          ImageIndex = 55
          OnClick = btMasInfoClick
        end
        object UniLabel32: TUniLabel
          Left = 13
          Top = 108
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 18
        end
        object UniLabel33: TUniLabel
          Left = 65
          Top = 108
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'A Stock'
          TabOrder = 19
        end
        object UniLabel34: TUniLabel
          Left = 163
          Top = 108
          Width = 32
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Merma'
          TabOrder = 20
        end
        object UniLabel35: TUniLabel
          Left = 214
          Top = 108
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Venta'
          TabOrder = 21
        end
        object UniLabel36: TUniLabel
          Left = 315
          Top = 108
          Width = 27
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Devol'
          TabOrder = 22
        end
        object UniLabel37: TUniLabel
          Left = 415
          Top = 108
          Width = 31
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Abono'
          TabOrder = 23
        end
        object UniDBEdit30: TUniDBEdit
          Left = 13
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'COMPRAHISTO'
          DataSource = dsLinea
          TabOrder = 24
        end
        object paStock: TUniDBEdit
          Left = 64
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'CANTIENALBA'
          DataSource = dsLinea
          TabOrder = 25
        end
        object pMerma: TUniDBEdit
          Left = 163
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MERMA'
          DataSource = dsLinea
          TabOrder = 26
        end
        object UniDBEdit33: TUniDBEdit
          Left = 214
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'VENDIDOS'
          DataSource = dsLinea
          TabOrder = 27
        end
        object UniDBEdit34: TUniDBEdit
          Left = 112
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'RECLAMADO'
          DataSource = dsLinea
          TabOrder = 28
        end
        object UniDBEdit35: TUniDBEdit
          Left = 262
          Top = 126
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ADENDUM'
          DataSource = dsLinea
          TabOrder = 29
        end
        object UniDBDateTimePicker6: TUniDBDateTimePicker
          Left = 315
          Top = 126
          Width = 98
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'FECHADEVOL'
          DataSource = dsLinea
          DateTime = 43375.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 30
        end
        object pFeAbono: TUniDBDateTimePicker
          Left = 415
          Top = 126
          Width = 98
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'FECHAABONO'
          DataSource = dsLinea
          DateTime = 43375.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 31
        end
        object UniLabel1: TUniLabel
          Left = 262
          Top = 108
          Width = 25
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Num.'
          TabOrder = 32
        end
        object UniLabel2: TUniLabel
          Left = 112
          Top = 108
          Width = 30
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Recla.'
          TabOrder = 33
        end
      end
      object pnlTotales: TUniPanel
        Left = 0
        Top = 158
        Width = 989
        Height = 60
        Hint = '60'
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        Caption = ''
        object pMargen1: TUniDBEdit
          Left = 6
          Top = 15
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN'
          DataSource = dsLinea
          TabOrder = 1
        end
        object pMargen2: TUniDBEdit
          Left = 6
          Top = 35
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN2'
          DataSource = dsLinea
          TabOrder = 2
        end
        object pEncarte2: TUniDBEdit
          Left = 65
          Top = 35
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE2'
          DataSource = dsLinea
          TabOrder = 3
        end
        object pEncarte1: TUniDBEdit
          Left = 65
          Top = 15
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE'
          DataSource = dsLinea
          TabOrder = 4
        end
        object UniLabel41: TUniLabel
          Left = 13
          Top = 1
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Margen'
          TabOrder = 5
        end
        object UniLabel42: TUniLabel
          Left = 69
          Top = 1
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Encarte'
          TabOrder = 6
        end
        object pTIva1: TUniDBEdit
          Left = 128
          Top = 15
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA'
          DataSource = dsLinea
          TabOrder = 7
        end
        object pTIva2: TUniDBEdit
          Left = 128
          Top = 35
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA2'
          DataSource = dsLinea
          TabOrder = 8
        end
        object UniDBEdit47: TUniDBEdit
          Left = 155
          Top = 15
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA'
          DataSource = dsLinea
          TabOrder = 9
        end
        object UniDBEdit48: TUniDBEdit
          Left = 199
          Top = 15
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE'
          DataSource = dsLinea
          TabOrder = 10
        end
        object UniDBEdit49: TUniDBEdit
          Left = 155
          Top = 35
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA2'
          DataSource = dsLinea
          TabOrder = 11
        end
        object UniDBEdit50: TUniDBEdit
          Left = 199
          Top = 35
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE2'
          DataSource = dsLinea
          TabOrder = 12
        end
        object UniLabel43: TUniLabel
          Left = 130
          Top = 1
          Width = 20
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Tipo'
          TabOrder = 13
        end
        object UniLabel44: TUniLabel
          Left = 160
          Top = 1
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '%IVA'
          TabOrder = 14
        end
        object UniLabel45: TUniLabel
          Left = 207
          Top = 1
          Width = 27
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '% RE'
          TabOrder = 15
        end
        object UniDBEdit51: TUniDBEdit
          Left = 244
          Top = 15
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE'
          DataSource = dsLinea
          TabOrder = 16
        end
        object UniDBEdit52: TUniDBEdit
          Left = 306
          Top = 15
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT'
          DataSource = dsLinea
          TabOrder = 17
        end
        object UniDBEdit53: TUniDBEdit
          Left = 244
          Top = 35
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE2'
          DataSource = dsLinea
          TabOrder = 18
        end
        object UniDBEdit54: TUniDBEdit
          Left = 306
          Top = 35
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT2'
          DataSource = dsLinea
          TabOrder = 19
        end
        object UniLabel46: TUniLabel
          Left = 254
          Top = 1
          Width = 42
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Coste'
          TabOrder = 20
        end
        object UniLabel47: TUniLabel
          Left = 310
          Top = 1
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Cte+RE'
          TabOrder = 21
        end
        object UniPanel17: TUniPanel
          Left = 372
          Top = -1
          Width = 369
          Height = 57
          Hint = ''
          ShowHint = True
          TabOrder = 22
          BorderStyle = ubsNone
          Caption = ''
          object UniDBEdit55: TUniDBEdit
            Left = 5
            Top = 17
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA'
            DataSource = dsLinea
            TabOrder = 1
          end
          object UniDBEdit56: TUniDBEdit
            Left = 70
            Top = 17
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE'
            DataSource = dsLinea
            TabOrder = 2
          end
          object UniDBEdit57: TUniDBEdit
            Left = 5
            Top = 37
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA2'
            DataSource = dsLinea
            TabOrder = 3
          end
          object UniDBEdit58: TUniDBEdit
            Left = 70
            Top = 37
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE2'
            DataSource = dsLinea
            TabOrder = 4
          end
          object UniLabel48: TUniLabel
            Left = 76
            Top = 3
            Width = 38
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste'
            TabOrder = 5
          end
          object UniLabel49: TUniLabel
            Left = 9
            Top = 3
            Width = 47
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'v.Compra'
            TabOrder = 6
          end
          object UniDBEdit59: TUniDBEdit
            Left = 135
            Top = 17
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT'
            DataSource = dsLinea
            TabOrder = 7
          end
          object UniDBEdit60: TUniDBEdit
            Left = 200
            Top = 17
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA'
            DataSource = dsLinea
            TabOrder = 8
          end
          object UniDBEdit61: TUniDBEdit
            Left = 135
            Top = 37
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT2'
            DataSource = dsLinea
            TabOrder = 9
          end
          object UniDBEdit62: TUniDBEdit
            Left = 200
            Top = 37
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA2'
            DataSource = dsLinea
            TabOrder = 10
          end
          object UniLabel50: TUniLabel
            Left = 136
            Top = 3
            Width = 65
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste + RE'
            TabOrder = 11
          end
          object UniLabel51: TUniLabel
            Left = 216
            Top = 3
            Width = 30
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Vta.'
            TabOrder = 12
          end
          object UniLabel52: TUniLabel
            Left = 275
            Top = 13
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Venta'
            TabOrder = 13
          end
          object UniLabel53: TUniLabel
            Left = 275
            Top = 26
            Width = 52
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Devolucion'
            TabOrder = 14
          end
          object UniLabel54: TUniLabel
            Left = 275
            Top = 39
            Width = 32
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Merma'
            TabOrder = 15
          end
          object UniDBText5: TUniDBText
            Left = 305
            Top = 13
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'VENTAHISTO'
          end
          object UniDBText6: TUniDBText
            Left = 305
            Top = 26
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'DEVOLHISTO'
          end
          object UniDBText7: TUniDBText
            Left = 305
            Top = 39
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'MERMAHISTO'
          end
        end
        object UniLabel55: TUniLabel
          Left = 742
          Top = 2
          Width = 25
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Alba.'
          TabOrder = 23
        end
        object UniLabel56: TUniLabel
          Left = 828
          Top = 2
          Width = 24
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Total'
          TabOrder = 24
        end
        object UniLabel57: TUniLabel
          Left = 762
          Top = 25
          Width = 37
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 25
        end
        object UniDBText8: TUniDBText
          Left = 770
          Top = 2
          Width = 56
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          DataField = 'CANTIENALBA'
        end
        object UniDBText9: TUniDBText
          Left = 804
          Top = 25
          Width = 56
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          DataField = 'COMPRAHISTO'
        end
      end
      object GridPreSeleccio: TUniDBGrid
        Left = 0
        Top = 218
        Width = 989
        Height = 229
        Hint = '348'
        ShowHint = True
        DataSource = dsLinea
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow]
        ReadOnly = True
        WebOptions.Paged = False
        WebOptions.FetchAll = True
        Grouping.Enabled = True
        LoadMask.Message = 'Loading data...'
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        Columns = <
          item
            FieldName = 'DEVUELTOS'
            Title.Caption = 'Devueltos'
            Width = 64
          end
          item
            FieldName = 'DESCRIPCION'
            Title.Caption = 'Descripcion'
            Width = 244
            ReadOnly = True
          end
          item
            FieldName = 'ADENDUM'
            Title.Caption = 'Num'
            Width = 34
          end
          item
            FieldName = 'PrecioTotal'
            Title.Caption = 'Total PVP'
            Width = 64
          end
          item
            FieldName = 'REFEPROVE'
            Title.Caption = 'Codi.Dist.'
            Width = 124
          end
          item
            FieldName = 'PAQUETE'
            Title.Caption = 'Paquete'
            Width = 64
          end
          item
            FieldName = 'CANTIENALBA'
            Title.Caption = 'A Stock'
            Width = 64
          end
          item
            FieldName = 'MERMA'
            Title.Caption = 'Merma'
            Width = 64
          end
          item
            FieldName = 'COMPRAHISTO'
            Title.Caption = 'His.Compra'
            Width = 64
            ReadOnly = True
          end
          item
            FieldName = 'MERMAHISTO'
            Title.Caption = 'His.Merma'
            Width = 64
            ReadOnly = True
          end
          item
            FieldName = 'DEVOLHISTO'
            Title.Caption = 'His.Devol.'
            Width = 64
            ReadOnly = True
          end
          item
            FieldName = 'VENTAHISTO'
            Title.Caption = 'His.Venta'
            Width = 64
            ReadOnly = True
          end
          item
            FieldName = 'VENDIDOS'
            Title.Caption = 'Vendidos'
            Width = 64
          end
          item
            FieldName = 'ID_ARTICULO'
            Title.Caption = 'Id.Arti'
            Width = 64
          end
          item
            FieldName = 'RECLAMADO'
            Title.Caption = 'Reclamado'
            Width = 64
          end
          item
            FieldName = 'FECHADEVOL'
            Title.Caption = 'Fecha Devol'
            Width = 64
          end
          item
            FieldName = 'FECHAAVISO'
            Title.Caption = 'Fecha Aviso'
            Width = 64
          end
          item
            FieldName = 'SWDEVOLUCION'
            Title.Caption = 'Sit. Devol'
            Width = 64
          end
          item
            FieldName = 'FECHA'
            Title.Caption = 'Fecha'
            Width = 64
          end
          item
            FieldName = 'IDDEVOLUCABE'
            Title.Caption = 'IDDEVOLUCABE'
            Width = 80
          end
          item
            FieldName = 'IDSTOCABE'
            Title.Caption = 'IDSTOCABE'
            Width = 64
          end
          item
            FieldName = 'ID_PROVEEDOR'
            Title.Caption = 'ID_PROVEEDOR'
            Width = 82
          end
          item
            FieldName = 'NOMBRE'
            Title.Caption = 'NOMBRE'
            Width = 244
            ReadOnly = True
          end
          item
            FieldName = 'BARRAS'
            Title.Caption = 'C. Barras'
            Width = 82
            ReadOnly = True
          end
          item
            FieldName = 'CANTIDAD'
            Title.Caption = 'CANTIDAD'
            Width = 64
          end
          item
            FieldName = 'FECHACOMPRA'
            Title.Caption = 'FECHACOMPRA'
            Width = 80
            ReadOnly = True
          end>
      end
      object UniPanel2: TUniPanel
        Left = 0
        Top = 447
        Width = 989
        Height = 52
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 3
        Caption = ''
        ExplicitLeft = 230
        ExplicitTop = 464
        ExplicitWidth = 256
        ExplicitHeight = 128
        object UniDBEdit1: TUniDBEdit
          Left = 35
          Top = 14
          Width = 34
          Height = 22
          Hint = ''
          ShowHint = True
          DataField = 'TOTAL'
          DataSource = dsLineaTotal
          TabOrder = 1
          ReadOnly = True
        end
        object UniLabel3: TUniLabel
          Left = 3
          Top = 18
          Width = 31
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total: '
          TabOrder = 2
        end
      end
    end
  end
  object ImgNativeList: TUniNativeImageList
    Left = 928
    Top = 48
    Images = {
      0D000000FFFFFF1F047104000089504E470D0A1A0A0000000D49484452000000
      16000000160806000000C4B46C3B0000000467414D410000AFC837058AE90000
      00097048597300000EC300000EC301C76FA8640000001974455874536F667477
      6172650041646F626520496D616765526561647971C9653C0000002174455874
      4372656174696F6E2054696D6500323030373A31323A32322031383A31383A33
      38CF77EBA1000003C149444154484BB5954B6C1B551486FF99B1C7F6B8695C9A
      882A2E1241294D858B2ACAA212A1A53C2221350B90A8541605762C40A2BCAAB2
      E8AE0810A20B844A592015812A8148449C05122291200D55D347D2F491B4C831
      8D129AA4A171E2DA9E873DFC673C76126848361CFBCFB973E7CE77FF7BEE1D47
      715D17FF4754C1358F1FF8FA99D627F7D5AE092B4EB1B4E26CD5016CB80A100C
      68CAE0D0B5CCC099FE36A86A2F042C7AFAE0C982C9C66AA3B44845E960740E4E
      BB7ACB3B1DC6CE77A1FAF3221A092B41BFBD9A9085964A40B10838944454D7A0
      E97A805A0017B97CFFFEB221CB2F1266538E2F9B9DB6FFA0C50E2E413E0BE095
      42AAEE896D71EB39AEF44BA654D214D65BB42A309F2D4BFEF8B93241E5A6A43B
      56B934B28215C11E64B148AC4045022AD8409E7922CB7298BCE604FF09E6734B
      209EFC6B99A4E0D02555E020A9B7F48365102D0B5EE2D217BF6528210235E9B2
      A451325EC0D2F0E3AEE0C5CEBCEC4B9E1367392EDD221401E6BC89226F8A3CC7
      FEE8BB822B833C8913BFEDC8B2654094AB3580ECF511CC1D7D01F95F3B803027
      094568D52821105DC631552D01DB3291382C70F4DCD828468F7D84F9541A85D4
      35DCB735053D3F804CC64543EF09BC39D919FBFEDCA7FF060BD4930F9697C1AB
      27A5E8C0544F12F1FA8B30BB8F61F6F208820D1B61CF71C21347B077F711BCFD
      F070735DF6A61CE972882B712730EF2C0A90D92250EAEAB9E7755DEB5ECCCCC5
      D1D8528B70EA5BF687A05D4FA279571217461FC2FBBF349D07B452153C3ECF9A
      1122BB2BCB16C9EBEB01995DEEFEADA12B981D388DA9F118863EEFC2F6C34FA0
      A6F97E6C3ED404F35C0E7D5F35418DC712F5B578A9FAB39978BDBDD0F3C9F3A1
      1A9E41D9750F28EE7DB0ED5A183DFE1A363F722FC2EBA270EF4C61DD965AA8F5
      310EBA8AECF9CBD0C333D02339DC6C77171CCBB9962904F24F28CD43D1028826
      DA303E19C0C8A90CD21DA7B84116DCC25FBC69C21ACAE0872FB6A1FDE30D39F3
      BBE9CE2A58A0625E204BA092E5DA5651B7BB0D46EB2138AE81075E7C0ABF7FD9
      47A717317EFC02623B0C38EB5524BBD626ADE1FC7355B0AA696A84BB1EA50C9E
      C9A8886755B211022294C1639AFBE91B6C4F4CC39E7091BE9180EB58B835FB18
      FEE86DC6BE977FC3C1ADFD3B27B350F9EE94E3CF1BA999CF7AA637DC43BAE9C8
      7F153A657FE5D8795B41F8FCEC46F475FF8889C4AB883F781BBB825731166F40
      37F663D3076FA031FDF399357CA4BA79CAB6575A105CFF1602811029C25C08D9
      0009191AD4A1D80EDCF05AE7D1C9FEBAAE3D67777C78BA317D14CF5E42EEF6F0
      E1C193EFED298E95FE06DE0E5D9824D4F3400000000049454E44AE426082FFFF
      FF1F04EE04000089504E470D0A1A0A0000000D49484452000000160000001608
      06000000C4B46C3B0000000467414D410000AFC837058AE90000000970485973
      00000EC300000EC301C76FA8640000001974455874536F667477617265004164
      6F626520496D616765526561647971C9653C0000002174455874437265617469
      6F6E2054696D6500323030373A31323A32322031393A35313A3530B90926EB00
      00043E49444154484BB5945D4C1C5514C7FF333B332CBBECC202C2B250055B40
      A06C432D149AA6E5C3104A3160D4C446248DA989ED8331697CEB0389F5414D8C
      314DF451EB8B8AD6168D35A6428BA1986A5B84200B08EE16B0CB0AFBCD02CBEE
      5CCF1D76494AA90F35FE9333737227E777CE3D73EF111863F83FA4811B1A1A20
      08024451844E279BB2B3323A9E282D6AB75AF3F6489292E1F32D79271C8EDF2E
      FFD0FF8DDBEDB928CB522812892410DB6B13ACAA8C83ED8D87EBDE3F78A8B9C1
      BB9E0FDF720A226B005B0F41563DD0C7A6D0DB7BE1DAB7DF5D391D8FC76E2618
      DB4A03D7D7D74365ACF2D8F3ED5FEE3DF842C9E882059916193B6D3A28B28060
      4485E34E14AE1917F2F03BFA2E9F9FF9ACE7D2314A7923C1B94F227F44A35173
      C3E103E70E3CD559F2EBA2152565A9A82896A0A40A88EB80D43411F60A3DAAEB
      4BE194F7A1AEF1D9C7F7D7547D48A1393C7E3B69E0CC4C4BC7D1D68E4363C11C
      64DB44ACABC0DD00B0B80CF8A8954BF45EF003F1385052B5036EA90C4D8D8D7B
      A9D72F6B946DA481ED15A5EDFA9C727862D0A05E0205A8B7FED50D0B9085A3B4
      4E49E857206D47118CD925D859F86807856771C65669E0A2C242FB328CD44F02
      103040101F07723F613EB2655A1709FC98CD02D16C85CD662BA4F062CED82A0D
      9CA2375854F2724D8059A1050158A3EA3988DB2AF9B484F41420DB408D4D1720
      2B7A3A9A522A2D3FC2195BA581FD7EFF92495161A1200ECF35D2FEC82C14C62D
      9BFCDC34229071DF2031B058187496292528F5FDD2C00EC7C4A8B8BA043DB522
      93E03CD84A102B25E1A641138932E87B28E045C8E3C4ECDC9C9BC2E73963AB34
      F08F57077AE71DB7601055DA1EFD1C6A8789B66DD2D3F6C9B86FA0A44632BA9C
      989F1AC7ECE430EEFCE51EA27017676C950676BA5C177B7A3E1F501726C1E24C
      AB5C03497486C9B86FA26A53A9DAF1B129F8666E63E8FAF5C51455BD40BD0FF1
      FE278D03F95BD7DDDD8DB7CE9E5D9B713A8785F86ACBAE82DC0C95FE89A42850
      14513B29BCCA503888B1E111FC71A30F95F67254EC2A96BAFCF3B9F1598F6F0C
      9822D626988F35ED4AF301C46530186ADA5A8F9C6B6939529D57580CD9980E3E
      FC964301B8E79C9818BD8DC1C19FFE6E6B6E497FEEC4EB4A71D6005C67CE845F
      F960FC543FF069124C57E15E704239F9F9F9C77757943D53905F50242B8ADEEB
      F3C6FE9C9EF18C4D4C0E46C2E1AF3FA9DB73BAF5A3E38DE9D235C83619136F4E
      ADBEFADEF0C9ABC0C7C98AA922EDB52949A2A66E288B12D6CAB2FC34F9CD64BB
      C9CC3CF0A8A26B1B693204D93B66C6AE543316EA64236F5485EA8097E8B3D692
      6DC17C2E3F48C9BDD512E466B5B8C2DEB530D6B78FB1E08BEC97D7EC2BFB2174
      3D3438F9B506E81AAA15C2ECED8C0D78B8937D7FA27CC94C791F4CF81725ABA6
      617CFEE4CFEC54FF17C115DC9AA6B3380D5589596808963F54C57487EE512555
      FED593D2DD4B4DC668B524F4D152D57F02733F593DDD9D3ABAA47C3E570110FF
      01B39F9A0D634B61870000000049454E44AE426082FFFFFF1F04720100008950
      4E470D0A1A0A0000000D4948445200000018000000180803000000D7A9CDCA00
      00000373424954080808DBE14FE00000000970485973000005BA000005BA011B
      ED8DC90000001974455874536F667477617265007777772E696E6B7363617065
      2E6F72679BEE3C1A00000054504C5445FFFFFFBF8080C39B6EC49971C69C73C8
      9E74CEA37AC29971D1A77ED8AC84A98258AB845AAC855BAE875EAF885FBF9870
      C89E75CDA67FCEA780D2AB85D4AD86D6AF8AD8B18BD9AD85DAAE86DFBB99E6C0
      A0F4D5BD72805B4B0000000A74524E530004335A81ADE5F0F5FE985B3F600000
      007A4944415428CFDDD15B1645300C85E1ED4E5B772287F9CF5358582D19C1F9
      9F9A7C6F2900240B4BEB26ADC76B497016951C0297D10919BF81B3631F575FA8
      62819CBFC03990B2069CA2E8AEE69F34DF530173D792D43EA30713D1A441E386
      C1350A18E39C37F810F48F60AD0AB5ED7B5B6BD71D8946F5ECE17FEC3CDD1D9D
      D21902360000000049454E44AE426082FFFFFF1F047F04000089504E470D0A1A
      0A0000000D4948445200000016000000160806000000C4B46C3B000000046741
      4D410000AFC837058AE9000000097048597300000EC300000EC301C76FA86400
      00001974455874536F6674776172650041646F626520496D6167655265616479
      71C9653C00000021744558744372656174696F6E2054696D6500323030373A31
      323A32322031393A35313A3530B90926EB000003CF49444154484BB5554D685D
      4514FE66EE4D9AB43198B649D3F794DAFCB42F3129882042B1518C0545A14404
      712B6E5CD485882B053782B8A8A2D42E5444140B456A17E2A2142A8A15AD0906
      F29236E6E5C5349888680DF5BDE4BD3B337EE7DC779B145CB8C9494ECECC9999
      EF9CF3CD991B1342C05688696F7FBE38389C2B181B85C405783A03FF88F58C29
      2AB1C572391DCB9AEC69F8F99B8E61E09D377F95AF2D9AC323AFBB97DE3B6E57
      432B6A6B1E090FD41C50A7CA38698C752E96BE5BE6992F616063B15EAFE38713
      EF7A33F2F09BEEF1D78EDB1F7FB57044CC3273DC9CA9FA0890F925A0FAC54AA6
      E2E37AB0161DED16731F9CF4E681D1B7DCD05363B6BABA8E8EDB08CE0D999886
      DD18FCB7C8B28D80F59AC7F81FAD289E3DE771EF91B7DDAB9F94C3B5DFFFE13D
      6E164F4DA84E67FF476EDCA88467DE590E71E1A48B3D79498265D808E5D22CAE
      CC961031FCC0E000F2F9BC66343555C4D2D212E238E6E5B1F68664C35AAD863B
      72DDB8ABB70007629192D81150F89272E6CB8BB87469024D4D3176EDEEBA095C
      2A2D606262122D2D2D3ACFC033E06A750DD5E102F6F51C4C3B8A98187AE8947B
      F1A3055251D1729CF3AADE0B15A938E75413F6A368BD9EA8D66A75D56A955A59
      0BD7FF5C0D4F9E5809187EDF596991CD1766AD5135C66846C4E5DCAA4651AA71
      1CD146F4C5DC1751593EF7052F5DC543A4320E420589909FC58579CCCD2FF240
      840307FAD0DDBD47C16766AE6279799960C2B1571F0BD2A0A2CC1A5D9D3BD1D3
      D34B6021D5906366227D2A19CEFE52C2C5AFBF578EDBDA76DC049E9EBE82F1F1
      9F95E30C4CFC84A735E4B88AA1BB0BD8BFBF4F5FA1646C7A8F7EEC46C746EC2B
      639DC877B68A5B450E926705111A24B03E08F2267EDE43C306D5E0EB707C75CF
      7EEAF1E5E90B3EE558C3DC2A0228E58A8A10975488A6DC0A2D99159F50C93BD7
      E0D2BADAC7C20B73C242B98439B69664D8DFDF8F5C2E07DE3E8AC51972FC9B02
      A5C1D24A329E85E39D1DB7A3AF573896341A7D2C13C968BA7815E72F7C438E9B
      B06DDB0EF6714E766172720A972F8F37384EAB11C9ACF4F1E0C041F4F1F2E4EA
      9801CCDEC7CEB8D147EFB36F3CDD89BDBB3738964E77FC74B1A3955F91845F1F
      E1D3B3ADD8CFCA77EAA326754426E0B9CF9AF1D5B9EFBCC93D71D61D7EF01EFB
      F2D1EDC8EFD98E9A7EA60477A364902A2D5D5B2D034FD7A56F833C5DFA2D815F
      F8BC1917CFFFE4CD9DC7BEF0871E39622A7F57644D9FB75CA6D0A3DA98679F4D
      997BCF8B6205D99AEC9344E4ECF5D086EAF4B7C134DFFFE1FCAE7CD73E1762FD
      0F22FD2D01B8373DD0500D429FCE057CD3BA8A5AED0D13565756B6EC7F1E1FF9
      5608F02FBB0CDEC15BB1ABD70000000049454E44AE426082FFFFFF1F04991100
      0089504E470D0A1A0A0000000D4948445200000020000000200806000000737A
      7AF4000000097048597300000B1300000B1301009A9C1800000A4F6943435050
      686F746F73686F70204943432070726F66696C65000078DA9D53675453E9163D
      F7DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551
      C1114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFB
      E17BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA942
      1E11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22
      C007BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B
      08801400407A8E42A600404601809D98265300A0040060CB6362E300502D0060
      277FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF56
      8A450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C
      00305188852900047B0060C8232378008499001446F2573CF12BAE10E72A0000
      7899B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC2
      7999193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2D
      EABF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA2
      25EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9
      E5E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE2248132
      5D814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962
      B9582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF
      3500B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D4280803
      806883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC70800
      0044A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A
      64801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B8
      0E3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985
      F821C14804128B2420C9881451224B91354831528A542055481DF23D72023987
      5C46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD0
      6474319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733
      C46C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB177
      04128145C0093604774220611E4148584C584ED848A8201C243411DA09370903
      8451C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843
      C437241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393
      C9DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F8
      53E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42AD
      A1B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA
      11DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B180718
      67197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591
      CA0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE465533
      53E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD8906
      59C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1
      CDD97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F8
      9C744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB
      48AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059D
      E753D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E80
      9E4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183C
      C535716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D46
      0F8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C
      3B4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6
      B86549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA7
      11A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D616762
      1767B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A56
      3ADE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD347
      671767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2
      F59D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051
      E5D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF7
      61EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF
      437F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB
      65F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE
      690E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD1319735
      77D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA
      3FC62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE3
      7B17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA8
      168C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92
      EC91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3A
      BD31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E95
      07C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB
      9E2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A
      39B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A
      4D6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA86
      9D1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964
      CF66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F
      97CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DD
      B561D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB
      49FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D5
      1DD23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4
      E9F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB
      5B625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D393
      67F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF
      8BE73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7
      BB9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E39
      3DDDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED
      41D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F
      43058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FE
      CBAE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2
      C61EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F5
      53D0A7FB93199393FF040398F3FC63332DDB0000000467414D410000B18E7CFB
      5193000000206348524D00007A25000080830000F9FF000080E9000075300000
      EA6000003A980000176F925FC546000006B44944415478DAEC9759685CD719C7
      7FE7DE3B77F619CD48A3C592B5D8B1BC86D8C4692795EC286E6843DC2D20521C
      28A50BB4A1A524831F5A485B91420B858ABA49082D144ADD906054A26292BA7D
      302E76B0ECD869BC91D8B5246B19692459A39166BBFBE9836583D3C4B68A5B3F
      B4E7E1DE03F770BEDFF97FE75BAE9052722F87C23D1EFF07B8E700DA7FC3C8B1
      2F25BF2D042F2151257CAF7B30FFE2F56F622551309049EF06560323C06560BC
      B77FC8B985E1664555FEA428BE07D37B5F677EE8CF7C70E4379E94F8BB07F3CE
      8A1418C8A41F5514E58D9ADA065FB55CC4A89691523A0399F4F832D015601478
      BFB77FE88D179FDEF9EA6CBCE7E9A7944BACBBAF0773669AA8D009476A954A39
      BF0578EF8E1518C8A43701C7366FDF9958D5D60980941E46A544A554A45A5EC2
      A894A8968B2C65473875B5DE29A8712DBD75031B9301D64D4C00309D3DC354F6
      2C9A5F74DB8677BC7B30EFDD166020936E04863A366C6D5BBBE9418A85796CCB
      20148DE30F841142DC589B3D7480C285D398ED1D2C7CF3F754DE3BC7CBBF7E85
      E71FBE8F862B87B06DC3F03CF968F7607EE88EEEC040261D068E34AE5EBB7DF3
      F61E3CCF65666218293D1CE79AEB354DC3CDCF517AFBAFA8AACD8627E304A21E
      07CEF7F0F2C1515AB76CA6C1A8D25B3D7258AD2E7EA67B30EFAE240ABE1EADA9
      DDBE71DB0E2CB34A7E368B69549818BE4028122796ACA37AE604EECC140D9B7D
      B43D5687359BC7C855D925FEC8C12D5FA1A39A773BCDD12F3EF2DAE89BFF4E18
      EE696E5FCF627E8662611ECBAC901DBD88E3D83FD166A71FB24E5F7DDC1F86FB
      BF9640556DAA63534857327E4A672927F846F26F1413D11FF4F69F78F3E30C7C
      AC0B0632E90EA128235D8FEF617E76927C6E8299C99105CDB1BE9B9A1CFD0E92
      744B3AA0AC7A288A393B8F5B35294E0BC6DFD5D122354CD6AFBA76593DD7969E
      F7706FFFD0E9952AF0E5646A15AAE6A3B25460363B4A6D76EC57BA65FC2E9810
      EAC6A7EA14AC0AD5F1696C0B264EFA291554663B3A4108A2357534B57562564A
      BED1F74FBF369049F7F4F60F4DAD04604FAA790D8BF9394A73D3A4C6874B9A6B
      FFB07D5748A9DB14C49C99C3336DF25714B2E77DB88D2D5C6D8F904C3551DBD8
      46385A4334910224D572715D6EFCD289814C7A776FFFD0D9DBBA602093DEA428
      CA85CE6D3B289C3C0CE3C3C41A15B9A137299C6211A750C4AEC2E8DB9A53B6FC
      62A9A5430DD53793AC6F211889134BD6A3AA379F6DE2F259C62E9D5BBC147BF2
      682EFCC93DFB325DA55B013CA159D6FEE4CC6452930EEB9E88105FAD61CEE431
      0DC9C80731E68A31CC40C8929AA6476A6A6968ED2496A827188E21948FAE71C7
      26C29C28ACC5F1C482849F4BCFDBF7912E681ABDF8394F5513566B0A3719E29D
      CB02E51F0E4246295901F460849A966612A9663D966A42557DD7E414D71E12F8
      F0C14C57E554A19D48433B8AAA242A8B0BCF1BC5C5E7B40F158FB410F22DE1D7
      6A16DA3A44B4AE83447D0BF5FE10527AB8B6452896241889A3AA2AAAAADEC884
      1289402084404A896DDB37411CCB35926C6E470F05582CDA042235E16A71D1BE
      01D0D7D7B7E553A98EC3EDA989A0BBA68952AE89FAD6F5C46A1B6F6C62949770
      6C0BA352221C891208DE9C8AAF0FC771B06C1BD775909E4B458639BF90644D73
      FC9A52C0E4D86409297FA62D1B6F09EA1C8FF43E136C755E60662C476529CAC2
      EC2466B5843F18C62817712C03A1AAA89ACE523E87A228F8743F9AEE47F3F9F1
      E901544DC7AC96282F15302A454072D4ECC1F3C5A91A369AA6329D5BC0AE964B
      527ABFD4FAFAFAE28A67BC7BFF03DD117F5D1385E15694853344A72764BE5C34
      67F4C092142205948179A045F707D5502C891E8AE2F307D1341DCF73FF4509D7
      B6C83B31269D3A44344076AA80AAAA58A5C29294DEB3FB325D96662D4D1D7CE7
      EC486ACBD64F70EEEF43DED1F95DC6036746A5DF2AFEF8D3FBC77EB11C156A6F
      FF90BB3C8F5B66B5CB9ACBEE007602DB7D7A400F4513E8A128AAE6C332CA14F3
      396CD3183E997CB6D58AC47CAAE3A22812CF31F11C6B0A380020F6EECD7851BF
      69044289D72B96FA1670BCAFAF2FBB82462504A497617600DB80BF00FB0FD7BE
      A03822F0073558135714054551C05E5C54F03EBF2FD37514404C1F4A37357EF6
      F8F4DDEE03BFF5D323AAE7C9CB9E1A6A57351DA108141CA94AE3C84B7BBB77DD
      E88AFF13C6AF9772572AC9445D2D8EEDE03A2ED22A175DC77DE6AEB4E5420884
      108A10C22F84080B219242887A21449B10626D3E37DCAC603337356926EA1278
      56C5765D6FF095EF3F72F18ECAF11D00A8800AE8CB6F3F100612CB733DD9B026
      D4F585E7BE5AD7BC7E37088460ED6F7FF4D8ECDD0210CB39C5BBD5BADBED2FFE
      E7FF8EFF39002BBC141D211C61280000000049454E44AE426082FFFFFF1F045D
      01000089504E470D0A1A0A0000000D4948445200000018000000180803000000
      D7A9CDCA0000000373424954080808DBE14FE00000000970485973000006A500
      0006A501179997DD0000001974455874536F667477617265007777772E696E6B
      73636170652E6F72679BEE3C1A00000033504C5445FFFFFFCC6633DC5D46D55A
      4AD6584BD6594BD75A4AD75A4AD75A4AD7594AD75A4AD75A4AD75A4ADF7A6DE0
      8175FBEFEEFCF3F2212726F90000000C74524E53000516304B6A7F80B2D0E6FA
      F55ABB46000000844944415428CF75924912C3200C0487C532CB98F8FFAFCD01
      0224F1F485AA16258124A0E3A3A55292458F1D77560EAAB9E543E6460E1F7F54
      7E518F71FFC793350080CBFC233B00271F30C0F74457EBA65D3D99471CE2D5B6
      838C306E91E969485C91E5995066C176DFD3B3E8804C258BCBE7CA0FCA96E826
      CAB6EB41E9D1EA65785A9F37CEC01529745D0F7E0000000049454E44AE426082
      FFFFFF1F04F901000089504E470D0A1A0A0000000D4948445200000018000000
      180803000000D7A9CDCA0000000373424954080808DBE14FE000000009704859
      73000006A5000006A501179997DD0000001974455874536F6674776172650077
      77772E696E6B73636170652E6F72679BEE3C1A00000099504C5445FFFFFF3399
      9923AE8B25AF8A25AD8824AD8924AF8926AD8725AD8825AE8825AE8825AE8825
      AE8827AF8928AF8928AF8A2BB08B2CB18C2EB18D38B59239B5933BB6943CB795
      3DB79544B99948BB9B4BBC9D52BFA157C0A359C1A45DC3A664C6AB6FCAB170CA
      B179CDB680D0BA88D3BE90D6C2A1DCCCABE0D1B7E4D8C1E8DDD0EEE5D3EEE7D9
      F1EADFF3EEE4F5F0E6F6F1E7F6F2E9F7F3F7FCFB3CCED0FA0000000C74524E53
      000516304B6A7F80B2D0E6FAF55ABB46000000BA4944415428CF7D52D7028320
      0C444464A47BEF5DBB6BFBFF1F5729A888A5F740E002979004210D4C28138251
      82918D209660206950F221070B3CCCF94842053232F71D3EF37CDF041C6AE02A
      4EEC904DB5D02C4F47A8715A2B318C88F3E0786E294310ADF2FB6B17B416ABF0
      9B7B4F6F1812CA7486FAB84CC7E686D08E65BA5066FA9A41E1D052F3E70E60F4
      58159A2C0F3EB824FDDBB60C468B74DBC9FB606541AC0F4EEC6AE17A498CD29F
      227ACBEE6F94BFB5FE61F8353E1FEB3C17E7A6193DB80000000049454E44AE42
      6082FFFFFF1F040C02000089504E470D0A1A0A0000000D494844520000001800
      0000180803000000D7A9CDCA0000000373424954080808DBE14FE00000000970
      485973000005BA000005BA011BED8DC90000001974455874536F667477617265
      007777772E696E6B73636170652E6F72679BEE3C1A000000A5504C5445FFFFFF
      FF0000FF5500D56A15D86214DC6823DE6421DF6A20DB661DDB671EDD6C1CDC6A
      1CDD681EDC691DDB6A1DDD691EDD691EDD681EDB691EDB691CDC691DDB691DDC
      691DDB691DDC691DDC681DDB691EDB691DDD691DDC691DDC691EDD681DDC691D
      DC681DDC691CDB691DDB6A1CDC691EDC691DDC691DDB691DDC691DDC691DDF7A
      1CDF7B1BE07F1BE0801BE1841BE1851BE38E1AE38F1AE49519E49719E59A19E5
      9C1909EA89E50000002A74524E530001030C0D161718232A2D484C506A707778
      81888B96979CA1A6ACB1B2B7BDC1CCCDCED0D7D8D9E2EDF753BB97AC000000A3
      4944415428918590B10EC2300C445F2A0BB50A43614042FCFF7FB1F105C5A8C4
      0943DB907AE98D7EBEF3C98155B7D1A0D397B1D7F91E00C6CB3610883D10ED0A
      20B103F84C041804AFA404E034B8B9CE20803C1C78CED001D9276516503C282B
      F0D5310EA2B2CB2A1BF0470A87C05DB70AD21EA4E328D737C3F22B0C62D8D6A7
      E586AC2BEF0A9CA3AF401B4781B97194364A2AB0262A416E1CA902D5E1FB6FAB
      0AF0038148422BCE6FCBA30000000049454E44AE426082FFFFFF1F040B060000
      89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
      3B0000000467414D410000AFC837058AE9000000097048597300000EC300000E
      C301C76FA8640000001974455874536F6674776172650041646F626520496D61
      6765526561647971C9653C00000021744558744372656174696F6E2054696D65
      00323030373A31323A32322031383A31383A3338CF77EBA10000055B49444154
      484B5D556B681C5514FEE6B1BBB3EFCD63DB6CD2B4B1CBA621B581A69A60B1D2
      48210A3EB0480B168A828815FA43D11010A5BFFC51100A152AFD57E88F0AC5EA
      0F41E90B45AB040C8D983E34AFE6DDDD6493EC637666EECC5CCF9DD96ACD59CE
      CC9D3B67BF73EE771E239D3DFB15368BE3382D7D7D4F5E6F6E6ED8C6B96BD7B7
      2149DE957EE06A2010BE70E1E237A74E7DF4366DD6C49BC745368C1A6AB52A74
      BDF2AF562A25ADB5359DCEE5B6253A3BB7373ED25C6E7B6336D7DEB83DDBDED4
      D1D1129165772F61B4F850FF177969698E802AB06D4E0E0C5293C04D6E9AA65B
      B7F1C476390CCB815E6328574D6FCF716C711AEE3D6C127974F42AEEDCB985C9
      C93FC890C13475D22A5CD7A99BF8A0B6EDC276485D5F85D7C76D368B1C0C6A28
      978BB8776F045353E304E0C0B298E0D933701C01CAC1E8CE8403DAB61907A738
      1FD90811FC874221A45229EF5916174551A1AA41CCCFDFC7ECEC9F585E9EA9B9
      24E29D00630420A2154E2C1139631EB065113794B8B6B636ECDAD58970582307
      5E867D602136D1C5180B2E2C4C34168B736F9141B3208F3171749F0E93C27508
      94310B2661B6B46CED8844C2879A9A9A92B15854AAD5741F8C44C966B302B4AB
      A1A1E1BDEEEE3D43BDFB9E1EEAEDDD7724DBD1AE34A7D314A9437CD28F22E68E
      4DCAE0DA0CD1A08C4AB9ACF5F53FF35226D3FA8AA6450E56AB7AC3FAFAFA149D
      C4900606067283832F7E77F8F0915C2291422299C0CD1B37B06E4791DBBD1BD5
      9A058B40295304EA800B86C8412216C1E86F37F1F2A167114F36C1D26B985F9C
      C3F0C71F7E7BFDDAD5E34A5F5FFFFB274F7EF01A79F5E880CB30B39C4743B60B
      E17814D14810A9848678220C2D1E41201683148BC389C4A07309C1DA3A4CA2EB
      76610ADB5B7620198D755DBBF6C3DF6A4747C753E93479B40C4AA00CCE0CAC21
      863C9288E92AD60C178B44DD9AC9A10BBF44BCE0DE956538D50CDA0353488764
      581B36D1C5D099EB443A9DEE57B76C49B7C729927299EAD7306194CA98B72398
      5E5171B700DC5D93615035C8B29F6D9174B11225C38D3006C32AF6C6423810EC
      861A54A164B688926B962D0B8962B1EC99062862590B602C9FC2C51F815F4680
      E234A0CF49A83CF0B53C23A1344DEB29A03AA561BCD880543C48B405A10614EA
      5C5B946F48A6489D9191FB181D9DC0E2E20A0CA6206845B1939AA2330CECA073
      B731A0D5F2B59DE878828A74B706F426393251A26DAD4A9DBB88B1B1494C4C2C
      529E39A4E1E14FC706075FEF310C8392EDD05139229A024E4DE3C841385CF1EB
      9812258A5E51005572A17006955B547A2E2A94075992A169218AB882E1E11357
      54C6CCA57038D423787EF87011E7CF9FA1CE321122BE0490A0B6DE4C9E502CD4
      75D4D2B476C959A95C45EFBE7E1C3DFAA617A9616CD010D32B6AB1B83A1D89A8
      8846E32814E6A83A741C3C38809E9E1E3212534C40886BBD1CC4DABB4B0810A7
      57AE7C4D519610A1B21473676E6E02A5D27A41BD7DFBF79F24C97E97E62FBDDC
      83A1A14FD0DDDD85643246538E8E4A51087544E7717F5E88A9269EC3E1309A9B
      3354310A32993405A7E1F2E571562CAEDD5174BDBA6059E69E6030B47363A324
      A7525B91CDB6797F9488375F895FAA5B315AC45AECF96346422211C3CCCC022F
      141EE2D6AD9FD9B9735FFC92CFE7BFACFF016945915FEDEB7BEEF3070F961871
      C8ABD51AAF546A7C63A3CA57572B7C65A5C4F3F975BEB454E40B0B2B7C7636CF
      272797B861D8FCF4E9336BF178E23345518E13564FDDAB2F070EBC804B97AE9E
      12A09B85A8E03493392397F461F1C06A35C6CB658BC607E7E3E37F71EA8977EA
      509E48C78E9DF092B17FFFF362D0BFB1BC3C7D840A5C1599F2F9F5B91577F1FC
      DFDADF27BFE2E3A08E8CFCFAFDEA6AE12C61D28C06FE016B5116450EA31BF100
      00000049454E44AE426082FFFFFF1F042805000089504E470D0A1A0A0000000D
      4948445200000016000000160806000000C4B46C3B0000000467414D410000AF
      C837058AE9000000097048597300000EC300000EC301C76FA864000000197445
      5874536F6674776172650041646F626520496D616765526561647971C9653C00
      000021744558744372656174696F6E2054696D6500323030373A31323A323220
      31393A35313A3530B90926EB0000047849444154484BB5946D4C5B5518C7FFA7
      BD7DA194522C30D8CA8B83990CDD1C9B316499909898C8A22426B83187C924EE
      C3CC34D9A6269AA0C98C413F28319AE8BE6C13448C08C98CF0C52959B68C6D46
      071B30CA8B84122883BEF7DEB6B7BDF71E9FD65A5C7CFB82FFE4E4DE73EF797E
      E73C2FE7619C73FC1FD2659E1BAEEC89FB184B3F533395466A47197881BE1E31
      E6E7D5E8ADD63C8DD6C625490C87223371A09BFE7D466B3585D6A6464A6F6578
      7F0B4E02B534EB2DDDFBE80395CDCDCC51570763BE1D0845202FCC63EDDA08EE
      5CBA843BA3E36E02B691C90FFF09A605CD266B6EF79E8ED3E6F2A3C7C04C3988
      AE7971ADB717AA24E291DA5A14180C509716313FFC3D8606BE557C61F1A4007C
      9CB26FCFF0EE8931B9B5C76CB7F5D4779D35571C3F99864296E11EFE1105E565
      D844D0D9CB9791585D453C22E1FEEDBBF06CD3D382CD66EDA4D03C419E669505
      6B8040A367F7E9B78D85CF1C487F0BBA5C90A6A761482450BA752B366FDB0681
      DEC58505F8A7A610A60D8AAC363CB5BB56AFE9581781CD694352169C000E6FA6
      98561C3B9E9E7BAE5EC518B93F3E380879799976D6C05515DCEBC5CCCA0AE6AC
      562C8FDF46C8E747357956535C5442A73E913626AD8782B1B68A3ACA99A083B8
      48F11B1A42DDA953B0575561F5C60D082613F482003F7960D9B103FB3A3A1024
      0F02348F8922AA8B1D805E7738435B079B1C05358E5C86E41B2F62E9EBAF5075
      E020E45008B12B57E0A43028F138343A7189C381647F3F4233D378A8BD1D7E8D
      C3353309838E23D768AAC8E0D6C17A8B254F601C81F39F43A630D86A1EC4F2C0
      009C5B9C30311D747A3D18558381B2BE291C86E7CC19588A8BA13EBC13EED520
      1825D9A8D79932B83F258F7656130A12944143E17DF49E009F9D865E0C0004E2
      61119AD7079D14058F8A502726A02693C82D2B4B5F0E35914C33FE50161C8B46
      255963E0663A5D5C06B350828301B83FFD04A191412CBC7414736D4710F8F526
      5C13E3489217290F583496B657150E4951A8067E57161C0E04A7BD6B21189C95
      E0A3A3744F15385A5A11F745109A7743764F22363F0EFF5A0089848AD2E70EA5
      ED22377F8149CF10A1F581647229FD9194052B9C77BB7E9E8079E72E245D1388
      75F520677F239CEFBC0BA3628612A1534954A80901DB5F7D0DF9ADCF43A2CAF1
      8C8CA0D061C724852ACE795F06B77EA53F624C50746CAEF16053799E6716C1DB
      8BA8ECFF0E68780CF2F5EB902F0E83532DE7D4EF83A9A11EFAB15BF8A9A909D2
      9A87FA880D5FDCF5F98955759EF3E03DE04EEA1514A0861C7BFEC5FD8D0D02A6
      6E21BA18C29613AFC374A8058C9244B54E376719F2850B98EC780FA2F72EF20A
      EC18F006348FA2B45056FACE667859F007992644DEBE9C6BB37DF8F8DE5AC11A
      09429C9A86D95A04A33D9F3A1F433418826FC90D439E1571A30943FEA0B69254
      DEA73A7B33657FEE9FC0A94622024F72C6CE5557384BAA287EE678149A2441A3
      724CA6E054922E29863151F2C99CBF42D02FD3C6A47F05D39D4F35794BEAF406
      C65A2D667365AAF8358AB1A8A809CAFE628CF36F28F39DB4D44F2D33ABBF8037
      5AD972DB5801BF01C5B4245FDB819AFC0000000049454E44AE426082E7EED600
      01F6060000424DF6060000000000003600000028000000180000001800000001
      00180000000000C0060000C21E0000C21E00000000000000000000D9E5F1C3D2
      E30E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E
      10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E1012
      0E1012B9BEEC7075C47277C77175C66F73C47074C66F73C66F72C76F72C76F71
      C86F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F
      71C97072C78A7DA30E1012CDC9E57975BA7473C36873C55C7CB65A7EB8567AB9
      5B75CC5E75CC5F76C1607AB6586FD25973C85B75C35F75C3636FCE656FCB6973
      C76D7BBC6F7AC5696CCE6B66DF6F70C50E1012C6CEE37378B66F74C36671C95D
      78BD5977BA5F7CC15E70C56A7AC4697BB36F84A6687AB8697EAE6A80A96D80AA
      6F79B8727DB96E7BAE6D81A07182AD6E78BA6B6DC96E70C80E1012BDD2E16C7A
      B46974C4646ECE6074C46276C3687AC66A72C6BEC6EDDAE4F4DEECF1E0E8F4E0
      EDF2E0EFF1E1EEF2E0E7F4DFE9F5DBE8F3DBF1EFDAEEF2CEDDF59299D26E70C8
      0E1012B4D6E0647CB36374C5616DD36270CA6670C76B71C5706EC0D3D1EFF3F4
      FBF9FEF7FFF9FFFEFDF8FDFEF6FBFEF9F8F8FEF6FAFFF1FBFCEDFEEEE9FBF3ED
      FAFC929BB96E70C80E1012B0D3E2617BB66275C46270CB6475C16675BE6D77C1
      6E70C2BCBFEADBE2F1DEEAEFE5DFF3E5E3F1E4E5F0E2E4F1E1DDF4E1E0F3DEE2
      F3ECFAF7F0FCFBF2F8FE9795B96E70C80E1012B4D0E6647AB96475C16573C263
      78B0798DC39CAEE27079C76E77C26B78B56B7EA87276BE727AB56F79AF707AB2
      7274BF7578B98086B6CFDCE4F0F9FEF1F3FD9F99C06E70C80E1012B8CCE66776
      B96874BD6D7ABB7890B1A9C4DBC6E1F56879C35F6FC56376C85D76BD6171CB61
      76C3667CC46276C1626DC86B74C47781BECBDAE8F1FAFEF4F4FEA198C06E70C8
      0E1012BBC7E46B72B4797EBF747EADB7CDD4E4FDFECFEAF4687BB46074C35F76
      C85A76C25F7DBE5578AE5C81B3597CB4637BC76479BC7186B6C8E0E3F0FEFEF4
      F9FCA29EB76E70C80E1012BBCBE56E77AF7A7FB8B1B3D4ECF6F9EFFBF8EAF4F8
      C3C9E9C8D1F1C1CFEEBFD3EBC6D5ECC3D7E7C1D9E8BFD6EAA8B7E57180B87687
      B2CBDFE3F1FEFEF4F9FDA29DB96E70C80E1012B9CFDF7481B0BABFEBF3EFFFF6
      F6F9F5F6F4FBFCFAF9F6FEF5F7FDEFF5F8F0FBF4F4FAFAF4FCF7F1FCFAEEF8FF
      CBCFEC757AB07D86B3CDDCE6F1FBFEF3F5FEA199BF6E70C80E1012B6D6DE6F82
      A7B8BDE5F4EDFFFCF6FCFFFBF9FFFDF8FFF7FEFFFCF9FFFEEDFFFEDCFFFEF0FF
      FBEAFEFCF3FEF9FFDDD2ED827AB48682B8D1D8EAF2F6FFF3EFFF9F93C86E70C8
      0E1012B8D2E56C7AB3767CBCB8B7E5EBF2FBF0FBFBEBF8F6C5D6E2B9D0E5B7D5
      E4B1D7E5C0CEE7C1D2E6C0D1E7C0CEE9AAAEE67576BD8586BED3D8E8F9F8FFFB
      F3FFAA97C16E72C50E1012B8CCE6717CBD6F73BF7D7BC4BEC5E7E7F1FBD8E5EF
      6D78AB6876B56477B4647EB36078B55F7BAD6481B05B76AC697BC76676BA7989
      BCC7DEE7EDFCFFF0F8FF9C9BBB6E72C50E1012BCCEE46D76BB7073C66B6AC180
      88C1B3BEE6D1DDF4737AC36F75C96E76C66572B8607BC55877B55F80B95978B6
      5E75C56076BC6F86B9BAD9E1E3FCFEE9FBFF97A0BC6E72C50E1012BDD2E26C79
      B56B73C46B72CF6777BD7082C2A6B7EB656BC47071CD7070C67476C05E68CD62
      72CA6072C46474C76872D26872C77885C6C3DAE9E9FBFFEDF8FF9599BD6E72C5
      0E1012BFD8DE6E80AC6979BC6073C65D7BBA5D7EB45F7EB36475B7868CC88F93
      C28A8EAE9087C79590C58E8BBB918BBD9386C8978AC49C92BFD7D7DEF9F7FAFC
      F7FEAA9DBD6E72C50E1012C1D5DD6D7CAE6D7BC45B6EC65979BC5B7EB75A7CAF
      6B7FBAC9D3F1E8F0FFF2F9FEF6F4FEF3F3FAF5F5F9F9F6FCF8EDFEFAEFFFF7EF
      FDFDFDF6FCFDF6FFF8FAA89CB76E72C50E1012C1CEDF6E77B56D76CB6170D45E
      7BC6597BB95D7EB3677BB5C9D6EFECF7FFEDF8FAF4FEFDF4FEF6F5FEF4F7FEF9
      F9F8FEF9F8FFF9F9FBF7FDEEFAFDF3F7F7F99F9BB76C70C30E1012C6CAE87372
      CA6865D46161DD6170D15E72C56278C16874C2A7ADDDB7BEDABBC4D4AAC9D6AB
      CCD3ACCDD3AFCAD5B1C0DDB0BEDDADBDDCAEC8D8ABC4D8A8BBDF7B87CD7175C8
      0E1012BDC6E26F70BA7470D06B68D6666EC46673BE6B7AC06C74C37379C26C75
      B07684AE637ABA657DB2677EB0697BB46B73C46B73C46974BF6B82B46279B366
      79C76773D76E72C50E1012C3D4DC747CAE6F71B97372C96D75B7717DB7717FB7
      6D76BE6F78BF6B78B46B7DAA6A77BE6C7BB56E7CB1717AB47273C37476C36B72
      B6697DA5697FAE687ABC5F6BC46F73C6C4D0E7D6EEE7C0D2D9C2CCE1C4C8E7C3
      CEE3C1D0E2BFD0E1BCC8E5BAC6E7B9CAE5B6CEE1C5C7E6C7CCE3CACEE1CCCCE2
      CDC6E6CDC8E6C9CBE2C7D8DCC2D6DFC1D2E2BCC8E4B7BBEED7E3F4FFFFFF1F04
      D701000089504E470D0A1A0A0000000D49484452000000180000001808060000
      00E0773DF80000000473424954080808087C0864880000000970485973000006
      0A0000060A01D64C94C00000001974455874536F667477617265007777772E69
      6E6B73636170652E6F72679BEE3C1A00000154494441544889ED95B14E024110
      86FF7F778F6B2C0C0986968282CE1730B181828B8D0589444B1EC4C780061225
      A1C042E23358D2DAF00E40022686CB8C158A78776C2EB151FF6A37F3CDFE9BD9
      DD59CEA7CD070097C8D642C8A878FAF40C00B7E3594FA19D03392B85B68CC7E2
      00706C1567DB89422E3C728E08368C0708001028B76382CC623F3882DE0679F5
      6FF0470C169EEC22659C2A51CE9D90D1EE234A04A1CB387CBDFFDC16DB2AA81B
      45C67B90D5DB261C52557D36935B3F7E067CB98A7A309ADD5B144B35B65DBB9B
      4C93C2D57EB3A441D09D5D3F7EEB6B0ED40E0E57A90C913A80448398B456B598
      14F32E1155BD1A5C6E83BC727992AAFD662926ED764EE34E002D54065179970B
      C5AD7319681074BFD65C0B006AD6DAD12E173B8C7319ECDF96CA202A5B6B47B3
      9BC9F93EFB2B9A1D575E24D58FDB933394960A1AC2F48F9CC07C03334C8B87E2
      D6B1C33829F60E4CF569099663028D0000000049454E44AE426082FFFFFF1F04
      7D05000089504E470D0A1A0A0000000D49484452000000180000001808060000
      00E0773DF8000000017352474200AECE1CE90000000467414D410000B18F0BFC
      6105000000097048597300000EC300000EC301C76FA864000000217445587443
      72656174696F6E2054696D6500323030353A30323A30342032313A32353A3532
      98F84897000004E549444154484BB5566B885465187EBE739D73E6BEB338BBBA
      4ADA56A6ADA9852876D9202410B284A20BA5464405D19F2482400B22A27FFDAC
      7EACF4272CC240221272126BFBA5B35A96DAE06EEBCECEECCCCECECC397366CE
      BDF79C5DA75D6F7FC4075EDE77CEF9BEF7F9DECBF79EC1ED065BD03785F6E9F2
      FB491D21B9237C70736C8CEF2F8E2DD8F304E420496A6360DF0047E4BB1B29B1
      D7016C8144041C92ABEC76B50DD7768689E097857D5D82032CAA1E64AA4CBFC8
      891F881B6AE691CD3B50D6D597387435B22D129787DB21370E07A7E3C2B3DD1C
      4B67689D558FBD71FEE92E8138B4F6A0B86E25A04FC03E3F01048E89C0ABF9B4
      C223DB87E7D34629022629E09209D22A583C052647C17812390DC8FD35C62735
      E3F09B972892C7AE21B0C7F2F09A12840DDBC96F199C2A822528839C0C661BA4
      290293C85D8922E0C28898A79017159E4719882C9BF49A65AD73E29399808056
      5C0D177C7F0262B600B1E71C78F10C98FE2BFCDA28EC7A0166E5323A750DA6D6
      861DA4C463141CC9C2EEAB718520EF962BF396675165FEE7F5A94CB6274077A2
      983133289A594C9114CD5E54EC14342702D3E3297D940C46448E158551D116B6
      83AA14822A380F77D682B4BA872CA3EBBC6927305ADD8213D54750F7D7A03F11
      8361FBE81833D896F807DB9353488B3E14DF069B3C66E0F2EF7F9183B0A5AF10
      2C019353F0BDE905E7317C3FB503271BCF63DFE61486FA22E0E8B0414A74B307
      47CF0FE0A3C2045E4B1EC59D8DE350B42987D94E1FBD1E0F7C5DA706C1660EAE
      A7A1E5A8F8A9F8288E559EC3AB0F26915119EA1D1B9A69A3D9B1605836B60D48
      D8B47A153E9FDE8A9A25C1F68594E723426E96108C7B738DD0F00DEA7FA51F0E
      9DBE66A6F0F5F82E3C718F82A6D541ADAD91E3065A66137AA7899AA1A34CC51E
      4804CDB31EA7FC4D707C3EC5ADE80D08428404D44ED4FCD47A5E1B7EDB05D7A3
      84E9F9BB318884DA4BB5D3615A35EA982A3CB702C79921A992D4288A26EAAD16
      56C4398CB943746338B04C3C485188EBA6086E392CB0413776B261A3ACFF4BAD
      781932370D810552A5E29521FA2530A7886A731A6353554CFBD9F9D11053BA33
      EB5A82A045B914BD3020B3D9F051AF3A8744A40245A812491D9C3F0B8EA29159
      0571BE8A1E690E111A273434200CD06D5F84C5047567628ED24321D2F904EADC
      6CA414BE90788E0AEC22260950A5181431065910A18A0CE9188781653CDD9128
      86565520DEB7127E5D0FF705584C90F75B6D30EAE760E0899C830DE9D34452C4
      F1C24A28BC4FA99121F049485206A29A0597EE83924EE2EC6C0F741A7C3B260F
      C1FEF667D8F98B81BFEBB4295D1470541FAB00B42C08A68277D67C89422D89AF
      FED844C3330B856652CC7210D70C48D3558C9E7271ACD08B57A28761CD5E40C7
      319F4AEC2F326A9C4381CBB0260168E01D67AA34CC659743DCBC0A4C98E776E7
      74FCF8E75DF842DF834454C6B68C860D711D173509A71B519C6DC4F14CF3433C
      1CFD0D83BB0EE1C237AFE78DD299E1873EF6C3BE5F4C10CE2B61FD7C03783375
      7895EE04C99D1536E7DE8F7F96A20D1B69E1304DEF3C5DA8FCFAD60FB91766DE
      1A59BB7B2732833BD1D2EFC585EFDEEE922C26D8456A2F49F065CB93E402A150
      BB9FBF1BE1E47B6C0FA991C12D1164B7BE8BB9E6033837F2E40811ECEB12DC2A
      AE9024FA6228A55FC2CB97761F2C1D78FC83A545BE05D06983A2EE9D2889B967
      C75FCC95B815C19F84DB0DE03FF4763210067428510000000049454E44AE4260
      82}
  end
  object dsLinea: TDataSource
    DataSet = DMDevolucion.sqlLinea
    Left = 868
    Top = 59
  end
  object dsCabe1: TDataSource
    DataSet = DMDevolucion.sqlCabe1
    Left = 868
    Top = 11
  end
  object dsHistoAnteF: TDataSource
    DataSet = DMDevolucion.sqlHistoAnteF
    Left = 876
    Top = 123
  end
  object dsHistoAnteFT: TDataSource
    DataSet = DMDevolucion.sqlHistoAnteFT
    Left = 876
    Top = 179
  end
  object dsHistoDevolver: TDataSource
    DataSet = DMDevolucion.sqlHistoDevolver
    Left = 916
    Top = 187
  end
  object dsHistoDevolverProve: TDataSource
    DataSet = DMDevolucion.sqlHistoDevolverProve
    Left = 916
    Top = 107
  end
  object dsCabeSCap: TDataSource
    DataSet = DMDevolucion.sqlCabeSCap
    Left = 804
    Top = 75
  end
  object dsProveS: TDataSource
    DataSet = DMDevolucion.sqlProveS
    Left = 804
    Top = 123
  end
  object dsCabeNuevo: TDataSource
    DataSet = DMDevolucion.sqlCabeNuevo
    Left = 812
    Top = 27
  end
  object dsLineaTotal: TDataSource
    DataSet = DMDevolucion.sqlLineaTotal
    Left = 804
    Top = 187
  end
end
