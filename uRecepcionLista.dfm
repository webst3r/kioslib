object FormListaRecepcion: TFormListaRecepcion
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 568
  ClientWidth = 1024
  Caption = 'FormListaRecepcion'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel2: TUniPanel
    Left = 0
    Top = 0
    Width = 1024
    Height = 65
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object UniLabel2: TUniLabel
      Left = 75
      Top = 14
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 1
    end
    object UniLabel3: TUniLabel
      Left = 75
      Top = 36
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 2
    end
    object edDesdeFEcha: TUniDBDateTimePicker
      Left = 111
      Top = 10
      Width = 97
      Hint = ''
      ShowHint = True
      DateTime = 43370.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 3
    end
    object edHastaFecha: TUniDBDateTimePicker
      Left = 111
      Top = 32
      Width = 97
      Hint = ''
      ShowHint = True
      DateTime = 43370.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 4
    end
    object UniSpeedButton8: TUniSpeedButton
      Left = 583
      Top = 14
      Width = 55
      Height = 42
      Hint = 'Documento'
      Visible = False
      ShowHint = True
      Caption = 'Docu.'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 2
      TabOrder = 5
    end
    object btAbrirFicha: TUniSpeedButton
      Left = 229
      Top = 17
      Width = 50
      Height = 35
      Hint = 'Documento'
      ShowHint = True
      Caption = ''
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 13
      TabOrder = 6
      OnClick = btAbrirFichaClick
    end
    object lbText: TUniLabel
      Left = 20
      Top = 25
      Width = 43
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'En Curso'
      TabOrder = 7
    end
    object btConsultaAbiertos: TUniBitBtn
      Left = 285
      Top = 17
      Width = 50
      Height = 35
      Hint = 'Consulta Abiertos'
      Visible = False
      ShowHint = True
      Caption = ''
      TabOrder = 8
      Images = DMppal.ImgListPrincipal
      ImageIndex = 16
    end
    object btConsultaCerrados: TUniBitBtn
      Left = 338
      Top = 17
      Width = 50
      Height = 35
      Hint = 'Consuta Cerradas'
      Visible = False
      ShowHint = True
      Caption = ''
      TabOrder = 9
      Images = DMppal.ImgListPrincipal
      ImageIndex = 15
    end
    object btConsultaTodos: TUniBitBtn
      Left = 391
      Top = 17
      Width = 50
      Height = 35
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = ''
      TabOrder = 10
      Images = DMppal.ImgListPrincipal
      ImageIndex = 14
    end
  end
  object gridListaRecepcion: TUniDBGrid
    Left = 0
    Top = 65
    Width = 1024
    Height = 503
    Hint = ''
    ShowHint = True
    DataSource = dsListaRecepcion
    ReadOnly = True
    WebOptions.PageSize = 14
    WebOptions.FetchAll = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Height = -13
    ParentFont = False
    TabOrder = 1
    Summary.Enabled = True
    OnDrawColumnCell = gridListaRecepcionDrawColumnCell
    OnColumnSummary = gridListaRecepcionColumnSummary
    OnColumnSummaryResult = gridListaRecepcionColumnSummaryResult
    Columns = <
      item
        FieldName = 'IDSTOCABE'
        Title.Caption = 'Id.'
        Width = 74
        Font.Height = -13
      end
      item
        FieldName = 'FECHA'
        Title.Caption = 'Recepcion'
        Width = 85
        Font.Height = -13
      end
      item
        FieldName = 'SEMANA'
        Title.Caption = 'Sem'
        Width = 45
        Font.Height = -13
      end
      item
        FieldName = 'FECHACARGO'
        Title.Caption = 'Cargo'
        Width = 88
        Font.Height = -13
      end
      item
        FieldName = 'ID_PROVEEDOR'
        Title.Caption = 'N.Prov'
        Width = 51
        Font.Height = -13
      end
      item
        FieldName = 'DOCTOPROVE'
        Title.Caption = 'Doc.Proveedor'
        Width = 98
        Font.Height = -13
      end
      item
        FieldName = 'DOCTOPROVEFECHA'
        Title.Caption = 'Fecha Docto.'
        Width = 83
        Font.Height = -13
      end
      item
        FieldName = 'NOMBRE'
        Title.Caption = 'Nombre'
        Width = 193
        Font.Height = -13
      end
      item
        FieldName = 'CANTIENALBA'
        Title.Caption = 'Albaran'
        Width = 79
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'CANTIDAD'
        Title.Caption = 'Recibidos'
        Width = 102
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'VALCOSTE'
        Title.Caption = 'Val.Coste'
        Width = 96
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'VALCOSTE2'
        Title.Caption = 'Val.Coste2'
        Width = 85
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'CosteTotal'
        Title.Caption = 'CosteTotal'
        Width = 80
        Font.Height = -13
        ShowSummary = True
      end>
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsListaRecepcion: TDataSource
    DataSet = DMMenuRecepcion.sqlListaRecepcion
    Left = 888
    Top = 424
  end
end
