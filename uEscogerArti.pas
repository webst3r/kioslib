unit uEscogerArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormEscogerArti = class(TUniForm)
    dsArticulo: TDataSource;
    pcDetalle: TUniPageControl;
    tabGrid: TUniTabSheet;
    gridUsuarios: TUniDBGrid;
    UniPanel1: TUniPanel;
    ImgList: TUniNativeImageList;
    btAceptar: TUniButton;
    btCancelar: TUniButton;
    lbBarras: TUniLabel;
    dsArtiRecepcion: TDataSource;
    dsArtiBarras: TDataSource;
    btDuplicar: TUniButton;
    procedure btAceptarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btDuplicarClick(Sender: TObject);


  private


    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    vNuevoUsu : Boolean;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swModificar, swNuevo : Boolean;
  
  end;

function FormEscogerArti: TFormEscogerArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolFicha, uDMRecepcion, uDMMenuArti, uRecepcionAlbaran, uFichaArti;

function FormEscogerArti: TFormEscogerArti;
begin
  Result := TFormEscogerArti(DMppal.GetFormInstance(TFormEscogerArti));

end;


procedure TFormEscogerArti.btAceptarClick(Sender: TObject);
begin
  if gridUsuarios.DataSource = dsArticulo then
  begin
    FormDevolFicha.pBarrasA.Text := DMppal.sqlArti1BARRAS.AsString;
    FormDevolFicha.pBarrasAExit(nil);
  end;

  if gridUsuarios.DataSource = dsArtiRecepcion then
  begin
    FormAlbaranRecepcion.edBarras.Text := DMMenuRecepcion.sqlArti1BARRAS.AsString;
    FormAlbaranRecepcion.edBarrasExit(nil);
  end;

  if gridUsuarios.DataSource = dsArtiBarras then
  begin
    FormManteArti.btCancelarArtiClick(nil);
    DMMenuArti.RutAbrirFichaArticulos(DMMenuArti.sqlComprobarBarrasID_ARTICULO.AsString);

  end;

  Self.Close;
  btDuplicar.Visible := false;
end;

procedure TFormEscogerArti.btCancelarClick(Sender: TObject);
begin
  Self.Close;
  btDuplicar.Visible := false;
  if gridUsuarios.DataSource = dsArticulo then FormDevolFicha.pBarrasA.SetFocus;
end;

procedure TFormEscogerArti.btDuplicarClick(Sender: TObject);
var
  vID_Ficha : Integer;
begin

  vID_Ficha := DMMenuArti.sqlFichaArticuloID_ARTICULO.AsInteger;
  CopyRecordDataset(DMMenuArti.sqlComprobarBarras, DMMenuArti.sqlFichaArticulo, 0);
  DMMenuArti.sqlFichaArticuloID_ARTICULO.AsInteger := vID_Ficha;
  DMMenuArti.sqlFichaArticuloDESCRIPCION.AsString := DMMenuArti.sqlFichaArticuloDESCRIPCION.AsString + ' ** DUPLICADO ** ';
  btDuplicar.Visible := false;

  Self.Close;
end;

end.



