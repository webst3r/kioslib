unit uDevolPaquetes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormDevolPaquetes = class(TUniForm)
    pnlTop: TUniPanel;
    gridListaPaquetes: TUniDBGrid;
    dsPaquetes: TDataSource;
    lbDatosDevol: TUniLabel;
    btSalir: TUniButton;
    btIrDevol: TUniButton;
    procedure btSalirClick(Sender: TObject);
    procedure btIrDevolClick(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }
    procedure RutInicioForm(vTotalPaquetes, vFecha: String; vIDProve,
      vIDCabe: Integer);

  end;

function FormDevolPaquetes: TFormDevolPaquetes;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu,
  uDMDevol, uDevolLista;

function FormDevolPaquetes: TFormDevolPaquetes;
begin
  Result := TFormDevolPaquetes(DMppal.GetFormInstance(TFormDevolPaquetes));

end;

procedure TFormDevolPaquetes.btIrDevolClick(Sender: TObject);
begin
  FormDevolLista.btFichaIndividualClick(nil);
  DMDevolucion.RutCambiarPaquete(DMDevolucion.sqlPaquetesPAQUETE.AsInteger);
  self.Close;

end;

procedure TFormDevolPaquetes.btSalirClick(Sender: TObject);
begin
 self.Close;
end;

procedure TFormDevolPaquetes.RutInicioForm(vTotalPaquetes, vFecha : String; vIDProve, vIDCabe : Integer);
begin
  //lbDatosDevol.Caption := 'Paquetes ' + DMDevolucion.sqlCabeSTOTALPAQUETES.AsString + ' -  ' + DateToStr(DMDevolucion.sqlCabeSFECHA.AsDateTime);
  //DMDevolucion.RutAbrirPaquetes(DMDevolucion.sqlCabeSID_PROVEEDOR.AsInteger,DMDevolucion.sqlCabeSIDSTOCABE.AsInteger);
  lbDatosDevol.Caption := 'Paquetes ' + vTotalPaquetes + ' -  ' + vFecha;
  DMDevolucion.RutAbrirPaquetes(vIDProve,vIDCabe);
end;

end.

