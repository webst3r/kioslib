unit uListaRecepcion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormListaRecepcion = class(TUniForm)
    tmSession: TUniTimer;
    UniPanel1: TUniPanel;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniSpeedButton3: TUniSpeedButton;
    UniNativeImageList1: TUniNativeImageList;
    UniLabel1: TUniLabel;
    UniBitBtn13: TUniBitBtn;
    UniPanel2: TUniPanel;
    UniDBNavigator1: TUniDBNavigator;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniLabel4: TUniLabel;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton7: TUniSpeedButton;
    UniSpeedButton8: TUniSpeedButton;
    UniSpeedButton6: TUniSpeedButton;
    UniDBGrid1: TUniDBGrid;
    UniSpeedButton9: TUniSpeedButton;
    dsListaRecepcion: TDataSource;
    procedure UniSpeedButton9Click(Sender: TObject);
    procedure UniFormShow(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormListaRecepcion: TFormListaRecepcion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uAlbaranRecepcion, uMenuRecepcion, uDMMenuRecepcion;

function FormListaRecepcion: TFormListaRecepcion;
begin
  Result := TFormListaRecepcion(DMppal.GetFormInstance(TFormListaRecepcion));

end;


procedure TFormListaRecepcion.UniFormShow(Sender: TObject);
begin
  DMMenuRecepcion.RutAbrirListaRecepcion;

end;

procedure TFormListaRecepcion.UniSpeedButton9Click(Sender: TObject);
begin
    FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabAlbaranRecepcion;
    FormAlbaran.Parent := FormMenuRecepcion.tabAlbaranRecepcion;
    FormAlbaran.Align  := alClient;
    FormAlbaran.Show();
    DMppal.RutInicioForm;
end;

end.
