unit uDMUltiCompra;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMUltiCompra = class(TDataModule)
    sqlUltiCompra: TFDQuery;
    sqlUltiCompraID_ARTICULO: TIntegerField;
    sqlUltiCompraADENDUM: TStringField;
    sqlUltiCompraFECHA: TDateField;
    sqlUltiCompraENTRADAS: TFloatField;
    sqlUltiCompraVENTAS: TFloatField;
    sqlUltiCompraDEVOLUCIONES: TFloatField;
    sqlUltiCompraMERMA: TFloatField;
    sqlUltiCompraSTOCK: TFloatField;
    sqlUltiCompraBARRAS: TStringField;
  
  private





    { Private declarations }
  public
    procedure RutAbrirUltiCompra(vID: Integer);

    { Public declarations }


    end;

function DMUltiCompra: TDMUltiCompra;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uMenu, uHistoArti;

function DMUltiCompra: TDMUltiCompra;
begin
  Result := TDMUltiCompra(DMppal.GetModuleInstance(TDMUltiCompra));
end;

procedure TDMUltiCompra.RutAbrirUltiCompra(vID : Integer);
begin

  with sqlUltiCompra do
  begin
    close;
    SQL.Text  := Copy(SQL.Text, 1,
                    pos('WHERE H.ID_ARTICULO', Uppercase(SQL.Text))-1)
                 + 'WHERE H.ID_ARTICULO = ' + IntToStr(vID)
                 + ' group by 1,2,3,4'
                 + ' order by 1,3 desc,2';
    Open;


  end;

end;


initialization
  RegisterModuleClass(TDMUltiCompra);





end.


