object FormCuadreMenu: TFormCuadreMenu
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 560
  ClientWidth = 1000
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalle: TUniPageControl
    Left = 0
    Top = 0
    Width = 1000
    Height = 560
    Hint = ''
    ShowHint = True
    ActivePage = tabFichaCuadre
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tabListaCuadre: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'Lista Facturas'
    end
    object tabFichaCuadre: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'Abono/Cargo'
    end
  end
end
