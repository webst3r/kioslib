unit uConsArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormConsArti = class(TUniForm)
    UniDBGrid1: TUniDBGrid;
    UniPanel1: TUniPanel;
    edBuscar: TUniEdit;
    UniLabel1: TUniLabel;
    btAgregarRecepcion: TUniBitBtn;
    btAgregarDevol: TUniBitBtn;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    ImgNativeList: TUniNativeImageList;
    btCerrar: TUniBitBtn;
    DataSource1: TDataSource;
    procedure btCerrarClick(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }


  end;

function FormConsArti: TFormConsArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormConsArti: TFormConsArti;
begin
  Result := TFormConsArti(DMppal.GetFormInstance(TFormConsArti));

end;

procedure TFormConsArti.btCerrarClick(Sender: TObject);
begin
  self.Close;
end;

end.
