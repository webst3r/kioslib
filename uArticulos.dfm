object FormArticulos: TFormArticulos
  Left = 0
  Top = 0
  ClientHeight = 840
  ClientWidth = 1177
  Caption = 'FormArticulos'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcTrabajos: TUniPageControl
    Left = 0
    Top = 240
    Width = 1177
    Height = 600
    Hint = ''
    ActivePage = tabArticulos
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ExplicitTop = 79
    ExplicitHeight = 757
    object tabArticulos: TUniTabSheet
      Hint = ''
      AlignmentControl = uniAlignmentClient
      ParentAlignmentControl = False
      Caption = 'Trabajos'
      ExplicitHeight = 729
      object btFinTrabajo: TUniButton
        Left = 223
        Top = 11
        Width = 118
        Height = 22
        Hint = ''
        Visible = False
        Caption = 'Finalizar Trabajo'
        TabOrder = 0
      end
      object btSeleccionar: TUniButton
        Left = 98
        Top = 11
        Width = 118
        Height = 22
        Hint = ''
        Enabled = False
        Visible = False
        Caption = 'Modificar Trabajo'
        TabOrder = 1
      end
      object btBorrarTrabajo: TUniButton
        Left = 348
        Top = 11
        Width = 118
        Height = 22
        Hint = ''
        Visible = False
        Caption = 'Borrar Trabajo'
        TabOrder = 2
      end
      object btNuevoTrabajo: TUniButton
        Left = 3
        Top = 11
        Width = 89
        Height = 22
        Hint = ''
        Visible = False
        Caption = 'Nuevo Trabajo'
        TabOrder = 3
      end
      object gridArt: TUniDBGrid
        Left = 3
        Top = 75
        Width = 1150
        Height = 430
        Hint = ''
        DataSource = dsArticulos
        LoadMask.Message = 'Loading data...'
        TabOrder = 4
        Columns = <
          item
            FieldName = 'ID_ARTICULO'
            Title.Caption = 'ID_ARTICULO'
            Width = 72
          end
          item
            FieldName = 'TBARRAS'
            Title.Caption = 'TBARRAS'
            Width = 64
          end
          item
            FieldName = 'BARRAS'
            Title.Caption = 'BARRAS'
            Width = 82
          end
          item
            FieldName = 'ADENDUM'
            Title.Caption = 'ADENDUM'
            Width = 53
          end
          item
            FieldName = 'FECHAADENDUM'
            Title.Caption = 'FECHAADENDUM'
            Width = 86
          end
          item
            FieldName = 'NADENDUM'
            Title.Caption = 'NADENDUM'
            Width = 64
          end>
      end
    end
    object tabProgramacion: TUniTabSheet
      Hint = ''
      Caption = 'Programacion'
      ExplicitHeight = 729
      object pcProgramacion: TUniPageControl
        Left = 0
        Top = 0
        Width = 1169
        Height = 572
        Hint = ''
        ActivePage = UniTabSheet1
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        ExplicitHeight = 729
        object UniTabSheet1: TUniTabSheet
          Hint = ''
          Caption = 'Programacion'
          ExplicitHeight = 701
          object UniPanel3: TUniPanel
            Left = 0
            Top = 0
            Width = 1161
            Height = 129
            Hint = ''
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            Caption = ''
            object edMemoSQL: TUniMemo
              Left = 1
              Top = 1
              Width = 1159
              Height = 127
              Hint = ''
              Lines.Strings = (
                'delete from FVISITA V'
                
                  ' where not exists (select * from FVISITAL L where L.ID_VISITA = ' +
                  'V.ID)'
                'and '
                '(   (V.Fecha < '#39'09/07/2017'#39')'
                'or'
                
                  '(   (V.Fecha = '#39'09/07/2017'#39')    and    (Hora <= '#39'12:00'#39')  )     ' +
                  ' )')
              ParentFont = False
              Font.Height = -13
              Align = alClient
              Anchors = [akLeft, akTop, akRight, akBottom]
              TabOrder = 1
            end
          end
          object UniPanel4: TUniPanel
            Left = 0
            Top = 129
            Width = 1161
            Height = 120
            Hint = ''
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            Caption = ''
            object btEjecutarMemo: TUniButton
              Left = 6
              Top = 42
              Width = 83
              Height = 41
              Hint = ''
              Caption = 'Ejecutar'
              TabOrder = 1
            end
            object rgTipoExec: TUniRadioGroup
              Left = 95
              Top = 26
              Width = 90
              Height = 57
              Hint = ''
              Items.Strings = (
                'Open'
                'ExecSQL')
              ItemIndex = 0
              Caption = 'Tipo Exec'
              TabOrder = 2
            end
            object UniButton1: TUniButton
              Left = 271
              Top = 45
              Width = 75
              Height = 38
              Hint = ''
              Visible = False
              Caption = 'Parada'
              TabOrder = 3
            end
            object cbParada: TUniCheckBox
              Left = 352
              Top = 45
              Width = 97
              Height = 17
              Hint = ''
              Visible = False
              Caption = 'cbParada'
              TabOrder = 4
            end
            object edMensajeTecnico: TUniEdit
              Left = 352
              Top = 61
              Width = 416
              Hint = ''
              Visible = False
              Text = '..... PARAMOS LA APLICACION DURANTE 5 MINUTOS ....'
              TabOrder = 5
            end
            object UniMemo1: TUniMemo
              Left = 774
              Top = 29
              Width = 427
              Height = 85
              Hint = ''
              Lines.Strings = (
                'alter TABLE FVISITA ADD TTERCERO Integer'
                'update FVISITA set TTERCERO = 1'
                'ALTER TABLE FVISITAL    ADD TITULARIZACION VARCHAR(50)'
                'CREATE INDEX FVISITAL_IDX2 ON FVISITAL (TIPOVISITA)'
                
                  'insert into FMTIPOS(CODIGO, descripcion) values (303, '#39'TIPOESTAD' +
                  'OOCUPACIONAL'#39')')
              TabOrder = 6
            end
            object edTema: TUniComboBox
              Left = 908
              Top = 3
              Width = 184
              Hint = ''
              Text = 'edTema'
              Items.Strings = (
                'default'
                'neptune'
                'uni_win10')
              TabOrder = 7
            end
            object UniLabel10: TUniLabel
              Left = 860
              Top = 7
              Width = 30
              Height = 13
              Hint = ''
              Caption = 'theme'
              TabOrder = 8
            end
            object UniDBNavigator5: TUniDBNavigator
              Left = 192
              Top = 88
              Width = 241
              Height = 25
              Hint = ''
              TabOrder = 9
            end
            object btAvisoDireccion: TUniBitBtn
              Left = 3
              Top = 9
              Width = 28
              Height = 27
              Hint = ''
              Visible = False
              Glyph.Data = {
                26050000424D260500000000000036040000280000000E0000000F0000000100
                080000000000F000000000000000000000000001000000000000000000000000
                BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000C0DCC000F0CA
                A600000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
                0707070707070707000007070707070000000007070707070000070707000000
                000000000007070700000707000000F9F9F9F90000000707000007070000F9F9
                F9F9F9F9000007077C00070000F9F9F9F9F9F9F9F90000070110070000F9F9F9
                F9F9F9F9F9000007E28D070000F9F9F9F9F9F9F9F9000007F400070000FFF9F9
                F9F9F9F9F90000070400070000FFFFF9F9F9F9F9F9000007030007070000FFFF
                F9F9F9F90000070791020707000000FFFFF9F900000007074E08070707000000
                000000000007070701000707070707000000000707070707FFFF070707070707
                07070707070707070000}
              Caption = ''
              TabOrder = 10
              Default = True
              ImageIndex = 0
            end
            object edNombre: TUniLabel
              Left = 37
              Top = 23
              Width = 48
              Height = 13
              Cursor = crHandPoint
              Hint = ''
              Visible = False
              Alignment = taRightJustify
              Caption = '+ Nombre'
              ParentFont = False
              Font.Style = [fsUnderline]
              TabOrder = 11
            end
          end
          object GridProgramacion: TUniDBGrid
            Left = 0
            Top = 249
            Width = 1161
            Height = 295
            Hint = ''
            DataSource = DataSource1
            WebOptions.FetchAll = True
            LoadMask.Message = 'Loading data...'
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 2
          end
        end
      end
    end
  end
  object pnlBusqueda: TUniPanel
    Left = 0
    Top = 0
    Width = 1177
    Height = 240
    Hint = ''
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object rbFiltroNombre: TUniRadioGroup
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 90
      Height = 78
      Hint = ''
      Items.Strings = (
        'Todo'
        'Cualquier'
        'Empiece')
      Caption = ''
      TabOrder = 1
      AutoScroll = True
    end
  end
  object dsArticulos: TDataSource
    DataSet = DMppal.sqlArticulos
    Left = 8
    Top = 760
  end
  object dsTiempo: TDataSource
    Left = 72
    Top = 760
  end
  object dsEmpresa: TDataSource
    Left = 8
    Top = 808
  end
  object dsSumTiempo: TDataSource
    Left = 128
    Top = 760
  end
  object dsTiempoCliente: TDataSource
    Left = 72
    Top = 808
  end
  object dsTodosTiempos: TDataSource
    Left = 120
    Top = 808
  end
  object dsEmpresa2: TDataSource
    Left = 200
    Top = 812
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    OnTimer = tmSessionTimer
    OnEventHandler = tmSessionEventHandler
    Left = 1148
    Top = 1
  end
  object DataSource1: TDataSource
    DataSet = DMppal.sqlUpdate
    Left = 208
    Top = 760
  end
end
