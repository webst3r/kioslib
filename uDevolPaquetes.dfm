object FormDevolPaquetes: TFormDevolPaquetes
  Left = 150
  Top = 150
  Hint = 'Otras'
  ClientHeight = 272
  ClientWidth = 461
  Caption = ''
  BorderStyle = bsSingle
  FormStyle = fsStayOnTop
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  BorderIcons = []
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TUniPanel
    Left = 0
    Top = 0
    Width = 461
    Height = 38
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object lbDatosDevol: TUniLabel
      Left = 0
      Top = 7
      Width = 108
      Height = 19
      Hint = ''
      ShowHint = True
      Caption = 'lbDatosDevol'
      ParentFont = False
      Font.Height = -16
      Font.Style = [fsBold]
      TabOrder = 1
    end
    object btSalir: TUniButton
      Left = 408
      Top = 0
      Width = 55
      Height = 35
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 2
      Images = DMppal.ImgListPrincipal
      ImageIndex = 10
      OnClick = btSalirClick
    end
    object btIrDevol: TUniButton
      Left = 336
      Top = 0
      Width = 55
      Height = 35
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 3
      Images = DMppal.ImgListPrincipal
      ImageIndex = 13
      OnClick = btIrDevolClick
    end
  end
  object gridListaPaquetes: TUniDBGrid
    Left = 0
    Top = 38
    Width = 461
    Height = 234
    Hint = ''
    ShowHint = True
    DataSource = dsPaquetes
    ReadOnly = True
    WebOptions.Paged = False
    WebOptions.FetchAll = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    Columns = <
      item
        FieldName = 'IDSTOCABE'
        Title.Caption = 'Devolucion'
        Width = 64
      end
      item
        FieldName = 'PAQUETE'
        Title.Caption = 'Paquete'
        Width = 69
      end
      item
        FieldName = 'EJEMPLARES'
        Title.Caption = 'Ejemplares'
        Width = 80
        ReadOnly = True
      end
      item
        FieldName = 'DISTRIBUIDORA'
        Title.Caption = 'Distribuidora'
        Width = 240
        ReadOnly = True
      end>
  end
  object dsPaquetes: TDataSource
    DataSet = DMDevolucion.sqlPaquetes
    Left = 432
    Top = 176
  end
end
