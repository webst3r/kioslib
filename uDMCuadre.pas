unit uDMCuadre;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMCuadre = class(TDataModule)
    sqlFraProve1: TFDQuery;
    sqlFraProveS: TFDQuery;
    sqlHiUpdaMarca: TFDQuery;
    sqlAlbaAbonoP: TFDQuery;
    sqlHistoP: TFDQuery;
    sqlFraProveSID_FRAPROVE: TIntegerField;
    sqlFraProveSID_PROVEEDOR: TIntegerField;
    sqlFraProveSSUFACTURA: TStringField;
    sqlFraProveSFECHAFACTURA: TDateField;
    sqlFraProveSNOMBRE: TStringField;
    sqlFraProveSNOMBRE2: TStringField;
    sqlFraProveSID_DIRECCION: TIntegerField;
    sqlFraProveSFECHACARGOA: TDateField;
    sqlFraProveSFECHACARGOZ: TDateField;
    sqlFraProveSFECHAABONOA: TDateField;
    sqlFraProveSFECHAABONOZ: TDateField;
    sqlFraProveSNIF: TStringField;
    sqlFraProveSEMAIL: TStringField;
    sqlFraProveSMENSAJEAVISO: TStringField;
    sqlFraProveSOBSERVACIONES: TMemoField;
    sqlFraProveSIMPOBRUTO: TSingleField;
    sqlFraProveSIMPODTO: TSingleField;
    sqlFraProveSTPCDTOPP: TSingleField;
    sqlFraProveSIMPODTOPP: TSingleField;
    sqlFraProveSIMPONETO: TSingleField;
    sqlFraProveSSWIVA: TSmallintField;
    sqlFraProveSIMPOBASE1: TSingleField;
    sqlFraProveSTPCIVA1: TSingleField;
    sqlFraProveSTPCRE1: TSingleField;
    sqlFraProveSIMPOIVA1: TSingleField;
    sqlFraProveSIMPORE1: TSingleField;
    sqlFraProveSIMPOBASE2: TSingleField;
    sqlFraProveSTPCIVA2: TSingleField;
    sqlFraProveSTPCRE2: TSingleField;
    sqlFraProveSIMPOIVA2: TSingleField;
    sqlFraProveSIMPORE2: TSingleField;
    sqlFraProveSIMPOBASE3: TSingleField;
    sqlFraProveSTPCIVA3: TSingleField;
    sqlFraProveSTPCRE3: TSingleField;
    sqlFraProveSIMPOIVA3: TSingleField;
    sqlFraProveSIMPORE3: TSingleField;
    sqlFraProveSIMPOBASE4: TSingleField;
    sqlFraProveSTPCIVA4: TSingleField;
    sqlFraProveSTPCRE4: TSingleField;
    sqlFraProveSIMPOIVA4: TSingleField;
    sqlFraProveSIMPORE4: TSingleField;
    sqlFraProveSIMPOIMPUESTOS: TSingleField;
    sqlFraProveSIMPOBASEEXENTA: TSingleField;
    sqlFraProveSIMPOTOTAL: TSingleField;
    sqlFraProveSVTO1: TDateField;
    sqlFraProveSIMPVTO1: TSingleField;
    sqlFraProveSVTO2: TDateField;
    sqlFraProveSIMPVTO2: TSingleField;
    sqlFraProveSVTO3: TDateField;
    sqlFraProveSIMPVTO3: TSingleField;
    sqlFraProveSTEFECTO: TSmallintField;
    sqlFraProveSEFECTOS: TSmallintField;
    sqlFraProveSFRECUENCIA: TSmallintField;
    sqlFraProveSDIASPRIMERVTO: TSmallintField;
    sqlFraProveSDIAFIJO1: TSmallintField;
    sqlFraProveSDIAFIJO2: TSmallintField;
    sqlFraProveSDIAFIJO3: TSmallintField;
    sqlFraProveSNALMACEN: TSmallintField;
    sqlFraProveSSWCONTABILIZADA: TSmallintField;
    sqlFraProveSTIVACLI: TSmallintField;
    sqlFraProveSSWTIPOCOBRO: TSmallintField;
    sqlFraProveSIMPOACUENTA: TSingleField;
    sqlFraProveSIMPOPENDIENTE: TSingleField;
    sqlFraProveSCTACONTACLI: TStringField;
    sqlFraProveSSWALTABAJA: TSmallintField;
    sqlFraProveSFECHABAJA: TDateField;
    sqlFraProveSFECHAALTA: TDateField;
    sqlFraProveSHORAALTA: TTimeField;
    sqlFraProveSFECHAULTI: TDateField;
    sqlFraProveSHORAULTI: TTimeField;
    sqlFraProveSUSUULTI: TStringField;
    sqlFraProveSNOTAULTI: TStringField;
    FDQuery1: TFDQuery;
    sqlProveS: TFDQuery;
    sqlProveSID_PROVEEDOR: TIntegerField;
    sqlProveSNOMBRE: TStringField;
    sqlProveSNIF: TStringField;
    sqlProveSREFEPROVEEDOR: TStringField;
    sqlFraProve1ID_FRAPROVE: TIntegerField;
    sqlFraProve1ID_PROVEEDOR: TIntegerField;
    sqlFraProve1SUFACTURA: TStringField;
    sqlFraProve1FECHAFACTURA: TDateField;
    sqlFraProve1NOMBRE: TStringField;
    sqlFraProve1NOMBRE2: TStringField;
    sqlFraProve1ID_DIRECCION: TIntegerField;
    sqlFraProve1NIF: TStringField;
    sqlFraProve1EMAIL: TStringField;
    sqlFraProve1MENSAJEAVISO: TStringField;
    sqlFraProve1OBSERVACIONES: TMemoField;
    sqlFraProve1IMPOBRUTO: TSingleField;
    sqlFraProve1IMPODTO: TSingleField;
    sqlFraProve1TPCDTOPP: TSingleField;
    sqlFraProve1IMPODTOPP: TSingleField;
    sqlFraProve1IMPONETO: TSingleField;
    sqlFraProve1SWIVA: TSmallintField;
    sqlFraProve1IMPOBASE1: TSingleField;
    sqlFraProve1TPCIVA1: TSingleField;
    sqlFraProve1TPCRE1: TSingleField;
    sqlFraProve1IMPOIVA1: TSingleField;
    sqlFraProve1IMPORE1: TSingleField;
    sqlFraProve1IMPOBASE2: TSingleField;
    sqlFraProve1TPCIVA2: TSingleField;
    sqlFraProve1TPCRE2: TSingleField;
    sqlFraProve1IMPOIVA2: TSingleField;
    sqlFraProve1IMPORE2: TSingleField;
    sqlFraProve1IMPOBASE3: TSingleField;
    sqlFraProve1TPCIVA3: TSingleField;
    sqlFraProve1TPCRE3: TSingleField;
    sqlFraProve1IMPOIVA3: TSingleField;
    sqlFraProve1IMPORE3: TSingleField;
    sqlFraProve1IMPOBASE4: TSingleField;
    sqlFraProve1TPCIVA4: TSingleField;
    sqlFraProve1TPCRE4: TSingleField;
    sqlFraProve1IMPOIVA4: TSingleField;
    sqlFraProve1IMPORE4: TSingleField;
    sqlFraProve1IMPOIMPUESTOS: TSingleField;
    sqlFraProve1IMPOBASEEXENTA: TSingleField;
    sqlFraProve1IMPOTOTAL: TSingleField;
    sqlFraProve1VTO1: TDateField;
    sqlFraProve1IMPVTO1: TSingleField;
    sqlFraProve1VTO2: TDateField;
    sqlFraProve1IMPVTO2: TSingleField;
    sqlFraProve1VTO3: TDateField;
    sqlFraProve1IMPVTO3: TSingleField;
    sqlFraProve1TEFECTO: TSmallintField;
    sqlFraProve1EFECTOS: TSmallintField;
    sqlFraProve1FRECUENCIA: TSmallintField;
    sqlFraProve1DIASPRIMERVTO: TSmallintField;
    sqlFraProve1DIAFIJO1: TSmallintField;
    sqlFraProve1DIAFIJO2: TSmallintField;
    sqlFraProve1DIAFIJO3: TSmallintField;
    sqlFraProve1NALMACEN: TSmallintField;
    sqlFraProve1SWCONTABILIZADA: TSmallintField;
    sqlFraProve1TIVACLI: TSmallintField;
    sqlFraProve1SWTIPOCOBRO: TSmallintField;
    sqlFraProve1IMPOACUENTA: TSingleField;
    sqlFraProve1IMPOPENDIENTE: TSingleField;
    sqlFraProve1CTACONTACLI: TStringField;
    sqlFraProve1TRANSPORTE: TSmallintField;
    sqlFraProve1FECHACARGOA: TDateField;
    sqlFraProve1FECHACARGOZ: TDateField;
    sqlFraProve1FECHAABONOA: TDateField;
    sqlFraProve1FECHAABONOZ: TDateField;
    sqlFraProve1SWALTABAJA: TSmallintField;
    sqlFraProve1FECHABAJA: TDateField;
    sqlFraProve1FECHAALTA: TDateField;
    sqlFraProve1HORAALTA: TTimeField;
    sqlFraProve1FECHAULTI: TDateField;
    sqlFraProve1HORAULTI: TTimeField;
    sqlFraProve1USUULTI: TStringField;
    sqlFraProve1NOTAULTI: TStringField;
    FDQuery2: TFDQuery;


  private
    procedure RutAbrirPorArticulos(vID: Integer);
    procedure RutAbrirPorDevol(vIdDevol, vIdFra: Integer);



    { Private declarations }
  public

    { Public declarations }
    procedure RutAbrirTablasLista;
    procedure RutAbrirFraProveS(vProvee : String; vTipo : Integer; vDesde,vHasta : TDate; vFecha : Boolean);
    procedure RutAbrirFraProve1(vProvee : Integer);



    end;

function DMCuadre: TDMCuadre;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uMenu;

function DMCuadre: TDMCuadre;
begin
  Result := TDMCuadre(DMppal.GetModuleInstance(TDMCuadre));
end;

procedure TDMCuadre.RutAbrirTablasLista;
begin

  sqlProveS.Close;
  sqlProveS.Open;

end;

procedure TDMCuadre.RutAbrirFraProveS(vProvee : String; vTipo : Integer; vDesde,vHasta : TDate; vFecha : Boolean);
var
  vWhere, vAnd, vStringFecha : String;
begin
  vWhere := ' > ' + vProvee;
  if vProvee <> '0' then vWhere := ' = ' + vProvee;

  vStringFecha := ' ';
  if vFecha then
  vStringFecha := ' AND FECHAFACTURA BETWEEN ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vDesde)) + ' AND ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vHasta));

//   0  En Curso    1  Cerrados  -1 Todos
  vAnd := '';
  if vTipo = 0 Then  vAnd := ' And ( SWALTABAJA = 0 or SWALTABAJA is null ) ';
  if vTipo = 1 Then  vAnd := ' And SWALTABAJA = 1 ';

   with sqlFraProveS do
  begin
    Close;
    sqlFraProveS.SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE ID_PROVEEDOR', Uppercase(SQL.Text))-1)
                  + 'WHERE ID_PROVEEDOR ' + vWhere
                  + vAnd
                  + vStringFecha
                  + ' Order by FECHAFACTURA DESC ' ;
    Open;
  end;
end;

procedure TDMCuadre.RutAbrirFraProve1(vProvee : Integer);
var
  vWhere, vAnd, vStringFecha : String;
begin
  vWhere := ' > ' + IntToStr(vProvee);
  if vProvee <> 0 then vWhere := ' = ' + IntToStr(vProvee);

  with sqlFraProve1 do
  begin
    Close;
    SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE ID_FRAPROVE', Uppercase(SQL.Text))-1)
                  + 'WHERE ID_FRAPROVE ' + vWhere
                  + ' Order by FECHAFACTURA DESC ' ;
    Open;
  end;
  RutAbrirPorDevol(-1,sqlFraProve1ID_FRAPROVE.AsInteger);

end;

procedure TDMCuadre.RutAbrirPorDevol(vIdDevol, vIdFra : Integer);
var
  vWhere, vAnd, vStringFecha : String;
begin
  vWhere := ' is not null ';
  if vIdDevol <> -1 then vWhere := ' = ' + IntToStr(vIdDevol);

  vAnd := ' AND (H.ID_FRAPROVE = 0 or H.ID_FRAPROVE IS NULL )';
  if vIdFra <> 0 then vAnd := ' AND (H.ID_FRAPROVE = ' + IntToStr(vIdFra) + ')';



  with sqlAlbaAbonoP do
  begin
    Close;
    sqlAlbaAbonoP.SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE H.IDSTOCABE', Uppercase(SQL.Text))-1)
                  + 'WHERE H.IDSTOCABE ' + vWhere
                  + ' AND C.SWTIPODOCU = 2 '
                  + ' AND H.CLAVE = ' + QuotedStr('54')
                  + vAnd
                  + ' GROUP BY H.IDSTOCABE,C.FECHA,H.ID_FRAPROVE,C.DOCTOPROVE, C.DOCTOPROVEFECHA,C.SWMARCA';
    Open;
  end;
end;





procedure TDMCuadre.RutAbrirPorArticulos(vID : Integer);
var
  vWhere, vAnd, vStringFecha : String;
begin

end;


initialization
  RegisterModuleClass(TDMCuadre);

end.


