unit uMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, Data.DB, uniBasicGrid, uniDBGrid,
  uniPageControl, uniGUIBaseClasses, uniPanel, uniButton,

  Dialogs, Vcl.ExtCtrls,

  uniTreeView, uniImage, uniSplitter, uniMultiItem, uniComboBox,
  uniToolBar, uniImageList, uniStrUtils, uniBitBtn, uniDBComboBox,
  uniDBLookupComboBox, uniEdit, uniDBText, uniLabel, uniCheckBox, uniChart,
  uniDBEdit, uniMemo,
  uniDateTimePicker, uniListBox,// Uni,
  uniGUIFrame,
  uniDBListBox, uniDBLookupListBox, uniDBCheckBox, uniDBMemo, Vcl.Imaging.jpeg,
  uniScrollBox, Vcl.Menus, uniMainMenu, uniRadioGroup, uniDBRadioGroup,
  uniDBDateTimePicker, uniDBNavigator, uniURLFrame, uniHTMLFrame,   // DBAccess,
  uniGroupBox, uniTimer,
  Shellapi, uniFileUpload, uniSpeedButton, Vcl.Imaging.pngimage;

type
  TFormMenu = class(TUniForm)
    UniSplitter1: TUniSplitter;
    pcDetalle: TUniPageControl;
    tabManteTPV: TUniTabSheet;
    UniFileUpload1: TUniFileUpload;
    tabArticulos: TUniTabSheet;
    tabCliente: TUniTabSheet;
    tabRecepcion: TUniTabSheet;
    tabDevolucion: TUniTabSheet;
    tmSession: TUniTimer;
    UniContainerPanel1: TUniContainerPanel;
    UniDBText2: TUniDBText;
    btMenu: TUniSpeedButton;
    btCerrarApp: TUniSpeedButton;
    btAtras: TUniSpeedButton;
    btVerMensaje: TUniBitBtn;
    tmMensaje: TUniTimer;
    lbMensaje: TUniMemo;
    tabDistribuidora: TUniTabSheet;
    tabPorDefecto: TUniTabSheet;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniSpeedButton7: TUniSpeedButton;
    tabConsArti: TUniTabSheet;
    imgAgrupada: TUniImage;
    ImgIndividual: TUniImage;
    imgAbierto: TUniImage;
    ImgCerrado: TUniImage;
    btExcel: TUniButton;
    tabHistoArti: TUniTabSheet;
    panelTopMenu: TUniContainerPanel;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniSpeedButton3: TUniSpeedButton;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton6: TUniSpeedButton;
    btClientes: TUniSpeedButton;
    UniSpeedButton8: TUniSpeedButton;
    UniSpeedButton9: TUniSpeedButton;
    UniSpeedButton10: TUniSpeedButton;
    UniSpeedButton11: TUniSpeedButton;
    UniSpeedButton12: TUniSpeedButton;
    UniSpeedButton13: TUniSpeedButton;
    UniSpeedButton14: TUniSpeedButton;
    UniSpeedButton15: TUniSpeedButton;
    UniSpeedButton16: TUniSpeedButton;
    UniSpeedButton17: TUniSpeedButton;
    UniSpeedButton18: TUniSpeedButton;
    UniSpeedButton19: TUniSpeedButton;
    UniSpeedButton20: TUniSpeedButton;
    UniSpeedButton21: TUniSpeedButton;
    UniSpeedButton22: TUniSpeedButton;
    UniSpeedButton23: TUniSpeedButton;
    UniSpeedButton24: TUniSpeedButton;
    UniSpeedButton25: TUniSpeedButton;
    UniSpeedButton26: TUniSpeedButton;
    UniSpeedButton27: TUniSpeedButton;
    panelSuperior: TUniContainerPanel;
    edMensajeTecnico: TUniLabel;
    UniDBText1: TUniDBText;
    lbMensajePrograma: TUniLabel;
    lbFormNombre: TUniLabel;
    tabReclamaciones: TUniTabSheet;
    tabCuadre: TUniTabSheet;
    procedure UniSpeedButton8Click(Sender: TObject);
    procedure btClientesClick(Sender: TObject);
    procedure UniSpeedButton6Click(Sender: TObject);
    procedure UniSpeedButton26Click(Sender: TObject);
    procedure UniSpeedButton27Click(Sender: TObject);
    procedure tmSessionTimer(Sender: TObject);
    procedure UniSpeedButton1Click(Sender: TObject);
    procedure btMenuClick(Sender: TObject);
    procedure btCerrarAppClick(Sender: TObject);
    procedure btAtrasClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure btVerMensajeClick(Sender: TObject);
    procedure tmMensajeTimer(Sender: TObject);
    procedure btExcelClick(Sender: TObject);



  private
    procedure RutComprobarAbierto;
    procedure RutVolverAtras(vForm: String);



    { Private declarations }
  public
    { Public declarations }

    FUrlDocumentacion :String;

    vUniForm : TUniForm;

    swReducirImagen : Boolean;


    vResult, vFoto : String;

    vDesdeFecha, vHastaFecha : TDateTime;

    FFolderUploadFile, FFolderFile  : String;

    swFormMenu : Integer;


    swAbrirTipos : Integer;
      swListaDet : Integer;

    vImage: TUniImage;
    vFormImage : String;


    swFicha,swManteTPV, swCliente, swArticulo, swRecepcion, swDevolucion
    , swDistribuidora, swConsArti, swCerrarAPP, swHistoArti
    , swReclamaciones, swCuadre : Integer;

    swMenuBts, swMensaje : Boolean;



    procedure DCallBack(Sender: TComponent; Res: Integer);
    procedure RutCambiarPestanaUSU;
    procedure RutGrabarNuevoUsuario;


    procedure RutAbrirConsArti;
    procedure RutAbrirHistoArti;
    procedure RutAbrirHistoArtiTablas(vId_Aritculo : Integer; vBarras,vAdendum,vDescripcion, vOrigen : String);

    procedure RutAbrirDevolucion;
    procedure RutAbrirCuadre;
    procedure RutAbrirRecepcion;
    procedure RutAbrirArticulos;
    procedure RutAbrirDistribuidora;
    procedure RutAbrirClientes;


  end;

function FormMenu: TFormMenu;




var
  vParada : Boolean;
  vMensajeTecnico : String;







implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uManteTPV, uCliente,
  uDevolMenu, uDevolLista, uDevolFicha,
  uDMRecepcion, uMenuRecepcion, uRecepcionLista, uRecepcionAlbaran,
  uDMDevol, uDMCliente, uMenuBotones, uConsArti, uMensaje, uNuevoUsuario, uClienteFicha,
  uDMDistribuidora, uDistribuidoraMenu, uDistribuidoraFicha, uDistribuidoraLista,
  uListaArti, uFichaArti, uMenuArti, uDMMenuArti,
  uLoadGRID, uLoadFiltro,
  uDMHistoArti, uHistoArti,
  uDMCuadre, uCuadreMenu, uCuadreFicha, uCuadreLista;

function FormMenu: TFormMenu;
begin
  Result := TFormMenu(DMppal.GetFormInstance(TFormMenu));
end;

procedure TFormMenu.UniFormCreate(Sender: TObject);
begin
  swMenuBts := False;
  lbMensaje.Caption := '';
  pcDetalle.ActivePage := tabPorDefecto;
  lbFormNombre.Caption := '';
end;

procedure TFormMenu.btMenuClick(Sender: TObject);
begin
  FormBotones.Parent := FormMenu;
  //FormBotones.Left := 200;
  //FormBotones.Top  := 105;
  if not swMenuBts then //Controlar que solo lo haga la primera vez que arranca
  begin
    FormBotones.Height := 140;
    //swMenuBts := True;
  end;
  FormBotones.ShowModal;
end;

procedure TFormMenu.btVerMensajeClick(Sender: TObject);
begin
//si es show modal no hace falta tener control si se abre 2 veces
  FormMensaje.ShowModal;
end;

procedure TFormMenu.btCerrarAppClick(Sender: TObject);
begin
//  UniApplication.Terminate('.. Gracias por utilizar este servicio ..');
  {
  if (FormMenu.swDevolucion = 1) and (FormDevolMenu.swDevoAlbaran = 1) then
  begin
    swCerrarAPP := 1;
    FormBotones.ShowModal();
  end
  else UniApplication.Terminate('.. Gracias por utilizar este servicio ..');
  }
 { if FormMenu.pcDetalle.ActivePage = FormMenu.tabManteTPV then
  begin
  //TEMPORALMENTE DESACTIVADO HASTA QUE MANTETPV FUNCIONE
    if FormManteTPV.swManteTpv = 1 then
    begin
      FormManteTPV.Close;
      FormDevolMenu.swDevoAlbaran := 0;
    end;
    if FormDevolMenu.swDevoLista = 1 then
    begin
      FormDevolLista.Close;
      FormDevolMenu.swDevoLista := 0;
    end;

    FormMenu.swDevolucion := 0;
    //RUTINA DE COMPROBAR QUE PROGRAMA ESTA ABIERTO E IR A EL
    //FormMenu.pcDetalle.ActivePage := FormMenu.tabPorDefecto; //linea temporal hasta tener rutina hecha

    //exit;//para salir y que no continue con los demas if

  end;
  }
  if FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion then
  begin

    if FormDevolMenu.swDevoAlbaran = 1 then
    begin
      FormDevolFicha.Close;
      FormDevolMenu.swDevoAlbaran := 0;
    end;
    if FormDevolMenu.swDevoLista = 1 then
    begin
      FormDevolLista.Close;
      FormDevolMenu.swDevoLista := 0;
    end;

    FormMenu.swDevolucion := 0;
    //RUTINA DE COMPROBAR QUE PROGRAMA ESTA ABIERTO E IR A EL
    //FormMenu.pcDetalle.ActivePage := FormMenu.tabPorDefecto; //linea temporal hasta tener rutina hecha

    //exit;//para salir y que no continue con los demas if

  end;

  if FormMenu.pcDetalle.ActivePage = FormMenu.tabRecepcion then
  begin

    if FormMenuRecepcion.swFichaRecepcion = 1 then
    begin
      FormAlbaranRecepcion.Close;
      FormMenuRecepcion.swFichaRecepcion := 0;
    end;
    if FormMenuRecepcion.swListaRecepcion = 1 then
    begin
      FormListaRecepcion.Close;
      FormMenuRecepcion.swListaRecepcion := 0;
    end;

    FormMenu.swRecepcion := 0;
  end;


  if FormMenu.pcDetalle.ActivePage = FormMenu.tabDistribuidora then
  begin

    if FormDistribuidoraMenu.swDistribuidoraFicha = 1 then
    begin
      FormDistribuidoraFicha.Close;
      FormDistribuidoraMenu.swDistribuidoraFicha := 0;
    end;
    if FormDistribuidoraMenu.swDistribuidoraLista = 1 then
    begin
      FormDistribuidoraLista.Close;
      FormDistribuidoraMenu.swDistribuidoraLista := 0;
    end;



    FormMenu.swDistribuidora := 0;  //swDistribuidora a 0 para que se habra de nuevo desde MenuBotones

    //RUTINA DE COMPROBAR QUE PROGRAMA ESTA ABIERTO E IR A EL
    //FormMenu.pcDetalle.ActivePage := FormMenu.tabPorDefecto; //linea temporal hasta tener rutina hecha

    //exit;//para salir y que no continue con los demas if

  end;

  if FormMenu.pcDetalle.ActivePage = FormMenu.tabArticulos then
  begin

    if FormMenuArti.swFicha = 1 then
    begin
      FormManteArti.Close;
      FormMenuArti.swFicha := 0;
    end;
    if FormMenuArti.swLista = 1 then
    begin
      FormListaArti.Close;
      FormMenuArti.swLista := 0;
    end;

    FormMenu.swArticulo := 0;
    //RUTINA DE COMPROBAR QUE PROGRAMA ESTA ABIERTO E IR A EL
    //FormMenu.pcDetalle.ActivePage := FormMenu.tabPorDefecto; //linea temporal hasta tener rutina hecha

    //exit;//para salir y que no continue con los demas if

  end;


  if FormMenu.pcDetalle.ActivePage = FormMenu.tabConsArti then
  begin
    //btAtrasClick(nil);
    //exit;
    if swConsArti = 1 then
    begin
      FormConsArti.Close;
      swConsArti := 0;
    end;

  end;

  if FormMenu.pcDetalle.ActivePage = FormMenu.tabHistoArti then
  begin
    if swHistoArti = 1 then
    begin
      FormHistoArti.Close;
      swHistoArti := 0;
    end;
    {if swConsArti = 1 then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabConsArti;
     // exit;
    end; }
  end;

  if FormMenu.pcDetalle.ActivePage = FormMenu.tabCuadre then
  begin

    if FormCuadreMenu.swFichaCuadre = 1 then
    begin
      FormCuadreFicha.Close;
      FormCuadreMenu.swFichaCuadre := 0;
    end;
    if FormCuadreMenu.swListaCuadre = 1 then
    begin
      FormCuadreLista.Close;
      FormCuadreMenu.swListaCuadre := 0;
    end;

    FormMenu.swCuadre := 0;
  end;


  if FormMenu.pcDetalle.ActivePage = FormMenu.tabPorDefecto then
  begin
    UniApplication.Terminate('.. Gracias por utilizar este servicio ..');
  end;


  RutComprobarAbierto;
end;

procedure TFormMenu.RutComprobarAbierto;
begin
  if swConsArti = 1 then
  begin
    FormMenu.pcDetalle.ActivePage := FormMenu.tabConsArti;
  end
  else
  if FormMenu.swDevolucion = 1 then
  begin
    if FormDevolMenu.swDevoAlbaran = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabDevolucion;
      FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabAlbaranDevolucion;
    end
    else
    if FormDevolMenu.swDevoLista = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabDevolucion;
      FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabListaDevolucion;
      DMPpal.RutCapturaGrid(FormDevolLista.Name, FormDevolLista.gridListaDevolucion, nil, 0, 0, 0);
    end
  end
  else
  if FormMenu.swRecepcion = 1 then
  begin
    if FormMenuRecepcion.swFichaRecepcion = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabRecepcion;
      FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabAlbaranRecepcion;
    end
    else
    if FormMenuRecepcion.swListaRecepcion = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabRecepcion;
      FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabListaRecepcion;
      DMPpal.RutCapturaGrid(FormListaRecepcion.Name, FormListaRecepcion.gridListaRecepcion, nil, 0, 0, 0);
    end
  end
  else
  if FormMenu.swDistribuidora = 1 then
  begin
    if FormDistribuidoraMenu.swDistribuidoraFicha = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabDistribuidora;
      FormDistribuidoraMenu.pcDetalle.ActivePage := FormDistribuidoraMenu.tabFichaDistribuidora;
    end
    else
    if FormDistribuidoraMenu.swDistribuidoraLista = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabDistribuidora;
      FormDistribuidoraMenu.pcDetalle.ActivePage := FormDistribuidoraMenu.tabListaDistribuidora;
      DMPpal.RutCapturaGrid(FormDistribuidoraLista.Name, FormDistribuidoraLista.gridDistribuidoraLista, nil, 0, 0, 0);
    end
  end
  else
  if FormMenu.swArticulo = 1 then
  begin
    if FormMenuArti.swFicha = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabArticulos;
      FormMenuArti.pcDetalle.ActivePage  := FormMenuArti.tabFichaArti;
    end
    else
    if FormMenuArti.swLista = 1 then
    begin
      FormMenu.pcDetalle.ActivePage      := FormMenu.tabArticulos;
      FormMenuArti.pcDetalle.ActivePage  := FormMenuArti.tabListaArti;
      DMPpal.RutCapturaGrid(FormListaArti.Name, FormListaArti.GridListaArti, nil, 0, 0, 0);
    end
  end
  else
  begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabPorDefecto;
  end;

  lbFormNombre.Caption := pcDetalle.ActivePage.Caption;

end;

procedure TFormMenu.RutVolverAtras(vForm : String);
begin
  if vForm = 'FormConsArti' then
  begin
    RutAbrirConsArti;
  end;

  if vForm = 'FormHistoArti' then
  begin
    RutAbrirHistoArti;
  end;

  if vForm = 'FormDevolLista' then
  begin
    RutAbrirDevolucion;
  end;

  if vForm = 'FormDevolFicha' then
  begin
    RutAbrirDevolucion;
    FormDevolMenu.RutAbrirAlbaranTab;
  end;

  if vForm = 'FormListaArti' then
  begin
    RutAbrirArticulos;
  end;

  if vForm = 'FormManteArti' then //fichaArticulo
  begin
    RutAbrirArticulos;
    FormMenuArti.RutAbrirFichaArti('');
  end;

  if vForm = 'FormListaRecepcion' then
  begin
    RutAbrirRecepcion;
  end;

  if vForm = 'FormAlbaranRecepcion' then
  begin
    RutAbrirRecepcion;
    FormMenuRecepcion.RutAbrirFichaRecepcion;
  end;

   if vForm = 'FormDistribuidoraLista' then
  begin
    RutAbrirDistribuidora;
  end;

  if vForm = 'FormDistribuidoraFicha' then
  begin
    RutAbrirDistribuidora;
    FormDistribuidoraMenu.RutAbrirFichaDistribuidora;
  end;

  if vForm = 'FormCuadreLista' then
  begin
    RutAbrirCuadre;
  end;

  if vForm = 'FormCuadreFicha' then
  begin
    RutAbrirCuadre;
    FormCuadreMenu.RutAbrirFicha(0);
  end;
end;


procedure TFormMenu.btAtrasClick(Sender: TObject);
begin
   if (FormMenu.pcDetalle.ActivePage    = tabConsArti) then
   begin
     RutVolverAtras(DMppal.swConsArti);
     DMppal.swConsArti := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage    = tabHistoArti) then
   begin
     RutVolverAtras(DMppal.swHisArti);
     DMppal.swHisArti := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage    = tabDevolucion) and (FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion) then
   begin
     RutVolverAtras(DMppal.swDevolucionLista);
     DMppal.swDevolucionLista := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage    = tabDevolucion) and (FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion) then
   begin
     RutVolverAtras(DMppal.swDevolucionFicha);
     DMppal.swDevolucionFicha := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabArticulos) and (FormMenuArti.pcDetalle.ActivePage = FormMenuArti.tabListaArti) then
   begin
     RutVolverAtras(DMppal.swArtiLista);
     DMppal.swArtiLista := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabArticulos) and (FormMenuArti.pcDetalle.ActivePage = FormMenuArti.tabFichaArti) then
   begin
     RutVolverAtras(DMppal.swArtiFicha);
     DMppal.swArtiFicha := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabRecepcion) and (FormMenuRecepcion.pcDetalle.ActivePage = FormMenuRecepcion.tabListaRecepcion) then
   begin
     RutVolverAtras(DMppal.swRecepcionLista);
     DMppal.swRecepcionLista := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabRecepcion) and (FormMenuRecepcion.pcDetalle.ActivePage = FormMenuRecepcion.tabAlbaranRecepcion) then
   begin
     RutVolverAtras(DMppal.swRecepcionFicha);
     DMppal.swRecepcionFicha := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabDistribuidora) and (FormDistribuidoraMenu.pcDetalle.ActivePage = FormDistribuidoraMenu.tabListaDistribuidora) then
   begin
     RutVolverAtras(DMppal.swDisitribuidoraLista);
     DMppal.swDisitribuidoraLista := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabDistribuidora) and (FormDistribuidoraMenu.pcDetalle.ActivePage = FormDistribuidoraMenu.tabFichaDistribuidora) then
   begin
     RutVolverAtras(DMppal.swDisitribuidoraFicha);
     DMppal.swDisitribuidoraFicha := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabCuadre) and (FormCuadreMenu.pcDetalle.ActivePage = FormCuadreMenu.tabListaCuadre) then
   begin
     RutVolverAtras(DMppal.swCuadreLista);
     DMppal.swCuadreLista := '';
   end
   else

   if (FormMenu.pcDetalle.ActivePage = tabCuadre) and (FormCuadreMenu.pcDetalle.ActivePage = FormCuadreMenu.tabFichaCuadre) then
   begin
     RutVolverAtras(DMppal.swCuadreFicha);
     DMppal.swCuadreFicha := '';
   end;




  {if (FormMenu.pcDetalle.ActivePage    = tabDevolucion) and (FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion)
  //if (FormMenu.swDevolucion = 1) and (FormDevolMenu.swDevoLista = 1)
  //then FormDevolMenu.pcDetalle.ActivePage    := FormDevolMenu.tabListaDevolucion
  then FormDevolMenu.RutAbrirLista
  else
  if FormMenu.pcDetalle.ActivePage = tabRecepcion
  then FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabListaRecepcion
  else
  if FormMenu.pcDetalle.ActivePage = tabCliente
  then FormCliente.pcDetalle.ActivePage := FormCliente.tabListaCliente
  else
  if (FormMenu.pcDetalle.ActivePage = tabArticulos) and (FormMenuArti.pcDetalle.ActivePage = FormMenuArti.tabFichaArti)
  //if (FormMenu.swArticulo = 1) and (FormMenuArti.swLista = 1)
  //then FormMenuArti.pcDetalle.ActivePage := FormMenuArti.tabListaArti
  then
  begin
    if FormManteArti.vStringOrigen = 'fichaDevol' then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;
      FormDevolMenu.RutAbrirAlbaranTab;
    end
    else
    if FormManteArti.vStringOrigen = 'consArti' then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabConsArti;
      FormMenu.RutAbrirConsArti;
    end
    else
    if FormManteArti.vStringOrigen = 'listaArti' then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;
      FormMenuArti.RutAbrirListaArti;
    end
    else
    if FormManteArti.vStringOrigen = 'fichaRecepcion' then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabRecepcion;
      FormListaRecepcion.RutAbrirAlbaranRece;
    end;

    FormManteArti.vStringOrigen := '';
  end
  else
  if FormMenu.pcDetalle.ActivePage = tabDistribuidora
  //then FormDistribuidoraMenu.pcDetalle.ActivePage := FormDistribuidoraMenu.tabListaDistribuidora;
  then FormDistribuidoraMenu.RutAbrirListaDistribuidora
  else
  if FormMenu.pcDetalle.ActivePage = tabConsArti then
  begin
    if FormConsArti.vStringOrigen = 'devolucion' then FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;
    if FormConsArti.vStringOrigen = 'articulos'  then FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;
    FormConsArti.vStringOrigen := '';
  end
  else
  if FormMenu.pcDetalle.ActivePage = tabHistoArti then
  begin
    if FormHistoArti.vStringOrigen = 'devolucion'      then FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;
    if FormHistoArti.vStringOrigen = 'articulos'       then FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;
    if FormHistoArti.vStringOrigen = 'consulta'        then FormMenu.pcDetalle.ActivePage := FormMenu.tabConsArti;
    if FormHistoArti.vStringOrigen = 'articulosFicha'  then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;
      //Sleep(2);//quizas lo podriamos usar para pararlo aqui y termine las demas ordenes
      FormMenuArti.pcDetalle.ActivePage := FormMenuArti.tabFichaArti;
    end;
    if FormHistoArti.vStringOrigen = 'Recepcion'  then
    begin
      FormMenu.pcDetalle.ActivePage := FormMenu.tabRecepcion;
      //Sleep(2);//quizas lo podriamos usar para pararlo aqui y termine las demas ordenes
      FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabAlbaranRecepcion;
    end;
    FormHistoArti.vStringOrigen := '';
  end;
  }
  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;
end;

procedure TFormMenu.DCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes : begin
              if vResult = '1' then
              begin
                FormNuevoUsuario.RutBorrarUsuario;
              end;



            end;
    mrNo  : vResult := 'mbNo';
  end;
end;


procedure TFormMenu.tmMensajeTimer(Sender: TObject);
begin
  DMppal.RutAbrirMensaje;
  lbMensaje.Text := DMppal.sqlMensajeMENSAJE.AsString;
end;

procedure TFormMenu.tmSessionTimer(Sender: TObject);
begin
//evento para que no se cierre el programa
end;

procedure TFormMenu.UniSpeedButton1Click(Sender: TObject);
begin
  if pcDetalle.ActivePage = tabDevolucion then
  begin
    if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then
       FormDevolLista.pnlBtsLista.Visible := not FormDevolLista.pnlBtsLista.Visible;
  end;

end;














//-------------------------------------------------
//BOTONES NO HACE NADA POR QUE ESTAN EN MENUBOTONES
//-------------------------------------------------
procedure TFormMenu.UniSpeedButton26Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabRecepcion;

  if swRecepcion = 0 then
  begin
    FormMenuRecepcion.Parent := tabRecepcion;
    FormMenuRecepcion.Align  := alClient;
    FormMenuRecepcion.Show();
    swRecepcion := 1;
    DMppal.RutInicioForm;

  end;
end;

procedure TFormMenu.UniSpeedButton27Click(Sender: TObject);
begin
   pcDetalle.ActivePage := tabDevolucion;

  if swDevolucion = 0 then
  begin
    FormDevolMenu.Parent := tabDevolucion;
    FormDevolMenu.Align  := alClient;
    FormDevolMenu.Show();
    swDevolucion := 1;
    DMppal.RutInicioForm;
    swMenuBts := True;
  end;

  DMDevolucion.RutAbrirTablas;
  FormDevolMenu.RutAbrirLista;
end;

procedure TFormMenu.btClientesClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabCliente;

  if swCliente = 0 then
  begin
    FormCliente.Parent := tabCliente;
    FormCliente.Align  := alClient;
    FormCliente.Show();
    swCliente := 1;
    DMppal.RutInicioForm;

  end;
  DMCliente.RutAbrirTablas;
  FormCliente.RutAbrirListaClie;
end;



procedure TFormMenu.UniSpeedButton6Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabArticulos;

  if swArticulo = 0 then
  begin
    FormMenuArti.Parent := tabArticulos;
    FormMenuArti.Align  := alClient;
    FormMenuArti.Show();
    swArticulo := 1;
    DMppal.RutInicioForm;

  end;

end;

procedure TFormMenu.UniSpeedButton8Click(Sender: TObject);
begin

  pcDetalle.ActivePage := tabManteTPV;

  if swManteTPV = 0 then
  begin
    FormManteTPV.Parent := tabManteTPV;
    FormManteTPV.Align := alClient;
    FormManteTPV.Show();
    swManteTPV := 1;
    DMppal.RutInicioForm;

  end;
end;
//-------------------------------------------------
//BOTONES NO HACE NADA POR QUE ESTAN EN MENUBOTONES
//-------------------------------------------------

procedure TFormMenu.RutCambiarPestanaUSU;
begin

//  FormNuevoUsuario.RutCambiarPestana;
end;

procedure TFormMenu.RutGrabarNuevoUsuario;
begin
  //FormNuevoUsuario.btGrabarClick(nil);
end;

procedure TFormMenu.btExcelClick(Sender: TObject);
begin
  if DMppal.vGrid = nil then exit;

  FormLoadGrid.Show;
end;




//-------------------------------------------------
//Rutinas de apertura de Forms
//-------------------------------------------------


procedure TFormMenu.RutAbrirDevolucion;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;

  if FormMenu.swDevolucion = 0 then
  begin
    FormDevolMenu.Parent := FormMenu.tabDevolucion;
    FormDevolMenu.Align  := alClient;
    FormDevolMenu.Show();
    FormMenu.swDevolucion := 1;
   // DMppal.RutInicioForm;
    FormMenu.swMenuBts := True;
    DMDevolucion.RutAbrirTablas;
  end;

  FormDevolMenu.RutAbrirLista;

  FormMenu.lbFormNombre.Caption := FormMenu.tabDevolucion.Caption;

end;


procedure TFormMenu.RutAbrirRecepcion;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabRecepcion;

  if FormMenu.swRecepcion = 0 then
  begin
    FormMenuRecepcion.Parent := FormMenu.tabRecepcion;
    FormMenuRecepcion.Align  := alClient;
    FormMenuRecepcion.Show();
    FormMenu.swRecepcion := 1;

    DMMenuRecepcion.RutAbrirListaRecepcion(FormListaRecepcion.edDesdeFecha.DateTime, FormListaRecepcion.edHastaFecha.DateTime,1);

  end;
  FormMenuRecepcion.RutAbrirListaRecepcion;

  FormMenu.lbFormNombre.Caption := FormMenu.tabRecepcion.Caption;
end;

procedure TFormMenu.RutAbrirCuadre;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabCuadre;

  if FormMenu.swCuadre = 0 then
  begin
    FormCuadreMenu.Parent := FormMenu.tabCuadre;
    FormCuadreMenu.Align  := alClient;
    FormCuadreMenu.Show();
    FormMenu.swCuadre := 1;
    //control de abrir tablas

  end;

  FormCuadreMenu.RutAbrirLista;

  FormMenu.lbFormNombre.Caption := FormMenu.tabCuadre.Caption;
end;

procedure TFormMenu.RutAbrirArticulos;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;

  if FormMenu.swArticulo = 0 then
  begin
    FormMenuArti.Parent := FormMenu.tabArticulos;
    FormMenuArti.Align  := alClient;
    FormMenuArti.Show();
    FormMenu.swArticulo := 1;

  end;
  FormMenuArti.RutAbrirListaArti;

  FormMenu.lbFormNombre.Caption := FormMenu.tabArticulos.Caption;

end;

procedure TFormMenu.RutAbrirDistribuidora;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabDistribuidora;

  if FormMenu.swDistribuidora = 0 then
  begin
    FormDistribuidoraMenu.Parent := FormMenu.tabDistribuidora;
    FormDistribuidoraMenu.Align  := alClient;
    FormDistribuidoraMenu.Show();
    FormMenu.swDistribuidora := 1;
    FormMenu.swMenuBts := True;
    DMDistribuidora.RutAbrirTablas;
  end;

  FormDistribuidoraMenu.RutAbrirListaDistribuidora;

  FormMenu.lbFormNombre.Caption := FormMenu.tabDistribuidora.Caption;
end;

procedure TFormMenu.RutAbrirClientes;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabCliente;

  if FormMenu.swCliente = 0 then
  begin
    FormCliente.Parent := FormMenu.tabCliente;
    FormCliente.Align  := alClient;
    FormCliente.Show();
    FormMenu.swCliente := 1;
    DMCliente.RutAbrirTablas;

  end;

  FormCliente.RutAbrirListaClie;

  FormMenu.lbFormNombre.Caption := FormMenu.tabCliente.Caption;

end;


procedure TFormMenu.RutAbrirHistoArtiTablas(vId_Aritculo : Integer; vBarras,vAdendum,vDescripcion, vOrigen : String);
begin
  RutAbrirHistoArti;

  FormHistoArti.vStringOrigen := vOrigen;
  DMHistoArti.RutAbrirHistoricoArti(vId_Aritculo,vBarras,vAdendum,vDescripcion,'', '');

end;

procedure TFormMenu.RutAbrirHistoArti;
begin
  pcDetalle.ActivePage := tabHistoArti;

  if swHistoArti = 0 then
  begin
    FormHistoArti.Parent := tabHistoArti;
    FormHistoArti.Align  := alClient;
    FormHistoArti.Show();

    swHistoArti := 1;
  end;

  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;
end;

procedure TFormMenu.RutAbrirConsArti;
begin
  pcDetalle.ActivePage := tabConsArti;

  if swConsArti = 0 then
  begin
    FormConsArti.Parent := tabConsArti;
    FormConsArti.Align  := alClient;
    FormConsArti.Show();
    swConsArti := 1;
  end;

  FormConsArti.RutInicioShow;
  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;
  FormConsArti.vIdEditorial := 0;
  FormConsArti.edBusqueda.SetFocus;
  DMPpal.RutCapturaGrid(FormConsArti.Name, FormConsArti.gridArti, nil, 0, 0, 0);
end;









//-------------------------------------------------
//FIN Rutinas de apertura de Forms
//-------------------------------------------------
initialization
  RegisterAppFormClass(TFormMenu);

end.






