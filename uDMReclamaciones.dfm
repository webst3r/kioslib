object DMReclamaciones: TDMReclamaciones
  OldCreateOrder = False
  Height = 412
  Width = 724
  object sqlProveS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_PROVEEDOR,A.NOMBRE,A.DIRECCION,A.POBLACION,A.PROVIN' +
        'CIA,A.CPOSTAL,A.CPROVINCIA,'
      '   A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL'
      'From FMPROVE A'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 32
    Top = 16
    object sqlProveSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProveSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProveSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlProveSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlProveSPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlProveSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlProveSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlProveSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProveSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlProveSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlProveSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
  end
  object sqlProve1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FMPROVE'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 32
    Top = 68
    object sqlProve1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProve1TPROVEEDOR: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
    object sqlProve1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProve1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProve1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlProve1NOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlProve1DIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlProve1DIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlProve1POBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlProve1PROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlProve1CPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlProve1CPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object sqlProve1CPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlProve1CONTACTO1NOMBRE: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object sqlProve1CONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlProve1CONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlProve1CONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlProve1TELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlProve1TELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlProve1MOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlProve1FAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlProve1EMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlProve1WEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlProve1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlProve1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlProve1BANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object sqlProve1AGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object sqlProve1DC: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object sqlProve1CUENTABANCO: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object sqlProve1TEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlProve1EFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlProve1FRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlProve1DIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object sqlProve1DIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlProve1DIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlProve1DIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlProve1TDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object sqlProve1TPAGO: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object sqlProve1SWTFACTURACION: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object sqlProve1SWBLOQUEO: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object sqlProve1REFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlProve1TRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlProve1RUTA: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object sqlProve1PROVEEDORHOSTING: TIntegerField
      FieldName = 'PROVEEDORHOSTING'
      Origin = 'PROVEEDORHOSTING'
    end
    object sqlProve1NOMBREHOSTING: TStringField
      FieldName = 'NOMBREHOSTING'
      Origin = 'NOMBREHOSTING'
      Size = 40
    end
    object sqlProve1ULTIMAFECHADESCARGA: TDateField
      FieldName = 'ULTIMAFECHADESCARGA'
      Origin = 'ULTIMAFECHADESCARGA'
    end
    object sqlProve1ULTIMAFECHAENVIO: TDateField
      FieldName = 'ULTIMAFECHAENVIO'
      Origin = 'ULTIMAFECHAENVIO'
    end
    object sqlProve1COLUMNASEXCEL: TStringField
      FieldName = 'COLUMNASEXCEL'
      Origin = 'COLUMNASEXCEL'
      Size = 30
    end
    object sqlProve1DESDEFILA: TIntegerField
      FieldName = 'DESDEFILA'
      Origin = 'DESDEFILA'
    end
    object sqlProve1SWRECIBIR: TSmallintField
      FieldName = 'SWRECIBIR'
      Origin = 'SWRECIBIR'
    end
    object sqlProve1SWENVIAR: TSmallintField
      FieldName = 'SWENVIAR'
      Origin = 'SWENVIAR'
    end
    object sqlProve1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlProve1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlProve1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlProve1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlProve1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlProve1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlProve1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlProve1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlProve1MAXPAQUETE: TIntegerField
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
    end
  end
  object sqlReclamaS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select '
      
        'H.cantidad as Cantidad_H, H.cantienalba as Cantienalba_H, H.devu' +
        'eltos as Devueltos_H, H.vendidos as Vendidos_H, H.reclamado as r' +
        'eclamado_H, FP.SUFACTURA, FP.fechafactura, C.DOCTOPROVE, C.docto' +
        'provefecha, '
      
        'R.ID_RECLAMA, R.TRECLAMACION, R.NRECLAMACION, R.FECHA, R.HORA, R' +
        '.ID_HISARTI, R.ID_PROVEEDOR, R.ID_CLIENTE, R.ID_ARTICULO, R.ADEN' +
        'DUM, R.CONCEPTO, R.CANTIDAD, R.CANTIENALBA, R.NALMACEN, R.SWESTA' +
        'DO ,'
      'A.BARRAS,A.DESCRIPCION,P.NOMBRE,P.NIF'
      'from FVRECLAMA R'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = R.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = R.ID_PROVEEDOR)'
      ' left outer join FVHISARTI H on (H.ID_HISARTI = R.ID_HISARTI)'
      ''
      'left join fvfraprove FP on (FP.id_fraprove = R.id_fraprove)'
      'left join fvstocabe  C on (C.idstocabe   = R.idstocabe)'
      ''
      'where R.ID_RECLAMA is not null'
      '')
    Left = 32
    Top = 174
    object sqlReclamaSCANTIDAD_H: TSingleField
      FieldName = 'CANTIDAD_H'
      Origin = 'CANTIDAD_H'
    end
    object sqlReclamaSCANTIENALBA_H: TSingleField
      FieldName = 'CANTIENALBA_H'
      Origin = 'CANTIENALBA_H'
    end
    object sqlReclamaSDEVUELTOS_H: TSingleField
      FieldName = 'DEVUELTOS_H'
      Origin = 'DEVUELTOS_H'
    end
    object sqlReclamaSVENDIDOS_H: TSingleField
      FieldName = 'VENDIDOS_H'
      Origin = 'VENDIDOS_H'
    end
    object sqlReclamaSRECLAMADO_H: TSingleField
      FieldName = 'RECLAMADO_H'
      Origin = 'RECLAMADO_H'
    end
    object sqlReclamaSSUFACTURA: TStringField
      FieldName = 'SUFACTURA'
      Origin = 'SUFACTURA'
    end
    object sqlReclamaSFECHAFACTURA: TDateField
      FieldName = 'FECHAFACTURA'
      Origin = 'FECHAFACTURA'
    end
    object sqlReclamaSDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlReclamaSDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlReclamaSID_RECLAMA: TIntegerField
      FieldName = 'ID_RECLAMA'
      Origin = 'ID_RECLAMA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlReclamaSTRECLAMACION: TSmallintField
      FieldName = 'TRECLAMACION'
      Origin = 'TRECLAMACION'
    end
    object sqlReclamaSNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlReclamaSFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlReclamaSHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlReclamaSID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
    end
    object sqlReclamaSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlReclamaSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlReclamaSID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlReclamaSADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlReclamaSCONCEPTO: TStringField
      FieldName = 'CONCEPTO'
      Origin = 'CONCEPTO'
      Size = 40
    end
    object sqlReclamaSCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlReclamaSCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlReclamaSNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlReclamaSSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlReclamaSBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlReclamaSDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlReclamaSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlReclamaSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlReclamaSDescriEstado: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DescriEstado'
    end
  end
  object sqlReclamaFi: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select Count(*) as ReclamaPendi from FVRECLAMA'
      ' where ID_RECLAMA is not null'
      'and swestado < 8'
      ' and ID_HISARTI = :ID_HISARTI')
    Left = 32
    Top = 227
    ParamData = <
      item
        Name = 'ID_HISARTI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlReclamaFiRECLAMAPENDI: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'RECLAMAPENDI'
      Origin = 'RECLAMAPENDI'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlHisto1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select ID_HISARTI,SWESTADO, SWDEVOLUCION, SWCERRADO, SWMARCA '
      'from FVHISARTI  where ID_HISARTI = :ID_HISARTI')
    Left = 32
    Top = 280
    ParamData = <
      item
        Name = 'ID_HISARTI'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlHisto1ID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisto1SWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlHisto1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHisto1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisto1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
  end
  object sqlReclama1: TFDQuery
    AfterScroll = sqlReclama1AfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select distinct(R.ID_HISARTI)'
      
        ', (select max(RR.FECHA) from FVRECLAMA RR where RR.ID_HISARTI = ' +
        'R.ID_HISARTI) as FechaReclamacion'
      ''
      ', H.SWES, H.FECHA, H.HORA, H.CLAVE, H.ID_ARTICULO, H.ADENDUM,'
      'H.ID_CLIENTE, H.SWTIPOFRA, H.NFACTURA, H.NALBARAN, H.NLINEA,'
      'H.DEVUELTOS, H.VENDIDOS, H.MERMA, H.CANTIDAD, H.CANTIENALBA,'
      'H.PRECIOCOMPRA, H.PRECIOCOSTE, H.PRECIOVENTA, H.SWDTO,'
      'H.TPCDTO, H.TIVA, H.TPCIVA, H.TPCRE, H.IMPOBRUTO, H.IMPODTO,'
      
        '(H.IMPOBASE + H.IMPOBASE2) as ImpoBase, (H.IMPOIVA + H.IMPOIVA2)' +
        ' as ImpoIva,'
      
        '(H.IMPORE + H.IMPORE2) as ImpoRe, (H.IMPOIVA + H.IMPOIVA2 + H.IM' +
        'PORE + H.IMPORE2) as ImpoImptos,'
      
        'H.IMPOTOTLIN, H.ENTRADAS, H.SALIDAS, (H.VALORCOSTE + H.VALORCOST' +
        'E2) as ValorCoste,'
      
        '(H.VALORCOMPRA + H.VALORCOMPRA2) as ValorCompra, (H.VALORVENTA +' +
        ' H.VALORVENTA2) as ValorVenta,'
      
        'H.VALORMOVI, H.ID_PROVEEDOR, H.IDSTOCABE, H.IDDEVOLUCABE, H.TPCC' +
        'OMISION, H.IMPOCOMISION,'
      
        'H.ENCARTE, H.ID_DIARIA, H.PARTIDA, H.TURNO, H.NALMACEN, H.ID_HIS' +
        'ARTI01, H.ID_FRAPROVE,'
      
        'H.NRECLAMACION, H.SWPDTEPAGO, H.SWESTADO, H.SWDEVOLUCION, H.SWCE' +
        'RRADO, H.FECHAAVISO, H.FECHADEVOL,'
      'H.FECHACARGO,H.SWMARCA,H.CARGO,H.ABONO,H.RECLAMADO,'
      ''
      'A.BARRAS,A.DESCRIPCION, A.REFEPROVE'
      ',C.NOMBRE as NomCliente'
      ', P.NOMBRE as NomProveedor'
      ''
      ', E.DOCTOPROVE, E.DOCTOPROVEFECHA'
      ''
      'From FVRECLAMA R'
      ''
      ' left join FVHISARTI H on (R.ID_HISARTI = H.ID_HISARTI)'
      ' left join FMARTICULO A on (R.ID_ARTICULO = A.ID_ARTICULO)'
      ' left join FMCLIENTES C    on (R.ID_CLIENTE = C.ID_CLIENTE)'
      ' left join FMPROVE P   on (R.ID_PROVEEDOR = P.ID_PROVEEDOR)'
      ' left join FVSTOCABE E on (E.IDSTOCABE = R.IDSTOCABE)'
      ''
      ' where ( H.SWESTADO < 99 or H.SWESTADO is null )'
      ' Order by P.NOMBRE, 2, A.descripcion, H.FECHA')
    Left = 32
    Top = 121
    object sqlReclama1ID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
    end
    object sqlReclama1FECHARECLAMACION: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHARECLAMACION'
      Origin = 'FECHARECLAMACION'
      ProviderFlags = []
    end
    object sqlReclama1SWES: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'SWES'
      Origin = 'SWES'
      ProviderFlags = []
      Size = 1
    end
    object sqlReclama1FECHA: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHA'
      Origin = 'FECHA'
      ProviderFlags = []
    end
    object sqlReclama1HORA: TTimeField
      AutoGenerateValue = arDefault
      FieldName = 'HORA'
      Origin = 'HORA'
      ProviderFlags = []
    end
    object sqlReclama1CLAVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      ProviderFlags = []
      Size = 2
    end
    object sqlReclama1ID_ARTICULO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = []
    end
    object sqlReclama1ADENDUM: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      ProviderFlags = []
      Size = 5
    end
    object sqlReclama1ID_CLIENTE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
      ProviderFlags = []
    end
    object sqlReclama1SWTIPOFRA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
      ProviderFlags = []
    end
    object sqlReclama1NFACTURA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
      ProviderFlags = []
    end
    object sqlReclama1NALBARAN: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
      ProviderFlags = []
    end
    object sqlReclama1NLINEA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
      ProviderFlags = []
    end
    object sqlReclama1DEVUELTOS: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
      ProviderFlags = []
    end
    object sqlReclama1VENDIDOS: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
      ProviderFlags = []
    end
    object sqlReclama1MERMA: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'MERMA'
      Origin = 'MERMA'
      ProviderFlags = []
    end
    object sqlReclama1CANTIDAD: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
      ProviderFlags = []
    end
    object sqlReclama1CANTIENALBA: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
      ProviderFlags = []
    end
    object sqlReclama1PRECIOCOMPRA: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
      ProviderFlags = []
    end
    object sqlReclama1PRECIOCOSTE: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      ProviderFlags = []
    end
    object sqlReclama1PRECIOVENTA: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
      ProviderFlags = []
    end
    object sqlReclama1SWDTO: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
      ProviderFlags = []
    end
    object sqlReclama1TPCDTO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
      ProviderFlags = []
    end
    object sqlReclama1TIVA: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'TIVA'
      Origin = 'TIVA'
      ProviderFlags = []
    end
    object sqlReclama1TPCIVA: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
      ProviderFlags = []
    end
    object sqlReclama1TPCRE: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
      ProviderFlags = []
    end
    object sqlReclama1IMPOBRUTO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      ProviderFlags = []
    end
    object sqlReclama1IMPODTO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      ProviderFlags = []
    end
    object sqlReclama1IMPOBASE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      ProviderFlags = []
    end
    object sqlReclama1IMPOIVA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      ProviderFlags = []
    end
    object sqlReclama1IMPORE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      ProviderFlags = []
    end
    object sqlReclama1IMPOIMPTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIMPTOS'
      Origin = 'IMPOIMPTOS'
      ProviderFlags = []
    end
    object sqlReclama1IMPOTOTLIN: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      ProviderFlags = []
    end
    object sqlReclama1ENTRADAS: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
      ProviderFlags = []
    end
    object sqlReclama1SALIDAS: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
      ProviderFlags = []
    end
    object sqlReclama1VALORCOSTE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
      ProviderFlags = []
    end
    object sqlReclama1VALORCOMPRA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
      ProviderFlags = []
    end
    object sqlReclama1VALORVENTA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
      ProviderFlags = []
    end
    object sqlReclama1VALORMOVI: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
      ProviderFlags = []
    end
    object sqlReclama1ID_PROVEEDOR: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = []
    end
    object sqlReclama1IDSTOCABE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = []
    end
    object sqlReclama1IDDEVOLUCABE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
      ProviderFlags = []
    end
    object sqlReclama1TPCCOMISION: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
      ProviderFlags = []
    end
    object sqlReclama1IMPOCOMISION: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
      ProviderFlags = []
    end
    object sqlReclama1ENCARTE: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
      ProviderFlags = []
    end
    object sqlReclama1ID_DIARIA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
      ProviderFlags = []
    end
    object sqlReclama1PARTIDA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
      ProviderFlags = []
    end
    object sqlReclama1TURNO: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'TURNO'
      Origin = 'TURNO'
      ProviderFlags = []
    end
    object sqlReclama1NALMACEN: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
      ProviderFlags = []
    end
    object sqlReclama1ID_HISARTI01: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
      ProviderFlags = []
    end
    object sqlReclama1ID_FRAPROVE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
      ProviderFlags = []
    end
    object sqlReclama1NRECLAMACION: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
      ProviderFlags = []
    end
    object sqlReclama1SWPDTEPAGO: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
      ProviderFlags = []
    end
    object sqlReclama1SWESTADO: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
      ProviderFlags = []
    end
    object sqlReclama1SWDEVOLUCION: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
      ProviderFlags = []
    end
    object sqlReclama1SWCERRADO: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
      ProviderFlags = []
    end
    object sqlReclama1FECHAAVISO: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
      ProviderFlags = []
    end
    object sqlReclama1FECHADEVOL: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
      ProviderFlags = []
    end
    object sqlReclama1FECHACARGO: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
      ProviderFlags = []
    end
    object sqlReclama1SWMARCA: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
      ProviderFlags = []
    end
    object sqlReclama1CARGO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'CARGO'
      Origin = 'CARGO'
      ProviderFlags = []
    end
    object sqlReclama1ABONO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'ABONO'
      Origin = 'ABONO'
      ProviderFlags = []
    end
    object sqlReclama1RECLAMADO: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
      ProviderFlags = []
    end
    object sqlReclama1BARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      Size = 13
    end
    object sqlReclama1DESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      Size = 40
    end
    object sqlReclama1REFEPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
      ProviderFlags = []
    end
    object sqlReclama1NOMCLIENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMCLIENTE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      Size = 40
    end
    object sqlReclama1NOMPROVEEDOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVEEDOR'
      Origin = 'NOMBRE'
      ProviderFlags = []
      Size = 40
    end
    object sqlReclama1DOCTOPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
      ProviderFlags = []
    end
    object sqlReclama1DOCTOPROVEFECHA: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
      ProviderFlags = []
    end
  end
end
