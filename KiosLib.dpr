program KiosLib;

uses
  Forms,
  uDMppal in 'uDMppal.pas' {DMppal: TUniGUIMainModule},
  uMenu in 'uMenu.pas' {FormMenu: TUniForm},
  uConex in '..\Comun\uConex.pas',
  uLogin in 'uLogin.pas' {UniLoginForm2: TUniLoginForm},
  uNativeXLSExport in '..\Comun\uNativeXLSExport.pas',
  uLoadFile in 'uLoadFile.pas' {FormLoadFile: TUniForm},
  uConstVar in '..\Comun\uConstVar.pas',
  ServerModule in 'ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  uDMManteTPV in 'uDMManteTPV.pas' {DMManteTPV: TDataModule},
  uVentasTPV in 'uVentasTPV.pas' {FormVentasTPV: TUniForm},
  uPagoTPV in 'uPagoTPV.pas' {FormPagoTPV: TUniForm},
  uDMMenuArti in 'uDMMenuArti.pas' {DMMenuArti: TDataModule},
  uFichaArti in 'uFichaArti.pas' {FormManteArti: TUniForm},
  uMenuArti in 'uMenuArti.pas' {FormMenuArti: TUniForm},
  uListaArti in 'uListaArti.pas' {FormListaArti: TUniForm},
  uManteTPV in 'uManteTPV.pas' {FormManteTPV: TUniForm},
  uRecepcionLista in 'uRecepcionLista.pas' {FormListaRecepcion: TUniForm},
  uMenuRecepcion in 'uMenuRecepcion.pas' {FormMenuRecepcion: TUniForm},
  uRecepcionAlbaran in 'uRecepcionAlbaran.pas' {FormAlbaranRecepcion: TUniForm},
  uDMRecepcion in 'uDMRecepcion.pas' {DMMenuRecepcion: TDataModule},
  uRecepcionReserva in 'uRecepcionReserva.pas' {FormReservas: TUniForm},
  uDevolMenu in 'uDevolMenu.pas' {FormDevolMenu: TUniForm},
  uCliente in 'uCliente.pas' {FormCliente: TUniForm},
  uClienteLista in 'uClienteLista.pas' {FormClienteLista: TUniForm},
  uClienteFicha in 'uClienteFicha.pas' {FormClienteFicha: TUniForm},
  uClienteReserva in 'uClienteReserva.pas' {FormClienteReserva: TUniForm},
  uDMCliente in 'uDMCliente.pas' {DMCliente: TDataModule},
  uDMDevol in 'uDMDevol.pas' {DMDevolucion: TDataModule},
  uDevolLista in 'uDevolLista.pas' {FormDevolLista: TUniForm},
  uDevolFicha in 'uDevolFicha.pas' {FormDevolFicha: TUniForm},
  uMenuBotones in 'uMenuBotones.pas' {FormBotones: TUniForm},
  ufrFicha in 'ufrFicha.pas' {FormfrFicha: TUniForm},
  frDMDevolucion in 'frDMDevolucion.pas' {frDMDevolucion: TDataModule},
  frDMKIOSLIB in 'frDMKIOSLIB.pas' {frDMKioslib: TDataModule},
  uSeguridad2 in 'uSeguridad2.pas' {UniForm2: TUniForm},
  uDevolNuevo in 'uDevolNuevo.pas' {FormDevolNuevo: TUniForm},
  uMensaje in 'uMensaje.pas' {FormMensaje: TUniForm},
  uNuevoUsuario in 'uNuevoUsuario.pas' {FormNuevoUsuario: TUniForm},
  uDMDistribuidora in 'uDMDistribuidora.pas' {DMDistribuidora: TDataModule},
  uDistribuidoraFicha in 'uDistribuidoraFicha.pas' {FormDistribuidoraFicha: TUniForm},
  uDistribuidoraLista in 'uDistribuidoraLista.pas' {FormDistribuidoraLista: TUniForm},
  uDistribuidoraMenu in 'uDistribuidoraMenu.pas' {FormDistribuidoraMenu: TUniForm},
  uConsArti in 'uConsArti.pas' {FormConsArti: TUniForm},
  uEscogerArti in 'uEscogerArti.pas' {FormEscogerArti: TUniForm},
  uMensajes in 'uMensajes.pas' {FormMensajes: TUniForm},
  uLoadMail in '..\Comun\uLoadMail.pas' {FormLoadMail: TUniForm},
  uLoadGRID in '..\Comun\uLoadGRID.pas' {FormLoadGRID: TUniForm},
  uLoadExportar in '..\Comun\uLoadExportar.pas' {FormLoadExportar: TUniForm},
  uLoadFiltro in '..\Comun\uLoadFiltro.pas' {FormLoadFiltro: TUniForm},
  uHistoArti in 'uHistoArti.pas' {FormHistoArti: TUniForm},
  uDMHistoArti in 'uDMHistoArti.pas' {DMHistoArti: TDataModule},
  uDevolPaquetes in 'uDevolPaquetes.pas' {FormDevolPaquetes: TUniForm},
  uReclamaciones in 'uReclamaciones.pas' {FormReclamaciones: TUniForm},
  uDMReclamaciones in 'uDMReclamaciones.pas' {DMReclamaciones: TDataModule},
  uDMCuadre in 'uDMCuadre.pas' {DMCuadre: TDataModule},
  uCuadreFicha in 'uCuadreFicha.pas' {FormCuadreFicha: TUniForm},
  uCuadreLista in 'uCuadreLista.pas' {FormCuadreLista: TUniForm},
  uCuadreMenu in 'uCuadreMenu.pas' {FormCuadreMenu: TUniForm},
  uUltiCompra in 'uUltiCompra.pas' {FormUltiCompra: TUniForm},
  uDMUltiCompra in 'uDMUltiCompra.pas' {DMUltiCompra: TDataModule},
  uModificarDesc in 'uModificarDesc.pas' {FormEditarDesc: TUniForm},
  uDMModificarDesc in 'uDMModificarDesc.pas' {DMModificarDesc: TDataModule};

{$R *.res}

begin

//http://trabajandocondelphixe.blogspot.com.es/

  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  TUniServerModule.Create(Application);
  Application.Run;
end.
