object DMppal: TDMppal
  OldCreateOrder = False
  OnCreate = UniGUIMainModuleCreate
  Theme = 'uni_win10'
  BrowserOptions = []
  MonitoredKeys.Keys = <>
  OnNewComponent = UniGUIMainModuleNewComponent
  Height = 672
  Width = 974
  object sqlHisArtiDev1: TFDQuery
    Connection = FDConnection1
    Left = 279
    Top = 4
  end
  object sqlUsuario: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      
        'Select U.ID, U.PASS, U.NumeroUsuario, U.Grupo, U.Grupo1, U.Grupo' +
        '2, U.Grupo3, U.Visible1, U.Visible2, U.Visible3, U.Visible4, U.V' +
        'isible5, U.Nombre'
      
        ', U.ControlTelefono, U.FechaPassword, U.TipoPassw, U.Actualizaci' +
        'onAuto'
      ''
      'from FGMUSUARIO U'
      ''
      'WHERE U.ID > '#39#39
      ''
      'order by 1')
    Left = 44
    Top = 619
    object sqlUsuarioID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 40
    end
    object sqlUsuarioPASS: TStringField
      FieldName = 'PASS'
      Origin = 'PASS'
      Size = 40
    end
    object sqlUsuarioNUMEROUSUARIO: TIntegerField
      FieldName = 'NUMEROUSUARIO'
      Origin = 'NUMEROUSUARIO'
    end
    object sqlUsuarioGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlUsuarioGRUPO1: TStringField
      FieldName = 'GRUPO1'
      Origin = 'GRUPO1'
    end
    object sqlUsuarioGRUPO2: TStringField
      FieldName = 'GRUPO2'
      Origin = 'GRUPO2'
    end
    object sqlUsuarioGRUPO3: TStringField
      FieldName = 'GRUPO3'
      Origin = 'GRUPO3'
    end
    object sqlUsuarioVISIBLE1: TSmallintField
      FieldName = 'VISIBLE1'
      Origin = 'VISIBLE1'
    end
    object sqlUsuarioVISIBLE2: TSmallintField
      FieldName = 'VISIBLE2'
      Origin = 'VISIBLE2'
    end
    object sqlUsuarioVISIBLE3: TSmallintField
      FieldName = 'VISIBLE3'
      Origin = 'VISIBLE3'
    end
    object sqlUsuarioVISIBLE4: TSmallintField
      FieldName = 'VISIBLE4'
      Origin = 'VISIBLE4'
    end
    object sqlUsuarioVISIBLE5: TSmallintField
      FieldName = 'VISIBLE5'
      Origin = 'VISIBLE5'
    end
    object sqlUsuarioNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlUsuarioTIPOPASSW: TIntegerField
      FieldName = 'TIPOPASSW'
      Origin = 'TIPOPASSW'
    end
    object sqlUsuarioCONTROLTELEFONO: TStringField
      FieldName = 'CONTROLTELEFONO'
      Origin = 'CONTROLTELEFONO'
      FixedChar = True
      Size = 1
    end
    object sqlUsuarioFECHAPASSWORD: TDateField
      FieldName = 'FECHAPASSWORD'
      Origin = 'FECHAPASSWORD'
    end
    object sqlUsuarioACTUALIZACIONAUTO: TSmallintField
      FieldName = 'ACTUALIZACIONAUTO'
      Origin = 'ACTUALIZACIONAUTO'
    end
  end
  object FDConnection0: TFDConnection
    Params.Strings = (
      'Database=localhost:C:\Dades\Trial\Dat\FAPVP_0.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 25
    Top = 60
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 103
    Top = 4
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 186
    Top = 4
  end
  object sqlHisArtiCom1: TFDQuery
    Connection = FDConnection1
    Left = 359
    Top = 4
  end
  object sqlUsu: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'Select *'
      'from FGMUSUARIO'
      'where ID > '#39#39
      'order by 1')
    Left = 128
    Top = 619
    object sqlUsuID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 40
    end
    object sqlUsuPASS: TStringField
      FieldName = 'PASS'
      Origin = 'PASS'
      Size = 40
    end
    object sqlUsuGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlUsuNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlUsuFORMINICIO: TStringField
      FieldName = 'FORMINICIO'
      Origin = 'FORMINICIO'
      Size = 40
    end
    object sqlUsuDEPARTAMENTO: TIntegerField
      FieldName = 'DEPARTAMENTO'
      Origin = 'DEPARTAMENTO'
    end
    object sqlUsuPATHACTUORIG: TStringField
      FieldName = 'PATHACTUORIG'
      Origin = 'PATHACTUORIG'
      Size = 255
    end
    object sqlUsuPATHACTUDEST: TStringField
      FieldName = 'PATHACTUDEST'
      Origin = 'PATHACTUDEST'
      Size = 255
    end
    object sqlUsuVARIFICACIONAUTO: TSmallintField
      FieldName = 'VARIFICACIONAUTO'
      Origin = 'VARIFICACIONAUTO'
    end
    object sqlUsuMOSTRARVENTANA: TSmallintField
      FieldName = 'MOSTRARVENTANA'
      Origin = 'MOSTRARVENTANA'
    end
    object sqlUsuACTUALIZACIONAUTO: TSmallintField
      FieldName = 'ACTUALIZACIONAUTO'
      Origin = 'ACTUALIZACIONAUTO'
    end
    object sqlUsuFTPUSUARIO: TStringField
      FieldName = 'FTPUSUARIO'
      Origin = 'FTPUSUARIO'
      Size = 50
    end
    object sqlUsuFTPPASSWORD: TStringField
      FieldName = 'FTPPASSWORD'
      Origin = 'FTPPASSWORD'
      Size = 50
    end
    object sqlUsuCONTROLTELEFONO: TStringField
      FieldName = 'CONTROLTELEFONO'
      Origin = 'CONTROLTELEFONO'
      FixedChar = True
      Size = 1
    end
    object sqlUsuENVIARMENSAJE: TStringField
      FieldName = 'ENVIARMENSAJE'
      Origin = 'ENVIARMENSAJE'
      FixedChar = True
      Size = 1
    end
    object sqlUsuREVISARCAMBIOS: TStringField
      FieldName = 'REVISARCAMBIOS'
      Origin = 'REVISARCAMBIOS'
      FixedChar = True
      Size = 1
    end
    object sqlUsuPATHUSUARIODB: TStringField
      FieldName = 'PATHUSUARIODB'
      Origin = 'PATHUSUARIODB'
      Size = 150
    end
    object sqlUsuMENSAJE: TBlobField
      FieldName = 'MENSAJE'
      Origin = 'MENSAJE'
    end
    object sqlUsuGRUPO1: TStringField
      FieldName = 'GRUPO1'
      Origin = 'GRUPO1'
    end
    object sqlUsuGRUPO2: TStringField
      FieldName = 'GRUPO2'
      Origin = 'GRUPO2'
    end
    object sqlUsuGRUPO3: TStringField
      FieldName = 'GRUPO3'
      Origin = 'GRUPO3'
    end
    object sqlUsuGRUPO4: TStringField
      FieldName = 'GRUPO4'
      Origin = 'GRUPO4'
    end
    object sqlUsuGRUPO5: TStringField
      FieldName = 'GRUPO5'
      Origin = 'GRUPO5'
    end
    object sqlUsuVISIBLE1: TSmallintField
      FieldName = 'VISIBLE1'
      Origin = 'VISIBLE1'
    end
    object sqlUsuVISIBLE2: TSmallintField
      FieldName = 'VISIBLE2'
      Origin = 'VISIBLE2'
    end
    object sqlUsuVISIBLE3: TSmallintField
      FieldName = 'VISIBLE3'
      Origin = 'VISIBLE3'
    end
    object sqlUsuVISIBLE4: TSmallintField
      FieldName = 'VISIBLE4'
      Origin = 'VISIBLE4'
    end
    object sqlUsuVISIBLE5: TSmallintField
      FieldName = 'VISIBLE5'
      Origin = 'VISIBLE5'
    end
    object sqlUsuIDIOMA: TIntegerField
      FieldName = 'IDIOMA'
      Origin = 'IDIOMA'
    end
    object sqlUsuSUSTITUIR: TBlobField
      FieldName = 'SUSTITUIR'
      Origin = 'SUSTITUIR'
    end
    object sqlUsuNUMEROUSUARIO: TIntegerField
      FieldName = 'NUMEROUSUARIO'
      Origin = 'NUMEROUSUARIO'
    end
    object sqlUsuREPRESENTANTE: TStringField
      FieldName = 'REPRESENTANTE'
      Origin = 'REPRESENTANTE'
    end
    object sqlUsuRUTAORIGEN: TStringField
      FieldName = 'RUTAORIGEN'
      Origin = 'RUTAORIGEN'
      Size = 255
    end
    object sqlUsuNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlUsuTIPOPASSW: TIntegerField
      FieldName = 'TIPOPASSW'
      Origin = 'TIPOPASSW'
    end
    object sqlUsuFECHAPASSWORD: TDateField
      FieldName = 'FECHAPASSWORD'
      Origin = 'FECHAPASSWORD'
    end
  end
  object sqlUsuAux: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select *'
      'from FGMUSUARIO'
      'where ID > '#39#39
      'order by 1')
    Left = 191
    Top = 139
    object sqlUsuAuxID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 40
    end
    object sqlUsuAuxPASS: TStringField
      FieldName = 'PASS'
      Origin = 'PASS'
      Size = 40
    end
    object sqlUsuAuxGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlUsuAuxNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlUsuAuxFORMINICIO: TStringField
      FieldName = 'FORMINICIO'
      Origin = 'FORMINICIO'
      Size = 40
    end
    object sqlUsuAuxDEPARTAMENTO: TIntegerField
      FieldName = 'DEPARTAMENTO'
      Origin = 'DEPARTAMENTO'
    end
    object sqlUsuAuxPATHACTUORIG: TStringField
      FieldName = 'PATHACTUORIG'
      Origin = 'PATHACTUORIG'
      Size = 255
    end
    object sqlUsuAuxPATHACTUDEST: TStringField
      FieldName = 'PATHACTUDEST'
      Origin = 'PATHACTUDEST'
      Size = 255
    end
    object sqlUsuAuxVARIFICACIONAUTO: TSmallintField
      FieldName = 'VARIFICACIONAUTO'
      Origin = 'VARIFICACIONAUTO'
    end
    object sqlUsuAuxMOSTRARVENTANA: TSmallintField
      FieldName = 'MOSTRARVENTANA'
      Origin = 'MOSTRARVENTANA'
    end
    object sqlUsuAuxACTUALIZACIONAUTO: TSmallintField
      FieldName = 'ACTUALIZACIONAUTO'
      Origin = 'ACTUALIZACIONAUTO'
    end
    object sqlUsuAuxFTPUSUARIO: TStringField
      FieldName = 'FTPUSUARIO'
      Origin = 'FTPUSUARIO'
      Size = 50
    end
    object sqlUsuAuxFTPPASSWORD: TStringField
      FieldName = 'FTPPASSWORD'
      Origin = 'FTPPASSWORD'
      Size = 50
    end
    object sqlUsuAuxCONTROLTELEFONO: TStringField
      FieldName = 'CONTROLTELEFONO'
      Origin = 'CONTROLTELEFONO'
      FixedChar = True
      Size = 1
    end
    object sqlUsuAuxENVIARMENSAJE: TStringField
      FieldName = 'ENVIARMENSAJE'
      Origin = 'ENVIARMENSAJE'
      FixedChar = True
      Size = 1
    end
    object sqlUsuAuxREVISARCAMBIOS: TStringField
      FieldName = 'REVISARCAMBIOS'
      Origin = 'REVISARCAMBIOS'
      FixedChar = True
      Size = 1
    end
    object sqlUsuAuxPATHUSUARIODB: TStringField
      FieldName = 'PATHUSUARIODB'
      Origin = 'PATHUSUARIODB'
      Size = 150
    end
    object sqlUsuAuxMENSAJE: TBlobField
      FieldName = 'MENSAJE'
      Origin = 'MENSAJE'
    end
    object sqlUsuAuxGRUPO1: TStringField
      FieldName = 'GRUPO1'
      Origin = 'GRUPO1'
    end
    object sqlUsuAuxGRUPO2: TStringField
      FieldName = 'GRUPO2'
      Origin = 'GRUPO2'
    end
    object sqlUsuAuxGRUPO3: TStringField
      FieldName = 'GRUPO3'
      Origin = 'GRUPO3'
    end
    object sqlUsuAuxGRUPO4: TStringField
      FieldName = 'GRUPO4'
      Origin = 'GRUPO4'
    end
    object sqlUsuAuxGRUPO5: TStringField
      FieldName = 'GRUPO5'
      Origin = 'GRUPO5'
    end
    object sqlUsuAuxVISIBLE1: TSmallintField
      FieldName = 'VISIBLE1'
      Origin = 'VISIBLE1'
    end
    object sqlUsuAuxVISIBLE2: TSmallintField
      FieldName = 'VISIBLE2'
      Origin = 'VISIBLE2'
    end
    object sqlUsuAuxVISIBLE3: TSmallintField
      FieldName = 'VISIBLE3'
      Origin = 'VISIBLE3'
    end
    object sqlUsuAuxVISIBLE4: TSmallintField
      FieldName = 'VISIBLE4'
      Origin = 'VISIBLE4'
    end
    object sqlUsuAuxVISIBLE5: TSmallintField
      FieldName = 'VISIBLE5'
      Origin = 'VISIBLE5'
    end
    object sqlUsuAuxIDIOMA: TIntegerField
      FieldName = 'IDIOMA'
      Origin = 'IDIOMA'
    end
    object sqlUsuAuxSUSTITUIR: TBlobField
      FieldName = 'SUSTITUIR'
      Origin = 'SUSTITUIR'
    end
    object sqlUsuAuxNUMEROUSUARIO: TIntegerField
      FieldName = 'NUMEROUSUARIO'
      Origin = 'NUMEROUSUARIO'
    end
    object sqlUsuAuxREPRESENTANTE: TStringField
      FieldName = 'REPRESENTANTE'
      Origin = 'REPRESENTANTE'
    end
  end
  object sqlTipoNOSIOtros: TFDQuery
    Filtered = True
    Connection = FDConnection1
    SQL.Strings = (
      'Select T.*'
      'From FDTIPOS T'
      
        'where T.CODIGO = (Select FM.CODIGO from FMTIPOS FM where FM.DESC' +
        'RIPCION = '#39'TIPONOSIOTROS'#39') '
      'and ((T.Orden2 = -1) or (T.Orden2 = 7))'
      ''
      'order by 1')
    Left = 711
    Top = 12
  end
  object sqlArticulos: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'select FA.ID_Articulo, FA.barras, FA.adendum, FA.descripcion, FA' +
        '.precio1, FA.preciocoste, FA.editorial, FA.Refeprove'
      ', FP.nombre as NombreEditorial'
      ''
      'from fmarticulo FA'
      'left join fmprove FP on FP.id_proveedor = FA.editorial'
      ''
      'where FA.editorial >= -1'
      ''
      'order by 1,2')
    Left = 55
    Top = 235
    object sqlArticulosBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArticulosADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArticulosDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArticulosPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
      DisplayFormat = '0.00'
    end
    object sqlArticulosPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = '0.00'
    end
    object sqlArticulosEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArticulosREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArticulosNOMBREEDITORIAL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBREEDITORIAL'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlArticulosID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object sqlArti1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select A.*'
      ', P.NOMBRE as NomProve'
      ' from FMARTICULO A'
      ' left outer join FMPROVE P on (P.id_proveedor = A.editorial)'
      'WHERE A.BARRAS = '#39#39
      'and A.ADENDUM = '#39#39
      'and A.EDITORIAL = 0'
      'Order by A.BARRAS,A.ADENDUM,A.EDITORIAL')
    Left = 391
    Top = 107
    object sqlArti1ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArti1TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArti1BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArti1ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArti1FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArti1NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArti1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArti1DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArti1IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArti1ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArti1EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArti1PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArti1PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArti1PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArti1PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArti1PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArti1PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArti1PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArti1TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArti1TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArti1TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArti1TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArti1TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArti1TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArti1TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArti1PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArti1PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArti1PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArti1PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArti1MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArti1MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArti1MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArti1EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArti1REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArti1PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArti1CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArti1TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArti1TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArti1SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArti1SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArti1SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArti1SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArti1SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArti1SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArti1TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArti1TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArti1TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArti1TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArti1AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArti1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArti1STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArti1STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArti1FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArti1FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArti1ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArti1STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArti1STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArti1STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArti1REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArti1UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArti1UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArti1IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArti1COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArti1GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArti1SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArti1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArti1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArti1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArti1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlArti1NOMPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object SP_GEN_HISARTI1: TFDStoredProc
    Connection = FDConnection0
    StoredProcName = 'SP_GEN_HISARTI'
    Left = 444
    Top = 3
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=localhost:C:\Dades\Trial\Dat\FAPVP_0.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    Connected = True
    LoginPrompt = False
    Left = 25
    Top = 4
  end
  object sqlHisArtiDev0: TFDQuery
    Connection = FDConnection0
    Left = 271
    Top = 52
  end
  object sqlArti0: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'Select A.*'
      ', P.NOMBRE as NomProve'
      ' from FMARTICULO A'
      ' left outer join FMPROVE P on (P.id_proveedor = A.editorial)'
      'WHERE A.BARRAS = '#39#39
      'and A.ADENDUM = '#39#39
      'and A.EDITORIAL = 0'
      'Order by A.BARRAS,A.ADENDUM,A.EDITORIAL')
    Left = 543
    Top = 107
    object sqlArti0ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArti0TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArti0BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArti0ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArti0FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArti0NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArti0DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArti0DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArti0IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArti0ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArti0EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArti0PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArti0PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArti0PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArti0PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArti0PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArti0PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArti0PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArti0TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArti0TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArti0TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArti0TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArti0TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArti0TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArti0TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArti0PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArti0PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArti0PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArti0PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArti0MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArti0MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArti0MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArti0EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArti0REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArti0PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArti0CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArti0TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArti0TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArti0SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArti0SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArti0SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArti0SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArti0SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArti0SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArti0TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArti0TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArti0TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArti0TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArti0AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArti0OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArti0STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArti0STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArti0FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArti0FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArti0ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArti0STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArti0STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArti0STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArti0REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArti0UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArti0UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArti0IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArti0COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArti0GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArti0SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArti0SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArti0FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArti0FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArti0FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlArti0NOMPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object SP_GEN_HISARTI0: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'SP_GEN_HISARTI'
    Left = 444
    Top = 51
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlHisArtiCom0: TFDQuery
    Connection = FDConnection0
    Left = 351
    Top = 52
  end
  object sqlUpdate: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'Select *'
      'from FGMUSUARIO'
      'where ID > '#39#39
      'order by 1')
    Left = 103
    Top = 59
  end
  object SP_GEN_ARTICULO: TFDStoredProc
    Connection = FDConnection0
    StoredProcName = 'SP_GEN_ARTICULO'
    Left = 468
    Top = 107
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlUpdate2: TFDQuery
    Connection = FDConnection0
    Left = 176
    Top = 72
  end
  object sqlMensaje: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'select * '
      'from FMENSAJE'
      'where Fecha = '#39'01/01/2018'#39
      'order by fecha, hora')
    Left = 44
    Top = 560
    object sqlMensajeID_MENSAJE: TIntegerField
      FieldName = 'ID_MENSAJE'
      Origin = 'ID_MENSAJE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlMensajeFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlMensajeHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlMensajeMENSAJE: TMemoField
      FieldName = 'MENSAJE'
      Origin = 'MENSAJE'
      BlobType = ftMemo
    end
    object sqlMensajeTPV: TIntegerField
      FieldName = 'TPV'
      Origin = 'TPV'
    end
  end
  object SP_GEN_MENSAJE_ID: TFDStoredProc
    Connection = FDConnection0
    StoredProcName = 'SP_GEN_MENSAJE_ID'
    Left = 44
    Top = 507
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlMensajesGeneral: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'select * '
      'from FMENSAJE'
      'where Fecha = '#39'01/01/2018'#39
      'order by fecha, hora')
    Left = 128
    Top = 560
    object sqlMensajesGeneralID_MENSAJE: TIntegerField
      FieldName = 'ID_MENSAJE'
      Origin = 'ID_MENSAJE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlMensajesGeneralFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlMensajesGeneralHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
      DisplayFormat = 'hh:mm'
    end
    object sqlMensajesGeneralMENSAJE: TMemoField
      FieldName = 'MENSAJE'
      Origin = 'MENSAJE'
      BlobType = ftMemo
    end
    object sqlMensajesGeneralTPV: TIntegerField
      FieldName = 'TPV'
      Origin = 'TPV'
    end
  end
  object sqlProve: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select P.id_proveedor, P.nombre, P.REFEPROVEEDOR, P.ruta, P.tdto'
      'from FMPROVE P')
    Left = 279
    Top = 611
    object sqlProveID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProveNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProveREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlProveRUTA: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object sqlProveTDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
  end
  object sqlProveedor2: TFDQuery
    Connection = FDConnection1
    Left = 487
    Top = 188
  end
  object sqlProveedor0: TFDQuery
    Connection = FDConnection0
    Left = 407
    Top = 244
  end
  object sqlHisArtiProve: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from FVSTOCABE'
      'where IDSTOCABE = 0')
    Left = 271
    Top = 100
    object sqlHisArtiProveIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisArtiProveSWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlHisArtiProveNDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlHisArtiProveFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHisArtiProveNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHisArtiProveNALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlHisArtiProveNALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlHisArtiProveID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHisArtiProveID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlHisArtiProveFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlHisArtiProveSWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlHisArtiProveDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlHisArtiProveDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlHisArtiProveFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHisArtiProveFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHisArtiProveNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlHisArtiProveNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlHisArtiProveMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlHisArtiProveOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlHisArtiProvePAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHisArtiProveSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHisArtiProveSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisArtiProveSWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlHisArtiProveDEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlHisArtiProveDEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlHisArtiProveDEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlHisArtiProveDEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlHisArtiProveID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHisArtiProveSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHisArtiProveSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHisArtiProveSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHisArtiProveSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHisArtiProveSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHisArtiProveSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHisArtiProveSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHisArtiProveFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlHisArtiProveFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlHisArtiProveHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlHisArtiProveFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlHisArtiProveHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlHisArtiProveUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlHisArtiProveNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlArticulos2: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select FA.*'
      'from fmarticulo FA'
      'where FA.id_articulo >= -1'
      'order by 1,2')
    Left = 127
    Top = 243
    object IntegerField23: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IntegerField24: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object StringField30: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object StringField31: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object DateField5: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object IntegerField25: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object StringField32: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object StringField33: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object StringField34: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object StringField35: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object IntegerField26: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object SingleField30: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object SingleField31: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object SingleField32: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object SingleField33: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object SingleField34: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object SingleField35: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object SingleField36: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object SingleField37: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object SingleField38: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object SingleField39: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object SingleField40: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object SingleField41: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object SingleField42: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object SingleField43: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object SingleField44: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object SingleField45: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object SingleField46: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object SingleField47: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object SingleField48: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object SingleField49: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object SingleField50: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object IntegerField27: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object StringField36: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object IntegerField28: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object IntegerField29: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object IntegerField30: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object IntegerField31: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object SmallintField15: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object SmallintField16: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object SmallintField17: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object SmallintField18: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object SmallintField19: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object IntegerField32: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object StringField37: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object IntegerField33: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object IntegerField34: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object IntegerField35: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object IntegerField36: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object MemoField2: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object SingleField51: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object SingleField52: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object StringField38: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object IntegerField37: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object IntegerField38: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object SingleField53: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object SingleField54: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object SingleField55: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object SingleField56: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object SingleField57: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object SingleField58: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object StringField39: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object StringField40: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object IntegerField39: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object IntegerField40: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object SmallintField20: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object DateField6: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object DateField7: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object DateField8: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
  end
  object sqlTBMem: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 536
    Top = 472
    object sqlTBMemCampo: TStringField
      FieldName = 'Campo'
      Size = 50
    end
  end
  object sqlFGTIVA: TFDQuery
    Connection = FDConnection0
    SQL.Strings = (
      'select * from FGTIVA'
      'where TIVA = :TIVA')
    Left = 464
    Top = 328
    ParamData = <
      item
        Name = 'TIVA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlFGTIVATIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFGTIVATIPOIVA: TStringField
      FieldName = 'TIPOIVA'
      Origin = 'TIPOIVA'
      Size = 35
    end
    object sqlFGTIVATPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlFGTIVACTACONTABLE: TStringField
      FieldName = 'CTACONTABLE'
      Origin = 'CTACONTABLE'
      Size = 12
    end
    object sqlFGTIVATPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlFGTIVACTARECARGO: TStringField
      FieldName = 'CTARECARGO'
      Origin = 'CTARECARGO'
      Size = 12
    end
    object sqlFGTIVAFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlFGTIVAFECHAUTI: TDateField
      FieldName = 'FECHAUTI'
      Origin = 'FECHAUTI'
    end
    object sqlFGTIVAHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlFGTIVAUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlFGTIVANOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlFGTIVAVALDESDE: TDateField
      FieldName = 'VALDESDE'
      Origin = 'VALDESDE'
    end
    object sqlFGTIVAVALHASTA: TDateField
      FieldName = 'VALHASTA'
      Origin = 'VALHASTA'
    end
  end
  object ImgListPrincipal: TUniNativeImageList
    Left = 790
    Top = 544
    Images = {
      2A000000FFFFFF1F047104000089504E470D0A1A0A0000000D49484452000000
      16000000160806000000C4B46C3B0000000467414D410000AFC837058AE90000
      00097048597300000EC300000EC301C76FA8640000001974455874536F667477
      6172650041646F626520496D616765526561647971C9653C0000002174455874
      4372656174696F6E2054696D6500323030373A31323A32322031383A31383A33
      38CF77EBA1000003C149444154484BB5954B6C1B551486FF99B1C7F6B8695C9A
      882A2E1241294D858B2ACAA212A1A53C2221350B90A8541605762C40A2BCAAB2
      E8AE0810A20B844A592015812A8148449C05122291200D55D347D2F491B4C831
      8D129AA4A171E2DA9E873DFC673C76126848361CFBCFB973E7CE77FF7BEE1D47
      715D17FF4754C1358F1FF8FA99D627F7D5AE092B4EB1B4E26CD5016CB80A100C
      68CAE0D0B5CCC099FE36A86A2F042C7AFAE0C982C9C66AA3B44845E960740E4E
      BB7ACB3B1DC6CE77A1FAF3221A092B41BFBD9A9085964A40B10838944454D7A0
      E97A805A0017B97CFFFEB221CB2F1266538E2F9B9DB6FFA0C50E2E413E0BE095
      42AAEE896D71EB39AEF44BA654D214D65BB42A309F2D4BFEF8B93241E5A6A43B
      56B934B28215C11E64B148AC4045022AD8409E7922CB7298BCE604FF09E6734B
      209EFC6B99A4E0D02555E020A9B7F48365102D0B5EE2D217BF6528210235E9B2
      A451325EC0D2F0E3AEE0C5CEBCEC4B9E1367392EDD221401E6BC89226F8A3CC7
      FEE8BB822B833C8913BFEDC8B2654094AB3580ECF511CC1D7D01F95F3B803027
      094568D52821105DC631552D01DB3291382C70F4DCD828468F7D84F9541A85D4
      35DCB735053D3F804CC64543EF09BC39D919FBFEDCA7FF060BD4930F9697C1AB
      27A5E8C0544F12F1FA8B30BB8F61F6F208820D1B61CF71C21347B077F711BCFD
      F070735DF6A61CE972882B712730EF2C0A90D92250EAEAB9E7755DEB5ECCCCC5
      D1D8528B70EA5BF687A05D4FA279571217461FC2FBBF349D07B452153C3ECF9A
      1122BB2BCB16C9EBEB01995DEEFEADA12B981D388DA9F118863EEFC2F6C34FA0
      A6F97E6C3ED404F35C0E7D5F35418DC712F5B578A9FAB39978BDBDD0F3C9F3A1
      1A9E41D9750F28EE7DB0ED5A183DFE1A363F722FC2EBA270EF4C61DD965AA8F5
      310EBA8AECF9CBD0C333D02339DC6C77171CCBB9962904F24F28CD43D1028826
      DA303E19C0C8A90CD21DA7B84116DCC25FBC69C21ACAE0872FB6A1FDE30D39F3
      BBE9CE2A58A0625E204BA092E5DA5651B7BB0D46EB2138AE81075E7C0ABF7FD9
      47A717317EFC02623B0C38EB5524BBD626ADE1FC7355B0AA696A84BB1EA50C9E
      C9A8886755B211022294C1639AFBE91B6C4F4CC39E7091BE9180EB58B835FB18
      FEE86DC6BE977FC3C1ADFD3B27B350F9EE94E3CF1BA999CF7AA637DC43BAE9C8
      7F153A657FE5D8795B41F8FCEC46F475FF8889C4AB883F781BBB825731166F40
      37F663D3076FA031FDF399357CA4BA79CAB6575A105CFF1602811029C25C08D9
      0009191AD4A1D80EDCF05AE7D1C9FEBAAE3D67777C78BA317D14CF5E42EEF6F0
      E1C193EFED298E95FE06DE0E5D9824D4F3400000000049454E44AE426082FFFF
      FF1F04EE04000089504E470D0A1A0A0000000D49484452000000160000001608
      06000000C4B46C3B0000000467414D410000AFC837058AE90000000970485973
      00000EC300000EC301C76FA8640000001974455874536F667477617265004164
      6F626520496D616765526561647971C9653C0000002174455874437265617469
      6F6E2054696D6500323030373A31323A32322031393A35313A3530B90926EB00
      00043E49444154484BB5945D4C1C5514C7FF333B332CBBECC202C2B250055B40
      A06C432D149AA6E5C3104A3160D4C446248DA989ED8331697CEB0389F5414D8C
      314DF451EB8B8AD6168D35A6428BA1986A5B84200B08EE16B0CB0AFBCD02CBEE
      5CCF1D76494AA90F35FE9333737227E777CE3D73EF111863F83FA4811B1A1A20
      08024451844E279BB2B3323A9E282D6AB75AF3F6489292E1F32D79271C8EDF2E
      FFD0FF8DDBEDB928CB522812892410DB6B13ACAA8C83ED8D87EBDE3F78A8B9C1
      BB9E0FDF720A226B005B0F41563DD0C7A6D0DB7BE1DAB7DF5D391D8FC76E2618
      DB4A03D7D7D74365ACF2D8F3ED5FEE3DF842C9E882059916193B6D3A28B28060
      4485E34E14AE1917F2F03BFA2E9F9FF9ACE7D2314A7923C1B94F227F44A35173
      C3E103E70E3CD559F2EBA2152565A9A82896A0A40A88EB80D43411F60A3DAAEB
      4BE194F7A1AEF1D9C7F7D7547D48A1393C7E3B69E0CC4C4BC7D1D68E4363C11C
      64DB44ACABC0DD00B0B80CF8A8954BF45EF003F1385052B5036EA90C4D8D8D7B
      A9D72F6B946DA481ED15A5EDFA9C727862D0A05E0205A8B7FED50D0B9085A3B4
      4E49E857206D47118CD925D859F86807856771C65669E0A2C242FB328CD44F02
      103040101F07723F613EB2655A1709FC98CD02D16C85CD662BA4F062CED82A0D
      9CA2375854F2724D8059A1050158A3EA3988DB2AF9B484F41420DB408D4D1720
      2B7A3A9A522A2D3FC2195BA581FD7EFF92495161A1200ECF35D2FEC82C14C62D
      9BFCDC34229071DF2031B058187496292528F5FDD2C00EC7C4A8B8BA043DB522
      93E03CD84A102B25E1A641138932E87B28E045C8E3C4ECDC9C9BC2E73963AB34
      F08F57077AE71DB7601055DA1EFD1C6A8789B66DD2D3F6C9B86FA0A44632BA9C
      989F1AC7ECE430EEFCE51EA27017676C950676BA5C177B7A3E1F501726C1E24C
      AB5C03497486C9B86FA26A53A9DAF1B129F8666E63E8FAF5C51455BD40BD0FF1
      FE278D03F95BD7DDDD8DB7CE9E5D9B713A8785F86ACBAE82DC0C95FE89A42850
      14513B29BCCA503888B1E111FC71A30F95F67254EC2A96BAFCF3B9F1598F6F0C
      9822D626988F35ED4AF301C46530186ADA5A8F9C6B6939529D57580CD9980E3E
      FC964301B8E79C9818BD8DC1C19FFE6E6B6E497FEEC4EB4A71D6005C67CE845F
      F960FC543FF069124C57E15E704239F9F9F9C77757943D53905F50242B8ADEEB
      F3C6FE9C9EF18C4D4C0E46C2E1AF3FA9DB73BAF5A3E38DE9D235C83619136F4E
      ADBEFADEF0C9ABC0C7C98AA922EDB52949A2A66E288B12D6CAB2FC34F9CD64BB
      C9CC3CF0A8A26B1B693204D93B66C6AE543316EA64236F5485EA8097E8B3D692
      6DC17C2E3F48C9BDD512E466B5B8C2DEB530D6B78FB1E08BEC97D7EC2BFB2174
      3D3438F9B506E81AAA15C2ECED8C0D78B8937D7FA27CC94C791F4CF81725ABA6
      617CFEE4CFEC54FF17C115DC9AA6B3380D5589596808963F54C57487EE512555
      FED593D2DD4B4DC668B524F4D152D57F02733F593DDD9D3ABAA47C3E570110FF
      01B39F9A0D634B61870000000049454E44AE426082FFFFFF1F04720100008950
      4E470D0A1A0A0000000D4948445200000018000000180803000000D7A9CDCA00
      00000373424954080808DBE14FE00000000970485973000005BA000005BA011B
      ED8DC90000001974455874536F667477617265007777772E696E6B7363617065
      2E6F72679BEE3C1A00000054504C5445FFFFFFBF8080C39B6EC49971C69C73C8
      9E74CEA37AC29971D1A77ED8AC84A98258AB845AAC855BAE875EAF885FBF9870
      C89E75CDA67FCEA780D2AB85D4AD86D6AF8AD8B18BD9AD85DAAE86DFBB99E6C0
      A0F4D5BD72805B4B0000000A74524E530004335A81ADE5F0F5FE985B3F600000
      007A4944415428CFDDD15B1645300C85E1ED4E5B772287F9CF5358582D19C1F9
      9F9A7C6F2900240B4BEB26ADC76B497016951C0297D10919BF81B3631F575FA8
      62819CBFC03990B2069CA2E8AEE69F34DF530173D792D43EA30713D1A441E386
      C1350A18E39C37F810F48F60AD0AB5ED7B5B6BD71D8946F5ECE17FEC3CDD1D9D
      D21902360000000049454E44AE426082FFFFFF1F04FE05000089504E470D0A1A
      0A0000000D4948445200000016000000160806000000C4B46C3B000000046741
      4D410000AFC837058AE9000000097048597300000EC300000EC301C76FA86400
      00001974455874536F6674776172650041646F626520496D6167655265616479
      71C9653C00000021744558744372656174696F6E2054696D6500323030373A31
      323A32322031383A31383A3338CF77EBA10000054E49444154484BB5556B6C93
      65147EBE4BBFDED6956D6DC72E5DD99CE0D8A0DB404490C401CA2561D10831A2
      D1480C897F88F1878A86191308414524313143C5080ACB0C4A201AD4C9655C05
      868C312E5BD98575DDD6B56BD77BBFABA77409E26F799293F3BEDFE579DFF79C
      E79C97D1340D0F03EC94FFDFC1B0ECC7DB9C4EF39B6EF7A3A206456555451278
      8067C0308A22F12C54966118288ACA319A64E075FCA58EF6F68B0387B64F635D
      7D61B60FE54A08EBB5088689F017B230196FB7A75EB595E7E6B9173B30DF5D85
      5CAB005074F47A321A1AC8EB74641C1D8FCE673603D1BB0D33B7BD5BB5A8E5D8
      FECD5053475406500141025C32D09BD93167B52CDE682C2CB495D458E11B1C86
      1CE361305A904800892810279F4A0232FDA528403A0D14BA04AC5C53672F122A
      1BAF7747B974F28AE7E992BC4F9FA8367DE4F526853EE02CA729EEFA3C5B71DD
      0C77052A6B74181A1A45623C8DDC9C5C8A1307898824314B2EA6684CF3242DA6
      D02E17369609B651434355ECA7758DCD5F34342CB3581DBD97971CF7E206A7AA
      06BBC3E16E3494CD464DA58A69C516886C02FEFE008C9C0926B31E1C85201386
      8C80543AB3427E640CE8DCFB035C6A135EDAB5D66AAD70A17BA21A557A0F7FE6
      AC4FC7CBAAFEEF6478249A8CB116CE500029EA87A3D40C314F84B7FB0646EE9A
      C0312C185503CF3130E65AC1A4F4089DD88A79EE362CDABC1EBC36043EF83A72
      C435F8EAA41DFD12A294924A59CF1B9EB73B673A6A1656C0CCC531D2DB079E55
      60C897C19A6330E4A590667DC871E9E0F57480BDF0019E5D3B88F99B36809D3C
      4E816F41B85FC3BE0FAFC7779EEC6D1DD6B08B84150F4663D12E361EA84EC429
      AD654548C71224701526930939397AE4582C30E45A3078FA1CB8D1DD58F1FE23
      98FEF853C07033E5FF3AFA4F9BB1B549F57F3F90FA4684B6871E0E700CF39826
      49E6625BBE6D95734E156656EAA1C82A26FD6388454250441105F622741ED803
      B6BB09CFBDB31AF9A57980E7336062106D7B2DD8B445EC3C124C6F27D16456F2
      67E4C673942045099C8B854792E1F1945196F5C8B33BEEA59F8A0445E5A5B8D8
      7210566F335E687A03ACF70270B31511FA7DDF775679475BEC772F949DC4D54E
      4632CE8255949B94ED9EDE68283614199B409C2445818746C992A41492A10002
      B7FE42E3BA97C15E390874B5A2E71470E8D7E5B8667B5BF282F99A3EA740DF27
      CD8065D93A727591783C70558D8610A1A230080C0A4B8A30AB66068EB5EE47ED
      8A05D03BAB11387715877F060EDC7E11F60D3BB0FC95D57AA3C161CB523D08DA
      DC2C723264395D523B67FE4A67B5132576862A4D41E7F94BD8B2F92DA4A43002
      1E1FCEF44C4797B200735FDB88FA79B3E1B05A98A3477EF34F4C0C1DCDD2DD07
      A9A297BA8D8CB464391F1EF3CBC988CA6BB49E255780DFEFC3B2A5CFA0BCBC06
      8CD385FA557350515986028B9EFA880E463B505B5B5FEBF19CCD27AE6096720A
      1CD740F6348D96E62F5AB0B3ABF9705CEB086A5A7F44D33CA1B4E617352D2869
      DA784AD612D4BCFF8BCF771F9CA08892F61E0407A6989A1909458BA55455B7F4
      C9794B664F683A24D32A82932CBC7E05BE7115BE00833B3E053DC30A7A4715DC
      1E563130CA4189F1C6B63F5A3A443171698AF31E3868D451B4CC29C6204AD6CA
      5915B5CBB4A00943B7A218BF23223820217857C50459981AEEE42883C80883B0
      8F413A4465443A3F7EE2DB6BC964F4CF2C6516D4C28BA6860A85A470AED3B9E4
      475B81B59465759AA013189D2080E70508829ED1E9F4E4059212431704C7087A
      81ED1FB89C6C3FF5C97BAA2A7D3945740F0CC7B9A6861928542CD17AAA8E2A7A
      9579F0AFAB8BF640B7CAD42433A596C4B0AA9AA4862A658A6328FB228B877499
      02FF001F1355DA9BB19F790000000049454E44AE426082FFFFFF1F0499110000
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000097048597300000B1300000B1301009A9C1800000A4F694343505068
      6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
      DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
      114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
      7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
      11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
      07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
      801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
      7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
      450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
      305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
      99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
      99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
      BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
      EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
      E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
      814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
      582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
      00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
      6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
      44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
      801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
      3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
      21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
      46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
      74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
      6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
      128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
      51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
      37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
      DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
      E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
      B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
      DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
      197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
      0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
      E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
      C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
      D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
      744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
      AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
      53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
      4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
      35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
      8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
      4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
      6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
      A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
      67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
      DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
      1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
      9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
      D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
      EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
      7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
      F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
      0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
      D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
      C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
      17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
      8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
      91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
      31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
      C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
      2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
      B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
      6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
      1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
      66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
      CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
      61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
      FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
      D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
      F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
      625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
      F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
      E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
      9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
      DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
      D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
      058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
      AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
      1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
      D0A7FB93199393FF040398F3FC63332DDB0000000467414D410000B18E7CFB51
      93000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000006B44944415478DAEC9759685CD719C77F
      E7DE3B77F619CD48A3C592B5D8B1BC86D8C4692795EC286E6843DC2D20521C28
      A50BB4A1A524831F5A485B91420B858ABA49082D144ADD906054A26292BA7D30
      2E76B0ECD869BC91D8B5246B19692459A39166BBFBE9836583D3C4B68A5B3FB4
      E7E1DE03F770BEDFF97FE75BAE9052722F87C23D1EFF07B8E700DA7FC3C8B12F
      25BF2D042F2151257CAF7B30FFE2F56F622551309049EF06560323C06560BCB7
      7FC8B985E1664555FEA428BE07D37B5F677EE8CF7C70E4379E94F8BB07F3CE8A
      1418C8A41F5514E58D9ADA065FB55CC4A89691523A0399F4F832D015601478BF
      B77FE88D179FDEF9EA6CBCE7E9A7944BACBBAF0773669AA8D009476A954A39BF
      0578EF8E1518C8A43701C7366FDF9958D5D60980941E46A544A554A45A5EC2A8
      94A8968B2C65473875B5DE29A8712DBD75031B9301D64D4C00309D3DC354F62C
      9A5F74DB8677BC7B30EFDD166020936E04863A366C6D5BBBE9418A85796CCB20
      148DE30F841142DC589B3D7480C285D398ED1D2C7CF3F754DE3BC7CBBF7E85E7
      1FBE8F862B87B06DC3F03CF968F7607EE88EEEC040261D068E34AE5EBB7DF3F6
      1E3CCF65666218293D1CE79AEB354DC3CDCF517AFBAFA8AACD8627E304A21E07
      CEF7F0F2C1515AB76CA6C1A8D25B3D7258AD2E7EA67B30EFAE240ABE1EADA9DD
      BE71DB0E2CB34A7E368B69549818BE4028122796ACA37AE604EECC140D9B7DB4
      3D5687359BC7C855D925FEC8C12D5FA1A39A773BCDD12F3EF2DAE89BFF4E18EE
      696E5FCF627E8662611ECBAC901DBD88E3D83FD166A71FB24E5F7DDC1F86FBBF
      9640556DAA63534857327E4A672927F846F26F1413D11FF4F69F78F3E30C7CAC
      0B0632E90EA128235D8FEF617E76927C6E8299C99105CDB1BE9B9A1CFD0E9274
      4B3AA0AC7A288A393B8F5B35294E0BC6DFD5D122354CD6AFBA76593DD7969EF7
      706FFFD0E9952AF0E5646A15AAE6A3B25460363B4A6D76EC57BA65FC2E9810EA
      C6A7EA14AC0AD5F1696C0B264EFA291554663B3A4108A2357534B57562564ABE
      D1F74FBF369049F7F4F60F4DAD04604FAA790D8BF9394A73D3A4C6874B9A6BFF
      B07D5748A9DB14C49C99C3336DF25714B2E77DB88D2D5C6D8F904C3551DBD846
      385A4334910224D572715D6EFCD289814C7A776FFFD0D9DBBA602093DEA428CA
      85CE6D3B289C3C0CE3C3C41A15B9A137299C6211A750C4AEC2E8DB9A53B6FC62
      A9A5430DD53793AC6F211889134BD6A3AA379F6DE2F259C62E9D5BBC147BF268
      2EFCC93DFB325DA55B013CA159D6FEE4CC6452930EEB9E88105FAD61CEE4310D
      C9C80731E68A31CC40C8929AA6476A6A6968ED2496A827188E21948FAE71C726
      C29C28ACC5F1C482849F4BCFDBF7912E681ABDF8394F5513566B0A3719E29DCB
      02E51F0E4246295901F460849A966612A9663D966A42557DD7E414D71E12F8F0
      C14C57E554A19D48433B8AAA242A8B0BCF1BC5C5E7B40F158FB410F22DE1D76A
      16DA3A44B4AE83447D0BF5FE10527AB8B6452896241889A3AA2AAAAADEC88412
      89402084404A896DDB37411CCB35926C6E470F05582CDA042235E16A71D1BE01
      D0D7D7B7E553A98EC3EDA989A0BBA68952AE89FAD6F5C46A1B6F6C629497706C
      0BA352221C891208DE9C8AAF0FC771B06C1BD775909E4B458639BF90644D73FC
      9A52C0E4D86409297FA62D1B6F09EA1C8FF43E136C755E60662C476529CAC2EC
      2466B5843F18C62817712C03A1AAA89ACE523E87A228F8743F9AEE47F3F9F1E9
      01544DC7AC96282F15302A454072D4ECC1F3C5A91A369AA6329D5BC0AE964B52
      7ABFD4FAFAFAE28A67BC7BFF03DD117F5D1385E15694853344A72764BE5C3467
      F4C092142205948179A045F707D5502C891E8AE2F307D1341DCF73FF4509D7B6
      C83B31269D3A44344076AA80AAAA58A5C29294DEB3FB325D96662D4D1D7CE7EC
      486ACBD64F70EEEF43DED1F95DC6036746A5DF2AFEF8D3FBC77EB11C156A6FFF
      90BB3C8F5B66B5CB9ACBEE007602DB7D7A400F4513E8A128AAE6C332CA14F339
      6CD3183E997CB6D58AC47CAAE3A22812CF31F11C6B0A380020F6EECD7851BF69
      044289D72B96FA1670BCAFAF2FBB82462504A497617600DB80BF00FB0FD7BEA0
      3822F0073558135714054551C05E5C54F03EBF2FD37514404C1F4A37357EF6F8
      F4DDEE03BFF5D323AAE7C9CB9E1A6A57351DA108141CA94AE3C84B7BBB77DDE8
      8AFF13C6AF9772572AC9445D2D8EEDE03A2ED22A175DC77DE6AEB4E542088410
      8A10C22F84080B219242887A21449B10626D3E37DCAC603337356926EA127856
      C5765D6FF095EF3F72F18ECAF11D00A8800AE8CB6F3F100612CB733DD9B026D4
      F585E7BE5AD7BC7E37088460ED6F7FF4D8ECDD0210CB39C5BBD5BADBED2FFEE7
      FF8EFF39002BBC141D211C61280000000049454E44AE426082FFFFFF1F045D01
      000089504E470D0A1A0A0000000D4948445200000018000000180803000000D7
      A9CDCA0000000373424954080808DBE14FE00000000970485973000006A50000
      06A501179997DD0000001974455874536F667477617265007777772E696E6B73
      636170652E6F72679BEE3C1A00000033504C5445FFFFFFCC6633DC5D46D55A4A
      D6584BD6594BD75A4AD75A4AD75A4AD7594AD75A4AD75A4AD75A4ADF7A6DE081
      75FBEFEEFCF3F2212726F90000000C74524E53000516304B6A7F80B2D0E6FAF5
      5ABB46000000844944415428CF75924912C3200C0487C532CB98F8FFAFCD0102
      24F1F485AA16258124A0E3A3A55292458F1D77560EAAB9E543E6460E1F7F547E
      518F71FFC793350080CBFC233B00271F30C0F74457EBA65D3D99471CE2D5B683
      8C306E91E969485C91E5995066C176DFD3B3E8804C258BCBE7CA0FCA96E826CA
      B6EB41E9D1EA65785A9F37CEC01529745D0F7E0000000049454E44AE426082FF
      FFFF1F04F901000089504E470D0A1A0A0000000D494844520000001800000018
      0803000000D7A9CDCA0000000373424954080808DBE14FE00000000970485973
      000006A5000006A501179997DD0000001974455874536F667477617265007777
      772E696E6B73636170652E6F72679BEE3C1A00000099504C5445FFFFFF339999
      23AE8B25AF8A25AD8824AD8924AF8926AD8725AD8825AE8825AE8825AE8825AE
      8827AF8928AF8928AF8A2BB08B2CB18C2EB18D38B59239B5933BB6943CB7953D
      B79544B99948BB9B4BBC9D52BFA157C0A359C1A45DC3A664C6AB6FCAB170CAB1
      79CDB680D0BA88D3BE90D6C2A1DCCCABE0D1B7E4D8C1E8DDD0EEE5D3EEE7D9F1
      EADFF3EEE4F5F0E6F6F1E7F6F2E9F7F3F7FCFB3CCED0FA0000000C74524E5300
      0516304B6A7F80B2D0E6FAF55ABB46000000BA4944415428CF7D52D70283200C
      444464A47BEF5DBB6BFBFF1F5729A888A5F740E002979004210D4C2813825182
      918D209660206950F221070B3CCCF94842053232F71D3EF37CDF041C6AE02A4E
      EC904DB5D02C4F47A8715A2B318C88F3E0786E294310ADF2FB6B17B416ABF09B
      7B4F6F1812CA7486FAB84CC7E686D08E65BA5066FA9A41E1D052F3E70E60F458
      159A2C0F3EB824FDDBB60C468B74DBC9FB606541AC0F4EEC6AE17A498CD29F22
      7ACBEE6F94BFB5FE61F8353E1FEB3C17E7A6193DB80000000049454E44AE4260
      82FFFFFF1F040C02000089504E470D0A1A0A0000000D49484452000000180000
      00180803000000D7A9CDCA0000000373424954080808DBE14FE0000000097048
      5973000005BA000005BA011BED8DC90000001974455874536F66747761726500
      7777772E696E6B73636170652E6F72679BEE3C1A000000A5504C5445FFFFFFFF
      0000FF5500D56A15D86214DC6823DE6421DF6A20DB661DDB671EDD6C1CDC6A1C
      DD681EDC691DDB6A1DDD691EDD691EDD681EDB691EDB691CDC691DDB691DDC69
      1DDB691DDC691DDC681DDB691EDB691DDD691DDC691DDC691EDD681DDC691DDC
      681DDC691CDB691DDB6A1CDC691EDC691DDC691DDB691DDC691DDC691DDF7A1C
      DF7B1BE07F1BE0801BE1841BE1851BE38E1AE38F1AE49519E49719E59A19E59C
      1909EA89E50000002A74524E530001030C0D161718232A2D484C506A70777881
      888B96979CA1A6ACB1B2B7BDC1CCCDCED0D7D8D9E2EDF753BB97AC000000A349
      44415428918590B10EC2300C445F2A0BB50A43614042FCFF7FB1F105C5A8C409
      43DB907AE98D7EBEF3C98155B7D1A0D397B1D7F91E00C6CB3610883D10ED0A20
      B103F84C041804AFA404E034B8B9CE20803C1C78CED001D9276516503C282BF0
      D5310EA2B2CB2A1BF0470A87C05DB70AD21EA4E328D737C3F22B0C62D8D6A7E5
      86AC2BEF0A9CA3AF401B4781B97194364A2AB0262A416E1CA902D5E1FB6FAB0A
      F0038148422BCE6FCBA30000000049454E44AE426082FFFFFF1F040B06000089
      504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C3B
      0000000467414D410000AFC837058AE9000000097048597300000EC300000EC3
      01C76FA8640000001974455874536F6674776172650041646F626520496D6167
      65526561647971C9653C00000021744558744372656174696F6E2054696D6500
      323030373A31323A32322031383A31383A3338CF77EBA10000055B4944415448
      4B5D556B681C5514FEE6B1BBB3EFCD63DB6CD2B4B1CBA621B581A69A60B1D248
      210A3EB0480B168A828815FA43D11010A5BFFC51100A152AFD57E88F0AC5EA0F
      41E90B45AB040C8D983E34AFE6DDDD6493EC637666EECC5CCF9DD96ACD59CECC
      9D3B67BF73EE771E239D3DFB15368BE3382D7D7D4F5E6F6E6ED8C6B96BD7B721
      49DE957EE06A2010BE70E1E237A74E7DF4366DD6C49BC745368C1A6AB52A74BD
      F2AF562A25ADB5359DCEE5B6253A3BB7373ED25C6E7B6336D7DEB83DDBDED4D1
      D1129165772F61B4F850FF177969698E802AB06D4E0E0C5293C04D6E9AA65BB7
      F1C476390CCB815E6328574D6FCF716C711AEE3D6C127974F42AEEDCB985C9C9
      3FC890C13475D22A5CD7A99BF8A0B6EDC276485D5F85D7C76D368B1C0C6A2897
      8BB8776F045353E304E0C0B298E0D933701C01CAC1E8CE8403DAB61907A7381F
      D90811FC874221A45229EF5916174551A1AA41CCCFDFC7ECEC9F585E9EA9B924
      E29D00630420A2154E2C1139631EB065113794B8B6B636ECDAD589705823075E
      867D602136D1C5180B2E2C4C34168B736F9141B3208F3171749F0E93C2750894
      310B2661B6B46CED8844C2879A9A9A92B15854AAD5741F8C44C966B302B4ABA1
      A1E1BDEEEE3D43BDFB9E1EEAEDDD7724DBD1AE34A7D314A9437CD28F22E68E4D
      CAE0DA0CD1A08C4AB9ACF5F53FF35226D3FA8AA6450E56AB7AC3FAFAFA149DC4
      900606067283832F7E77F8F0915C2291422299C0CD1B37B06E4791DBBD1BD59A
      058B40295304EA800B86C8412216C1E86F37F1F2A167114F36C1D26B985F9CC3
      F0C71F7E7BFDDAD5E34A5F5FFFFB274F7EF01A79F5E880CB30B39C4743B60BE1
      7814D14810A9848678220C2D1E41201683148BC389C4A07309C1DA3A4CA2EB76
      610ADB5B7620198D755DBBF6C3DF6A4747C753E93479B40C4AA00CCE0CAC2186
      3C9288E92AD60C178B44DD9AC9A10BBF44BCE0DE956538D50CDA035348876458
      1B36D1C5D099EB443A9DEE57B76C49B7C729927299EAD7306194CA98B723985E
      5171B700DC5D93615035C8B29F6D9174B11225C38D3006C32AF6C6423810EC86
      1A54A164B688926B962D0B8962B1EC99062862590B602C9FC2C51F815F4680E2
      34A0CF49A83CF0B53C23A1344DEB29A03AA561BCD880543C48B405A10614EA5C
      5B946F48A6489D9191FB181D9DC0E2E20A0CA6206845B1939AA2330CECA073B7
      31A0D5F2B59DE878828A74B706F426393251A26DAD4A9DBB88B1B1494C4C2C52
      9E39A4E1E14FC706075FEF310C8392EDD05139229A024E4DE3C841385CF1EB98
      12258A5E51005572A17006955B547A2E2A94075992A169218AB882E1E1135754
      C6CCA57038D423787EF87011E7CF9FA1CE321122BE0490A0B6DE4C9E502CD475
      D4D2B476C959A95C45EFBE7E1C3DFAA617A9616CD010D32B6AB1B83A1D89A888
      46E32814E6A83A741C3C38809E9E1E3212534C40886BBD1CC4DABB4B0810A757
      AE7C4D519610A1B21473676E6E02A5D27A41BD7DFBF79F24C97E97E62FBDDC83
      A1A14FD0DDDD85643246538E8E4A51087544E7717F5E88A9269EC3E1309A9B33
      54310A32993405A7E1F2E571562CAEDD5174BDBA6059E69E6030B47363A324A7
      525B91CDB6797F9488375F895FAA5B315AC45AECF96346422211C3CCCC022F14
      1EE2D6AD9FD9B9735FFC92CFE7BFACFF016945915FEDEB7BEEF3070F961871C8
      ABD51AAF546A7C63A3CA57572B7C65A5C4F3F975BEB454E40B0B2B7C7636CF27
      2797B861D8FCF4E9336BF178E23345518E13564FDDAB2F070EBC804B97AE9E12
      A09B85A8E03493392397F461F1C06A35C6CB658BC607E7E3E37F71EA8977EA50
      9E48C78E9DF092B17FFFF362D0BFB1BC3C7D840A5C1599F2F9F5B91577F1FCDF
      DADF27BFE2E3A08E8CFCFAFDEA6AE12C61D28C06FE016B5116450EA31BF10000
      000049454E44AE426082FFFFFF1F042805000089504E470D0A1A0A0000000D49
      48445200000016000000160806000000C4B46C3B0000000467414D410000AFC8
      37058AE9000000097048597300000EC300000EC301C76FA86400000019744558
      74536F6674776172650041646F626520496D616765526561647971C9653C0000
      0021744558744372656174696F6E2054696D6500323030373A31323A32322031
      393A35313A3530B90926EB0000047849444154484BB5946D4C5B5518C7FFA7BD
      7DA194522C30D8CA8B83990CDD1C9B316499909898C8A22426B83187C924EEC3
      CC34D9A6269AA0C98C413F28319AE8BE6C13448C08C98CF0C52959B68C6D4607
      1B30CA8B84122883BEF7DEB6B7BDF71E9FD65A5C7CFB82FFE4E4DE73EF797EE7
      3C2FE7619C73FC1FD2659E1BAEEC89FB184B3F533395466A47197881BE1E31E6
      E7D5E8ADD63C8DD6C625490C87223371A09BFE7D466B3585D6A6464A6F65787F
      0B4E02B534EB2DDDFBE80395CDCDCC51570763BE1D0845202FCC63EDDA08EE5C
      BA843BA3E36E02B691C90FFF09A605CD266B6EF79E8ED3E6F2A3C7C04C3988AE
      7971ADB717AA24E291DA5A14180C509716313FFC3D8606BE557C61F1A4007C9C
      B26FCFF0EE8931B9B5C76CB7F5D4779D35571C3F99864296E11EFE1105E565D8
      44D0D9CB9791585D453C22E1FEEDBBF06CD3D382CD66EDA4D03C419E6695056B
      8040A367F7E9B78D85CF1C487F0BBA5C90A6A761482450BA752B366FDB0681DE
      C58505F8A7A610A60D8AAC363CB5BB56AFE9581781CD694352169C000E6FA698
      561C3B9E9E7BAE5EC518B93F3E380879799976D6C05515DCEBC5CCCA0AE6AC56
      2C8FDF46C8E747357956535C5442A73E913626AD8782B1B68A3ACA99A083B848
      F11B1A42DDA953B0575561F5C60D082613F482003F7960D9B103FB3A3A10240F
      02348F8922AA8B1D805E7738435B079B1C05358E5C86E41B2F62E9EBAF5075E0
      20E45008B12B57E0A43028F138343A7189C381647F3F4233D378A8BD1D7E8DC3
      353309838E23D768AAC8E0D6C17A8B254F601C81F39F43A630D86A1EC4F2C000
      9C5B9C30311D747A3D18558381B2BE291C86E7CC19588A8BA13EBC13EED52018
      25D9A8D79932B83F258F7656130A12944143E17DF49E009F9D865E0C0004E261
      119AD7079D14058F8A502726A02693C82D2B4B5F0E35914C33FE50161C8B4625
      5963E0663A5D5C06B350828301B83FFD04A191412CBC7414736D4710F8F5265C
      13E3489217290F583496B657150E4951A8067E57161C0E04A7BD6B21189C95E0
      A3A3744F15385A5A11F745109A7743764F22363F0EFF5A0089848AD2E70EA5ED
      22377F8149CF10A1F581647229FD9194052B9C77BB7E9E8079E72E245D138875
      F520677F239CEFBC0BA3628612A1534954A80901DB5F7D0DF9ADCF43A2CAF18C
      8CA0D061C724852ACE795F06B77EA53F624C50746CAEF16053799E6716C1DB8B
      A8ECFF0E68780CF2F5EB902F0E83532DE7D4EF83A9A11EFAB15BF8A9A909D29A
      87FA880D5FDCF5F98955759EF3E03DE04EEA1514A0861C7BFEC5FD8D0D02A66E
      21BA18C29613AFC374A8058C9244B54E376719F2850B98EC780FA2F72EF20AEC
      18F006348FA2B45056FACE667859F007992644DEBE9C6BB37DF8F8DE5AC11A09
      429C9A86D95A04A33D9F3A1F433418826FC90D439E1571A30943FEA0B69254DE
      A73A7B33657FEE9FC0A94622024F72C6CE5557384BAA287EE678149A2441A372
      4CA6E054922E29863151F2C99CBF42D02FD3C6A47F05D39D4F35794BEAF406C6
      5A2D667365AAF8358AB1A8A809CAFE628CF36F28F39DB4D44F2D33ABBF80375A
      D972DB5801BF01C5B4245FDB819AFC0000000049454E44AE426082E7EED60001
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C21E0000C21E00000000000000000000D9E5F1C3D2E3
      0E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10
      120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E
      1012B9BEEC7075C47277C77175C66F73C47074C66F73C66F72C76F72C76F71C8
      6F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71
      C97072C78A7DA30E1012CDC9E57975BA7473C36873C55C7CB65A7EB8567AB95B
      75CC5E75CC5F76C1607AB6586FD25973C85B75C35F75C3636FCE656FCB6973C7
      6D7BBC6F7AC5696CCE6B66DF6F70C50E1012C6CEE37378B66F74C36671C95D78
      BD5977BA5F7CC15E70C56A7AC4697BB36F84A6687AB8697EAE6A80A96D80AA6F
      79B8727DB96E7BAE6D81A07182AD6E78BA6B6DC96E70C80E1012BDD2E16C7AB4
      6974C4646ECE6074C46276C3687AC66A72C6BEC6EDDAE4F4DEECF1E0E8F4E0ED
      F2E0EFF1E1EEF2E0E7F4DFE9F5DBE8F3DBF1EFDAEEF2CEDDF59299D26E70C80E
      1012B4D6E0647CB36374C5616DD36270CA6670C76B71C5706EC0D3D1EFF3F4FB
      F9FEF7FFF9FFFEFDF8FDFEF6FBFEF9F8F8FEF6FAFFF1FBFCEDFEEEE9FBF3EDFA
      FC929BB96E70C80E1012B0D3E2617BB66275C46270CB6475C16675BE6D77C16E
      70C2BCBFEADBE2F1DEEAEFE5DFF3E5E3F1E4E5F0E2E4F1E1DDF4E1E0F3DEE2F3
      ECFAF7F0FCFBF2F8FE9795B96E70C80E1012B4D0E6647AB96475C16573C26378
      B0798DC39CAEE27079C76E77C26B78B56B7EA87276BE727AB56F79AF707AB272
      74BF7578B98086B6CFDCE4F0F9FEF1F3FD9F99C06E70C80E1012B8CCE66776B9
      6874BD6D7ABB7890B1A9C4DBC6E1F56879C35F6FC56376C85D76BD6171CB6176
      C3667CC46276C1626DC86B74C47781BECBDAE8F1FAFEF4F4FEA198C06E70C80E
      1012BBC7E46B72B4797EBF747EADB7CDD4E4FDFECFEAF4687BB46074C35F76C8
      5A76C25F7DBE5578AE5C81B3597CB4637BC76479BC7186B6C8E0E3F0FEFEF4F9
      FCA29EB76E70C80E1012BBCBE56E77AF7A7FB8B1B3D4ECF6F9EFFBF8EAF4F8C3
      C9E9C8D1F1C1CFEEBFD3EBC6D5ECC3D7E7C1D9E8BFD6EAA8B7E57180B87687B2
      CBDFE3F1FEFEF4F9FDA29DB96E70C80E1012B9CFDF7481B0BABFEBF3EFFFF6F6
      F9F5F6F4FBFCFAF9F6FEF5F7FDEFF5F8F0FBF4F4FAFAF4FCF7F1FCFAEEF8FFCB
      CFEC757AB07D86B3CDDCE6F1FBFEF3F5FEA199BF6E70C80E1012B6D6DE6F82A7
      B8BDE5F4EDFFFCF6FCFFFBF9FFFDF8FFF7FEFFFCF9FFFEEDFFFEDCFFFEF0FFFB
      EAFEFCF3FEF9FFDDD2ED827AB48682B8D1D8EAF2F6FFF3EFFF9F93C86E70C80E
      1012B8D2E56C7AB3767CBCB8B7E5EBF2FBF0FBFBEBF8F6C5D6E2B9D0E5B7D5E4
      B1D7E5C0CEE7C1D2E6C0D1E7C0CEE9AAAEE67576BD8586BED3D8E8F9F8FFFBF3
      FFAA97C16E72C50E1012B8CCE6717CBD6F73BF7D7BC4BEC5E7E7F1FBD8E5EF6D
      78AB6876B56477B4647EB36078B55F7BAD6481B05B76AC697BC76676BA7989BC
      C7DEE7EDFCFFF0F8FF9C9BBB6E72C50E1012BCCEE46D76BB7073C66B6AC18088
      C1B3BEE6D1DDF4737AC36F75C96E76C66572B8607BC55877B55F80B95978B65E
      75C56076BC6F86B9BAD9E1E3FCFEE9FBFF97A0BC6E72C50E1012BDD2E26C79B5
      6B73C46B72CF6777BD7082C2A6B7EB656BC47071CD7070C67476C05E68CD6272
      CA6072C46474C76872D26872C77885C6C3DAE9E9FBFFEDF8FF9599BD6E72C50E
      1012BFD8DE6E80AC6979BC6073C65D7BBA5D7EB45F7EB36475B7868CC88F93C2
      8A8EAE9087C79590C58E8BBB918BBD9386C8978AC49C92BFD7D7DEF9F7FAFCF7
      FEAA9DBD6E72C50E1012C1D5DD6D7CAE6D7BC45B6EC65979BC5B7EB75A7CAF6B
      7FBAC9D3F1E8F0FFF2F9FEF6F4FEF3F3FAF5F5F9F9F6FCF8EDFEFAEFFFF7EFFD
      FDFDF6FCFDF6FFF8FAA89CB76E72C50E1012C1CEDF6E77B56D76CB6170D45E7B
      C6597BB95D7EB3677BB5C9D6EFECF7FFEDF8FAF4FEFDF4FEF6F5FEF4F7FEF9F9
      F8FEF9F8FFF9F9FBF7FDEEFAFDF3F7F7F99F9BB76C70C30E1012C6CAE87372CA
      6865D46161DD6170D15E72C56278C16874C2A7ADDDB7BEDABBC4D4AAC9D6ABCC
      D3ACCDD3AFCAD5B1C0DDB0BEDDADBDDCAEC8D8ABC4D8A8BBDF7B87CD7175C80E
      1012BDC6E26F70BA7470D06B68D6666EC46673BE6B7AC06C74C37379C26C75B0
      7684AE637ABA657DB2677EB0697BB46B73C46B73C46974BF6B82B46279B36679
      C76773D76E72C50E1012C3D4DC747CAE6F71B97372C96D75B7717DB7717FB76D
      76BE6F78BF6B78B46B7DAA6A77BE6C7BB56E7CB1717AB47273C37476C36B72B6
      697DA5697FAE687ABC5F6BC46F73C6C4D0E7D6EEE7C0D2D9C2CCE1C4C8E7C3CE
      E3C1D0E2BFD0E1BCC8E5BAC6E7B9CAE5B6CEE1C5C7E6C7CCE3CACEE1CCCCE2CD
      C6E6CDC8E6C9CBE2C7D8DCC2D6DFC1D2E2BCC8E4B7BBEED7E3F4FFFFFF1F04D7
      01000089504E470D0A1A0A0000000D4948445200000018000000180806000000
      E0773DF80000000473424954080808087C08648800000009704859730000060A
      0000060A01D64C94C00000001974455874536F667477617265007777772E696E
      6B73636170652E6F72679BEE3C1A00000154494441544889ED95B14E02411086
      FF7F778F6B2C0C0986968282CE1730B181828B8D0589444B1EC4C780061225A1
      C042E23358D2DAF00E40022686CB8C158A78776C2EB151FF6A37F3CDFE9BD9DD
      59CEA7CD070097C8D642C8A878FAF40C00B7E3594FA19D03392B85B68CC7E200
      706C1567DB89422E3C728E08368C0708001028B76382CC623F3882DE0679F56F
      F0470C169EEC22659C2A51CE9D90D1EE234A04A1CB387CBDFFDC16DB2AA81B45
      C67B90D5DB261C52557D36935B3F7E067CB98A7A309ADD5B144B35B65DBB9B4C
      93C2D57EB3A441D09D5D3F7EEB6B0ED40E0E57A90C913A80448398B456B59814
      F32E1155BD1A5C6E83BC727992AAFD662926ED764EE34E002D54065179970BC5
      AD7319681074BFD65C0B006AD6DAD12E173B8C7319ECDF96CA202A5B6B47B39B
      C9F93EFB2B9A1D575E24D58FDB933394960A1AC2F48F9CC07C03334C8B87E2D6
      B1C33829F60E4CF569099663028D0000000049454E44AE426082FFFFFF1F045C
      02000089504E470D0A1A0A0000000D4948445200000018000000180803000000
      D7A9CDCA0000000373424954080808DBE14FE00000000970485973000005BA00
      0005BA011BED8DC90000001974455874536F667477617265007777772E696E6B
      73636170652E6F72679BEE3C1A000000DB504C5445FFFFFFFFFF00BFBFBFE6CC
      4DF1B81CEBCC47D5BB88ECCC4DCFCAAFEDBB17EFD04BD1BF85EECF4ACFBF8DEF
      CE49ECB915F0CE4ADABD5EEABA17EBB916EFCD4AEFCE4BEFCD4AEECE49EADB9B
      D7CA94DDC56FE9BD2AEBBD20EDC532E0BC47EDC739EFCE4AEFCE4AEECC43EECD
      4AE7CC60EFC93EF0D465F0D56AD1C084F4EFDCF3EEDBEFCE4AF4EFDAEEEAD6F4
      EED9EBCC51CFC9AED1BF86EFCD4AEABA16EFCE48EBBA16EFCE4AEFCE4BCEC9AE
      D9D4BBDCD7BFE0DBC3E0DBC4E8E3CEE9E4CEEBBA16ECE7D2EFCE4AEFEAD6F0EB
      D7F0EBD8F1ECD8F3EEDAF3EEDBF4EFDCFF9B825D0000003874524E530001040A
      12191E28303841585A6073848688888B9FABAFB8BAC1C1C1C1C2C6CAD0D1D5D8
      D9D9D9D9DAE0E1E2E2E4E4E7E8EAF2F3F3F5FCFC687187D9000000AF49444154
      28916DD0E70E8320148661BAF7DE7BEF69F7B0B52A15EFFF8ACA31868670DE9F
      DF1330420809C697AED482784533B70D0A290DE04B2965085877DE5B824441C3
      AF3A7701D847C47C303CB01E224B02A975292CC079891C776BE404D8FAD34FB7
      3914F1ABFE509F494D27C37E05607F359506000D7537B3006375BF4438CC9103
      35F878138134C04EDD0F210E2DE44019FEA3834092C3F1A4EEAB00873672200F
      CFDE4320063052F72AEC3F54E776FC8CD9CF630000000049454E44AE426082FF
      FFFF1F04BF02000089504E470D0A1A0A0000000D494844520000001800000018
      0803000000D7A9CDCA0000000373424954080808DBE14FE00000000970485973
      000005A2000005A201767395660000001974455874536F667477617265007777
      772E696E6B73636170652E6F72679BEE3C1A00000126504C5445FFFFFFFF5555
      4E6276A44940536073555B7355607565718E5560715E5E6FA446406F7EA56F7E
      A4A84C3E667394545E74B57F3DA34741545E745761754A5265585E6CCC564854
      5E73646568667296A64E3FC65749D88829A34740D55A4B8292C4A3473F404656
      545E735861765D66795E677A687597787F8C7E646D8595C88595C98596C98596
      CA8697CB8E939B8F755A908D8A9DA5BCA49686B2B4B4B5B6B6B8BCC6B9BDC6C0
      B5AAC3B5A8CA8939CC624CD7892BD7D6D3DAD5C1DDD8C6DDD9C6E08C26E0DBC9
      E18D26E29333E2DDCCE3B378E4E0D0E6E2D2E7E2D3E7E3D3E7E3D4E8E3D4E9DA
      C6E9E4D5EAC89AEAC99BEAE5D7EAE6D8EBE6D8EBE7D9EBE7DBECE8DAEDE9DBED
      E9DCEEA53DEEE7D8EEEADCEEEADDEFD5ACEFEBDDEFEBDEF0C07CF0D5A9F29E26
      26619A5F0000002174524E5300030D1C282A303F484C5455575E5F808D96989B
      A1B1BDD8DCDCE5E5E5ECF9FDFDD0210714000000DE4944415428536360606103
      026E1E3E454545111E1E76108F890104C49580404B571E2821A9ABAB06E27140
      248C434242B475753535357574750D811C15A884495C5C9C992914D80039AA48
      1228002111EA0902619812012E2010882981D3280F5B04B0274A47B01B087885
      0707B9B9B96391880A429720DA724E46EC3A34E4C4982112D1A1081016176325
      2D2B0891F0B64300C784444B29052E4CA3121223AC6578199075F842C59D1D04
      60AE82D8110D1577D2C7F4476C3C50DC0F8B07237D5C9DFCE11E54D78303030B
      73233D3D658804BF101C884A08836956A0300006027A74394C54F80000000049
      454E44AE426082FFFFFF1F04D403000089504E470D0A1A0A0000000D49484452
      00000018000000180803000000D7A9CDCA0000000373424954080808DBE14FE0
      0000000970485973000000A6000000A601DD7DFF380000001974455874536F66
      7477617265007777772E696E6B73636170652E6F72679BEE3C1A000001D7504C
      5445FFFFFF4949494747474E4E4E4A4A4A4B4B4B4D4D4D4E4E4E52504D505050
      54524D545250535353555350555755514F4F51514D565656625D4F6A59535656
      56565A56555A57575957675853595C595555555D57545F5A4E55555552545252
      54525B5B5B6A5F596B6450585858655F505757575959596E7D706E7E706F6F6F
      5D5D5D5C645D625B56555454616161735B537B7B7B51504E5A53506F5B547E7E
      7EB28D79B39C5B5353505B5D5C5F56535F605E615652656865666A5D68645968
      6C696A6E6B6D645C6D836F6E6C6B715F59718772728874736F57745E56746F57
      75705875725975957A78826E78836E79856F7C907E7C927E7E93807F7C7A7F7C
      7B80A88681808081818183838386AF8D87878789B18F8A8A8A8E8E8E8FB79593
      939396B99C97979798C19F9A9A9A9AC3A19BC0A19BC4A29CC2A39D9D9D9DC6A4
      9EC4A49F8071A4A4A4A5A5A5A88F53A96A56AB965ABF957FC1A75DCA9C84CBA5
      51D3B158D8AF51E8BA52F2C75BF2C85BF3C85CF4C95DF4CA5DF5CB5EF96640FA
      6F49FA724CFB764FFB7751FB7A53FC8059FC835DFC855EFC9C77FC9D79FC9E7A
      FC9F7BFCA17DFCA683FCAD8BFCB594FCB795FCB796FCB896FCB998FCBA98FCBA
      99FCBB99FCBB9AFCD464FCD564FDD665FDD666FED666FED7662201B72C000000
      3774524E53000E121A30586062636D6D6D6E6F6F88889192B2CCCCCED2D4D5D8
      D8DADBE1E3E8E8E8E9E9EBEBEBEBEBEDEEF2F3F3F6FBFCFCFCFCFCFD4ED8336C
      0000012C49444154181905C13D6A54511800D073EF7CEF8DC94C9489A3060BA7
      B0D04EC4CA9F5E1B0BD7E05E04D7E10A5C8290CACA4A425084108D0C8CA8C9C3
      CCFBBB9E131CDCE2740300108BE5D597E3E4FD75E0DFF71110FBCFBFE492F79F
      B299DA1EAFB780D8FD79BEA139A3B4CAB50C1065B5C213C0E5478068EE7645E8
      417CCDA10C10BAA6988E1DA8AD3AE9E402A1DA2BB80226EED7A95E5F206C1B00
      C269AD4C20140014376FE88E210000A95455552B7D00009ADD767EFB713A5A87
      A80A80CA6CA712D5FCC18768DF3D03C0A7D99F5874272CE26CF3D9F2D1F900CC
      76B07CC59B182F991D4C7B008CDFD21D0100807150049461AF0710C3245216B0
      3DECA57BA500BF1F767A017F8F92F98BAA003941808136C70800014002401200
      E3E1EB1140CA3900DA1F6F01907FFD0719CB69208A58B4E70000000049454E44
      AE426082FFFFFF1F042802000089504E470D0A1A0A0000000D49484452000000
      180000001808040000004A7EF5730000000467414D410000B18F0BFC61050000
      00206348524D00007A26000080840000FA00000080E8000075300000EA600000
      3A98000017709CBA513C00000002624B47440000AA8D23320000000970485973
      00000DFB00000DFB010B77359B0000000774494D4507E20B090A23002EBE9C05
      000000F64944415438CBA5D2B16AC25014C6F15FA5A583B343C0D147B04B50C8
      18C117C8DAB5BBF8225DC477489FA2124BC1DD4542F3020567BBA8343186B47E
      87BB7CF7FCCF399773EF54D5F522F4840FEF5EEDCBD79D4AFAD8C6C0D2D0D0D2
      C0C65883C6729150AA50488522F975A46B2B32974B040289DC5C64AB5B0FCC2C
      8472BDB3D3930B2DCCEA813753A9A4E42552536FF5402150084A5E70746B7538
      9E46B7E38F3A01BB73A543254EEEEEB271B30E378E74233092C98C5A4C785426
      16CBEA32EE6BF1BD47AA1FBBA9C3C4CACAA47D07BEAF55AE07FABED06F0F3C78
      C667FB373464FC7B716B71635E6C5D3646B28B7FFA3BCE7BFF0142EB582BAC86
      C6E30000002574455874646174653A63726561746500323031382D31312D3039
      5431303A33353A30302B30313A3030F6BCE7590000002574455874646174653A
      6D6F6469667900323031382D31312D30395431303A33353A30302B30313A3030
      87E15FE50000001974455874536F667477617265007777772E696E6B73636170
      652E6F72679BEE3C1A0000000049454E44AE426082FFFFFF1F04620200008950
      4E470D0A1A0A0000000D49484452000000180000001808040000004A7EF57300
      00000467414D410000B18F0BFC6105000000206348524D00007A260000808400
      00FA00000080E8000075300000EA6000003A98000017709CBA513C0000000262
      4B47440000AA8D2332000000097048597300000DFB00000DFB010B77359B0000
      000774494D4507E20B090A2309576224A1000001304944415438CB8D92AF4B43
      511480BF0D5D7082203E10169E61EA8A7328CE607C6960D2B020CCF48AC96611
      4C32FC03043558142C0699B089AC8833CC1FB0BAB0728788A0A6A7AF1A547CDE
      EBBDBBA7DDC3F7DD73EE3937C657B86C324D82806BB609E811456EF1F058244F
      897B66CC78862BB23439648B1A3B8C73C7A04938678E26D9EFD31A27AC50D6E3
      711A14D988646E7068E8F1341DF27F804B1648EA85014292BC4772CF8C12D309
      7D843840D86B90BF429B2ED0B61760DD1606945E87F12930C110470A1B72CC83
      2C1CF0448534018F8AE0B04B4E4EB68CFDEC918B1B81313CB96993B0CC3E05CE
      D429E962151FC10523BCD85538C56712A2B859A832CF2C75FB37A4100852F682
      8B40E0DA0B1F2C519117689A525DDDAB5AE19529ED05FD6478932B94FFF9743F
      91A08AF8047F3E40055523C0D00000002574455874646174653A637265617465
      00323031382D31312D30395431303A33353A30392B30313A30306324A28A0000
      002574455874646174653A6D6F6469667900323031382D31312D30395431303A
      33353A30392B30313A303012791A360000001974455874536F66747761726500
      7777772E696E6B73636170652E6F72679BEE3C1A0000000049454E44AE426082
      FFFFFF1F042503000089504E470D0A1A0A0000000D4948445200000018000000
      180806000000E0773DF80000000473424954080808087C086488000000097048
      5973000000A6000000A601DD7DFF380000001974455874536F66747761726500
      7777772E696E6B73636170652E6F72679BEE3C1A000002A2494441544889ED95
      4148936118C7FFCFB76FDF26BAA5CD499BD96C246846607510236405621D3A44
      7828CD4B97A0303C798ABA75342422F4608508E2A94351102BE910E14549CDA4
      3073CEB9E9D44DF77DDBF77D4F079D3A9B2E5975EA7F7BFFEFF33EBFF7FDF3BE
      BCC4CCF89B12FE6AF77F011031DC540F462B0896A429DB1A4A7473D9BEAD85E6
      B9AE09211150360CC62088DA70FCE9CAEE00A00B84E2AD261B2CD0A4E26DA554
      953A4435C04300BA76030800B677DA8B0E662A10D352D530488B80380E70220B
      3E403CD4F8EB3D2501BCCE26E800F4359FF5D4BA2877A05F7F91E2B116818F3F
      A0AF4FDB19F0BB9AD4DFC983F9B5AB459B49991643C89D9FF183B56AB4F74DA5
      8D682F1A3F518FC4A933705B451889116720E47DED7079FBDB015CCAFA1D3000
      21B284AF8108FC0A412440387D0E00CEA3B521E78F3D34831A4734388FB1B00A
      B351C4B2F3B01964F4641D9135348D19A77BD3884431112094DA9CB0FA27EBB2
      06B84706E01E19D8693A3F6D44BA9ECECDAC74CB524E303046E87E4B18F501E5
      4EA0B996E1A9CC7C8B3FAE0AE89C37E0934C2833311A0B34D459D670F4F249D3
      2000F8C29CD3F946A810253389A2085555915014BE7E561F2BB12196AE71EC07
      CF46BEB0EBF18258019364308A46A8AA8AB8A27073813A9E47FC9E4E5EB8B6B1
      4593D904AB350F47DD8730FA6D0ACBCB5128B292AE778A2493048B2517C78E94
      627C721A8B4B11C831391911DF48161A25639BCB51E4AAA9AAC44A4CC66759F9
      AEC8F2FD8C00496A71DA6DE535559548681A86576341C4F80E00D0D62FD373F5
      568B40D47EA0B000B3A13074E6DBDE9E8E0799009E2B379B89A8DB61DF8F4028
      0C4DD3EE7A7B1FDE5B3FC1A6EC89C2474131B8E80B2C5C24E2E776D5DE9B311F
      00F0DB7B74C75CDC1758B82C0878A568A667C929FAFFE967D24F1E860709C0F9
      A8EC0000000049454E44AE426082FFFFFF1F04D603000089504E470D0A1A0A00
      00000D4948445200000018000000180806000000E0773DF80000000473424954
      080808087C0864880000000970485973000000A6000000A601DD7DFF38000000
      1974455874536F667477617265007777772E696E6B73636170652E6F72679BEE
      3C1A00000353494441544889BD554D6B5D55145DE7E37EE5DDF791D7BE984888
      B1510295A215B1688A69540405C58116220E7424D4D6BF20450427820375A0E8
      48101CE840938123410D6D2A1502D6B60A164969DA244D72DFC7BDE7DE7BF676
      9026A9C92D7952EA1AEEBDCF5A672FF63E473033EE24C4B1270E7DE6452BAF09
      B2F2768892B077E6A39FCF8C6D8F6B2FBAFE7AFD918362F4C861DCD5DFD895C8
      5A0B63326479863C273013CC6A44539F7FF36C51BD166485DB68A05CABC0719D
      5D051C38F0031F440C6B733003519EF317A74E4545F5E2F8E30FCFF949EB8057
      AF23DC5BDF55603BD85AA45184ABEDFCBB8F7F9C7D7E870033E38DA7C69F5164
      87E4807D9743A72F688448A3044C8C9EBE72D762F16A6C1CDF112AD0CA444966
      57E22BE2E6293AF6D69319795A57AA01E24E0A6246B5DAD3B540AB99C0F5345C
      57A3D33648920CB73539DD4003C0C9971E7B9A2CEE0FEA322181D08D53B0C9C0
      0C381DD31551E62820B5C4528214496444722D59151FBCF860744F9A94D9F520
      83EEEDD880B0046933FC255574E2AB33D51D1D0CA571797EEC058C1CD88F5AAD
      D2353111C198148949912406A58BBFFF5D54A7A15CE47E0F4AA5ADDB67790E93
      A49B044A49789E0BDF73E1791EA414905222087C04810F00583E7BBA700F3472
      03A7D344B3D946BDBEDEA1A3359C50230C6F6DD9F60E54B952D8BEBEE487CBF7
      CE4CEDC967A6B1A875B70E6D81190E13DA9E3F5C9416CC8C93471F7DC8A4F2BE
      8DA089F2F7DCF6DAC87FD1C972C1446273A958A93C29553F1145CFF52B13137B
      5F7EF5B985DAE08012B278558808CC80523BF357AF5CC3C51F7EC2CAAF735428
      0000535F7E98D70707D4CD314B0493A4887F3B077B7A1600430E0F23983802DF
      77A1D47A79D46C616EE6172C7C3B8D42D3DF1C3FF4757FD9975EAD0A5170C3DE
      5684C6EACA3A593BC6F985A57FE59B8B4B48AE2DC194AA7F6A00989C9C3C2884
      7A1F1072A81655C70EDBD1D93F7CB1D664007687C048C6D8F8399A19E3721CE3
      81FE459C5FDC83EB1D1F707A59F7F5BCFDE9F4F7EF680010428D039800184A10
      061B09CE2E07305158685F6404CE6907462A6826E87A80D17D292E591F465401
      4018A009A0D8A2DD30EF5530EF6D8DFDDD68DDB2F6FF794D8970414AC480900C
      212D0907101920A83B9AF533BC7986890817801B8B7627F10F20D68085A4CE86
      460000000049454E44AE426082FFFFFF1F046805000089504E470D0A1A0A0000
      000D4948445200000018000000180806000000E0773DF8000000047342495408
      0808087C0864880000000970485973000000AB000000AB01D44D92AB00000019
      74455874536F667477617265007777772E696E6B73636170652E6F72679BEE3C
      1A000004E5494441544889AD966B6C93551CC67FE77DDF5EDEB5DB0ADDDAD2D5
      8D42C1A1668E4B4BB61608DB00715318D904420C0462F08B97681412893131C4
      4BE2257E301283CA07A3C14934C68831B8186306320263307083AD6CC06E1658
      D9D6CBDA1E3FC816714E86F1FFED3CE739CFEF5CDE9CF30A2925D3292144B1AE
      EB27DC6EF7E8BFF97A7B7B191919C91F6F6BD34ABF55151515A35BB76DCB3B73
      BEFD365D4A4866241BD6AEA2BEBEFEEA5FFBEE0A00F0F1C1439CF79461CC2F98
      D0624371067FFE0E5F8173927F124008516E369B7D4208473299EC32994C3393
      C9A403383CEED18B8A3117CC991873F3E27514DBC97F9CD03FADA070C18205FB
      6A6B6BB570383C6AB3D9D403070E1089448E0084FC8B881FF9E0B6014A42320B
      F0782AA70560F3E6CDDA8D1B37B4743A9D535A5A4A6363E3402412A1B9B9D9A8
      EB7AC46E52FE0C5654AB94326D37656200FBF7EF676868C8784700C0DAB56B89
      C562B85CAE71A9ABBFBF7F454343C378BB3C10AC78377CE17C6AA0FFEAC340F2
      963E362DC0BE7DFBE8ECECA4AEAE6E5C4A48294F09217C8007B066D263A9546A
      6C0CB0DD029C9152FE3E25C06ACD794ED7B34200814000AFD78BCFE743511425
      37D7B6C736332F916BB33DF6484D8DCB62B1C8542A932E7DA0585355A5A1BDBD
      5D3B7AECD811AB35A7717838FAF6248010C233DBEBDBB328109C2184C0E7F3E1
      743A713A9DE859D6BC9D4FED7EDE6C36F3E927EFB377EF5EDADADAF0FBFD0034
      3535118FC74131D774757604851007A5949701947180C562A95F535D9BED9D33
      8FB17486920717120C2DA7C05388949292D2252C5C5286D56AC5603030383838
      B1F2BEBE3E9C4E2716AB9535D5B5D9168BA57E62E2E35745BEC37572F7CB6F96
      BEF5DA4B8064C64CFB44C0A5702725A57E5EDCF33ACFECDCC496AD3B269DD9C8
      F030CDC78FB2E3C917D8F5ECF65383037D0B27B6480891ED29F4BAE7CE2BE6FD
      8FBE9CEADC9152B2BEEE71864712933B8581CAD5EB9869CFC3AC67B98510D952
      CA9B1A80AAAAD5C1E555D900EFBCF90A97BA2E904A8DA1AA1A994C1A4551585F
      B7859C1C1B39B936A2D11B7CD5F029994C06455149A753689A8122AF8F45FE72
      82CBABB21B3EFBA81AF85C01B0DB1D3BCA422B75809EEE2E2A6B36E29855C8B2
      AA7578BCF3F18756138D0EB1381064712048343A843FB41A8F773ECBAAD6E198
      554865CD467ABABB00280BADD4ED76C70E004D08A1E53B5C2545B3E732D0DFCB
      40DF15BEF9E2634686A374873B48C463B49F3DC5A31B3671E2D75F2676A4F1F0
      2192C9041DE74E131B1DE672F7454687A30CF4F752347B2E12592284D05460C5
      B215ABB62C591A32B79C3C8E41CFC61FAC642C99A46CF943188C464A16976332
      A8ACACAAC65D50C86FE75A7117CD47CFB21028AF446632AC58BD9E5C9B1DA341
      E39E222F577A2EA52E769C6F52F31CCE57EB376F0F385D6E5A5B9A39DEF4135D
      17CE121B89D213EEE0E6D0357A2F87295DB494C8603FBD577A501495C6EFBFE6
      E6D0357AC21DC446A2842FB4716DB08F024F21F3EEBD1F935937B79E6E56455E
      BEABFBBD0F3F7734FEF0AD3AE5E7F31F6AE5AAEAF4D34F6C1AD00C46637E4B6B
      9B692072FDFFCCA7A5B54D33188DF91A80A2AADC57E29FD21C8F8D924EA56ED3
      544DC3AC67DD1134AD27B3B9E947FAAE76DFA6B9DC85842A6AEE0C482612A944
      3C86C9AC4F699A4ED0DF2B118F914C245222DF59B027934EED329A4C77FD03F0
      6F954C24528AAABDF107CE93CAE6AEA40C1B0000000049454E44AE426082FFFF
      FF1F041E03000089504E470D0A1A0A0000000D49484452000000180000001808
      03000000D7A9CDCA0000000373424954080808DBE14FE0000000097048597300
      00064500000645015FA37FB30000001974455874536F66747761726500777777
      2E696E6B73636170652E6F72679BEE3C1A0000011D504C5445FFFFFF5555AA66
      6699DBDBFFDFDFDF5B5B80515E86596680596485EAEAEAEBEBEB575D83556080
      566280D5D9E0CBD2D9555F80546080556180E7ECECE7ECEC556080555F815660
      81546080556081E8ECECE6ECEC566081555F80565F80E7EDEDE7EBED55608056
      5F80556080556080556180E6ECEDE6ECED838CA2848EA3868FA47D869E80899F
      929BAE767F98959FB1747D97757E97A9B0BF68738EAAB1C0556080626C8A606B
      89BDC4CE555F80556080C3C9D35B6685C8CDD5C7CED5596583556080CBD2D9E7
      EBED596382D2D8DDE7ECEDE7ECED566181556080566181DAE0E3556080566180
      566081DDE2E5DEE2E6566080556180E7ECED556080E7ECED556080556080E3E9
      EB555F80E5EAEB556080556080556081556080E7ECEDE89193020000005D7452
      4E5300030507080E131417181A293044494A4E52545E5F6063656A757A7B7D7E
      838C8D9C9EADB2B6BABBBFBFBFC0C0C0C1C1C2C2C6C7C7CCCDCFD2D3D5D6D7D8
      D9DCDDDDDEDFE2E8E9EAECECECEDEDEEEFF0F1F3F3F4F4F6FAFAFBFBFCFDFE8A
      E963710000010A4944415418194DC1895A41511406D04D894269520A0D68D240
      75D1646A7653A96E847FBFFF6374CE3EE7F3DDB5C88A2432B9A65B48C743E417
      4879B086D9204DC41CF83851B2920368DD4AA50BCD4B92887A501A2D565A0D28
      83182941074AAFC3A2D383E20488280BADCA56155A8A2834845662AB04CD8B50
      1CA2CC561922416988225B45880C15206A6CD52072E442D4D9AA4334C9853861
      EB00E29BAE209EFB2CEE4610D7948771CEE202469ED660DCFEB1723386B14EE1
      018C3A2BA7307E67887620C60FAC1CC3D825A219175A89B5CE1BB49730298B43
      E0FE928DF7C34F60B44462F5F5ACCF131F47BD2459F36DF6692FD0C4F4E60F5B
      5F1B53E437B7B2BDFFF4B8B7B53C4BC63FA8CE970EDBE752010000000049454E
      44AE426082FFFFFF1F040004000089504E470D0A1A0A0000000D494844520000
      0018000000180803000000D7A9CDCA0000000373424954080808DBE14FE00000
      000970485973000000A6000000A601DD7DFF380000001974455874536F667477
      617265007777772E696E6B73636170652E6F72679BEE3C1A0000019E504C5445
      FFFFFF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000140F0700000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001813070000001B1508261D0B1D160956431B795F
      26000000785E26271F0C27220F312912312A133931163A3217453A1A453C1B46
      3716463717463C1B4738164741228674359F7F349F863B9F893EA7893AA79041
      AE8837AE8C3AAE913FAE9F51B48D38B49B47CCA040DEAE46DEC057E8B649E8C8
      5BE9B749E9DB77EFBB4BEFCF60F3D25FF6C14DF6D460F7C44FFBC54FFBDC69FF
      C850FFC951FFCA52FFCD55FFCE56FFCF57FFD058FFD159FFD25AFFD35BFFD45C
      FFD55DFFD75FFFD860FFD961FFDA62FFDC64FFDD66FFE26CFFE470FFF0812AC1
      43EC0000004D74524E53000102030406090B0C0F13151618191A1C1D1E1F2023
      2526292D2F31333D3E3F454A4E4F59636768696A6B6C6F7577797D7F81858B99
      9DA2AAB1B3B4BBBDBEC0C4C5C7CACED0D7E6E8E8F1F2F24FC848CB0000017B49
      44415428916D52875202410C7D2756142B16ACD845C5821D1511114401157B39
      3BD80BE85A903B455CCB5FBB7727C88CBC999DEC249BBCE4650105056DE6499B
      CD6C6AD42019B983132D79CCAAF55D969EF4B8D750DE30559F78C375582B7FAF
      C37DA3599255E9742AC9968E1B94C0F4DC583138A3C3E972391D460E481B5772
      06DAF3A1B5BB4361510C87DC762DCBB14A3C65EC686702029521046658A4A387
      397B19A13D1015A3D4EFA7F4231AB073E02C1A546500C659E1951CAC79BD6B87
      DF9F82DB08743562480D3842B1FBD3A520CF07372EBE68C801E84D60092A67F8
      851CCF5FF33CBDBAA334EC54416DC608A07389114276167D9B32BFE8D20136D4
      C88127421EDE377D8BBB7F8166B95484DC5E3EF2FCF5C249A2543F24F23772BE
      7EB3B51D5C3EA371F2CC7CB9DD08D95BF17856F71F63F1760B87940119FDD111
      B9171303223B4B91E44D787A16624992E45824B15288084D491A52CA8ED64EA4
      5C14437745AAD532145536D5E2DF6790515C57DD5996FC7D7E00C5206B0CB5B3
      85BF0000000049454E44AE426082FFFFFF1F048603000089504E470D0A1A0A00
      00000D4948445200000018000000180803000000D7A9CDCA0000000373424954
      080808DBE14FE000000009704859730000058900000589016D689DFA00000019
      74455874536F667477617265007777772E696E6B73636170652E6F72679BEE3C
      1A00000180504C5445FFFFFF0000FF8080808080FF4080806666992BAAAA5555
      8080AAD54D668027B19D24B6928899CC20BF9F8495CA22BB99555E808796CB23
      B89C5762825560808595CA8099C7555E8228B79754608026B89A8598CB8597CD
      545E818596CB8696CB27B89A5460805661815461808697CC8698CA54618031B4
      9F27B99956618026B89A5461808797CC5560805560805E698D26B999626E9360
      6C91606C918697CA5E6A8F8797CB5D688B26B9985C688A7888B68697CB556080
      8697CB8698CB5560808697CC26B9995560805964855560805764848697CB7988
      B78698CB7C8CBD55608055608026BA997E8EBF808EC18697CB55608055A8B226
      B99956608126B9998697CB55608026B9998697CB556081555F805561808697CB
      8596CA5560808697CA26B9995560808697CB26B99927B99A2BB79C2FBC9D31BD
      9E34BEA039BFA23DB1A543C2A745C3A852A9B052C7AE54A9B15560805EA5B662
      A4B863A4B96CD0BA73D2BD779CC37C9AC68597CA8697CBADE5D9B5E7DCBDEAE0
      C8EDE5D5F2EBD7F2EC580A3A0F0000006374524E530001020204050606060A0D
      0E0F101D1E1E22242F30303239404044454749494E565859646E747C82848C8D
      919FB2B8B8BBC0C1C2C2C3C5C6C7C7CACBCCCCCFD0D0D1D1D1D2D2D3D4D4DCDD
      DFE0E0E2E3E5EAECEEF2F3F7F8F9FAFBFBFBFCFDFDFEFEFE1897ED6E00000109
      4944415418196DC1055742411006D0CF6EB115BBBBB1B010BBBBC51C3BC776CC
      BFEEDB7DFBF0C8E15EFC139F1E85302A073665ADA304A1AA440B14E34F82BB3C
      2B62506C8D08AADE66E6FE80D8FAE028634D8C21389A58D91163120E1F2B5E31
      8E52618CB2E21147366C697BAC8C89B1900C5B26DB7AC5560823669AB516D1BA
      1054C1DAD6942805088A9B61AD5D2CE329F9A519308A586B1391B7A50D226A75
      4173B3B23E2F2277A4CD26C212DDCC16EF9C88BC1C93E5FC93EA01E40E33F3C8
      8428F7A47C3C911F910D87CCDCB92FDA0D9D7E5D3DBF13ADA28E2D2BBB627B20
      BAFDF93E23EAC1225BBAC5783D21BAB826A21AB0E211C7236997390895E73FA0
      E5DA588491E482E5175F5891BBEC8DEC660000000049454E44AE426082FFFFFF
      1F04C503000089504E470D0A1A0A0000000D4948445200000018000000180806
      000000E0773DF80000000473424954080808087C086488000000097048597300
      0000A6000000A601DD7DFF380000001974455874536F66747761726500777777
      2E696E6B73636170652E6F72679BEE3C1A00000342494441544889B5943B6C5C
      451486BF99B977DF0F07AF63231B5BC63128B189A20819D18480101214542821
      5428A28132281D4428A2A143A28910454249071505563005B223CC4B48310279
      C160C9C489F163EDFB9A07C5C66B2FBBCB5A044E3373E79CFFFFE79C3BE708E7
      1C9DECCD594E7774DE3527A9BD3DC5579DFCDE3F81055CB7CEB11986AD3E2128
      673208CB77C0897F25001069CDEC2F4BAD402979FAE1F16E7064D7887BB4FF5D
      A06B893CA93852E96D3997F26077EB2AE02BC991BECA81C8DA9938F751F50981
      7CB59D73EC81E1B3BBFB9F6FDFA1944E93F57D56B6B618BF2BEA9CBBB9F8FB6F
      DFB72587CB9E80517067DB0540FD157952D25FC8E32B0F250503C502B1314821
      9008D5092F84BBD228A4D6BABE1AD314B41546445AF36710B01DC7C4C6706727
      A016450449D28873CE61AD6DE2827DAFE8F32FBF00606EFE06E1BEC64AACC53A
      47A40DDA1A8C75445AA3ADC5D8BD29B0726B85859F7EC418D3E08203FCE49E6C
      064F29FA8B45529E4249C9FDA512294F2185E806EFDE073B7142620C1B61C04E
      1C136BCD7A10102409B136DDE0DD33087582AF14B528C693122524B52822A554
      1DEDFBF72630582E0350CA1C6E9C95B399C6DEDACED318F695289FCB0390CD66
      11B2B5B6C25A8E7E3DC3E8C27CFB9B7A3EA9741A214483AB2983C71F7D0C8093
      8FEC4DDE07C537ACBA11B6B88F89F9EB3CFFE13B005CBBF02ECBA3C7E8659982
      58CD5439445F6F85BEDE4A13575306009E348C94FF00602C5DE565719133F232
      00DBC543F592288F305F02E0BCBCC0797571B8476D208463A4BCC2DF736F1278
      69F2332E9DBACAD4E04DB64C0187A0E6EA834E99A44E9ECDE3472102C7067DC4
      E44C60333C3B36C7A553D77866EC466781F5B0505F8322AF1F7E0F816342CC30
      157FC20BEFBF85349A5C6D833357DEE0297195211648B3AD5EAB7CD0C06E46F9
      F602C3A55B3C373E0BC02BC73F66C85F6E041D0B679076EFCD1736D77828996B
      7C1FCFFEC0B9C969005E9C98A698DA691598EC5FC497F51952C9AE37DD42D1DA
      5072DF9912869C5F1F2F8554C0D1BE5F5B0576C9FF0BDBCFE525F0695AB82757
      839E81E9EAC901004F68958B86525EAA3700D0A1D24BA7C79B9B32F4CCA27F42
      0967ADB66BA58DEA606DD7558BF34B52B835CFE3DBBF00AC1C36615190498500
      00000049454E44AE426082FFFFFF1F042A05000089504E470D0A1A0A0000000D
      4948445200000018000000180806000000E0773DF80000000473424954080808
      087C0864880000000970485973000000A6000000A601DD7DFF38000000197445
      5874536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A00
      0004A7494441544889B595596C545518C7FFE7CE7A67E9EC6BA716E994D2A60E
      2D9421654A150891A82C11088B0FC610DC3031D12740128DFA46624834B1A8D1
      171E78102161312CB522D4B2B694D63453A1CB4C3BC34C3BCCD2B93377E6DE7B
      7C289852D099C47892F372BEEFFFFF9DEF7C27F908A514FFE762E61F10421C84
      10EBD39209212A39215B5C156C979C9037CA22504AF1B00A87B3DA7BC8E8A8CC
      78FD2F707A8BF3F539B180CFEBECF179DD799FC7221DDEECA7B5567D1A00FB28
      E79F3663F354BFE25A5837EEF5F9C7FDEBB77DC028941A93D3A366B5EC0642C8
      F3569DEA8FC595965F776F6F5F71E48B37552AB592E8550AB45459755AB56C0B
      21C44E08594D08214FADDA68734EAED9B5D7F567FFD5F8E89DEB49A150B8BFA8
      C91F88DE0B8EF3D3E1CBEFB52E7CCDA4D5E0E7FB59A8640C569AB5D029E5381F
      9CC4C5E148B6D16D9299346AD9E044E2D0787266FF13008661DE52EB8D2BB854
      E25B4A693721C450DFD2165958FF5CF1ECD18EAFD7D7DAF66E6C706B150A1603
      D124BAC762748617A4F65A17D3E67511BD4A0151A2383330D67DE4CA50603E40
      2E8A6207808E393D492DA8F78D2C5E1668E83977B2E97A787A9217A97B249985
      BFDAC1EE0ED433CE0A8D6CAE898C2100C15242C8B394D291B9B1277E11002463
      D18F87FB6F8A7A83B9FE41AEF8BDBD82A59FBEB444BBB3C5CB382B34B31799A7
      A9B119D45E9BFEE07CAFA7025289F88981AB5DB9952F6FB54326B345D2F92200
      8892080088A438EC397A09C76EDE452A57000034384D9033CCA6B20094D2A224
      8AD76C950B5446AB63D5502C3D2D520A519C05FCD83702BB9E45A5518B1F7A82
      E085D9F3A62A8B4141486B4900008C0EF5EFBB7DE5A2E0F054BBB205A1AB7F32
      498B6211C91C0F8952BCDAB400D1740EEFAF6E844A3EDB92B5751E598DC3F079
      59004AE9B57B83BD99551B763845392B5C188AC428A5503112DE6EAB47B3C78A
      5BA1A9C7340E3D0B56216F2184284A02008000C7254A64DA0AE3EAFB197E325F
      142188020824C818826AB30E77A7D28F69DA6A9C3A13ABDC5216602C38F0D98D
      CE33855ADF325322CB5FEE199B2E0040BE90075FE4B166911BB7C3D3F301C469
      D07C541680523A3A15198F2F5FBBC1AE3698EDBF04A3F147B142B180CA0A0536
      F9AA1FD3689472C819E61956ABFF8410B2F15F0100C0E7B92FE3931394D5E957
      64782198E00A7FC744490497CF229BCB622A338353FDA3D28153370A72474D71
      F3BB070F1A6CCEEF48A979400831D42D6D8D1ACD16F1DAC5D3E7B62EF1ACDBEC
      ABD201802051F44D24A4CEBB8914AF34D0C675DBF4AEC6E58AAE63DF14E3A17B
      C90297DD272F5501A53465B639F25266CAD8DCDCBCEE4A3824685451713491CB
      84F20CAD5BB95EE56B6B320DF7F508DDE74ECEB4B2151FDEB974F63CA5340400
      252B309BCDEFD42DAAFDCA60348AE15038B2AABDBDEAF84F2766DA77EED53D98
      8A49C19BBFE5049E1F8C8F0FEF1704A193CE372C35302C164BE7AE9D3BA8C964
      0A0178B1A16171281008C42CAEAAB0DE6CDD831243A7F41349522CCB716059D6
      CCF37C87B7C66BBCD5DBCB4F47265A28A5D152FAB29AEC76BB6FF897B7786D36
      3B06070713BD7D7D17388EDB5ECABC2CC04388CB64321E502A55D6743AFD3BC7
      7187CB312F1BF05FD65F62CE4C4E76C94A220000000049454E44AE426082FFFF
      FF1F04E302000089504E470D0A1A0A0000000D49484452000000180000001808
      040000004A7EF5730000000467414D410000B18F0BFC6105000000206348524D
      00007A26000080840000FA00000080E8000075300000EA6000003A9800001770
      9CBA513C00000002624B47440000AA8D2332000000097048597300000DFB0000
      0DFB010B77359B0000000774494D4507E20B150E2216DEB758EE000001B14944
      415438CB95D4CD4B54511806F0DF1DC632293528185B48668250A44484ABA84D
      BBC216158510414814448B6A25E626087251BB5611142D5BF907F4E1AE8802A1
      84028D424782A4FC0EE7B698D3F1CE9885EFBB39CF73DE87FB7E9D9BC85A62A3
      6A4B2D5AD3BAA4AB7CB23224679D96575B259F366839E03A379097CF0AE63382
      CB98319611CCA3CFCDACA0DA9A7467048D8AABBB305451F47B7BB407DF6FC2A4
      24DE0F4A13A9451F0271CF032C49634D358A065C0CB8457D1E2F5D0FC4568CE8
      508A297D9218F236E05EE7F338E876201E63AFD15843AD82A2C37A02DE572E7A
      C4FD402CE095D331A506CFF1DA4CC0679CCAA3416720DEA1D3DD8CA0C1826DDA
      03DE59FEC28638BC1C6A1CAFEA63ADC670DA546EEB8A5FFAEB2E0D64710EA969
      CB26A466F1C5093F5DF5C84747FCC0849229BF7C375B4E69D853673DD16B1CDB
      1D55E7A426CD7A6D31EFA16E6F741857AF67AD9496FE9512F4BB622E9639ACCF
      B9380DBEB9E6D64A5F2819B7C3D718F0CC802E9F231EB3CB68765B73FA4D698B
      0117F46BD612F101738E55AE77ABD64CE70BEE54CDE2D09FC3BA9F68A290416D
      5EAC8A28DA6D73565029FFEF6FE6379CA79EA69829935A000000257445587464
      6174653A63726561746500323031382D31312D32315431343A33343A32322B30
      313A3030AB52FF2D0000002574455874646174653A6D6F646966790032303138
      2D31312D32315431343A33343A32322B30313A3030DA0F479100000019744558
      74536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A0000
      000049454E44AE426082FFFFFF1F04FE02000089504E470D0A1A0A0000000D49
      4844520000000C000000160806000000F4387D1A0000000467414D410000B18F
      0BFC6105000000206348524D00007A26000080840000FA00000080E800007530
      0000EA6000003A98000017709CBA513C000000097048597300000EC400000EC4
      01952B0E1B00000002624B47440000AA8D23320000000774494D4507E20B150E
      2216DEB758EE0000002574455874646174653A63726561746500323031382D31
      312D32315431343A33343A32322B30313A3030AB52FF2D000000257445587464
      6174653A6D6F6469667900323031382D31312D32315431343A33343A32322B30
      313A3030DA0F47910000001974455874536F667477617265007777772E696E6B
      73636170652E6F72679BEE3C1A000001CC49444154384F95534DCB6961145D38
      BEF291814249926260C8401473BFC0C00F3033604C323252928994A152CC1592
      6268606E20A10C483E0678EE797678DFB773BAF7BEAB76EDB5CFB3CED97B3DFB
      603A9D3200FF0C9BCDC6389422F91584E7F34989C56241369B854AA522FEC6E5
      7241B95CA6FC7EBF4398CFE7448C4623DC6EB7AC40AFD7535E2A9520502662B3
      D9A0D7EBC90A0E8703C419882BF8D0E170187EBF1F9D4E475690482420CE4B2F
      45B3D9FC38A1D1689856ABFD116AB5FAE352BD5E67180C0654080402ECF17890
      75DF713E9F99DD6E27C16AB562C27ABD16CF038BC5023E9F4FD2D2ED76C376BB
      A51946A31104ABD54A0F42A110DAED36140A05F1378EC72362B118E5C16010C2
      F57A25C2EDCD6432B2021E3A9D0EFBFD1EDC99CFD07F0B3E43BFDF67CADD6E27
      F2FFC3643281D2603010713A9DE876BB30994CA8542A48A552F07ABD180E8730
      9BCD74C6E1708017E893DCF3743ACD4497582412611E8F870982C092C92413E7
      A29644C7186AB59AA4DFF7657D0F2EC8E7F3D2F58E46A3B464AD564B72271C12
      413C1E47A150C06C3683CBE57A55BF2011341A0D148B4588FD63B95CBEAA5F90
      08F81AE4723954ABD557E5277EFD8B623C1E4B1C910BEED2E974627F008C3A00
      BFDF6D233C0000000049454E44AE426082FFFFFF1F047201000089504E470D0A
      1A0A0000000D4948445200000018000000180806000000E0773DF80000002063
      48524D00007A25000080830000F9FF000080E8000052080001155800003A9700
      00176FD75A1F90000000097048597300000DD700000DD70142289B7800000021
      744558744372656174696F6E2054696D6500323031383A31313A30382031303A
      31313A3536CBBA8972000000CB49444154484BCD96610A83300C8575BF77D49D
      C31B6C77D9EF1DCCE51303B6A46DD276B0074F6262BE8782E2BAEFFBF22FBA8B
      DFA7A9A70AE047CCED62EA6921397C6A4809AE1E0AB1E09C5BBD7048094EBF36
      73C903E80EA92DAEE2ED347538A4057F9E3D4C1D0A89C0D5EE901EB8DA15C2AB
      6F0D2C38E756AF14023B0968C1E9D76679C811409302B7E0AA56C89597C80357
      45AE3D145E10B9777AE0AAE6EE085C5565F0EA9B83A0AC90ED2687AB5EE28798
      6154ECB00B2311C9D78FD8A812DECFFF2AF2473459CBF205E153C4F7688A39D0
      0000000049454E44AE426082FFFFFF1F046B01000089504E470D0A1A0A000000
      0D4948445200000018000000180806000000E0773DF8000000206348524D0000
      7A25000080830000F9FF000080E8000052080001155800003A970000176FD75A
      1F90000000097048597300000DD700000DD70142289B78000000217445587443
      72656174696F6E2054696D6500323031383A31313A30382031303A31363A3436
      4F76808A000000C449444154484BB596510E83201044B1DF3D690FE209EA61FA
      DD83D11DC2244815765998642251F6BD68A2718B31869579E4E3D26CD23D176B
      6FFE7858E039A187D423C12C18E4EDF5237A49DFD2110966300BC629B575E44E
      BA0C8F443D3B2231CF5806D47B9FD24F2ED69AC1D69E9A9716DCF495F6243D38
      183C0FF64980F6245A389A0457175A92B22D3819292312359CB148CC70A62729
      3F626638A3191C86332D801BCEDC81A6C0992B4959179CB9934C8133B5642A9C
      0110AF3EAA862FFE6D09E1070558C0F58EF4F2640000000049454E44AE426082
      FFFFFF1F047201000089504E470D0A1A0A0000000D4948445200000018000000
      1808040000004A7EF573000000097048597300000DD700000DD70142289B7800
      0000206348524D00007A25000080830000F9FF000080E8000052080001155800
      003A970000176FD75A1F90000000F84944415478DADCD33F2B05601805F0DF35
      29299FC52C33B151AC06AB2493249BA20C963B580C7750D770074ACA27B8DF82
      81E4E272FFDFE1B1BCEAEAFEDFE4BCDBE99C9EF739E77D33613C4CF87386DE98
      74E3DECCE8862B4D6DD7A3CEBFD01042CBE528F23335914ED3F930F991AA109E
      3D09A1E174907C4F45086F962D781142DD613FF9964F2194AD83152521D4ECF6
      926F280BE1CB661757E9E01256BD0BA16AE717BF9D2EF961AD935EF49A861F74
      4DDE4F31942CFD50732991BA939EBB1DABA7E4E661D6630A30DB37BD6CAAF2C1
      2CC5D46A6E603F392D2114C96B6B290C7D01056D2D79A6DDB93535D49074997F
      F045C7367C0F007E416FA5FCE137A30000000049454E44AE426082FFFFFF1F04
      3101000089504E470D0A1A0A0000000D49484452000000180000001808040000
      004A7EF573000000097048597300000DD700000DD70142289B78000000206348
      524D00007A25000080830000F9FF000080E8000052080001155800003A970000
      176FD75A1F90000000B74944415478DADC93BB0D833014450F11252D4B30055B
      C0168CC11448DE02B1041D2D7D3A0A244C816EAA44021B934829A2B8B37DDEC7
      F75D47E2B375E3E70220A1A323799F6BB058CC6580C16269604088853A88D72C
      083140C9841033D5295E3123C444B9DF165EBC70533E0BDEC91D3CE7EE6BDAB0
      22C448B6C3334684588FB2C4B46C08D193BE4E537A84D868898FA5DD4B7F9240
      F9B3364F1E1812C22B61586ACF90AE86E9B1C1955D1CA399AF583EFA833FFD18
      00F506771B7E1113A40000000049454E44AE426082FFFFFF1F04910600008950
      4E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C3B00
      00000467414D410000AFC837058AE9000000097048597300000EC300000EC301
      C76FA8640000001974455874536F6674776172650041646F626520496D616765
      526561647971C9653C00000021744558744372656174696F6E2054696D650032
      3030373A31323A32322031383A31383A3338CF77EBA1000005E149444154484B
      B5948B53D45514C7CFEFF7DBB7ECB22B0888862822C4434D8C9D74C66C440D74
      0C41544C13CB145F68A013DA4C2E8AA8839A4E9A4DE2A3F0456935E338A5A013
      6226E904F234C045319075D555D865977D9DCEFDAD93FA077476CECC6FF67EEF
      E79E7B1E974344F83F4C0427254D01B7DB0DFEFE0365010183F22322A252FCFD
      B561566BAFA5B6B6A6FAEAD5CBA516CBE3CABE3EBBF7F93E080E0E86E4E46471
      9FCBE502994C061E8F07480FCDCDB741C2441CC781C56201AF9757C5C4BC91B9
      72E5EA689D4E4E9B60B0C5628D3977EEFC82DDBB0BB73735356C25F97F70B68F
      3903BADDAEB0C8C8A8559D9D5D8D4D4DCDA5C0229E3A3529603C99542A018D46
      3369E3C6820EB3D9865E2FA2C783E876239696FE88C1C1216B9F332128280816
      2D5A04191919909B9B9B5D5959D55953D38E3367CE69A0E518119C95F5D18E23
      474E637CFC98CD6C135D4BBF62656E4B6B5B27F6F4F4A1D5EA40B3D98A5959D9
      7769398E690203032135F53D484949D1555494DF250E1EF8EA04FAA935C76839
      8467A2E1C347256566CE83E2E22F0DD1D171454EA7B3FA70C9FE5907F6EF37DA
      6C4E7038FAE9300924EA270C55281413D99ED1A3E3D9F5B98484F139562B0CCB
      CFDF01070FEEAAB3F6F61CA7E56E31C74AE500DDD3A73DA0D727C2F6ED5FE46F
      D9B251E6723B7FF1D7EA3C4EA78B724F89F5F22097C9049EE707AAD56AF0F353
      098989137746458DCB2B28D854535373E328A1FE2067A9F015AFBBBBB3BDA7C7
      36C26EEF83B16347735B0BF7E43E34995624248C53B18A3B1C1E90CB01EC7607
      158B97868404C1E4C9D3F6858484AF2A28C8BFD9D050FB39612E927B188F99D8
      6E7AFD5B3906C3EE7D912323A0CF6E05995C01022F407FBF033C5E0FF0821424
      943439D10F957CDDEE76D95B626212920A0B3755198D6D06E254FA702F4C044B
      A5826E76DA825339399F4E0F0C081081885E5F2BD1BAC56482D0F061A0D3A9A0
      B9A1118C77BB60F7AEADD79B9B1BD610E3A60FF5AA090683018A8AB639DADB5B
      AFB6DD6919AED10C8C5228941C9D096C1CDA2E9C876765DFC063B3094C721598
      1E9A61546438E8B44103EAEB6B6ED86CB63A89440282C08384AEC55330D4A620
      B69B4C21078DCE1F389E536BB4DA25B1F163CB12274CBA149BA03F9F3D66ACD9
      52761C2F2DCA44C38C54BC7AE316EB2CB4F5B971EFDE92DE21435E5BC52294C9
      A4942A292889C50E11C183287743B5FEA0A501A11A893E1A40FB8E525E50B17A
      799FF9F4716CCC48456A503C99BB1ECD3D0E74B9DC687778F0D0A153EEF0E123
      F2D8ED594B2A95541F163D3B6D71BF13FAFAFBC559E5A534F32E674844F4A8E2
      B9EB721606040680F1DBA3D07ABD1A7A078782C6CB81CDE600E8E740A356C192
      25F305412229DAB973B3BAE5EF26032BBA682CE23CCAE70AFA5E4ABE0C20B174
      FA94BF7AAF54E0B3B3A7B0353D197F0A50A15EED5797F6FE876D55BF37605B9B
      098D771EA0C964411A1E8A1EF1E4A973DEE8E8D82D3C054BBDFE02FC31415702
      24572C5BFCC0DB548BCF4E1CC6B6B4A9782D548333346A36CAE932A522EE83AC
      E5D557AAEAB0B5B50B8DC607D8D9F508EFDF37D1D83BF1CC990BA8D74FDC43F5
      538AE07504A6BE79B76AF5B247D87E1B2D47F6E3BDD477B03E4C83D93AB50504
      09CBA14AE0396A4D69C49CB90BABAE54D5E31D16B9B10B2F96DFC096967FB0DB
      64C3E5D9794FA84B9245F02700F1E50BE7DDC3F666EC29D98B5D696FE39D7035
      E6EBFCAC3289740741B5E462D55961381EC2D2D3337FBB7CB98622EFC6C6C6BB
      5857DF811B376DF30E0A0A3E4BD238117C2C36F207F7B50AB47F77009FCC9E88
      1D116AFC4CA7B20D9048F79128944199098220B615830B02173A7366DAAFE5E5
      7F62EDAD0E5CBFC1E0D6F86B4E922C561433F0CFB1E1F558BC013D9914E90815
      AED1289E487849112D0F1645CF8D7227827D70810D437072F2ACEF972E5B6BD7
      6AB5A52419E9539231B09E83F965C1F287674355EE24B9B4962ECB4655E753BC
      B097C1CC2504A7BCEBE806D36839C2A7F299F856D09BC02901DEA4AFD7E99568
      A2FF6F913B45C54BC6C0543CF11BE9C721FD41E6A417F05503F817868FB9DE80
      F6A6130000000049454E44AE426082FF00FF0001E6000000424DE60000000000
      000076000000280000000E0000000E0000000100040000000000700000000000
      0000000000001000000010000000000000000000BF0000BF000000BFBF00BF00
      0000BF00BF00BFBF0000C0C0C000808080000000FF0000FF000000FFFF00FF00
      0000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDDDD00DDDDD4444DDDDD00DDD4
      4444444DDD00DD444DDDD444DD00DD44DDDDDD44DD00D44DDDDDDDD44D00D44D
      DDDDDDD44D00D44DDDDDDDD44D00D44DDDDDDDD44D00DD44DDDD4D44DD00DD44
      DDDD4444DD00DDDDDDDD444DDD00DDDDDDDD4444DD00DDDDDDDDDDDDDD00FFFF
      FF1F045510000089504E470D0A1A0A0000000D49484452000000200000002008
      06000000737A7AF4000000097048597300000B1300000B1301009A9C1800000A
      4F6943435050686F746F73686F70204943432070726F66696C65000078DA9D53
      675453E9163DF7DEF4424B8880944B6F5215082052428B801491262A2109104A
      8821A1D91551C1114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E
      83A3888ACAFBE17BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C964833
      5135800CA9421E11E083C7C4C6E1E42E40810A2470001008B3642173FD230100
      F87E3C3C2B22C007BE000178D30B0800C04D9BC0301C87FF0FEA42995C018084
      01C07491384B08801400407A8E42A600404601809D98265300A0040060CB6362
      E300502D0060277FE6D300809DF8997B01005B94211501A09100201365884400
      683B00ACCF568A450058300014664BC43900D82D00304957664800B0B700C0CE
      100BB200080C00305188852900047B0060C8232378008499001446F2573CF12B
      AE10E72A00007899B23CB9243945815B082D710757572E1E28CE49172B143661
      02619A402EC27999193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE
      368EB60E5F2DEABF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A80
      3B06806DFEA225EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C
      45A190B9D9D9E5E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5
      E0BEE22481325D814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC
      1DD322C44962B9582A14E35112718E449A8CF332A52289429229C525D2FF64E2
      DF2CFB033EDF3500B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB
      6FC1D4280803806883E1CF77FFEF3FFD47A02500806649927100005E44242E54
      CAB33FC708000044A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224
      C4C24210420A64801C726029AC82422886CDB01D2A602FD4401D34C051688693
      700E2EC255B80E3D700FFA61089EC128BC81090441C808136121DA8801628A58
      238E08179985F821C14804128B2420C9881451224B91354831528A542055481D
      F23D720239875C46BA913BC8003282FC86BC47319481B2513DD40CB543B9A837
      1A8446A20BD06474319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C7
      30C0E8180733C46C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB
      89F563CFB17704128145C0093604774220611E4148584C584ED848A8201C2434
      11DA093709038451C2272293A84BB426BA11F9C4186232318758482C23D6128F
      132F107B8843C437241289433227B9900249B1A454D212D246D26E5223E92CA9
      9B34481A2393C9DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A
      9D624071A4F853E22852CA6A4A19E510E534E5066598324155A39A52DDA8A154
      11358F5A42ADA1B652AF5187A81334759A39CD8316494BA5ADA295D31A681768
      F769AFE874BA11DD951E4E97D057D2CBE947E897E803F4770C0D861583C78867
      28199B18071867197718AF984CA619D38B19C754303731EB98E7990F996F5558
      2AB62A7C1591CA0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E
      537DAE46553353E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543F
      A47E59FD890659C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86
      758135C426B1CDD97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F39466
      3F07E39871F89C744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6B
      AA96979658AB48AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C27
      47678FCE059DE753D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7
      EE989EBE5E809E4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C24
      06DB0CCE183CC535716F3C1D2FC7DBF151435DC34043A561956197E18491B9D1
      3CA3D5468D460F8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524D
      B9A629A63B4C3B4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A
      785A2CB6A8B6B86549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD
      25D6BBADBBA711A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB6
      6DB67D6167621767B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E
      73B472143A563ADE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4
      699D539BD347671767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74
      F5715DE17AD2F59D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373
      D0C3C843E051E5D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7
      B0B7A577AAF761EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4F
      C36F9E5F85DF437F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C
      21BF8E3F3ADB65F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21
      F7E798CE91CE690E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088
      581AD131973577D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A
      3CDA37BA34BA3FC62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF3
      87E29DE20BE37B17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E38
      94F041102AA8168C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4E
      F2482A4D7A92EC91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A
      76206D323D3ABD31839291907142AA214D93B667EA67E66676CBAC6585B2FEC5
      6E8BB72F1E9507C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BF
      CD89CA3996AB9E2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D
      1D58E6BDAC6A39B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED57
      97AE7EBD267A4D6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F
      59DFB561FA869D1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94
      B4A9ABC4B964CF66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4
      EDF5F645DB2F97CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454
      FA5436EED2DDB561D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB550155
      4DD566D565FB49FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD0723
      0EB6D7B9D4D51DD23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6
      E223704479E4E9F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF2
      9A469B539AFB5B625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C9
      69DAE982D39367F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA
      1074E1D245FF8BE73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA
      3CFE93D34FC7BB9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F1
      16FFD6D59E393DDDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC
      4FBC5FF440ED41D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583
      CFFE91F58F0F43058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF
      269E17FEA2FECBAE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0
      EB19AFDBC6C2C61EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F
      28FF68F9B1F553D0A7FB93199393FF040398F3FC63332DDB0000000467414D41
      0000B18E7CFB5193000000206348524D00007A25000080830000F9FF000080E9
      000075300000EA6000003A980000176F925FC546000005704944415478DABC97
      5B88555518C77FDFDAFBCC38A667CCC6D1C2CB2854A3F89017244BCD46C7E6C1
      FBE4254D89204813A4B08722A4371F0CEA21ECA1A72C0D4C1123CAD24CD4BC85
      10A89829598A78371DC79C3373CEFA7A587BAFB3CF9E7334285B9CC3DE67AFB5
      F6FA2EFFEFFFFD8FA82AC9F1E1F4DA4F0496F22F860252E6263A69CD8AAF6EBE
      1DAF0DD39B0596363664B91FC35AE5E4D95B6F01DD0D38BC7ADA07B93F8EAE9C
      3DAEDE6FB8F6EA716794105BEF1C8A3DD578CE792809A735B156819B1B5EA6E3
      D7DDA4025E34E0F1210FAD64C8E492C95EB5E17FE7FEB2F51C7A7D3045575206
      9CD9BF8BDAFC5FDE4B05DA66E6D3EBA33028A2A0918F126D502905814F7F021C
      AA52F22AE3F353802BA63FE76E07DCCCA90FB126AFF157051B81CAFD56ACAA5B
      171F684BF7D928515A2E028B962DD7D63777B176D972D68E1AC0A163C7993FEE
      516E24122F0ADBBEDC8F090C46043182201803228620309840DC550C084C787A
      64020CCE735B0E031B3F5A279B5BFAE8AA9E70E9244C6FEC4B554DAF22A0D4DD
      CD98F91471E015896220A5A59788B70544B45B1ACA6260606D86FE3D727475E5
      E9EC68E7FAF913281AE5CC1DF9D9869D1863C86442AAAA42C23020089CF7A535
      E24E696E1EEBCC4D58672B81B060E17C3008E9B8C083B91CD585AA289FC50D8B
      164FF5C678E09529CFF889FA5A756B2DDAAD0C4D8C818BEFEE66D5E93AEC8011
      ECBB60E837B8D183CA0351231029581554C5BFD0C660F3CF35FABA7D1650EBC0
      7A4F0C4C6DAC270C33DEBBEFBF3BE2C0670411296122F518295E51689A329A9D
      3B8E60ADD23C6DACC78DA662E553D0370C1856D7090899FC2D6E5CFCC57BD734
      758C0FA34615E1AB8344E949316316689A32C61B642303552B60C01694CBA63F
      B9F66BD476B493AD11876294AD5BF674CBF29CB993A2E7E9C335FA28D6BA03E7
      2D68F286A679ADC803EFEDE59D88070E1E3BCE82271FE34C97521DC2ECD6671C
      90132FB0C0ACB913BD61716662CCF9C5E270A251CFF8473C3069581D99EA07B0
      0AB73B95AFB7FE8091A4B3E2F9B70413D198FDFC64B66DDE8346199FDD3A3902
      2EA8ADC0033D73C28886029DB93C997C1BEDB93C612074159419ADCF12080426
      55EABE4DC66D51FDD4CCD6493E083191A15A9907AC853FA52FED5D37A8D53BDC
      39751811C818835525AFAE5F044131E1A29AD01C4584BAE75E80A009664CF702
      CF03B975FB58767A083C3292839743068E180FAA8838D29188C7935C1037A698
      0F34221ABF4E8BC88F23513605690C4C189A2534E20023B079E30EC230700D28
      CA79FCE239F39AF8E2F31D9E3535229C242F2C5CDAE20DB6957A41D806A31B2D
      A810849D8E07A2F8B52E6AEE56E7E2DB2CCC7DA1B90C098B5FE8F9C9DE050300
      97A49E5CFB75B21DB7A9AD313E779BD67F0B02F397B4B0E9D3ED15454F7A3E36
      72FE92162F60D2824454D5F1C08AD7223D50E0C4B1E32C9CF8043FBDF40D8846
      ADB79C324AB757AF918A8F050FCAABAB07F1F39976D6ECB923F7E481D0382644
      852D1BB697D5DE9A68875615B58AAAF5E08B71F0E22BB3A27BA9CC03611B8C1A
      AEE4EE7491C9B771E1B7A39E48E62E7E2E213FB48CBFC552F3B192B82CDDDCC3
      0F18AE68F76E68923FCE31906BB6868E4227D57DEA7D99C52C06C5D61CD7B446
      DE7B13547D39C6A5D7900D18D43B8C34A194E781C2C73FB2EA541DF9FEC3D97B
      5EC8F6A977E253D4A95DD1040F2472EF432D9E7CE2FB4084A1B521FD7A06451E
      48E5A0B21E3092E86CC5A02B95F49D0BB900D9DE196A02A83690C9186E1412D1
      E36E3C30DC4D87DA5EA2074A135D140449DD5F5365A80E0D99007A04EE71C142
      DE969A9F2622490B8437C6576B5D36C3FD1A57DAF2BC7FA0432AFE39B5CA81CB
      37BBC6A77B7C39E15DE6FF70C92F8DA296DA75A05B04446430500734F0FF8CDF
      81ABAA7AF6EF010031D927965D1348FD0000000049454E44AE426082C0C0C000
      016E040000424D6E040000000000003600000028000000130000001200000001
      001800000000003804000000000000000000000000000000000000C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0808080808080808080C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00
      FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF
      0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0
      C0C080808080808080808080808080808080808080808000FF0000FF0000FF00
      808080808080808080808080808080808080808080C0C0C0000000FFFFFF00FF
      0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000FF0000FF00808080000000FFFFFF00FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000FF0000FF00808080000000C0C0C0FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF00FF0000FF0000FF00FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00
      FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF
      0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00
      808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0000116020000424D1602
      00000000000076000000280000001A0000001A0000000100040000000000A001
      000000000000000000000000000000000000000000000000BF0000BF000000BF
      BF00BF000000BF00BF00BFBF0000C0C0C000808080000000FF0000FF000000FF
      FF00FF000000FF00FF00FFFF0000FFFFFF007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777888888888888888887777700
      0000777911111111111111111877770000007779111111111111111118777700
      0000777911111111111111111877770000007779111111111111111118777700
      0000777799999999999999999777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      0000777777777777777777777777770000007777777777777777777777777700
      00007777777777777777777777777700000000800000016A080000424D6A0800
      000000000036000000280000001C000000190000000100180000000000340800
      0000000000000000000000000000000000008000008000008000008000008000
      0080000080000080000080000080000080000080000080000080000080000080
      0000800000800000800000800000800000800000800000800000800000800000
      8000008000008000008000008000008000008000008000008000008000008000
      008000008000B3CBB9B3C9B9B3C9B9B3C6B90080000080000080000080000080
      0000800000800000800000800000800000800000800000800000800000800000
      80000080000080000080000080000080006AA26E1F7023025C06036C0A036508
      036408015A040140032E603195B09A0080000080000080000080000080000080
      0000800000800000800000800000800000800000800000800000800000800077
      B07C1074130883160EA5260EB4250CB3220AB21C07B01706AF1302A70C018307
      014C041F55210080000080000080000080000080000080000080000080000080
      000080000080000080000080000080004B9A4F067D1115AA3318B83A15B63412
      B52F10B42B0CB3230BB32108B11907B01604AF1001AD0B017D0701450395B09A
      0080000080000080000080000080000080000080000080000080000080000080
      005AA75D0782131DB6471DBA471BB94218B83C17B73913B63112B52E0FB4280C
      B3220AB21E07B01706B01503AE0D019809014503A4BBA9008000008000008000
      00800000800000800000800000800000800078BB7C09881424BA5523BD5421BC
      501EBA491CBA461AB93F44C560F0FAF2F0FAF24CC7600DB3240CB32109B11B07
      B01605AF12019309104E12008000008000008000008000008000008000008000
      008000008000108A1225B55529C06128BF5E25BE5723BD5320BB4D48C76AF1FB
      F3FFFFFFFFFFFF88DA9712B52F0FB4290CB3230BB21F08B11807B01503770B5B
      895D0080000080000080000080000080000080000080006AB96D129D2A2FC26C
      2FC26B2BC16429C06026BF5B4DCA75F1FBF4FFFFFFFFFFFFF1FBF335C15416B7
      3713B53111B52D0EB4250CB3220AB21C07AA1601510400800000800000800000
      80000080000080000080001F9C2126B8582FC26C2FC26C2FC26C2EC26952CC80
      F2FBF5FFFFFFFFFFFFF1FBF448C76B1CBA4519B83D18B83A15B63412B52F10B4
      2B0CB3230BB32104780D78A27C008000008000008000008000008000D1E9D601
      93022FC26C2FC26C2FC26C2FC26C56CD87F2FBF6FFFFFFFFFFFFF2FBF54DCA76
      22BD521FBB4B1DBA471BB94218B83C17B73813B63112B52E0FB4280890173D7D
      40008000008000008000008000008000B3DEB80DA11D2FC26C2FC26C2FC26C56
      CD87F2FBF6FFFFFFFFFFFFFFFFFFBCEBCD93DFAE91DEAA91DEA98FDDA58EDDA3
      8DDCA18BDC9D52CA6C15B73512B5300DA0231068130080000080000080000080
      00008000B3DFB80DA61D2FC26C2FC26C56CD87F2FBF6FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8CDCA019B83D
      17B83A12B02F016404008000008000008000008000008000B3E1B80DA91D2FC2
      6C2FC26C56CD87F2FBF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF8EDDA31DBA461AB94015AE34016C04008000
      008000008000008000008000B3E1B80CAA1734C36F2FC26C2FC26C56CD87F2FB
      F6FFFFFFFFFFFFFFFFFFD8F4E397E1B597E1B597E1B597E1B594E0B194E0AF92
      DFAD5ACE7F20BC4E1DBA4816A6362E8631008000008000008000008000008000
      00800001A9024ECA7A43C8782FC26C2FC26C56CD87F2FBF6FFFFFFFFFFFFFFFF
      FF7DD9A32FC26C2FC26C2FC26C2FC26B2CC16629C06027BF5C24BD5622BD5212
      9B2D4C9A4F0080000080000080000080000080000080002EB9304CC66164D18C
      4FCB8039C5722FC26C56CD87F2FBF6FFFFFFFFFFFFFFFFFF7DD9A32FC26C2FC2
      6C2FC26C2FC26C2EC26A2BC16329C05F26BE5907841295C49A00800000800000
      800000800000800000800095DB9913B51687DBA373D5955BCE8645C87A31C36E
      56CD87F2FBF6FFFFFFFFFFFFFFFFFF49CA7F2FC26C2FC26C2FC26C2FC26C2FC2
      6C2CC1671FB04A10821200800000800000800000800000800000800000800000
      80002EBE2F5CCD6696DFAC81D99E69D38F54CC823BC57456CD87F2FBF6FFFFFF
      FFFFFF63D1902FC26C2FC26C2FC26C2FC26C2FC26C2FC26C0A8D17A4D0A90080
      0000800000800000800000800000800000800000800000800001B40192DD9AA2
      E2B38CDCA677D79860D0894CCA7E4ECB81B1E8C897E1B52FC26C2FC26C2FC26C
      2FC26C2FC26C2FC26C129E2A3DA33F0080000080000080000080000080000080
      0000800000800000800000800095DE990EBA0E94DF97AEE5BB9BE0AF82DAA06E
      D49256CD8343C8782FC26C2FC26C2FC26C2FC26C2FC26C2FC26C12A22A2EA231
      0080000080000080000080000080000080000080000080000080000080000080
      00008000A4E2A810BC1059CF5AB3E7B8A7E3B691DEA97CD89B64D18C4FCB8039
      C5722FC26C2FC26C24B9520A9E162EA730008000008000008000008000008000
      0080000080000080000080000080000080000080000080000080000080005BCF
      5D0EBB0E4AC94C6BD17476D5837ED89763D08241C46422B7420AA81610A31177
      C97B008000008000008000008000008000008000008000008000008000008000
      00800000800000800000800000800000800000800000800078D77A3DC53E10B5
      1001B10101AD022EB8304CBF4E95D69900800000800000800000800000800000
      8000008000008000008000008000008000008000008000008000008000008000
      0080000080000080000080000080000080000080000080000080000080000080
      0000800000800000800000800000800000800000800000800000800000800000
      800000800000800000016A080000424D6A080000000000003600000028000000
      1B00000019000000010018000000000034080000000000000000000000000000
      0000000000800000800000800000800000800000800000800000800000800000
      8000008000008000008000008000008000008000008000008000008000008000
      0080000080000080000080000080000080000080000000000080000080000080
      00008000008000008000008000008000008000008000008000B3CAB7B3C8B7B3
      C9B7B3C7B7D0DDD5008000008000008000008000008000008000008000008000
      0080000080000080000000000080000080000080000080000080000080000080
      0000800095BD992E7B3101580403670A0367080364080162060149031F542169
      8D6C008000008000008000008000008000008000008000008000008000000000
      0080000080000080000080000080000080000080001F7C2204730A0C951F0EAF
      270CB3230BB21F08B01807B01503AE0F01980901680610491278997B00800000
      8000008000008000008000008000008000000000008000008000008000008000
      00800095C49903740810972718B83B16B73712B53011B52D0EB4250CB3220AB2
      1C07B01606AF1302AD0C0198090153045B825D00800000800000800000800000
      800000800000000000800000800000800000800095C59A037B091BAE411DBA48
      1CBA4419B83D18B83A15B63412B52F10B42B0CB3230BB32108B11907B01604AF
      1001A60A0154045B825D00800000800000800000800000800000000000800000
      80000080000080001085121EAE4624BD5622BD521FBB4B1DBA471BB94152CA6D
      F1FBF3F0FAF23EC3550FB4280CB3220AB21E07B01706B01503A80D01580486A5
      8A0080000080000080000080000000000080000080000080003CA03F17A1362A
      C06229C05F26BE5923BD5421BC501EBA498EDDA3FFFFFFFFFFFFF0FAF23FC356
      11B52C0DB3240CB32109B11B07B016059C101054120080000080000080000080
      00000000008000008000008000048B0A2CBE652FC26C2CC16729C06128BF5E24
      BE5623BD534AC86FF1FBF3FFFFFFFFFFFFF0FAF23FC35712B52F0FB4290CB323
      0BB21F08B01803710A789F7B00800000800000800000000000800000800077C1
      7B12A22A2FC26C2FC26C2FC26C2EC26B2BC16429C06026BF5B23BD554CC974F1
      FBF4FFFFFFFFFFFFF1FBF342C45C12B53011B52D0EB4250CB3220795161F6622
      0080000080000080000000000080000080003CAC3F21B34B2FC26C2FC26C2FC2
      6C2FC26C2FC26C2DC1682AC06228C05F25BE584CC974F1FBF4FFFFFFFFFFFFF1
      FBF343C55F15B63412B52F10B42B0CB323015704008000008000008000000000
      008000008000109E1124B7522FC26C2FC26C63D19097E1B597E1B597E1B596E0
      B494E0B094E0AF92DFABBAEAC9FFFFFFFFFFFFFFFFFFF1FBF344C56017B73813
      B63112B52E05750E008000008000008000000000008000008000019C022FC26C
      2FC26C2FC26C97E1B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFF1FBF344C56118B83B15B73506791000800000
      800000800000000000800000800001A1022CC0652FC26C2FC26C97E1B5FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFD5F2DE2BBE531BB94318B83C077F1200800000800000800000000000800000
      80001FAE202BBC562FC26C2FC26C63D19097E1B597E1B597E1B597E1B597E1B5
      97E1B597E1B5D8F4E3FFFFFFFFFFFFFFFFFFD6F3E031C16022BC521EBB491DBA
      4604760C0080000080000080000000000080000080004BC14E33BE4D4CCA7E34
      C36F2FC26C2FC26C2FC26C2FC26C2FC26C2FC26C2FC26C7DD9A3FFFFFFFFFFFF
      FFFFFFD8F4E337C46C28C05F25BE5823BD541EB7490172040080000080000080
      0000000000800000800095D89920B6286ED49256CD8343C8782FC26C2FC26C2F
      C26C2FC26C2FC26C7DD9A3FFFFFFFFFFFFFFFFFFD8F4E33CC6752FC26B2CC165
      29C06027BF5C17A3374C9D4F0080000080000080000000000080000080000080
      0010B41067D07B7CD89B64D18C4FCB8039C5722FC26C2FC26C56CD87FFFFFFFF
      FFFFFFFFFFD8F4E33CC6752FC26C2FC26C2FC26C2EC26A2AC06206840FA4CEA7
      00800000800000800000000000800000800000800078D37A1FBA239DE1B085DB
      A173D5955BCE8645C87A31C36E63D190FFFFFFFFFFFFD8F4E33CC6752FC26C2F
      C26C2FC26C2FC26C2FC26C1BA63E3D9E3F008000008000008000008000000000
      0080000080000080000080002EC12F45C649ACE5B996DFAC7ED99D69D38F54CC
      823BC57497E1B5B1E8C83CC6752FC26C2FC26C2FC26C2FC26C2FC26C26B75810
      8E12008000008000008000008000008000000000008000008000008000008000
      0080002EC32F4CCB4DB8E8C1A2E2B38CDCA677D79860D0894CCA7E34C36F2FC2
      6C2FC26C2FC26C2FC26C2FC26C21B34B04910A95CD9900800000800000800000
      80000080000000000080000080000080000080000080000080002EC42F27C227
      94DF97AEE5BB98E0AD82DAA06ED49256CD8343C8782FC26C2FC26C2CC06515AB
      30109B11A4D5A700800000800000800000800000800000800000000000800000
      800000800000800000800000800000800078D67A10BC1027C02768D06D7BD686
      7BD79174D59146C6672FBC4F12AF2A04A1095BBD5D0080000080000080000080
      0000800000800000800000800000000000800000800000800000800000800000
      800000800000800000800095DD984CCA4D2EBF2F01B10101AE0110AF113DBC3F
      86D0890080000080000080000080000080000080000080000080000080000080
      0000000000800000800000800000800000800000800000800000800000800000
      8000008000008000008000008000008000008000008000008000008000008000
      008000008000008000008000008000008000008000000000FFFFFF00010E0600
      00424D0E06000000000000360000002800000016000000160000000100180000
      000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFA99E8E646136988776FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF0000FFFFFFFFFFFFFFFFFFFFFFFFF2F1EF65673D41AE48527735AA9E8EFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFB3AF9E4D823645DC7844C76451
      6F2FB7B3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFEFEFD6D754749B45762E396
      5BE29246BE5D52692BCCCCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFD3D6C9457E2E5FDC
      887AE6A871E2A062E69948B859536E2EDADED2FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF7B8E5E4A
      AD5186EEB592ECBB85E8B17CE6AB67E9A046B252557230EBEEE7FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFECF0E9
      407B2669DA8CA6F5CF83EBB088EEB88EECBB81EBB365EAA043A9465F7F41F6F8
      F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
      FF92AC8041A33EA5FAD18BEDB63BA83E4CBF5F83F1B993F1C483F0BB64EAA03F
      A43E688A4DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF
      FFFFF9FAF845822A6ED88C9EFCD03FAF464C7E2F427F2642B44B75EDAC90F6C9
      83F2C15FE89A399A3081A36DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF0000FFFFFFB4CDAB399D3680F8BB49BB57488830F9FAF8E8EEE4599041349F
      3060E08D85F8C97FF7C859E69334942792B583FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF0000FFFFFF57984549D16D48CC653B8926E7EFE4FFFFFFFFFFFFFF
      FFFF7DA96D2B911D4DD16F78F8C079FDD053E1892F8E1EB2CEAAFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000FFFFFF2F97253EC74D348F25E1ECDEFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFBAD3B3348E213ABB4967F4AE71FFD44EDF842C9018BFD9
      B9FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF449D39319523D0E4CCFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9F2E74C9A3A2CAB2B58EC9A6BFFDA49
      D876298F16DAEBD8FFFFFFFFFFFFFFFFFF0000FFFFFFF1F7F0EBF4EAFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7CB872229E1A
      47DC7769FFCE46D46F2F9620E9F4E8FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFB4D8B026981937C85265FEBF3ECE60389D2EF9FCF9FFFFFF0000FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFE7F3E63DA53827B83655EF9433C44655B252FFFFFF0000FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7BC37B1BAA1F3FD95B1AAD1CFFFF
      FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4DFB41BA61B28
      AC28FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF00010E060000424D0E0600000000
      0000360000002800000016000000160000000100180000000000D8050000130B
      0000130B00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF9E9ED53030
      B82E2EB8A0A0D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0
      B0E32A2AC13030C58080D3FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFA8A8D737
      37B83131F72727F42626B9A0A0D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      B5B5E32222BE4E4EF16262F93E3EC98C8CD5FFFFFFFFFFFF0000FFFFFF9797D0
      3E3EB53737F70101F50000EF2121EE2222BC9898D3FFFFFFFFFFFFFFFFFFFFFF
      FFAAAADE1E1EBE3F3FEC3E3EEF4545F26D6DF84545C97E7ED1FFFFFF0000FFFF
      FF3E3EAD4242FA0101FB0000F10000EB0000E91818EE1C1CBCA1A1D6FFFFFFFF
      FFFFB6B6E11A1ABC3333EC3232E83B3BEA4545F04E4EF78080FE4141C4FFFFFF
      0000FFFFFF3E3EA94848F00808FE0000F10000EC0000E60000E31111E71717BA
      A8A8DABCBCE21515B82323E62727E32F2FE53C3CEB4444F05555FA7D7DF73C3C
      C0FFFFFF0000FFFFFFC3C3E23C3CA74040EF0606F70000EB0000E70000E10000
      DE0A0AE40F0FB90F0FB41414E31B1BDF2525E03131E63A3AEA4949F46D6DF436
      36BDAAAAE1FFFFFF0000FFFFFFFFFFFFCACAE53838A83939F00606F30000E600
      00E20000DB0000D70303E00707E00F0FD81A1ADB2626E12F2FE53D3DEF6161F4
      3333BDB5B5E3FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFC2C2E13636A92F2FE9
      0404EE0000E10000DC0000D60000D10202D10F0FD51A1ADB2525E03333EA5050
      EE2E2EBBAAAADEFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFCACA
      E43131A82929E90404E60000D90000D40000CE0000CE0808D41616D92828E442
      42EB2828B8B5B5E1FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFD4D4E92828A51919E31515E74646EE4D4DEF4E4EEF4A4AEE2D2DE5
      2D2DE62020B4C1C1E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFC0C0DF2323A82222E76464FA8C8CFF8787FF8787FF8A8A
      FF7272FA3A3AEB1E1EB7ACACDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
      FFFFFFFFFFFFFFFFFFFFBABADA2929A82929ED6A6AFB9898FF8F8FFF9898FF98
      98FF9090FF9595FF7D7DFB4A4AF02424B89E9ED6FFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFB0B0D43030A93333EE7878FFA8A8FFA0A0FFACACFF
      7373F46C6CF2ABABFFA0A0FFA4A4FF8D8DFE5959F22D2DBA9595D3FFFFFFFFFF
      FFFFFFFF0000FFFFFFFFFFFFBABAD83636A63A3AF48585FFBBBBFFAEAEFFBDBD
      FF8484F60A0ADF0505DC7B7BF4BCBCFFAFAFFFB6B6FF9D9DFE6B6BF83333BB9E
      9ED6FFFFFFFFFFFF0000FFFFFFB7B7D63B3BA04343F28E8EFFC8C8FFBDBDFFCE
      CEFF9191FB1212E61111A91414A41515E28C8CF9CDCDFFBEBEFFC3C3FFAAAAFF
      7777F73737B89A9AD6FFFFFF0000FFFFFF42429E5050F29F9FFFDADAFFCECEFF
      DBDBFF9D9DFB1E1EE71616A8BDBDDDCECEE61A1AA72727E39B9BFAD9D9FECDCD
      FFD3D3FFBEBEFF8B8BF83E3EB8FFFFFF0000FFFFFF43439C4F4FF5ACACFFEEEE
      FFEEEEFFA8A8FD2424ED1C1CA8BBBBDAFFFFFFFFFFFFCCCCE51F1FA83434E9A9
      A9FCEBEBFFE7E7FFCBCBFF8C8CFA3E3EB7FFFFFF0000FFFFFFB3B3D33D3D9F49
      49F2B9B9FFB6B6FF2E2EEC2323A5B2B2D4FFFFFFFFFFFFFFFFFFFFFFFFC4C4E1
      2323A94242E8BEBEFFD7D7FF7F7FF83A3AB79494D2FFFFFF0000FFFFFFFFFFFF
      BDBDD73838A04444F23838EF2A2AA0BCBCD9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFCACAE42828A75656ED7171F73535B5A7A7D8FFFFFFFFFFFF0000FFFF
      FFFFFFFFFFFFFFB5B5D439399E38389FBCBCD9FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFC8C8E43131A92F2FAB9D9DD2FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFF0000FFFFFF1F040505000089504E470D0A1A0A0000000D4948445200
      000020000000200806000000737A7AF40000000473424954080808087C086488
      0000000970485973000000B1000000B101C62D498D0000001974455874536F66
      7477617265007777772E696E6B73636170652E6F72679BEE3C1A000004824944
      415458859D975D6C536518C77FCFE9694FE9286C6C0C8A8C0F33DC8A4C188694
      30758CA017C464A8C96E20DE180326C6C4C88D4A8818BCE1428231F1CEC4440C
      5E6020314A027261DC85F23160715533E6506144075DD6EEF4F39CD78B76DDDA
      D36EA77D6E7ADEE739EFF3FFBDEF73FA7E88520A57362A0619F660D30F841142
      40A8109D40310144D1B8808F2BB4ABB49BB4B228405442288EA1380004DDD112
      473883F0216135511FC0B8F831398AE26D20E052B8DC4C84530438C10695720F
      3022AB519C0722750A97DBCF08FBD9AC1E2C0E302C5D687C07AC2D0D18E00D15
      5B961602D12AAA79528395DCFF60B38F2E355C1D203FF2AB0E713B80BAF51272
      F0CBA2EBBF58926AE55B196D451A672A43083BE6CFC4DC10C6C55F9876A7F8E5
      088CFD5951AC9229D3404D35540AAD45719E71F13B014C8E525EF359F1D15F5D
      8BBB808814B480D912442584CD28E55F7BF638FCDB987F6E58013D078BA1854A
      D072BDADF82C8174A5729868B41356137A1E97630E71804DBDB0ADB7DA205D99
      328D3C482944A0A0F986C6A8188545A6A24D4FC779149BE2516C8A7426533784
      A31C8A038C8AA193610F0BAC70EFBCF70177C6C6011878A59FC3AFBD5A37040A
      A4A938134132ECD1B1E947AA777CF7C85BA45269BEFDFE1257AF0DD50D00A092
      8572CC42D8F4EB4078A14E8F6F580F403A9DE6E2A52B7C7D6992E0F21612A65E
      F211662DB8359603C0887DEAC8F358E02F0E759E2C4008D2940008EB855D6D51
      EB7A328CEEF5F3F9B921F4E6E7016F49DCB21577279385D63E47FF75CB2639D4
      79120095F4014B91A64448037700BAAED3D1D1853D3DE4E67587B5F81F96B455
      D2871D5BBAB1F2625EC5F6F67663C76F83B26A060805EE3B7C92F67A3460C1FD
      7ABEEDDCB11D6525B1677EAB1960DD52E752AEC4CE6985938C2B5BD5BA9255AB
      D762D551868DC13F9C4E0D5303A2B5247A76673792B8513340E7F261874F3CF6
      3D0D8D0BB5248AECE8263B7317957DE4BA8F086C5CF6BBD3AF5B67757C5C214D
      1C97E7BDA7B66CC6F019EC6CBB46C796DD457F7E1DC8FF358D58E9981AF4041A
      7669224D2934F3B44EBB4A332267501C7603E0F57AD9BEAD8BD4C321FAB63E53
      127B617BFEB7E5FA9B8B27F2E5A244D4F4C2DB71D3C72863ABA3EF85CBC37CF2
      C58F253E65DBC4A7A72A6AAD08C2C523B7E71CA2B002996EEFAEC4CDFC761C56
      138CC82914EF97F49C3C827A18849CA7C4DD17F072A3BD839F465B0038FCDC1D
      56065344EF1BC577BE1A6C2695C92FD5AFF7959E45C5C80E7A77256E02E8456F
      8013CCB097F9A7228F8DD61CC72E8308FAB3AC694C16DB5BDBA6D8D41AA7F789
      399173BF3493CA8088D0D7199B13F75A0909EA2FCEB6E70036A81423B2DF7128
      AD0231F0F4DF00849627D9D41AA7DC8EBF7C97CF7E58C3DE2D53AC68C8E69D9A
      6DC9925C0FDB12C55AB93F965B9A03A226D36C4BF3A707E831BF99EFAEED6252
      278478AD842CC9F51049DC2E8F55DE8C36AB0734B01BE123C02CFA0BE54077B9
      198942FC99416994B64AE2F957EAB99C2E36139A52F87251CB631F98FDDAAB33
      D67B3DB7B53635195C8FD244899D43C3148F7D4F74EB2C9A799A889A7693F67F
      AD69DE221246F8A10000000049454E44AE426082C0C0C0000116020000424D16
      0200000000000076000000280000001A0000001A0000000100040000000000A0
      01000000000000000000000000000000000000000000000000BF0000BF000000
      BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000FF0000FF000000
      FFFF00FF000000FF00FF00FFFF0000FFFFFF0077777777777777777777777777
      0000007777777777777777777777777700000077777778877777777777777777
      000000777777008877777777777777770000007777770B088777777777777777
      00000077777770B0887777777777777700000077777770BB0887777777777777
      000000777777770BB088777777777777000000777777770BBB08877777777777
      000000777777000BBBB08877777777770000007777770BBBBBBB087777777777
      00000077777770BBB00007777777777700000077777770BBBB08877777777777
      000000777777770BBBB0887777777777000000777777770BBBBB088777777777
      0000007777770000BBBBB088777777770000007777770BBBBBBBBB0877777777
      00000077777770BBBBB000077777777700000077777770BBBBBB088777777777
      000000777777770BBBBBB08877777777000000777777770BBBBBBB0887777777
      0000007777777770BBBBBBB0887777770000007777777770BBBBBBBB08777777
      0000007777777770000000000777777700000077777777777777777777777777
      00000077777777777777777777777777000000}
  end
  object sqlEmpresa: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from FGEMPRESA')
    Left = 343
    Top = 619
    object sqlEmpresaNREFERENCIA: TIntegerField
      FieldName = 'NREFERENCIA'
      Origin = 'NREFERENCIA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlEmpresaNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlEmpresaNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlEmpresaTVIA: TStringField
      FieldName = 'TVIA'
      Origin = 'TVIA'
      FixedChar = True
      Size = 3
    end
    object sqlEmpresaVIANOMBRE: TStringField
      FieldName = 'VIANOMBRE'
      Origin = 'VIANOMBRE'
      Size = 30
    end
    object sqlEmpresaVIANUM: TStringField
      FieldName = 'VIANUM'
      Origin = 'VIANUM'
      Size = 5
    end
    object sqlEmpresaVIABLOC: TStringField
      FieldName = 'VIABLOC'
      Origin = 'VIABLOC'
      Size = 5
    end
    object sqlEmpresaVIABIS: TStringField
      FieldName = 'VIABIS'
      Origin = 'VIABIS'
      Size = 5
    end
    object sqlEmpresaVIAESCA: TStringField
      FieldName = 'VIAESCA'
      Origin = 'VIAESCA'
      Size = 5
    end
    object sqlEmpresaVIAPISO: TStringField
      FieldName = 'VIAPISO'
      Origin = 'VIAPISO'
      Size = 5
    end
    object sqlEmpresaVIAPUERTA: TStringField
      FieldName = 'VIAPUERTA'
      Origin = 'VIAPUERTA'
      Size = 5
    end
    object sqlEmpresaDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlEmpresaDIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlEmpresaPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlEmpresaPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlEmpresaCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlEmpresaCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object sqlEmpresaCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlEmpresaNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlEmpresaCONTACTO1NOMRE: TStringField
      FieldName = 'CONTACTO1NOMRE'
      Origin = 'CONTACTO1NOMRE'
      Size = 30
    end
    object sqlEmpresaCONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlEmpresaCONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlEmpresaCONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlEmpresaTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlEmpresaTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlEmpresaMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlEmpresaFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlEmpresaEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlEmpresaWEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlEmpresaMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlEmpresaOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlEmpresaNACTIVIDAD: TIntegerField
      FieldName = 'NACTIVIDAD'
      Origin = 'NACTIVIDAD'
    end
    object sqlEmpresaNSECTOR: TIntegerField
      FieldName = 'NSECTOR'
      Origin = 'NSECTOR'
    end
    object sqlEmpresaTPCIVA1: TSingleField
      FieldName = 'TPCIVA1'
      Origin = 'TPCIVA1'
    end
    object sqlEmpresaTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlEmpresaTPCIVA3: TSingleField
      FieldName = 'TPCIVA3'
      Origin = 'TPCIVA3'
    end
    object sqlEmpresaTPCRE1: TSingleField
      FieldName = 'TPCRE1'
      Origin = 'TPCRE1'
    end
    object sqlEmpresaTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlEmpresaTPCRE3: TSingleField
      FieldName = 'TPCRE3'
      Origin = 'TPCRE3'
    end
    object sqlEmpresaNPEDIDO: TIntegerField
      FieldName = 'NPEDIDO'
      Origin = 'NPEDIDO'
    end
    object sqlEmpresaNPEDIPROVE: TIntegerField
      FieldName = 'NPEDIPROVE'
      Origin = 'NPEDIPROVE'
    end
    object sqlEmpresaNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlEmpresaNALBASERVI: TIntegerField
      FieldName = 'NALBASERVI'
      Origin = 'NALBASERVI'
    end
    object sqlEmpresaNALBACOLECTOR: TIntegerField
      FieldName = 'NALBACOLECTOR'
      Origin = 'NALBACOLECTOR'
    end
    object sqlEmpresaNALBALMA: TIntegerField
      FieldName = 'NALBALMA'
      Origin = 'NALBALMA'
    end
    object sqlEmpresaNALBAREPARA: TIntegerField
      FieldName = 'NALBAREPARA'
      Origin = 'NALBAREPARA'
    end
    object sqlEmpresaNFACTURANORMAL: TIntegerField
      FieldName = 'NFACTURANORMAL'
      Origin = 'NFACTURANORMAL'
    end
    object sqlEmpresaNFACTURASERVI: TIntegerField
      FieldName = 'NFACTURASERVI'
      Origin = 'NFACTURASERVI'
    end
    object sqlEmpresaNFACTURACOLECTOR: TIntegerField
      FieldName = 'NFACTURACOLECTOR'
      Origin = 'NFACTURACOLECTOR'
    end
    object sqlEmpresaNFACTURATIENDA: TIntegerField
      FieldName = 'NFACTURATIENDA'
      Origin = 'NFACTURATIENDA'
    end
    object sqlEmpresaNFACTURAREPARA: TIntegerField
      FieldName = 'NFACTURAREPARA'
      Origin = 'NFACTURAREPARA'
    end
    object sqlEmpresaNUMTICKET: TIntegerField
      FieldName = 'NUMTICKET'
      Origin = 'NUMTICKET'
    end
    object sqlEmpresaNCOMPRA: TIntegerField
      FieldName = 'NCOMPRA'
      Origin = 'NCOMPRA'
    end
    object sqlEmpresaNTRASPASO: TIntegerField
      FieldName = 'NTRASPASO'
      Origin = 'NTRASPASO'
    end
    object sqlEmpresaREGISTROMERCANTIL: TStringField
      FieldName = 'REGISTROMERCANTIL'
      Origin = 'REGISTROMERCANTIL'
      Size = 200
    end
    object sqlEmpresaPIEFRANORMAL: TMemoField
      FieldName = 'PIEFRANORMAL'
      Origin = 'PIEFRANORMAL'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRASERVI: TMemoField
      FieldName = 'PIEFRASERVI'
      Origin = 'PIEFRASERVI'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRACOLLECTOR: TMemoField
      FieldName = 'PIEFRACOLLECTOR'
      Origin = 'PIEFRACOLLECTOR'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRATIENDA: TMemoField
      FieldName = 'PIEFRATIENDA'
      Origin = 'PIEFRATIENDA'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRAREPARA: TMemoField
      FieldName = 'PIEFRAREPARA'
      Origin = 'PIEFRAREPARA'
      BlobType = ftMemo
    end
    object sqlEmpresaIMPTOPEFRAIDE: TSingleField
      FieldName = 'IMPTOPEFRAIDE'
      Origin = 'IMPTOPEFRAIDE'
    end
    object sqlEmpresaCTACONTAVTA1: TStringField
      FieldName = 'CTACONTAVTA1'
      Origin = 'CTACONTAVTA1'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA2: TStringField
      FieldName = 'CTACONTAVTA2'
      Origin = 'CTACONTAVTA2'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA3: TStringField
      FieldName = 'CTACONTAVTA3'
      Origin = 'CTACONTAVTA3'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA4: TStringField
      FieldName = 'CTACONTAVTA4'
      Origin = 'CTACONTAVTA4'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA5: TStringField
      FieldName = 'CTACONTAVTA5'
      Origin = 'CTACONTAVTA5'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA1: TStringField
      FieldName = 'CTACONTAIVA1'
      Origin = 'CTACONTAIVA1'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA2: TStringField
      FieldName = 'CTACONTAIVA2'
      Origin = 'CTACONTAIVA2'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA3: TStringField
      FieldName = 'CTACONTAIVA3'
      Origin = 'CTACONTAIVA3'
      Size = 9
    end
    object sqlEmpresaCTACONTARE1: TStringField
      FieldName = 'CTACONTARE1'
      Origin = 'CTACONTARE1'
      Size = 9
    end
    object sqlEmpresaCTACONTARE2: TStringField
      FieldName = 'CTACONTARE2'
      Origin = 'CTACONTARE2'
      Size = 9
    end
    object sqlEmpresaCTACONTARE3: TStringField
      FieldName = 'CTACONTARE3'
      Origin = 'CTACONTARE3'
      Size = 9
    end
    object sqlEmpresaTPCINCRECOMPRACESIO: TSingleField
      FieldName = 'TPCINCRECOMPRACESIO'
      Origin = 'TPCINCRECOMPRACESIO'
    end
    object sqlEmpresaTPCINCRECOMPRAVENTA: TSingleField
      FieldName = 'TPCINCRECOMPRAVENTA'
      Origin = 'TPCINCRECOMPRAVENTA'
    end
    object sqlEmpresaNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlEmpresaRUTAFTP: TStringField
      FieldName = 'RUTAFTP'
      Origin = 'RUTAFTP'
      Size = 80
    end
    object sqlEmpresaRUTAIMAGENFTP: TStringField
      FieldName = 'RUTAIMAGENFTP'
      Origin = 'RUTAIMAGENFTP'
      Size = 80
    end
    object sqlEmpresaRUTAIMAGENPC: TStringField
      FieldName = 'RUTAIMAGENPC'
      Origin = 'RUTAIMAGENPC'
      Size = 80
    end
    object sqlEmpresaRUTACAMPO: TIntegerField
      FieldName = 'RUTACAMPO'
      Origin = 'RUTACAMPO'
    end
    object sqlEmpresaRUTAAVISOSFTP: TStringField
      FieldName = 'RUTAAVISOSFTP'
      Origin = 'RUTAAVISOSFTP'
      Size = 80
    end
    object sqlEmpresaRUTAAVISOSPC: TStringField
      FieldName = 'RUTAAVISOSPC'
      Origin = 'RUTAAVISOSPC'
      Size = 80
    end
    object sqlEmpresaHOSTFTP: TStringField
      FieldName = 'HOSTFTP'
      Origin = 'HOSTFTP'
      Size = 80
    end
    object sqlEmpresaUSUFTP: TStringField
      FieldName = 'USUFTP'
      Origin = 'USUFTP'
    end
    object sqlEmpresaPASSFTP: TStringField
      FieldName = 'PASSFTP'
      Origin = 'PASSFTP'
    end
    object sqlEmpresaDIASRECEGEN: TSmallintField
      FieldName = 'DIASRECEGEN'
      Origin = 'DIASRECEGEN'
    end
    object sqlEmpresaTRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlEmpresaSWRECARGOE: TSmallintField
      FieldName = 'SWRECARGOE'
      Origin = 'SWRECARGOE'
    end
    object sqlEmpresaSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlEmpresaFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlEmpresaFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlEmpresaHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlEmpresaFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlEmpresaHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlEmpresaUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlEmpresaNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
end
