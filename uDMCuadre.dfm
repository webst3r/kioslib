object DMCuadre: TDMCuadre
  OldCreateOrder = False
  Height = 468
  Width = 712
  object sqlFraProve1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select * from FVFRAPROVE'
      'where ID_FRAPROVE is not null')
    Left = 64
    Top = 50
    object sqlFraProve1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFraProve1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlFraProve1SUFACTURA: TStringField
      FieldName = 'SUFACTURA'
      Origin = 'SUFACTURA'
    end
    object sqlFraProve1FECHAFACTURA: TDateField
      FieldName = 'FECHAFACTURA'
      Origin = 'FECHAFACTURA'
    end
    object sqlFraProve1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlFraProve1NOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlFraProve1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlFraProve1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlFraProve1EMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlFraProve1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlFraProve1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlFraProve1IMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object sqlFraProve1IMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object sqlFraProve1TPCDTOPP: TSingleField
      FieldName = 'TPCDTOPP'
      Origin = 'TPCDTOPP'
    end
    object sqlFraProve1IMPODTOPP: TSingleField
      FieldName = 'IMPODTOPP'
      Origin = 'IMPODTOPP'
    end
    object sqlFraProve1IMPONETO: TSingleField
      FieldName = 'IMPONETO'
      Origin = 'IMPONETO'
    end
    object sqlFraProve1SWIVA: TSmallintField
      FieldName = 'SWIVA'
      Origin = 'SWIVA'
    end
    object sqlFraProve1IMPOBASE1: TSingleField
      FieldName = 'IMPOBASE1'
      Origin = 'IMPOBASE1'
    end
    object sqlFraProve1TPCIVA1: TSingleField
      FieldName = 'TPCIVA1'
      Origin = 'TPCIVA1'
    end
    object sqlFraProve1TPCRE1: TSingleField
      FieldName = 'TPCRE1'
      Origin = 'TPCRE1'
    end
    object sqlFraProve1IMPOIVA1: TSingleField
      FieldName = 'IMPOIVA1'
      Origin = 'IMPOIVA1'
    end
    object sqlFraProve1IMPORE1: TSingleField
      FieldName = 'IMPORE1'
      Origin = 'IMPORE1'
    end
    object sqlFraProve1IMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object sqlFraProve1TPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlFraProve1TPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlFraProve1IMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object sqlFraProve1IMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object sqlFraProve1IMPOBASE3: TSingleField
      FieldName = 'IMPOBASE3'
      Origin = 'IMPOBASE3'
    end
    object sqlFraProve1TPCIVA3: TSingleField
      FieldName = 'TPCIVA3'
      Origin = 'TPCIVA3'
    end
    object sqlFraProve1TPCRE3: TSingleField
      FieldName = 'TPCRE3'
      Origin = 'TPCRE3'
    end
    object sqlFraProve1IMPOIVA3: TSingleField
      FieldName = 'IMPOIVA3'
      Origin = 'IMPOIVA3'
    end
    object sqlFraProve1IMPORE3: TSingleField
      FieldName = 'IMPORE3'
      Origin = 'IMPORE3'
    end
    object sqlFraProve1IMPOBASE4: TSingleField
      FieldName = 'IMPOBASE4'
      Origin = 'IMPOBASE4'
    end
    object sqlFraProve1TPCIVA4: TSingleField
      FieldName = 'TPCIVA4'
      Origin = 'TPCIVA4'
    end
    object sqlFraProve1TPCRE4: TSingleField
      FieldName = 'TPCRE4'
      Origin = 'TPCRE4'
    end
    object sqlFraProve1IMPOIVA4: TSingleField
      FieldName = 'IMPOIVA4'
      Origin = 'IMPOIVA4'
    end
    object sqlFraProve1IMPORE4: TSingleField
      FieldName = 'IMPORE4'
      Origin = 'IMPORE4'
    end
    object sqlFraProve1IMPOIMPUESTOS: TSingleField
      FieldName = 'IMPOIMPUESTOS'
      Origin = 'IMPOIMPUESTOS'
    end
    object sqlFraProve1IMPOBASEEXENTA: TSingleField
      FieldName = 'IMPOBASEEXENTA'
      Origin = 'IMPOBASEEXENTA'
    end
    object sqlFraProve1IMPOTOTAL: TSingleField
      FieldName = 'IMPOTOTAL'
      Origin = 'IMPOTOTAL'
    end
    object sqlFraProve1VTO1: TDateField
      FieldName = 'VTO1'
      Origin = 'VTO1'
    end
    object sqlFraProve1IMPVTO1: TSingleField
      FieldName = 'IMPVTO1'
      Origin = 'IMPVTO1'
    end
    object sqlFraProve1VTO2: TDateField
      FieldName = 'VTO2'
      Origin = 'VTO2'
    end
    object sqlFraProve1IMPVTO2: TSingleField
      FieldName = 'IMPVTO2'
      Origin = 'IMPVTO2'
    end
    object sqlFraProve1VTO3: TDateField
      FieldName = 'VTO3'
      Origin = 'VTO3'
    end
    object sqlFraProve1IMPVTO3: TSingleField
      FieldName = 'IMPVTO3'
      Origin = 'IMPVTO3'
    end
    object sqlFraProve1TEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlFraProve1EFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlFraProve1FRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlFraProve1DIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object sqlFraProve1DIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlFraProve1DIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlFraProve1DIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlFraProve1NALMACEN: TSmallintField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlFraProve1SWCONTABILIZADA: TSmallintField
      FieldName = 'SWCONTABILIZADA'
      Origin = 'SWCONTABILIZADA'
    end
    object sqlFraProve1TIVACLI: TSmallintField
      FieldName = 'TIVACLI'
      Origin = 'TIVACLI'
    end
    object sqlFraProve1SWTIPOCOBRO: TSmallintField
      FieldName = 'SWTIPOCOBRO'
      Origin = 'SWTIPOCOBRO'
    end
    object sqlFraProve1IMPOACUENTA: TSingleField
      FieldName = 'IMPOACUENTA'
      Origin = 'IMPOACUENTA'
    end
    object sqlFraProve1IMPOPENDIENTE: TSingleField
      FieldName = 'IMPOPENDIENTE'
      Origin = 'IMPOPENDIENTE'
    end
    object sqlFraProve1CTACONTACLI: TStringField
      FieldName = 'CTACONTACLI'
      Origin = 'CTACONTACLI'
      Size = 9
    end
    object sqlFraProve1TRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlFraProve1FECHACARGOA: TDateField
      FieldName = 'FECHACARGOA'
      Origin = 'FECHACARGOA'
    end
    object sqlFraProve1FECHACARGOZ: TDateField
      FieldName = 'FECHACARGOZ'
      Origin = 'FECHACARGOZ'
    end
    object sqlFraProve1FECHAABONOA: TDateField
      FieldName = 'FECHAABONOA'
      Origin = 'FECHAABONOA'
    end
    object sqlFraProve1FECHAABONOZ: TDateField
      FieldName = 'FECHAABONOZ'
      Origin = 'FECHAABONOZ'
    end
    object sqlFraProve1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlFraProve1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlFraProve1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlFraProve1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlFraProve1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlFraProve1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlFraProve1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlFraProve1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlFraProveS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select ID_FRAPROVE, ID_PROVEEDOR, SUFACTURA, FECHAFACTURA, NOMBR' +
        'E, NOMBRE2, ID_DIRECCION, '
      'FECHACARGOA,FECHACARGOZ,FECHAABONOA,FECHAABONOZ,'
      
        'NIF, EMAIL, MENSAJEAVISO, OBSERVACIONES, IMPOBRUTO, IMPODTO, TPC' +
        'DTOPP, IMPODTOPP, IMPONETO, SWIVA, IMPOBASE1, TPCIVA1, TPCRE1, I' +
        'MPOIVA1, IMPORE1, IMPOBASE2, TPCIVA2, TPCRE2, IMPOIVA2, IMPORE2,' +
        ' IMPOBASE3, TPCIVA3, TPCRE3, IMPOIVA3, IMPORE3, IMPOBASE4, TPCIV' +
        'A4, TPCRE4, IMPOIVA4, IMPORE4, IMPOIMPUESTOS, IMPOBASEEXENTA, IM' +
        'POTOTAL, VTO1, IMPVTO1, VTO2, IMPVTO2, VTO3, IMPVTO3, TEFECTO, E' +
        'FECTOS, FRECUENCIA, DIASPRIMERVTO, DIAFIJO1, DIAFIJO2, DIAFIJO3,' +
        ' NALMACEN, SWCONTABILIZADA, TIVACLI, SWTIPOCOBRO, IMPOACUENTA, I' +
        'MPOPENDIENTE, CTACONTACLI, SWALTABAJA, FECHABAJA, FECHAALTA, HOR' +
        'AALTA, FECHAULTI, HORAULTI, USUULTI, NOTAULTI from FVFRAPROVE'
      'where ID_PROVEEDOR is not null')
    Left = 64
    Top = 98
    object sqlFraProveSID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFraProveSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlFraProveSSUFACTURA: TStringField
      FieldName = 'SUFACTURA'
      Origin = 'SUFACTURA'
    end
    object sqlFraProveSFECHAFACTURA: TDateField
      FieldName = 'FECHAFACTURA'
      Origin = 'FECHAFACTURA'
    end
    object sqlFraProveSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlFraProveSNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlFraProveSID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlFraProveSFECHACARGOA: TDateField
      FieldName = 'FECHACARGOA'
      Origin = 'FECHACARGOA'
    end
    object sqlFraProveSFECHACARGOZ: TDateField
      FieldName = 'FECHACARGOZ'
      Origin = 'FECHACARGOZ'
    end
    object sqlFraProveSFECHAABONOA: TDateField
      FieldName = 'FECHAABONOA'
      Origin = 'FECHAABONOA'
    end
    object sqlFraProveSFECHAABONOZ: TDateField
      FieldName = 'FECHAABONOZ'
      Origin = 'FECHAABONOZ'
    end
    object sqlFraProveSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlFraProveSEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlFraProveSMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlFraProveSOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlFraProveSIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object sqlFraProveSIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object sqlFraProveSTPCDTOPP: TSingleField
      FieldName = 'TPCDTOPP'
      Origin = 'TPCDTOPP'
    end
    object sqlFraProveSIMPODTOPP: TSingleField
      FieldName = 'IMPODTOPP'
      Origin = 'IMPODTOPP'
    end
    object sqlFraProveSIMPONETO: TSingleField
      FieldName = 'IMPONETO'
      Origin = 'IMPONETO'
    end
    object sqlFraProveSSWIVA: TSmallintField
      FieldName = 'SWIVA'
      Origin = 'SWIVA'
    end
    object sqlFraProveSIMPOBASE1: TSingleField
      FieldName = 'IMPOBASE1'
      Origin = 'IMPOBASE1'
    end
    object sqlFraProveSTPCIVA1: TSingleField
      FieldName = 'TPCIVA1'
      Origin = 'TPCIVA1'
    end
    object sqlFraProveSTPCRE1: TSingleField
      FieldName = 'TPCRE1'
      Origin = 'TPCRE1'
    end
    object sqlFraProveSIMPOIVA1: TSingleField
      FieldName = 'IMPOIVA1'
      Origin = 'IMPOIVA1'
    end
    object sqlFraProveSIMPORE1: TSingleField
      FieldName = 'IMPORE1'
      Origin = 'IMPORE1'
    end
    object sqlFraProveSIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object sqlFraProveSTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlFraProveSTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlFraProveSIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object sqlFraProveSIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object sqlFraProveSIMPOBASE3: TSingleField
      FieldName = 'IMPOBASE3'
      Origin = 'IMPOBASE3'
    end
    object sqlFraProveSTPCIVA3: TSingleField
      FieldName = 'TPCIVA3'
      Origin = 'TPCIVA3'
    end
    object sqlFraProveSTPCRE3: TSingleField
      FieldName = 'TPCRE3'
      Origin = 'TPCRE3'
    end
    object sqlFraProveSIMPOIVA3: TSingleField
      FieldName = 'IMPOIVA3'
      Origin = 'IMPOIVA3'
    end
    object sqlFraProveSIMPORE3: TSingleField
      FieldName = 'IMPORE3'
      Origin = 'IMPORE3'
    end
    object sqlFraProveSIMPOBASE4: TSingleField
      FieldName = 'IMPOBASE4'
      Origin = 'IMPOBASE4'
    end
    object sqlFraProveSTPCIVA4: TSingleField
      FieldName = 'TPCIVA4'
      Origin = 'TPCIVA4'
    end
    object sqlFraProveSTPCRE4: TSingleField
      FieldName = 'TPCRE4'
      Origin = 'TPCRE4'
    end
    object sqlFraProveSIMPOIVA4: TSingleField
      FieldName = 'IMPOIVA4'
      Origin = 'IMPOIVA4'
    end
    object sqlFraProveSIMPORE4: TSingleField
      FieldName = 'IMPORE4'
      Origin = 'IMPORE4'
    end
    object sqlFraProveSIMPOIMPUESTOS: TSingleField
      FieldName = 'IMPOIMPUESTOS'
      Origin = 'IMPOIMPUESTOS'
    end
    object sqlFraProveSIMPOBASEEXENTA: TSingleField
      FieldName = 'IMPOBASEEXENTA'
      Origin = 'IMPOBASEEXENTA'
    end
    object sqlFraProveSIMPOTOTAL: TSingleField
      FieldName = 'IMPOTOTAL'
      Origin = 'IMPOTOTAL'
    end
    object sqlFraProveSVTO1: TDateField
      FieldName = 'VTO1'
      Origin = 'VTO1'
    end
    object sqlFraProveSIMPVTO1: TSingleField
      FieldName = 'IMPVTO1'
      Origin = 'IMPVTO1'
    end
    object sqlFraProveSVTO2: TDateField
      FieldName = 'VTO2'
      Origin = 'VTO2'
    end
    object sqlFraProveSIMPVTO2: TSingleField
      FieldName = 'IMPVTO2'
      Origin = 'IMPVTO2'
    end
    object sqlFraProveSVTO3: TDateField
      FieldName = 'VTO3'
      Origin = 'VTO3'
    end
    object sqlFraProveSIMPVTO3: TSingleField
      FieldName = 'IMPVTO3'
      Origin = 'IMPVTO3'
    end
    object sqlFraProveSTEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlFraProveSEFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlFraProveSFRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlFraProveSDIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object sqlFraProveSDIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlFraProveSDIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlFraProveSDIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlFraProveSNALMACEN: TSmallintField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlFraProveSSWCONTABILIZADA: TSmallintField
      FieldName = 'SWCONTABILIZADA'
      Origin = 'SWCONTABILIZADA'
    end
    object sqlFraProveSTIVACLI: TSmallintField
      FieldName = 'TIVACLI'
      Origin = 'TIVACLI'
    end
    object sqlFraProveSSWTIPOCOBRO: TSmallintField
      FieldName = 'SWTIPOCOBRO'
      Origin = 'SWTIPOCOBRO'
    end
    object sqlFraProveSIMPOACUENTA: TSingleField
      FieldName = 'IMPOACUENTA'
      Origin = 'IMPOACUENTA'
    end
    object sqlFraProveSIMPOPENDIENTE: TSingleField
      FieldName = 'IMPOPENDIENTE'
      Origin = 'IMPOPENDIENTE'
    end
    object sqlFraProveSCTACONTACLI: TStringField
      FieldName = 'CTACONTACLI'
      Origin = 'CTACONTACLI'
      Size = 9
    end
    object sqlFraProveSSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlFraProveSFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlFraProveSFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlFraProveSHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlFraProveSFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlFraProveSHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlFraProveSUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlFraProveSNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlHiUpdaMarca: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Update FVHISARTI Set SWMARCA2 = 0'
      '  where ID_HISARTI  is not null'
      '  And IDSTOCABE = -1'
      '  and (ID_FRAPROVE is null or ID_FRAPROVE = 0)')
    Left = 64
    Top = 146
  end
  object sqlAlbaAbonoP: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select H.IDSTOCABE,H.ID_FRAPROVE,C.DOCTOPROVE, C.DOCTOPROVEFECHA' +
        ',C.FECHA,'
      'sum (DEVUELTOS) as Ejemplares,'
      'Sum (IMPOBRUTO) as ImpBruto,'
      'Sum (IMPOBASE + IMPOBASE2)  as ImpBase,'
      'Sum (IMPOIVA  + IMPOIVA2) as ImpIva,'
      'Sum (IMPORE   + IMPORE2) as ImpRe,'
      'Sum (IMPOTOTLIN) as ImpTotal,'
      'C.SWMARCA'
      'From FVHISARTI H'
      ' Left outer join FVSTOCABE C on (C.IDSTOCABE = H.IDSTOCABE)'
      'where H.IDSTOCABE is not null and C.SWTIPODOCU = 2'
      'and H.clave = '#39'54'#39
      'and (H.ID_FRAPROVE = 0 or H.ID_FRAPROVE is null )'
      
        'Group by H.IDSTOCABE,C.FECHA,H.ID_FRAPROVE,C.DOCTOPROVE, C.DOCTO' +
        'PROVEFECHA,C.SWMARCA')
    Left = 64
    Top = 194
  end
  object sqlHistoP: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select H.ID_HISARTI, H.SWES, H.FECHA, H.HORA, H.CLAVE, H.ID_ARTI' +
        'CULO, H.ADENDUM, H.ID_CLIENTE, H.SWTIPOFRA, H.NFACTURA, H.NALBAR' +
        'AN, H.NLINEA, H.DEVUELTOS, H.VENDIDOS, H.MERMA, H.CANTIDAD, H.CA' +
        'NTIENALBA, H.PRECIOCOMPRA, H.PRECIOCOSTE, H.PRECIOVENTA, H.SWDTO' +
        ', H.TPCDTO, H.TIVA, H.TPCIVA, H.TPCRE, H.IMPOBRUTO, H.IMPODTO, H' +
        '.IMPOBASE, H.IMPOIVA, H.IMPORE, H.IMPOTOTLIN, H.ENTRADAS, H.SALI' +
        'DAS, H.VALORCOSTE, H.VALORCOMPRA, H.VALORVENTA, H.VALORMOVI, H.I' +
        'D_PROVEEDOR, H.IDSTOCABE, H.IDDEVOLUCABE, H.TPCCOMISION, H.IMPOC' +
        'OMISION, H.ENCARTE, H.ID_DIARIA, H.PARTIDA, H.TURNO, H.NALMACEN,' +
        ' H.ID_HISARTI01, H.ID_FRAPROVE, H.NRECLAMACION, H.SWPDTEPAGO, H.' +
        'SWESTADO, H.SWDEVOLUCION, H.SWCERRADO, H.FECHAAVISO, H.FECHADEVO' +
        'L, H.FECHACARGO,H.FECHAABONO,H.SWMARCA,H.SWMARCA2,H.CARGO,H.ABON' +
        'O,H.RECLAMADO, H.PAQUETE,'
      '  (H.VALORCOSTE + H.VALORCOSTE2) as CosteBase,'
      
        '  ((H.VALORCOSTE * H.TPCIVA /100) + (H.VALORCOSTE2 * H.TPCIVA2 /' +
        '100)) as CosteIva,'
      
        '  ((H.VALORCOSTE * H.TPCRE  /100) + (H.VALORCOSTE2 * H.TPCRE2  /' +
        '100)) as CosteRe,'
      
        '  ((H.VALORCOSTE * H.TPCIVA /100) + (H.VALORCOSTE2 * H.TPCIVA2 /' +
        '100) + (H.VALORCOSTE * H.TPCRE  /100) + (H.VALORCOSTE2 * H.TPCRE' +
        '2  /100)) as CosteImpuestos,'
      
        '  ((H.VALORCOSTE * H.TPCIVA /100) + (H.VALORCOSTE2 * H.TPCIVA2 /' +
        '100) + (H.VALORCOSTE * H.TPCRE  /100) + (H.VALORCOSTE2 * H.TPCRE' +
        '2  /100) +(H.VALORCOSTE + H.VALORCOSTE2) ) as CosteTotal,'
      'A.BARRAS,A.DESCRIPCION'
      ' from FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      'where H.ID_HISARTI is not null')
    Left = 64
    Top = 242
  end
  object FDQuery1: TFDQuery
    Connection = DMppal.FDConnection1
    Left = 136
    Top = 354
  end
  object sqlProveS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  A.ID_PROVEEDOR,A.NOMBRE'
      ', A.NIF'
      ', A.REFEPROVEEDOR'
      ''
      ''
      'From FMPROVE A'
      'where ID_PROVEEDOR is not null'
      'Order by Nombre')
    Left = 616
    Top = 368
    object sqlProveSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProveSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProveSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProveSREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
  end
  object FDQuery2: TFDQuery
    Connection = DMppal.FDConnection1
    Left = 192
    Top = 346
  end
end
