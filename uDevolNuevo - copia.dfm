object FormDevolNuevo: TFormDevolNuevo
  Left = 0
  Top = 0
  ClientHeight = 202
  ClientWidth = 384
  Caption = 'FormNuevaDevo'
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  PixelsPerInch = 96
  TextHeight = 13
  object UniDBEdit1: TUniDBEdit
    Left = 90
    Top = 37
    Width = 121
    Height = 22
    Hint = ''
    DataField = 'PAQUETE'
    TabOrder = 0
  end
  object UniLabel4: TUniLabel
    Left = 44
    Top = 40
    Width = 40
    Height = 13
    Hint = ''
    Caption = 'Paquete'
    TabOrder = 1
  end
  object UniLabel5: TUniLabel
    Left = 21
    Top = 68
    Width = 63
    Height = 13
    Hint = ''
    Caption = 'Max Paquete'
    TabOrder = 2
  end
  object UniDBEdit2: TUniDBEdit
    Left = 89
    Top = 65
    Width = 121
    Height = 22
    Hint = ''
    DataField = 'MAXPAQUETE'
    TabOrder = 3
  end
  object btGrabar: TUniButton
    Left = 215
    Top = 158
    Width = 75
    Height = 25
    Hint = ''
    Caption = 'Aceptar'
    TabOrder = 4
  end
  object btCancelar: TUniButton
    Left = 294
    Top = 158
    Width = 75
    Height = 25
    Hint = ''
    Caption = 'Cancelar'
    TabOrder = 5
  end
  object UniLabel8: TUniLabel
    Left = 9
    Top = 96
    Width = 75
    Height = 13
    Hint = ''
    Caption = 'Doc. Proveedor'
    TabOrder = 6
  end
  object UniDBEdit3: TUniDBEdit
    Left = 89
    Top = 93
    Width = 121
    Height = 22
    Hint = ''
    DataField = 'DOCTOPROVE'
    TabOrder = 7
  end
  object UniLabel1: TUniLabel
    Left = 121
    Top = 112
    Width = 75
    Height = 13
    Hint = ''
    Caption = 'Doc. Proveedor'
    TabOrder = 8
  end
  object UniDBEdit4: TUniDBEdit
    Left = 201
    Top = 109
    Width = 121
    Height = 22
    Hint = ''
    DataField = 'DOCTOPROVE'
    TabOrder = 9
  end
  object dsCabeNuevo: TDataSource
    DataSet = DMDevolucion.sqlCabeNuevo
    Left = 20
    Top = 131
  end
end
