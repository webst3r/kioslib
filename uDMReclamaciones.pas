unit uDMReclamaciones;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMReclamaciones = class(TDataModule)
    sqlProveS: TFDQuery;
    sqlProve1: TFDQuery;
    sqlReclamaS: TFDQuery;
    sqlReclamaFi: TFDQuery;
    sqlHisto1: TFDQuery;
    sqlProveSID_PROVEEDOR: TIntegerField;
    sqlProveSNOMBRE: TStringField;
    sqlProveSDIRECCION: TStringField;
    sqlProveSPOBLACION: TStringField;
    sqlProveSPROVINCIA: TStringField;
    sqlProveSCPOSTAL: TStringField;
    sqlProveSCPROVINCIA: TStringField;
    sqlProveSNIF: TStringField;
    sqlProveSTELEFONO1: TStringField;
    sqlProveSTELEFONO2: TStringField;
    sqlProveSMOVIL: TStringField;
    sqlProve1ID_PROVEEDOR: TIntegerField;
    sqlProve1TPROVEEDOR: TSmallintField;
    sqlProve1NOMBRE: TStringField;
    sqlProve1NIF: TStringField;
    sqlProve1ID_DIRECCION: TIntegerField;
    sqlProve1NOMBRE2: TStringField;
    sqlProve1DIRECCION: TStringField;
    sqlProve1DIRECCION2: TStringField;
    sqlProve1POBLACION: TStringField;
    sqlProve1PROVINCIA: TStringField;
    sqlProve1CPOSTAL: TStringField;
    sqlProve1CPAIS: TStringField;
    sqlProve1CPROVINCIA: TStringField;
    sqlProve1CONTACTO1NOMBRE: TStringField;
    sqlProve1CONTACTO1CARGO: TStringField;
    sqlProve1CONTACTO2NOMBRE: TStringField;
    sqlProve1CONTACTO2CARGO: TStringField;
    sqlProve1TELEFONO1: TStringField;
    sqlProve1TELEFONO2: TStringField;
    sqlProve1MOVIL: TStringField;
    sqlProve1FAX: TStringField;
    sqlProve1EMAIL: TStringField;
    sqlProve1WEB: TStringField;
    sqlProve1MENSAJEAVISO: TStringField;
    sqlProve1OBSERVACIONES: TMemoField;
    sqlProve1BANCO: TStringField;
    sqlProve1AGENCIA: TStringField;
    sqlProve1DC: TStringField;
    sqlProve1CUENTABANCO: TStringField;
    sqlProve1TEFECTO: TSmallintField;
    sqlProve1EFECTOS: TSmallintField;
    sqlProve1FRECUENCIA: TSmallintField;
    sqlProve1DIASPRIMERVTO: TSmallintField;
    sqlProve1DIAFIJO1: TSmallintField;
    sqlProve1DIAFIJO2: TSmallintField;
    sqlProve1DIAFIJO3: TSmallintField;
    sqlProve1TDTO: TIntegerField;
    sqlProve1TPAGO: TIntegerField;
    sqlProve1SWTFACTURACION: TSmallintField;
    sqlProve1SWBLOQUEO: TSmallintField;
    sqlProve1REFEPROVEEDOR: TStringField;
    sqlProve1TRANSPORTE: TSmallintField;
    sqlProve1RUTA: TStringField;
    sqlProve1PROVEEDORHOSTING: TIntegerField;
    sqlProve1NOMBREHOSTING: TStringField;
    sqlProve1ULTIMAFECHADESCARGA: TDateField;
    sqlProve1ULTIMAFECHAENVIO: TDateField;
    sqlProve1COLUMNASEXCEL: TStringField;
    sqlProve1DESDEFILA: TIntegerField;
    sqlProve1SWRECIBIR: TSmallintField;
    sqlProve1SWENVIAR: TSmallintField;
    sqlProve1SWALTABAJA: TSmallintField;
    sqlProve1FECHABAJA: TDateField;
    sqlProve1FECHAALTA: TDateField;
    sqlProve1HORAALTA: TTimeField;
    sqlProve1FECHAULTI: TDateField;
    sqlProve1HORAULTI: TTimeField;
    sqlProve1USUULTI: TStringField;
    sqlProve1NOTAULTI: TStringField;
    sqlProve1MAXPAQUETE: TIntegerField;
    sqlReclamaSCANTIDAD_H: TSingleField;
    sqlReclamaSCANTIENALBA_H: TSingleField;
    sqlReclamaSDEVUELTOS_H: TSingleField;
    sqlReclamaSVENDIDOS_H: TSingleField;
    sqlReclamaSRECLAMADO_H: TSingleField;
    sqlReclamaSSUFACTURA: TStringField;
    sqlReclamaSFECHAFACTURA: TDateField;
    sqlReclamaSDOCTOPROVE: TStringField;
    sqlReclamaSDOCTOPROVEFECHA: TDateField;
    sqlReclamaSID_RECLAMA: TIntegerField;
    sqlReclamaSTRECLAMACION: TSmallintField;
    sqlReclamaSNRECLAMACION: TIntegerField;
    sqlReclamaSFECHA: TDateField;
    sqlReclamaSHORA: TTimeField;
    sqlReclamaSID_HISARTI: TIntegerField;
    sqlReclamaSID_PROVEEDOR: TIntegerField;
    sqlReclamaSID_CLIENTE: TIntegerField;
    sqlReclamaSID_ARTICULO: TIntegerField;
    sqlReclamaSADENDUM: TStringField;
    sqlReclamaSCONCEPTO: TStringField;
    sqlReclamaSCANTIDAD: TSingleField;
    sqlReclamaSCANTIENALBA: TSingleField;
    sqlReclamaSNALMACEN: TIntegerField;
    sqlReclamaSSWESTADO: TSmallintField;
    sqlReclamaSBARRAS: TStringField;
    sqlReclamaSDESCRIPCION: TStringField;
    sqlReclamaSNOMBRE: TStringField;
    sqlReclamaSNIF: TStringField;
    sqlReclamaFiRECLAMAPENDI: TIntegerField;
    sqlHisto1ID_HISARTI: TIntegerField;
    sqlHisto1SWESTADO: TSmallintField;
    sqlHisto1SWDEVOLUCION: TSmallintField;
    sqlHisto1SWCERRADO: TSmallintField;
    sqlHisto1SWMARCA: TSmallintField;
    sqlReclama1: TFDQuery;
    sqlReclama1ID_HISARTI: TIntegerField;
    sqlReclama1FECHARECLAMACION: TDateField;
    sqlReclama1SWES: TStringField;
    sqlReclama1FECHA: TDateField;
    sqlReclama1HORA: TTimeField;
    sqlReclama1CLAVE: TStringField;
    sqlReclama1ID_ARTICULO: TIntegerField;
    sqlReclama1ADENDUM: TStringField;
    sqlReclama1ID_CLIENTE: TIntegerField;
    sqlReclama1SWTIPOFRA: TIntegerField;
    sqlReclama1NFACTURA: TIntegerField;
    sqlReclama1NALBARAN: TIntegerField;
    sqlReclama1NLINEA: TIntegerField;
    sqlReclama1DEVUELTOS: TSingleField;
    sqlReclama1VENDIDOS: TSingleField;
    sqlReclama1MERMA: TSingleField;
    sqlReclama1CANTIDAD: TSingleField;
    sqlReclama1CANTIENALBA: TSingleField;
    sqlReclama1PRECIOCOMPRA: TSingleField;
    sqlReclama1PRECIOCOSTE: TSingleField;
    sqlReclama1PRECIOVENTA: TSingleField;
    sqlReclama1SWDTO: TSmallintField;
    sqlReclama1TPCDTO: TSingleField;
    sqlReclama1TIVA: TSmallintField;
    sqlReclama1TPCIVA: TSingleField;
    sqlReclama1TPCRE: TSingleField;
    sqlReclama1IMPOBRUTO: TSingleField;
    sqlReclama1IMPODTO: TSingleField;
    sqlReclama1IMPOBASE: TFloatField;
    sqlReclama1IMPOIVA: TFloatField;
    sqlReclama1IMPORE: TFloatField;
    sqlReclama1IMPOIMPTOS: TFloatField;
    sqlReclama1IMPOTOTLIN: TSingleField;
    sqlReclama1ENTRADAS: TSingleField;
    sqlReclama1SALIDAS: TSingleField;
    sqlReclama1VALORCOSTE: TFloatField;
    sqlReclama1VALORCOMPRA: TFloatField;
    sqlReclama1VALORVENTA: TFloatField;
    sqlReclama1VALORMOVI: TSingleField;
    sqlReclama1ID_PROVEEDOR: TIntegerField;
    sqlReclama1IDSTOCABE: TIntegerField;
    sqlReclama1IDDEVOLUCABE: TIntegerField;
    sqlReclama1TPCCOMISION: TSingleField;
    sqlReclama1IMPOCOMISION: TSingleField;
    sqlReclama1ENCARTE: TSingleField;
    sqlReclama1ID_DIARIA: TIntegerField;
    sqlReclama1PARTIDA: TIntegerField;
    sqlReclama1TURNO: TSmallintField;
    sqlReclama1NALMACEN: TIntegerField;
    sqlReclama1ID_HISARTI01: TIntegerField;
    sqlReclama1ID_FRAPROVE: TIntegerField;
    sqlReclama1NRECLAMACION: TIntegerField;
    sqlReclama1SWPDTEPAGO: TSmallintField;
    sqlReclama1SWESTADO: TSmallintField;
    sqlReclama1SWDEVOLUCION: TSmallintField;
    sqlReclama1SWCERRADO: TSmallintField;
    sqlReclama1FECHAAVISO: TDateField;
    sqlReclama1FECHADEVOL: TDateField;
    sqlReclama1FECHACARGO: TDateField;
    sqlReclama1SWMARCA: TSmallintField;
    sqlReclama1CARGO: TSingleField;
    sqlReclama1ABONO: TSingleField;
    sqlReclama1RECLAMADO: TSingleField;
    sqlReclama1BARRAS: TStringField;
    sqlReclama1DESCRIPCION: TStringField;
    sqlReclama1REFEPROVE: TStringField;
    sqlReclama1NOMCLIENTE: TStringField;
    sqlReclama1NOMPROVEEDOR: TStringField;
    sqlReclama1DOCTOPROVE: TStringField;
    sqlReclama1DOCTOPROVEFECHA: TDateField;
    sqlReclamaSDescriEstado: TStringField;
    procedure sqlReclama1AfterScroll(DataSet: TDataSet);


  private
    function RutTexFechas_SQL(zTexData, zDataA, zDataZ: String): String;
    function RutTexFechaSQL(zTexGuia, zData: String): String;






    { Private declarations }
  public
    vTipo : Integer;

    procedure ProObrirHistoric(RgTReclama, RGTipo, RgDistribu: Integer; fDesde, fHasta : String);
    procedure ProObrirReclamaHi(zId: Integer);
    { Public declarations }


    end;

function DMReclamaciones: TDMReclamaciones;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uMenu
  ,uReclamaciones;

function DMReclamaciones: TDMReclamaciones;
begin
  Result := TDMReclamaciones(DMppal.GetModuleInstance(TDMReclamaciones));
end;


procedure TDMReclamaciones.ProObrirHistoric(RgTReclama , RGTipo,RgDistribu  : Integer; fDesde, fHasta : String);
var wSeleA, wSeleB, wSeleC, wSeleD, wSeleE, wSeleF,  wOrder      : String;
begin

  //vSwRecla := False;


  if RgTReclama = 0 Then    wSeleA := ' where ( H.SWESTADO < 8 or H.SWESTADO is null ) ';
  if RgTReclama = 1 Then    wSeleA := ' where ( H.SWESTADO > 7 ) ';
  if RgTReclama = 2 Then    wSeleA := ' where ( H.SWESTADO < 99 or H.SWESTADO is null ) ';

  if RgDistribu = 0 Then
  begin
     //vProveedor := StrToIntDef(pDistriCodi.Text,0);
     //wSeleB     := ' and ( H.ID_PROVEEDOR = ' + IntToStr(vProveedor) + ')';
  end else
  begin
  end;

//  0  Compras    1  Devoluciones   2  Todas
  if RGTipo     = 0 Then    wSeleC := ' and ( H.CLAVE = ' + '''' + '01' + '''' + ' ) ';
  if RGTipo     = 1 Then    wSeleC := ' and ( H.CLAVE >= ' + '''' + '54' + '''' + ' ) ';
  if RGTipo     = 2 Then    wSeleC := ' and ( ( H.CLAVE = ' + '''' + '01' + '''' + ' ) '
                                  +   '  or ( H.CLAVE >= ' + '''' + '54' + '''' + ' ) ) ';


  wSeleD := RutTexFechas_SQL (' R.FECHA ',fDesde, fHasta);

  wSeleF := '';
  if vTipo > 0 then wSeleF := ' and (R.ID_FRAPROVE = ' + IntToStr(vTipo) + ')';


  wOrder := ' Order by P.NOMBRE, 2, A.descripcion, H.FECHA ';   // NOMPROVEEDOR

  with sqlReclama1 do
  begin
  Close;
  SQL.Text := Copy(SQL.Text,1,
                      pos('WHERE (', Uppercase(SQL.Text))-1)
                 + wSeleA + wSeleB + wSeleC + wSeleD + wSeleF + wOrder;
  Open;
  First;
  end;
  //ProSumarPeu;

  //ProObrirReclamaHi (DMReclamaA.cdsReclama1.FieldByName('ID_HISARTI').AsInteger);
  //vSwRecla := True;

end;

procedure TDMReclamaciones.sqlReclama1AfterScroll(DataSet: TDataSet);
begin
  ProObrirReclamaHi(sqlReclama1ID_HISARTI.AsInteger);
end;


procedure TDMReclamaciones.ProObrirReclamaHi(zId:Integer);
var wSeleA, wSeleB,  wOrder      : String;
begin
  wSeleA      := '';
  wSeleB      := '';
  wOrder      := '';

  wSeleA := ' where R.ID_RECLAMA is not null ';
  wSeleB := ' and R.ID_HISARTI = ' + IntToStr(zId);
  wOrder := ' Order by R.FECHA,R.NRECLAMACION ';

  with sqlReclamaS do
  begin
    Close;
    SQL.Text := Copy(SQL.Text,1,
                pos('WHERE', Uppercase(SQL.Text))-1)
                + wSeleA + wSeleB + wOrder;
    Open;
    First;
  end;
end;


Function TDMReclamaciones.RutTexFechas_SQL (zTexData,zDataA,zDataZ:String):String;
var wData : TDate;
wTexData  : String;
Begin
 Result := '';
 if (zDataA = '') and (zDataZ = '') Then Exit;

 if (zDataA <> '') and (zDataZ <> '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' between ', zDataA)
              + RutTexFechaSQL(' and ', zDataZ) + ' ) ';
 end;

 if (zDataA  = '') and (zDataZ <> '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' <= ', zDataZ) + ' ) ';
 end;

 if (zDataA <> '') and (zDataZ  = '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' >= ', zDataA) + ' ) ';
 end;

 Result := wTexData;

end;

Function TDMReclamaciones.RutTexFechaSQL (zTexGuia,zData:String):String;
var wData : TDate;
Begin
 Result := '';
 if zData = '' Then  Exit;
 Try
   wData := StrToDate(zData);
 Except
   Exit;
 end;
 Result := zTexGuia + '''' + FormatDateTime('mm/dd/yyyy',wData) + '''';
end;

initialization
  RegisterModuleClass(TDMReclamaciones);





end.


