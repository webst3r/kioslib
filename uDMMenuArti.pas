unit uDMMenuArti;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, math;

type
  TDMMenuArti = class(TDataModule)
    sqlListaArticulo: TFDQuery;
    sqlFichaArticulo: TFDQuery;
    sqlArtiLink: TFDQuery;
    sqlArtiLinkID_ARTICULO: TIntegerField;
    sqlArtiLinkDESCRIPCION: TStringField;
    sqlArtiLinkEDITORIAL: TIntegerField;
    sqlArtiLinkNOMBRE: TStringField;
    sqlArtiLinkBARRAS: TStringField;
    sqlArtiLinkADENDUM: TStringField;
    sqlArtiLinkTBARRAS: TIntegerField;
    sqlArtiLinkFAMILIAVENTA: TIntegerField;
    sqlArtiLinkORDENVENTA: TIntegerField;
    sqlArtiLinkREFEPROVE: TStringField;
    sqlArtiLinkFECHABAJA: TDateField;
    sqlArtiLinkNADENDUM: TIntegerField;
    sqlArtiLinkSTOCK: TSingleField;
    sqlEditorialS: TFDQuery;
    sqlEditorialSID_PROVEEDOR: TIntegerField;
    sqlEditorialSTPROVEEDOR: TSmallintField;
    sqlEditorialSNOMBRE: TStringField;
    sqlPeriodi: TFDQuery;
    sqlArTitulo: TFDQuery;
    sqlArTituloID_ARTITULO: TIntegerField;
    sqlArTituloID_ARTICULO: TIntegerField;
    sqlArTituloADENDUM: TStringField;
    sqlArTituloTITULO: TStringField;
    sqlArTituloIMAGEN: TStringField;
    sqlArTituloSWALTABAJA: TSmallintField;
    sqlArTituloFECHABAJA: TDateField;
    sqlArTituloFECHAALTA: TDateField;
    sqlArTituloFECHAULTI: TDateField;
    sqlPreus: TFDQuery;
    sqlPreusID_ARTICULO: TIntegerField;
    sqlPreusSWACTIVO: TSmallintField;
    sqlPreusTDIASEMA: TSmallintField;
    sqlPreusNUM: TIntegerField;
    sqlPreusPRECIO1: TSingleField;
    sqlPreusPRECIOCOSTE: TSingleField;
    sqlPreusPRECIOCOSTETOT: TSingleField;
    sqlPreusMARGEN: TSingleField;
    sqlPreusTPCENCARTE1: TSingleField;
    sqlPreusTIVA: TSmallintField;
    sqlPreusPRECIO2: TSingleField;
    sqlPreusPRECIOCOSTE2: TSingleField;
    sqlPreusPRECIOCOSTETOT2: TSingleField;
    sqlPreusMARGEN2: TSingleField;
    sqlPreusTPCENCARTE2: TSingleField;
    sqlPreusTIVA2: TSmallintField;
    sqlPreusFECHAPRECIO: TDateField;
    sqlTConteni1: TFDQuery;
    sqlTConteni1TTIPO: TStringField;
    sqlTConteni1NORDEN: TIntegerField;
    sqlTConteni1DESCRIPCION: TStringField;
    sqlTTema1: TFDQuery;
    sqlTTema1TTIPO: TStringField;
    sqlTTema1NORDEN: TIntegerField;
    sqlTTema1DESCRIPCION: TStringField;
    sqlTAutor: TFDQuery;
    sqlTAutorTTIPO: TStringField;
    sqlTAutorNORDEN: TIntegerField;
    sqlTAutorDESCRIPCION: TStringField;
    sqlTEditor: TFDQuery;
    sqlTProdu: TFDQuery;
    sqlTProduTTIPO: TStringField;
    sqlTProduNORDEN: TIntegerField;
    sqlTProduDESCRIPCION: TStringField;
    sqlTProduMARGEN: TSingleField;
    sqlTProduCADUCIDAD: TIntegerField;
    sqlTEditorTTIPO: TStringField;
    sqlTEditorNORDEN: TIntegerField;
    sqlTEditorDESCRIPCION: TStringField;
    sqlPeriodiTTIPO: TStringField;
    sqlPeriodiNORDEN: TIntegerField;
    sqlPeriodiDESCRIPCION: TStringField;
    sqlPeriodiMARGEN: TSingleField;
    sqlPeriodiCADUCIDAD: TIntegerField;
    sqlPeriodicidad: TFDQuery;
    sqlPeriodicidadTTIPO: TStringField;
    sqlPeriodicidadNORDEN: TIntegerField;
    sqlPeriodicidadDESCRIPCION: TStringField;
    sqlTGrupo: TFDQuery;
    sqlTGrupoTTIPO: TStringField;
    sqlTGrupoNORDEN: TIntegerField;
    sqlTGrupoDESCRIPCION: TStringField;
    sqlTGrupoMARGEN: TSingleField;
    sqlTGrupoCADUCIDAD: TIntegerField;
    sqlFamiVenta1: TFDQuery;
    sqlFamiVenta1TTIPO: TStringField;
    sqlFamiVenta1NORDEN: TIntegerField;
    sqlFamiVenta1DESCRIPCION: TStringField;
    sqlVarisSubGrupo: TFDQuery;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    SingleField1: TSingleField;
    IntegerField2: TIntegerField;
    sqlTipoProdu: TFDQuery;
    sqlTipoProduTTIPO: TStringField;
    sqlTipoProduNORDEN: TIntegerField;
    sqlTipoProduDESCRIPCION: TStringField;
    sqlTipoProduMARGEN: TSingleField;
    sqlTipoProduCADUCIDAD: TIntegerField;
    sqlListaArticuloID_ARTICULO: TIntegerField;
    sqlListaArticuloBARRAS: TStringField;
    sqlListaArticuloADENDUM: TStringField;
    sqlListaArticuloDESCRIPCION: TStringField;
    sqlListaArticuloPRECIOCOSTE: TSingleField;
    sqlListaArticuloPRECIOCOSTETOT: TSingleField;
    sqlListaArticuloPRECIO1: TSingleField;
    sqlListaArticuloPRECIO2: TSingleField;
    sqlListaArticuloREFEPROVE: TStringField;
    SP_GEN_ARTICULO: TFDStoredProc;
    sqlFichaArticuloID_ARTICULO: TIntegerField;
    sqlFichaArticuloTBARRAS: TIntegerField;
    sqlFichaArticuloBARRAS: TStringField;
    sqlFichaArticuloADENDUM: TStringField;
    sqlFichaArticuloFECHAADENDUM: TDateField;
    sqlFichaArticuloNADENDUM: TIntegerField;
    sqlFichaArticuloDESCRIPCION: TStringField;
    sqlFichaArticuloDESCRIPCION2: TStringField;
    sqlFichaArticuloIBS: TStringField;
    sqlFichaArticuloISBN: TStringField;
    sqlFichaArticuloEDITOR: TIntegerField;
    sqlFichaArticuloPRECIO1: TSingleField;
    sqlFichaArticuloPRECIO2: TSingleField;
    sqlFichaArticuloPRECIO3: TSingleField;
    sqlFichaArticuloPRECIO4: TSingleField;
    sqlFichaArticuloPRECIO5: TSingleField;
    sqlFichaArticuloPRECIO6: TSingleField;
    sqlFichaArticuloPRECIO7: TSingleField;
    sqlFichaArticuloTPCENCARTE1: TSingleField;
    sqlFichaArticuloTPCENCARTE2: TSingleField;
    sqlFichaArticuloTPCENCARTE3: TSingleField;
    sqlFichaArticuloTPCENCARTE4: TSingleField;
    sqlFichaArticuloTPCENCARTE5: TSingleField;
    sqlFichaArticuloTPCENCARTE6: TSingleField;
    sqlFichaArticuloTPCENCARTE7: TSingleField;
    sqlFichaArticuloPRECIOCOSTE: TSingleField;
    sqlFichaArticuloPRECIOCOSTE2: TSingleField;
    sqlFichaArticuloPRECIOCOSTETOT: TSingleField;
    sqlFichaArticuloPRECIOCOSTETOT2: TSingleField;
    sqlFichaArticuloMARGEN: TSingleField;
    sqlFichaArticuloMARGEN2: TSingleField;
    sqlFichaArticuloMARGENMAXIMO: TSingleField;
    sqlFichaArticuloEDITORIAL: TIntegerField;
    sqlFichaArticuloREFEPROVE: TStringField;
    sqlFichaArticuloPERIODICIDAD: TIntegerField;
    sqlFichaArticuloCADUCIDAD: TIntegerField;
    sqlFichaArticuloTIVA: TIntegerField;
    sqlFichaArticuloTIVA2: TIntegerField;
    sqlFichaArticuloSWRECARGO: TSmallintField;
    sqlFichaArticuloSWACTIVADO: TSmallintField;
    sqlFichaArticuloSWINGRESO: TSmallintField;
    sqlFichaArticuloSWCONTROLFECHA: TSmallintField;
    sqlFichaArticuloSWTPRECIO: TSmallintField;
    sqlFichaArticuloSWORIGEN: TIntegerField;
    sqlFichaArticuloTIPO: TStringField;
    sqlFichaArticuloTPRODUCTO: TIntegerField;
    sqlFichaArticuloTCONTENIDO: TIntegerField;
    sqlFichaArticuloTEMATICA: TIntegerField;
    sqlFichaArticuloAUTOR: TIntegerField;
    sqlFichaArticuloOBSERVACIONES: TMemoField;
    sqlFichaArticuloSTOCKMINIMO: TSingleField;
    sqlFichaArticuloSTOCKMAXIMO: TSingleField;
    sqlFichaArticuloFAMILIA: TStringField;
    sqlFichaArticuloFAMILIAVENTA: TIntegerField;
    sqlFichaArticuloORDENVENTA: TIntegerField;
    sqlFichaArticuloSTOCKMINIMOESTANTE: TSingleField;
    sqlFichaArticuloSTOCKMAXIMOESTANTE: TSingleField;
    sqlFichaArticuloSTOCKVENTAESTANTE: TSingleField;
    sqlFichaArticuloREPONERESTANTE: TSingleField;
    sqlFichaArticuloUNIDADESCOMPRA: TSingleField;
    sqlFichaArticuloUNIDADESVENTA: TSingleField;
    sqlFichaArticuloIMAGEN: TStringField;
    sqlFichaArticuloCOD_ARTICU: TStringField;
    sqlFichaArticuloGRUPO: TIntegerField;
    sqlFichaArticuloSUBGRUPO: TIntegerField;
    sqlFichaArticuloSWALTABAJA: TSmallintField;
    sqlFichaArticuloFECHABAJA: TDateField;
    sqlFichaArticuloFECHAALTA: TDateField;
    sqlFichaArticuloFECHAULTI: TDateField;
    sqlFichaArticuloNOMAUTOR: TStringField;
    sqlFichaArticuloNOMEDITOR: TStringField;
    sqlFichaArticuloCONTENIDO: TStringField;
    sqlFichaArticuloTIPODEPRODUCTO: TStringField;
    sqlFichaArticuloNOMTEMATICA: TStringField;
    sqlFichaArticuloNOMBRE: TStringField;
    sqlTIVA1: TFDQuery;
    sqlTIVA1TIVA: TIntegerField;
    sqlTIVA1TIPOIVA: TStringField;
    sqlTIVA1TPCIVA: TSingleField;
    sqlTIVA1CTACONTABLE: TStringField;
    sqlTIVA1TPCRE: TSingleField;
    sqlTIVA1CTARECARGO: TStringField;
    sqlTIVA1FECHAALTA: TDateField;
    sqlTIVA1FECHAUTI: TDateField;
    sqlTIVA1HORAULTI: TTimeField;
    sqlTIVA1USUULTI: TStringField;
    sqlTIVA1NOTAULTI: TStringField;
    sqlTIVA1VALDESDE: TDateField;
    sqlTIVA1VALHASTA: TDateField;
    sqlFichaArticuloTPCIVA1: TFloatField;
    sqlFichaArticuloTPCRE1: TFloatField;
    sqlFichaArticulo0: TFDQuery;
    sqlFichaArticulo0ID_ARTICULO: TIntegerField;
    sqlFichaArticulo0TBARRAS: TIntegerField;
    sqlFichaArticulo0BARRAS: TStringField;
    sqlFichaArticulo0ADENDUM: TStringField;
    sqlFichaArticulo0FECHAADENDUM: TDateField;
    sqlFichaArticulo0NADENDUM: TIntegerField;
    sqlFichaArticulo0DESCRIPCION: TStringField;
    sqlFichaArticulo0DESCRIPCION2: TStringField;
    sqlFichaArticulo0IBS: TStringField;
    sqlFichaArticulo0ISBN: TStringField;
    sqlFichaArticulo0EDITOR: TIntegerField;
    sqlFichaArticulo0PRECIO1: TSingleField;
    sqlFichaArticulo0PRECIO2: TSingleField;
    sqlFichaArticulo0PRECIO3: TSingleField;
    sqlFichaArticulo0PRECIO4: TSingleField;
    sqlFichaArticulo0PRECIO5: TSingleField;
    sqlFichaArticulo0PRECIO6: TSingleField;
    sqlFichaArticulo0PRECIO7: TSingleField;
    sqlFichaArticulo0TPCENCARTE1: TSingleField;
    sqlFichaArticulo0TPCENCARTE2: TSingleField;
    sqlFichaArticulo0TPCENCARTE3: TSingleField;
    sqlFichaArticulo0TPCENCARTE4: TSingleField;
    sqlFichaArticulo0TPCENCARTE5: TSingleField;
    sqlFichaArticulo0TPCENCARTE6: TSingleField;
    sqlFichaArticulo0TPCENCARTE7: TSingleField;
    sqlFichaArticulo0PRECIOCOSTE: TSingleField;
    sqlFichaArticulo0PRECIOCOSTE2: TSingleField;
    sqlFichaArticulo0PRECIOCOSTETOT: TSingleField;
    sqlFichaArticulo0PRECIOCOSTETOT2: TSingleField;
    sqlFichaArticulo0MARGEN: TSingleField;
    sqlFichaArticulo0MARGEN2: TSingleField;
    sqlFichaArticulo0MARGENMAXIMO: TSingleField;
    sqlFichaArticulo0EDITORIAL: TIntegerField;
    sqlFichaArticulo0REFEPROVE: TStringField;
    sqlFichaArticulo0PERIODICIDAD: TIntegerField;
    sqlFichaArticulo0CADUCIDAD: TIntegerField;
    sqlFichaArticulo0TIVA: TIntegerField;
    sqlFichaArticulo0TIVA2: TIntegerField;
    sqlFichaArticulo0SWRECARGO: TSmallintField;
    sqlFichaArticulo0SWACTIVADO: TSmallintField;
    sqlFichaArticulo0SWINGRESO: TSmallintField;
    sqlFichaArticulo0SWCONTROLFECHA: TSmallintField;
    sqlFichaArticulo0SWTPRECIO: TSmallintField;
    sqlFichaArticulo0SWORIGEN: TIntegerField;
    sqlFichaArticulo0TIPO: TStringField;
    sqlFichaArticulo0TPRODUCTO: TIntegerField;
    sqlFichaArticulo0TCONTENIDO: TIntegerField;
    sqlFichaArticulo0TEMATICA: TIntegerField;
    sqlFichaArticulo0AUTOR: TIntegerField;
    sqlFichaArticulo0OBSERVACIONES: TMemoField;
    sqlFichaArticulo0STOCKMINIMO: TSingleField;
    sqlFichaArticulo0STOCKMAXIMO: TSingleField;
    sqlFichaArticulo0FAMILIA: TStringField;
    sqlFichaArticulo0FAMILIAVENTA: TIntegerField;
    sqlFichaArticulo0ORDENVENTA: TIntegerField;
    sqlFichaArticulo0STOCKMINIMOESTANTE: TSingleField;
    sqlFichaArticulo0STOCKMAXIMOESTANTE: TSingleField;
    sqlFichaArticulo0STOCKVENTAESTANTE: TSingleField;
    sqlFichaArticulo0REPONERESTANTE: TSingleField;
    sqlFichaArticulo0UNIDADESCOMPRA: TSingleField;
    sqlFichaArticulo0UNIDADESVENTA: TSingleField;
    sqlFichaArticulo0IMAGEN: TStringField;
    sqlFichaArticulo0COD_ARTICU: TStringField;
    sqlFichaArticulo0GRUPO: TIntegerField;
    sqlFichaArticulo0SUBGRUPO: TIntegerField;
    sqlFichaArticulo0SWALTABAJA: TSmallintField;
    sqlFichaArticulo0FECHABAJA: TDateField;
    sqlFichaArticulo0FECHAALTA: TDateField;
    sqlFichaArticulo0FECHAULTI: TDateField;
    sqlFichaArticulo0NOMAUTOR: TStringField;
    sqlFichaArticulo0NOMEDITOR: TStringField;
    sqlFichaArticulo0CONTENIDO: TStringField;
    sqlFichaArticulo0TIPODEPRODUCTO: TStringField;
    sqlFichaArticulo0NOMTEMATICA: TStringField;
    sqlFichaArticulo0NOMBRE: TStringField;
    SP_GEN_ARTICULO0: TFDStoredProc;
    sqlFichaArticulo0TPCIVA1: TFloatField;
    sqlFichaArticulo0TPCRE1: TFloatField;
    sqlFichaArticulo0TPCIVA2: TFloatField;
    sqlFichaArticulo0TPCRE2: TFloatField;
    sqlComprobarBarras: TFDQuery;
    sqlComprobarBarrasID_ARTICULO: TIntegerField;
    sqlComprobarBarrasTBARRAS: TIntegerField;
    sqlComprobarBarrasBARRAS: TStringField;
    sqlComprobarBarrasADENDUM: TStringField;
    sqlComprobarBarrasFECHAADENDUM: TDateField;
    sqlComprobarBarrasNADENDUM: TIntegerField;
    sqlComprobarBarrasDESCRIPCION: TStringField;
    sqlComprobarBarrasDESCRIPCION2: TStringField;
    sqlComprobarBarrasIBS: TStringField;
    sqlComprobarBarrasISBN: TStringField;
    sqlComprobarBarrasEDITOR: TIntegerField;
    sqlComprobarBarrasPRECIO1: TSingleField;
    sqlComprobarBarrasPRECIO2: TSingleField;
    sqlComprobarBarrasPRECIO3: TSingleField;
    sqlComprobarBarrasPRECIO4: TSingleField;
    sqlComprobarBarrasPRECIO5: TSingleField;
    sqlComprobarBarrasPRECIO6: TSingleField;
    sqlComprobarBarrasPRECIO7: TSingleField;
    sqlComprobarBarrasTPCENCARTE1: TSingleField;
    sqlComprobarBarrasTPCENCARTE2: TSingleField;
    sqlComprobarBarrasTPCENCARTE3: TSingleField;
    sqlComprobarBarrasTPCENCARTE4: TSingleField;
    sqlComprobarBarrasTPCENCARTE5: TSingleField;
    sqlComprobarBarrasTPCENCARTE6: TSingleField;
    sqlComprobarBarrasTPCENCARTE7: TSingleField;
    sqlComprobarBarrasPRECIOCOSTE: TSingleField;
    sqlComprobarBarrasPRECIOCOSTE2: TSingleField;
    sqlComprobarBarrasPRECIOCOSTETOT: TSingleField;
    sqlComprobarBarrasPRECIOCOSTETOT2: TSingleField;
    sqlComprobarBarrasMARGEN: TSingleField;
    sqlComprobarBarrasMARGEN2: TSingleField;
    sqlComprobarBarrasMARGENMAXIMO: TSingleField;
    sqlComprobarBarrasEDITORIAL: TIntegerField;
    sqlComprobarBarrasREFEPROVE: TStringField;
    sqlComprobarBarrasPERIODICIDAD: TIntegerField;
    sqlComprobarBarrasCADUCIDAD: TIntegerField;
    sqlComprobarBarrasTIVA: TIntegerField;
    sqlComprobarBarrasTIVA2: TIntegerField;
    sqlComprobarBarrasSWRECARGO: TSmallintField;
    sqlComprobarBarrasSWACTIVADO: TSmallintField;
    sqlComprobarBarrasSWINGRESO: TSmallintField;
    sqlComprobarBarrasSWCONTROLFECHA: TSmallintField;
    sqlComprobarBarrasSWTPRECIO: TSmallintField;
    sqlComprobarBarrasSWORIGEN: TIntegerField;
    sqlComprobarBarrasTIPO: TStringField;
    sqlComprobarBarrasTPRODUCTO: TIntegerField;
    sqlComprobarBarrasTCONTENIDO: TIntegerField;
    sqlComprobarBarrasTEMATICA: TIntegerField;
    sqlComprobarBarrasAUTOR: TIntegerField;
    sqlComprobarBarrasOBSERVACIONES: TMemoField;
    sqlComprobarBarrasSTOCKMINIMO: TSingleField;
    sqlComprobarBarrasSTOCKMAXIMO: TSingleField;
    sqlComprobarBarrasFAMILIA: TStringField;
    sqlComprobarBarrasFAMILIAVENTA: TIntegerField;
    sqlComprobarBarrasORDENVENTA: TIntegerField;
    sqlComprobarBarrasSTOCKMINIMOESTANTE: TSingleField;
    sqlComprobarBarrasSTOCKMAXIMOESTANTE: TSingleField;
    sqlComprobarBarrasSTOCKVENTAESTANTE: TSingleField;
    sqlComprobarBarrasREPONERESTANTE: TSingleField;
    sqlComprobarBarrasUNIDADESCOMPRA: TSingleField;
    sqlComprobarBarrasUNIDADESVENTA: TSingleField;
    sqlComprobarBarrasIMAGEN: TStringField;
    sqlComprobarBarrasCOD_ARTICU: TStringField;
    sqlComprobarBarrasGRUPO: TIntegerField;
    sqlComprobarBarrasSUBGRUPO: TIntegerField;
    sqlComprobarBarrasSWALTABAJA: TSmallintField;
    sqlComprobarBarrasFECHABAJA: TDateField;
    sqlComprobarBarrasFECHAALTA: TDateField;
    sqlComprobarBarrasFECHAULTI: TDateField;
    procedure sqlFichaArticuloBeforeEdit(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);

  private
    procedure RutAbrirTablasAux;

















    { Private declarations }
  public
    { Public declarations }
    vID : Integer;
    swCreando : Boolean;
    wBene : Double;
  procedure RutAbrirEditorialS;
  procedure RutAbrirListaArticulos;
  procedure RutAbrirFichaArticulos(vID :String);
  procedure RutAbrirConsArti;
  procedure RutAbrirPreus(vPrecio : Integer);
  procedure RutActivarFiltro(vTabla: TFDQuery; vCampo, vFiltro: String);
  procedure RutNuevoArti;
  procedure RutCalcularPreciosArti;
  function RutCalculMarge_A(zPreu, zTpcIVA, zPrCost: Double): Double;
  procedure ProCalcularPreuCost1;
  procedure RutCalculaCost_Taula_Tot(zTab: TFDQuery; zTipo: String);
  procedure ProCalcularPreuCost2;
  procedure RutLeerIdArti(vID: Integer);
  procedure RutCalculaCost_Taula(zTab: TFDQuery; zTipo: String);
  function RutTex_TPCIVA_IVA1(zTab: TFDQuery): String;
  function RutExisteCampo(zFitxer: TFDQuery; zCamp: String): Boolean;
  function RutPreparaMarge(zTab: TFDQuery; zTxMarge, zTxEncarte,
      zTipo: String): Double;

  function RutTex_TPCRE_RE1(zTab: TFDQuery): String;
  function RutTpcDe(zCampA, zCampB: Double; zDecim: Integer): Double;

    end;

function DMMenuArti: TDMMenuArti;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uFichaArti, uMenuArti, uMenu, uDevolFicha;

function DMMenuArti: TDMMenuArti;
begin
  Result := TDMMenuArti(DMppal.GetModuleInstance(TDMMenuArti));
end;



procedure TDMMenuArti.RutAbrirListaArticulos;
begin
   sqlListaArticulo.Close;
   sqlListaArticulo.Open;
end;





procedure TDMMenuArti.RutAbrirFichaArticulos(vID :String);
begin

  with sqlFichaArticulo do
  begin
    Close;
    sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE A.ID_ARTICULO', Uppercase(SQL.Text))-1)
                 + 'WHERE A.ID_ARTICULO = ' + vID
                 + ' ORDER BY 1';
    Open;
  end;

  sqlPeriodi.Close;
  sqlPeriodi.Open;

  sqlTIVA1.Close;
  sqlTIVA1.Open;

  sqlEditorialS.Close;
  sqlEditorialS.Open;

  sqlTProdu.Close;
  sqlTProdu.Open;
end;

procedure TDMMenuArti.RutAbrirConsArti;
begin
   sqlArtiLink.Close;
   sqlArtiLink.Open;
end;

procedure TDMMenuArti.RutAbrirEditorialS;
begin
   sqlEditorialS.Close;
   sqlEditorialS.Open;
end;

Function TDMMenuArti.RutCalculMarge_A  (zPreu,zTpcIVA,zPrCost:Double):Double;
var wA, wB, wC, wMarge : Double;
begin
 wMarge := 0;
 wA :=  zPreu / ( 1 + (zTpcIVA / 100));
 wB :=  zPrCost;

 if wA = 0  Then  wC := 0
 else             wC :=  ( 100 * wB ) / wA;

 wMarge := 100 - wC;
 Result := wMarge;

end;

procedure TDMMenuArti.RutCalcularPreciosArti;
var
  wMarge, wVenta, wAux1, wAux2 : real;
begin

  if StrToFloatDef(FormManteArti.edPreu2.Text,0) > 0 then
  begin
    if sqlFichaArticuloPRECIO1.AsFloat = 0 then exit;
//    if DMManArtiB.cdsCabe.FieldByName('TPCIVA1').AsFloat = 0 then exit;
    wVenta := StrToFloatDef(FormManteArti.edPreu2.Text,0);

    wMarge := RutCalculMarge_A  (wVenta,
                                 sqlFichaArticuloTPCIVA1.AsFloat,
                                 sqlFichaArticuloPRECIOCOSTE.AsFloat);


    if DMPpal.RutEdiInse (sqlFichaArticulo) = False Then sqlFichaArticulo.Edit;
    wMarge := RoundTo( wMarge,-2);
    sqlFichaArticuloMARGEN.AsFloat := wMarge;
    sqlFichaArticuloPRECIO1.AsFloat := wVenta;

    ProCalcularPreuCost1;

    exit;
  end;

  wMarge := StrToFloatDef(FormManteArti.edMargen2.Text, 0);

  wVenta :=  (sqlFichaArticuloPRECIOCOSTE.AsFloat * wMarge / 100);
  wVenta :=  wVenta + sqlFichaArticuloPRECIOCOSTE.AsFloat;

  wAux1  :=  (sqlFichaArticuloTPCIVA1.AsFloat * wVenta / 100);
  wAux2  :=  (sqlFichaArticuloTPCRE1.AsFloat  * wVenta / 100);
  wVenta :=  wVenta + wAux1 + wAux2;

  wVenta := RoundTo(wVenta,-2);

  if DMPpal.RutEdiInse (sqlFichaArticulo) = False Then sqlFichaArticulo.Edit;
  sqlFichaArticuloPRECIO1.AsFloat := wVenta;

  FormManteArti.edCosteDireExit(nil);
  sqlFichaArticuloPRECIOCOSTE.AsFloat := RoundTo(sqlFichaArticuloPRECIOCOSTE.AsFloat, -4);
end;
procedure TDMMenuArti.RutLeerIdArti(vID : Integer);
begin
  with DMppal.sqlArti1 do
  begin
    close;
    SQL.Text :=  Copy(SQL.Text,1,
         pos('WHERE', Uppercase(SQL.Text))-1)
         + 'WHERE ID_ARTICULO = '  + IntToStr(vID)
         + ' ORDER BY ID_ARTICULO';
    Open;

  end;

  with DMppal.sqlArti0 do
  begin
    close;
    SQL.Text := Dmppal.sqlArti1.SQL.Text;
    Open;
  end;
end;
procedure TDMMenuArti.DataModuleCreate(Sender: TObject);
begin
  DMppal.RutCrearSortingSQL(sqlListaArticulo);
end;

procedure TDMMenuArti.ProCalcularPreuCost1;
begin
  if DMPpal.RutEdiInse(sqlFichaArticulo) = False Then Exit;
  RutCalculaCost_Taula     (sqlFichaArticulo, '1A');
  RutCalculaCost_Taula_Tot (sqlFichaArticulo, '1A');
end;

procedure TDMMenuArti.ProCalcularPreuCost2;
begin
  if DMPpal.RutEdiInse(sqlFichaArticulo) = False Then Exit;
  RutCalculaCost_Taula     (sqlFichaArticulo, '2A');
  RutCalculaCost_Taula_Tot (sqlFichaArticulo, '2A');
end;


Procedure  TDMMenuArti.RutCalculaCost_Taula_Tot (zTab:TFDQuery; zTipo:String);
var wIva, wRe                            : Double;
    wTxTpcIVA, wTxTpcRE, wTipoA, wTipoB  : String;
    wTxPreu,  wTxMarge,  wTxEncarte, wTxPreuCost : String;
begin
   wIva := 0;
   wRe  := 0;

   wTipoA := Copy (zTipo,2,1);
   wTipoB := Copy (zTipo,1,1);

   if wTipoB = '1' Then
   begin
       wTxTpcIVA := RutTex_TPCIVA_IVA1 (zTab);
       wTxTpcRE  := RutTex_TPCRE_RE1   (zTab);
   end else
   begin
       wTxTpcIVA := 'TPCIVA2';
       wTxTpcRE  := 'TPCRE2';
   end;


   if wTipoB = '1' Then
   begin
       if wTxTpcIVA <> '' Then
          wIVA := RutTpcDe (zTab.FieldByName('PRECIOCOSTE').AsFloat, zTab.FieldByName(wTxTpcIVA).AsFloat, -4);

       if wTxTpcRE  <> '' Then
          wRE  := RutTpcDe (zTab.FieldByName('PRECIOCOSTE').AsFloat, zTab.FieldByName(wTxTpcRE).AsFloat, -4);

       zTab.FieldByName('PRECIOCOSTETOT').AsFloat := zTab.FieldByName('PRECIOCOSTE').AsFloat
                                                     + wIVA  + wRE;
       zTab.FieldByName('PRECIOCOSTETOT').AsFloat := RoundTo(zTab.FieldByName('PRECIOCOSTETOT').AsFloat, -4);
   end else
   begin
       if wTxTpcIVA <> '' Then
          wIVA := RutTpcDe (zTab.FieldByName('PRECIOCOSTE2').AsFloat, zTab.FieldByName(wTxTpcIVA).AsFloat, -4);

       if wTxTpcRE  <> '' Then
          wRE  := RutTpcDe (zTab.FieldByName('PRECIOCOSTE2').AsFloat, zTab.FieldByName(wTxTpcRE).AsFloat, -4);

       zTab.FieldByName('PRECIOCOSTETOT2').AsFloat := zTab.FieldByName('PRECIOCOSTE2').AsFloat
                                                     + wIVA  + wRE;
       zTab.FieldByName('PRECIOCOSTETOT2').AsFloat := RoundTo(zTab.FieldByName('PRECIOCOSTETOT2').AsFloat, -4);
   end;
end;

Function TDMMenuArti.RutTpcDe (zCampA,zCampB:Double; zDecim:Integer):Double;
var wCamp : Double;
begin
 Result := 0;
 wCamp := (zCampA * zCampB);
 if (wCamp = 0) Then Exit;
 Try    wCamp := wCamp / 100;
 except wCamp := 0;
 end;
 Result := RoundTo(wCamp,zDecim);
end;

procedure TDMMenuArti.sqlFichaArticuloBeforeEdit(DataSet: TDataSet);
begin
  if FormManteArti.btConfirmarArti.Visible = False then abort
end;

Function  TDMMenuArti.RutTex_TPCRE_RE1     (zTab:TFDQuery):String;
Begin
 Result := '';
 if    RutExisteCampo (zTab, 'TPCRE1') Then
 Begin
       Result := 'TPCRE1';
       Exit;
 end;
 if    RutExisteCampo (zTab, 'TPCRE') Then
 Begin
       Result := 'TPCRE';
       Exit;
 end;
end;

procedure TDMMenuArti.RutCalculaCost_Taula     (zTab:TFDQuery; zTipo:String);
var wA, wB, wC, wD, wMargen                      : Double;
    wTxTpcIVA, wTipoA, wTipoB                    : String;
    wTxPreu,  wTxMarge,  wTxEncarte, wTxPreuCost : String;
begin
  wA := 0;
  wB := 0;
  wC := 0;
  wD := 0;

 wTipoA := Copy (zTipo,2,1);
 wTipoB := Copy (zTipo,1,1);

 if UpperCase(wTipoA) = 'H' Then    //  H Historic   ---  A Article
 begin
   if UpperCase(wTipoB) = '2' Then
   begin
      wTxPreu    := 'PRECIOVENTA2';
      wTxMarge   := 'MARGEN2';
      wTxEncarte := 'ENCARTE2';
      wTxPreuCost:= 'PRECIOCOSTE2';
   end else
   begin
      wTxPreu    := 'PRECIOVENTA';
      wTxMarge   := 'MARGEN';
      wTxEncarte := 'ENCARTE';
      wTxPreuCost:= 'PRECIOCOSTE';
   end;
 end else     //   ---------------------------------------------  A Article
 begin
   if UpperCase(wTipoB) = '2' Then
   begin
      wTxPreu    := 'PRECIO2';
      wTxMarge   := 'MARGEN2';
      wTxEncarte := 'TPCENCARTE2';
      wTxPreuCost:= 'PRECIOCOSTE2';
   end else
   begin
      wTxPreu    := 'PRECIO1';
      wTxMarge   := 'MARGEN';
      wTxEncarte := 'TPCENCARTE1';
      wTxPreuCost:= 'PRECIOCOSTE';
   end;
 end;

 if wTipoB = '2' Then    wTxTpcIVA := 'TPCIVA2'
 else                    wTxTpcIVA := RutTex_TPCIVA_IVA1 (zTab);

 if wTxTpcIVA = '' Then Exit;

 wA :=  1 + (zTab.FieldByName(wTxTpcIVA).AsFloat / 100);
 if wA <> 0 Then
 begin
       wB := zTab.FieldByName(wTxPreu).AsFloat / wA;
  wMargen := RutPreparaMarge (zTab, wTxMarge, wTxEncarte, 'B');
       wC := (100 - wMargen) / 100;
//     wC := (100 - zTab.FieldByName('MARGEN').AsFloat) / 100;
       wD := (wB * wC);
       zTab.FieldByName(wTxPreuCost).AsFloat := wD;
 end else
 begin
       zTab.FieldByName(wTxPreuCost).AsFloat := wD;
 end;

       zTab.FieldByName(wTxPreuCost).AsFloat := RoundTo(zTab.FieldByName(wTxPreuCost).AsFloat, -4);

 wBene := wB - wD;

end;

Function TDMMenuArti.RutPreparaMarge (zTab:TFDQuery; zTxMarge,zTxEncarte,zTipo:String):Double;
var wMargen, wEncarte : Double;
begin
  Result := 0;

  if zTxMarge                        = ''     Then Exit;
  if DMMenuArti.RutExisteCampo (zTab, zTxMarge) = False  Then Exit;
  if zTab.FieldByName(zTxMarge).AsFloat = 0   Then Exit;

  wMargen := zTab.FieldByName(zTxMarge).AsFloat;
  Result  := wMargen;

  if UpperCase(zTipo) = 'A' Then
  begin
      Exit;
  end;

  if zTxEncarte                        = ''     Then Exit;
  if DMMenuArti.RutExisteCampo(zTab, zTxEncarte) = False  Then Exit;
  if zTab.FieldByName(zTxEncarte).AsFloat = 0   Then Exit;

  wEncarte := (100 - zTab.FieldByName(zTxMarge).AsFloat) * zTab.FieldByName(zTxEncarte).AsFloat;
  wEncarte := wEncarte / 100;

  Result   := wMargen + wEncarte;

end;



Function  TDMMenuArti.RutExisteCampo(zFitxer:TFDQuery; zCamp:String):Boolean;
var wInd   : Integer;
    wField : TField;
Begin
 Result   := False;
 For wInd := 0 to zFitxer.FieldCount -1 do
 Begin
     Result := False;
     wField := zFitxer.Fields[ wInd ];
     if UpperCase(wField.FieldName) = UpperCase(zCamp) Then Begin
        Result := True;
        Exit;
     end;
 end;
end;

Function TDMMenuArti.RutTex_TPCIVA_IVA1     (zTab:TFDQuery):String;
Begin
 Result := '';
 if    RutExisteCampo (zTab, 'TPCIVA1') Then
 Begin
       Result := 'TPCIVA1';
       Exit;
 end;
 if    RutExisteCampo (zTab, 'TPCIVA') Then
 Begin
       Result := 'TPCIVA';
       Exit;
 end;
end;

procedure TDMMenuArti.RutNuevoArti;
begin


  swCreando := true;

  sqlFichaArticulo.Close;
  sqlFichaArticulo.Open;

  sqlPeriodi.Close;
  sqlPeriodi.Open;

  sqlTIVA1.Close;
  sqlTIVA1.Open;

  sqlEditorialS.Close;
  sqlEditorialS.Open();

  sqlTProdu.Close;
  sqlTProdu.Open;

  FormMenuArti.RutAbrirDevolArti;

  sqlFichaArticulo.Append;
  SP_GEN_ARTICULO.ExecProc;
  sqlFichaArticuloID_ARTICULO.AsInteger := SP_GEN_ARTICULO.Params[0].Value;
  FormManteArti.RutLeerTipoBarras(FormDevolFicha.pBarrasA.Text);




end;

procedure TDMMenuArti.RutAbrirPreus(vPrecio : Integer);
begin
   if vPrecio = 1 then
   begin
     with DMMenuArti.sqlPreus do
     begin
      close;
        SQL.Text :=  Copy(SQL.Text,1,
            pos('WHERE', Uppercase(SQL.Text))-1)
            + ' WHERE ID_ARTICULO = ' + DMMenuArti.sqlFichaArticuloID_ARTICULO.AsString
            + ' SWACTIVO = 1 ';
//                 + vWhereCabe + vFiltroEspecial + vOrderCabe;
      Open;
      First;
     end;
   end;
end;

procedure TDMMenuArti.RutActivarFiltro(vTabla:TFDQuery; vCampo, vFiltro:String);
var
  vValorInteger: Integer;
  vValorFloat  : Real;
  vValorBoolean: Boolean;
begin
  with vTabla do begin

  {if vTabla.IndexName = 'w2wTempIndex' then
   if vTabla.IndexDefs.Find('w2wTempIndex').Fields > '' then
     vCampo:=vTabla.IndexDefs.Find('w2wTempIndex').Fields;
  if vTabla.IndexFieldNames > '' then
    vCampo:=vTabla.IndexFieldNames;  }


    Filtered := True;
    if vCampo  = '' then begin Filter:=''; exit; end;
    if vFiltro = '' then begin Filter:=''; exit; end;

    if FieldByName(vCampo) is TStringField then
       Filter := Format('upper(' + vCampo + ') Like %s',
              [quotedStr('%' + Uppercase(vFiltro) + '%')]);

    if (FieldByName(vCampo) is TIntegerField) or
       (FieldByName(vCampo) is TSmallintField) then
       begin
         vValorInteger := StrToIntDef(vFiltro,0);
         Filter := vCampo +  '=' + IntToStr(vValorInteger);
       end;

    if (FieldByName(vCampo) is TFloatField) then
       begin
         vValorFloat := StrToFloatDef(vFiltro,0);
         Filter := vCampo +  '=' + FloatToStr(vValorFloat);
       end;

    if (FieldByName(vCampo) is TBooleanField) then
       begin
         Filter := vCampo +  '=' + vFiltro;
       end;
  end;

end;


procedure TDMMenuArti.RutAbrirTablasAux;
begin

  DMMenuArti.sqlTConteni1.Close;
  DMMenuArti.sqlTConteni1.Open;

  DMMenuArti.sqlEditorialS.Close;
  DMMenuArti.sqlEditorialS.Open;

  DMMenuArti.sqlPeriodicidad.Close;
  DMMenuArti.sqlPeriodicidad.Open;

  DMMenuArti.sqlTProdu.Close;
  DMMenuArti.sqlTProdu.Open;

  DMMenuArti.sqlTTema1.Close;
  DMMenuArti.sqlTTema1.Open;

  DMMenuArti.sqlTAutor.Close;
  DMMenuArti.sqlTAutor.Open;

  DMMenuArti.sqlTEditor.Close;
  DMMenuArti.sqlTEditor.Open;

  DMMenuArti.sqlTConteni1.Close;
  DMMenuArti.sqlTConteni1.Open;




end;

initialization
  RegisterModuleClass(TDMMenuArti);




end.




