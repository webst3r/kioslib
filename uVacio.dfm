object Form: TForm
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1100
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object dsTrabajos: TDataSource
    Left = 8
    Top = 760
  end
  object dsTiempo: TDataSource
    Left = 72
    Top = 760
  end
  object dsEmpresa: TDataSource
    Left = 8
    Top = 816
  end
  object dsSumTiempo: TDataSource
    Left = 128
    Top = 760
  end
  object dsTiempoCliente: TDataSource
    Left = 72
    Top = 808
  end
  object dsTodosTiempos: TDataSource
    Left = 120
    Top = 808
  end
  object dsEmpresa2: TDataSource
    Left = 8
    Top = 708
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
end
