unit uDevol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormDevolucion = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaDevolucion: TUniTabSheet;
    tabAlbaranDevolucion: TUniTabSheet;
    ImgNativeList: TUniNativeImageList;
    tabNuevaDevolucion: TUniTabSheet;
    procedure btMenuClick(Sender: TObject);


  private


    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swDevoLista, swDevoAlbaran, swNuevaDevo : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    procedure RutAbrirAlbaranTab;
    procedure RutAbrirLista;
    procedure RutAbrirNuevo;
  end;

function FormDevolucion: TFormDevolucion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolLista, uDevolAlbaran, uDevolNuevo, uDMDevol;

function FormDevolucion: TFormDevolucion;
begin
  Result := TFormDevolucion(DMppal.GetFormInstance(TFormDevolucion));

end;

procedure TFormDevolucion.RutAbrirNuevo;
begin
{  pcDetalle.ActivePage := tabNuevaDevolucion;
  if pcDetalle.ActivePage = tabNuevaDevolucion then
  begin
    if swNuevaDevo = 0 then
    begin
      FormDevolucionNuevo.Parent := tabNuevaDevolucion;
     // FormDevolucionAlbaran.pcDetalle.ActivePage := FormDevolucionAlbaran.tabAlbaran;
      FormDevolucionNuevo.Show();
      swNuevaDevo := 1;
    end;
  end;
 }
end;

procedure TFormDevolucion.RutAbrirAlbaranTab;
begin
  pcDetalle.ActivePage := tabAlbaranDevolucion;
  if pcDetalle.ActivePage = tabAlbaranDevolucion then
  begin
    if swDevoAlbaran = 0 then
    begin
      FormDevolucionAlbaran.Parent := tabAlbaranDevolucion;
      //Devolucion.RutAbrirAlbaran(TipoDoc);
      FormDevolucionAlbaran.Show();
      swDevoAlbaran := 1;
    end;
  end;

end;


procedure TFormDevolucion.RutAbrirLista;
begin
  pcDetalle.ActivePage := tabListaDevolucion;
  if pcDetalle.ActivePage = tabListaDevolucion then
  begin
    if swDevoLista = 0 then
    begin
      FormDevolucionLista.Parent := tabListaDevolucion;
      //FormMenuDevolucionLista.pnlBtsMenu2.Parent := FormMenuDevolucionLista.pnlBtsLista;
      FormDevolucionLista.Show();
      swDevoLista := 1;
    end;
  end;
end;

procedure TFormDevolucion.btMenuClick(Sender: TObject);
begin
//  if pcDetalle.ActivePage = tabListaDevolucion   then FormDevolucionLista.pnlBtsLista.Visible     := not FormDevolucionLista.pnlBtsLista.Visible;
//  if pcDetalle.ActivePage = tabAlbaranDevolucion then FormDevolucionAlbaran.pnlBtsAlbaran.Visible := not FormDevolucionAlbaran.pnlBtsAlbaran.Visible;
 { if pcDetalle.ActivePage = tabListaDevolucion  then
  begin
    //FormMenuDevolucionLista.pnlBtsMenu2.Parent := pnlParentMenu;
   // FormMenuDevolucionLista.pnlBtsLista.Visible     := not FormMenuDevolucionLista.pnlBtsLista.Visible;
    FormMenuDevolucionLista.pnlBtsMenu2.Visible := True;
  end;
  }
end;

end.
