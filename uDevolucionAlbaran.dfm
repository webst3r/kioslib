object FormDevolucionAlbaran: TFormDevolucionAlbaran
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1099
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1099
    Height = 103
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object UniPanel2: TUniPanel
      Left = 3
      Top = 0
      Width = 558
      Height = 44
      Hint = ''
      ShowHint = True
      TabOrder = 1
      Caption = ''
      object UniDBEdit1: TUniDBEdit
        Left = 10
        Top = 19
        Width = 40
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 1
      end
      object UniLabel1: TUniLabel
        Left = 156
        Top = 3
        Width = 54
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Distribuidor'
        TabOrder = 2
      end
      object UniDBEdit2: TUniDBEdit
        Left = 49
        Top = 19
        Width = 279
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 3
      end
      object UniDBEdit3: TUniDBEdit
        Left = 327
        Top = 19
        Width = 83
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 4
      end
      object UniDBEdit4: TUniDBEdit
        Left = 409
        Top = 19
        Width = 40
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 5
      end
      object UniDBDateTimePicker1: TUniDBDateTimePicker
        Left = 448
        Top = 19
        Width = 100
        Hint = ''
        ShowHint = True
        DateTime = 43374.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
      end
      object UniLabel2: TUniLabel
        Left = 329
        Top = 3
        Width = 68
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'N. Documento'
        TabOrder = 7
      end
      object UniLabel3: TUniLabel
        Left = 409
        Top = 3
        Width = 40
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Paquete'
        TabOrder = 8
      end
      object UniLabel4: TUniLabel
        Left = 480
        Top = 3
        Width = 29
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha'
        TabOrder = 9
      end
    end
    object UniPanel3: TUniPanel
      Left = 567
      Top = 0
      Width = 286
      Height = 49
      Hint = ''
      ShowHint = True
      TabOrder = 2
      Caption = ''
      object pFechaPreDevolu: TUniDBDateTimePicker
        Left = 159
        Top = 19
        Width = 100
        Hint = ''
        ShowHint = True
        DateTime = 43374.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 1
      end
      object UniLabel7: TUniLabel
        Left = 10
        Top = 14
        Width = 143
        Height = 23
        Hint = ''
        ShowHint = True
        Caption = 'Pre Devolucion'
        ParentFont = False
        Font.Height = -19
        Font.Style = [fsBold]
        TabOrder = 2
      end
      object UniLabel8: TUniLabel
        Left = 164
        Top = 4
        Width = 84
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha Devolucion'
        TabOrder = 3
      end
    end
    object rgOrdenado: TUniDBRadioGroup
      Left = 3
      Top = 50
      Width = 223
      Height = 45
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 3
      Items.Strings = (
        'Descripci'#243'n'
        'Paquete')
      Columns = 2
    end
    object fcPanel3: TUniPanel
      Left = 232
      Top = 50
      Width = 81
      Height = 45
      Hint = ''
      ShowHint = True
      TabOrder = 4
      Caption = ''
      object UniLabel5: TUniLabel
        Left = 11
        Top = 1
        Width = 54
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'N. Paquete'
        TabOrder = 1
      end
      object UniDBEdit5: TUniDBEdit
        Left = 18
        Top = 17
        Width = 40
        Height = 20
        Hint = ''
        ShowHint = True
        TabOrder = 2
      end
    end
    object Rgpaquetes: TUniDBRadioGroup
      Left = 319
      Top = 50
      Width = 141
      Height = 45
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 5
      Items.Strings = (
        'Todos'
        'Uno :')
      Columns = 2
    end
    object UniPanel4: TUniPanel
      Left = 459
      Top = 50
      Width = 81
      Height = 45
      Hint = ''
      ShowHint = True
      TabOrder = 6
      Caption = ''
      object UniLabel6: TUniLabel
        Left = 19
        Top = 3
        Width = 21
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Num'
        TabOrder = 1
      end
      object UniDBEdit6: TUniDBEdit
        Left = 18
        Top = 17
        Width = 40
        Height = 20
        Hint = ''
        ShowHint = True
        TabOrder = 2
      end
    end
    object UniBitBtn1: TUniBitBtn
      Left = 560
      Top = 53
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Utilidades'
      TabOrder = 7
    end
  end
  object UniPageControl1: TUniPageControl
    Left = 0
    Top = 103
    Width = 1099
    Height = 512
    Hint = ''
    ShowHint = True
    ActivePage = TabSheet3
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object TabSheet3: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'TabSheet3'
      object UniPageControl2: TUniPageControl
        Left = 0
        Top = 0
        Width = 1091
        Height = 484
        Hint = ''
        ShowHint = True
        ActivePage = TabSheet6
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object TabSheet6: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Entrada'
          object UniPanel5: TUniPanel
            Left = 0
            Top = 0
            Width = 1083
            Height = 76
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            Caption = ''
            object btVerDirectoDevolucion: TUniBitBtn
              Left = 2
              Top = 2
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Directo'
              TabOrder = 1
            end
            object BtnVerVtaAuto: TUniBitBtn
              Left = 58
              Top = 2
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Vta Auto'
              TabOrder = 2
            end
            object BtnNouAdendum: TUniBitBtn
              Left = 2
              Top = 47
              Width = 55
              Height = 27
              Hint = ''
              ShowHint = True
              Caption = 'Num'
              TabOrder = 3
            end
            object UniLabel9: TUniLabel
              Left = 125
              Top = 2
              Width = 65
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'C.Barras [F5]'
              TabOrder = 4
            end
            object UniDBEdit7: TUniDBEdit
              Left = 124
              Top = 16
              Width = 231
              Height = 27
              Hint = ''
              ShowHint = True
              TabOrder = 5
            end
            object UniDBEdit8: TUniDBEdit
              Left = 354
              Top = 16
              Width = 77
              Height = 27
              Hint = ''
              ShowHint = True
              TabOrder = 6
            end
            object UniLabel10: TUniLabel
              Left = 372
              Top = 2
              Width = 41
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Num[F7]'
              TabOrder = 7
            end
            object UniDBEdit9: TUniDBEdit
              Left = 443
              Top = 16
              Width = 57
              Height = 27
              Hint = ''
              ShowHint = True
              TabOrder = 8
            end
            object UniLabel11: TUniLabel
              Left = 446
              Top = 2
              Width = 43
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Devolver'
              TabOrder = 9
            end
            object BtnSumarCanti: TUniBitBtn
              Left = 522
              Top = 3
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 10
            end
            object BtnRestarCanti: TUniBitBtn
              Left = 583
              Top = 3
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 11
            end
            object UniLabel12: TUniLabel
              Left = 648
              Top = 2
              Width = 70
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Total Devuelto'
              TabOrder = 12
            end
            object UniDBEdit10: TUniDBEdit
              Left = 655
              Top = 16
              Width = 57
              Height = 27
              Hint = ''
              ShowHint = True
              TabOrder = 13
            end
            object pnlPendi: TUniPanel
              Left = 736
              Top = 3
              Width = 62
              Height = 57
              Hint = ''
              ShowHint = True
              TabOrder = 14
              Caption = ''
              Color = clLime
              object BtnPendi: TUniBitBtn
                Left = 3
                Top = 3
                Width = 56
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Pendiente'
                TabOrder = 1
              end
            end
            object pnlProveLine: TUniPanel
              Left = 806
              Top = 1
              Width = 306
              Height = 56
              Hint = ''
              ShowHint = True
              TabOrder = 15
              Caption = ''
              object UniDBEdit11: TUniDBEdit
                Left = 5
                Top = 22
                Width = 55
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 1
              end
              object UniLabel13: TUniLabel
                Left = 47
                Top = 6
                Width = 173
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Proveedor de la PRE - DEVOLUCION'
                TabOrder = 2
              end
              object UniDBEdit12: TUniDBEdit
                Left = 59
                Top = 22
                Width = 242
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 3
              end
            end
          end
          object UniPanel12: TUniPanel
            Left = 0
            Top = 76
            Width = 1083
            Height = 126
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            Caption = ''
            object UniPanel13: TUniPanel
              Left = 561
              Top = 1
              Width = 521
              Height = 124
              Hint = ''
              ShowHint = True
              Align = alClient
              Anchors = [akLeft, akTop, akRight, akBottom]
              TabOrder = 1
              Caption = ''
              object UniLabel32: TUniLabel
                Left = 18
                Top = 4
                Width = 37
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Compra'
                TabOrder = 1
              end
              object UniLabel33: TUniLabel
                Left = 19
                Top = 24
                Width = 36
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'A Stock'
                TabOrder = 2
              end
              object UniLabel34: TUniLabel
                Left = 23
                Top = 43
                Width = 32
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Merma'
                TabOrder = 3
              end
              object UniLabel35: TUniLabel
                Left = 27
                Top = 63
                Width = 28
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Venta'
                TabOrder = 4
              end
              object UniLabel36: TUniLabel
                Left = 28
                Top = 83
                Width = 27
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Devol'
                TabOrder = 5
              end
              object UniLabel37: TUniLabel
                Left = 24
                Top = 103
                Width = 31
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Abono'
                TabOrder = 6
              end
              object UniDBEdit30: TUniDBEdit
                Left = 61
                Top = 1
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 7
              end
              object UniDBEdit31: TUniDBEdit
                Left = 61
                Top = 21
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 8
              end
              object UniDBEdit32: TUniDBEdit
                Left = 61
                Top = 41
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 9
              end
              object UniDBEdit33: TUniDBEdit
                Left = 61
                Top = 61
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 10
              end
              object UniDBEdit34: TUniDBEdit
                Left = 110
                Top = 21
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 11
              end
              object UniDBEdit35: TUniDBEdit
                Left = 110
                Top = 61
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 12
              end
              object UniDBDateTimePicker6: TUniDBDateTimePicker
                Left = 61
                Top = 81
                Width = 98
                Height = 20
                Hint = ''
                ShowHint = True
                DateTime = 43375.000000000000000000
                DateFormat = 'dd/MM/yyyy'
                TimeFormat = 'HH:mm:ss'
                TabOrder = 13
              end
              object UniDBDateTimePicker7: TUniDBDateTimePicker
                Left = 61
                Top = 100
                Width = 98
                Height = 20
                Hint = ''
                ShowHint = True
                DateTime = 43375.000000000000000000
                DateFormat = 'dd/MM/yyyy'
                TimeFormat = 'HH:mm:ss'
                TabOrder = 14
              end
              object UniCheckBox4: TUniCheckBox
                Left = 174
                Top = 103
                Width = 171
                Height = 17
                Hint = ''
                ShowHint = True
                Caption = 'Desactivar ventas Automaticas'
                TabOrder = 15
              end
              object UniDBEdit36: TUniDBEdit
                Left = 262
                Top = 2
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 16
              end
              object UniLabel38: TUniLabel
                Left = 273
                Top = 21
                Width = 28
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Venta'
                TabOrder = 17
              end
            end
            object UniPanel14: TUniPanel
              Left = 1
              Top = 1
              Width = 424
              Height = 124
              Hint = ''
              ShowHint = True
              Align = alLeft
              Anchors = [akLeft, akTop, akBottom]
              TabOrder = 2
              Caption = ''
              object UniLabel27: TUniLabel
                Left = 21
                Top = 5
                Width = 33
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Codigo'
                TabOrder = 1
              end
              object UniLabel28: TUniLabel
                Left = 5
                Top = 45
                Width = 49
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Ref.Distri.'
                TabOrder = 2
              end
              object UniDBEdit22: TUniDBEdit
                Left = 57
                Top = 1
                Width = 98
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 3
              end
              object UniDBEdit23: TUniDBEdit
                Left = 155
                Top = 1
                Width = 98
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 4
              end
              object UniDBEdit24: TUniDBEdit
                Left = 253
                Top = 1
                Width = 49
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 5
              end
              object UniLabel29: TUniLabel
                Left = 308
                Top = 3
                Width = 51
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'N.Paquete'
                TabOrder = 6
              end
              object UniDBEdit25: TUniDBEdit
                Left = 362
                Top = 1
                Width = 45
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 7
              end
              object UniDBEdit26: TUniDBEdit
                Left = 57
                Top = 21
                Width = 350
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 8
              end
              object UniDBEdit27: TUniDBEdit
                Left = 57
                Top = 41
                Width = 80
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 9
              end
              object UniDBEdit28: TUniDBEdit
                Left = 137
                Top = 41
                Width = 270
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 10
              end
              object UniLabel30: TUniLabel
                Left = 108
                Top = 77
                Width = 281
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Ha sido actualizado el Art'#237'culo con Distribuidor / Referencia'
                TabOrder = 11
              end
            end
            object UniPanel15: TUniPanel
              Left = 425
              Top = 1
              Width = 136
              Height = 124
              Hint = ''
              ShowHint = True
              Align = alLeft
              Anchors = [akLeft, akTop, akBottom]
              TabOrder = 3
              Caption = ''
              object UniLabel31: TUniLabel
                Left = 42
                Top = 5
                Width = 38
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Importe'
                TabOrder = 1
              end
              object UniDBEdit29: TUniDBEdit
                Left = 15
                Top = 25
                Width = 106
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 2
              end
              object UniBitBtn11: TUniBitBtn
                Left = 15
                Top = 48
                Width = 50
                Height = 40
                Hint = ''
                ShowHint = True
                Caption = ''
                TabOrder = 3
              end
              object UniBitBtn12: TUniBitBtn
                Left = 71
                Top = 48
                Width = 50
                Height = 40
                Hint = ''
                ShowHint = True
                Caption = ''
                TabOrder = 4
              end
            end
          end
          object UniPanel16: TUniPanel
            Left = 0
            Top = 202
            Width = 1083
            Height = 58
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 2
            Caption = ''
            object UniBitBtn13: TUniBitBtn
              Left = 2
              Top = 13
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Anula'
              TabOrder = 1
            end
            object UniDBEdit37: TUniDBEdit
              Left = 61
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 2
            end
            object UniDBEdit38: TUniDBEdit
              Left = 61
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 3
            end
            object UniDBEdit39: TUniDBEdit
              Left = 121
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 4
            end
            object UniDBEdit40: TUniDBEdit
              Left = 181
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 5
            end
            object UniDBEdit41: TUniDBEdit
              Left = 121
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 6
            end
            object UniDBEdit42: TUniDBEdit
              Left = 181
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 7
            end
            object UniDBEdit43: TUniDBEdit
              Left = 240
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 8
            end
            object UniDBEdit44: TUniDBEdit
              Left = 240
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 9
            end
            object UniLabel39: TUniLabel
              Left = 66
              Top = 3
              Width = 45
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Total PVP'
              TabOrder = 10
            end
            object UniLabel40: TUniLabel
              Left = 131
              Top = 3
              Width = 30
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'P.V.P.'
              TabOrder = 11
            end
            object UniLabel41: TUniLabel
              Left = 188
              Top = 3
              Width = 36
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Margen'
              TabOrder = 12
            end
            object UniLabel42: TUniLabel
              Left = 244
              Top = 3
              Width = 37
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Encarte'
              TabOrder = 13
            end
            object UniDBEdit45: TUniDBEdit
              Left = 303
              Top = 17
              Width = 27
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 14
            end
            object UniDBEdit46: TUniDBEdit
              Left = 303
              Top = 37
              Width = 27
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 15
            end
            object UniDBEdit47: TUniDBEdit
              Left = 330
              Top = 17
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 16
            end
            object UniDBEdit48: TUniDBEdit
              Left = 374
              Top = 17
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 17
            end
            object UniDBEdit49: TUniDBEdit
              Left = 330
              Top = 37
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 18
            end
            object UniDBEdit50: TUniDBEdit
              Left = 374
              Top = 37
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 19
            end
            object UniLabel43: TUniLabel
              Left = 305
              Top = 3
              Width = 20
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Tipo'
              TabOrder = 20
            end
            object UniLabel44: TUniLabel
              Left = 335
              Top = 3
              Width = 28
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = '%IVA'
              TabOrder = 21
            end
            object UniLabel45: TUniLabel
              Left = 382
              Top = 3
              Width = 27
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = '% RE'
              TabOrder = 22
            end
            object UniDBEdit51: TUniDBEdit
              Left = 421
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 23
            end
            object UniDBEdit52: TUniDBEdit
              Left = 481
              Top = 17
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 24
            end
            object UniDBEdit53: TUniDBEdit
              Left = 421
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 25
            end
            object UniDBEdit54: TUniDBEdit
              Left = 481
              Top = 37
              Width = 60
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 26
            end
            object UniLabel46: TUniLabel
              Left = 429
              Top = 3
              Width = 42
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Pr.Coste'
              TabOrder = 27
            end
            object UniLabel47: TUniLabel
              Left = 485
              Top = 3
              Width = 52
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Pr.Cte+RE'
              TabOrder = 28
            end
            object UniPanel17: TUniPanel
              Left = 545
              Top = 0
              Width = 369
              Height = 57
              Hint = ''
              ShowHint = True
              TabOrder = 29
              Caption = ''
              object UniDBEdit55: TUniDBEdit
                Left = 5
                Top = 15
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 1
              end
              object UniDBEdit56: TUniDBEdit
                Left = 70
                Top = 15
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 2
              end
              object UniDBEdit57: TUniDBEdit
                Left = 5
                Top = 35
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 3
              end
              object UniDBEdit58: TUniDBEdit
                Left = 70
                Top = 35
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 4
              end
              object UniLabel48: TUniLabel
                Left = 76
                Top = 1
                Width = 38
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'V.Coste'
                TabOrder = 5
              end
              object UniLabel49: TUniLabel
                Left = 9
                Top = 1
                Width = 47
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'v.Compra'
                TabOrder = 6
              end
              object UniDBEdit59: TUniDBEdit
                Left = 135
                Top = 15
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 7
              end
              object UniDBEdit60: TUniDBEdit
                Left = 200
                Top = 15
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 8
              end
              object UniDBEdit61: TUniDBEdit
                Left = 135
                Top = 35
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 9
              end
              object UniDBEdit62: TUniDBEdit
                Left = 200
                Top = 35
                Width = 65
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 10
              end
              object UniLabel50: TUniLabel
                Left = 136
                Top = 1
                Width = 65
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'V.Coste + RE'
                TabOrder = 11
              end
              object UniLabel51: TUniLabel
                Left = 216
                Top = 1
                Width = 30
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'V.Vta.'
                TabOrder = 12
              end
              object UniLabel52: TUniLabel
                Left = 275
                Top = 13
                Width = 28
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Venta'
                TabOrder = 13
              end
              object UniLabel53: TUniLabel
                Left = 275
                Top = 26
                Width = 52
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Devolucion'
                TabOrder = 14
              end
              object UniLabel54: TUniLabel
                Left = 275
                Top = 39
                Width = 32
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Merma'
                TabOrder = 15
              end
              object UniDBText5: TUniDBText
                Left = 305
                Top = 13
                Width = 56
                Height = 13
                Hint = ''
                ShowHint = True
              end
              object UniDBText6: TUniDBText
                Left = 305
                Top = 26
                Width = 56
                Height = 13
                Hint = ''
                ShowHint = True
              end
              object UniDBText7: TUniDBText
                Left = 305
                Top = 39
                Width = 56
                Height = 13
                Hint = ''
                ShowHint = True
              end
            end
            object UniLabel55: TUniLabel
              Left = 917
              Top = 4
              Width = 25
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Alba.'
              TabOrder = 30
            end
            object UniLabel56: TUniLabel
              Left = 1003
              Top = 4
              Width = 24
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Total'
              TabOrder = 31
            end
            object UniLabel57: TUniLabel
              Left = 937
              Top = 27
              Width = 37
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Compra'
              TabOrder = 32
            end
            object UniDBText8: TUniDBText
              Left = 945
              Top = 4
              Width = 56
              Height = 13
              Hint = ''
              ShowHint = True
            end
            object UniDBText9: TUniDBText
              Left = 979
              Top = 27
              Width = 56
              Height = 13
              Hint = ''
              ShowHint = True
            end
          end
          object UniPanel18: TUniPanel
            Left = 0
            Top = 260
            Width = 1083
            Height = 36
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 3
            Caption = ''
            object UniLabel58: TUniLabel
              Left = 21
              Top = 0
              Width = 54
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Cod.Barras'
              TabOrder = 1
            end
            object UniDBEdit63: TUniDBEdit
              Left = 2
              Top = 14
              Width = 138
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 2
            end
            object UniDBEdit64: TUniDBEdit
              Left = 140
              Top = 14
              Width = 43
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 3
            end
            object UniDBEdit65: TUniDBEdit
              Left = 183
              Top = 14
              Width = 273
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 4
            end
            object UniDBEdit66: TUniDBEdit
              Left = 456
              Top = 14
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 5
            end
            object UniLabel59: TUniLabel
              Left = 221
              Top = 0
              Width = 54
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Descripcion'
              TabOrder = 6
            end
            object UniLabel60: TUniLabel
              Left = 448
              Top = 0
              Width = 51
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'N.Paquete'
              TabOrder = 7
            end
            object UniDBEdit67: TUniDBEdit
              Left = 514
              Top = 14
              Width = 44
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 8
            end
            object UniLabel61: TUniLabel
              Left = 509
              Top = 0
              Width = 48
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Devueltos'
              TabOrder = 9
            end
            object UniLabel62: TUniLabel
              Left = 585
              Top = 16
              Width = 171
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = '< ---   Ultima Modificacion Realizada'
              TabOrder = 10
            end
          end
          object UniDBGrid3: TUniDBGrid
            Left = 0
            Top = 296
            Width = 1083
            Height = 160
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 4
          end
        end
        object TabSheet4: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Pendiente'
          object UniPanel6: TUniPanel
            Left = 0
            Top = 0
            Width = 1083
            Height = 140
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            Caption = ''
            object UniPanel7: TUniPanel
              Left = 1
              Top = 1
              Width = 627
              Height = 138
              Hint = ''
              ShowHint = True
              Align = alLeft
              Anchors = [akLeft, akTop, akBottom]
              TabOrder = 1
              Caption = ''
              object UniLabel14: TUniLabel
                Left = 3
                Top = 8
                Width = 86
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Localizar C.barras'
                TabOrder = 1
              end
              object UniLabel15: TUniLabel
                Left = 293
                Top = 8
                Width = 53
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'A Devolver'
                TabOrder = 2
              end
              object UniDBText1: TUniDBText
                Left = 216
                Top = 8
                Width = 56
                Height = 13
                Hint = ''
                ShowHint = True
              end
              object UniDBEdit13: TUniDBEdit
                Left = 3
                Top = 22
                Width = 208
                Height = 26
                Hint = ''
                ShowHint = True
                TabOrder = 4
              end
              object UniDBEdit14: TUniDBEdit
                Left = 211
                Top = 22
                Width = 70
                Height = 26
                Hint = ''
                ShowHint = True
                TabOrder = 5
              end
              object UniDBEdit15: TUniDBEdit
                Left = 281
                Top = 22
                Width = 70
                Height = 26
                Hint = ''
                ShowHint = True
                TabOrder = 6
              end
              object UniSpeedButton1: TUniSpeedButton
                Left = 351
                Top = 6
                Width = 50
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = '+'
                ParentColor = False
                Color = clWindow
                TabOrder = 7
              end
              object UniSpeedButton2: TUniSpeedButton
                Left = 400
                Top = 6
                Width = 50
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = '-'
                ParentColor = False
                Color = clWindow
                TabOrder = 8
              end
              object UniBitBtn2: TUniBitBtn
                Left = 454
                Top = 6
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Detalle'
                TabOrder = 9
              end
              object UniBitBtn3: TUniBitBtn
                Left = 508
                Top = 6
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Ult.Dev.'
                TabOrder = 10
              end
              object UniBitBtn4: TUniBitBtn
                Left = 563
                Top = 6
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Todo Vta'
                TabOrder = 11
              end
              object UniDBText2: TUniDBText
                Left = 3
                Top = 59
                Width = 65
                Height = 16
                Hint = ''
                ShowHint = True
                ParentFont = False
                Font.Height = -13
              end
              object UniPanel8: TUniPanel
                Left = 0
                Top = 80
                Width = 281
                Height = 58
                Hint = ''
                ShowHint = True
                TabOrder = 13
                Caption = ''
                object UniLabel16: TUniLabel
                  Left = 3
                  Top = 3
                  Width = 32
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Prove:'
                  TabOrder = 1
                end
                object UniDBText3: TUniDBText
                  Left = 41
                  Top = 3
                  Width = 56
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  ParentFont = False
                end
                object UniDBText4: TUniDBText
                  Left = 100
                  Top = 3
                  Width = 56
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  ParentFont = False
                end
                object UniDBEdit16: TUniDBEdit
                  Left = 3
                  Top = 20
                  Width = 156
                  Height = 26
                  Hint = ''
                  ShowHint = True
                  TabOrder = 4
                end
                object UniBitBtn5: TUniBitBtn
                  Left = 162
                  Top = 20
                  Width = 27
                  Height = 26
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 5
                end
                object UniCheckBox1: TUniCheckBox
                  Left = 192
                  Top = 3
                  Width = 87
                  Height = 17
                  Hint = ''
                  ShowHint = True
                  Caption = 'Pase Auto'
                  TabOrder = 6
                end
                object UniCheckBox2: TUniCheckBox
                  Left = 192
                  Top = 18
                  Width = 75
                  Height = 17
                  Hint = ''
                  ShowHint = True
                  Caption = 'Teorica'
                  TabOrder = 7
                end
                object UniCheckBox3: TUniCheckBox
                  Left = 192
                  Top = 33
                  Width = 82
                  Height = 17
                  Hint = ''
                  ShowHint = True
                  Caption = 'Ver m'#225's +'
                  TabOrder = 8
                end
              end
              object UniBitBtn6: TUniBitBtn
                Left = 563
                Top = 93
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'UniBitBtn2'
                TabOrder = 14
              end
            end
            object UniPanel9: TUniPanel
              Left = 628
              Top = 2
              Width = 313
              Height = 137
              Hint = ''
              ShowHint = True
              TabOrder = 2
              Caption = ''
              object UniGroupBox1: TUniGroupBox
                Left = 2
                Top = -3
                Width = 307
                Height = 88
                Hint = ''
                ShowHint = True
                Caption = 'Seleccion por Fechas'
                TabOrder = 1
                object UniLabel17: TUniLabel
                  Left = 4
                  Top = 40
                  Width = 34
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Desde:'
                  TabOrder = 1
                end
                object UniLabel18: TUniLabel
                  Left = 4
                  Top = 62
                  Width = 32
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Hasta:'
                  TabOrder = 2
                end
                object UniDBDateTimePicker2: TUniDBDateTimePicker
                  Left = 42
                  Top = 36
                  Width = 96
                  Hint = ''
                  ShowHint = True
                  DateTime = 43375.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 3
                end
                object UniDBDateTimePicker3: TUniDBDateTimePicker
                  Left = 42
                  Top = 58
                  Width = 96
                  Hint = ''
                  ShowHint = True
                  DateTime = 43375.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 4
                end
                object UniDBDateTimePicker4: TUniDBDateTimePicker
                  Left = 138
                  Top = 36
                  Width = 96
                  Hint = ''
                  ShowHint = True
                  DateTime = 43375.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 5
                end
                object UniDBDateTimePicker5: TUniDBDateTimePicker
                  Left = 138
                  Top = 58
                  Width = 96
                  Hint = ''
                  ShowHint = True
                  DateTime = 43375.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 6
                end
                object UniBitBtn7: TUniBitBtn
                  Left = 236
                  Top = 39
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = 'Ejecutar'
                  TabOrder = 7
                end
                object UniLabel19: TUniLabel
                  Left = 53
                  Top = 21
                  Width = 59
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'F.Recepcion'
                  TabOrder = 8
                end
                object UniLabel20: TUniLabel
                  Left = 150
                  Top = 21
                  Width = 62
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'F.Devolucion'
                  TabOrder = 9
                end
                object UniLabel21: TUniLabel
                  Left = 240
                  Top = 17
                  Width = 28
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Regs.'
                  TabOrder = 10
                end
              end
              object UniSpeedButton3: TUniSpeedButton
                Left = 1
                Top = 92
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Traspas'
                ParentColor = False
                Color = clWindow
                TabOrder = 2
              end
              object UniLabel22: TUniLabel
                Left = 99
                Top = 94
                Width = 62
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Id Recepcion'
                TabOrder = 3
              end
              object UniDBEdit17: TUniDBEdit
                Left = 94
                Top = 109
                Width = 71
                Height = 20
                Hint = ''
                ShowHint = True
                TabOrder = 4
              end
              object UniSpeedButton4: TUniSpeedButton
                Left = 181
                Top = 92
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Teorico'
                ParentColor = False
                Color = clWindow
                TabOrder = 5
              end
              object UniSpeedButton5: TUniSpeedButton
                Left = 238
                Top = 92
                Width = 55
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Limpiar'
                ParentColor = False
                Color = clWindow
                TabOrder = 6
              end
            end
          end
          object UniPageControl3: TUniPageControl
            Left = 0
            Top = 140
            Width = 1083
            Height = 316
            Hint = ''
            ShowHint = True
            ActivePage = UniTabSheet2
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 1
            object UniTabSheet1: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'UniTabSheet1'
              object UniDBGrid1: TUniDBGrid
                Left = 0
                Top = 0
                Width = 1075
                Height = 288
                Hint = ''
                ShowHint = True
                LoadMask.Message = 'Loading data...'
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 0
              end
            end
            object UniTabSheet2: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'UniTabSheet2'
              object UniDBGrid2: TUniDBGrid
                Left = 0
                Top = 39
                Width = 1075
                Height = 249
                Hint = ''
                ShowHint = True
                LoadMask.Message = 'Loading data...'
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 0
              end
              object UniPanel10: TUniPanel
                Left = 0
                Top = 0
                Width = 1075
                Height = 39
                Hint = ''
                ShowHint = True
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 1
                Caption = ''
                object UniLabel23: TUniLabel
                  Left = 233
                  Top = 9
                  Width = 96
                  Height = 19
                  Hint = ''
                  ShowHint = True
                  Caption = 'Detalle devol.'
                  ParentFont = False
                  Font.Height = -16
                  TabOrder = 1
                end
                object UniBitBtn8: TUniBitBtn
                  Left = 335
                  Top = 8
                  Width = 129
                  Height = 29
                  Hint = ''
                  ShowHint = True
                  Caption = 'UniBitBtn8'
                  TabOrder = 2
                end
                object UniLabel24: TUniLabel
                  Left = 470
                  Top = 0
                  Width = 43
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Cantidad'
                  TabOrder = 3
                end
                object UniLabel25: TUniLabel
                  Left = 550
                  Top = 0
                  Width = 54
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Descripcion'
                  TabOrder = 4
                end
                object UniLabel26: TUniLabel
                  Left = 834
                  Top = 1
                  Width = 42
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'C.Barras'
                  TabOrder = 5
                end
                object UniDBEdit18: TUniDBEdit
                  Left = 469
                  Top = 15
                  Width = 63
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  TabOrder = 6
                end
                object UniDBNavigator1: TUniDBNavigator
                  Left = 0
                  Top = 2
                  Width = 225
                  Height = 37
                  Hint = ''
                  ShowHint = True
                  VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
                  TabOrder = 7
                end
                object UniDBEdit19: TUniDBEdit
                  Left = 532
                  Top = 15
                  Width = 293
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  TabOrder = 8
                end
                object UniDBEdit20: TUniDBEdit
                  Left = 829
                  Top = 15
                  Width = 104
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  TabOrder = 9
                end
                object UniDBEdit21: TUniDBEdit
                  Left = 936
                  Top = 15
                  Width = 42
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  TabOrder = 10
                end
              end
            end
          end
        end
        object TabSheet7: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Anulado'
          object UniDBMemo1: TUniDBMemo
            Left = 1
            Top = 263
            Width = 1005
            Height = 193
            Hint = ''
            ShowHint = True
            TabOrder = 0
          end
          object UniPanel11: TUniPanel
            Left = 8
            Top = 16
            Width = 184
            Height = 53
            Hint = ''
            ShowHint = True
            TabOrder = 1
            Caption = ''
            object UniBitBtn9: TUniBitBtn
              Left = 2
              Top = 4
              Width = 60
              Height = 45
              Hint = ''
              ShowHint = True
              Caption = 'Nueva'
              TabOrder = 1
            end
            object UniBitBtn10: TUniBitBtn
              Left = 61
              Top = 4
              Width = 60
              Height = 45
              Hint = ''
              ShowHint = True
              Caption = 'Modificar'
              TabOrder = 2
            end
          end
        end
      end
    end
    object TabDirectoDevolucion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'TabDirectoDevolucion'
      object AdvPanel2: TUniPanel
        Left = 0
        Top = 0
        Width = 1091
        Height = 49
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        object UniContainerPanel1: TUniContainerPanel
          Left = 1
          Top = 1
          Width = 488
          Height = 47
          Hint = ''
          ShowHint = True
          ParentColor = False
          Color = clRed
          Align = alLeft
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 1
          object btCargarDevolucionAuto: TUniBitBtn
            Left = 8
            Top = 4
            Width = 125
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cargar Devolucion'
            TabOrder = 1
          end
          object btVolverADevolucion: TUniBitBtn
            Left = 152
            Top = 4
            Width = 125
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Volver a Devoluci'#243'n'
            TabOrder = 2
          end
          object btEliminarDirectoDevolucion: TUniBitBtn
            Left = 350
            Top = 4
            Width = 125
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Borrar Directo'
            TabOrder = 3
          end
        end
      end
      object UniDBGrid4: TUniDBGrid
        Left = 0
        Top = 49
        Width = 1091
        Height = 435
        Hint = ''
        ShowHint = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
      end
      object PanelCargarDevolucion: TUniPanel
        Left = 632
        Top = 3
        Width = 112
        Height = 113
        Hint = ''
        ShowHint = True
        TabOrder = 2
        Caption = ''
        object spTraspasar1: TUniBitBtn
          Left = 8
          Top = 7
          Width = 97
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Traspasar solo 1'
          TabOrder = 1
        end
        object spTraspasarTodos: TUniBitBtn
          Left = 8
          Top = 63
          Width = 97
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Traspasar Todos'
          TabOrder = 2
        end
      end
    end
    object TabPdteDevolu: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'TabPdteDevolu'
      object UniPanel19: TUniPanel
        Left = 0
        Top = 0
        Width = 1091
        Height = 49
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        object UniContainerPanel2: TUniContainerPanel
          Left = 1
          Top = 1
          Width = 398
          Height = 47
          Hint = ''
          ShowHint = True
          ParentColor = False
          Color = clLime
          Align = alLeft
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 1
          object UniBitBtn14: TUniBitBtn
            Left = 8
            Top = 4
            Width = 125
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cargar Devolucion'
            TabOrder = 1
          end
          object UniBitBtn15: TUniBitBtn
            Left = 256
            Top = 4
            Width = 125
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Volver a Devoluci'#243'n'
            TabOrder = 2
          end
        end
      end
      object UniDBGrid5: TUniDBGrid
        Left = 0
        Top = 49
        Width = 1091
        Height = 435
        Hint = ''
        ShowHint = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
      end
    end
  end
  object PanelConfiguracion: TUniPanel
    Left = 482
    Top = 233
    Width = 665
    Height = 297
    Hint = ''
    ShowHint = True
    TabOrder = 2
    Caption = ''
    object BtnReclama: TUniBitBtn
      Left = 12
      Top = 10
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Reclama'
      TabOrder = 1
    end
    object pChkVerArti: TUniCheckBox
      Left = 10
      Top = 86
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Todos los proveedores'
      TabOrder = 2
    end
    object pCbxLineaAutomatica: TUniCheckBox
      Left = 10
      Top = 62
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Checked = True
      Caption = 'L'#237'nea autom'#225'tica'
      TabOrder = 3
    end
    object cbSoloProveedor: TUniCheckBox
      Left = 10
      Top = 110
      Width = 217
      Height = 17
      Hint = ''
      ShowHint = True
      Checked = True
      Caption = 'Devoluci'#243'n de solo este proveedor'
      TabOrder = 4
    end
    object pcxCoDistri: TUniCheckBox
      Left = 10
      Top = 134
      Width = 217
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Acceder por c'#243'digo de distribuidora'
      TabOrder = 5
    end
    object cbSumando: TUniCheckBox
      Left = 10
      Top = 159
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Devolver Sumando'
      TabOrder = 6
    end
    object cbUnchecked: TUniCheckBox
      Left = 10
      Top = 183
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Separando precios'
      TabOrder = 7
    end
    object cbxDevolTeorica: TUniCheckBox
      Left = 10
      Top = 207
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Checked = True
      Caption = 'Solo Devol.Teorica'
      TabOrder = 8
    end
    object cbCerrarCodigos: TUniCheckBox
      Left = 10
      Top = 231
      Width = 217
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Cerrar Compras ya vendidas / devueltas'
      TabOrder = 9
    end
    object cbAbrirCerrados: TUniCheckBox
      Left = 10
      Top = 256
      Width = 179
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'Abrir Movimientos cerrados'
      TabOrder = 10
    end
    object cbVerSumando: TUniCheckBox
      Left = 136
      Top = 159
      Width = 91
      Height = 17
      Hint = ''
      ShowHint = True
      Caption = 'SUMANDO'
      TabOrder = 11
    end
    object UniLabel63: TUniLabel
      Left = 179
      Top = 8
      Width = 45
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Etiquetas'
      TabOrder = 12
    end
    object edEtiquetas: TUniDBEdit
      Left = 181
      Top = 24
      Width = 40
      Height = 20
      Hint = ''
      ShowHint = True
      TabOrder = 13
    end
    object btListados: TUniBitBtn
      Left = 228
      Top = 10
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Listados'
      TabOrder = 14
    end
    object BtnPrecios: TUniBitBtn
      Left = 498
      Top = 12
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Precios'
      TabOrder = 15
    end
    object btSalirConfiguracion: TUniBitBtn
      Left = 585
      Top = 12
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Salir'
      TabOrder = 16
    end
    object PageAbrirCerrarCodigos: TUniPageControl
      Left = 306
      Top = 56
      Width = 321
      Height = 161
      Hint = ''
      ShowHint = True
      ActivePage = UniTabSheet4
      TabOrder = 17
      object UniTabSheet3: TUniTabSheet
        Hint = ''
        ShowHint = True
        Caption = 'Cerrar Compras'
        object UniLabel64: TUniLabel
          Left = 24
          Top = 8
          Width = 93
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cerrar hasta Fecha'
          TabOrder = 0
        end
        object edCerrarHastaFecha: TUniDBDateTimePicker
          Left = 21
          Top = 24
          Width = 116
          Hint = ''
          ShowHint = True
          DateTime = 43374.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
        end
        object UniLabel65: TUniLabel
          Left = 168
          Top = 8
          Width = 78
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Numero a cerrar'
          TabOrder = 2
        end
        object edNumeroCerrar: TUniDBEdit
          Left = 168
          Top = 25
          Width = 97
          Height = 21
          Hint = ''
          ShowHint = True
          TabOrder = 3
        end
        object btCerrarCodigos: TUniBitBtn
          Left = 22
          Top = 56
          Width = 117
          Height = 33
          Hint = ''
          ShowHint = True
          Caption = 'Cerrar C'#243'digos'
          TabOrder = 4
        end
        object lbRegistros: TUniLabel
          Left = 152
          Top = 72
          Width = 53
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'lbRegistros'
          TabOrder = 5
        end
        object LabelMensajeCerrar: TUniLabel
          Left = 16
          Top = 104
          Width = 96
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'LabelMensajeCerrar'
          TabOrder = 6
        end
        object lbRegistrosAcabados: TUniLabel
          Left = 152
          Top = 104
          Width = 53
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'lbRegistros'
          TabOrder = 7
        end
        object lbRegistrosEnProceso: TUniLabel
          Left = 152
          Top = 120
          Width = 53
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'lbRegistros'
          TabOrder = 8
        end
        object ProgressBar1: TUniProgressBar
          Left = 152
          Top = 88
          Width = 81
          Height = 17
          Hint = ''
          ShowHint = True
          TabOrder = 9
        end
      end
      object UniTabSheet4: TUniTabSheet
        Hint = ''
        ShowHint = True
        Caption = 'Abrir Movimientos'
        object btAbrirCodigos: TUniBitBtn
          Left = 14
          Top = 56
          Width = 139
          Height = 33
          Hint = ''
          ShowHint = True
          Caption = 'Traspasar Todos'
          TabOrder = 0
        end
        object btBuscarNumero: TUniBitBtn
          Left = 8
          Top = 24
          Width = 145
          Height = 25
          Hint = ''
          ShowHint = True
          Caption = 'Buscar Ultimo Numero'
          TabOrder = 1
        end
        object edUltimoNumeroCierre: TUniDBEdit
          Left = 168
          Top = 25
          Width = 121
          Height = 21
          Hint = ''
          ShowHint = True
          TabOrder = 2
        end
        object UniLabel66: TUniLabel
          Left = 168
          Top = 8
          Width = 111
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ultimo Numero Cerrado'
          TabOrder = 3
        end
        object LabelMensajeAbrir: TUniLabel
          Left = 16
          Top = 104
          Width = 88
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'LabelMensajeAbrir'
          TabOrder = 4
        end
      end
    end
  end
end
