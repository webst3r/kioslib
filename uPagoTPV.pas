unit uPagoTPV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormPagoTPV = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    pcControlTotal: TUniPageControl;
    tabCambio: TUniTabSheet;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniListBox1: TUniListBox;
    tabTarejtas: TUniTabSheet;
    btVolver: TUniBitBtn;
    btVisa: TUniBitBtn;
    btMaestro: TUniBitBtn;
    btMasterCard: TUniBitBtn;
    btElectron: TUniBitBtn;
    btTab: TUniBitBtn;
    btAmerican: TUniBitBtn;
    btDiners: TUniBitBtn;
    btOtras: TUniBitBtn;
    UniTabSheet3: TUniTabSheet;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniLabel5: TUniLabel;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormPagoTPV: TFormPagoTPV;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMManteTPV;

function FormPagoTPV: TFormPagoTPV;
begin
  Result := TFormPagoTPV(DMppal.GetFormInstance(TFormPagoTPV));

end;


end.
