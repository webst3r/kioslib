unit uFichaArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniDBCheckBox,
  uniImageList, math;

type
  TFormManteArti = class(TUniForm)
    tmSession: TUniTimer;
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniLabel16: TUniLabel;
    UniLabel18: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    edDiasCadu: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    edTipoProducto: TUniDBLookupComboBox;
    edPeriodicidad: TUniDBLookupComboBox;
    edBarras: TUniDBEdit;
    UniPageControl2: TUniPageControl;
    UniTabSheet3: TUniTabSheet;
    UniPanel3: TUniPanel;
    UniTabSheet8: TUniTabSheet;
    PageControl: TUniPageControl;
    UniDBNavigator1: TUniDBNavigator;
    GridPreus: TUniDBGrid;
    btTecNume: TUniBitBtn;
    edPreu: TUniDBEdit;
    edMargen: TUniDBEdit;
    edMargen2: TUniEdit;
    edEncarte1: TUniDBEdit;
    UniLabel69: TUniLabel;
    UniLabel70: TUniLabel;
    UniLabel71: TUniLabel;
    edTIva1: TUniDBEdit;
    edTPCIVA1: TUniDBEdit;
    edTPCRE1: TUniDBEdit;
    edCoste1: TUniDBEdit;
    edCosteDire: TUniDBEdit;
    edSumarCoste: TUniEdit;
    UniLabel73: TUniLabel;
    UniLabel74: TUniLabel;
    UniLabel75: TUniLabel;
    UniLabel76: TUniLabel;
    UniLabel77: TUniLabel;
    UniSpeedButton34: TUniSpeedButton;
    UniDBEdit15: TUniDBEdit;
    UniBitBtn15: TUniBitBtn;
    UniLabel37: TUniLabel;
    dsFichaArticulo: TDataSource;
    rgTipoBarras: TUniDBRadioGroup;
    dsEditorialS: TDataSource;
    dsArTitulo: TDataSource;
    dsPreus: TDataSource;
    dsPeriodicidad: TDataSource;
    dsVarisSubGrupo: TDataSource;
    pnBotonera: TUniPanel;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    UniLabel2: TUniLabel;
    dsTipoProdu: TDataSource;
    btAceptar: TUniButton;
    btCancelar: TUniButton;
    btNuevoArticulo: TUniBitBtn;
    btModificarArti: TUniBitBtn;
    btConfirmarArti: TUniBitBtn;
    btCancelarArti: TUniBitBtn;
    btBorrarArti: TUniBitBtn;
    btHistorico: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    edPreu2: TUniEdit;
    UniLabel30: TUniLabel;
    UniLabel31: TUniLabel;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    edPVP2: TUniDBEdit;
    UniDBEdit17: TUniDBEdit;
    UniDBEdit18: TUniDBEdit;
    UniDBEdit20: TUniDBEdit;
    edTIVA2: TUniDBEdit;
    UniDBEdit22: TUniDBEdit;
    UniDBEdit23: TUniDBEdit;
    edCoste2: TUniDBEdit;
    UniDBEdit26: TUniDBEdit;
    UniDBEdit27: TUniDBEdit;
    UniLabel38: TUniLabel;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    edCodbarras: TUniEdit;
    lbCodigoBarras: TUniLabel;
    lbGrabar: TUniLabel;
    lbVolver: TUniLabel;
    procedure btListaClick(Sender: TObject);
    procedure btConsuClick(Sender: TObject);
    procedure UniBitBtn18Click(Sender: TObject);
    procedure btAceptarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edTipoProductoChange(Sender: TObject);
    procedure btTecNumeClick(Sender: TObject);
    procedure edCosteDireExit(Sender: TObject);
    procedure UniSpeedButton34Click(Sender: TObject);
    procedure edTIva1Exit(Sender: TObject);
    procedure edCoste1Exit(Sender: TObject);
    procedure edPreuExit(Sender: TObject);
    procedure edMargenExit(Sender: TObject);
    procedure edEncarte1Exit(Sender: TObject);
    procedure edSumarCosteExit(Sender: TObject);
    procedure edPVP2Exit(Sender: TObject);
    procedure edTIVA2Exit(Sender: TObject);
    procedure edCoste2Exit(Sender: TObject);
    procedure btNuevoArticuloClick(Sender: TObject);
    procedure btBorrarArtiClick(Sender: TObject);
    procedure btModificarArtiClick(Sender: TObject);
    procedure btConfirmarArtiClick(Sender: TObject);
    procedure btCancelarArtiClick(Sender: TObject);
    procedure btHistoricoClick(Sender: TObject);
    procedure edPreuKeyPress(Sender: TObject; var Key: Char);
    procedure edCodbarrasExit(Sender: TObject);
    procedure edCodbarrasEnter(Sender: TObject);
    procedure edPreuEnter(Sender: TObject);
    procedure edMargenEnter(Sender: TObject);
    procedure edTIva1Enter(Sender: TObject);
    procedure edTPCRE1Enter(Sender: TObject);
    procedure edCoste1Enter(Sender: TObject);
    procedure UniDBEdit15Enter(Sender: TObject);
    procedure edCosteDireEnter(Sender: TObject);
    procedure edSumarCosteEnter(Sender: TObject);
    procedure edMargen2Enter(Sender: TObject);
    procedure edPreu2Enter(Sender: TObject);


  private

    function RutComprobarArti0(vBarres : String): Boolean;
    function RutBuscarExisteBarras(vBarres: String): Boolean;


    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2, vStringOrigen : string;
    vHora : Double;
    swAppend : Boolean;
    procedure RutShow;
    procedure RutHabitlitarBTs(vTipo: Boolean);
    procedure RutLeerTipoBarras(vCodBarres: String);
  end;

function FormManteArti: TFormManteArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule, uConstVar,
  uMenu, uMenuArti, uListaArti, uConsArti, uDMMenuArti, uDevolMenu
  , uDevolFicha, uDMDevol, uMensajes, uEscogerArti;

function FormManteArti: TFormManteArti;
begin
  Result := TFormManteArti(DMppal.GetFormInstance(TFormManteArti));

end;


procedure TFormManteArti.btConfirmarArtiClick(Sender: TObject);
begin

  if DMMenuArti.sqlFichaArticulo.State <> dsBrowse then
  begin
    DMMenuArti.sqlFichaArticulo.Post;
    if swAppend = true then
    begin
      if RutComprobarArti0(DMMenuArti.sqlFichaArticuloBARRAS.AsString) then
      else
      begin
        DMMenuArti.sqlFichaArticulo0.Append;
        CopyRecordDataset(DMMenuArti.sqlFichaArticulo, DMMenuArti.sqlFichaArticulo0,0);
        DMMenuArti.SP_GEN_ARTICULO0.ExecProc;
        DMMenuArti.sqlFichaArticulo0ID_ARTICULO.AsInteger := DMMenuArti.SP_GEN_ARTICULO0.Params[0].Value;
        DMMenuArti.sqlFichaArticulo0.Post;
      end;
    end;
  end;

  RutHabitlitarBTs(False);
end;

function TFormManteArti.RutComprobarArti0(vBarres : String):Boolean;
begin
  Result := false;
  with DMMenuArti.sqlFichaArticulo0 do
  begin

    close;
    SQL.Text :=  Copy(SQL.Text,1,
         pos('WHERE', Uppercase(SQL.Text))-1)
         + 'WHERE BARRAS = '  + QuotedStr(vBarres);
    Open;

    if RecordCount > 0 then Result := true;

  end;


end;

procedure TFormManteArti.btConsuClick(Sender: TObject);
begin
  DMppal.swConsArti := self.Name;
  FormMenuArti.RutAbrirConsArti;
end;

procedure TFormManteArti.btHistoricoClick(Sender: TObject);
begin
  FormMenu.RutAbrirHistoArtiTablas(DMMenuArti.sqlFichaArticuloID_ARTICULO.AsInteger, DMMenuArti.sqlFichaArticuloBARRAS.AsString
                            ,DMMenuArti.sqlFichaArticuloADENDUM.AsString,DMMenuArti.sqlFichaArticuloDESCRIPCION.AsString
                            , 'articulosFicha');
  DMppal.swHisArti := self.Name;
end;

procedure TFormManteArti.btListaClick(Sender: TObject);
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;
  FormMenuArti.RutAbrirListaArti;

end;

procedure TFormManteArti.btModificarArtiClick(Sender: TObject);
begin
  RutHabitlitarBTs(True);
  swAppend := false;
  DMMenuArti.sqlFichaArticulo.Edit;
  lbCodigoBarras.Visible := true;
  edCodbarras.Visible := true;
end;

procedure TFormManteArti.RutHabitlitarBTs(vTipo : Boolean);
begin
  btModificarArti.Enabled   := not vTipo;
  btConfirmarArti.Visible   := vTipo;
  btCancelarArti.Visible    := vTipo;
end;

procedure TFormManteArti.btAceptarClick(Sender: TObject);
begin

  DMMenuArti.sqlFichaArticulo.Post;
  if RutComprobarArti0(DMMenuArti.sqlFichaArticuloBARRAS.AsString) then
  else
  begin
    DMMenuArti.sqlFichaArticulo0.Append;
    CopyRecordDataset(DMMenuArti.sqlFichaArticulo, DMMenuArti.sqlFichaArticulo0,0);
    DMMenuArti.SP_GEN_ARTICULO0.ExecProc;
    DMMenuArti.sqlFichaArticulo0ID_ARTICULO.AsInteger := DMMenuArti.SP_GEN_ARTICULO0.Params[0].Value;
    DMMenuArti.sqlFichaArticulo0.Post;
  end;

  DMMenuArti.swCreando := false;

  //pasar el valor cap a devol
  FormDevolMenu.RutAbrirArtiDevol;
  FormDevolFicha.pBarrasA.Text := DMMenuArti.sqlFichaArticuloBARRAS.AsString;
  DMDevolucion.RutLeerCodigoBarras;
  DMDevolucion.sqlLinea.Refresh;
  DMDevolucion.sqlLineaTotal.Refresh;
  DMDevolucion.RutComprobarPaquete;
 // FormDevolMenu.RutAbrirAlbaranTab;
end;

procedure TFormManteArti.btBorrarArtiClick(Sender: TObject);
begin
  FormMensajes.RutInicioForm(1, UpperCase(self.Name), 'ACEPTARCANC', '�Quiere Borrar el Articulo?');
end;

procedure TFormManteArti.btCancelarArtiClick(Sender: TObject);
begin
  DMMenuArti.sqlFichaArticulo.Cancel;
//  FormMenuArti.RutAbrirListaArti;
  RutHabitlitarBTs(False);
  edCodbarras.Visible    := false;
  lbCodigoBarras.Visible := false;
end;

procedure TFormManteArti.btCancelarClick(Sender: TObject);
begin
  if DMMenuArti.sqlFichaArticulo.State <> dsBrowse then DMMenuArti.sqlFichaArticulo.Cancel;
  FormDevolMenu.RutAbrirArtiDevol;
end;

procedure TFormManteArti.btTecNumeClick(Sender: TObject);
begin
  DMMenuArti.RutCalcularPreciosArti;
end;


procedure TFormManteArti.RutLeerTipoBarras(vCodBarres : String);
var
  vLongitud: Integer; vCodResult : String;
begin
  vLongitud := length(vCodBarres);
  if (vLongitud = 18) or
     (vLongitud = 15) or
     (vLongitud = 13) or
     (vLongitud = 9) or
     (vLongitud = 7) then
  begin
    if vLongitud = 18 then
    begin
      vCodResult := Copy(vCodBarres,1,13);
      rgTipoBarras.ItemIndex := 5;
    end;

    if vLongitud = 15 then
    begin
      vCodResult := Copy(vCodBarres,1,13);
      rgTipoBarras.ItemIndex := 4;
    end;

     if vLongitud = 13 then
    begin
      vCodResult := vCodbarres;
      rgTipoBarras.ItemIndex := 3;
    end;

    if vLongitud = 9 then
    begin
      vCodResult := Copy(vCodBarres,1,7);
      rgTipoBarras.ItemIndex := 2;
    end;

    if vLongitud = 7 then
    begin
      vCodResult := vCodbarres;
      rgTipoBarras.ItemIndex := 1;
    end;

    if (RutBuscarExisteBarras(vCodResult)) and (edCodBarras.Visible = true) then
    begin
      FormEscogerArti.gridUsuarios.DataSource := FormEscogerArti.dsArtiBarras;
      FormEscogerArti.lbBarras.Caption := 'C�digo de Barras Duplicado';
      FormEscogerArti.btDuplicar.Visible := true;
      FormEscogerArti.ShowModal();
    end;

    DMMenuArti.sqlFichaArticuloBARRAS.AsString := vCodResult;

  end;
end;

function TFormManteArti.RutBuscarExisteBarras(vBarres : String):Boolean;
begin
   Result := false;
   with DMMenuArti.sqlComprobarBarras do
   begin
     close;
     SQL.Text :=  'Select A.* '
                 + 'from FMARTICULO A '
                 + 'WHERE A.BARRAS = ' + QuotedStr(vBarres)
                 + 'order by A.DESCRIPCION';

     Open;

     if RecordCount > 0 then Result := true;


   end;
end;

procedure TFormManteArti.edCoste1Enter(Sender: TObject);
begin
  edCoste1.SelectAll;
end;

procedure TFormManteArti.edCoste1Exit(Sender: TObject);
begin
  if DMPpal.RutEdiInse(DMMenuArti.sqlFichaArticulo) = False Then Exit;
  DMMenuArti.RutCalculaCost_Taula_Tot (DMMenuArti.sqlFichaArticulo, '1A');
end;

procedure TFormManteArti.edCoste2Exit(Sender: TObject);
begin
  if dmppal.RutEdiInse(DMMenuArti.sqlFichaArticulo) = False Then Exit;
  DMMenuArti.RutCalculaCost_Taula_Tot (DMMenuArti.sqlFichaArticulo, '2A');
end;

procedure TFormManteArti.edCosteDireEnter(Sender: TObject);
begin
  edCosteDire.SelectAll;
end;

procedure TFormManteArti.edCosteDireExit(Sender: TObject);
var wMarge : Double;
begin
  if  StrToFloatDef(edSumarCoste.Text, 0) <> 0 then
    edPreu2.Text := FormatFloat(',0.00', (StrToFloatDef(edCosteDire.Text,0) + StrToFloatDef(edSumarCoste.Text, 0)));

//  pCosteDire.Visible := False;
  if DMMenuArti.sqlFichaArticuloPRECIO1.AsFloat = 0 then exit;
  if DMMenuArti.sqlFichaArticuloTPCIVA1.AsFloat = 0 then exit;

  wMarge := DMMenuArti.RutCalculMarge_A  (DMMenuArti.sqlFichaArticuloPRECIO1.AsFloat,
                               DMMenuArti.sqlFichaArticuloTPCIVA1.AsFloat,
                               DMMenuArti.sqlFichaArticuloPRECIOCOSTE.AsFloat);


  if DMPpal.RutEdiInse (DMMenuArti.sqlFichaArticulo) = False Then DMMenuArti.sqlFichaArticulo.Edit;
  DMMenuArti.sqlFichaArticuloMARGEN.AsFloat := wMarge;
  wMarge := RoundTo(wMarge,2);
  DMMenuArti.ProCalcularPreuCost1;
end;

procedure TFormManteArti.edEncarte1Exit(Sender: TObject);
begin
  DMMenuArti.ProCalcularPreuCost1;
end;

procedure TFormManteArti.edMargen2Enter(Sender: TObject);
begin
  edMargen2.SelectAll;
end;

procedure TFormManteArti.edMargenEnter(Sender: TObject);
begin
 edMargen.SelectAll;
end;

procedure TFormManteArti.edMargenExit(Sender: TObject);
begin
  DMMenuArti.ProCalcularPreuCost1;
end;

procedure TFormManteArti.edPreu2Enter(Sender: TObject);
begin
  edPreu2.SelectAll;
end;

procedure TFormManteArti.edPreuEnter(Sender: TObject);
begin
  edPreu.SelectAll;
end;

procedure TFormManteArti.edPreuExit(Sender: TObject);
begin
  DMMenuArti.ProCalcularPreuCost1;
end;

procedure TFormManteArti.edPreuKeyPress(Sender: TObject; var Key: Char);
begin
  if ((Uppercase(key)='.')) then key:=',';
end;

procedure TFormManteArti.edSumarCosteEnter(Sender: TObject);
begin
  edSumarCoste.SelectAll;
end;

procedure TFormManteArti.edSumarCosteExit(Sender: TObject);
begin
  edPreu2.Text := FormatFloat(',0.00', (StrToFloatDef(edCosteDire.Text,0) + StrToFloatDef(edSumarCoste.Text, 0)));
end;

procedure TFormManteArti.edTipoProductoChange(Sender: TObject);
begin
  if DMMenuArti.swCreando then
  begin
    DMMenuArti.sqlFichaArticuloCADUCIDAD.AsInteger := DMMenuArti.sqlTProduCADUCIDAD.AsInteger;
    DMMenuArti.sqlFichaArticuloMARGEN.AsFloat := DMMenuArti.sqlTProduMARGEN.AsFloat;
  end;
end;

procedure TFormManteArti.edTIva1Enter(Sender: TObject);
begin
  edTIva1.SelectAll;
end;

procedure TFormManteArti.edTIva1Exit(Sender: TObject);
begin
  DMMenuArti.ProCalcularPreuCost1;
end;

procedure TFormManteArti.edTIVA2Exit(Sender: TObject);
begin
  edPVP2Exit(nil);
end;

procedure TFormManteArti.edTPCRE1Enter(Sender: TObject);
begin
  edTPCRE1.SelectAll;
end;

procedure TFormManteArti.UniBitBtn18Click(Sender: TObject);
begin
  FormMenuArti.Close;
  FormMenu.lbFormNombre.Caption := '';
end;

procedure TFormManteArti.UniDBEdit15Enter(Sender: TObject);
begin
  UniDBEdit15.SelectAll;
end;

procedure TFormManteArti.edCodbarrasEnter(Sender: TObject);
begin
  edCodbarras.SelectAll;
end;

procedure TFormManteArti.edCodbarrasExit(Sender: TObject);
begin
  RutLeerTipoBarras(edCodbarras.Text);
end;

procedure TFormManteArti.btNuevoArticuloClick(Sender: TObject);
begin
  DMMenuArti.sqlFichaArticulo.Append;
  DMMenuArti.SP_GEN_ARTICULO.ExecProc;
  DMMenuArti.sqlFichaArticuloID_ARTICULO.AsInteger := DMMenuArti.SP_GEN_ARTICULO.Params[0].Value;
  swAppend := true;
  FormManteArti.RutHabitlitarBTs(True);
  lbCodigoBarras.Visible := true;
  edCodbarras.Visible := true;
  edCodBarras.SetFocus;
  edCodbarras.Text := '';
end;

procedure TFormManteArti.edPVP2Exit(Sender: TObject);
begin
  DMMenuArti.ProCalcularPreuCost2;
end;

procedure TFormManteArti.RutShow;
begin
  if DMMenuArti.swCreando then
  begin
    btCancelar.Visible := true;
    btAceptar.Visible  := true;
    pnBotonera.Visible := false;
    lbGrabar.Visible   := true;
    lbVolver.Visible   := true;
  end else
  begin
    btCancelar.Visible := false;
    btAceptar.Visible  := false;
    pnBotonera.Visible := true;
    lbGrabar.Visible   := false;
    lbVolver.Visible   := false;
  end;


end;

procedure TFormManteArti.UniSpeedButton34Click(Sender: TObject);
begin
  edMargen2.Visible := true;
  edCosteDire.Visible := true;
  edSumarCoste.Visible := true;

  edCosteDire.SetFocus;
end;

end.








