object DMMenuRecepcion: TDMMenuRecepcion
  OldCreateOrder = False
  Height = 552
  Width = 807
  object sqlListaRecepcion: TFDQuery
    OnCalcFields = sqlListaRecepcionCalcFields
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_PROVEEDOR,A.FECHA,A.FECHACARGO, A.SWDOCTOPROVE,A.DO' +
        'CTOPROVE, A.DOCTOPROVEFECHA,A.NOMBRE,A.NIF,A.ID_DIRECCION,D.DIRE' +
        'CCION,D.POBLACION,D.PROVINCIA,D.CPOSTAL,D.CPROVINCIA,D.TELEFONO1' +
        ','
      'D.TELEFONO2,D.MOVIL,A.SEMANA,'
      
        '  (Select Sum(CANTIDAD)    From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)  as Cantidad,'
      
        '  (Select Sum(CANTIENALBA) From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE) as CantiEnAlba,'
      
        '  (Select Sum(VALORVENTA)  From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)  as ValVenta,'
      
        '  (Select Sum(IMPOIVA)     From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)     as ImpoIVA,'
      
        '  (Select Sum(IMPORE)      From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)    as ImpoRE,'
      
        '  (Select Sum(VALORCOSTE)  From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)     as ValCoste,'
      
        '  (Select Sum(VALORCOSTETOT)  From FVHISARTI  where IDSTOCABE = ' +
        'A.IDSTOCABE)  as ValCosteRE,'
      
        '  (Select Sum(VALORVENTA2)    From FVHISARTI  where IDSTOCABE = ' +
        'A.IDSTOCABE)  as ValVenta2,'
      
        '  (Select Sum(IMPOIVA2)    From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)     as ImpoIVA2,'
      
        '  (Select Sum(IMPORE2)     From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)    as ImpoRE2,'
      
        '  (Select Sum(VALORCOSTE2)    From FVHISARTI  where IDSTOCABE = ' +
        'A.IDSTOCABE)  as ValCoste2,'
      
        '  (Select Sum(VALORCOSTETOT2) From FVHISARTI  where IDSTOCABE = ' +
        'A.IDSTOCABE)  as ValCosteRE2,'
      
        '  (Select Sum(IMPOTOTLIN)  From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)  as ImpoTotLin,'
      
        '  (Select Sum(IMPOBRUTO)   From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE) as ImpoBruto,'
      
        '  (Select Sum(IMPODTO)     From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)     as ImpoDto,'
      
        '  (Select Sum(IMPOBASE)    From FVHISARTI  where IDSTOCABE = A.I' +
        'DSTOCABE)  as ImpoBase,'
      
        '  (Select Count(*)    From FVRECLAMA  where IDSTOCABE = A.IDSTOC' +
        'ABE and SWESTADO = 1)  as nRecla1,'
      
        '  (Select Count(*)    From FVRECLAMA  where IDSTOCABE = A.IDSTOC' +
        'ABE and SWESTADO = 0)  as nRecla0,'
      '   A.IDSTOCABE'
      ' From FVSTOCABE A'
      
        ' left outer join FMDIRECCION D on (D.ID_DIRECCION = A.ID_DIRECCI' +
        'ON)'
      ' where A.IDSTOCABE is not null and A.SWTIPODOCU = 0'
      ' Order by A.IDSTOCABE')
    Left = 40
    Top = 128
    object sqlListaRecepcionID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlListaRecepcionFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlListaRecepcionFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlListaRecepcionSWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlListaRecepcionDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlListaRecepcionDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlListaRecepcionNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlListaRecepcionNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlListaRecepcionID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlListaRecepcionDIRECCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlListaRecepcionPOBLACION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlListaRecepcionPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlListaRecepcionCPOSTAL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      ProviderFlags = []
      ReadOnly = True
      Size = 8
    end
    object sqlListaRecepcionCPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object sqlListaRecepcionTELEFONO1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
    object sqlListaRecepcionTELEFONO2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlListaRecepcionMOVIL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
    object sqlListaRecepcionSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlListaRecepcionCANTIDAD: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionCANTIENALBA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionVALVENTA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALVENTA'
      Origin = 'VALVENTA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPOIVA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPORE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionVALCOSTE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALCOSTE'
      Origin = 'VALCOSTE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlListaRecepcionVALCOSTERE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALCOSTERE'
      Origin = 'VALCOSTERE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionVALVENTA2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALVENTA2'
      Origin = 'VALVENTA2'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPOIVA2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPORE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionVALCOSTE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALCOSTE2'
      Origin = 'VALCOSTE2'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionVALCOSTERE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALCOSTERE2'
      Origin = 'VALCOSTERE2'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPOTOTLIN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPOBRUTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPODTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIMPOBASE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionNRECLA1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NRECLA1'
      Origin = 'NRECLA1'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionNRECLA0: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NRECLA0'
      Origin = 'NRECLA0'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlListaRecepcionIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlListaRecepcionCosteTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CosteTotal'
      DisplayFormat = ',0.000'
      Calculated = True
    end
  end
  object sqlHisArti: TFDQuery
    OnCalcFields = sqlHisArtiCalcFields
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select H.*,A.BARRAS,A.DESCRIPCION,A.TBARRAS'
      '  From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' where H.IDSTOCABE is not null'
      ' and H.SWCERRADO <> 1'
      'order by H.ID_HISARTI')
    Left = 160
    Top = 136
    object sqlHisArtiID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisArtiSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlHisArtiFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHisArtiHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlHisArtiCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlHisArtiID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHisArtiADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHisArtiID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHisArtiREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlHisArtiIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlHisArtiIDDEVOLUCABE: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object sqlHisArtiPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHisArtiID_HISARTI01: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object sqlHisArtiID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlHisArtiPARTIDA: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object sqlHisArtiNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlHisArtiID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHisArtiID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlHisArtiSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlHisArtiNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlHisArtiNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlHisArtiNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlHisArtiDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlHisArtiVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlHisArtiMERMA: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object sqlHisArtiCARGO: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object sqlHisArtiABONO: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object sqlHisArtiRECLAMADO: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object sqlHisArtiCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlHisArtiCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlHisArtiCANTISUSCRI: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object sqlHisArtiPRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOMPRA2: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlHisArtiTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlHisArtiTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlHisArtiSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlHisArtiVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOSTE2: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOSTETOT: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOSTETOT2: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOMPRA2: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORVENTA2: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlHisArtiMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlHisArtiENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object sqlHisArtiENCARTE2: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
    end
    object sqlHisArtiTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlHisArtiNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHisArtiSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHisArtiSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlHisArtiSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlHisArtiSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHisArtiSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisArtiSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHisArtiSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHisArtiSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHisArtiSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHisArtiSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHisArtiSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHisArtiTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlHisArtiFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlHisArtiFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlHisArtiFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHisArtiFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHisArtiSWRECLAMA: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object sqlHisArtiID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlHisArtiSWACABADO: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object sqlHisArtiBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlHisArtiDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHisArtiTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHisArtiCosteTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CosteTotal'
      DisplayFormat = ',0.0000'
      Calculated = True
    end
    object sqlHisArtiPrecioTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PrecioTotal'
      DisplayFormat = ',0.00'
      Calculated = True
    end
  end
  object sqlListaRecepcion1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FVSTOCABE'
      'where IDSTOCABE is not null'
      'Order by IDSTOCABE')
    Left = 240
    Top = 136
    object sqlListaRecepcion1IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlListaRecepcion1SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlListaRecepcion1NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlListaRecepcion1FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlListaRecepcion1NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlListaRecepcion1NALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlListaRecepcion1NALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlListaRecepcion1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlListaRecepcion1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlListaRecepcion1FABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlListaRecepcion1SWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlListaRecepcion1DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlListaRecepcion1DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlListaRecepcion1FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlListaRecepcion1FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlListaRecepcion1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlListaRecepcion1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlListaRecepcion1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlListaRecepcion1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlListaRecepcion1PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlListaRecepcion1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlListaRecepcion1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlListaRecepcion1SWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlListaRecepcion1DEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlListaRecepcion1DEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlListaRecepcion1DEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlListaRecepcion1DEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlListaRecepcion1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlListaRecepcion1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlListaRecepcion1SWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlListaRecepcion1SWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlListaRecepcion1SWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlListaRecepcion1SWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlListaRecepcion1SEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlListaRecepcion1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlListaRecepcion1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlListaRecepcion1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlListaRecepcion1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlListaRecepcion1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlListaRecepcion1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlListaRecepcion1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlListaRecepcion1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlReservasRecepcion: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select ID_ARTICULO, BARRAS18, CANTIRESERVA, CANTIRECI'
      'from FVRESERVA'
      'where ID_ARTICULO is not null')
    Left = 56
    Top = 200
    object sqlReservasRecepcionID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlReservasRecepcionBARRAS18: TStringField
      FieldName = 'BARRAS18'
      Origin = 'BARRAS18'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 18
    end
    object sqlReservasRecepcionCANTIRESERVA: TSingleField
      FieldName = 'CANTIRESERVA'
      Origin = 'CANTIRESERVA'
    end
    object sqlReservasRecepcionCANTIRECI: TSingleField
      FieldName = 'CANTIRECI'
      Origin = 'CANTIRECI'
    end
    object sqlReservasRecepcionDescripcion: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Descripcion'
      Size = 50
    end
  end
  object sqlUltiCompra: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select First 5 H.ID_ARTICULO, H.ADENDUM, H.FECHA'
      ', sum(H.entradas) as Entradas'
      ''
      ', (Select SUM(Coalesce(H1.Salidas,0)) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      
        '   and H1.adendum = H.Adendum and ((H1.clave = '#39'51'#39') or (H1.clav' +
        'e = '#39'53'#39'))'
      '   and H1.Fecha >= H.FECHA) as Ventas'
      ''
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum and H1.clave = '#39'54'#39
      '   and H1.Fecha >= H.FECHA) as Devoluciones'
      ''
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum and H1.clave = '#39'55'#39
      '   and H1.Fecha >= H.FECHA) as Merma'
      ''
      ', (Select SUM(H1.Entradas - H1.Salidas) From FVHISARTI H1'
      '   where H1.ID_ARTICULO = H.ID_ARTICULO '
      '   and H1.adendum = H.Adendum'
      '   and H1.Fecha >= H.FECHA) as Stock'
      ''
      'from FVHISARTI H'
      ''
      'WHERE H.ID_ARTICULO = 0'
      'and H.clave = '#39'01'#39
      ''
      'group by 1,2,3'
      'order by 1,3 desc,2')
    Left = 320
    Top = 136
    object sqlUltiCompraID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlUltiCompraADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlUltiCompraFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlUltiCompraENTRADAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraVENTAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAS'
      Origin = 'VENTAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraDEVOLUCIONES: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLUCIONES'
      Origin = 'DEVOLUCIONES'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraMERMA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMA'
      Origin = 'MERMA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlUltiCompraSTOCK: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'STOCK'
      Origin = 'STOCK'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlHisArtiTotResumen: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FechaCargo, '
      '        Sum(CANTIDAD)    as Cantidad,'
      '        Sum(CANTIENALBA) as CantiEnAlba,'
      
        '        Sum(IMPOBRUTO)   as ImpoBruto,     Sum(IMPODTO)     as I' +
        'mpoDto,'
      
        '        Sum(IMPOBASE)    as ImpoBase,      Sum(IMPOIVA)     as I' +
        'mpoIVA,'
      
        '        Sum(IMPORE)      as ImpoRE,        Sum(IMPOTOTLIN)  as I' +
        'mpoTotLin,'
      
        '        Sum(VALORCOSTE)  as ValorCoste,    Sum(VALORCOMPRA) as V' +
        'alorCompra,'
      
        '        Sum(VALORVENTA)  as ValorVenta,    Sum(VALORMOVI)   as V' +
        'alorMovi,'
      ''
      
        '        Sum(IMPOCOMISION) as ImpoComision, Sum(IMPOBASE2)     as' +
        ' ImpoBase2,'
      
        '        Sum(IMPOIVA2)     as ImpoIva2,     Sum(IMPORE2)       as' +
        ' ImpoRe2,'
      
        '        Sum(VALORCOSTE2)  as ValorCoste2,  Sum(VALORCOMPRA2)  as' +
        ' ValorCompra2,'
      
        '        Sum(VALORVENTA2)  as ValorVenta2,  Sum(VALORCOSTETOT) as' +
        ' ValorCosteTot,'
      ''
      '        Sum(VALORCOSTE + VALORCOSTE2)  as ValorCosteTotal,'
      ''
      '       Count(*) as Lineas'
      ''
      'From FVHISARTI'
      ''
      'where IDSTOCABE is not null'
      'group by FechaCargo'
      'Order by FechaCargo')
    Left = 160
    Top = 200
    object sqlHisArtiTotResumenFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHisArtiTotResumenCANTIDAD: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHisArtiTotResumenCANTIENALBA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHisArtiTotResumenIMPOBRUTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPODTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOBASE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOIVA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPORE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOTOTLIN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOSTE: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOMPRA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORVENTA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORMOVI: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOCOMISION: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOBASE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPOIVA2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenIMPORE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOSTE2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOMPRA2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORVENTA2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOSTETOT: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenVALORCOSTETOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALORCOSTETOTAL'
      Origin = 'VALORCOSTETOTAL'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object sqlHisArtiTotResumenLINEAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'LINEAS'
      Origin = 'LINEAS'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlCabe1: TFDQuery
    AfterScroll = sqlCabe1AfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FC.*'
      ', P.maxpaquete'
      ', (SELECT SUM(Devueltos) as TotalDevol'
      '   FROM FVHISARTI'
      '   WHERE IDSTOCABE = FC.idstocabe)'
      ''
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where FC.IDSTOCABE > 0 AND FC.SWTIPODOCU = 0'
      'Order by FC.IDSTOCABE')
    Left = 112
    Top = 128
    object sqlCabe1IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabe1SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabe1NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlCabe1FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabe1NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlCabe1NALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlCabe1NALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlCabe1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabe1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlCabe1FABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlCabe1SWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlCabe1DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabe1DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabe1FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlCabe1FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlCabe1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabe1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabe1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabe1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlCabe1PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlCabe1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlCabe1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlCabe1SWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlCabe1DEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlCabe1DEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlCabe1DEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlCabe1DEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlCabe1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlCabe1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlCabe1SWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlCabe1SWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlCabe1SWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlCabe1SWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlCabe1SEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlCabe1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCabe1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCabe1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCabe1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCabe1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCabe1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCabe1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCabe1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlAntiguos: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  A.ID_PROVEEDOR, A.FECHA, A.IDSTOCABE'
      ' From FVSTOCABE A'
      ' where A.IDSTOCABE is not null'
      ' Order by A.FECHA')
    Left = 304
    Top = 200
    object sqlAntiguosID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlAntiguosFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlAntiguosIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object sqlAntiguosLines: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select H.*,A.BARRAS,A.DESCRIPCION,A.TBARRAS'
      '  From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' where H.ID_HISARTI is not null'
      ' and H.SWCERRADO <> 1')
    Left = 392
    Top = 200
    object sqlAntiguosLinesID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlAntiguosLinesSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlAntiguosLinesFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlAntiguosLinesHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlAntiguosLinesCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlAntiguosLinesID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlAntiguosLinesADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlAntiguosLinesID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlAntiguosLinesREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlAntiguosLinesIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlAntiguosLinesIDDEVOLUCABE: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object sqlAntiguosLinesPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlAntiguosLinesID_HISARTI01: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object sqlAntiguosLinesID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlAntiguosLinesPARTIDA: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object sqlAntiguosLinesNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlAntiguosLinesID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlAntiguosLinesID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlAntiguosLinesSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlAntiguosLinesNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlAntiguosLinesNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlAntiguosLinesNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlAntiguosLinesDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlAntiguosLinesVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlAntiguosLinesMERMA: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object sqlAntiguosLinesCARGO: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object sqlAntiguosLinesABONO: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object sqlAntiguosLinesRECLAMADO: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object sqlAntiguosLinesCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlAntiguosLinesCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlAntiguosLinesCANTISUSCRI: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object sqlAntiguosLinesPRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
    end
    object sqlAntiguosLinesPRECIOCOMPRA2: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
    end
    object sqlAntiguosLinesPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlAntiguosLinesPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlAntiguosLinesPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlAntiguosLinesPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlAntiguosLinesPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object sqlAntiguosLinesPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object sqlAntiguosLinesSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object sqlAntiguosLinesTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
    end
    object sqlAntiguosLinesTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlAntiguosLinesTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlAntiguosLinesTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlAntiguosLinesTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlAntiguosLinesTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlAntiguosLinesTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlAntiguosLinesIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object sqlAntiguosLinesIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object sqlAntiguosLinesIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
    end
    object sqlAntiguosLinesIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
    end
    object sqlAntiguosLinesIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
    end
    object sqlAntiguosLinesIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object sqlAntiguosLinesIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object sqlAntiguosLinesIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object sqlAntiguosLinesIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
    end
    object sqlAntiguosLinesENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlAntiguosLinesSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlAntiguosLinesVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
    end
    object sqlAntiguosLinesVALORCOSTE2: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
    end
    object sqlAntiguosLinesVALORCOSTETOT: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
    end
    object sqlAntiguosLinesVALORCOSTETOT2: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
    end
    object sqlAntiguosLinesVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
    end
    object sqlAntiguosLinesVALORCOMPRA2: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
    end
    object sqlAntiguosLinesVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
    end
    object sqlAntiguosLinesVALORVENTA2: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
    end
    object sqlAntiguosLinesVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
    end
    object sqlAntiguosLinesTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object sqlAntiguosLinesIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
    end
    object sqlAntiguosLinesMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlAntiguosLinesMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlAntiguosLinesENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object sqlAntiguosLinesENCARTE2: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
    end
    object sqlAntiguosLinesTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlAntiguosLinesNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlAntiguosLinesSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlAntiguosLinesSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlAntiguosLinesSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlAntiguosLinesSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlAntiguosLinesSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlAntiguosLinesSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlAntiguosLinesSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlAntiguosLinesSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlAntiguosLinesSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlAntiguosLinesSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlAntiguosLinesSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlAntiguosLinesTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlAntiguosLinesFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlAntiguosLinesFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlAntiguosLinesFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlAntiguosLinesFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlAntiguosLinesSWRECLAMA: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object sqlAntiguosLinesID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlAntiguosLinesSWACABADO: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object sqlAntiguosLinesBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlAntiguosLinesDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlAntiguosLinesTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlProveS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_PROVEEDOR,A.NOMBRE,A.DIRECCION,A.POBLACION,A.PROVIN' +
        'CIA,A.CPOSTAL,A.CPROVINCIA,'
      '   A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL'
      'From FMPROVE A'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 56
    Top = 256
    object sqlProveSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProveSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProveSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlProveSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlProveSPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlProveSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlProveSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlProveSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProveSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlProveSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlProveSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
  end
  object sqlArticulos1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  * From  FMARTICULO'
      'WHERE ID_ARTICULO = 0')
    Left = 136
    Top = 264
    object sqlArticulos1ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArticulos1TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArticulos1BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArticulos1ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArticulos1FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArticulos1NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArticulos1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArticulos1DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArticulos1IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArticulos1ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArticulos1EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArticulos1PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArticulos1PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArticulos1PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArticulos1PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArticulos1PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArticulos1PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArticulos1PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArticulos1TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArticulos1TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArticulos1TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArticulos1TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArticulos1TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArticulos1TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArticulos1TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArticulos1PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArticulos1PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArticulos1PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArticulos1PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArticulos1MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArticulos1MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArticulos1MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArticulos1EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArticulos1REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArticulos1PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArticulos1CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArticulos1TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArticulos1TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArticulos1SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArticulos1SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArticulos1SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArticulos1SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArticulos1SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArticulos1SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArticulos1TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArticulos1TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArticulos1TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArticulos1TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArticulos1AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArticulos1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArticulos1STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArticulos1STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArticulos1FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArticulos1FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArticulos1ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArticulos1STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArticulos1STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArticulos1STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArticulos1REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArticulos1UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArticulos1UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArticulos1IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArticulos1COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArticulos1GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArticulos1SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArticulos1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArticulos1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArticulos1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArticulos1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
  end
  object sqlArti1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select A.*'
      ', P.NOMBRE as NomProve'
      ' from FMARTICULO A'
      ' left outer join FMPROVE P on (P.id_proveedor = A.editorial)'
      'where A.BARRAS = '#39#39
      'and A.ADENDUM = '#39#39
      'and A.EDITORIAL = 0'
      'Order by A.BARRAS,A.ADENDUM,A.EDITORIAL'
      '')
    Left = 96
    Top = 32
    object sqlArti1ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArti1TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArti1BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArti1ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArti1FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArti1NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArti1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArti1DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArti1IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArti1ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArti1EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArti1PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArti1PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArti1PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArti1PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArti1PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArti1PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArti1PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArti1TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArti1TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArti1TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArti1TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArti1TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArti1TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArti1TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArti1PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArti1PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArti1PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArti1PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArti1MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArti1MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArti1MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArti1EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArti1REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArti1PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArti1CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArti1TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArti1TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArti1SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArti1SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArti1SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArti1SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArti1SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArti1SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArti1TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArti1TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArti1TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArti1TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArti1AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArti1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArti1STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArti1STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArti1FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArti1FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArti1ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArti1STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArti1STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArti1STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArti1REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArti1UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArti1UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArti1IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArti1COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArti1GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArti1SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArti1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArti1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArti1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArti1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlArti1NOMPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object sqlArti0: TFDQuery
    Connection = DMppal.FDConnection0
    SQL.Strings = (
      'Select A.*'
      ', P.NOMBRE as NomProve'
      ' from FMARTICULO A'
      ' left outer join FMPROVE P on (P.id_proveedor = A.editorial)'
      'where A.BARRAS = '#39#39
      'and A.ADENDUM = '#39#39
      'and A.EDITORIAL = 0'
      'Order by A.BARRAS,A.ADENDUM,A.EDITORIAL')
    Left = 32
    Top = 32
    object sqlArti0ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArti0TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArti0BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArti0ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArti0FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArti0NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArti0DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArti0DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArti0IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArti0ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArti0EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArti0PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArti0PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArti0PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArti0PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArti0PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArti0PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArti0PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArti0TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArti0TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArti0TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArti0TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArti0TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArti0TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArti0TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArti0PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArti0PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArti0PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArti0PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArti0MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArti0MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArti0MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArti0EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArti0REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArti0PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArti0CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArti0TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArti0TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArti0SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArti0SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArti0SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArti0SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArti0SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArti0SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArti0TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArti0TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArti0TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArti0TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArti0AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArti0OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArti0STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArti0STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArti0FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArti0FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArti0ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArti0STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArti0STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArti0STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArti0REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArti0UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArti0UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArti0IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArti0COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArti0GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArti0SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArti0SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArti0FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArti0FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArti0FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlArti0NOMPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object sqlHisArtiCabe1: TFDQuery
    OnCalcFields = sqlHisArtiCalcFields
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FC.*'
      ', P.maxpaquete'
      ', (SELECT SUM(Devueltos) as TotalDevol'
      '   FROM FVHISARTI'
      '   WHERE IDSTOCABE = FC.idstocabe)'
      ''
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where FC.IDSTOCABE > 0 AND FC.SWTIPODOCU = 0'
      'Order by FC.IDSTOCABE')
    Left = 192
    Top = 40
    object sqlHisArtiCabe1IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisArtiCabe1SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlHisArtiCabe1NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlHisArtiCabe1FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHisArtiCabe1NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHisArtiCabe1NALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlHisArtiCabe1NALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlHisArtiCabe1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHisArtiCabe1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlHisArtiCabe1FABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlHisArtiCabe1SWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlHisArtiCabe1DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlHisArtiCabe1DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlHisArtiCabe1FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHisArtiCabe1FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHisArtiCabe1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlHisArtiCabe1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlHisArtiCabe1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlHisArtiCabe1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlHisArtiCabe1PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHisArtiCabe1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHisArtiCabe1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisArtiCabe1SWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlHisArtiCabe1DEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlHisArtiCabe1DEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlHisArtiCabe1DEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlHisArtiCabe1DEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlHisArtiCabe1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHisArtiCabe1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHisArtiCabe1SWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHisArtiCabe1SWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHisArtiCabe1SWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHisArtiCabe1SWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHisArtiCabe1SEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHisArtiCabe1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHisArtiCabe1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlHisArtiCabe1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlHisArtiCabe1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlHisArtiCabe1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlHisArtiCabe1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlHisArtiCabe1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlHisArtiCabe1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlHisArtiCabe1MAXPAQUETE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHisArtiCabe1TOTALDEVOL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALDEVOL'
      Origin = 'TOTALDEVOL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object SP_GEN_HISARTI1: TFDStoredProc
    Connection = DMppal.FDConnection1
    StoredProcName = 'SP_GEN_HISARTI'
    Left = 344
    Top = 40
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
end
