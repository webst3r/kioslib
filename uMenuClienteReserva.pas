unit uMenuClienteReserva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormMenuClienteReserva = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    pnl1: TUniPanel;
    UniLabel1: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniLabel13: TUniLabel;
    UniDBEdit15: TUniDBEdit;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniDBNavigator1: TUniDBNavigator;
    UniPageControl1: TUniPageControl;
    tabTablas: TUniTabSheet;
    tabNuevo: TUniTabSheet;
    UniPanel1: TUniPanel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniLabel6: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniLabel7: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    UniLabel8: TUniLabel;
    UniDBEdit5: TUniDBEdit;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniDBEdit8: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    UniDBRadioGroup2: TUniDBRadioGroup;
    chLunes: TUniCheckBox;
    chMartes: TUniCheckBox;
    chMiercoles: TUniCheckBox;
    chJueves: TUniCheckBox;
    chViernes: TUniCheckBox;
    chSabado: TUniCheckBox;
    chDomingo: TUniCheckBox;
    UniGroupBox1: TUniGroupBox;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniDBDateTimePicker4: TUniDBDateTimePicker;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniGroupBox2: TUniGroupBox;
    UniDBDateTimePicker5: TUniDBDateTimePicker;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    UniDBRadioGroup3: TUniDBRadioGroup;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniLabel16: TUniLabel;
    UniDBEdit6: TUniDBEdit;
    UniDBGrid1: TUniDBGrid;
    UniDBGrid2: TUniDBGrid;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormMenuClienteReserva: TFormMenuClienteReserva;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuCliente;

function FormMenuClienteReserva: TFormMenuClienteReserva;
begin
  Result := TFormMenuClienteReserva(DMppal.GetFormInstance(TFormMenuClienteReserva));

end;


end.
