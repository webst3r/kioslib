unit uArticulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniRadioButton;

type
  TFormArticulos = class(TUniForm)
    dsArticulos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    pcTrabajos: TUniPageControl;
    tabArticulos: TUniTabSheet;
    btFinTrabajo: TUniButton;
    pnlBusqueda: TUniPanel;
    btSeleccionar: TUniButton;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    btBorrarTrabajo: TUniButton;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    tabProgramacion: TUniTabSheet;
    pcProgramacion: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniPanel3: TUniPanel;
    edMemoSQL: TUniMemo;
    UniPanel4: TUniPanel;
    btEjecutarMemo: TUniButton;
    rgTipoExec: TUniRadioGroup;
    UniButton1: TUniButton;
    cbParada: TUniCheckBox;
    edMensajeTecnico: TUniEdit;
    UniMemo1: TUniMemo;
    edTema: TUniComboBox;
    UniLabel10: TUniLabel;
    UniDBNavigator5: TUniDBNavigator;
    btAvisoDireccion: TUniBitBtn;
    edNombre: TUniLabel;
    GridProgramacion: TUniDBGrid;
    DataSource1: TDataSource;
    btNuevoTrabajo: TUniButton;
    rbFiltroNombre: TUniRadioGroup;
    gridArt: TUniDBGrid;

    procedure tmSessionTimer(Sender: TObject);
    procedure tmSessionEventHandler(Sender: TComponent; EventName: string;
      Params: TUniStrings);
    procedure UniFormCreate(Sender: TObject);



  private

//    procedure RutDisableAllTimer;
    procedure RutExecID;
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2, vControlGrid : string;
    vHora : Double;




  end;

function FormArticulos: TFormArticulos;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormArticulos: TFormArticulos;
begin
  Result := TFormArticulos(DMppal.GetFormInstance(TFormArticulos));

end;




procedure TFormArticulos.UniFormCreate(Sender: TObject);
begin
//inicio de pantalla
  pcTrabajos.ActivePage  := tabArticulos;

 { with gridArticulos do
  begin

    Columns[0].Width := 77;
    Columns[1].Width := 85;
    Columns[2].Width := 77;
    Columns[3].Width := 80;
    Columns[4].Width := 80;
    Columns[5].Width := 80;
    Columns[6].Width := 320;
    Columns[7].Width := 77;

  end;
  }
end;







procedure TFormArticulos.tmSessionEventHandler(Sender: TComponent;
  EventName: string; Params: TUniStrings);
begin
//Evento vacio para que no se cierre el programa
end;

procedure TFormArticulos.tmSessionTimer(Sender: TObject);
begin
//Evento vacio para que no se cierre el programa
end;

{
procedure TFormArticulos.btEjecutarMemoClick(Sender: TObject);
begin
  with DMppal.sqlUpdate do
  begin
    Close;
    SQL.Text := edMemoSQL.Text;
    if rgTipoExec.ItemIndex = 0 then open;
    if rgTipoExec.ItemIndex = 1 then ExecSQL;
  end;
end;
}


procedure TFormArticulos.RutExecID;
begin
 // DMppal.swAltaFMURA := True;

 // DMppal.sqlTrabajos.Append;
end;

end.
