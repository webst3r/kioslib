unit uCuadreMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormCuadreMenu = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaCuadre: TUniTabSheet;
    tabFichaCuadre: TUniTabSheet;



  private


    { Private declarations }

  public
    { Public declarations }
    swListaCuadre, swFichaCuadre : Integer;

    procedure RutAbrirLista;
    procedure RutAbrirFicha(vID: Integer);


  end;

function FormCuadreMenu: TFormCuadreMenu;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMCuadre, uCuadreLista, uCuadreFicha;

function FormCuadreMenu: TFormCuadreMenu;
begin
  Result := TFormCuadreMenu(DMppal.GetFormInstance(TFormCuadreMenu));

end;

procedure TFormCuadreMenu.RutAbrirLista;
begin
  pcDetalle.ActivePage := tabListaCuadre;
  if pcDetalle.ActivePage = tabListaCuadre then
  begin
    if swListaCuadre = 0 then
    begin
      FormCuadreLista.Parent := tabListaCuadre;
      //FormMenuDevolucionLista.pnlBtsMenu2.Parent := FormMenuDevolucionLista.pnlBtsLista;
      FormCuadreLista.Show();
      swListaCuadre := 1;
      DMCuadre.RutAbrirTablasLista;
    end;
  end;
  DMPpal.RutCapturaGrid(FormCuadreLista.Name, FormCuadreLista.gridListaFra, nil, 0, 0, 0);
end;

procedure TFormCuadreMenu.RutAbrirFicha(vID : Integer);
begin
  pcDetalle.ActivePage := tabFichaCuadre;
  if pcDetalle.ActivePage = tabFichaCuadre then
  begin
    if swFichaCuadre = 0 then
    begin
      FormCuadreFicha.Parent := tabFichaCuadre;
      //FormMenuDevolucionLista.pnlBtsMenu2.Parent := FormMenuDevolucionLista.pnlBtsLista;
      FormCuadreFicha.Show();
      swFichaCuadre := 1;
      DMCuadre.RutAbrirFraProve1(vID);
    end;
  end;
  //DMPpal.RutCapturaGrid(FormDevolLista.Name, FormDevolLista.gridListaDevolucion, nil, 0, 0, 0);
end;

end.
