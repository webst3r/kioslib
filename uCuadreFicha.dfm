object FormCuadreFicha: TFormCuadreFicha
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 591
  ClientWidth = 1016
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 97
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    ExplicitWidth = 1000
    object UniLabel1: TUniLabel
      Left = 40
      Top = 16
      Width = 60
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Distribuidora'
      TabOrder = 1
    end
    object UniLabel2: TUniLabel
      Left = 40
      Top = 44
      Width = 52
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Su Factura'
      TabOrder = 2
    end
    object UniLabel3: TUniLabel
      Left = 40
      Top = 72
      Width = 69
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Fecha Factura'
      TabOrder = 3
    end
    object UniLabel9: TUniLabel
      Left = 576
      Top = 1
      Width = 31
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Abono'
      TabOrder = 4
    end
    object edDesdeCargo: TUniDBDateTimePicker
      Left = 392
      Top = 16
      Width = 120
      Hint = ''
      ShowHint = True
      DataField = 'FECHACARGOA'
      DataSource = dsFraProve1
      DateTime = 43444.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
    end
    object edHastaCargo: TUniDBDateTimePicker
      Left = 392
      Top = 44
      Width = 120
      Hint = ''
      ShowHint = True
      DataField = 'FECHACARGOZ'
      DataSource = dsFraProve1
      DateTime = 43444.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 6
    end
    object edDesdeAbono: TUniDBDateTimePicker
      Left = 576
      Top = 16
      Width = 120
      Hint = ''
      ShowHint = True
      DataField = 'FECHAABONOA'
      DataSource = dsFraProve1
      DateTime = 43444.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 7
    end
    object edHastaAbono: TUniDBDateTimePicker
      Left = 576
      Top = 44
      Width = 120
      Hint = ''
      ShowHint = True
      DataField = 'FECHAABONOZ'
      DataSource = dsFraProve1
      DateTime = 43444.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 8
    end
    object UniLabel4: TUniLabel
      Left = 352
      Top = 25
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 9
    end
    object UniLabel5: TUniLabel
      Left = 352
      Top = 44
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 10
    end
    object UniLabel6: TUniLabel
      Left = 542
      Top = 44
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 11
    end
    object UniLabel7: TUniLabel
      Left = 542
      Top = 25
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 12
    end
    object UniLabel8: TUniLabel
      Left = 392
      Top = 1
      Width = 29
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Cargo'
      TabOrder = 13
    end
    object rgOpcion: TUniRadioGroup
      Left = 728
      Top = 3
      Width = 161
      Height = 68
      Hint = ''
      ShowHint = True
      Items.Strings = (
        'Por Devoluci'#243'n'
        'Por Art'#237'culos')
      ItemIndex = 0
      Caption = 'Modo'
      TabOrder = 14
      OnClick = rgOpcionClick
    end
    object UniDBEdit1: TUniDBEdit
      Left = 114
      Top = 16
      Width = 220
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'NOMBRE'
      DataSource = dsFraProve1
      TabOrder = 15
    end
    object UniDBEdit2: TUniDBEdit
      Left = 114
      Top = 44
      Width = 220
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'SUFACTURA'
      DataSource = dsFraProve1
      TabOrder = 16
    end
    object UniDBEdit3: TUniDBEdit
      Left = 114
      Top = 71
      Width = 220
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'FECHAFACTURA'
      DataSource = dsFraProve1
      TabOrder = 17
    end
  end
  object pcModo: TUniPageControl
    Left = 0
    Top = 97
    Width = 1016
    Height = 494
    Hint = ''
    ShowHint = True
    ActivePage = tabPorDevolucion
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ExplicitWidth = 1000
    ExplicitHeight = 463
    object tabPorDevolucion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabPorDevolucion'
      ExplicitWidth = 992
      ExplicitHeight = 435
      object UniDBGrid2: TUniDBGrid
        Left = 0
        Top = 0
        Width = 1008
        Height = 466
        Hint = ''
        ShowHint = True
        DataSource = dsAlbaAbonoP
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
    object tabPorArticulos: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabPorArticulos'
      ExplicitWidth = 992
      ExplicitHeight = 435
      object UniDBGrid1: TUniDBGrid
        Left = 0
        Top = 0
        Width = 1008
        Height = 466
        Hint = ''
        ShowHint = True
        DataSource = dsHistoP
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
  end
  object dsFraProve1: TDataSource
    DataSet = DMCuadre.sqlFraProve1
    Left = 912
    Top = 113
  end
  object dsHistoP: TDataSource
    DataSet = DMCuadre.sqlHistoP
    Left = 908
    Top = 225
  end
  object dsAlbaAbonoP: TDataSource
    DataSet = DMCuadre.sqlAlbaAbonoP
    Left = 908
    Top = 169
  end
end
