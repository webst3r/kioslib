object FormDevolNuevo: TFormDevolNuevo
  Left = 0
  Top = 0
  ClientHeight = 244
  ClientWidth = 531
  Caption = ''
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  PixelsPerInch = 96
  TextHeight = 13
  object lbAccion: TUniLabel
    Left = 19
    Top = 2
    Width = 63
    Height = 18
    Hint = ''
    Caption = 'lbAccion'
    ParentFont = False
    Font.Height = -15
    Font.Style = [fsBold]
    TabOrder = 0
  end
  object pcDetalle: TUniPageControl
    Left = 0
    Top = 40
    Width = 531
    Height = 204
    Hint = ''
    ActivePage = tabEdit
    TabBarVisible = False
    Align = alBottom
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 1
    object tabEdit: TUniTabSheet
      Hint = ''
      Caption = 'tabEdit'
      object cbDistri: TUniDBLookupComboBox
        Left = 83
        Top = 10
        Width = 424
        Height = 27
        Hint = ''
        ListField = 'NOMBRE'
        ListSource = dsProveS
        KeyField = 'NOMBRE'
        ListFieldIndex = 0
        DataField = 'NOMBRE'
        DataSource = dsCabe1
        ParentFont = False
        Font.Height = -16
        AnyMatch = True
        TabOrder = 0
        Color = clWindow
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'   config.lis' +
            'tConfig = {'#13#10'        cls: '#39'mylist'#39#13#10'    }'#13#10'}')
        Style = csDropDown
        OnChange = cbDistriChange
      end
      object UniLabel3: TUniLabel
        Left = 18
        Top = 13
        Width = 60
        Height = 13
        Hint = ''
        Caption = 'Distribuidora'
        TabOrder = 1
      end
      object edPaquete: TUniDBEdit
        Left = 84
        Top = 47
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'PAQUETE'
        DataSource = dsCabe1
        ParentFont = False
        Font.Height = -13
        TabOrder = 2
      end
      object UniLabel4: TUniLabel
        Left = 38
        Top = 52
        Width = 40
        Height = 13
        Hint = ''
        Caption = 'Paquete'
        TabOrder = 3
      end
      object UniLabel5: TUniLabel
        Left = 15
        Top = 85
        Width = 63
        Height = 13
        Hint = ''
        Caption = 'Max Paquete'
        TabOrder = 4
      end
      object UniDBEdit2: TUniDBEdit
        Left = 83
        Top = 82
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'MAXPAQUETE'
        DataSource = dsProveS
        ParentFont = False
        Font.Height = -13
        TabOrder = 5
      end
      object btGrabarGrupo: TUniButton
        Left = 320
        Top = 143
        Width = 85
        Height = 42
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 6
        OnClick = btGrabarGrupoClick
      end
      object btCancelar: TUniButton
        Left = 408
        Top = 143
        Width = 99
        Height = 42
        Hint = ''
        Caption = 'Cancelar'
        TabOrder = 7
        OnClick = btCancelarClick
      end
      object edFecha: TUniDBDateTimePicker
        Left = 387
        Top = 47
        Width = 120
        Hint = ''
        DataField = 'FECHA'
        DataSource = dsCabe1
        DateTime = 43395.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 8
        ParentFont = False
        Font.Height = -13
      end
      object UniLabel6: TUniLabel
        Left = 347
        Top = 52
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Fecha'
        TabOrder = 9
      end
      object UniLabel7: TUniLabel
        Left = 292
        Top = 85
        Width = 84
        Height = 13
        Hint = ''
        Caption = 'Fecha Devolucion'
        TabOrder = 10
      end
      object UniDBDateTimePicker2: TUniDBDateTimePicker
        Left = 387
        Top = 82
        Width = 120
        Hint = ''
        DataField = 'DOCTOPROVEFECHA'
        DataSource = dsCabe1
        DateTime = 43395.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 11
        ParentFont = False
        Font.Height = -13
      end
      object UniLabel8: TUniLabel
        Left = 3
        Top = 119
        Width = 75
        Height = 13
        Hint = ''
        Caption = 'Doc. Proveedor'
        TabOrder = 12
      end
      object UniDBEdit3: TUniDBEdit
        Left = 83
        Top = 116
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'DOCTOPROVE'
        DataSource = dsCabe1
        ParentFont = False
        Font.Height = -13
        TabOrder = 13
      end
      object btGrabarIndividual: TUniButton
        Left = 225
        Top = 143
        Width = 90
        Height = 42
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 14
        OnClick = btGrabarIndividualClick
      end
    end
    object tabNuevo: TUniTabSheet
      Hint = ''
      Caption = 'tabNuevo'
      object UniDBLookupComboBox1: TUniDBLookupComboBox
        Left = 83
        Top = 10
        Width = 424
        Height = 27
        Hint = ''
        ListField = 'NOMBRE'
        ListSource = dsProveS
        KeyField = 'NOMBRE'
        ListFieldIndex = 0
        DataField = 'NOMBRE'
        DataSource = dsCabeNuevo
        ParentFont = False
        Font.Height = -16
        AnyMatch = True
        TabOrder = 0
        Color = clWindow
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'   config.lis' +
            'tConfig = {'#13#10'        cls: '#39'mylist'#39#13#10'    }'#13#10'}')
        Style = csDropDown
        OnChange = cbDistriChange
      end
      object UniLabel1: TUniLabel
        Left = 18
        Top = 13
        Width = 60
        Height = 13
        Hint = ''
        Caption = 'Distribuidora'
        TabOrder = 1
      end
      object edPaqueteNuevo: TUniDBEdit
        Left = 84
        Top = 47
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'PAQUETE'
        DataSource = dsCabeNuevo
        ParentFont = False
        Font.Height = -13
        TabOrder = 2
      end
      object UniLabel2: TUniLabel
        Left = 38
        Top = 52
        Width = 40
        Height = 13
        Hint = ''
        Caption = 'Paquete'
        TabOrder = 3
      end
      object UniLabel9: TUniLabel
        Left = 15
        Top = 85
        Width = 63
        Height = 13
        Hint = ''
        Caption = 'Max Paquete'
        TabOrder = 4
      end
      object UniDBEdit4: TUniDBEdit
        Left = 83
        Top = 82
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'MAXPAQUETE'
        DataSource = dsProveS
        ParentFont = False
        Font.Height = -13
        TabOrder = 5
      end
      object btAceptarAgru: TUniButton
        Left = 320
        Top = 143
        Width = 85
        Height = 42
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 6
        OnClick = btGrabarGrupoClick
      end
      object UniButton2: TUniButton
        Left = 408
        Top = 143
        Width = 99
        Height = 42
        Hint = ''
        Caption = 'Cancelar'
        TabOrder = 7
        OnClick = btCancelarClick
      end
      object UniDBDateTimePicker1: TUniDBDateTimePicker
        Left = 387
        Top = 47
        Width = 120
        Hint = ''
        DataField = 'FECHA'
        DataSource = dsCabeNuevo
        DateTime = 43395.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 8
        ParentFont = False
        Font.Height = -13
      end
      object UniLabel10: TUniLabel
        Left = 347
        Top = 52
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Fecha'
        TabOrder = 9
      end
      object UniLabel11: TUniLabel
        Left = 292
        Top = 85
        Width = 84
        Height = 13
        Hint = ''
        Caption = 'Fecha Devolucion'
        TabOrder = 10
      end
      object UniDBDateTimePicker3: TUniDBDateTimePicker
        Left = 387
        Top = 82
        Width = 120
        Hint = ''
        DataField = 'DOCTOPROVEFECHA'
        DataSource = dsCabeNuevo
        DateTime = 43395.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 11
        ParentFont = False
        Font.Height = -13
      end
      object UniLabel12: TUniLabel
        Left = 3
        Top = 119
        Width = 75
        Height = 13
        Hint = ''
        Caption = 'Doc. Proveedor'
        TabOrder = 12
      end
      object UniDBEdit5: TUniDBEdit
        Left = 83
        Top = 116
        Width = 121
        Height = 22
        Hint = ''
        DataField = 'DOCTOPROVE'
        DataSource = dsCabeNuevo
        ParentFont = False
        Font.Height = -13
        TabOrder = 13
      end
      object btAceptarIndv: TUniButton
        Left = 225
        Top = 143
        Width = 90
        Height = 42
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 14
        OnClick = btGrabarIndividualClick
      end
    end
  end
  object dsCabe1: TDataSource
    DataSet = DMDevolucion.sqlCabe1
    Left = 60
    Top = 188
  end
  object dsProveS: TDataSource
    DataSet = DMDevolucion.sqlProveS
    Left = 108
    Top = 196
  end
  object dsCabeNuevo: TDataSource
    DataSet = DMDevolucion.sqlCabeNuevo
    Left = 20
    Top = 156
  end
end
