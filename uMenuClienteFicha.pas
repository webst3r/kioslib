unit uMenuClienteFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniProgressBar, uniDBCheckBox;

type
  TFormMenuClienteFicha = class(TUniForm)
    pnl1: TUniPanel;
    btVolver: TUniBitBtn;
    btSalir: TUniBitBtn;
    btBuscar: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    btCambiarCodigo: TUniButton;
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    pCodi: TUniDBEdit;
    UniLabel2: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniLabel3: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniDBEdit7: TUniDBEdit;
    UniLabel8: TUniLabel;
    UniDBEdit8: TUniDBEdit;
    UniLabel9: TUniLabel;
    UniDBEdit9: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniDBEdit10: TUniDBEdit;
    UniLabel5: TUniLabel;
    UniDBEdit11: TUniDBEdit;
    UniLabel10: TUniLabel;
    UniDBEdit12: TUniDBEdit;
    UniLabel11: TUniLabel;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBMemo1: TUniDBMemo;
    UniLabel13: TUniLabel;
    UniDBEdit15: TUniDBEdit;
    UniLabel14: TUniLabel;
    UniGroupBox1: TUniGroupBox;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniLabel12: TUniLabel;
    UniDBEdit17: TUniDBEdit;
    UniDBEdit19: TUniDBEdit;
    UniLabel15: TUniLabel;
    UniDBEdit20: TUniDBEdit;
    UniLabel16: TUniLabel;
    UniDBEdit21: TUniDBEdit;
    UniDBEdit22: TUniDBEdit;
    UniLabel17: TUniLabel;
    UniDBEdit23: TUniDBEdit;
    UniDBEdit24: TUniDBEdit;
    UniLabel18: TUniLabel;
    UniDBEdit25: TUniDBEdit;
    UniDBEdit26: TUniDBEdit;
    UniLabel19: TUniLabel;
    UniDBEdit27: TUniDBEdit;
    UniLabel20: TUniLabel;
    UniLabel21: TUniLabel;
    UniLabel22: TUniLabel;
    UniDBEdit28: TUniDBEdit;
    btValidarDC: TUniButton;
    lbResuValidacion: TUniLabel;
    PanelCambiarCodigo: TUniPanel;
    edCodigoAntiguo: TUniEdit;
    edCodigoNuevo: TUniEdit;
    ProgressBar1: TUniProgressBar;
    rgTipoCambioCodigo: TUniRadioGroup;
    UniLabel23: TUniLabel;
    btEjecutarCambiarCodigo: TUniBitBtn;
    dsCabe: TDataSource;
    UniDBEdit29: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    dsPoblacionS: TDataSource;
    UniDBEdit6: TUniDBEdit;
    UniDBRadioGroup2: TUniDBRadioGroup;
    cbFijarPantalla: TUniCheckBox;
    BtnAnterior: TUniBitBtn;
    BtnSiguiente: TUniBitBtn;
    UniDBLookupComboBox2: TUniDBLookupComboBox;
    UniDBLookupComboBox3: TUniDBLookupComboBox;
    dsTFacturacio: TDataSource;
    UniDBCheckBox1: TUniDBCheckBox;
    dsTPago: TDataSource;
    procedure btCambiarCodigoClick(Sender: TObject);
    procedure btEjecutarCambiarCodigoClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormMenuClienteFicha: TFormMenuClienteFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uDMMenuArti, uMenuCliente, uDMMenuCliente;

function FormMenuClienteFicha: TFormMenuClienteFicha;
begin
  Result := TFormMenuClienteFicha(DMppal.GetFormInstance(TFormMenuClienteFicha));

end;


procedure TFormMenuClienteFicha.btCambiarCodigoClick(Sender: TObject);
begin
  inherited;
  if PanelCambiarCodigo.Visible then
  begin
    PanelCambiarCodigo.Visible := False;
    exit;
  end;
  edCodigoAntiguo.Text := DMMenuCliente.sqlCabeID_CLIENTE.AsString;

  with PanelCambiarCodigo do
  begin
    Top := btCambiarCodigo.Top;
    Left:= btCambiarCodigo.Left + btCambiarCodigo.Width;
    Visible := True;
    BringToFront;
    btEjecutarCambiarCodigo.Enabled := True;
    edCodigoNuevo.Text := '';
    edCodigoNuevo.SetFocus;
  end;
end;

procedure TFormMenuClienteFicha.btEjecutarCambiarCodigoClick(Sender: TObject);
begin
  PanelCambiarCodigo.Visible := False;
end;

end.
