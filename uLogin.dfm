object UniLoginForm2: TUniLoginForm2
  Left = 0
  Top = 0
  ClientHeight = 465
  ClientWidth = 448
  Caption = ''
  OnShow = UniLoginFormShow
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object UniContainerPanel4: TUniContainerPanel
    Left = 32
    Top = 24
    Width = 393
    Height = 193
    Hint = ''
    ParentColor = False
    TabOrder = 0
    object UniLabel20: TUniLabel
      Left = 22
      Top = 11
      Width = 62
      Height = 23
      Hint = ''
      Caption = 'Usuario'
      ParentFont = False
      Font.Height = -19
      TabOrder = 1
    end
    object edUsuario: TUniEdit
      Left = 95
      Top = 3
      Width = 275
      Height = 30
      Hint = ''
      Text = ''
      ParentFont = False
      Font.Height = -19
      TabOrder = 2
      OnEnter = edUsuarioEnter
    end
    object edPassword: TUniEdit
      Left = 95
      Top = 39
      Width = 275
      Height = 30
      Hint = ''
      PasswordChar = '*'
      Text = ''
      ParentFont = False
      Font.Height = -19
      TabOrder = 3
      OnEnter = edPasswordEnter
    end
    object UniLabel21: TUniLabel
      Left = 6
      Top = 40
      Width = 78
      Height = 23
      Hint = ''
      Caption = 'Password'
      ParentFont = False
      Font.Height = -19
      TabOrder = 4
    end
    object edAceptarLogin: TUniButton
      Left = 95
      Top = 116
      Width = 275
      Height = 35
      Hint = ''
      Caption = 'Conectar'
      ParentFont = False
      Font.Height = -16
      TabOrder = 5
      OnClick = edAceptarLoginClick
    end
    object lbPath: TUniLabel
      Left = 6
      Top = 169
      Width = 158
      Height = 13
      Hint = ''
      Visible = False
      Caption = 'Correcto, esperando conexi'#243'n...'
      TabOrder = 6
    end
    object cbRecuerdame: TUniCheckBox
      Left = 6
      Top = 85
      Width = 78
      Height = 17
      Hint = ''
      Caption = 'Recuerdame'
      TabOrder = 7
    end
    object btLogout: TUniButton
      Left = 248
      Top = 95
      Width = 41
      Height = 31
      Hint = ''
      Visible = False
      Caption = 'Desconectar'
      ParentFont = False
      Font.Height = -16
      TabOrder = 8
      OnClick = btLogoutClick
    end
    object cbDesconectar: TUniCheckBox
      Left = 290
      Top = 85
      Width = 80
      Height = 17
      Hint = ''
      Caption = 'Desconectar'
      TabOrder = 9
      OnClick = cbDesconectarClick
    end
    object lbNuevoUsuario: TUniLabel
      Left = 179
      Top = 159
      Width = 191
      Height = 23
      Cursor = crHandPoint
      Hint = ''
      Caption = 'Quieres registrarte?'
      ParentFont = False
      Font.Height = -19
      Font.Style = [fsBold, fsItalic, fsUnderline]
      TabOrder = 10
      OnClick = lbNuevoUsuarioClick
    end
  end
  object edNuevaPass: TUniEdit
    Left = 127
    Top = 326
    Width = 275
    Height = 30
    Hint = ''
    PasswordChar = '*'
    Text = ''
    ParentFont = False
    Font.Height = -19
    TabOrder = 1
    TabStop = False
  end
  object edConfirmarPass: TUniEdit
    Left = 127
    Top = 378
    Width = 275
    Height = 30
    Hint = ''
    PasswordChar = '*'
    Text = ''
    ParentFont = False
    Font.Height = -19
    TabOrder = 2
    TabStop = False
  end
  object UniLabel1: TUniLabel
    Left = 24
    Top = 326
    Width = 94
    Height = 16
    Hint = ''
    Caption = 'Nuevo Password'
    ParentFont = False
    Font.Height = -13
    TabOrder = 3
  end
  object UniLabel2: TUniLabel
    Left = 60
    Top = 378
    Width = 61
    Height = 16
    Hint = ''
    Caption = 'Confirmar '
    ParentFont = False
    Font.Height = -13
    TabOrder = 4
  end
  object btCambiarPass: TUniButton
    Left = 208
    Top = 419
    Width = 95
    Height = 31
    Hint = ''
    Caption = 'Confirmar'
    ParentFont = False
    Font.Height = -16
    TabStop = False
    TabOrder = 5
    OnClick = btCambiarPassClick
  end
  object UniLabel3: TUniLabel
    Left = 63
    Top = 279
    Width = 214
    Height = 23
    Hint = ''
    Caption = 'Debe canviar el Password'
    ParentFont = False
    Font.Height = -19
    TabOrder = 6
  end
end
