unit uConsultas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn;

type
  TFormConsultas = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    DataSource1: TDataSource;
    pcConsultas: TUniPageControl;
    tabTiemposTrabajos: TUniTabSheet;
    UniDBNavigator2: TUniDBNavigator;
    gridSumTiempo: TUniDBGrid;
    tabTiemposClientes: TUniTabSheet;
    UniDBNavigator3: TUniDBNavigator;
    gridTiempoCliente: TUniDBGrid;
    tabTodosTiempos: TUniTabSheet;
    UniDBNavigator4: TUniDBNavigator;
    gridTodosTiempos: TUniDBGrid;
    UniPanel2: TUniPanel;
    tmDesdeTiempoTrabajos: TUniDateTimePicker;
    tmHastaTiempoTrabajos: TUniDateTimePicker;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    btFiltrarTiemposTrabajos: TUniButton;
    cbTodasFechas: TUniCheckBox;

    procedure btFiltrarTiemposTrabajosClick(Sender: TObject);
    procedure cbTodasFechasClick(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormConsultas: TFormConsultas;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormConsultas: TFormConsultas;
begin
  Result := TFormConsultas(DMppal.GetFormInstance(TFormConsultas));

end;

procedure TFormConsultas.UniFormShow(Sender: TObject);
begin
  with gridTodosTiempos do
      begin
        Columns[0].Width := 70;
        Columns[1].Width := 70;
        Columns[2].Width := 70;
        Columns[3].Width := 160;
        Columns[4].Width := 320;
        Columns[5].Width := 320;
        Columns[6].Width := 100;
        Columns[7].Width := 70;
        Columns[8].Width := 70;

      end;

    with gridSumTiempo do
      begin
        Columns[0].Width := 70;
        Columns[1].Width := 70;
        Columns[2].Width := 320;
      end;

    tmHastaTiempoTrabajos.DateTime := now;
end;

{
procedure TFormTrabajos.pcTrabajosChange(Sender: TObject);
begin
//ARREGLAR EN PROGRAMA PRINCIPAL
//en Trabajos pendientes  ->  Mostrar los que no tiene hora fin
 { if pcTrabajos.ActivePage = tabConsultas then
  begin
    pcConsultas.ActivePage := tabTiemposTrabajos;
    DMppal.RutAbrirConsultas;

    with gridTodosTiempos do
      begin
        Columns[0].Width := 70;
        Columns[1].Width := 70;
        Columns[2].Width := 70;
        Columns[3].Width := 160;
        Columns[4].Width := 320;
        Columns[5].Width := 320;
        Columns[6].Width := 100;
        Columns[7].Width := 70;
        Columns[8].Width := 70;

      end;

    with gridSumTiempo do
      begin
        Columns[0].Width := 70;
        Columns[1].Width := 70;
        Columns[2].Width := 320;
      end;
  end;


//si selecciono uno pendiente que me diga para continuar etc (botones)

end;
}


//inicio control paradas



procedure TFormConsultas.btFiltrarTiemposTrabajosClick(Sender: TObject);
begin
//rutfiltrofechaconsultatiempos por trabajo

end;

procedure TFormConsultas.cbTodasFechasClick(Sender: TObject);
begin


  //if cbTodasFechas.Checked then DMppal.RutAbrirConsultas;
  //gridSumTiempo.Refresh;
  //gridTodosTiempos.Refresh;

end;




end.
