unit uClienteFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniProgressBar, uniDBCheckBox,
  uniImageList;

type
  TFormClienteFicha = class(TUniForm)
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    pCodi: TUniDBEdit;
    UniLabel2: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniLabel3: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniDBEdit7: TUniDBEdit;
    UniLabel8: TUniLabel;
    UniDBEdit8: TUniDBEdit;
    UniLabel9: TUniLabel;
    UniDBEdit9: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniDBEdit10: TUniDBEdit;
    UniLabel5: TUniLabel;
    UniDBEdit11: TUniDBEdit;
    UniLabel10: TUniLabel;
    UniDBEdit12: TUniDBEdit;
    UniLabel11: TUniLabel;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBMemo1: TUniDBMemo;
    UniLabel13: TUniLabel;
    UniDBEdit15: TUniDBEdit;
    UniLabel14: TUniLabel;
    UniGroupBox1: TUniGroupBox;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniLabel12: TUniLabel;
    UniDBEdit17: TUniDBEdit;
    UniDBEdit19: TUniDBEdit;
    UniLabel15: TUniLabel;
    UniDBEdit20: TUniDBEdit;
    UniLabel16: TUniLabel;
    UniDBEdit21: TUniDBEdit;
    UniDBEdit22: TUniDBEdit;
    UniLabel17: TUniLabel;
    UniDBEdit23: TUniDBEdit;
    UniDBEdit24: TUniDBEdit;
    UniLabel18: TUniLabel;
    UniDBEdit25: TUniDBEdit;
    UniDBEdit26: TUniDBEdit;
    UniLabel19: TUniLabel;
    UniDBEdit27: TUniDBEdit;
    UniLabel20: TUniLabel;
    UniLabel21: TUniLabel;
    UniLabel22: TUniLabel;
    UniDBEdit28: TUniDBEdit;
    btValidarDC: TUniButton;
    lbResuValidacion: TUniLabel;
    PanelCambiarCodigo: TUniPanel;
    edCodigoAntiguo: TUniEdit;
    edCodigoNuevo: TUniEdit;
    ProgressBar1: TUniProgressBar;
    rgTipoCambioCodigo: TUniRadioGroup;
    UniLabel23: TUniLabel;
    btEjecutarCambiarCodigo: TUniBitBtn;
    dsCabe: TDataSource;
    UniDBEdit29: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    dsPoblacionS: TDataSource;
    UniDBEdit6: TUniDBEdit;
    UniDBRadioGroup2: TUniDBRadioGroup;
    cbFijarPantalla: TUniCheckBox;
    BtnAnterior: TUniBitBtn;
    BtnSiguiente: TUniBitBtn;
    UniDBLookupComboBox2: TUniDBLookupComboBox;
    UniDBLookupComboBox3: TUniDBLookupComboBox;
    dsTFacturacio: TDataSource;
    UniDBCheckBox1: TUniDBCheckBox;
    dsTPago: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    BtnEditar: TUniBitBtn;
    UniBitBtn30: TUniBitBtn;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    BtnGrabar: TUniBitBtn;
    BtnCancelar: TUniBitBtn;
    btOpciones: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    btReserva: TUniBitBtn;
    btLista: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    btBuscar: TUniBitBtn;
    btCambiarCodigo: TUniButton;
    procedure btCambiarCodigoClick(Sender: TObject);
    procedure btEjecutarCambiarCodigoClick(Sender: TObject);
    procedure btListaClick(Sender: TObject);
    procedure btReservaClick(Sender: TObject);
    procedure BtnEditarClick(Sender: TObject);
    procedure BtnGrabarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormClienteFicha: TFormClienteFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uDMMenuArti, uCliente, uDMCliente, uMenu;

function FormClienteFicha: TFormClienteFicha;
begin
  Result := TFormClienteFicha(DMppal.GetFormInstance(TFormClienteFicha));

end;


procedure TFormClienteFicha.btCambiarCodigoClick(Sender: TObject);
begin
  inherited;
  if PanelCambiarCodigo.Visible then
  begin
    PanelCambiarCodigo.Visible := False;
    exit;
  end;
  edCodigoAntiguo.Text := DMCliente.sqlCabeID_CLIENTE.AsString;

  with PanelCambiarCodigo do
  begin
    Top := btCambiarCodigo.Top;
    Left:= btCambiarCodigo.Left;// + btCambiarCodigo.Width;
    Visible := True;
    BringToFront;
    btEjecutarCambiarCodigo.Enabled := True;
    edCodigoNuevo.Text := '';
    edCodigoNuevo.SetFocus;
  end;
end;

procedure TFormClienteFicha.btEjecutarCambiarCodigoClick(Sender: TObject);
begin
  PanelCambiarCodigo.Visible := False;
end;

procedure TFormClienteFicha.btListaClick(Sender: TObject);
begin
  FormCliente.RutAbrirListaClie;
end;

procedure TFormClienteFicha.BtnCancelarClick(Sender: TObject);
begin
  UniPanel27.Visible := not unipanel27.visible;
  UniPanel24.Enabled := not UniPanel24.Enabled;
end;

procedure TFormClienteFicha.BtnEditarClick(Sender: TObject);
begin
  UniPanel27.Visible := not unipanel27.visible;
  UniPanel24.Enabled := not UniPanel24.Enabled;
end;

procedure TFormClienteFicha.BtnGrabarClick(Sender: TObject);
begin
  UniPanel27.Visible := not unipanel27.visible;
  UniPanel24.Enabled := not UniPanel24.Enabled;
end;

procedure TFormClienteFicha.btReservaClick(Sender: TObject);
begin
  FormCliente.RutAbrirReservaClie;
end;

end.

