unit uMensaje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, Data.DB, uniBasicGrid, uniDBGrid,
  uniPageControl, uniGUIBaseClasses, uniPanel, uniButton,

  Dialogs, Vcl.ExtCtrls,

  uniTreeView, uniImage, uniSplitter, uniMultiItem, uniComboBox,
  uniToolBar, uniImageList, uniStrUtils, uniBitBtn, uniDBComboBox,
  uniDBLookupComboBox, uniEdit, uniDBText, uniLabel, uniCheckBox, uniChart,
  uniDBEdit, uniMemo,
  uniDateTimePicker, uniListBox,// Uni,
  uniGUIFrame,
  uniDBListBox, uniDBLookupListBox, uniDBCheckBox, uniDBMemo, Vcl.Imaging.jpeg,
  uniScrollBox, Vcl.Menus, uniMainMenu, uniRadioGroup, uniDBRadioGroup,
  uniDBDateTimePicker, uniDBNavigator, uniURLFrame, uniHTMLFrame,   // DBAccess,
  uniGroupBox, uniTimer,
  Shellapi, uniFileUpload, uniSpeedButton;

type
  TFormMensaje = class(TUniForm)
    UniSplitter1: TUniSplitter;
    gridMensajes: TUniDBGrid;
    pnMensaje: TUniPanel;
    btEnviarMensaje: TUniBitBtn;
    dsMensajes: TDataSource;
    UniLabel1: TUniLabel;
    dtFecha: TUniDateTimePicker;
    btCerrar: TUniBitBtn;
    edMensaje: TUniMemo;
    btNuevoMensaje: TUniBitBtn;
    procedure btEnviarMensajeClick(Sender: TObject);
    procedure btCerrarClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure dtFechaChange(Sender: TObject);
    procedure gridMensajesTitleClick(Column: TUniDBGridColumn);
    procedure btNuevoMensajeClick(Sender: TObject);



  private





    { Private declarations }
  public
    { Public declarations }

    FUrlDocumentacion :String;




  end;

function FormMensaje: TFormMensaje;




var
  vParada : Boolean;
  vMensajeTecnico : String;


implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule
  ,uMenu;

function FormMensaje: TFormMensaje;
begin
  Result := TFormMensaje(DMppal.GetFormInstance(TFormMensaje));
end;




procedure TFormMensaje.btCerrarClick(Sender: TObject);
begin
  self.Close;
end;

procedure TFormMensaje.btEnviarMensajeClick(Sender: TObject);
begin
  pnMensaje.Height := 0;

  if edMensaje.Text <> '' then DMppal.RutEnviarMensaje(edMensaje.Text);
  FormMenu.tmMensajeTimer(nil);
end;

procedure TFormMensaje.btNuevoMensajeClick(Sender: TObject);
begin
  if pnMensaje.Height = 0 then pnMensaje.Height := 90
  else pnMensaje.Height := 0;
end;

procedure TFormMensaje.dtFechaChange(Sender: TObject);
begin
  DMppal.RutAbrirMensajes(dtFecha.DateTime);
end;

procedure TFormMensaje.gridMensajesTitleClick(Column: TUniDBGridColumn);
begin

 if Column.FieldName = 'MENSAJE' then
 begin
   pnMensaje.Height := 90;
 end;
end;

procedure TFormMensaje.UniFormCreate(Sender: TObject);
begin
  edMensaje.Text := '';
  pnMensaje.Height := 0;

  dtFecha.DateTime := now;
  DMppal.RutAbrirMensajes(dtFecha.DateTime);

  gridMensajes.Columns[1].Title.Caption := 'Mensajes del ' + DMppal.sqlMensajesGeneralFECHA.AsString;
end;

initialization
  RegisterAppFormClass(TFormMensaje);

end.
