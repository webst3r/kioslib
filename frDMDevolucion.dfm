object frDMDevolucion: TfrDMDevolucion
  OldCreateOrder = False
  Height = 401
  Width = 711
  object frxPDFExport1: TfrxPDFExport
    FileName = 'C:\Users\Toni\Desktop\1111.pdf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 43130.701088159720000000
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 189
    Top = 62
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = dsUR
    BCDToCurrency = False
    Left = 285
    Top = 64
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 392
    Top = 64
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43127.731099664400000000
    ReportOptions.LastChange = 43242.791833993100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '//   picture2.loadfromfile('#39'C:\Users\Toni\Documents\Embarcadero\' +
        'Studio\Projects\Sogemedi\Win32\Debug\files\UR\FOTOS\1\UniImage2.' +
        'jpg'#39');'
      
        '//   picture3.loadfromfile('#39'C:\Users\Toni\Documents\Embarcadero\' +
        'Studio\Projects\Sogemedi\Win32\Debug\files\UR\FOTOS\1\UniImage3.' +
        'jpg'#39');'
      'end;'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'begin'
      ''
      ''
      'end.')
    OnPreview = frxReport1Preview
    Left = 96
    Top = 64
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 211.653680000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        Stretched = True
        object Memo7: TfrxMemoView
          Left = 9.338590000000000000
          Top = 71.811070000000000000
          Width = 695.433070870000000000
          Height = 139.842610000000000000
          StretchMode = smMaxHeight
          AllowHTMLTags = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'Con esta fecha, hago entrega de las llaves de la finca registral' +
              ' [frxDBDataset1."NUM_FINCA_REGISTRAL"] de  [frxDBDataset1."POBLA' +
              'CION_INMUEBLE"] Inscrita en el Registro de la Propiedad Numero [' +
              'frxDBDataset1."NUM_REGISTRO"] de [frxDBDataset1."POBLACION_REGIS' +
              'TRO"], sita en, [frxDBDataset1."DIRECCION_INMUEBLE"] a  [frxDBDa' +
              'taset1."NOMBRESOCIEDAD"], dej'#225'ndola libre, vacua y expedita, dan' +
              'do desde este momento posesi'#243'n a dicha Entidad. Dicha finca ha s' +
              'ido adjudicada en virtud del procedimiento hipotecario, seguido ' +
              'ante el Juzgado de Primera Instancia Numero [frxDBDataset1."NOMB' +
              'RE_JUZGADO"] de [frxDBDataset1."POBLACION_JUZGADO"], Autos n'#186' [f' +
              'rxDBDataset1."NUM_AUTOS"], sirviendo este acto de entrega formal' +
              ' de la posesi'#243'n de la misma.')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo4: TfrxMemoView
          Left = 9.338590000000000000
          Top = 7.559060000000000000
          Width = 695.433070870000000000
          Height = 18.897650000000000000
          StretchMode = smActualHeight
          AutoWidth = True
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'UR: [frxDBDataset1."UR"] ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 9.338590000000000000
          Top = 30.236240000000000000
          Width = 695.433070870000000000
          Height = 18.897650000000000000
          StretchMode = smActualHeight
          AutoWidth = True
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'EN [frxDBDataset1."POBLACION_INMUEBLE"] a [frxDBDataset1."FECHA_' +
              'INFORME"]')
          ParentFont = False
          Formats = <
            item
            end
            item
              FormatStr = 'dd "de" mmmm "de" yyyy'
              Kind = fkDateTime
            end>
        end
      end
      object DetailData5: TfrxDetailData
        FillType = ftBrush
        Height = 172.850310700000000000
        Top = 525.354670000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object Memo175: TfrxMemoView
          Left = 9.448752990000000000
          Top = 8.440940000000000000
          Width = 325.039580000000000000
          Height = 34.015770000000000000
          AutoWidth = True
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Don/do'#241'a: [frxDBDataset1."NOMBRE_DEUDOR"]'
            'DNI: [frxDBDataset1."NIF_DEUDOR"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 251.008040000000000000
        Top = 253.228510000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        Stretched = True
        object Memo5: TfrxMemoView
          Left = 9.338590000000000000
          Top = 1.559060000000000000
          Width = 695.433070870000000000
          Height = 245.669450000000000000
          StretchMode = smMaxHeight
          AllowHTMLTags = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              '[frxDBDataset1."NOMBRE_DEUDOR"], manifiesto que en esta fecha he' +
              ' retirado cuantos enseres, mobiliario y dem'#225's elementos de mi pr' +
              'opiedad hab'#237'a en la referida finca, considerando abandonados tod' +
              'os los que, en su caso, pudieren encontrarse en su interior y au' +
              'torizando a [frxDBDataset1."NOMBRESOCIEDAD"] a hacer con ellos l' +
              'o que estime conveniente.'
            ''
            
              'Asimismo manifiesto bajo mi responsabilidad que sobre dicha finc' +
              'a no existe vigente ning'#250'n derecho de ocupaci'#243'n, quedando a la m' +
              'isma, libre a disposici'#243'n de [frxDBDataset1."NOMBRESOCIEDAD"].'
            ''
            
              'Del mismo modo autorizo a [frxDBDataset1."NOMBRESOCIEDAD"], al d' +
              'escerrajamiento de la puerta para su acceso al interior al no te' +
              'ner en mi disposici'#243'n juego de llaves originales que facilitar'#237'a' +
              'n el mismo'
            ''
            
              'Asimismo, informamos que esta visita est'#225' '#250'nica y exclusivamente' +
              ' relacionada a la entrega voluntaria de las llaves de la propied' +
              'ad anteriormente indicada. Sin que ello conlleve la condonaci'#243'n ' +
              'de ninguna otra obligaci'#243'n que tenga pendiente de pago con La Ca' +
              'ixa o cualquier otra entidad / organismo.')
          ParentFont = False
        end
      end
    end
  end
  object dsUR: TDataSource
    Left = 191
    Top = 144
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 288
    Top = 144
  end
end
