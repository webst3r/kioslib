unit uMenuDevolucion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormMenuDevolucion = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaDevolucion: TUniTabSheet;
    tabAlbaranDevolucion: TUniTabSheet;
    pnlBotonera: TUniPanel;
    pnlBtsPesta�as: TUniPanel;
    btListaDevolucion: TUniBitBtn;
    btAlbaranDevolucion: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    pnlBotonesGeneral: TUniPanel;
    btVolver: TUniBitBtn;
    btBuscar: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    btSalir: TUniBitBtn;

    procedure btListaDevolucionClick(Sender: TObject);

    procedure btAlbaranDevolucionClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniButton1Click(Sender: TObject);
    procedure UniButton2Click(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swDevoLista, swDevoAlbaran : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormMenuDevolucion: TFormMenuDevolucion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolucionLista, uDevolucionAlbaran;

function FormMenuDevolucion: TFormMenuDevolucion;
begin
  Result := TFormMenuDevolucion(DMppal.GetFormInstance(TFormMenuDevolucion));

end;

procedure TFormMenuDevolucion.btAlbaranDevolucionClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabAlbaranDevolucion;
  if pcDetalle.ActivePage = tabAlbaranDevolucion then
  begin
    if swDevoAlbaran = 0 then
    begin
      FormDevolucionAlbaran.Parent := tabAlbaranDevolucion;
      FormDevolucionAlbaran.Show();
      swDevoAlbaran := 1;
    end;
  end;

end;

procedure TFormMenuDevolucion.btListaDevolucionClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaDevolucion;
  if pcDetalle.ActivePage = tabListaDevolucion then
  begin
    if swDevoLista = 0 then
    begin
      FormDevolucionLista.Parent := tabListaDevolucion;
      FormDevolucionLista.Show();
      swDevoLista := 1;
    end;
  end;
end;






procedure TFormMenuDevolucion.UniButton1Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabAlbaranDevolucion;

  FormDevolucionAlbaran.Parent := tabAlbaranDevolucion;
  FormDevolucionAlbaran.Align := alClient;
  FormDevolucionAlbaran.Show();
  DMppal.RutInicioForm;
end;

procedure TFormMenuDevolucion.UniButton2Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaDevolucion;
end;

procedure TFormMenuDevolucion.UniFormCreate(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaDevolucion;

  FormDevolucionLista.Parent := tabListaDevolucion;
  FormDevolucionLista.Align  := alClient;
  FormDevolucionLista.Show();
  DMppal.RutInicioForm;
end;

end.
