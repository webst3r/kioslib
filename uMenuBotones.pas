unit uMenuBotones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, Data.DB, uniBasicGrid, uniDBGrid,
  uniPageControl, uniGUIBaseClasses, uniPanel, uniButton,

  Dialogs, Vcl.ExtCtrls,

  uniTreeView, uniImage, uniSplitter, uniMultiItem, uniComboBox,
  uniToolBar, uniImageList, uniStrUtils, uniBitBtn, uniDBComboBox,
  uniDBLookupComboBox, uniEdit, uniDBText, uniLabel, uniCheckBox, uniChart,
  uniDBEdit, uniMemo,
  uniDateTimePicker, uniListBox,// Uni,
  uniGUIFrame,
  uniDBListBox, uniDBLookupListBox, uniDBCheckBox, uniDBMemo, Vcl.Imaging.jpeg,
  uniScrollBox, Vcl.Menus, uniMainMenu, uniRadioGroup, uniDBRadioGroup,
  uniDBDateTimePicker, uniDBNavigator, uniURLFrame, uniHTMLFrame,   // DBAccess,
  uniGroupBox, uniTimer,
  Shellapi, uniFileUpload, uniSpeedButton;

type
  TFormBotones = class(TUniForm)
    UniSplitter1: TUniSplitter;
    pnlBts2: TUniPanel;
    btArticulos: TUniSpeedButton;
    btAgregarUsuario: TUniSpeedButton;
    btCerrarMenu: TUniSpeedButton;
    btRecepcion: TUniSpeedButton;
    btDevolucion: TUniSpeedButton;
    pcBts: TUniPageControl;
    tabBtDevolucion: TUniTabSheet;
    tabBtRecepcion: TUniTabSheet;
    pnlMensaje: TUniPanel;
    lbMensaje: TUniLabel;
    btNuevo: TUniBitBtn;
    btBorrarCS: TUniBitBtn;
    lbCrearDevol: TUniLabel;
    lbBorrarDevol: TUniLabel;
    btModificar: TUniBitBtn;
    lbModifcarDevol: TUniLabel;
    tabPruebasCreacionBTsEjecucion: TUniTabSheet;
    sbContainer: TUniScrollBox;
    tabListaAbiertos: TUniTabSheet;
    UniLabel4: TUniLabel;
    btCerrarDevolFicha: TUniButton;
    btIrDevolFicha: TUniButton;
    lbDevolFichaCerrar: TUniLabel;
    lbIrDevolFicha: TUniLabel;
    btIrRecepcionFicha: TUniButton;
    lbIrRecepcionFichar: TUniLabel;
    lbRecepcionFichaCerrar: TUniLabel;
    btCerrarRecepcionFicha: TUniButton;
    btCerrarAPP: TUniButton;
    btSalir: TUniLabel;
    UniHiddenPanel1: TUniHiddenPanel;
    UniSpeedButton2: TUniSpeedButton;
    UniSpeedButton3: TUniSpeedButton;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton9: TUniSpeedButton;
    UniSpeedButton10: TUniSpeedButton;
    UniSpeedButton11: TUniSpeedButton;
    UniSpeedButton12: TUniSpeedButton;
    UniSpeedButton15: TUniSpeedButton;
    UniSpeedButton16: TUniSpeedButton;
    UniSpeedButton17: TUniSpeedButton;
    UniSpeedButton18: TUniSpeedButton;
    UniSpeedButton19: TUniSpeedButton;
    UniSpeedButton20: TUniSpeedButton;
    UniSpeedButton21: TUniSpeedButton;
    UniSpeedButton22: TUniSpeedButton;
    UniSpeedButton24: TUniSpeedButton;
    UniSpeedButton13: TUniSpeedButton;
    btMostrarBotones: TUniSpeedButton;
    btAbrirDevol: TUniBitBtn;
    lbAbrirDevol: TUniLabel;
    btCerrarDevol: TUniBitBtn;
    lbCerrarDevol: TUniLabel;
    btImprimir: TUniBitBtn;
    lbImprimir: TUniLabel;
    lbArticulos: TUniLabel;
    lbUsuarios: TUniLabel;
    lbDevolucion: TUniLabel;
    lbRecepcion: TUniLabel;
    tabBtClientes: TUniTabSheet;
    tabBtArticulos: TUniTabSheet;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniBitBtn3: TUniBitBtn;
    UniLabel3: TUniLabel;
    UniBitBtn4: TUniBitBtn;
    UniLabel5: TUniLabel;
    UniBitBtn5: TUniBitBtn;
    UniLabel6: TUniLabel;
    UniBitBtn6: TUniBitBtn;
    UniLabel7: TUniLabel;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniBitBtn9: TUniBitBtn;
    UniLabel10: TUniLabel;
    UniBitBtn10: TUniBitBtn;
    UniLabel11: TUniLabel;
    UniBitBtn11: TUniBitBtn;
    UniLabel12: TUniLabel;
    UniBitBtn12: TUniBitBtn;
    UniLabel13: TUniLabel;
    lbDistribuidores: TUniLabel;
    tabBtDistribuidores: TUniTabSheet;
    btDistribuidores: TUniSpeedButton;
    btNuevaDistribuidora: TUniBitBtn;
    btBorrarDistribuidora: TUniBitBtn;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    btModificarDistribuidora: TUniBitBtn;
    UniLabel16: TUniLabel;
    UniBitBtn16: TUniBitBtn;
    UniLabel17: TUniLabel;
    UniBitBtn17: TUniBitBtn;
    UniLabel18: TUniLabel;
    UniBitBtn18: TUniBitBtn;
    UniLabel19: TUniLabel;
    btClientes: TUniSpeedButton;
    UniSpeedButton8: TUniSpeedButton;
    lbClientes: TUniLabel;
    lbTPV: TUniLabel;
    btConsArti: TUniSpeedButton;
    lbConsArti: TUniLabel;
    btCuadre: TUniSpeedButton;
    btHistorico: TUniSpeedButton;
    lbHistorico: TUniLabel;
    UniLabel20: TUniLabel;
    btVerFichaArti: TUniBitBtn;
    UniLabel21: TUniLabel;
    btVerUltimasCompras: TUniBitBtn;
    UniLabel22: TUniLabel;
    btVerPaquetes: TUniBitBtn;
    UniLabel23: TUniLabel;
    btModificarDescripcionCodi: TUniBitBtn;
    UniLabel24: TUniLabel;
    procedure btCerrarMenuClick(Sender: TObject);
    procedure btClientesClick(Sender: TObject);
    procedure btArticulosClick(Sender: TObject);
    procedure UniSpeedButton8Click(Sender: TObject);
    procedure btRecepcionClick(Sender: TObject);
    procedure btDevolucionClick(Sender: TObject);
    procedure btNuevoClick(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure btBorrarCSClick(Sender: TObject);
    procedure btModificarClick(Sender: TObject);
    procedure UniSpeedButton13Click(Sender: TObject);
    procedure btMostrarBotonesClick(Sender: TObject);
    procedure btCerrarAPPClick(Sender: TObject);
    procedure btIrDevolFichaClick(Sender: TObject);
    procedure btCerrarDevolFichaClick(Sender: TObject);
    procedure btAgregarUsuarioClick(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure btAbrirDevolClick(Sender: TObject);
    procedure btCerrarDevolClick(Sender: TObject);
    procedure btDistribuidoresClick(Sender: TObject);
    procedure btNuevaDistribuidoraClick(Sender: TObject);
    procedure btBorrarDistribuidoraClick(Sender: TObject);
    procedure btModificarDistribuidoraClick(Sender: TObject);
    procedure btConsArtiClick(Sender: TObject);
    procedure btHistoricoClick(Sender: TObject);
    procedure btCuadreClick(Sender: TObject);
    procedure btVerFichaArtiClick(Sender: TObject);
    procedure btVerUltimasComprasClick(Sender: TObject);
    procedure btVerPaquetesClick(Sender: TObject);
    procedure btModificarDescripcionCodiClick(Sender: TObject);



  private
    procedure RutArray;
    procedure ClickBt(Sender: TObject);
    procedure RutCrearBototnes;
    procedure RutHabilitarBoton;
    Function  RutConsultaSW(swForm : Integer) : Boolean;






    { Private declarations }
  public
    { Public declarations }



    vResult : String;

    procedure DCallBack(Sender: TComponent; Res: Integer);
    procedure RutMensajes;


  end;

function FormBotones: TFormBotones;




var
  vParada : Boolean;
  vMensajeTecnico : String;
  aBotones: array of TUniButton;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule
  ,uNuevoUsuario, uManteTPV, uMenu
  ,uDMMenuArti, uArticulos, uMenuArti, uListaArti
  ,uDMRecepcion, uMenuRecepcion, uRecepcionLista, uRecepcionAlbaran
  ,uDMCliente, uCliente
  ,uDMDevol, uDevolMenu, uDevolLista, uDevolFicha, uDevolNuevo
  ,uDMDistribuidora, uDistribuidoraMenu, uDistribuidoraFicha, uDistribuidoraLista
  ,uMensajes, uConsArti
  ,uDMHistoArti, uHistoArti
  ,uDMReclamaciones, uReclamaciones
  ,uDMCuadre, uCuadreMenu, uCuadreLista, uCuadreFicha;


function FormBotones: TFormBotones;
begin
  Result := TFormBotones(DMppal.GetFormInstance(TFormBotones));
end;



//--------------------------------------
//PRUEBA DE CREACION DE LISTA DE BOTONES
//--------------------------------------
procedure TFormBotones.UniSpeedButton13Click(Sender: TObject);
begin
//  RutCrearBototnes;
end;

procedure TFormBotones.RutCrearBototnes;
var
  vHeight, vWidth, vTop, vLeft: Integer;
  i : Integer;
begin
  pcBts.ActivePage := tabPruebasCreacionBTsEjecucion;
  vHeight := 42;
  vWidth  := 55;
  vTop    := 0;
  vLeft   := 10;

  SetLength(aBotones,10);

  for i := Low(aBotones) to High(aBotones) do
  begin
    vTop := (vHeight * i) + 4;
    aBotones[i]         := TUniButton.Create(sbContainer);
    aBotones[i].Name    := 'bt' + IntTostr(i);
    aBotones[i].Caption := aBotones[i].Name;
    aBotones[i].Width   := vWidth;
    aBotones[i].Height  := vHeight;
    aBotones[i].Top     := vTop;
    aBotones[i].Left    := vLeft;
    aBotones[i].OnClick := ClickBt;
    aBotones[i].Parent  := sbContainer;

  end;
  //aBotones.SetOnClick(ClickBt);
end;

procedure TFormBotones.ClickBt(Sender: TObject);
begin
  ShowMessage((TUniButton(sender)).Caption);
end;





procedure TFormBotones.RutArray;
var
  Array_D : array of Integer;
  i : Integer;
begin//prueba de funcionamiento de array
  i := 0;
  lbMensaje.Visible := True;
  lbMensaje.Caption := '';

  SetLength(Array_D, 10);

  for i := Low(Array_D) to High(Array_D) do
  begin
    Array_D[i] := i;
    lbMensaje.Caption := lbMensaje.Caption + IntToStr(Array_D[i])
  end;

end;


//--------------------------------------
//FIN PRUEBA DE CREACION DE LISTA DE BOTONES
//--------------------------------------


procedure TFormBotones.UniFormShow(Sender: TObject);
begin
  if DMppal.vAdmin then
  begin
    btAgregarUsuario.Visible := True;
    lbUsuarios.Visible       := True;
  end
  else
  begin
    btAgregarUsuario.Visible := False;
    lbUsuarios.Visible       := False;
  end;


  //si la ventana devolFicha esta abierta swCerrar APP = True y ense�a una ventana paraa ir a la ficha o cerrarla
 if FormMenu.swCerrarAPP = 1 then btMostrarBotonesClick(nil)
 else
 begin
   if FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion then
   begin
    if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion then
    begin
      pcBts.ActivePage    := tabBtDevolucion;
      btImprimir.Visible  := True;
      lbImprimir.Visible  := True;
//      self.Height := 375;
      self.Height := 450;

      btHistorico.Visible := True;
      lbHistorico.Visible := True;
    end;
    //if (FormMenu.swDevolucion = 1) and (FormDevolucion.swDevoAlbaran = 1)
    //else self.Height := 95; //reducimos height de la ventana

    if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then
    begin
      pcBts.ActivePage := tabBtDevolucion;
      btImprimir.Visible  := False;
      lbImprimir.Visible  := False;
      self.Height := 190; //indicar el alto del form
    end;

    if DMDevolucion.sqlCabe1SWDEVOLUCION.AsInteger = 3 then
    begin
      btAbrirDevol.Enabled := True;
      lbAbrirDevol.Enabled := True;

      btCerrarDevol.Enabled := False;
      lbCerrarDevol.Enabled := False;
    end;

    if DMDevolucion.sqlCabe1SWDEVOLUCION.AsInteger = 1 then
    begin
      btAbrirDevol.Enabled := False;
      lbAbrirDevol.Enabled := False;

      btCerrarDevol.Enabled := True;
      lbCerrarDevol.Enabled := True;
    end;

   end
   else if FormMenu.pcDetalle.ActivePage = FormMenu.tabRecepcion then
   begin
     pcBts.ActivePage := tabBtRecepcion;
     self.Height := 190; //temporal
   end
   else if FormMenu.pcDetalle.ActivePage = FormMenu.tabCliente then
   begin
     pcBts.ActivePage := tabBtClientes;
     self.Height := 190; //temporal
   end
   else if FormMenu.pcDetalle.ActivePage = FormMenu.tabDistribuidora then
   begin
     if (FormDistribuidoraMenu.pcDetalle.ActivePage = FormDistribuidoraMenu.tabListaDistribuidora)
        or (FormDistribuidoraMenu.pcDetalle.ActivePage = FormDistribuidoraMenu.tabFichaDistribuidora) then
     begin
       pcBts.ActivePage    := tabBtDistribuidores;

       self.Height := 375;
     end;

   end else self.Height := 190; //reducimos height de la ventana;

   if (FormMenu.pcDetalle.ActivePage = FormMenu.tabArticulos)
        or ((FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion) and (FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion))
        or (FormMenu.pcDetalle.ActivePage = FormMenu.tabConsArti) then
   begin
      //solo para habilitar bt ahistorico
     btHistorico.Visible := True;
     lbHistorico.Visible := True;


   end
 end;


end;

procedure TFormBotones.DCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes : begin

              if vResult = '22' then
              begin
                //lbMensaje.Caption := 'No se puede borrar, ya que contiene paquetes';
              end;

            end;
    mrNo  : vResult := 'mbNo';
  end;
end;


procedure TFormBotones.RutMensajes;
begin

  FormMensajes.RutInicioForm(1, UpperCase(self.Name), 'ACEPTAR', 'No se puede borrar la devoluci�n, ya que contiene paquetes');
  FormMensajes.ShowModal();

end;

procedure TFormBotones.btNuevoClick(Sender: TObject);
begin
//la botonera aparece solo cuando estas en fichaDevol por lo tanto no hace falta abrir cabe1
//llama directamente a la rutina que hace el append
//  FormDevolFicha.btNuevoClick(nil);
  FormDevolLista.RutNuevaDevolucion(0);

  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btAgregarUsuarioClick(Sender: TObject);
begin
  FormNuevoUsuario.ShowModal();
end;


procedure TFormBotones.btBorrarCSClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount = 0 then
  begin
    FormMensajes.RutInicioForm(2, UpperCase(self.Name), 'ACEPTARCANC', '�Estas seguro de eliminar este registro completamente?');
    FormMensajes.ShowModal();
  end
  else
  begin
    FormMensajes.RutInicioForm(3, UpperCase(self.Name), 'ACEPTAR', 'No puede borrar esta devoluci�n ya que contiene paquetes');
    FormMensajes.ShowModal();
  end;

//  btCerrarMenuClick(nil);
end;


procedure TFormBotones.btModificarClick(Sender: TObject);
begin
//  DMDevolucion.RutAbrirTablasNuevo;
//  DMDevolucion.RutAbrirCabe1;
  FormDevolNuevo.RutInicioForm(True,0);

  FormDevolNuevo.ShowModal();

  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btModificarDescripcionCodiClick(Sender: TObject);
begin
  FormDevolFicha.btModificarDescClick(nil);
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btVerFichaArtiClick(Sender: TObject);
begin
  FormDevolFicha.btVerFichaClick(nil);
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btVerPaquetesClick(Sender: TObject);
begin
  FormDevolFicha.btPaquetesClick(nil);
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btVerUltimasComprasClick(Sender: TObject);
begin
  FormDevolFicha.btVerUltiCompraClick(nil);
  btCerrarMenuClick(nil);
end;



procedure TFormBotones.btCerrarMenuClick(Sender: TObject);
begin
  FormMenu.lbFormNombre.Caption := FormMenu.pcDetalle.ActivePage.Caption;
  FormBotones.Close;
end;

procedure TFormBotones.btRecepcionClick(Sender: TObject);
begin
  FormMenu.RutAbrirRecepcion;
  pcBts.ActivePage := tabBtRecepcion;
  btCerrarMenuClick(nil);

end;

procedure TFormBotones.btCuadreClick(Sender: TObject);
begin
  FormMenu.RutAbrirCuadre;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btDevolucionClick(Sender: TObject);
begin
  FormMenu.RutAbrirDevolucion;
  btCerrarMenuClick(nil);

end;

procedure TFormBotones.btDistribuidoresClick(Sender: TObject);
begin
  FormMenu.RutAbrirDistribuidora;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btClientesClick(Sender: TObject);
begin
  FormMenu.RutAbrirClientes;
  btCerrarMenuClick(nil);
end;


procedure TFormBotones.btConsArtiClick(Sender: TObject);
begin
  FormMenu.RutAbrirConsArti;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btHistoricoClick(Sender: TObject);
begin
  if (FormMenu.pcDetalle.ActivePage = FormMenu.tabArticulos)
    and (FormMenuArti.pcDetalle.ActivePage = FormMenuArti.tabListaArti) then
  begin
    FormListaArti.RutAbrirHistoListaArti;
    btCerrarMenuClick(nil);
  end
  else
  if (FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion)
    and (FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion) then
  begin
    FormDevolFicha.RutHistoArtiClick;
    btCerrarMenuClick(nil);
  end;

end;

procedure TFormBotones.btArticulosClick(Sender: TObject);
begin
  FormMenu.RutAbrirArticulos;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.UniSpeedButton8Click(Sender: TObject);
begin

  FormMenu.pcDetalle.ActivePage := FormMenu.tabManteTPV;

  if FormMenu.swManteTPV = 0 then
  begin
    FormManteTPV.Parent := FormMenu.tabManteTPV;
    FormManteTPV.Align := alClient;
    FormManteTPV.Show();
    FormMenu.swManteTPV := 1;
    //DMppal.RutInicioForm;

  end;

  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabManteTPV.Caption;

end;




//------------------------------------
//RUT ABRIR LISTA DE VENTANAS ABIERTAS
//------------------------------------
procedure TFormBotones.btCerrarAPPClick(Sender: TObject);
begin
  UniApplication.Terminate('.. Gracias por utilizar este servicio ..');
end;

procedure TFormBotones.btMostrarBotonesClick(Sender: TObject);
begin
  RutHabilitarBoton;
  Self.Height := 375;

  pcBts.ActivePage := tabListaAbiertos;

end;

procedure TFormBotones.RutHabilitarBoton;
begin
  lbDevolFichaCerrar.Visible := RutConsultaSW(FormDevolMenu.swDevoAlbaran);
  lbIrDevolFicha.Visible     := RutConsultaSW(FormDevolMenu.swDevoAlbaran);
  btIrDevolFicha.Visible     := RutConsultaSW(FormDevolMenu.swDevoAlbaran);
  btCerrarDevolFicha.Visible := RutConsultaSW(FormDevolMenu.swDevoAlbaran);

  //falta hacer control de ventanas abiertas por sw
{  lbRecepcionFichaCerrar.Visible := RutConsultaSW(FormMenuRecepcion.swDevoAlbaran);
  lbIrRecepcionFichar.Visible     := RutConsultaSW(FormMenuRecepcion.swDevoAlbaran);
  btIrRecepcionFicha.Visible     := RutConsultaSW(FormMenuRecepcion.swDevoAlbaran);
  btCerrarRecepcionFicha.Visible := RutConsultaSW(FormMenuRecepcion.swDevoAlbaran);}
end;

Function TFormBotones.RutConsultaSW(swForm : Integer) : Boolean;
begin
  Result := false;
  if swForm = 1 then Result := True;
end;

procedure TFormBotones.btIrDevolFichaClick(Sender: TObject);
begin
  FormMenu.pcDetalle.ActivePage       := FormMenu.tabDevolucion;
  FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabAlbaranDevolucion;
  self.Close;
end;

//------------------------------------
//FIN RUT ABRIR LISTA DE VENTANAS ABIERTAS
//------------------------------------


procedure TFormBotones.btAbrirDevolClick(Sender: TObject);
var
  vID : Integer;
begin
 { if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then
  begin
    vID := DMDevolucion.sqlCabeSIDSTOCABE.AsInteger;
    DMDevolucion.RutAbrirCerrarDevol(1);
    FormDevolLista.RutAlbaranClick(' = ' + IntToStr(vID));
    FormDevolFicha.RutMensajeArticulo(7);
  end;
  }
  DMDevolucion.RutAbrirCerrarDevol(DMDevolucion.sqlCabe1IDSTOCABE.AsInteger ,1);
  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion then
  begin
    FormDevolFicha.RutMensajeArticulo(7);
    DMDevolucion.sqlCabe1.Refresh;
  end;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btCerrarDevolClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirCerrarDevol(DMDevolucion.sqlCabe1IDSTOCABE.AsInteger, 3);
  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion then
  begin
    FormDevolFicha.RutMensajeArticulo(6);
    DMDevolucion.sqlCabe1.Refresh;
  end;
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btImprimirClick(Sender: TObject);
begin
  FormDevolFicha.btImprimirClick(nil);
end;


procedure TFormBotones.btCerrarDevolFichaClick(Sender: TObject);
begin
  FormDevolMenu.swDevoAlbaran := 0;
  FormMenu.swCerrarAPP := 0;
  FormDevolFicha.Close;
  FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabListaDevolucion;
  FormMenu.swMenuBts := False;
  self.Close;
end;

//------------------------------------------
//DISTRIBUIDORA
//------------------------------------------

procedure TFormBotones.btNuevaDistribuidoraClick(Sender: TObject);
begin
  FormDistribuidoraFicha.btNuevaDistribuidoraClick(nil);
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btBorrarDistribuidoraClick(Sender: TObject);
begin
  FormDistribuidoraFicha.btBorrarDistribuidoraClick(nil);
  btCerrarMenuClick(nil);
end;

procedure TFormBotones.btModificarDistribuidoraClick(Sender: TObject);
begin
  FormDistribuidoraLista.RutFichaClick(DMDistribuidora.sqlCabeSID_PROVEEDOR.AsInteger);
  FormDistribuidoraFicha.btEditarDistribuidoraClick(nil);
  btCerrarMenuClick(nil);
end;




initialization
  RegisterAppFormClass(TFormBotones);

end.


