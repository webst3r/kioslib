unit frDMFicha;

interface

uses
  SysUtils, Classes, frxGradient, frxClass, frxDBSet, frxExportPDF, Data.DB,
  frxDesgn, frxCross;

type
  TfrDM = class(TDataModule)
    frxPDFExport1: TfrxPDFExport;
    frxDBDataset1: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxReport1: TfrxReport;
    dsUR: TDataSource;
    frxDesigner1: TfrxDesigner;
    procedure frxReport1Preview(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    vNumInfo : String;
    function GenReportPDF(const InvNum: string): string;
    function ModiReportPDF(const InvNum: string): string;

    function GenExportPDF(const InvNum: string): string;

  end;

implementation

uses
  uDmppal, ServerModule;

{$R *.dfm}


{ TfrDM }


procedure TfrDM.frxReport1Preview(Sender: TObject);
var
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  Picture1 : TfrxPictureView;
begin
// Add the TfrxCrossView to it




{
   Memo := TfrxReportPage(frxpReport1.FindObject('Memo4'));

   Memo.Text  := 'AAAAAAAA';    //(['Binnen 1 week'], ['Q1 2005'], [0.6]);

  if (Sender is TfrxMemoView) then
     if (Sender as TfrxMemoView).Name = 'Memo4' then
  Begin
    TfrxMemoView(Sender).Memo.Text := 'AAAAAAAA';    //(['Binnen 1 week'], ['Q1 2005'], [0.6]);
  end;
}

//  frxReport1.Variables.Variables['picture1'].loadfromfile := 'C:\Users\Toni\Documents\Embarcadero\Studio\Projects\Sogemedi\Win32\Debug\files\UR\FOTOS\1\UniImage1.jpg';
//  frxReport1.Variables.Variables['Memo4'].text := 'AAAAAAAAAAAAAA';

//   Memo := frxReport1.FindObject('Memo4') as TfrxMemoView;
//   if Memo <> nil then
//   begin
//     Memo.Text := 'AAAAAAAAAAAAA';
//   end;

{
   Memo := frxReport1.FindObject('Memo4') as TfrxMemoView;
   if Memo <> nil then
   begin
     Memo.Text := 'AAAAAAAAAAAAA';
   end;
}

end;


{
procedure TfrDM.RutCargarPicture(vPicture:TfrxPictureView; vFile:String):
var
  FFolder : string;
  Picture1 : TfrxPictureView;
begin

   Picture1 := frxReport1.FindObject('picture1') as TfrxPictureView;
   if Picture1 <> nil then
   begin
     if FileExists(FFolder + DMppal.sqlURID.AsString + '\UniImage1.jpg') then
        Picture1.picture.loadfromfile(FFolder + DMppal.sqlURID.AsString + '\UniImage1.jpg');
   end;
end;
}

function TfrDM.GenExportPDF(const InvNum: string): string;
var
  FFolder : string;
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  vPicture : TfrxPictureView;
  Sr : TSearchRec;
  vStrList: TStringlist;
  I, J, vFotos : Integer;
  swFinal, swFoto : Boolean;

    Exp1: TfrxPDFExport;
    AUrl : string;
    P: Integer;
    OldFName, NewFName: String;

begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode := True;
    frxReport1.EngineOptions.EnableThreadSafe := True;
    frxReport1.EngineOptions.DestroyForms := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');

    frxPDFExport1.Background := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog := False;
    frxPDFExport1.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath := '';

    frxReport1.PreviewOptions.AllowEdit := False;




    frxReport1.PrepareReport;
    frxReport1.Export(frxPDFExport1);



  finally



  end;

end;

function TfrDM.GenReportPDF(const InvNum: string): string;
var
  FFolder : string;
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  vPicture : TfrxPictureView;
  vPage6, vPage6b, vPage6c : Boolean;
  Sr : TSearchRec;
  vStrList: TStringlist;
  I, J, vFotos : Integer;
  swFinal, swFoto : Boolean;
  vTipoInmueble : Integer;
  vTotalInteger : Integer;
begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode           := True;
    frxReport1.EngineOptions.EnableThreadSafe     := True;
    frxReport1.EngineOptions.DestroyForms         := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');

    frxPDFExport1.Background   := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog   := False;
    frxPDFExport1.FileName     := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath  := '';

    frxReport1.PreviewOptions.AllowEdit := False;




//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//                        Comun a todos los informes
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------

    frxReport1.PrepareReport;
    frxReport1.Export(frxPDFExport1);

//------------------------------------------------------------------------------

  finally
  end;
end;



function TfrDM.ModiReportPDF(const InvNum: string): string;
var
  FFolder : string;
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  vPicture : TfrxPictureView;
begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode := True;
    frxReport1.EngineOptions.EnableThreadSafe := True;
    frxReport1.EngineOptions.DestroyForms := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');

    frxPDFExport1.Background := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog := False;
    frxPDFExport1.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath := '';

    frxReport1.PreviewOptions.AllowEdit := False;

//VARIABLES
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

    frxReport1.DesignReport;
  finally
  end;
end;


end.
