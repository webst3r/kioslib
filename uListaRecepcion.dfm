object FormListaRecepcion: TFormListaRecepcion
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 568
  ClientWidth = 1024
  Caption = 'FormListaRecepcion'
  OnShow = UniFormShow
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1024
    Height = 57
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object UniSpeedButton1: TUniSpeedButton
      Left = 246
      Top = 6
      Width = 119
      Height = 45
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C89D9DA0CECECEFDFDFD0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B21C46B51047B3424468CECECE
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B30B3AC0118CFF39CBFF276B
        B69D9DA10000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B50C41C91089FF34BEFF5C
        F4FF396FBDC7C7CA0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2A2B60A3BCF118BFF34BEFF
        5BF2FF3573CDA1A1B5FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        ECECECC7C7C7B4B4B4B9B9B9D3D3D3F1F1F1FFFFFFB4B5C9164DD40E8AFF35BC
        FF5CF3FF3B7BD4A2A2B7FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFE9E9
        E99595956D6F6F6E6F716F70716163635354556B6B6AB5B5B5656CB98FD1EB50
        D5FA55EFFF3676D99F9FB7FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFCB
        CBCB7778799C9C9EC1BCB7D0C7BCD0C6BBC1B9B0A19D987070714446477E7E85
        EBDFD6ADF2FB3375DEA2A2BAFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        D3D3D37F8183CCC8C4EEE0CDFCEDD7FFF3DEFFF5E1FFF4DFF1E7D1CABEAD908D
        8BBBBCB897979D6971C5B4B4CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFF1F1F1868789D7D1CDFEE9D0FFF0DAFFEFDAFFEED9FFEED9FFEFDAFFF3DEFF
        F6DFDAC8B1928F8B353739B5B5B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFAFAFAFB6B5B8FDE8CFFFECD1FFEBD2FFEAD0FFEACEFFEAD0FFEAD0
        FFEAD1FFECD6FFF3DAC3B19C5E6061646463F2F2F2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFF96989AE8DDD3FFE6C5FFE9CEFFE5C6FFE3C3FFE2C1FFE2
        C1FFE3C3FFE5C7FFE7CBFFF1D8ECD6B78E867E404143D3D3D3FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFF9C9FA1FBE5D1FFE1BDFFDFBCFFDAB2FFD7ABFF
        D5A9FFD5A9FFD7ACFFDAB2FFDEBAFFE7C8FEE5C4AE9C8B444649B8B8B8FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFA2A5A8FDDFC4FFD7ACFFD2A5FFCF9B
        FFCC98FFCC98FFCC98FFCC98FFCE9BFFD2A5FFDCB4FFE1BABAA08C4A4C4FB2B2
        B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFA1A5A8FDDDC6FFCA95FFCC
        97FFCE9BFFD09FFFD2A3FFD2A3FFD0A0FFCE9AFFCB96FFD2A0FED2A4B29A874E
        5053C7C7C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF9FA2A4F2DDD1FF
        C58FFFD3A3FFD4AAFFD8B0FFD9B2FFD9B2FFD7AFFFD5ABFFD1A1FFD39EEDB885
        9A8C85525456ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFBCBDBD
        C8C7C9FFC9A5FFD5A8FFDFBCFFDFBFFFE1C2FFE1C2FFDFBEFFDBB7FFDBB3FFD4
        A0CC9B7974787A898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFF4F4F4999D9DE9D7D0FDBB94FFE1C1FFEED8FFECD6FFEBD3FFEAD1FFEBCEFF
        DEB6E2A37AA397915A5D5EE9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFDDDDDD9EA3A6E5CFC8F6B89BFBC9ADFEE3D0FFE8D6FDDEC8
        F7C0A0D89E81AE9E9A676C6EC9C9C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFDDDDDD9CA0A2C0BDBEDBB8ACE2AD9BE2A6
        91D5A190BF9F959A9B9D75797ACDCDCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFF6F6F6C2C2C3A5A9ABA1
        A7A8A0A4A6979C9E939799ACADADF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = 'Consulta Albaranes'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      TabOrder = 1
    end
    object UniSpeedButton2: TUniSpeedButton
      Left = 512
      Top = 6
      Width = 119
      Height = 45
      Hint = ''
      ShowHint = True
      Caption = 'Descargar Art'#237'culos'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 6
      TabOrder = 2
    end
    object UniSpeedButton3: TUniSpeedButton
      Left = 379
      Top = 6
      Width = 119
      Height = 45
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        DACEC39F74509E71499E724B9E724B9E724B9E724B9E734B9E734B9E724BA375
        4BA0724CAB7A4F795A632076E8585784AC784FB69277FDFDFEFFFFFF0000FFFF
        FFFFFFFF99612AEABE86F0C798EFC391EFC290EFC28EEFC18DEFC08BEFBF8AEF
        C68ABA957D566390BE8F76AD909143C6FA84A1B3E2A171526796596BCDFFFFFF
        0000FFFFFFFFFFFFB27231FFFFF1FFFFFDFFFCF3FFFBF1FFFBEFFFF8ECFFF7E9
        FFF5E6FFFFE79EA5CA6DBCF578B0E96C6ABB4AC1F16A86D05F99DE22DFFF275A
        DBFDFDFF0000FFFFFFFFFFFFAF7031FFF9E8FFF9F2FFF4E9FFF3E6FFF1E4FFF0
        E1FFF0DFFFEEDDFFF6DDF9EAD48290C279CAEE4088DB50C5F22F96E437D5FD5B
        7FA6846996FFFFFF0000FFFFFFFFFFFFAF7032FFFAE9FFF9F5FFF5EBFFF4E9FF
        F3E6FFF1E4FFF1E1FDF3E0B2B5C980A4CA7891C63670CC7AEAFF7EF7FE60FDFF
        2D9DED596CAA3F6DB05674EA0000FFFFFFFFFFFFAF7031FFFAEAFFFBF7FFF6ED
        FFF5EBFFF4E9FFF3E6FFF1E4F9F0DF6589BE7CBBE587CCED71C1EC80EAFD90FF
        FF67FCFF3CCAF329D2F91FDBFF208AE70000FFFFFFFFFFFFAF7334FFFBEDFFFC
        FAFFF7F0FFF6EDFFF5EBFFF4E9FFF2E6FFF5E5D5CFD4AFB0CA8681BA396CC675
        E0FC6EE9FD55F0FF2C9DEE6A4C9057477FA1A6F00000FFFFFFFFFFFFAF804EFF
        FFF9FFFEFCFFFAF6FFF9F3FFF7EEFFF5EBFFF4E8FFF2E6FFFCE8E7E0DA769CD0
        85D4F33562C94EC1F03177D836D0F546AACA6D5797FFFFFF0000FFFFFFFFFFFF
        AF8151FFFFFBFFFFFEFFFDF9FFFBF8FFFAF6FFF9F1FFF6ECFFF3E8FFFFEA919A
        C65BAAE37698D28B84C147C2F27E9CD58391CD1BCBFB2461E8FDFDFF0000FFFF
        FFFFFFFFAF8151FFFFFBFFFFFFFFFDFAFFFCF9FFFBF8FFFBF6FFFAF4FFF7EFFF
        F9E9DEDBDC8995C8DFDAD9B2AFD12BA8ED7EAADAFEF0DB968AA5685A95FFFFFF
        0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFDFBFFFDFBFFFCFAFFFBF8FFFBF7
        FFFAF5FFF7F1FFF8EAFFFEE9FFFCE6D8D1D94D7BD0AEB3D4FFFFEAF5C68BAA78
        50FFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFEFEFFFEFCFFFDFBFFFD
        F9FFFCF8FFFBF6FFFBF5FFF8F2FFF3E9FFF2E5FFF6E5F8ECDEFFF0DDFFF7EAEC
        BF91A2754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFFFFFFFEFDFF
        FEFCFFFDFAFFFCF9FFFBF7FFFBF7FFFAF6FFF8F3FFF5EBFFF2E5FFF5E4FFF1E0
        FFF8EDECC092A3754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFFFF
        FFFFFFFFFFFEFFFEFCFFFDFBFFFCF9FFFBF8FFFBF7FFFAF5FFF9F3FFF5ECFFF2
        E6FFF1E2FFFAEFECC293A3754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFCFFFDFAFFFCF9FFFBF8FFFBF6FFFAF5FF
        F9F4FFF6F1FFF3EAFFFCF3ECC394A3744EFFFFFF0000FFFFFFFFFFFFAF8151FF
        FFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFCFFFDFBFFFDFAFFFBF8
        FFFBF6FFFAF5FFF9F4FFF8F2FFFFFCECCFAEA37952FFFFFF0000FFFFFFFFFFFF
        AF8151FFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFEFCFFFD
        FAFFFCF9FFFBF8FFFBF6FFFAF4FFF9F2FFFFFDECD5B9A37B54FFFFFF0000FFFF
        FFFFFFFFAF8151FFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFEFFFDFCFFFDFAFFFCF9FFFBF8FFFBF6FFF9F4FFFFFFECD3B7A37A54FFFFFF
        0000FFFFFFFFFFFFB18353FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFDFCFFFCFBFFFCF9FFFFFFEED7BDA27A
        52FFFFFF0000FFFFFFFFFFFFA97B49FFFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFDFFFFFFEB
        D1B2A07954FFFFFF0000FFFFFFFFFFFFA38263B38352BA8E61B98C5DB98C5DB9
        8C5DB98C5DB98C5DB98C5DB98C5DB98C5DB98C5DB98C5CB98C5CB98B5CB98C5B
        BB8F609C6C3DDACDC3FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = 'Nuevo Albaran'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      TabOrder = 3
    end
    object UniLabel1: TUniLabel
      Left = 7
      Top = 3
      Width = 216
      Height = 24
      Hint = ''
      ShowHint = True
      Caption = 'Recepci'#243'n de art'#237'culos'
      ParentFont = False
      Font.Color = clNavy
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TabOrder = 4
    end
    object UniBitBtn13: TUniBitBtn
      Left = 847
      Top = 6
      Width = 55
      Height = 45
      Hint = 'Salir'
      ShowHint = True
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C21E0000C21E00000000000000000000D9E5F1C3D2E3
        0E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10
        120E10120E10120E10120E10120E10120E10120E10120E10120E10120E10120E
        1012B9BEEC7075C47277C77175C66F73C47074C66F73C66F72C76F72C76F71C8
        6F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71C96F71
        C97072C78A7DA30E1012CDC9E57975BA7473C36873C55C7CB65A7EB8567AB95B
        75CC5E75CC5F76C1607AB6586FD25973C85B75C35F75C3636FCE656FCB6973C7
        6D7BBC6F7AC5696CCE6B66DF6F70C50E1012C6CEE37378B66F74C36671C95D78
        BD5977BA5F7CC15E70C56A7AC4697BB36F84A6687AB8697EAE6A80A96D80AA6F
        79B8727DB96E7BAE6D81A07182AD6E78BA6B6DC96E70C80E1012BDD2E16C7AB4
        6974C4646ECE6074C46276C3687AC66A72C6BEC6EDDAE4F4DEECF1E0E8F4E0ED
        F2E0EFF1E1EEF2E0E7F4DFE9F5DBE8F3DBF1EFDAEEF2CEDDF59299D26E70C80E
        1012B4D6E0647CB36374C5616DD36270CA6670C76B71C5706EC0D3D1EFF3F4FB
        F9FEF7FFF9FFFEFDF8FDFEF6FBFEF9F8F8FEF6FAFFF1FBFCEDFEEEE9FBF3EDFA
        FC929BB96E70C80E1012B0D3E2617BB66275C46270CB6475C16675BE6D77C16E
        70C2BCBFEADBE2F1DEEAEFE5DFF3E5E3F1E4E5F0E2E4F1E1DDF4E1E0F3DEE2F3
        ECFAF7F0FCFBF2F8FE9795B96E70C80E1012B4D0E6647AB96475C16573C26378
        B0798DC39CAEE27079C76E77C26B78B56B7EA87276BE727AB56F79AF707AB272
        74BF7578B98086B6CFDCE4F0F9FEF1F3FD9F99C06E70C80E1012B8CCE66776B9
        6874BD6D7ABB7890B1A9C4DBC6E1F56879C35F6FC56376C85D76BD6171CB6176
        C3667CC46276C1626DC86B74C47781BECBDAE8F1FAFEF4F4FEA198C06E70C80E
        1012BBC7E46B72B4797EBF747EADB7CDD4E4FDFECFEAF4687BB46074C35F76C8
        5A76C25F7DBE5578AE5C81B3597CB4637BC76479BC7186B6C8E0E3F0FEFEF4F9
        FCA29EB76E70C80E1012BBCBE56E77AF7A7FB8B1B3D4ECF6F9EFFBF8EAF4F8C3
        C9E9C8D1F1C1CFEEBFD3EBC6D5ECC3D7E7C1D9E8BFD6EAA8B7E57180B87687B2
        CBDFE3F1FEFEF4F9FDA29DB96E70C80E1012B9CFDF7481B0BABFEBF3EFFFF6F6
        F9F5F6F4FBFCFAF9F6FEF5F7FDEFF5F8F0FBF4F4FAFAF4FCF7F1FCFAEEF8FFCB
        CFEC757AB07D86B3CDDCE6F1FBFEF3F5FEA199BF6E70C80E1012B6D6DE6F82A7
        B8BDE5F4EDFFFCF6FCFFFBF9FFFDF8FFF7FEFFFCF9FFFEEDFFFEDCFFFEF0FFFB
        EAFEFCF3FEF9FFDDD2ED827AB48682B8D1D8EAF2F6FFF3EFFF9F93C86E70C80E
        1012B8D2E56C7AB3767CBCB8B7E5EBF2FBF0FBFBEBF8F6C5D6E2B9D0E5B7D5E4
        B1D7E5C0CEE7C1D2E6C0D1E7C0CEE9AAAEE67576BD8586BED3D8E8F9F8FFFBF3
        FFAA97C16E72C50E1012B8CCE6717CBD6F73BF7D7BC4BEC5E7E7F1FBD8E5EF6D
        78AB6876B56477B4647EB36078B55F7BAD6481B05B76AC697BC76676BA7989BC
        C7DEE7EDFCFFF0F8FF9C9BBB6E72C50E1012BCCEE46D76BB7073C66B6AC18088
        C1B3BEE6D1DDF4737AC36F75C96E76C66572B8607BC55877B55F80B95978B65E
        75C56076BC6F86B9BAD9E1E3FCFEE9FBFF97A0BC6E72C50E1012BDD2E26C79B5
        6B73C46B72CF6777BD7082C2A6B7EB656BC47071CD7070C67476C05E68CD6272
        CA6072C46474C76872D26872C77885C6C3DAE9E9FBFFEDF8FF9599BD6E72C50E
        1012BFD8DE6E80AC6979BC6073C65D7BBA5D7EB45F7EB36475B7868CC88F93C2
        8A8EAE9087C79590C58E8BBB918BBD9386C8978AC49C92BFD7D7DEF9F7FAFCF7
        FEAA9DBD6E72C50E1012C1D5DD6D7CAE6D7BC45B6EC65979BC5B7EB75A7CAF6B
        7FBAC9D3F1E8F0FFF2F9FEF6F4FEF3F3FAF5F5F9F9F6FCF8EDFEFAEFFFF7EFFD
        FDFDF6FCFDF6FFF8FAA89CB76E72C50E1012C1CEDF6E77B56D76CB6170D45E7B
        C6597BB95D7EB3677BB5C9D6EFECF7FFEDF8FAF4FEFDF4FEF6F5FEF4F7FEF9F9
        F8FEF9F8FFF9F9FBF7FDEEFAFDF3F7F7F99F9BB76C70C30E1012C6CAE87372CA
        6865D46161DD6170D15E72C56278C16874C2A7ADDDB7BEDABBC4D4AAC9D6ABCC
        D3ACCDD3AFCAD5B1C0DDB0BEDDADBDDCAEC8D8ABC4D8A8BBDF7B87CD7175C80E
        1012BDC6E26F70BA7470D06B68D6666EC46673BE6B7AC06C74C37379C26C75B0
        7684AE637ABA657DB2677EB0697BB46B73C46B73C46974BF6B82B46279B36679
        C76773D76E72C50E1012C3D4DC747CAE6F71B97372C96D75B7717DB7717FB76D
        76BE6F78BF6B78B46B7DAA6A77BE6C7BB56E7CB1717AB47273C37476C36B72B6
        697DA5697FAE687ABC5F6BC46F73C6C4D0E7D6EEE7C0D2D9C2CCE1C4C8E7C3CE
        E3C1D0E2BFD0E1BCC8E5BAC6E7B9CAE5B6CEE1C5C7E6C7CCE3CACEE1CCCCE2CD
        C6E6CDC8E6C9CBE2C7D8DCC2D6DFC1D2E2BCC8E4B7BBEED7E3F4}
      Caption = ''
      TabOrder = 5
    end
  end
  object UniPanel2: TUniPanel
    Left = 0
    Top = 57
    Width = 1024
    Height = 80
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Caption = ''
    object UniDBNavigator1: TUniDBNavigator
      Left = 2
      Top = 54
      Width = 102
      Height = 25
      Hint = ''
      ShowHint = True
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 1
    end
    object UniLabel2: TUniLabel
      Left = 184
      Top = 38
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 2
    end
    object UniLabel3: TUniLabel
      Left = 184
      Top = 60
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 3
    end
    object UniDBDateTimePicker1: TUniDBDateTimePicker
      Left = 220
      Top = 34
      Width = 97
      Hint = ''
      ShowHint = True
      DateTime = 43370.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 4
    end
    object UniDBDateTimePicker2: TUniDBDateTimePicker
      Left = 220
      Top = 56
      Width = 97
      Hint = ''
      ShowHint = True
      DateTime = 43370.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
    end
    object UniLabel4: TUniLabel
      Left = 220
      Top = 1
      Width = 108
      Height = 29
      Hint = ''
      ShowHint = True
      Caption = 'En Curso'
      ParentFont = False
      Font.Color = clNavy
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TabOrder = 6
    end
    object UniSpeedButton4: TUniSpeedButton
      Left = 334
      Top = 36
      Width = 55
      Height = 42
      Hint = 'En Curso'
      ShowHint = True
      Caption = 'En Curso'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 5
      TabOrder = 7
    end
    object UniSpeedButton5: TUniSpeedButton
      Left = 394
      Top = 36
      Width = 55
      Height = 42
      Hint = 'Cerradas'
      ShowHint = True
      Caption = 'Cerradas'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 4
      TabOrder = 8
    end
    object UniSpeedButton7: TUniSpeedButton
      Left = 455
      Top = 36
      Width = 55
      Height = 42
      Hint = 'Todas'
      ShowHint = True
      Caption = 'Todas'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 3
      TabOrder = 9
    end
    object UniSpeedButton8: TUniSpeedButton
      Left = 516
      Top = 36
      Width = 55
      Height = 42
      Hint = 'Documento'
      ShowHint = True
      Caption = 'Docu.'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 2
      TabOrder = 10
    end
    object UniSpeedButton6: TUniSpeedButton
      Left = 120
      Top = 36
      Width = 55
      Height = 42
      Hint = 'En Curso'
      ShowHint = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00EEEEEEEEEEEE
        EEEFEEEEEEEEEEEEEEEFEE00000000000EEFEEE00EEEEEE00EEFEEEE00EEEEEE
        0EEFEEEEE00EEEEE0EEFEEEEEE00EEEEE0EFEEEEEEE00EEEEEEFEEEEEEEE00EE
        EEEFEEEEEEE00EEEEEEFEEEEEE00EEEEEEEFEEEEE00EEEEE0EEFEEEE00EEEEEE
        0EEFEEE00EEEEEE00EEFEE00000000000EEFEEEEEEEEEEEEEEEF}
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = UniNativeImageList1
      ImageIndex = 5
      TabOrder = 11
    end
    object UniSpeedButton9: TUniSpeedButton
      Left = 724
      Top = 6
      Width = 55
      Height = 42
      Hint = 'Documento'
      ShowHint = True
      Caption = 'Albaran'
      ParentColor = False
      Color = clWindow
      IconAlign = iaTop
      Images = UniNativeImageList1
      ImageIndex = 2
      TabOrder = 12
      OnClick = UniSpeedButton9Click
    end
  end
  object UniDBGrid1: TUniDBGrid
    Left = 0
    Top = 137
    Width = 1024
    Height = 431
    Hint = ''
    ShowHint = True
    DataSource = dsListaRecepcion
    ReadOnly = True
    WebOptions.FetchAll = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    Columns = <
      item
        FieldName = 'IDSTOCABE'
        Title.Caption = 'Id.'
        Width = 46
      end
      item
        FieldName = 'FECHA'
        Title.Caption = 'Recepcion'
        Width = 64
      end
      item
        FieldName = 'SEMANA'
        Title.Caption = 'Sem'
        Width = 39
      end
      item
        FieldName = 'FECHACARGO'
        Title.Caption = 'Cargo'
        Width = 64
      end
      item
        FieldName = 'ID_PROVEEDOR'
        Title.Caption = 'N.Prov'
        Width = 64
      end
      item
        FieldName = 'DOCTOPROVE'
        Title.Caption = 'Doc.Proveedor'
        Width = 102
      end
      item
        FieldName = 'DOCTOPROVEFECHA'
        Title.Caption = 'Fecha Docto.'
        Width = 68
      end
      item
        FieldName = 'NOMBRE'
        Title.Caption = 'Nombre'
        Width = 193
      end
      item
        FieldName = 'CANTIENALBA'
        Title.Caption = 'Albaran'
        Width = 64
        ReadOnly = True
      end
      item
        FieldName = 'CANTIDAD'
        Title.Caption = 'Recibidos'
        Width = 64
        ReadOnly = True
      end
      item
        FieldName = 'VALCOSTE'
        Title.Caption = 'Val.Coste'
        Width = 64
        ReadOnly = True
      end
      item
        FieldName = 'VALCOSTE2'
        Title.Caption = 'Val.Coste2'
        Width = 49
        ReadOnly = True
      end
      item
        FieldName = 'CosteTotal'
        Title.Caption = 'CosteTotal'
        Width = 64
      end>
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object UniNativeImageList1: TUniNativeImageList
    Left = 984
    Top = 240
    Images = {
      07000000FFFFFF1F04A802000089504E470D0A1A0A0000000D49484452000000
      18000000180803000000D7A9CDCA0000000373424954080808DBE14FE0000000
      0970485973000005A2000005A201767395660000001974455874536F66747761
      7265007777772E696E6B73636170652E6F72679BEE3C1A0000011D504C5445FF
      FFFF8796D28694C9859BC88796CB8898CA8797CB8795CC8596CBEEEBDD8796CC
      E4E1D48899CDEEEADE8B9BCEEFEBDDE1DFDCE0DBCA8697CAEEEBDD90A1D0ADB6
      D38697CB8697CB91A3D295A8D5EFEBDE8FA1D196A8D5A4B9DE9AAED89DAFD7AB
      C0E2A7B7DDEAE6D9DDD9C796A6D197A7D29BACD49FAFD6A2B2D9A2B4D9A6B2D3
      A7B9DCA8B3CEACBFE0B4C9E7B6CBE9B8CDE8BEC5D6C5D4EBC6CBD7C9C9C7CBCF
      D8CCE3F6CDE5F7CFD2D8D0E8F9D2E9F9D3E7F7D5D0BBD5D0BCD7D8DAD9EDFADA
      D5C1DBD7C4DCD8C5DDD8C6DDD9C6DED9C7DFDAC8E0DBC9E3DECEE3E2DCE4DFCF
      E4E0CFE4E0D0E4E2DCE5F2FBE6E2D2E6F4FCE7E3D3E8E5D9E9E6DDEAE5D7EAE6
      D7EAF6FDEBE7D9ECE8D9ECE8DAECE8DBEDE8DBEDE9DBEEEADDEFEBDE7AD1CDA7
      0000002474524E5300111317223E4046494C555F767B7D80898C9698BCC0CACC
      CCCDD0E9EBEFF1F3F6F9FCFE9E23EA0B000000CD49444154285363908A430025
      410604409608B515C421618324832A812483268190014AC4BA383939F9002522
      9C8140991BA123263A3A3A16AE8F1FD3287489582F0F180844D511E80F0321C4
      1915171E0A0351A876B8BBC180376EA3EC0C4559B148046B69EA1B284A30428D
      727480011D3D4BAB80302311741DBE6A20F1307B191634090B5DB0B8A50217C4
      2857672830D1068B5BCAB2437444C24090AA1F48DC5C9A11DD55C61AD6969666
      727C98FE3055519797E41167C612569E020C0C6C624C0CBCC2E88013680E8710
      00A8C487A27D4511830000000049454E44AE426082FFFFFF1F04160200008950
      4E470D0A1A0A0000000D4948445200000018000000180803000000D7A9CDCA00
      00000373424954080808DBE14FE00000000970485973000005BA000005BA011B
      ED8DC90000001974455874536F667477617265007777772E696E6B7363617065
      2E6F72679BEE3C1A000000C3504C5445FFFFFF1CAA5520AA60ECE3D9DEDECDDF
      DACAE0E0D0E1DCCDEFEBDFB1DABB20AF5DEFEADFE3DFCEDDD9C6D8D4C0D9D4C0
      D8D3BF21AE5E23AF5F39B7703BB8714DBE7E4FBF8053C08255BA7B57BC7D58BC
      7E59C2865ABE815ABF825AC3886FCB9771CB988BD5AB93D8B1A5DEBEA6DFBFB2
      E3C7D5D0BBD8F1E3DAD5C1DBD6C3DCE5D2DDD8C6DFDAC8E0DCCAE1DDCBE1E7D5
      E3F5EBE4DFCFE5E1D1E6E2D2E6F6EDE7E3D3E8E4D5E9E5D6E9E5D7EAE5D7EAE6
      D8EBE7D8ECE8DAEDE8DBEDE9DBEEEADDEFEBDE70E7F9CD0000001174524E5300
      09181B2E30313340526060E7F2F9F9FA55DD3396000000A84944415428538DCE
      790F82201C8061A3B2B24BBA8B2EB3E83EB4940EB3BEFFA78AB10151B2F9FCC1
      18EFF881517E7FA9E40C4109FB5A5E1370DDD4046C9B9A80ED820CC18E7AD28D
      B7A6AA59116242C855DE2BFE8FFA0D5120C44A080FC23DDD286DF0574298EE86
      74DC502500408685F396EB4108C74DBA582CBC226E48CF5C24823452C3C3E706
      6AB8795C1F4E1A2E6AA18451B3C57CBAEC26BDE15C4EEDE4C79D8EE6578CF501
      238F5E52613D0A110000000049454E44AE426082FFFFFF1F041902000089504E
      470D0A1A0A0000000D49484452000000180000001808040000004A7EF5730000
      000467414D410000B18F0BFC6105000000206348524D00007A26000080840000
      FA00000080E8000075300000EA6000003A98000017709CBA513C00000002624B
      47440000AA8D2332000000097048597300000DD700000DD70142289B78000000
      0774494D4507E2091B0E382835E49BCD000000E74944415438CBCD942B0EC240
      10863FA0060DE80A5C55AF80202101410209494FD00B2071382CA227209080E3
      6148B842555D4505AA09480C02415F69675B8161D6CC4CE6CBCCFC9BDD1A000E
      3D5EA8ADC90DFBEB6A516A865B029871790AA8BAE8585CB2092DE34B5D5C16D4
      B9CB806C234E9CD3B05E093C1832A6A10234CCE86819A48FC1401E494FF458E1
      0344F1912D53AE45C04F050460491B80276B19C8DB3EF1A2BBC803061B01B3F0
      54B27A98E52DAB652DC8F8F72375990B55F1250A4080230081BAC3BBF4210940
      8789507520FC79E91D2D1C20147728003654154A3BEC2ABF1AE00355F930068F
      3CFA940000002574455874646174653A63726561746500323031382D30392D32
      375431343A35363A34302B30323A30303B001D34000000257445587464617465
      3A6D6F6469667900323031382D30392D32375431343A35363A34302B30323A30
      304A5DA5880000001974455874536F667477617265007777772E696E6B736361
      70652E6F72679BEE3C1A0000000049454E44AE426082FFFFFF1F04B601000089
      504E470D0A1A0A0000000D49484452000000180000001808040000004A7EF573
      0000000467414D410000B18F0BFC6105000000206348524D00007A2600008084
      0000FA00000080E8000075300000EA6000003A98000017709CBA513C00000002
      624B47440000AA8D2332000000097048597300000DD700000DD70142289B7800
      00000774494D4507E2091B0F000D68BD9946000000844944415438CB6360A03F
      78C0F0004AFFC7021FB060689027D586FF0CFFF14933916A1E051A54194E306C
      235EA317C37B86FF0C8791FC802394181818181819AA19FE32FC6758C3C0438C
      061E86B50CFF19FE325411174A0C0C7B19FE33BC67F0A459B092E124923D4D46
      B0C222EE34C346E29C840A0651E2430744E7B887A4DA496D000018B475F3B6FB
      0FB30000002574455874646174653A63726561746500323031382D30392D3237
      5431353A30303A31332B30323A3030E3D70C4D0000002574455874646174653A
      6D6F6469667900323031382D30392D32375431353A30303A31332B30323A3030
      928AB4F10000001974455874536F667477617265007777772E696E6B73636170
      652E6F72679BEE3C1A0000000049454E44AE426082FFFFFF1F04C50100008950
      4E470D0A1A0A0000000D49484452000000180000001808040000004A7EF57300
      00000467414D410000B18F0BFC6105000000206348524D00007A260000808400
      00FA00000080E8000075300000EA6000003A98000017709CBA513C0000000262
      4B47440000AA8D2332000000097048597300000DD700000DD70142289B780000
      000774494D4507E2091B0F020D5A8BFBC4000000934944415438CBED943D0A83
      401085BF24A090327DC84D7237F10462659323D87A951469E29E2020FE216E1A
      23627616A6147CAF1BDEB73BC3B00B4A1D0008B97104C062A8FD4840428F9D3D
      F0E0EC03E245F8E7CC07940EA09A5A75CE609DF5887655197951D0E0385FF693
      AB7483A45C0B7CB4C0B4AE1DD82060547973E2C25D01A4FF4F54764F42B0FE04
      648DBCE9B413035FAA555C631ABF68C00000002574455874646174653A637265
      61746500323031382D30392D32375431353A30323A31332B30323A3030E722DC
      700000002574455874646174653A6D6F6469667900323031382D30392D323754
      31353A30323A31332B30323A3030967F64CC0000001974455874536F66747761
      7265007777772E696E6B73636170652E6F72679BEE3C1A0000000049454E44AE
      426082FFFFFF1F04C302000089504E470D0A1A0A0000000D4948445200000018
      0000001808040000004A7EF5730000000467414D410000B18F0BFC6105000000
      206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
      98000017709CBA513C00000002624B47440000AA8D2332000000097048597300
      000DD700000DD70142289B780000000774494D4507E2091B0F0336F29B23A100
      0001914944415438CB95D34D4854511887F1DFD884B8082C5A04469F1054430B
      99C8A0850481440B41B0A85C09E1A6B014193530219A88823068379B09613681
      D1C6A68F452B0909DCD4A2452D0CAA4D1B951643B5B863F7DEF128F65FBDF7DC
      E739EF790FF766343BAF47B35FD6CF7725EFA232A368C12B939E986BC046958C
      819F0E9BF431126674A34DC1D50661BFAF0E806E35AD6E46CB33E08A1E1B6542
      575464C1BCA35EEB5B07CED8665635292CCA6FB07B874EF7F4EAB5E24ED6E632
      E8BABC3F1EC5C229BB83E8075CF2D2596597B5346D62F7D3AA6E28289BF33CEE
      F0432D889FB3C390935AE594946221E75000BFA86AD85E472C587277F596E085
      376BF03EB34690B32FC29342BFF606BC4BD908C85B8EF0A430B5E622BF29805B
      7619585D8E8551271278BBE93A3E21673E7E110BC504DEE28BC53AFEDB7D9D21
      21D961A7DB0EBA66BB7EE3E993863B3CF6DE8AA287F6A0232C0C39FEAF3E63C0
      B2B78EA978E6735898B24528B5F4979CD5E602184EDD529CA73EA585078119D2
      49CDD0A4A2E23F12CDB0B5FE678793311D3FFC05DC9357373B09048C00000025
      74455874646174653A63726561746500323031382D30392D32375431353A3033
      3A35342B30323A3030490D873A0000002574455874646174653A6D6F64696679
      00323031382D30392D32375431353A30333A35342B30323A303038503F860000
      001974455874536F667477617265007777772E696E6B73636170652E6F72679B
      EE3C1A0000000049454E44AE426082FFFFFF1F041B04000089504E470D0A1A0A
      0000000D4948445200000018000000180806000000E0773DF800000009704859
      7300000EC400000EC401952B0E1B000000206348524D00007A25000080830000
      F9FF000080E8000052080001155800003A970000176FD75A1F90000003A14944
      415478DAB495CB6B5D5514C67F6B9FC74D6E9B374D258AD5480C452742EB8342
      B50369FE00E9C891032B8E0441270A2254EA481CA903870527CE44A516B1A2B4
      12A1E02025A66A684BCDAB79F426B9E7B1F75E0ECEB939B9F75ADBA22ED89C07
      6BAFC7B7D6B796A82AFFA78477A3F4ED92D5EB09DC72453003A1F0400F1CDB17
      CA9DEECA3F65F0FE5CA6CB4E884341A4C3962A9953EE0BE1F54722B927079F5D
      CBF44203EAB1C129E40A9D7A2210891008245639D2072FDCDFEDA8CBC1875752
      9DB78218701EACC2780F7486A1C07C02A14020C5F7440D5E7D2896DB3A387335
      D50B9B804009375EA126DAE540805405539A0BCAE7D101C389B12A93B6229F5B
      F3C4A1605DF5AFE1E0F4B8214768C52202DE2BEFCC7BF606BB3AC6C0D99B8E13
      63D1CE3FD37A797B665B8D81A6F564BE3AA9F3183145CC521D2342E2DA75B7AD
      C7A3BCFB6BA25D19CC353DC608B6038BDC2B7F6C3B12ADEA60CA2A58EF495D7B
      5D9D57666D070FBEF8335587925AC597F8B66433570EF70738A10DA240E1BDDF
      33A25DDA0AE425545F2F653A351A4B0830BBE948AC92ABD2D96789535205A782
      2F1D18810025719ED4485777452A5C6E38A646CB0C16534FEA3DD6170ABBAFA4
      4EB14EC9015FA66044885052A7A4A6DDB8947A0B4D5741947B4F5A66D0924C61
      CB2A2BB92F9C50D540503CCA8DC4927BC39E40D84DB1C80B99F79583A14848BD
      23F7A5710FC78642DE18AF6315120FBE830916E1D2D3838072EAB72D7ED870B4
      28A6060683B06A88C9BE90CC51B6A5A2AA7CB992717C7A9DE9F59C81D0E03D64
      AE983F280C84F0F942C2918B6B9CBB99A3BE80ACB0018F9604D961F263DF2CA9
      A2E4BEAA8103D672CF137D21A727FB188E0CA111661A396FCE3658C894FE5076
      C8A44064000F33C7F74B1B0F0EEE09F8A591937B45155673C595B09C5FCD78F2
      FB655E3A50E77AE239BB9810C7864060D34228C250280507313C5E37DDB368AE
      91D79E3BBF9C0446584C1D179F196128323B334981A6538C40CDC84E96A10857
      9B96E7A75719AD0538EBF9EED97D3D130371DA96C1445F94BE7CA0CE47F35BC4
      029F5EDBA666AAF9733B31020DABC40632EB39F9609D96F1BF1DD72FFEB8AC5F
      2DA734B5285A27B33B4965805A20F48A30351C71E6E87EB9E3C279EBD2AA7E70
      659338129C82D5A22EBA6B548B14F0040259EE79EDE1BD9C3A342277BD3267D7
      B3DE933FAD6C4F6FE448208571A9D826803AE5707FC4274F8DD427876ACD7BDE
      C92DF9F8F286FEBC9A72A3A4FF586FC0A1E198570E0EFEBBA5FF5FC85F0300A8
      090643B04F19790000000049454E44AE426082}
  end
  object dsListaRecepcion: TDataSource
    DataSet = DMMenuRecepcion.sqlListaRecepcion
    Left = 888
    Top = 424
  end
end
