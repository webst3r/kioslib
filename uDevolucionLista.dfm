object FormDevolucionLista: TFormDevolucionLista
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1024
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1024
    Height = 57
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object BtnDevolEnCurso: TUniBitBtn
      Left = 8
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888800000088888008888888888800000088880FF000000000080000008880
        F0080FFFFFFF08000000880F0FF00F00000F0800000080F0F0080FFFFFFF0800
        0000880F0FF00F00000F0800000080F0F0080FFFFFFF08000000880F0FB00F00
        F0000800000080F0FBFB0FFFF0F088000000880FBFBF0FFFF0088800000080FB
        FBFB00000088880000008800BFBFBFBF088888000000888800FBFBF088888800
        000088888800B808888888000000888888880088888888000000888888888888
        888888000000888888888888888888000000}
      Caption = ''
      TabOrder = 1
      ImageIndex = 13
    end
    object BtnDevolEnviadas: TUniBitBtn
      Left = 69
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 2
    end
    object spTodas: TUniBitBtn
      Left = 130
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77770000000000000007FFFFFFFFFFFFFFF00000F000F00F000F70910BB30E60
        330070910BB30E60330070910BB30E60330070910BB30E60330070910BB30E60
        330070910BB30E603300709100000E603300709100FF0E6000007000010F0E60
        0F00700F0100000000007700F00700FF00777770000770000077}
      Caption = ''
      TabOrder = 3
    end
    object BtnDocumentoCabe: TUniBitBtn
      Left = 191
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888000000080000000000000008000000080F8FFFFFFFFFFF0800000008089
        9FFF899998F08000000080F98FFFF8888FF08000000080FFFFFFFFFFFFF08000
        000080F7F447844447F08000000080F8F888F88888F08000000080F8F7788777
        78F08000000080F7F747847747F08000000080FFFFFFF8888FF0800000008080
        0007FFFFFFF08000000080F8FF8FFFFFFFF08000000080866666F88888808000
        000080E767EEEEEEEE708000000080E8E7EEEEEEEE8080000000800000000000
        000080000000}
      Caption = ''
      TabOrder = 4
    end
    object UniLabel1: TUniLabel
      Left = 295
      Top = 8
      Width = 54
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Documento'
      TabOrder = 5
    end
    object edDocumentoProve: TUniDBEdit
      Left = 287
      Top = 28
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 6
    end
    object spVerDocumentoProveedor: TUniBitBtn
      Left = 448
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000F0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0087FFFFFFFFFFFF0B3087FFFFFFFFFFF0BB0087FF
        FFFFFFFF0BB3008FFFFFFFFFF0BBB008FFFFFFFFF00BBB007FFFFFFF00BBB007
        FFF0FFFFF00BBB007FF0FFFFFFF00BB007F0FFFFFFFFF00B0070FFFFFFFFFFF0
        00F0FFFFFFFFFFFFFFF0}
      Caption = ''
      TabOrder = 7
      ImageIndex = 13
    end
    object btEtiquetasDocumento: TUniBitBtn
      Left = 509
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42050000424D4205000000000000360000002800000016000000130000000100
        1800000000000C05000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FFFFFF000000000000000000000000FFFFFFC0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C00000000000000000000000000000000000000000000000C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C0000000C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0008000008000C0C0
        C00000FF0000FFC0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C00000
        00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0000000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFC0C0C0C0
        C0C0808080000000000000808080808080808080808080808080808080808080
        8080808080808080808080808080800000000000008080800000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000FFFFFFFFFFFF00008080
        80000000000000808080808080808080000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000FFFFFF000000
        000000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000
        0000FFFFFF000000000000000000000000000000000000000000000000FFFFFF
        000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000000000FFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF000000FFFFFF0000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000
        000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000}
      Caption = ''
      TabOrder = 8
    end
    object btInicioConfiguracion: TUniBitBtn
      Left = 584
      Top = 25
      Width = 25
      Height = 25
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000013000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        3333333000003243342222224433333000003224422222222243333000003222
        222AAAAA222433300000322222A33333A222433000003222223333333A224330
        00003222222333333A44433000003AAAAAAA3333333333300000333333333333
        3333333000003333333333334444443000003A444333333A2222243000003A22
        43333333A2222430000033A22433333442222430000033A22244444222222430
        0000333A2222222222AA243000003333AA222222AA33A3300000333333AAAAAA
        333333300000333333333333333333300000}
      Caption = ''
      TabOrder = 9
    end
    object BtnPreProve: TUniBitBtn
      Left = 629
      Top = 8
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000007777777777777777770000007777777777770007770000007444
        4400000006007700000074FFFF08880600080700000074F008000060EE070700
        000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
        00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
        080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
        000074F444F444F444F477000000744444444444444477000000777777777777
        777777000000777777777777777777000000}
      Caption = ''
      TabOrder = 10
    end
  end
  object pcControl: TUniPageControl
    Left = 0
    Top = 57
    Width = 1024
    Height = 558
    Hint = ''
    ShowHint = True
    ActivePage = tabOpciones
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ExplicitTop = 89
    ExplicitHeight = 526
    object tabGrid: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabGrid'
      object wwDBGrid1: TUniDBGrid
        Left = 0
        Top = 0
        Width = 1016
        Height = 530
        Hint = ''
        ShowHint = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
    object tabOpciones: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabOpciones'
      object UniContainerPanel1: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 1016
        Height = 49
        Hint = ''
        ShowHint = True
        ParentColor = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        object BtnBuscaDistribui: TUniBitBtn
          Left = 3
          Top = 3
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 1
          ImageIndex = 13
        end
        object pDistriCodi: TUniDBEdit
          Left = 64
          Top = 23
          Width = 54
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 2
        end
        object UniLabel2: TUniLabel
          Left = 64
          Top = 4
          Width = 54
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Documento'
          TabOrder = 3
        end
        object pDistriNom: TUniDBEdit
          Left = 124
          Top = 23
          Width = 157
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 4
        end
        object btcambiardistribuidora: TUniBitBtn
          Left = 299
          Top = 3
          Width = 116
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Cambiar Distribuidora'
          TabOrder = 5
          ImageIndex = 13
        end
        object BitBtn3: TUniBitBtn
          Left = 420
          Top = 3
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 6
          ImageIndex = 13
        end
      end
      object PageControl3: TUniPageControl
        Left = 0
        Top = 49
        Width = 1016
        Height = 602
        Hint = ''
        ShowHint = True
        ActivePage = TabSheet8
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        object TabSheet8: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Datos Cabecera documento'
          object UniContainerPanel2: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 1008
            Height = 47
            Hint = ''
            ShowHint = True
            ParentColor = False
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            object UniLabel3: TUniLabel
              Left = 12
              Top = 23
              Width = 68
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'N. Documento'
              TabOrder = 1
            end
            object wwDBEdit2: TUniDBEdit
              Left = 86
              Top = 18
              Width = 89
              Height = 22
              Hint = ''
              ShowHint = True
              TabOrder = 2
            end
            object wwDBEdit12: TUniDBEdit
              Left = 181
              Top = 18
              Width = 53
              Height = 22
              Hint = ''
              ShowHint = True
              TabOrder = 3
            end
            object UniLabel4: TUniLabel
              Left = 86
              Top = 3
              Width = 68
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'N. Documento'
              TabOrder = 4
            end
            object UniLabel5: TUniLabel
              Left = 181
              Top = 3
              Width = 40
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Paquete'
              TabOrder = 5
            end
            object pDistriData: TUniDBDateTimePicker
              Left = 256
              Top = 18
              Width = 120
              Hint = ''
              ShowHint = True
              DateTime = 43374.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 6
            end
            object DBText2: TUniDBText
              Left = 382
              Top = 23
              Width = 41
              Height = 13
              Hint = ''
              ShowHint = True
            end
            object Fecha: TUniLabel
              Left = 256
              Top = 3
              Width = 29
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha'
              TabOrder = 8
            end
          end
          object UniPanel4: TUniPanel
            Left = 0
            Top = 47
            Width = 1008
            Height = 175
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            Caption = ''
            object PnlSeleDevolE: TUniPanel
              Left = 1
              Top = 1
              Width = 1006
              Height = 49
              Hint = ''
              ShowHint = True
              Align = alTop
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 1
              Caption = ''
              object UniLabel6: TUniLabel
                Left = 7
                Top = 24
                Width = 75
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Doc. Proveedor'
                TabOrder = 1
              end
              object wwDBEdit5: TUniDBEdit
                Left = 86
                Top = 19
                Width = 148
                Height = 22
                Hint = ''
                ShowHint = True
                TabOrder = 2
              end
              object UniLabel7: TUniLabel
                Left = 256
                Top = 3
                Width = 44
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'de Fecha'
                TabOrder = 3
              end
              object DBText1: TUniDBText
                Left = 382
                Top = 24
                Width = 41
                Height = 13
                Hint = ''
                ShowHint = True
              end
              object wwDBDateTimePicker2: TUniDBDateTimePicker
                Left = 256
                Top = 19
                Width = 120
                Hint = ''
                ShowHint = True
                DateTime = 43374.000000000000000000
                DateFormat = 'dd/MM/yyyy'
                TimeFormat = 'HH:mm:ss'
                TabOrder = 5
              end
            end
            object PnlSeleDevolC: TUniPanel
              Left = 1
              Top = 50
              Width = 1006
              Height = 99
              Hint = ''
              ShowHint = True
              Align = alTop
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 2
              Caption = ''
              ExplicitTop = 48
              DesignSize = (
                1006
                99)
              object RgFechaAviDevoC: TUniDBRadioGroup
                Left = 3
                Top = 3
                Width = 170
                Height = 87
                Hint = ''
                ShowHint = True
                Caption = 'Selecion por'
                TabOrder = 1
                Items.Strings = (
                  'Fecha Aviso'
                  'Fecha Devolucion'
                  'Docto.Distribuidor')
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
              object UniGroupBox1: TUniGroupBox
                Left = 183
                Top = 6
                Width = 193
                Height = 84
                Hint = ''
                ShowHint = True
                Caption = ''
                TabOrder = 2
                object UniLabel8: TUniLabel
                  Left = 7
                  Top = 3
                  Width = 29
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Fecha'
                  TabOrder = 1
                end
                object pFechaDevolu: TUniDBDateTimePicker
                  Left = 15
                  Top = 59
                  Width = 120
                  Hint = ''
                  ShowHint = True
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 2
                end
              end
              object UniGroupBox2: TUniGroupBox
                Left = 382
                Top = 6
                Width = 193
                Height = 84
                Hint = ''
                ShowHint = True
                Caption = ''
                Anchors = []
                TabOrder = 3
                object BtnBuscaDocuNou: TUniBitBtn
                  Left = 4
                  Top = 13
                  Width = 70
                  Height = 39
                  Hint = ''
                  ShowHint = True
                  Caption = 'Doc. Distri'
                  TabOrder = 1
                end
                object UniLabel9: TUniLabel
                  Left = 19
                  Top = 66
                  Width = 44
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'de Fecha'
                  TabOrder = 2
                end
                object UniDBDateTimePicker1: TUniDBDateTimePicker
                  Left = 79
                  Top = 57
                  Width = 100
                  Hint = ''
                  ShowHint = True
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 3
                end
                object wwDBEdit14: TUniDBEdit
                  Left = 79
                  Top = 29
                  Width = 100
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  TabOrder = 4
                end
                object UniLabel10: TUniLabel
                  Left = 79
                  Top = 13
                  Width = 75
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Doc. Proveedor'
                  TabOrder = 5
                end
              end
            end
            object cbRestoVentas: TUniCheckBox
              Left = 386
              Top = 152
              Width = 190
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Desactivar ventas autom'#225'ticas'
              TabOrder = 3
            end
          end
          object UniPanel3: TUniPanel
            Left = 0
            Top = 222
            Width = 1008
            Height = 176
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 2
            Caption = ''
            object UniLabel11: TUniLabel
              Left = 3
              Top = 6
              Width = 66
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Obervaciones'
              TabOrder = 1
            end
            object DBMemo1: TUniDBMemo
              Left = 1
              Top = 21
              Width = 410
              Height = 89
              Hint = ''
              ShowHint = True
              ScrollBars = ssBoth
              TabOrder = 2
            end
            object UniLabel12: TUniLabel
              Left = 3
              Top = 120
              Width = 57
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Vencimiento'
              TabOrder = 3
            end
            object pFechaCargoCrea: TUniDBDateTimePicker
              Left = 3
              Top = 139
              Width = 100
              Hint = ''
              ShowHint = True
              DateTime = 43374.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 4
            end
            object UniLabel13: TUniLabel
              Left = 109
              Top = 120
              Width = 63
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha Abono'
              TabOrder = 5
            end
            object pFechaAbonoCrea: TUniDBDateTimePicker
              Left = 109
              Top = 139
              Width = 100
              Hint = ''
              ShowHint = True
              DateTime = 43374.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 6
            end
            object BtnGrabarCabe: TUniBitBtn
              Left = 223
              Top = 119
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 7
            end
            object BtnCancelarCabe: TUniBitBtn
              Left = 299
              Top = 119
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 8
            end
            object BtnReactivar: TUniBitBtn
              Left = 366
              Top = 119
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 9
            end
          end
        end
        object TabSheet9: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Devoluciones del mismo cliente'
          object UniLabel14: TUniLabel
            Left = 0
            Top = 0
            Width = 1008
            Height = 13
            Hint = ''
            ShowHint = True
            Alignment = taCenter
            AutoSize = False
            Caption = 'Albaranes de Devolucion del Distribuidor seleccionado'
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
          end
          object wwDBGrid3: TUniDBGrid
            Left = 0
            Top = 13
            Width = 1008
            Height = 209
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
          end
          object UniPanel2: TUniPanel
            Left = 0
            Top = 222
            Width = 1008
            Height = 352
            Hint = ''
            ShowHint = True
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 2
            Caption = ''
            object UniLabel15: TUniLabel
              Left = 3
              Top = 24
              Width = 267
              Height = 67
              Hint = ''
              ShowHint = True
              AutoSize = False
              Caption = 
                'Hay varias devoluciones en curso de esta distribuidora Si desea ' +
                'puede [ampliar] la devolucion en que est'#233' situado la rejilla o p' +
                'ulsar un [nuevo] documento de devoluci'#243'n'
              TabOrder = 1
            end
            object BtnDevoluAfegir: TUniBitBtn
              Left = 276
              Top = 27
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 2
              ImageIndex = 13
            end
            object BtnDevoluNova: TUniBitBtn
              Left = 337
              Top = 27
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 3
              ImageIndex = 13
            end
          end
        end
        object TabSheet15: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'TabSheet15'
          object UniLabel16: TUniLabel
            Left = 26
            Top = 3
            Width = 66
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'ID Devolucion'
            TabOrder = 0
          end
          object UniLabel17: TUniLabel
            Left = 30
            Top = 38
            Width = 62
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Por:'
            TabOrder = 1
          end
          object edNuevaIDDevolucion: TUniDBEdit
            Left = 115
            Top = 29
            Width = 121
            Height = 22
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
          object UniLabel18: TUniLabel
            Left = 36
            Top = 63
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Contrase'#241'a'
            TabOrder = 3
          end
          object edContraCambiarID: TUniDBEdit
            Left = 115
            Top = 62
            Width = 121
            Height = 22
            Hint = ''
            ShowHint = True
            TabOrder = 4
          end
          object DBText10: TUniDBText
            Left = 115
            Top = 8
            Width = 47
            Height = 13
            Hint = ''
            ShowHint = True
          end
          object btCambiarIdDevolucion: TUniBitBtn
            Left = 115
            Top = 90
            Width = 75
            Height = 25
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar'
            TabOrder = 6
            ImageIndex = 13
          end
        end
        object TabSheet16: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Cambiar Distribuidora'
          object btNuevaDistribuidora: TUniBitBtn
            Left = 3
            Top = 6
            Width = 126
            Height = 45
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Distribuidora'
            TabOrder = 1
            ImageIndex = 13
          end
          object wwDBEdit46: TUniDBEdit
            Left = 178
            Top = 29
            Width = 288
            Height = 22
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
          object wwDBEdit45: TUniDBEdit
            Left = 135
            Top = 29
            Width = 47
            Height = 22
            Hint = ''
            ShowHint = True
            TabOrder = 3
          end
          object UniLabel19: TUniLabel
            Left = 135
            Top = 10
            Width = 94
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Nueva Distribuidora'
            TabOrder = 4
          end
          object BitBtn4: TUniBitBtn
            Left = 72
            Top = 58
            Width = 149
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Distribuidora'
            TabOrder = 5
            ImageIndex = 13
          end
          object BitBtn5: TUniBitBtn
            Left = 227
            Top = 58
            Width = 149
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Distribuidora'
            TabOrder = 6
            ImageIndex = 13
          end
          object UniDBLookupComboBox1: TUniDBLookupComboBox
            Left = 3
            Top = 10
            Width = 30
            Hint = ''
            ShowHint = True
            ListFieldIndex = 0
            TabOrder = 0
            Color = clWindow
          end
          object LabelProcesoDistri: TUniLabel
            Left = 179
            Top = 121
            Width = 87
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'LabelProcesoDistri'
            TabOrder = 7
          end
        end
      end
    end
  end
  object plRecalcularPendiente: TUniPanel
    Left = 648
    Top = 56
    Width = 209
    Height = 49
    Hint = ''
    ShowHint = True
    ParentFont = False
    Font.Height = -16
    Font.Style = [fsBold]
    TabOrder = 2
    Caption = 'Recalculando pendiente'
  end
end
