object FormDevolNuevo: TFormDevolNuevo
  Left = 0
  Top = 0
  ClientHeight = 344
  ClientWidth = 539
  Caption = 'FormNuevaDevo'
  OnShow = UniFormShow
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  PixelsPerInch = 96
  TextHeight = 13
  object cbDistri: TUniDBLookupComboBox
    Left = 98
    Top = 66
    Width = 433
    Height = 24
    Hint = ''
    ListField = 'NOMBRE'
    ListSource = dsProveS
    KeyField = 'NOMBRE'
    ListFieldIndex = 0
    DataField = 'NOMBRE'
    DataSource = dsCabe1
    ParentFont = False
    Font.Height = -13
    AnyMatch = True
    TabOrder = 0
    Color = clWindow
    ClientEvents.UniEvents.Strings = (
      
        'beforeInit=  function beforeInit(sender, config)'#13#10'{'#13#10'    config.' +
        'listConfig = {'#13#10'        cls: '#39'mylist'#39#13#10'    }'#13#10'}')
    Style = csDropDown
    OnChange = cbDistriChange
  end
  object UniLabel3: TUniLabel
    Left = 19
    Top = 68
    Width = 72
    Height = 16
    Hint = ''
    Caption = 'Distribuidora'
    ParentFont = False
    Font.Height = -13
    TabOrder = 8
  end
  object edPaquete: TUniDBEdit
    Left = 99
    Top = 101
    Width = 173
    Height = 30
    Hint = ''
    DataField = 'PAQUETE'
    DataSource = dsCabe1
    ParentFont = False
    Font.Height = -16
    TabOrder = 1
  end
  object UniLabel4: TUniLabel
    Left = 39
    Top = 104
    Width = 46
    Height = 16
    Hint = ''
    Caption = 'Paquete'
    ParentFont = False
    Font.Height = -13
    TabOrder = 9
  end
  object UniLabel5: TUniLabel
    Left = 16
    Top = 140
    Width = 73
    Height = 16
    Hint = ''
    Caption = 'Max Paquete'
    ParentFont = False
    Font.Height = -13
    TabOrder = 10
  end
  object UniDBEdit2: TUniDBEdit
    Left = 99
    Top = 137
    Width = 173
    Height = 30
    Hint = ''
    DataField = 'MAXPAQUETE'
    DataSource = dsProveS
    ParentFont = False
    Font.Height = -16
    TabOrder = 2
  end
  object btGrabarGrupo: TUniButton
    Left = 341
    Top = 222
    Width = 85
    Height = 42
    Hint = ''
    Caption = 'Aceptar Grupo'
    TabOrder = 6
    OnClick = btGrabarGrupoClick
  end
  object btCancelar: TUniButton
    Left = 429
    Top = 222
    Width = 99
    Height = 42
    Hint = ''
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = btCancelarClick
  end
  object edFecha: TUniDBDateTimePicker
    Left = 412
    Top = 101
    Width = 120
    Height = 30
    Hint = ''
    DataField = 'FECHA'
    DataSource = dsCabe1
    DateTime = 43395.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 4
    ParentFont = False
    Font.Height = -16
  end
  object UniLabel6: TUniLabel
    Left = 372
    Top = 104
    Width = 34
    Height = 16
    Hint = ''
    Caption = 'Fecha'
    ParentFont = False
    Font.Height = -13
    TabOrder = 11
  end
  object UniLabel7: TUniLabel
    Left = 307
    Top = 176
    Width = 99
    Height = 16
    Hint = ''
    Caption = 'Fecha Devolucion'
    ParentFont = False
    Font.Height = -13
    TabOrder = 12
  end
  object UniDBDateTimePicker2: TUniDBDateTimePicker
    Left = 412
    Top = 173
    Width = 120
    Height = 30
    Hint = ''
    DataField = 'DOCTOPROVEFECHA'
    DataSource = dsCabe1
    DateTime = 43395.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 5
    ParentFont = False
    Font.Height = -16
  end
  object UniLabel8: TUniLabel
    Left = 4
    Top = 176
    Width = 87
    Height = 16
    Hint = ''
    Caption = 'Doc. Proveedor'
    ParentFont = False
    Font.Height = -13
    TabOrder = 13
  end
  object UniDBEdit3: TUniDBEdit
    Left = 99
    Top = 173
    Width = 173
    Height = 30
    Hint = ''
    DataField = 'DOCTOPROVE'
    DataSource = dsCabe1
    ParentFont = False
    Font.Height = -16
    TabOrder = 3
  end
  object btGrabarIndividual: TUniButton
    Left = 246
    Top = 222
    Width = 90
    Height = 42
    Hint = ''
    Caption = 'Aceptar Indivual'
    TabOrder = 14
    OnClick = btGrabarIndividualClick
  end
  object lbAccion: TUniLabel
    Left = 16
    Top = 8
    Width = 51
    Height = 18
    Hint = ''
    Caption = 'lbAccion'
    ParentFont = False
    Font.Height = -15
    TabOrder = 15
  end
  object dsCabe1: TDataSource
    DataSet = DMDevolucion.sqlCabe1
    Left = 60
    Top = 131
  end
  object dsProveS: TDataSource
    DataSet = DMDevolucion.sqlProveS
    Left = 92
    Top = 115
  end
  object dsCabeNuevo: TDataSource
    DataSet = DMDevolucion.sqlCabeNuevo
    Left = 20
    Top = 131
  end
end
