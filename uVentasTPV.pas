unit uVentasTPV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormVentasTPV = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    UniPanel8: TUniPanel;
    panel1: TUniPanel;
    pnUp: TUniPanel;
    pnDown: TUniPanel;
    pn_v5: TUniPanel;
    pn5_10: TUniPanel;
    uni5_10: TUniImage;
    UniPanel22: TUniPanel;
    uni5_1: TUniImage;
    pn5_9: TUniPanel;
    uni5_9: TUniImage;
    pn5_8: TUniPanel;
    uni5_8: TUniImage;
    UniPanel25: TUniPanel;
    uni5_7: TUniImage;
    UniPanel26: TUniPanel;
    uni5_6: TUniImage;
    UniPanel27: TUniPanel;
    uni5_5: TUniImage;
    UniPanel28: TUniPanel;
    uni5_4: TUniImage;
    UniPanel29: TUniPanel;
    uni5_3: TUniImage;
    UniPanel30: TUniPanel;
    uni5_2: TUniImage;
    pn_v4: TUniPanel;
    pn4_10: TUniPanel;
    uni4_10: TUniImage;
    pn4_1: TUniPanel;
    uni4_1: TUniImage;
    pn4_9: TUniPanel;
    uni4_9: TUniImage;
    pn4_8: TUniPanel;
    uni4_8: TUniImage;
    pn4_7: TUniPanel;
    uni4_7: TUniImage;
    pn4_6: TUniPanel;
    uni4_6: TUniImage;
    pn4_5: TUniPanel;
    uni4_5: TUniImage;
    pn4_4: TUniPanel;
    uni4_4: TUniImage;
    pn4_3: TUniPanel;
    uni4_3: TUniImage;
    pn4_2: TUniPanel;
    uni4_2: TUniImage;
    pn_v3: TUniPanel;
    pn3_10: TUniPanel;
    uni3_10: TUniImage;
    pn3_1: TUniPanel;
    uni3_1: TUniImage;
    pn3_9: TUniPanel;
    uni3_9: TUniImage;
    pn3_8: TUniPanel;
    uni3_8: TUniImage;
    pn3_7: TUniPanel;
    uni3_7: TUniImage;
    pn3_6: TUniPanel;
    uni3_6: TUniImage;
    pn3_5: TUniPanel;
    uni3_5: TUniImage;
    pn3_4: TUniPanel;
    uni3_4: TUniImage;
    pn3_3: TUniPanel;
    uni3_3: TUniImage;
    pn3_2: TUniPanel;
    uni3_2: TUniImage;
    pn_v2: TUniPanel;
    pn2_10: TUniPanel;
    uni2_10: TUniImage;
    pn2_1: TUniPanel;
    uni2_1: TUniImage;
    pn2_9: TUniPanel;
    uni2_9: TUniImage;
    pn2_8: TUniPanel;
    uni2_8: TUniImage;
    pn2_7: TUniPanel;
    uni2_7: TUniImage;
    pn2_6: TUniPanel;
    uni2_6: TUniImage;
    pn2_5: TUniPanel;
    uni2_5: TUniImage;
    pn2_4: TUniPanel;
    uni2_4: TUniImage;
    pn2_3: TUniPanel;
    uni2_3: TUniImage;
    pn2_2: TUniPanel;
    uni2_2: TUniImage;
    pn_v1: TUniPanel;
    pn1_10: TUniPanel;
    uni1_10: TUniImage;
    pn1_1: TUniPanel;
    uni1_1: TUniImage;
    pn1_9: TUniPanel;
    uni1_9: TUniImage;
    pn1_8: TUniPanel;
    uni1_8: TUniImage;
    pn1_7: TUniPanel;
    uni1_7: TUniImage;
    pn1_6: TUniPanel;
    uni1_6: TUniImage;
    pn1_5: TUniPanel;
    uni1_5: TUniImage;
    pn1_4: TUniPanel;
    uni1_4: TUniImage;
    pn1_3: TUniPanel;
    uni1_3: TUniImage;
    pn1_2: TUniPanel;
    uni1_2: TUniImage;
    pn_v6: TUniPanel;
    pn6_10: TUniPanel;
    uni6_10: TUniImage;
    pn6_1: TUniPanel;
    uni6_1: TUniImage;
    pn6_9: TUniPanel;
    uni6_9: TUniImage;
    pn6_8: TUniPanel;
    uni6_8: TUniImage;
    pn6_7: TUniPanel;
    uni6_7: TUniImage;
    pn6_6: TUniPanel;
    uni6_6: TUniImage;
    pn6_5: TUniPanel;
    uni6_5: TUniImage;
    pn6_4: TUniPanel;
    uni6_4: TUniImage;
    pn6_3: TUniPanel;
    uni6_3: TUniImage;
    pn6_2: TUniPanel;
    uni6_2: TUniImage;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniCheckBox1: TUniCheckBox;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    UniBitBtn12: TUniBitBtn;
    UniBitBtn13: TUniBitBtn;
    UniBitBtn14: TUniBitBtn;
    UniBitBtn15: TUniBitBtn;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormVentasTPV: TFormVentasTPV;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMManteTPV;

function FormVentasTPV: TFormVentasTPV;
begin
  Result := TFormVentasTPV(DMppal.GetFormInstance(TFormVentasTPV));

end;


end.
