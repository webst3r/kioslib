unit uMenuClienteFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TForm = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function Form: TForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti;

function Form: TForm;
begin
  Result := TForm(DMppal.GetFormInstance(TForm));

end;


end.
