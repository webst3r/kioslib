object UniServerModule: TUniServerModule
  OldCreateOrder = False
  OnCreate = UniGUIServerModuleCreate
  TempFolder = 'temp\'
  Port = 8081
  Title = 'KIOSLIB'
  AjaxTimeout = 60000
  SuppressErrors = []
  Bindings = <>
  CustomCSS.Strings = (
    '.myList .x-boundlist-item {'
    '    color: Black;'
    '    height: 30; '
    '    font-size: 16px;'
    '    font-family: Tahoma;'
    '}'
    ''
    '.mGridCliente .x-grid-cell {'
    '    height: 30px !important;'
    '    line-height: 30px !important;'
    '    color: black;'
    '    '
    '}'
    ''
    '')
  ISAPIOptions.ThreadPoolsize = 400
  ISAPIOptions.ThreadRecycleCount = 400
  ServerLimits.ThreadPoolSize = 4000
  ServerLimits.MaxSessions = 1000
  ServerLimits.MaxRequests = 1000
  ServerLimits.MaxConnections = 100
  ServerLimits.MaxGDIObjects = 500000
  SSL.SSLOptions.RootCertFile = 'root.pem'
  SSL.SSLOptions.CertFile = 'cert.pem'
  SSL.SSLOptions.KeyFile = 'key.pem'
  SSL.SSLOptions.Method = sslvTLSv1_1
  SSL.SSLOptions.SSLVersions = [sslvTLSv1_1]
  SSL.SSLOptions.Mode = sslmServer
  SSL.SSLOptions.VerifyMode = []
  SSL.SSLOptions.VerifyDepth = 0
  SSL.SSLPort = 443
  Options = [soShowLicenseInfo, soWipeShadowSessions]
  ConnectionFailureRecovery.ErrorMessage = 'Conectando con Server.. espere'
  ConnectionFailureRecovery.RetryMessage = 'Un momento..'
  HTTPServerOptions.MaxAliveSecs = 240
  HTTPServerOptions.MaxPoolSize = 800
  OnBeforeInit = UniGUIServerModuleBeforeInit
  Height = 150
  Width = 215
end
