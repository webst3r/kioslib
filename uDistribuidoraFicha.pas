unit uDistribuidoraFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniProgressBar, uniDBCheckBox,
  uniImageList, frxRich;

type
  TFormDistribuidoraFicha = class(TUniForm)
    pnlFicha: TUniPanel;
    UniLabel1: TUniLabel;
    pCodi: TUniDBEdit;
    UniLabel2: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniLabel3: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniDBEdit7: TUniDBEdit;
    UniLabel8: TUniLabel;
    UniDBEdit8: TUniDBEdit;
    UniLabel9: TUniLabel;
    UniDBEdit9: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniDBEdit10: TUniDBEdit;
    UniLabel5: TUniLabel;
    UniDBEdit11: TUniDBEdit;
    UniLabel10: TUniLabel;
    UniDBEdit12: TUniDBEdit;
    UniLabel11: TUniLabel;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBMemo1: TUniDBMemo;
    UniLabel13: TUniLabel;
    pNif: TUniDBEdit;
    UniLabel14: TUniLabel;
    PanelCambiarCodigo: TUniPanel;
    edCodigoAntiguo: TUniEdit;
    edCodigoNuevo: TUniEdit;
    ProgressBar1: TUniProgressBar;
    rgTipoCambioCodigo: TUniRadioGroup;
    UniLabel23: TUniLabel;
    btEjecutarCambiarCodigo: TUniBitBtn;
    dsCabe: TDataSource;
    UniDBEdit29: TUniDBEdit;
    pCPostalE: TUniDBLookupComboBox;
    dsPoblacionS: TDataSource;
    UniDBEdit6: TUniDBEdit;
    dsTFacturacio: TDataSource;
    dsTPago: TDataSource;
    UniLabel24: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniLabel25: TUniLabel;
    UniDBEdit16: TUniDBEdit;
    UniLabel26: TUniLabel;
    UniDBEdit18: TUniDBEdit;
    UniLabel27: TUniLabel;
    UniDBEdit30: TUniDBEdit;
    UniLabel28: TUniLabel;
    UniDBEdit31: TUniDBEdit;
    edDiaSemanaCargo: TUniComboBox;
    edDiaSemanaAbono: TUniComboBox;
    PanelBajaArticulos: TUniPanel;
    UniLabel29: TUniLabel;
    UniBitBtn1: TUniBitBtn;
    edFechaBajaCompra: TUniDBDateTimePicker;
    UniLabel30: TUniLabel;
    UniLabel31: TUniLabel;
    edFechaBajanueva: TUniDBDateTimePicker;
    pnlBts: TUniPanel;
    UniBitBtn3: TUniBitBtn;
    btEditarDistribuidora: TUniBitBtn;
    btGrabarDistribuidora: TUniBitBtn;
    btCancelarDistribuidora: TUniBitBtn;
    btBorrarDistribuidora: TUniBitBtn;
    UniBitBtn12: TUniBitBtn;
    UniBitBtn13: TUniBitBtn;
    UniBitBtn14: TUniBitBtn;
    btNuevaDistribuidora: TUniBitBtn;
    btAnterior: TUniBitBtn;
    btSiguiente: TUniBitBtn;
    procedure btCambiarCodigoClick(Sender: TObject);
    procedure btEjecutarCambiarCodigoClick(Sender: TObject);
    procedure pCPostalECloseUp(Sender: TObject);
    procedure btGrabarDistribuidoraClick(Sender: TObject);
    procedure btCancelarDistribuidoraClick(Sender: TObject);
    procedure btNuevaDistribuidoraClick(Sender: TObject);
    procedure btEditarDistribuidoraClick(Sender: TObject);
    procedure btBorrarDistribuidoraClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btSiguienteClick(Sender: TObject);
    procedure edDiaSemanaCargoCloseUp(Sender: TObject);
    procedure edDiaSemanaAbonoCloseUp(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swNuevo: Boolean;//contorlar si entra a crear/editar un registro

    procedure RutHabilitarBTs(vTipo: Boolean);

  end;

function FormDistribuidoraFicha: TFormDistribuidoraFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule, uMensajes
  ,uMenu, uDMDistribuidora, uDistribuidoraMenu, uDistribuidoraLista,
  uDevolLista;

function FormDistribuidoraFicha: TFormDistribuidoraFicha;
begin
  Result := TFormDistribuidoraFicha(DMppal.GetFormInstance(TFormDistribuidoraFicha));

end;


procedure TFormDistribuidoraFicha.btCambiarCodigoClick(Sender: TObject);
begin
  inherited;
  {if PanelCambiarCodigo.Visible then
  begin
    PanelCambiarCodigo.Visible := False;
    exit;
  end;
  edCodigoAntiguo.Text := DMDistribuidora.sqlCabeSID_PROVEEDOR.AsString;

  with PanelCambiarCodigo do
  begin
    Visible := not Visible;

    if visible then
    begin
      Top := btCambiarCodigo.Top;
      Left:= btCambiarCodigo.Left;// + btCambiarCodigo.Width;

      BringToFront;
      btEjecutarCambiarCodigo.Enabled := True;
      edCodigoNuevo.Text := '';
      edCodigoNuevo.SetFocus;
    end;
  end;
  }
end;

procedure TFormDistribuidoraFicha.btAnteriorClick(Sender: TObject);
begin
  DMDistribuidora.sqlCabeS.Prior;
  FormDistribuidoraLista.RutFichaClick(DMDistribuidora.sqlCabeSID_PROVEEDOR.AsInteger);
end;

procedure TFormDistribuidoraFicha.btBorrarDistribuidoraClick(Sender: TObject);
begin
//Consulta a hisArti y Articulos si esta distri tiene algo no se puede borrar
  if not DMDistribuidora.RutComprobarRegistros(DMDistribuidora.sqlCabeID_PROVEEDOR.AsInteger) then
  begin
    FormMensajes.RutInicioForm(1, UpperCase(self.Name), 'ACEPTARCANC', 'Desea borrar la Distribuidora?');
    FormMensajes.ShowModal();
  end
  else
  begin
    FormMensajes.RutInicioForm(2, UpperCase(self.Name), 'ACEPTAR', 'No puede borrar la Distribuidora, ya que tiene Articulos o Historial de Recepcion/Devolucion');
    FormMensajes.ShowModal();
  end;

end;

procedure TFormDistribuidoraFicha.btNuevaDistribuidoraClick(Sender: TObject);
begin
  FormDistribuidoraLista.btNuevaDistribuidoraClick(nil);
end;

procedure TFormDistribuidoraFicha.btSiguienteClick(Sender: TObject);
begin
  DMDistribuidora.sqlCabeS.Next;
  FormDistribuidoraLista.RutFichaClick(DMDistribuidora.sqlCabeSID_PROVEEDOR.AsInteger);
end;

procedure TFormDistribuidoraFicha.edDiaSemanaAbonoCloseUp(Sender: TObject);
begin
  if btEditarDistribuidora.Enabled = False then
  begin
    DMDistribuidora.sqlCabe.Edit;
    DMDistribuidora.sqlCabeDIAFIJO2.AsInteger := edDiaSemanaAbono.ItemIndex + 1;
  end;
end;

procedure TFormDistribuidoraFicha.edDiaSemanaCargoCloseUp(Sender: TObject);
begin
  if btEditarDistribuidora.Enabled = False then
  begin
    DMDistribuidora.sqlCabe.Edit;
    DMDistribuidora.sqlCabeDIAFIJO1.AsInteger := edDiaSemanaCargo.ItemIndex + 1;
  end;
end;

procedure TFormDistribuidoraFicha.btGrabarDistribuidoraClick(Sender: TObject);
begin
  //DMDistribuidora.sqlCabeTPROVEEDOR.AsInteger := 1;  //movido al dm

  if DMDistribuidora.sqlCabe.State <> dsBrowse then
  begin
    DMDistribuidora.sqlCabe.Post;
    DMDistribuidora.sqlCabeS.Refresh;
  end;
  RutHabilitarBTs(False);
end;

procedure TFormDistribuidoraFicha.btCancelarDistribuidoraClick(Sender: TObject);
begin
  DMDistribuidora.sqlCabe.Cancel;
 // if swNuevo then FormDistribuidoraMenu.RutAbrirListaDistribuidora;

  RutHabilitarBTs(False);
end;

procedure TFormDistribuidoraFicha.btEjecutarCambiarCodigoClick(Sender: TObject);
begin
  PanelCambiarCodigo.Visible := False;
end;

procedure TFormDistribuidoraFicha.pCPostalECloseUp(Sender: TObject);
begin
  if DMDistribuidora.sqlCabe.State = dsEdit then
     DMDistribuidora.sqlCabePOBLACION.AsString := DMDistribuidora.sqlPoblacionSPOBLACION.AsString;
  {if swNuevo then
  begin
    DMDistribuidora.sqlCabe.Edit;
    DMDistribuidora.sqlCabePOBLACION.AsString := DMDistribuidora.sqlPoblacionSPOBLACION.AsString;
    DMDistribuidora.sqlCabe.Post;
  end;}
end;

procedure TFormDistribuidoraFicha.btEditarDistribuidoraClick(Sender: TObject);
begin
  RutHabilitarBTs(True);
  DMDistribuidora.sqlCabe.Edit;
end;

procedure TFormDistribuidoraFicha.RutHabilitarBTs(vTipo : Boolean);
begin
  btEditarDistribuidora.Enabled   := not vTipo;
  btGrabarDistribuidora.Visible   := vTipo;
  btCancelarDistribuidora.Visible := vTipo;

  edDiaSemanaCargo.Enabled := vTipo;
  edDiaSemanaAbono.Enabled := vTipo;
end;


end.

