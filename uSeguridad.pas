unit uSeguridad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniDBText, uniButton, uniBitBtn, uniRadioGroup,
  uniDBRadioGroup, uniDateTimePicker, uniDBDateTimePicker, uniLabel, uniEdit,
  uniDBEdit, uniPanel, uniPageControl, uniGUIBaseClasses;

type
  TUniForm1 = class(TUniForm)
    UniPageControl1: TUniPageControl;
    tabDevolucion: TUniTabSheet;
    UniPanel1: TUniPanel;
    UniPanel2: TUniPanel;
    UniDBEdit1: TUniDBEdit;
    UniLabel1: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniPanel3: TUniPanel;
    pFechaPreDevolu: TUniDBDateTimePicker;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    rgOrdenado: TUniDBRadioGroup;
    fcPanel3: TUniPanel;
    UniLabel5: TUniLabel;
    UniDBEdit5: TUniDBEdit;
    Rgpaquetes: TUniDBRadioGroup;
    UniPanel4: TUniPanel;
    UniLabel6: TUniLabel;
    UniDBEdit6: TUniDBEdit;
    UniBitBtn1: TUniBitBtn;
    UniPanel16: TUniPanel;
    UniBitBtn13: TUniBitBtn;
    pMargen1: TUniDBEdit;
    pMargen2: TUniDBEdit;
    pEncarte2: TUniDBEdit;
    pEncarte1: TUniDBEdit;
    UniLabel41: TUniLabel;
    UniLabel42: TUniLabel;
    pTIva1: TUniDBEdit;
    pTIva2: TUniDBEdit;
    UniDBEdit47: TUniDBEdit;
    UniDBEdit48: TUniDBEdit;
    UniDBEdit49: TUniDBEdit;
    UniDBEdit50: TUniDBEdit;
    UniLabel43: TUniLabel;
    UniLabel44: TUniLabel;
    UniLabel45: TUniLabel;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel46: TUniLabel;
    UniLabel47: TUniLabel;
    UniPanel17: TUniPanel;
    UniDBEdit55: TUniDBEdit;
    UniDBEdit56: TUniDBEdit;
    UniDBEdit57: TUniDBEdit;
    UniDBEdit58: TUniDBEdit;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniDBEdit59: TUniDBEdit;
    UniDBEdit60: TUniDBEdit;
    UniDBEdit61: TUniDBEdit;
    UniDBEdit62: TUniDBEdit;
    UniLabel50: TUniLabel;
    UniLabel51: TUniLabel;
    UniLabel52: TUniLabel;
    UniLabel53: TUniLabel;
    UniLabel54: TUniLabel;
    UniDBText5: TUniDBText;
    UniDBText6: TUniDBText;
    UniDBText7: TUniDBText;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniDBText8: TUniDBText;
    UniDBText9: TUniDBText;
    BtnNouAdendum: TUniBitBtn;
    BtnVerVtaAuto: TUniBitBtn;
    btVerDirectoDevolucion: TUniBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function UniForm1: TUniForm1;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function UniForm1: TUniForm1;
begin
  Result := TUniForm1(UniMainModule.GetFormInstance(TUniForm1));
end;

end.
