unit uDMRecepcion;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, frxGradient, frxDesgn, frxClass, frxDBSet, frxExportPDF,
  Variants;

type
  TDMMenuRecepcion = class(TDataModule)
    sqlListaRecepcion: TFDQuery;
    sqlListaRecepcionID_PROVEEDOR: TIntegerField;
    sqlListaRecepcionFECHA: TDateField;
    sqlListaRecepcionFECHACARGO: TDateField;
    sqlListaRecepcionSWDOCTOPROVE: TSmallintField;
    sqlListaRecepcionDOCTOPROVE: TStringField;
    sqlListaRecepcionDOCTOPROVEFECHA: TDateField;
    sqlListaRecepcionNOMBRE: TStringField;
    sqlListaRecepcionNIF: TStringField;
    sqlListaRecepcionID_DIRECCION: TIntegerField;
    sqlListaRecepcionDIRECCION: TStringField;
    sqlListaRecepcionPOBLACION: TStringField;
    sqlListaRecepcionPROVINCIA: TStringField;
    sqlListaRecepcionCPOSTAL: TStringField;
    sqlListaRecepcionCPROVINCIA: TStringField;
    sqlListaRecepcionTELEFONO1: TStringField;
    sqlListaRecepcionTELEFONO2: TStringField;
    sqlListaRecepcionMOVIL: TStringField;
    sqlListaRecepcionSEMANA: TSmallintField;
    sqlListaRecepcionCANTIDAD: TFloatField;
    sqlListaRecepcionCANTIENALBA: TFloatField;
    sqlListaRecepcionVALVENTA: TFloatField;
    sqlListaRecepcionIMPOIVA: TFloatField;
    sqlListaRecepcionIMPORE: TFloatField;
    sqlListaRecepcionVALCOSTE: TFloatField;
    sqlListaRecepcionVALCOSTERE: TFloatField;
    sqlListaRecepcionVALVENTA2: TFloatField;
    sqlListaRecepcionIMPOIVA2: TFloatField;
    sqlListaRecepcionIMPORE2: TFloatField;
    sqlListaRecepcionVALCOSTE2: TFloatField;
    sqlListaRecepcionVALCOSTERE2: TFloatField;
    sqlListaRecepcionIMPOTOTLIN: TFloatField;
    sqlListaRecepcionIMPOBRUTO: TFloatField;
    sqlListaRecepcionIMPODTO: TFloatField;
    sqlListaRecepcionIMPOBASE: TFloatField;
    sqlListaRecepcionNRECLA1: TIntegerField;
    sqlListaRecepcionNRECLA0: TIntegerField;
    sqlListaRecepcionIDSTOCABE: TIntegerField;
    sqlListaRecepcionCosteTotal: TFloatField;
    sqlHisArti: TFDQuery;
    sqlHisArtiID_HISARTI: TIntegerField;
    sqlHisArtiSWES: TStringField;
    sqlHisArtiFECHA: TDateField;
    sqlHisArtiHORA: TTimeField;
    sqlHisArtiCLAVE: TStringField;
    sqlHisArtiID_ARTICULO: TIntegerField;
    sqlHisArtiADENDUM: TStringField;
    sqlHisArtiID_PROVEEDOR: TIntegerField;
    sqlHisArtiREFEPROVE: TStringField;
    sqlHisArtiIDSTOCABE: TIntegerField;
    sqlHisArtiIDDEVOLUCABE: TIntegerField;
    sqlHisArtiPAQUETE: TIntegerField;
    sqlHisArtiID_HISARTI01: TIntegerField;
    sqlHisArtiID_DIARIA: TIntegerField;
    sqlHisArtiPARTIDA: TIntegerField;
    sqlHisArtiNRECLAMACION: TIntegerField;
    sqlHisArtiID_FRAPROVE: TIntegerField;
    sqlHisArtiID_CLIENTE: TIntegerField;
    sqlHisArtiSWTIPOFRA: TIntegerField;
    sqlHisArtiNFACTURA: TIntegerField;
    sqlHisArtiNALBARAN: TIntegerField;
    sqlHisArtiNLINEA: TIntegerField;
    sqlHisArtiDEVUELTOS: TSingleField;
    sqlHisArtiVENDIDOS: TSingleField;
    sqlHisArtiMERMA: TSingleField;
    sqlHisArtiCARGO: TSingleField;
    sqlHisArtiABONO: TSingleField;
    sqlHisArtiRECLAMADO: TSingleField;
    sqlHisArtiCANTIDAD: TSingleField;
    sqlHisArtiCANTIENALBA: TSingleField;
    sqlHisArtiCANTISUSCRI: TSingleField;
    sqlHisArtiPRECIOCOMPRA: TSingleField;
    sqlHisArtiPRECIOCOMPRA2: TSingleField;
    sqlHisArtiPRECIOCOSTE: TSingleField;
    sqlHisArtiPRECIOCOSTE2: TSingleField;
    sqlHisArtiPRECIOCOSTETOT: TSingleField;
    sqlHisArtiPRECIOCOSTETOT2: TSingleField;
    sqlHisArtiPRECIOVENTA: TSingleField;
    sqlHisArtiPRECIOVENTA2: TSingleField;
    sqlHisArtiSWDTO: TSmallintField;
    sqlHisArtiTPCDTO: TSingleField;
    sqlHisArtiTIVA: TSmallintField;
    sqlHisArtiTPCIVA: TSingleField;
    sqlHisArtiTPCRE: TSingleField;
    sqlHisArtiTIVA2: TSmallintField;
    sqlHisArtiTPCIVA2: TSingleField;
    sqlHisArtiTPCRE2: TSingleField;
    sqlHisArtiIMPOBRUTO: TSingleField;
    sqlHisArtiIMPODTO: TSingleField;
    sqlHisArtiIMPOBASE: TSingleField;
    sqlHisArtiIMPOIVA: TSingleField;
    sqlHisArtiIMPORE: TSingleField;
    sqlHisArtiIMPOBASE2: TSingleField;
    sqlHisArtiIMPOIVA2: TSingleField;
    sqlHisArtiIMPORE2: TSingleField;
    sqlHisArtiIMPOTOTLIN: TSingleField;
    sqlHisArtiENTRADAS: TSingleField;
    sqlHisArtiSALIDAS: TSingleField;
    sqlHisArtiVALORCOSTE: TSingleField;
    sqlHisArtiVALORCOSTE2: TSingleField;
    sqlHisArtiVALORCOSTETOT: TSingleField;
    sqlHisArtiVALORCOSTETOT2: TSingleField;
    sqlHisArtiVALORCOMPRA: TSingleField;
    sqlHisArtiVALORCOMPRA2: TSingleField;
    sqlHisArtiVALORVENTA: TSingleField;
    sqlHisArtiVALORVENTA2: TSingleField;
    sqlHisArtiVALORMOVI: TSingleField;
    sqlHisArtiTPCCOMISION: TSingleField;
    sqlHisArtiIMPOCOMISION: TSingleField;
    sqlHisArtiMARGEN: TSingleField;
    sqlHisArtiMARGEN2: TSingleField;
    sqlHisArtiENCARTE: TSingleField;
    sqlHisArtiENCARTE2: TSingleField;
    sqlHisArtiTURNO: TSmallintField;
    sqlHisArtiNALMACEN: TIntegerField;
    sqlHisArtiSWALTABAJA: TSmallintField;
    sqlHisArtiSWPDTEPAGO: TSmallintField;
    sqlHisArtiSWESTADO: TSmallintField;
    sqlHisArtiSWDEVOLUCION: TSmallintField;
    sqlHisArtiSWCERRADO: TSmallintField;
    sqlHisArtiSWMARCA: TSmallintField;
    sqlHisArtiSWMARCA2: TSmallintField;
    sqlHisArtiSWMARCA3: TSmallintField;
    sqlHisArtiSWMARCA4: TSmallintField;
    sqlHisArtiSWMARCA5: TSmallintField;
    sqlHisArtiSEMANA: TSmallintField;
    sqlHisArtiTIPOVENTA: TSmallintField;
    sqlHisArtiFECHAAVISO: TDateField;
    sqlHisArtiFECHADEVOL: TDateField;
    sqlHisArtiFECHACARGO: TDateField;
    sqlHisArtiFECHAABONO: TDateField;
    sqlHisArtiSWRECLAMA: TSmallintField;
    sqlHisArtiID_HISARTIRE: TIntegerField;
    sqlHisArtiSWACABADO: TSmallintField;
    sqlHisArtiBARRAS: TStringField;
    sqlHisArtiDESCRIPCION: TStringField;
    sqlHisArtiTBARRAS: TIntegerField;
    sqlListaRecepcion1: TFDQuery;
    sqlListaRecepcion1IDSTOCABE: TIntegerField;
    sqlListaRecepcion1SWTIPODOCU: TSmallintField;
    sqlListaRecepcion1NDOCSTOCK: TIntegerField;
    sqlListaRecepcion1FECHA: TDateField;
    sqlListaRecepcion1NALMACEN: TIntegerField;
    sqlListaRecepcion1NALMACENORIGEN: TIntegerField;
    sqlListaRecepcion1NALMACENDESTINO: TIntegerField;
    sqlListaRecepcion1ID_PROVEEDOR: TIntegerField;
    sqlListaRecepcion1ID_DIRECCION: TIntegerField;
    sqlListaRecepcion1FABRICANTE: TStringField;
    sqlListaRecepcion1SWDOCTOPROVE: TSmallintField;
    sqlListaRecepcion1DOCTOPROVE: TStringField;
    sqlListaRecepcion1DOCTOPROVEFECHA: TDateField;
    sqlListaRecepcion1FECHACARGO: TDateField;
    sqlListaRecepcion1FECHAABONO: TDateField;
    sqlListaRecepcion1NOMBRE: TStringField;
    sqlListaRecepcion1NIF: TStringField;
    sqlListaRecepcion1MENSAJEAVISO: TStringField;
    sqlListaRecepcion1OBSERVACIONES: TMemoField;
    sqlListaRecepcion1PAQUETE: TIntegerField;
    sqlListaRecepcion1SWDEVOLUCION: TSmallintField;
    sqlListaRecepcion1SWCERRADO: TSmallintField;
    sqlListaRecepcion1SWFRATIENDA: TSmallintField;
    sqlListaRecepcion1DEVOLUSWTIPOINC: TSmallintField;
    sqlListaRecepcion1DEVOLUFECHASEL: TDateField;
    sqlListaRecepcion1DEVOLUDOCTOPROVE: TStringField;
    sqlListaRecepcion1DEVOLUIDSTOCABE: TIntegerField;
    sqlListaRecepcion1ID_FRAPROVE: TIntegerField;
    sqlListaRecepcion1SWMARCA: TSmallintField;
    sqlListaRecepcion1SWMARCA2: TSmallintField;
    sqlListaRecepcion1SWMARCA3: TSmallintField;
    sqlListaRecepcion1SWMARCA4: TSmallintField;
    sqlListaRecepcion1SWMARCA5: TSmallintField;
    sqlListaRecepcion1SEMANA: TSmallintField;
    sqlListaRecepcion1SWALTABAJA: TSmallintField;
    sqlListaRecepcion1FECHABAJA: TDateField;
    sqlListaRecepcion1FECHAALTA: TDateField;
    sqlListaRecepcion1HORAALTA: TTimeField;
    sqlListaRecepcion1FECHAULTI: TDateField;
    sqlListaRecepcion1HORAULTI: TTimeField;
    sqlListaRecepcion1USUULTI: TStringField;
    sqlListaRecepcion1NOTAULTI: TStringField;
    sqlReservasRecepcion: TFDQuery;
    sqlReservasRecepcionID_ARTICULO: TIntegerField;
    sqlReservasRecepcionBARRAS18: TStringField;
    sqlReservasRecepcionCANTIRESERVA: TSingleField;
    sqlReservasRecepcionCANTIRECI: TSingleField;
    sqlReservasRecepcionDescripcion: TStringField;
    sqlUltiCompra: TFDQuery;
    sqlHisArtiTotResumen: TFDQuery;
    sqlCabe1: TFDQuery;
    sqlCabe1IDSTOCABE: TIntegerField;
    sqlCabe1SWTIPODOCU: TSmallintField;
    sqlCabe1NDOCSTOCK: TIntegerField;
    sqlCabe1FECHA: TDateField;
    sqlCabe1NALMACEN: TIntegerField;
    sqlCabe1NALMACENORIGEN: TIntegerField;
    sqlCabe1NALMACENDESTINO: TIntegerField;
    sqlCabe1ID_PROVEEDOR: TIntegerField;
    sqlCabe1ID_DIRECCION: TIntegerField;
    sqlCabe1FABRICANTE: TStringField;
    sqlCabe1SWDOCTOPROVE: TSmallintField;
    sqlCabe1DOCTOPROVE: TStringField;
    sqlCabe1DOCTOPROVEFECHA: TDateField;
    sqlCabe1FECHACARGO: TDateField;
    sqlCabe1FECHAABONO: TDateField;
    sqlCabe1NOMBRE: TStringField;
    sqlCabe1NIF: TStringField;
    sqlCabe1MENSAJEAVISO: TStringField;
    sqlCabe1OBSERVACIONES: TMemoField;
    sqlCabe1PAQUETE: TIntegerField;
    sqlCabe1SWDEVOLUCION: TSmallintField;
    sqlCabe1SWCERRADO: TSmallintField;
    sqlCabe1SWFRATIENDA: TSmallintField;
    sqlCabe1DEVOLUSWTIPOINC: TSmallintField;
    sqlCabe1DEVOLUFECHASEL: TDateField;
    sqlCabe1DEVOLUDOCTOPROVE: TStringField;
    sqlCabe1DEVOLUIDSTOCABE: TIntegerField;
    sqlCabe1ID_FRAPROVE: TIntegerField;
    sqlCabe1SWMARCA: TSmallintField;
    sqlCabe1SWMARCA2: TSmallintField;
    sqlCabe1SWMARCA3: TSmallintField;
    sqlCabe1SWMARCA4: TSmallintField;
    sqlCabe1SWMARCA5: TSmallintField;
    sqlCabe1SEMANA: TSmallintField;
    sqlCabe1SWALTABAJA: TSmallintField;
    sqlCabe1FECHABAJA: TDateField;
    sqlCabe1FECHAALTA: TDateField;
    sqlCabe1HORAALTA: TTimeField;
    sqlCabe1FECHAULTI: TDateField;
    sqlCabe1HORAULTI: TTimeField;
    sqlCabe1USUULTI: TStringField;
    sqlCabe1NOTAULTI: TStringField;
    sqlAntiguos: TFDQuery;
    sqlAntiguosID_PROVEEDOR: TIntegerField;
    sqlAntiguosFECHA: TDateField;
    sqlAntiguosIDSTOCABE: TIntegerField;
    sqlAntiguosLines: TFDQuery;
    sqlAntiguosLinesID_HISARTI: TIntegerField;
    sqlAntiguosLinesSWES: TStringField;
    sqlAntiguosLinesFECHA: TDateField;
    sqlAntiguosLinesHORA: TTimeField;
    sqlAntiguosLinesCLAVE: TStringField;
    sqlAntiguosLinesID_ARTICULO: TIntegerField;
    sqlAntiguosLinesADENDUM: TStringField;
    sqlAntiguosLinesID_PROVEEDOR: TIntegerField;
    sqlAntiguosLinesREFEPROVE: TStringField;
    sqlAntiguosLinesIDSTOCABE: TIntegerField;
    sqlAntiguosLinesIDDEVOLUCABE: TIntegerField;
    sqlAntiguosLinesPAQUETE: TIntegerField;
    sqlAntiguosLinesID_HISARTI01: TIntegerField;
    sqlAntiguosLinesID_DIARIA: TIntegerField;
    sqlAntiguosLinesPARTIDA: TIntegerField;
    sqlAntiguosLinesNRECLAMACION: TIntegerField;
    sqlAntiguosLinesID_FRAPROVE: TIntegerField;
    sqlAntiguosLinesID_CLIENTE: TIntegerField;
    sqlAntiguosLinesSWTIPOFRA: TIntegerField;
    sqlAntiguosLinesNFACTURA: TIntegerField;
    sqlAntiguosLinesNALBARAN: TIntegerField;
    sqlAntiguosLinesNLINEA: TIntegerField;
    sqlAntiguosLinesDEVUELTOS: TSingleField;
    sqlAntiguosLinesVENDIDOS: TSingleField;
    sqlAntiguosLinesMERMA: TSingleField;
    sqlAntiguosLinesCARGO: TSingleField;
    sqlAntiguosLinesABONO: TSingleField;
    sqlAntiguosLinesRECLAMADO: TSingleField;
    sqlAntiguosLinesCANTIDAD: TSingleField;
    sqlAntiguosLinesCANTIENALBA: TSingleField;
    sqlAntiguosLinesCANTISUSCRI: TSingleField;
    sqlAntiguosLinesPRECIOCOMPRA: TSingleField;
    sqlAntiguosLinesPRECIOCOMPRA2: TSingleField;
    sqlAntiguosLinesPRECIOCOSTE: TSingleField;
    sqlAntiguosLinesPRECIOCOSTE2: TSingleField;
    sqlAntiguosLinesPRECIOCOSTETOT: TSingleField;
    sqlAntiguosLinesPRECIOCOSTETOT2: TSingleField;
    sqlAntiguosLinesPRECIOVENTA: TSingleField;
    sqlAntiguosLinesPRECIOVENTA2: TSingleField;
    sqlAntiguosLinesSWDTO: TSmallintField;
    sqlAntiguosLinesTPCDTO: TSingleField;
    sqlAntiguosLinesTIVA: TSmallintField;
    sqlAntiguosLinesTPCIVA: TSingleField;
    sqlAntiguosLinesTPCRE: TSingleField;
    sqlAntiguosLinesTIVA2: TSmallintField;
    sqlAntiguosLinesTPCIVA2: TSingleField;
    sqlAntiguosLinesTPCRE2: TSingleField;
    sqlAntiguosLinesIMPOBRUTO: TSingleField;
    sqlAntiguosLinesIMPODTO: TSingleField;
    sqlAntiguosLinesIMPOBASE: TSingleField;
    sqlAntiguosLinesIMPOIVA: TSingleField;
    sqlAntiguosLinesIMPORE: TSingleField;
    sqlAntiguosLinesIMPOBASE2: TSingleField;
    sqlAntiguosLinesIMPOIVA2: TSingleField;
    sqlAntiguosLinesIMPORE2: TSingleField;
    sqlAntiguosLinesIMPOTOTLIN: TSingleField;
    sqlAntiguosLinesENTRADAS: TSingleField;
    sqlAntiguosLinesSALIDAS: TSingleField;
    sqlAntiguosLinesVALORCOSTE: TSingleField;
    sqlAntiguosLinesVALORCOSTE2: TSingleField;
    sqlAntiguosLinesVALORCOSTETOT: TSingleField;
    sqlAntiguosLinesVALORCOSTETOT2: TSingleField;
    sqlAntiguosLinesVALORCOMPRA: TSingleField;
    sqlAntiguosLinesVALORCOMPRA2: TSingleField;
    sqlAntiguosLinesVALORVENTA: TSingleField;
    sqlAntiguosLinesVALORVENTA2: TSingleField;
    sqlAntiguosLinesVALORMOVI: TSingleField;
    sqlAntiguosLinesTPCCOMISION: TSingleField;
    sqlAntiguosLinesIMPOCOMISION: TSingleField;
    sqlAntiguosLinesMARGEN: TSingleField;
    sqlAntiguosLinesMARGEN2: TSingleField;
    sqlAntiguosLinesENCARTE: TSingleField;
    sqlAntiguosLinesENCARTE2: TSingleField;
    sqlAntiguosLinesTURNO: TSmallintField;
    sqlAntiguosLinesNALMACEN: TIntegerField;
    sqlAntiguosLinesSWALTABAJA: TSmallintField;
    sqlAntiguosLinesSWPDTEPAGO: TSmallintField;
    sqlAntiguosLinesSWESTADO: TSmallintField;
    sqlAntiguosLinesSWDEVOLUCION: TSmallintField;
    sqlAntiguosLinesSWCERRADO: TSmallintField;
    sqlAntiguosLinesSWMARCA: TSmallintField;
    sqlAntiguosLinesSWMARCA2: TSmallintField;
    sqlAntiguosLinesSWMARCA3: TSmallintField;
    sqlAntiguosLinesSWMARCA4: TSmallintField;
    sqlAntiguosLinesSWMARCA5: TSmallintField;
    sqlAntiguosLinesSEMANA: TSmallintField;
    sqlAntiguosLinesTIPOVENTA: TSmallintField;
    sqlAntiguosLinesFECHAAVISO: TDateField;
    sqlAntiguosLinesFECHADEVOL: TDateField;
    sqlAntiguosLinesFECHACARGO: TDateField;
    sqlAntiguosLinesFECHAABONO: TDateField;
    sqlAntiguosLinesSWRECLAMA: TSmallintField;
    sqlAntiguosLinesID_HISARTIRE: TIntegerField;
    sqlAntiguosLinesSWACABADO: TSmallintField;
    sqlAntiguosLinesBARRAS: TStringField;
    sqlAntiguosLinesDESCRIPCION: TStringField;
    sqlAntiguosLinesTBARRAS: TIntegerField;
    sqlProveS: TFDQuery;
    sqlProveSID_PROVEEDOR: TIntegerField;
    sqlProveSNOMBRE: TStringField;
    sqlProveSDIRECCION: TStringField;
    sqlProveSPOBLACION: TStringField;
    sqlProveSPROVINCIA: TStringField;
    sqlProveSCPOSTAL: TStringField;
    sqlProveSCPROVINCIA: TStringField;
    sqlProveSNIF: TStringField;
    sqlProveSTELEFONO1: TStringField;
    sqlProveSTELEFONO2: TStringField;
    sqlProveSMOVIL: TStringField;
    sqlHisArtiCosteTotal: TFloatField;
    sqlHisArtiPrecioTotal: TFloatField;
    sqlHisArtiTotResumenFECHACARGO: TDateField;
    sqlHisArtiTotResumenCANTIDAD: TFloatField;
    sqlHisArtiTotResumenCANTIENALBA: TFloatField;
    sqlHisArtiTotResumenIMPOBRUTO: TFloatField;
    sqlHisArtiTotResumenIMPODTO: TFloatField;
    sqlHisArtiTotResumenIMPOBASE: TFloatField;
    sqlHisArtiTotResumenIMPOIVA: TFloatField;
    sqlHisArtiTotResumenIMPORE: TFloatField;
    sqlHisArtiTotResumenIMPOTOTLIN: TFloatField;
    sqlHisArtiTotResumenVALORCOSTE: TFloatField;
    sqlHisArtiTotResumenVALORCOMPRA: TFloatField;
    sqlHisArtiTotResumenVALORVENTA: TFloatField;
    sqlHisArtiTotResumenVALORMOVI: TFloatField;
    sqlHisArtiTotResumenIMPOCOMISION: TFloatField;
    sqlHisArtiTotResumenIMPOBASE2: TFloatField;
    sqlHisArtiTotResumenIMPOIVA2: TFloatField;
    sqlHisArtiTotResumenIMPORE2: TFloatField;
    sqlHisArtiTotResumenVALORCOSTE2: TFloatField;
    sqlHisArtiTotResumenVALORCOMPRA2: TFloatField;
    sqlHisArtiTotResumenVALORVENTA2: TFloatField;
    sqlHisArtiTotResumenVALORCOSTETOT: TFloatField;
    sqlHisArtiTotResumenVALORCOSTETOTAL: TFloatField;
    sqlHisArtiTotResumenLINEAS: TIntegerField;
    sqlArticulos1: TFDQuery;
    sqlArticulos1ID_ARTICULO: TIntegerField;
    sqlArticulos1TBARRAS: TIntegerField;
    sqlArticulos1BARRAS: TStringField;
    sqlArticulos1ADENDUM: TStringField;
    sqlArticulos1FECHAADENDUM: TDateField;
    sqlArticulos1NADENDUM: TIntegerField;
    sqlArticulos1DESCRIPCION: TStringField;
    sqlArticulos1DESCRIPCION2: TStringField;
    sqlArticulos1IBS: TStringField;
    sqlArticulos1ISBN: TStringField;
    sqlArticulos1EDITOR: TIntegerField;
    sqlArticulos1PRECIO1: TSingleField;
    sqlArticulos1PRECIO2: TSingleField;
    sqlArticulos1PRECIO3: TSingleField;
    sqlArticulos1PRECIO4: TSingleField;
    sqlArticulos1PRECIO5: TSingleField;
    sqlArticulos1PRECIO6: TSingleField;
    sqlArticulos1PRECIO7: TSingleField;
    sqlArticulos1TPCENCARTE1: TSingleField;
    sqlArticulos1TPCENCARTE2: TSingleField;
    sqlArticulos1TPCENCARTE3: TSingleField;
    sqlArticulos1TPCENCARTE4: TSingleField;
    sqlArticulos1TPCENCARTE5: TSingleField;
    sqlArticulos1TPCENCARTE6: TSingleField;
    sqlArticulos1TPCENCARTE7: TSingleField;
    sqlArticulos1PRECIOCOSTE: TSingleField;
    sqlArticulos1PRECIOCOSTE2: TSingleField;
    sqlArticulos1PRECIOCOSTETOT: TSingleField;
    sqlArticulos1PRECIOCOSTETOT2: TSingleField;
    sqlArticulos1MARGEN: TSingleField;
    sqlArticulos1MARGEN2: TSingleField;
    sqlArticulos1MARGENMAXIMO: TSingleField;
    sqlArticulos1EDITORIAL: TIntegerField;
    sqlArticulos1REFEPROVE: TStringField;
    sqlArticulos1PERIODICIDAD: TIntegerField;
    sqlArticulos1CADUCIDAD: TIntegerField;
    sqlArticulos1TIVA: TIntegerField;
    sqlArticulos1TIVA2: TIntegerField;
    sqlArticulos1SWRECARGO: TSmallintField;
    sqlArticulos1SWACTIVADO: TSmallintField;
    sqlArticulos1SWINGRESO: TSmallintField;
    sqlArticulos1SWCONTROLFECHA: TSmallintField;
    sqlArticulos1SWTPRECIO: TSmallintField;
    sqlArticulos1SWORIGEN: TIntegerField;
    sqlArticulos1TIPO: TStringField;
    sqlArticulos1TPRODUCTO: TIntegerField;
    sqlArticulos1TCONTENIDO: TIntegerField;
    sqlArticulos1TEMATICA: TIntegerField;
    sqlArticulos1AUTOR: TIntegerField;
    sqlArticulos1OBSERVACIONES: TMemoField;
    sqlArticulos1STOCKMINIMO: TSingleField;
    sqlArticulos1STOCKMAXIMO: TSingleField;
    sqlArticulos1FAMILIA: TStringField;
    sqlArticulos1FAMILIAVENTA: TIntegerField;
    sqlArticulos1ORDENVENTA: TIntegerField;
    sqlArticulos1STOCKMINIMOESTANTE: TSingleField;
    sqlArticulos1STOCKMAXIMOESTANTE: TSingleField;
    sqlArticulos1STOCKVENTAESTANTE: TSingleField;
    sqlArticulos1REPONERESTANTE: TSingleField;
    sqlArticulos1UNIDADESCOMPRA: TSingleField;
    sqlArticulos1UNIDADESVENTA: TSingleField;
    sqlArticulos1IMAGEN: TStringField;
    sqlArticulos1COD_ARTICU: TStringField;
    sqlArticulos1GRUPO: TIntegerField;
    sqlArticulos1SUBGRUPO: TIntegerField;
    sqlArticulos1SWALTABAJA: TSmallintField;
    sqlArticulos1FECHABAJA: TDateField;
    sqlArticulos1FECHAALTA: TDateField;
    sqlArticulos1FECHAULTI: TDateField;
    sqlUltiCompraID_ARTICULO: TIntegerField;
    sqlUltiCompraADENDUM: TStringField;
    sqlUltiCompraFECHA: TDateField;
    sqlUltiCompraENTRADAS: TFloatField;
    sqlUltiCompraVENTAS: TFloatField;
    sqlUltiCompraDEVOLUCIONES: TFloatField;
    sqlUltiCompraMERMA: TFloatField;
    sqlUltiCompraSTOCK: TFloatField;
    sqlArti1: TFDQuery;
    sqlArti0: TFDQuery;
    sqlHisArtiCabe1: TFDQuery;
    sqlArti0ID_ARTICULO: TIntegerField;
    sqlArti0TBARRAS: TIntegerField;
    sqlArti0BARRAS: TStringField;
    sqlArti0ADENDUM: TStringField;
    sqlArti0FECHAADENDUM: TDateField;
    sqlArti0NADENDUM: TIntegerField;
    sqlArti0DESCRIPCION: TStringField;
    sqlArti0DESCRIPCION2: TStringField;
    sqlArti0IBS: TStringField;
    sqlArti0ISBN: TStringField;
    sqlArti0EDITOR: TIntegerField;
    sqlArti0PRECIO1: TSingleField;
    sqlArti0PRECIO2: TSingleField;
    sqlArti0PRECIO3: TSingleField;
    sqlArti0PRECIO4: TSingleField;
    sqlArti0PRECIO5: TSingleField;
    sqlArti0PRECIO6: TSingleField;
    sqlArti0PRECIO7: TSingleField;
    sqlArti0TPCENCARTE1: TSingleField;
    sqlArti0TPCENCARTE2: TSingleField;
    sqlArti0TPCENCARTE3: TSingleField;
    sqlArti0TPCENCARTE4: TSingleField;
    sqlArti0TPCENCARTE5: TSingleField;
    sqlArti0TPCENCARTE6: TSingleField;
    sqlArti0TPCENCARTE7: TSingleField;
    sqlArti0PRECIOCOSTE: TSingleField;
    sqlArti0PRECIOCOSTE2: TSingleField;
    sqlArti0PRECIOCOSTETOT: TSingleField;
    sqlArti0PRECIOCOSTETOT2: TSingleField;
    sqlArti0MARGEN: TSingleField;
    sqlArti0MARGEN2: TSingleField;
    sqlArti0MARGENMAXIMO: TSingleField;
    sqlArti0EDITORIAL: TIntegerField;
    sqlArti0REFEPROVE: TStringField;
    sqlArti0PERIODICIDAD: TIntegerField;
    sqlArti0CADUCIDAD: TIntegerField;
    sqlArti0TIVA: TIntegerField;
    sqlArti0TIVA2: TIntegerField;
    sqlArti0SWRECARGO: TSmallintField;
    sqlArti0SWACTIVADO: TSmallintField;
    sqlArti0SWINGRESO: TSmallintField;
    sqlArti0SWCONTROLFECHA: TSmallintField;
    sqlArti0SWTPRECIO: TSmallintField;
    sqlArti0SWORIGEN: TIntegerField;
    sqlArti0TIPO: TStringField;
    sqlArti0TPRODUCTO: TIntegerField;
    sqlArti0TCONTENIDO: TIntegerField;
    sqlArti0TEMATICA: TIntegerField;
    sqlArti0AUTOR: TIntegerField;
    sqlArti0OBSERVACIONES: TMemoField;
    sqlArti0STOCKMINIMO: TSingleField;
    sqlArti0STOCKMAXIMO: TSingleField;
    sqlArti0FAMILIA: TStringField;
    sqlArti0FAMILIAVENTA: TIntegerField;
    sqlArti0ORDENVENTA: TIntegerField;
    sqlArti0STOCKMINIMOESTANTE: TSingleField;
    sqlArti0STOCKMAXIMOESTANTE: TSingleField;
    sqlArti0STOCKVENTAESTANTE: TSingleField;
    sqlArti0REPONERESTANTE: TSingleField;
    sqlArti0UNIDADESCOMPRA: TSingleField;
    sqlArti0UNIDADESVENTA: TSingleField;
    sqlArti0IMAGEN: TStringField;
    sqlArti0COD_ARTICU: TStringField;
    sqlArti0GRUPO: TIntegerField;
    sqlArti0SUBGRUPO: TIntegerField;
    sqlArti0SWALTABAJA: TSmallintField;
    sqlArti0FECHABAJA: TDateField;
    sqlArti0FECHAALTA: TDateField;
    sqlArti0FECHAULTI: TDateField;
    sqlArti0NOMPROVE: TStringField;
    sqlArti1ID_ARTICULO: TIntegerField;
    sqlArti1TBARRAS: TIntegerField;
    sqlArti1BARRAS: TStringField;
    sqlArti1ADENDUM: TStringField;
    sqlArti1FECHAADENDUM: TDateField;
    sqlArti1NADENDUM: TIntegerField;
    sqlArti1DESCRIPCION: TStringField;
    sqlArti1DESCRIPCION2: TStringField;
    sqlArti1IBS: TStringField;
    sqlArti1ISBN: TStringField;
    sqlArti1EDITOR: TIntegerField;
    sqlArti1PRECIO1: TSingleField;
    sqlArti1PRECIO2: TSingleField;
    sqlArti1PRECIO3: TSingleField;
    sqlArti1PRECIO4: TSingleField;
    sqlArti1PRECIO5: TSingleField;
    sqlArti1PRECIO6: TSingleField;
    sqlArti1PRECIO7: TSingleField;
    sqlArti1TPCENCARTE1: TSingleField;
    sqlArti1TPCENCARTE2: TSingleField;
    sqlArti1TPCENCARTE3: TSingleField;
    sqlArti1TPCENCARTE4: TSingleField;
    sqlArti1TPCENCARTE5: TSingleField;
    sqlArti1TPCENCARTE6: TSingleField;
    sqlArti1TPCENCARTE7: TSingleField;
    sqlArti1PRECIOCOSTE: TSingleField;
    sqlArti1PRECIOCOSTE2: TSingleField;
    sqlArti1PRECIOCOSTETOT: TSingleField;
    sqlArti1PRECIOCOSTETOT2: TSingleField;
    sqlArti1MARGEN: TSingleField;
    sqlArti1MARGEN2: TSingleField;
    sqlArti1MARGENMAXIMO: TSingleField;
    sqlArti1EDITORIAL: TIntegerField;
    sqlArti1REFEPROVE: TStringField;
    sqlArti1PERIODICIDAD: TIntegerField;
    sqlArti1CADUCIDAD: TIntegerField;
    sqlArti1TIVA: TIntegerField;
    sqlArti1TIVA2: TIntegerField;
    sqlArti1SWRECARGO: TSmallintField;
    sqlArti1SWACTIVADO: TSmallintField;
    sqlArti1SWINGRESO: TSmallintField;
    sqlArti1SWCONTROLFECHA: TSmallintField;
    sqlArti1SWTPRECIO: TSmallintField;
    sqlArti1SWORIGEN: TIntegerField;
    sqlArti1TIPO: TStringField;
    sqlArti1TPRODUCTO: TIntegerField;
    sqlArti1TCONTENIDO: TIntegerField;
    sqlArti1TEMATICA: TIntegerField;
    sqlArti1AUTOR: TIntegerField;
    sqlArti1OBSERVACIONES: TMemoField;
    sqlArti1STOCKMINIMO: TSingleField;
    sqlArti1STOCKMAXIMO: TSingleField;
    sqlArti1FAMILIA: TStringField;
    sqlArti1FAMILIAVENTA: TIntegerField;
    sqlArti1ORDENVENTA: TIntegerField;
    sqlArti1STOCKMINIMOESTANTE: TSingleField;
    sqlArti1STOCKMAXIMOESTANTE: TSingleField;
    sqlArti1STOCKVENTAESTANTE: TSingleField;
    sqlArti1REPONERESTANTE: TSingleField;
    sqlArti1UNIDADESCOMPRA: TSingleField;
    sqlArti1UNIDADESVENTA: TSingleField;
    sqlArti1IMAGEN: TStringField;
    sqlArti1COD_ARTICU: TStringField;
    sqlArti1GRUPO: TIntegerField;
    sqlArti1SUBGRUPO: TIntegerField;
    sqlArti1SWALTABAJA: TSmallintField;
    sqlArti1FECHABAJA: TDateField;
    sqlArti1FECHAALTA: TDateField;
    sqlArti1FECHAULTI: TDateField;
    sqlArti1NOMPROVE: TStringField;
    sqlHisArtiCabe1IDSTOCABE: TIntegerField;
    sqlHisArtiCabe1SWTIPODOCU: TSmallintField;
    sqlHisArtiCabe1NDOCSTOCK: TIntegerField;
    sqlHisArtiCabe1FECHA: TDateField;
    sqlHisArtiCabe1NALMACEN: TIntegerField;
    sqlHisArtiCabe1NALMACENORIGEN: TIntegerField;
    sqlHisArtiCabe1NALMACENDESTINO: TIntegerField;
    sqlHisArtiCabe1ID_PROVEEDOR: TIntegerField;
    sqlHisArtiCabe1ID_DIRECCION: TIntegerField;
    sqlHisArtiCabe1FABRICANTE: TStringField;
    sqlHisArtiCabe1SWDOCTOPROVE: TSmallintField;
    sqlHisArtiCabe1DOCTOPROVE: TStringField;
    sqlHisArtiCabe1DOCTOPROVEFECHA: TDateField;
    sqlHisArtiCabe1FECHACARGO: TDateField;
    sqlHisArtiCabe1FECHAABONO: TDateField;
    sqlHisArtiCabe1NOMBRE: TStringField;
    sqlHisArtiCabe1NIF: TStringField;
    sqlHisArtiCabe1MENSAJEAVISO: TStringField;
    sqlHisArtiCabe1OBSERVACIONES: TMemoField;
    sqlHisArtiCabe1PAQUETE: TIntegerField;
    sqlHisArtiCabe1SWDEVOLUCION: TSmallintField;
    sqlHisArtiCabe1SWCERRADO: TSmallintField;
    sqlHisArtiCabe1SWFRATIENDA: TSmallintField;
    sqlHisArtiCabe1DEVOLUSWTIPOINC: TSmallintField;
    sqlHisArtiCabe1DEVOLUFECHASEL: TDateField;
    sqlHisArtiCabe1DEVOLUDOCTOPROVE: TStringField;
    sqlHisArtiCabe1DEVOLUIDSTOCABE: TIntegerField;
    sqlHisArtiCabe1ID_FRAPROVE: TIntegerField;
    sqlHisArtiCabe1SWMARCA: TSmallintField;
    sqlHisArtiCabe1SWMARCA2: TSmallintField;
    sqlHisArtiCabe1SWMARCA3: TSmallintField;
    sqlHisArtiCabe1SWMARCA4: TSmallintField;
    sqlHisArtiCabe1SWMARCA5: TSmallintField;
    sqlHisArtiCabe1SEMANA: TSmallintField;
    sqlHisArtiCabe1SWALTABAJA: TSmallintField;
    sqlHisArtiCabe1FECHABAJA: TDateField;
    sqlHisArtiCabe1FECHAALTA: TDateField;
    sqlHisArtiCabe1HORAALTA: TTimeField;
    sqlHisArtiCabe1FECHAULTI: TDateField;
    sqlHisArtiCabe1HORAULTI: TTimeField;
    sqlHisArtiCabe1USUULTI: TStringField;
    sqlHisArtiCabe1NOTAULTI: TStringField;
    sqlHisArtiCabe1MAXPAQUETE: TIntegerField;
    sqlHisArtiCabe1TOTALDEVOL: TFloatField;
    SP_GEN_HISARTI1: TFDStoredProc;
    procedure sqlHisArtiCalcFields(DataSet: TDataSet);
    procedure sqlListaRecepcionCalcFields(DataSet: TDataSet);
    procedure sqlCabe1AfterScroll(DataSet: TDataSet);

  private

    function RutExisteProveedor(vTabla: TFDQuery; vCampo: String): Boolean;
    procedure RutLeerArticulos;
    procedure RutPasarCampos(vTablaOrigen, vTablaDestino: TFDQuery; vTipo,
      vIDProve: Integer);






   // procedure ProCalcularUltiCompra;








    { Private declarations }
  public
    { Public declarations }
    wBeneficio, wBeneficio2 : Double;
    vID : Integer;
    vRecibido, vDevuelto : Double;
    vBarres, vAdendum : String;
    swRepetido : boolean;
  procedure RutLocateLinea(vID: Integer);
  procedure RutAbrirReservasRecepcion;
  procedure RutAbrirListaRecepcion(vDesde, vHasta :TDate; vSwCerrado : Integer);
  procedure RutAbrirCabe1(vIDCabe,vTipoDoc : String; vDesde,vHasta : TDate; vFiltroFecha : Boolean);
  procedure RutAbrirHisArti(vIdStocable: String);
  procedure RutAbrirArti(vIdArticulo: Integer);
  procedure ProSortidaPreus(zTipo: Integer);
  procedure ProCalcularPreuCost1;
  procedure RutLeerCodigoBarras;
  procedure ProCargarUltiCompra;
  procedure ProCalcularAdevolver;
  procedure RutLeerProveedor(vID: Integer);
    end;

function DMMenuRecepcion: TDMMenuRecepcion;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uRecepcionLista, uRecepcionAlbaran, uDMMenuArti, uConstVar,
  uEscogerArti;

function DMMenuRecepcion: TDMMenuRecepcion;
begin
  Result := TDMMenuRecepcion(DMppal.GetModuleInstance(TDMMenuRecepcion));
end;




procedure TDMMenuRecepcion.RutAbrirListaRecepcion(vDesde, vHasta :TDate; vSwCerrado : Integer);
var
  vStringFecha, vWhere : String;
begin

   if vSwCerrado = 0 then vWhere := ' ';
   if vSwCerrado = 1 then vWhere := ' and SWCERRADO = 0 ';
   if vSwCerrado = 2 then vWhere := ' and SWCERRADO = 1 ';

   vStringFecha := ' AND A.FECHA BETWEEN ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vDesde)) + ' AND ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vHasta));
  with sqlListaRecepcion do
  begin
    Close;
    sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE A.IDSTOCABE', Uppercase(SQL.Text))-1)
                 + 'WHERE A.IDSTOCABE is not null and A.SWTIPODOCU = 0'
                 +  vStringFecha
                 +  vWhere
                 + ' ORDER BY 1';
    Open;
  end;

  FormListaRecepcion.ProCalcularCabeceras;
end;

procedure TDMMenuRecepcion.RutLocateLinea(vID:Integer);
begin
 if sqlHisArti.RecordCount > 0 then

  sqlHisArti.Locate('ID_HISARTI', vID, [])
  else

end;


procedure TDMMenuRecepcion.RutPasarCampos(vTablaOrigen, vTablaDestino : TFDQuery; vTipo, vIDProve : Integer);
begin

  if vTipo >= 1 then
  begin
    vTablaDestino.FieldByName('ID_PROVEEDOR').AsInteger  := vIDProve;
    vTablaDestino.FieldByName('ID_ARTICULO').AsInteger   := vTablaOrigen.FieldByName('ID_ARTICULO').AsInteger;
    vTablaDestino.FieldByName('ADENDUM').AsString        := vAdendum;

    vTablaDestino.FieldByName('REFEPROVE').AsString      := vTablaOrigen.FieldByName('REFEPROVE').AsString;
    vTablaDestino.FieldByName('IDSTOCABE').AsInteger     := sqlCabe1IDSTOCABE.AsInteger;
    vTablaDestino.FieldByName('PRECIOCOSTE').AsFloat     := vTablaOrigen.FieldByName('PRECIOCOSTE').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTE2').AsFloat    := vTablaOrigen.FieldByName('PRECIOCOSTE2').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTETOT').AsFloat  := vTablaOrigen.FieldByName('PRECIOCOSTETOT').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTETOT2').AsFloat := vTablaOrigen.FieldByName('PRECIOCOSTETOT2').AsFloat;
    vTablaDestino.FieldByName('TIVA2').AsInteger         := vTablaOrigen.FieldByName('TIVA2').AsInteger;
    vTablaDestino.FieldByName('TIVA').AsInteger          := 0;
    vTablaDestino.FieldByName('MARGEN').AsFloat          := vTablaOrigen.FieldByName('MARGEN').AsFloat;
    vTablaDestino.FieldByName('MARGEN2').AsFloat         := vTablaOrigen.FieldByName('MARGEN2').AsFloat;
    vTablaDestino.FieldByName('PRECIOVENTA').AsFloat     := vTablaOrigen.FieldByName('PRECIO1').AsFloat;
    vTablaDestino.FieldByName('PRECIOVENTA2').AsFloat    := vTablaOrigen.FieldByName('PRECIO2').AsFloat;
   // vTablaDestino.FieldByName('FECHADEVOL').AsDateTime   := sqlCabe1FECHA.AsDateTime;
   // vTablaDestino.FieldByName('FECHAAVISO').AsDateTime   := vTablaDestino.FieldByName('FECHADEVOL').AsDateTime - 7;
    vTablaDestino.FieldByName('FECHA').AsDateTime        := now;
    vTablaDestino.FieldByName('HORA').AsDateTime         := now;
    vTablaDestino.FieldByName('SWES').AsString           := 'E';
    vTablaDestino.FieldByName('SWTIPOFRA').AsInteger     := 0;
    vTablaDestino.FieldByName('NFACTURA').AsInteger      := 0;
    vTablaDestino.FieldByName('NALBARAN').AsInteger      := 0;
  end;

  if vTipo >= 2 then
  begin


    //vTablaDestino.FieldByName('IDDEVOLUCABE').AsInteger  := vTablaOrigen.FieldByName('IDDEVOLUCABE').AsInteger;
    vTablaDestino.FieldByName('PAQUETE').AsInteger       := vTablaOrigen.FieldByName('PAQUETE').AsInteger;
    vTablaDestino.FieldByName('ID_HISARTI01').AsInteger  := vTablaOrigen.FieldByName('ID_HISARTI01').AsInteger;
    vTablaDestino.FieldByName('ID_DIARIA').AsInteger     := vTablaOrigen.FieldByName('ID_DIARIA').AsInteger;
    vTablaDestino.FieldByName('PARTIDA').AsInteger       := vTablaOrigen.FieldByName('PARTIDA').AsInteger;
    vTablaDestino.FieldByName('NRECLAMACION').AsInteger  := vTablaOrigen.FieldByName('NRECLAMACION').AsInteger;
    vTablaDestino.FieldByName('ID_FRAPROVE').AsInteger   := vTablaOrigen.FieldByName('ID_FRAPROVE').AsInteger;
    vTablaDestino.FieldByName('ID_CLIENTE').AsInteger    := vTablaOrigen.FieldByName('ID_CLIENTE').AsInteger;
    vTablaDestino.FieldByName('NLINEA').AsInteger        := 0;
    vTablaDestino.FieldByName('DEVUELTOS').AsFloat       := vTablaDestino.FieldByName('CANTIDAD').AsFloat;
    vTablaDestino.FieldByName('VENDIDOS').AsFloat        := vTablaOrigen.FieldByName('VENDIDOS').AsFloat;
    vTablaDestino.FieldByName('MERMA').AsFloat           := vTablaOrigen.FieldByName('MERMA').AsFloat;
    vTablaDestino.FieldByName('CARGO').AsFloat           := vTablaOrigen.FieldByName('CARGO').AsFloat;
    vTablaDestino.FieldByName('ABONO').AsFloat           := vTablaOrigen.FieldByName('ABONO').AsFloat;
    vTablaDestino.FieldByName('RECLAMADO').AsFloat       := vTablaOrigen.FieldByName('RECLAMADO').AsFloat;
    vTablaDestino.FieldByName('CANTIENALBA').AsFloat     := vTablaOrigen.FieldByName('CANTIENALBA').AsFloat;
    vTablaDestino.FieldByName('CANTISUSCRI').AsFloat     := vTablaOrigen.FieldByName('CANTISUSCRI').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOMPRA').AsFloat    := vTablaOrigen.FieldByName('PRECIOCOMPRA').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOMPRA2').AsFloat   := vTablaOrigen.FieldByName('PRECIOCOMPRA2').AsFloat;
    vTablaDestino.FieldByName('SWDTO').AsInteger         := vTablaOrigen.FieldByName('SWDTO').AsInteger;
    vTablaDestino.FieldByName('TPCDTO').AsFloat          := vTablaOrigen.FieldByName('TPCDTO').AsFloat;
    vTablaDestino.FieldByName('TPCIVA').AsFloat          := vTablaOrigen.FieldByName('TPCIVA').AsFloat;
    vTablaDestino.FieldByName('TPCRE').AsFloat           := vTablaOrigen.FieldByName('TPCRE').AsFloat;
    vTablaDestino.FieldByName('TPCRE2').AsFloat          := vTablaOrigen.FieldByName('TPCRE2').AsFloat;
    vTablaDestino.FieldByName('IMPOBRUTO').AsFloat       := vTablaOrigen.FieldByName('IMPOBRUTO').AsFloat;
    vTablaDestino.FieldByName('IMPODTO').AsFloat         := vTablaOrigen.FieldByName('IMPODTO').AsFloat;
    vTablaDestino.FieldByName('IMPOBASE').AsFloat        := vTablaOrigen.FieldByName('IMPOBASE').AsFloat;
    vTablaDestino.FieldByName('IMPOIVA').AsFloat         := vTablaOrigen.FieldByName('IMPOIVA').AsFloat;
    vTablaDestino.FieldByName('IMPORE').AsFloat          := vTablaOrigen.FieldByName('IMPORE').AsFloat;
    vTablaDestino.FieldByName('IMPOBASE2').AsFloat       := vTablaOrigen.FieldByName('IMPOBASE2').AsFloat;
    vTablaDestino.FieldByName('IMPOIVA2').AsFloat        := vTablaOrigen.FieldByName('IMPOIVA2').AsFloat;
    vTablaDestino.FieldByName('IMPORE2').AsFloat         := vTablaOrigen.FieldByName('IMPORE2').AsFloat;
    vTablaDestino.FieldByName('IMPOTOTLIN').AsFloat      := vTablaOrigen.FieldByName('IMPOTOTLIN').AsFloat;
    vTablaDestino.FieldByName('ENTRADAS').AsFloat        := vTablaOrigen.FieldByName('ENTRADAS').AsFloat;
    vTablaDestino.FieldByName('SALIDAS').AsFloat         := vTablaOrigen.FieldByName('SALIDAS').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTE').AsFloat      := vTablaOrigen.FieldByName('VALORCOSTE').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTE2').AsFloat     := vTablaOrigen.FieldByName('VALORCOSTE2').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTETOT').AsFloat   := vTablaOrigen.FieldByName('VALORCOSTETOT').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTETOT2').AsFloat  := vTablaOrigen.FieldByName('VALORCOSTETOT2').AsFloat;
    vTablaDestino.FieldByName('VALORCOMPRA').AsFloat     := vTablaOrigen.FieldByName('VALORCOMPRA').AsFloat;
    vTablaDestino.FieldByName('VALORCOMPRA2').AsFloat    := vTablaOrigen.FieldByName('VALORCOMPRA2').AsFloat;
    vTablaDestino.FieldByName('VALORVENTA').AsFloat      := vTablaOrigen.FieldByName('VALORVENTA').AsFloat;
    vTablaDestino.FieldByName('VALORVENTA2').AsFloat     := vTablaOrigen.FieldByName('VALORVENTA2').AsFloat;
    vTablaDestino.FieldByName('VALORMOVI').AsFloat       := vTablaOrigen.FieldByName('VALORMOVI').AsFloat;
    vTablaDestino.FieldByName('TPCCOMISION').AsFloat     := vTablaOrigen.FieldByName('TPCCOMISION').AsFloat;
    vTablaDestino.FieldByName('IMPOCOMISION').AsFloat    := vTablaOrigen.FieldByName('IMPOCOMISION').AsFloat;
    vTablaDestino.FieldByName('ENCARTE').AsFloat         := vTablaOrigen.FieldByName('ENCARTE').AsFloat;
    vTablaDestino.FieldByName('ENCARTE2').AsFloat        := vTablaOrigen.FieldByName('ENCARTE2').AsFloat;
    vTablaDestino.FieldByName('TURNO').AsInteger         := vTablaOrigen.FieldByName('TURNO').AsInteger;
    vTablaDestino.FieldByName('NALMACEN').AsInteger      := vTablaOrigen.FieldByName('NALMACEN').AsInteger;
    vTablaDestino.FieldByName('SWALTABAJA').AsInteger    := vTablaOrigen.FieldByName('SWALTABAJA').AsInteger;
    vTablaDestino.FieldByName('SWPDTEPAGO').AsInteger    := vTablaOrigen.FieldByName('SWPDTEPAGO').AsInteger;
    vTablaDestino.FieldByName('SWESTADO').AsInteger      := vTablaOrigen.FieldByName('SWESTADO').AsInteger;
   // vTablaDestino.FieldByName('SWDEVOLUCION').AsInteger  := vTablaOrigen.FieldByName('SWDEVOLUCION').AsInteger;
    vTablaDestino.FieldByName('SWCERRADO').AsInteger     := 0;
    vTablaDestino.FieldByName('SWMARCA').AsInteger       := vTablaOrigen.FieldByName('SWMARCA').AsInteger;
    vTablaDestino.FieldByName('SWMARCA2').AsInteger      := vTablaOrigen.FieldByName('SWMARCA2').AsInteger;
    vTablaDestino.FieldByName('SWMARCA3').AsInteger      := vTablaOrigen.FieldByName('SWMARCA3').AsInteger;
    vTablaDestino.FieldByName('SWMARCA4').AsInteger      := vTablaOrigen.FieldByName('SWMARCA4').AsInteger;
    vTablaDestino.FieldByName('SWMARCA5').AsInteger      := vTablaOrigen.FieldByName('SWMARCA5').AsInteger;
    vTablaDestino.FieldByName('SEMANA').AsInteger        := vTablaOrigen.FieldByName('SEMANA').AsInteger;
    vTablaDestino.FieldByName('TIPOVENTA').AsInteger     := vTablaOrigen.FieldByName('TIPOVENTA').AsInteger;
    vTablaDestino.FieldByName('FECHACARGO').AsDateTime   := vTablaOrigen.FieldByName('FECHACARGO').AsDateTime;
    vTablaDestino.FieldByName('FECHAABONO').AsDateTime   := vTablaOrigen.FieldByName('FECHAABONO').AsDateTime;
    vTablaDestino.FieldByName('SWRECLAMA').AsInteger     := vTablaOrigen.FieldByName('SWRECLAMA').AsInteger;
    vTablaDestino.FieldByName('ID_HISARTIRE').AsInteger  := vTablaOrigen.FieldByName('ID_HISARTIRE').AsInteger;
    vTablaDestino.FieldByName('SWACABADO').AsInteger     := vTablaOrigen.FieldByName('SWACABADO').AsInteger;

  end;

  if vTablaDestino = sqlHisArti then
  begin
    vTablaDestino.FieldByName('CANTIDAD').AsFloat        := 1;
    vTablaDestino.FieldByName('CLAVE').AsString          := '01';
    vTablaDestino.FieldByName('DEVUELTOS').AsFloat       := vTablaDestino.FieldByName('CANTIDAD').AsFloat;
  end;

  FormAlbaranRecepcion.edTIva1Exit(nil);



end;

procedure TDMMenuRecepcion.RutLeerArticulos;
begin


  sqlArti1.close;
  sqlArti1.SQL.Text :=  Copy(sqlArti1.SQL.Text,1,
         pos('WHERE', Uppercase(sqlArti1.SQL.Text))-1)
         + ' WHERE A.BARRAS = '  + QuotedStr(vBarres)
         + ' ORDER BY A.BARRAS';
  sqlArti1.Open;


  sqlArti0.close;
  sqlArti0.SQL.Text :=  Copy(sqlArti0.SQL.Text,1,
         pos('WHERE', Uppercase(sqlArti0.SQL.Text))-1)
         + ' WHERE A.BARRAS = '  + QuotedStr(vBarres)
         + ' ORDER BY A.BARRAS';
  sqlArti0.Open;

end;
procedure TDMMenuRecepcion.RutLeerCodigoBarras;
var
  swNoExiste, swPrecios: Boolean;
begin
  vBarres  :=  DMPpal.vg_Barres;
  vAdendum :=  DMPpal.vg_Adendum;
  if swRepetido = false then// si sw repetido es igual a false pues lees sino no haces nada
  RutLeerArticulos
  else;

  //Si no existeix en la BD local i si en la General copiem el registre

  if (sqlArti1.RecordCount = 0) and (sqlArti0.RecordCount > 0) then
  begin
    with sqlArti0 do
    begin
      First;
      while not eof do
      begin
        sqlArti1.Append;
        CopyRecordDataset(sqlArti0, sqlArti1, 0);


        sqlArti1.Post;

        Next;
      end;

    end;

  end;

  if (sqlArti1.RecordCount > 0) and (sqlArti0.RecordCount > 0) then
  begin
    if sqlArti1.FieldByName('PRECIO1').AsFloat <> sqlArti0.FieldByName('PRECIO1').AsFloat then
    begin
      with sqlArti1 do
      begin
        First;
        while not eof do
        begin
          Edit;
          FieldByName('PRECIO1').AsFloat := sqlArti0.FieldByName('PRECIO1').AsFloat;
          Post;
          Next;
        end;
      end;
      swPrecios := true;
    end else swPrecios := false;
  end;

  if (swRepetido = true) or (sqlArti0.RecordCount <= 1) then //if el recordcount es igual o mas peque�o que 1 sigue sino abre el form
  begin
   swNoExiste := false;


      DMPpal.sqlHisArtiCom1.Close;
      DMPpal.sqlHisArtiCom1.SQL.Text := ' select *'
                + ' from FVHISARTI H'
                + ' left join fmarticulo A on (A.ID_ARTICULO = H.ID_ARTICULO)'
                + ' where H.id_articulo = ' + IntToStr(sqlArti1ID_ARTICULO.AsInteger) //cambiat el arti1 per arti0 (probes)
                + ' and A.BARRAS = '        + QuotedStr(vBarres)
                + ' and H.Adendum =  '      + QuotedStr(vAdendum)
                + ' and H.clave = 01 ';

      DMPpal.sqlHisArtiCom1.Open();
      //Mirem si existeix a compres i si existeix creem el registre a devolucions
      if DMPpal.sqlHisArtiCom1.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlHisArtiCom1, 'ID_PROVEEDOR') then
        begin

          if sqlHisArti.Locate('IDSTOCABE; ID_ARTICULO; ADENDUM; PAQUETE',

                                        VarArrayOf([
                                        sqlCabe1IDSTOCABE.AsInteger,
                                        DMPpal.sqlHisArtiCom1.FieldByName('ID_ARTICULO').AsInteger,
                                        vAdendum,
                                        sqlCabe1PAQUETE.AsInteger]),

                                        []) then
          begin
            sqlHisArti.Edit;
            sqlHisArti.FieldByName('CANTIDAD').AsInteger   := sqlHisArti.FieldByName('CANTIDAD').AsInteger + 1;
          //  sqlHisArti.FieldByName('DEVUELTOS').AsInteger  := sqlHisArti.FieldByName('DEVUELTOS').AsInteger + 1;
            FormAlbaranRecepcion.RutMensajeArticulo(1);
          end else
          begin
            sqlHisArti.Append;
            Sp_gen_Hisarti1.execproc;
            sqlHisArti.FieldByName('ID_HISARTI').AsInteger := SP_GEN_HISARTI1.Params[0].Value;
            //----//
            RutPasarCampos(DMPpal.sqlHisArtiCom1, sqlHisArti, 2,DMPpal.sqlHisArtiCom1.FieldByName('ID_PROVEEDOR').AsInteger);


            sqlHisArti.FieldByName('IDSTOCABE').AsInteger := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlHisArti.FieldByName('ADENDUM').AsString  := vAdendum;
            sqlHisArti.FieldByName('PAQUETE').AsInteger := sqlCabe1PAQUETE.AsInteger;


            DMPpal.RutCalculaImportes(sqlHisArti, 'CANTIDAD', -2, 0);

            if swPrecios then FormAlbaranRecepcion.RutMensajeArticulo(9)
            else FormAlbaranRecepcion.RutMensajeArticulo(1);
          end;

          sqlHisArti.Post;
          swNoExiste := false;
          swRepetido := false;
          exit;
        end else
        begin
          vID := DMPpal.sqlHisArtiCom1.FieldByName('ID_PROVEEDOR').asInteger;
          FormAlbaranRecepcion.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;

      end else  swNoExiste := true;



    if swNoExiste = true then
    begin

      DMPpal.sqlHisArtiCom0.Close;
      DMPpal.sqlHisArtiCom0.SQL.Text := ' select *'
              + ' from FVHISARTI H'
              + ' left join fmarticulo A on (A.ID_ARTICULO = H.ID_ARTICULO)'
              + ' where H.id_articulo = ' + IntToStr(sqlArti0ID_ARTICULO.AsInteger)
              + ' and A.BARRAS = '        + QuotedStr(DMPpal.vBarres)
              + ' and H.Adendum =  '      + QuotedStr(DMPpal.vAdendum)
              + ' and H.clave = 01 ';

      DMPpal.sqlHisArtiCom0.Open();
        //Si no existeix mirem a la BD general si existeix la compra i si existeix creem registre de devoluci�
      if DMPpal.sqlHisArtiCom0.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlHisArtiCom0, 'ID_PROVEEDOR') then
        begin

          if sqlHisArti.Locate('IDSTOCABE; ID_ARTICULO; ADENDUM; PAQUETE',

                                        VarArrayOf([

                                        sqlCabe1IDSTOCABE.AsInteger,
                                        DMPpal.sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
                                        vAdendum,
                                        sqlCabe1PAQUETE.AsInteger  ]),

                                        []) then
          begin
            sqlHisArti.Edit;
            sqlHisArti.FieldByName('CANTIDAD').AsInteger   := sqlHisArti.FieldByName('CANTIDAD').AsInteger + 1;
            sqlHisArti.FieldByName('DEVUELTOS').AsInteger  := sqlHisArti.FieldByName('DEVUELTOS').AsInteger + 1;
            FormAlbaranRecepcion.RutMensajeArticulo(1);

          end else
          begin
            sqlHisArti.Append;
            sp_gen_Hisarti1.execproc;
            sqlHisArti.FieldByName('ID_HISARTI').AsInteger := SP_GEN_HISARTI1.Params[0].Value;
            //canviar...
            RutPasarCampos(DMPpal.sqlHisArtiCom0, sqlHisArti, 2, DMPpal.sqlHisArtiCom0.FieldByName('ID_PROVEEDOR').AsInteger);


            sqlHisArti.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlHisArti.FieldByName('ADENDUM').AsString     := vadendum;
            sqlHisArti.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;


            DMPpal.RutCalculaImportes(sqlHisArti, 'CANTIDAD', -2, 0);

            if swPrecios then FormAlbaranRecepcion.RutMensajeArticulo(9)
            else FormAlbaranRecepcion.RutMensajeArticulo(1);


          end;
            sqlHisArti.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          vID := DMPpal.sqlHisArtiCom0.FieldByName('ID_PROVEEDOR').asInteger;
          FormAlbaranRecepcion.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;
      end else  swNoExiste := true;
    end;


    if swNoExiste = true then
    begin
      if sqlArti1.RecordCount > 0 then
      begin
        if RutExisteProveedor(sqlArti1, 'EDITORIAL') then
        begin


            sqlHisArti.Append;
            sp_gen_Hisarti1.execproc;
            sqlHisArti.FieldByName('ID_HISARTI').AsInteger := SP_GEN_HISARTI1.Params[0].Value;
            //----canviar----//
            RutPasarCampos(sqlArti1, sqlHisArti, 1, sqlArti1EDITORIAL.AsInteger);
            sqlHisArti.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlHisArti.FieldByName('ADENDUM').AsString     := vadendum;
            sqlHisArti.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;
            DMPpal.RutCalculaImportes(sqlHisArti, 'CANTIDAD', -2, 0);

            if swPrecios then FormAlbaranRecepcion.RutMensajeArticulo(9)
            else FormAlbaranRecepcion.RutMensajeArticulo(1);



            sqlHisArti.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          vID := sqlArti1.FieldByName('EDITORIAL').asInteger;
          FormAlbaranRecepcion.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;
      end else  swNoExiste := true;

    end;

    if swNoExiste = true then
    begin
      if sqlArti0.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlArti0, 'EDITORIAL') then
        begin


            sqlHisArti.Append;
            sp_gen_Hisarti1.execproc;
            sqlHisArti.FieldByName('ID_HISARTI').AsInteger :=  SP_GEN_HISARTI1.Params[0].Value;
            //----canviar---//
            RutPasarCampos( DMPpal.sqlArti0, sqlHisArti, 1,  sqlArti0EDITORIAL.AsInteger);
            sqlHisArti.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlHisArti.FieldByName('ADENDUM').AsString     := vadendum;
            sqlHisArti.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;
            DMPpal.RutCalculaImportes(sqlHisArti, 'CANTIDAD', -2, 0);

            if swPrecios then FormAlbaranRecepcion.RutMensajeArticulo(9)
            else FormAlbaranRecepcion.RutMensajeArticulo(1);




            sqlHisArti.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          DMPpal.vID :=  DMPpal.sqlArti0.FieldByName('EDITORIAL').asInteger;
          DMPpal.swCrear := true;
          FormAlbaranRecepcion.RutMensajeDistri;
          swNoExiste := false;
        end;

      end else  swNoExiste := true;
    end;

    if swNoExiste = true then FormAlbaranRecepcion.RutMensajeArticulo(5);

  end else
  begin
    swRepetido := true;
    FormEscogerArti.gridUsuarios.DataSource := FormEscogerArti.dsArtiRecepcion;
    FormEscogerArti.lbBarras.Caption := 'Seleccione uno';
    FormEscogerArti.ShowModal();
  end;
end;


function TDMMenuRecepcion.RutExisteProveedor(vTabla : TFDQuery; vCampo : String):Boolean;
begin

  if DMMenuRecepcion.sqlHisArtiCabe1.Locate('ID_PROVEEDOR', vTabla.FieldByName(vCampo).AsInteger,[]) then
  begin
    Result := true;
    DMMenuRecepcion.sqlCabe1.Locate('IDSTOCABE', DMMenuRecepcion.sqlHisArtiCabe1IDSTOCABE.AsInteger, []);
  end else
  begin
    Result := false;

  end;

end;


procedure TDMMenuRecepcion.ProSortidaPreus (zTipo:Integer);
begin
   if DMppal.RutEdiInse(sqlHisArti) = False Then Exit;

  if DMppal.rutcomprobartopecantitat (sqlHisArti,1000,0,'PRECIOVENTA','SS') = False Then
  begin
      if FormAlbaranRecepcion.edPreu.CanFocus then FormAlbaranRecepcion.edPreu.SetFocus;
      Exit;
  end;

  sqlHisArti.FieldByName('PRECIOCOMPRA').AsFloat := sqlHisArti.FieldByName('PRECIOVENTA').AsFloat;

  DMMenuArti.RutCalculaCost_Taula     (sqlHisArti, '1H');
  wBeneficio := DMMenuArti.wBene;
  DMMenuArti.RutCalculaCost_Taula_Tot (sqlHisArti, '1H');
  DMMenuArti.RutCalculaCost_Taula     (sqlHisArti, '2H');
  wBeneficio2 := DMMenuArti.wBene;
  DMMenuArti.RutCalculaCost_Taula_Tot (sqlHisArti, '2H');

//a  RutCalculaLinHisto_N     (DMDevoluA.cdsHistoAnte, 'CANTIENALBA', 2, 0);
//a  RutDevolu_CalculoVendidos (DMDevoluA.cdsHistoAnte, 'COMPRAHISTO');
//  DMDevolucion.RutDevolu_CalculoVendidos_A (DMDevolucion.sqllinea, FormDevoluA.cbRestoVentas1.Checked);
//  if BtnGrabaLin.Enabled Then BtnGrabaLin.SetFocus;

end;

procedure TDMMenuRecepcion.ProCalcularPreuCost1;
begin
  inherited;
  if DMppal.RutEdiInse(DMMenuRecepcion.sqlHisArti) = False Then Exit;
  DMMenuArti.RutCalculaCost_Taula     (DMMenuRecepcion.sqlHisArti, '1H');
  DMMenuArti.RutCalculaCost_Taula_Tot (DMMenuRecepcion.sqlHisArti, '1H');
end;

procedure TDMMenuRecepcion.RutAbrirCabe1(vIDCabe,vTipoDoc : String; vDesde,vHasta : TDate; vFiltroFecha : Boolean);
var
  vWhere, vStringFecha : string;
begin
  vStringFecha := ' ';
  if vFiltroFecha then
    vStringFecha := ' AND FC.FECHA BETWEEN ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vDesde)) + ' AND ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vHasta));


   with sqlCabe1 do
  begin
    Close;
    sqlCabe1.SQL.Text :=  Copy(sqlCabe1.SQL.Text,1,
                      pos('WHERE FC.IDSTOCABE', Uppercase(SQL.Text))-1)
                  + 'WHERE FC.IDSTOCABE ' + vIDCabe
                  + ' AND (FC.SWTIPODOCU ' + vTipoDoc + ')'
                  + vStringFecha
                  + ' Order by FC.NOMBRE ' ;
    Open;
  end;

  with sqlHisArtiCabe1 do
  begin
    close;
    SQL.Text := sqlCabe1.SQL.Text;
    open;
  end;

end;


procedure TDMMenuRecepcion.ProCalcularAdevolver;
begin

  ProCargarUltiCompra;

  if (sqlArticulos1STOCKMAXIMO.AsFloat = 0) then
  begin
    sqlHisArti.edit;
    sqlHisArtiABONO.AsFloat := 0;
  end;

  if (sqlArticulos1STOCKMAXIMO.AsFloat > 0) and
     ((sqlHisArtiCANTIENALBA.AsFloat + vRecibido - vDevuelto) > sqlArticulos1STOCKMAXIMO.AsFloat) then
     begin
       sqlHisArti.edit;
       sqlHisArtiABONO.AsFloat := sqlHisArtiCANTIENALBA.AsFloat + vRecibido - vDevuelto -
                                               sqlArticulos1STOCKMAXIMO.AsFloat;
       sqlHisArtiSWPDTEPAGO.AsInteger := 2;

     end;

      if vRecibido > sqlHisArtiCANTIENALBA.AsFloat then
         vRecibido:= sqlHisArtiCANTIENALBA.AsFloat;
     FormAlbaranRecepcion.edAcumCompras.Text := FormatFloat(',0', vRecibido);
//      if vDevuelto > 0 then
//         vRecibido:= DMReceGenA.cdsHisArtiCANTIENALBA.AsFloat;
      FormAlbaranRecepcion.edAcumDevol.Text := FormatFloat(',0', vDevuelto);

end;

procedure TDMMenuRecepcion.ProCargarUltiCompra;
var
  vControlFechaSinAdendum, vIdArticulo : String;
  vSelectHisarti, vSelectAdendum, vFechaH, vFechaH1 : String;
begin

  vRecibido := 0;
  vDevuelto := 0;

  with DMMenuRecepcion.sqlUltiCompra do
  begin
    if DMMenuRecepcion.sqlHisArtiADENDUM.AsString = '' then
    begin
       FormAlbaranRecepcion.pnPieCompras.Visible := True;
       vControlFechaSinAdendum := '  and H1.Fecha >= H.FECHA';
    end else
    begin
       FormAlbaranRecepcion.pnPieCompras.Visible := False;
       vControlFechaSinAdendum := '  ';
    end;

    if DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsString = '' then
       vIdArticulo := '0'
       else
       vIdArticulo := DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsString;

// ver los totales de compras, ventas, etc

  vFechaH  := Dmppal.RutTexFechas_SQL (' H .FECHA ', FormatDateTime('dd/mm/yyyy', (Now- 270)), FormatDateTime('dd/mm/yyyy', (Now + 120)));
  vFechaH1 := DMppal.RutTexFechas_SQL (' H1.FECHA ', FormatDateTime('dd/mm/yyyy', (Now- 270)), FormatDateTime('dd/mm/yyyy', (Now + 120)));

    if DMMenuRecepcion.sqlHisArtiADENDUM.AsString = '' then
    begin
      DMPpal.sqlUpdate.Close;
      DMPpal.sqlUpdate.SQL.Text := 'Select H.ID_ARTICULO'
                + ', sum(H.entradas) as Entradas'
                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO'
                + '  and ((H1.clave = ' + QuotedStr('51') + ') or (H1.clave = ' + QuotedStr('53') + '))'
                + vFechaH1
                + ') as Ventas'

                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO'
                + '  and H1.clave = ' + QuotedStr('54')
                + vFechaH1
                + ') as Devoluciones'

                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO'
                + '  and H1.clave = ' + QuotedStr('55')
                + vFechaH1
                + ') as Merma'

                + ', (Select SUM(H1.Entradas - H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO'
                + '  and H1.clave <= ' + QuotedStr('90')
                + vFechaH1
                + ') as Stock'

                + '  from FVHISARTI H'

                + '  WHERE H.ID_ARTICULO = ' + vIdArticulo
                + '  and H.clave = ' + QuotedStr('01')
                + vFechaH
                + '  group by 1'
                + '  order by 1';

      DMPpal.sqlUpdate.Open;

    {  FormAlbaranRecepcion.edCompras.Text      := FormatFloat('0', DMPpal.sqlUpdate.fieldbyname('Entradas').AsFloat);
      FormAlbaranRecepcion.edVentas.Text       := FormatFloat('0', DMPpal.sqlUpdate.fieldbyname('Ventas').AsFloat);
      FormAlbaranRecepcion.edDevoluciones.Text := FormatFloat('0', DMPpal.sqlUpdate.fieldbyname('Devoluciones').AsFloat);
      FormAlbaranRecepcion.edMermas.Text       := FormatFloat('0', DMPpal.sqlUpdate.fieldbyname('Merma').AsFloat);
      FormAlbaranRecepcion.edStock2.Text        := FormatFloat('0', DMPpal.sqlUpdate.fieldbyname('Stock').AsFloat);     }
    end;




    Close;
    sqlUltiCompra.SQL.Text := 'Select First 5 H.ID_ARTICULO, H.ADENDUM, H.FECHA'
                + ', sum(H.entradas) as Entradas'
                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO'
                + '  and H1.adendum = H.Adendum and ((H1.clave = ' + QuotedStr('51') + ') or (H1.clave = ' + QuotedStr('53') + '))'
                + vControlFechaSinAdendum
                + vFechaH1
                + ') as Ventas'
                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adendum'
                + '  and H1.clave = ' + QuotedStr('54')
                + vControlFechaSinAdendum
                + vFechaH1
                + ') as Devoluciones'

                + ', (Select SUM(H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adendum'
                + '  and H1.clave = ' + QuotedStr('55')
                + vControlFechaSinAdendum
                + vFechaH1
                + ') as Merma'

                + ', (Select SUM(H1.Entradas - H1.Salidas) From FVHISARTI H1 where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adendum'
                + '  and H1.clave <= ' + QuotedStr('90')
                + vControlFechaSinAdendum
                + vFechaH1
                + ') as Stock'

                + '  from FVHISARTI H'

                + '  WHERE H.ID_ARTICULO = ' + vIdArticulo
                + '  and H.clave = ' + QuotedStr('01')
                + vFechaH

                + '  group by 1,2,3'
                + '  order by 1,3 desc,2';

    Open;





  end;

  vSelectHisarti := FormatFloat('0', sqlHisArtiID_HISARTI.AsInteger);
  vSelectAdendum := sqlHisArtiADENDUM.AsString;
  if FormAlbaranRecepcion.edAdendum.Text > '' then
  vSelectAdendum := FormAlbaranRecepcion.edAdendum.Text;


    DMPpal.sqlUpdate.Close;
    DMPpal.sqlUpdate.SQL.Text := 'Select '
                + '  sum(H.entradas) as Entradas'

                + '  from FVHISARTI H'

                + '  WHERE H.ID_ARTICULO = ' + vIdArticulo
                + '  and H.adendum = ' + QuotedStr(vSelectAdendum)
                + '  and H.clave = ' + QuotedStr('01')
                + vFechaH
                + '  and H.ID_HISARTI <> ' + QuotedStr(vSelectHisarti);
    DMPpal.sqlUpdate.Open;
    vRecibido := DMPpal.sqlUpdate.fieldbyname('Entradas').AsFloat;

    DMPpal.sqlUpdate.Close;
    DMPpal.sqlUpdate.SQL.Text := 'Select '
                + '  sum(H.salidas) as Salidas'

                + '  from FVHISARTI H'

                + '  WHERE H.ID_ARTICULO = ' + vIdArticulo
                + '  and H.adendum = ' + QuotedStr(vSelectAdendum)
                + '  and H.clave = ' + QuotedStr('54')
                + vFechaH
                + '  and H.ID_HISARTI <> ' + QuotedStr(vSelectHisarti);
    DMPpal.sqlUpdate.Open;
    vDevuelto := DMPpal.sqlUpdate.fieldbyname('Salidas').AsFloat;

end;



procedure TDMMenuRecepcion.RutAbrirHisArti(vIdStocable : String);
begin
 with sqlHisArti do
  begin
    Close;
    sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE H.IDSTOCABE', Uppercase(SQL.Text))-1)
                 + 'WHERE H.IDSTOCABE = ' + vIdStocable
                 + ' and H.CLAVE = ' + '''' + '01' + ''''
                 + ' ORDER BY H.ID_HISARTI';
    Open;
  end;

  with sqlHisArtiTotResumen do
  begin
    close;
    sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE IDSTOCABE', Uppercase(SQL.Text))-1)
                 + 'WHERE IDSTOCABE = ' + vIdStocable
                 + ' and CLAVE = ' + '''' + '01' + ''''
                 + ' group by FechaCargo '
                 + ' Order by FechaCargo';
    Open;

  end;

end;

procedure TDMMenuRecepcion.RutAbrirArti(vIdArticulo : Integer);
begin
 with sqlArticulos1 do
  begin
    Close;
    sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE ID_ARTICULO', Uppercase(SQL.Text))-1)
                 + 'WHERE ID_ARTICULO = ' + IntToStr(vIdArticulo)
                 + ' ORDER BY ID_ARTICULO';
    Open;
  end;


end;
{
procedure TDMMenuRecepcion.ProCalcularUltiCompra;
var
  vVentas, vEntradas, vMerma, vDevoluciones : Double;
begin
  inherited;
  with sqlUltiCompra do
  begin
    Last;
    vVentas       := fieldbyname('Ventas').AsFloat;
    vDevoluciones := fieldbyname('Devoluciones').AsFloat;
    vMerma        := fieldbyname('Merma').AsFloat;

    while not bof do
    begin
      edit;
//      fieldbyname('Ventas').AsFloat       := fieldbyname('Ventas').AsFloat - vVentas;
//      fieldbyname('Devoluciones').AsFloat := fieldbyname('Devoluciones').AsFloat - vDevoluciones;
//      fieldbyname('Merma').AsFloat        := fieldbyname('Merma').AsFloat - vMerma;
//
//      vVentas       := vVentas + fieldbyname('Ventas').AsFloat;
//      vDevoluciones := vDevoluciones + fieldbyname('Devoluciones').AsFloat;
//      vMerma        := vMerma + fieldbyname('Merma').AsFloat;
//
//      fieldbyname('Stock').AsFloat := fieldbyname('Entradas').AsFloat
//                                    - fieldbyname('Ventas').AsFloat
//                                    - fieldbyname('Devoluciones').AsFloat
//                                    - fieldbyname('Merma').AsFloat;

      fieldbyname('Ventas').AsFloat       := 0;
      fieldbyname('Devoluciones').AsFloat := 0;
      fieldbyname('Merma').AsFloat        := 0;
      fieldbyname('Stock').AsFloat        := 0;

      Post;
      Prior;
    end;
  end;
end;
}

procedure TDMMenuRecepcion.RutAbrirReservasRecepcion;
begin
  sqlReservasRecepcion.Close;
  sqlReservasRecepcion.Open;
end;

procedure TDMMenuRecepcion.RutLeerProveedor(vID:Integer);
begin

  with DMppal.sqlProveedor2 do
  begin
    Close;
    SQL.Text := 'Select * ' +
                'From FMPROVE ' +
                'Where ID_PROVEEDOR = ' + IntToStr(vID);


    Open;
  end;

end;





procedure TDMMenuRecepcion.sqlCabe1AfterScroll(DataSet: TDataSet);
begin
  RutAbrirHisArti(DMMenuRecepcion.sqlCabe1IDSTOCABE.AsString);
end;

procedure TDMMenuRecepcion.sqlHisArtiCalcFields(DataSet: TDataSet);
begin
  sqlHisArtiCosteTotal.AsFloat := sqlHisArtiVALORCOSTE.AsFloat + sqlHisArtiVALORCOSTE2.AsFloat;
  DataSet.FieldByName('PrecioTotal').AsFloat :=
  DataSet.FieldByName('PrecioVenta').AsFloat +
  DataSet.FieldByName('PrecioVenta2').AsFloat;
end;

procedure TDMMenuRecepcion.sqlListaRecepcionCalcFields(DataSet: TDataSet);
begin
  if sqlListaRecepcion.Active = False Then Exit;
  Dmppal.RutCalculaMargesCaps (sqlListaRecepcion);
  sqlListaRecepcionCosteTotal.AsFloat := sqlListaRecepcionVALCOSTE.AsFloat + sqlListaRecepcionVALCOSTE2.AsFloat;
end;

initialization
  RegisterModuleClass(TDMMenuRecepcion);





end.


