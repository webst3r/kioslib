unit uDevolucionAlbaran;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar;

type
  TFormDevolucionAlbaran = class(TUniForm)
    UniPanel1: TUniPanel;
    UniPanel2: TUniPanel;
    UniDBEdit1: TUniDBEdit;
    UniLabel1: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniPanel3: TUniPanel;
    pFechaPreDevolu: TUniDBDateTimePicker;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    rgOrdenado: TUniDBRadioGroup;
    fcPanel3: TUniPanel;
    UniLabel5: TUniLabel;
    UniDBEdit5: TUniDBEdit;
    Rgpaquetes: TUniDBRadioGroup;
    UniPanel4: TUniPanel;
    UniLabel6: TUniLabel;
    UniDBEdit6: TUniDBEdit;
    UniBitBtn1: TUniBitBtn;
    UniPageControl1: TUniPageControl;
    TabSheet3: TUniTabSheet;
    UniPageControl2: TUniPageControl;
    TabSheet6: TUniTabSheet;
    TabSheet4: TUniTabSheet;
    TabSheet7: TUniTabSheet;
    UniPanel5: TUniPanel;
    btVerDirectoDevolucion: TUniBitBtn;
    BtnVerVtaAuto: TUniBitBtn;
    BtnNouAdendum: TUniBitBtn;
    UniLabel9: TUniLabel;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniLabel10: TUniLabel;
    UniDBEdit9: TUniDBEdit;
    UniLabel11: TUniLabel;
    BtnSumarCanti: TUniBitBtn;
    BtnRestarCanti: TUniBitBtn;
    UniLabel12: TUniLabel;
    UniDBEdit10: TUniDBEdit;
    pnlPendi: TUniPanel;
    BtnPendi: TUniBitBtn;
    pnlProveLine: TUniPanel;
    UniDBEdit11: TUniDBEdit;
    UniLabel13: TUniLabel;
    UniDBEdit12: TUniDBEdit;
    UniPanel6: TUniPanel;
    UniPanel7: TUniPanel;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    UniDBText1: TUniDBText;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBEdit15: TUniDBEdit;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniDBText2: TUniDBText;
    UniPanel8: TUniPanel;
    UniLabel16: TUniLabel;
    UniDBText3: TUniDBText;
    UniDBText4: TUniDBText;
    UniDBEdit16: TUniDBEdit;
    UniBitBtn5: TUniBitBtn;
    UniCheckBox1: TUniCheckBox;
    UniCheckBox2: TUniCheckBox;
    UniCheckBox3: TUniCheckBox;
    UniBitBtn6: TUniBitBtn;
    UniPanel9: TUniPanel;
    UniGroupBox1: TUniGroupBox;
    UniLabel17: TUniLabel;
    UniLabel18: TUniLabel;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniDBDateTimePicker4: TUniDBDateTimePicker;
    UniDBDateTimePicker5: TUniDBDateTimePicker;
    UniBitBtn7: TUniBitBtn;
    UniLabel19: TUniLabel;
    UniLabel20: TUniLabel;
    UniLabel21: TUniLabel;
    UniSpeedButton3: TUniSpeedButton;
    UniLabel22: TUniLabel;
    UniDBEdit17: TUniDBEdit;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniPageControl3: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniTabSheet2: TUniTabSheet;
    UniDBGrid1: TUniDBGrid;
    UniDBGrid2: TUniDBGrid;
    UniPanel10: TUniPanel;
    UniLabel23: TUniLabel;
    UniBitBtn8: TUniBitBtn;
    UniLabel24: TUniLabel;
    UniLabel25: TUniLabel;
    UniLabel26: TUniLabel;
    UniDBEdit18: TUniDBEdit;
    UniDBNavigator1: TUniDBNavigator;
    UniDBEdit19: TUniDBEdit;
    UniDBEdit20: TUniDBEdit;
    UniDBEdit21: TUniDBEdit;
    UniDBMemo1: TUniDBMemo;
    UniPanel11: TUniPanel;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniPanel12: TUniPanel;
    UniPanel13: TUniPanel;
    UniPanel14: TUniPanel;
    UniPanel15: TUniPanel;
    UniLabel27: TUniLabel;
    UniLabel28: TUniLabel;
    UniDBEdit22: TUniDBEdit;
    UniDBEdit23: TUniDBEdit;
    UniDBEdit24: TUniDBEdit;
    UniLabel29: TUniLabel;
    UniDBEdit25: TUniDBEdit;
    UniDBEdit26: TUniDBEdit;
    UniDBEdit27: TUniDBEdit;
    UniDBEdit28: TUniDBEdit;
    UniLabel30: TUniLabel;
    UniLabel31: TUniLabel;
    UniDBEdit29: TUniDBEdit;
    UniBitBtn11: TUniBitBtn;
    UniBitBtn12: TUniBitBtn;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    UniLabel37: TUniLabel;
    UniDBEdit30: TUniDBEdit;
    UniDBEdit31: TUniDBEdit;
    UniDBEdit32: TUniDBEdit;
    UniDBEdit33: TUniDBEdit;
    UniDBEdit34: TUniDBEdit;
    UniDBEdit35: TUniDBEdit;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    UniDBDateTimePicker7: TUniDBDateTimePicker;
    UniCheckBox4: TUniCheckBox;
    UniDBEdit36: TUniDBEdit;
    UniLabel38: TUniLabel;
    UniPanel16: TUniPanel;
    UniBitBtn13: TUniBitBtn;
    UniDBEdit37: TUniDBEdit;
    UniDBEdit38: TUniDBEdit;
    UniDBEdit39: TUniDBEdit;
    UniDBEdit40: TUniDBEdit;
    UniDBEdit41: TUniDBEdit;
    UniDBEdit42: TUniDBEdit;
    UniDBEdit43: TUniDBEdit;
    UniDBEdit44: TUniDBEdit;
    UniLabel39: TUniLabel;
    UniLabel40: TUniLabel;
    UniLabel41: TUniLabel;
    UniLabel42: TUniLabel;
    UniDBEdit45: TUniDBEdit;
    UniDBEdit46: TUniDBEdit;
    UniDBEdit47: TUniDBEdit;
    UniDBEdit48: TUniDBEdit;
    UniDBEdit49: TUniDBEdit;
    UniDBEdit50: TUniDBEdit;
    UniLabel43: TUniLabel;
    UniLabel44: TUniLabel;
    UniLabel45: TUniLabel;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel46: TUniLabel;
    UniLabel47: TUniLabel;
    UniPanel17: TUniPanel;
    UniDBEdit55: TUniDBEdit;
    UniDBEdit56: TUniDBEdit;
    UniDBEdit57: TUniDBEdit;
    UniDBEdit58: TUniDBEdit;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniDBEdit59: TUniDBEdit;
    UniDBEdit60: TUniDBEdit;
    UniDBEdit61: TUniDBEdit;
    UniDBEdit62: TUniDBEdit;
    UniLabel50: TUniLabel;
    UniLabel51: TUniLabel;
    UniLabel52: TUniLabel;
    UniLabel53: TUniLabel;
    UniLabel54: TUniLabel;
    UniDBText5: TUniDBText;
    UniDBText6: TUniDBText;
    UniDBText7: TUniDBText;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniDBText8: TUniDBText;
    UniDBText9: TUniDBText;
    UniPanel18: TUniPanel;
    UniLabel58: TUniLabel;
    UniDBEdit63: TUniDBEdit;
    UniDBEdit64: TUniDBEdit;
    UniDBEdit65: TUniDBEdit;
    UniDBEdit66: TUniDBEdit;
    UniLabel59: TUniLabel;
    UniLabel60: TUniLabel;
    UniDBEdit67: TUniDBEdit;
    UniLabel61: TUniLabel;
    UniLabel62: TUniLabel;
    UniDBGrid3: TUniDBGrid;
    TabDirectoDevolucion: TUniTabSheet;
    AdvPanel2: TUniPanel;
    UniContainerPanel1: TUniContainerPanel;
    btCargarDevolucionAuto: TUniBitBtn;
    btVolverADevolucion: TUniBitBtn;
    btEliminarDirectoDevolucion: TUniBitBtn;
    UniDBGrid4: TUniDBGrid;
    PanelCargarDevolucion: TUniPanel;
    spTraspasar1: TUniBitBtn;
    spTraspasarTodos: TUniBitBtn;
    TabPdteDevolu: TUniTabSheet;
    UniPanel19: TUniPanel;
    UniContainerPanel2: TUniContainerPanel;
    UniBitBtn14: TUniBitBtn;
    UniBitBtn15: TUniBitBtn;
    UniDBGrid5: TUniDBGrid;
    PanelConfiguracion: TUniPanel;
    BtnReclama: TUniBitBtn;
    pChkVerArti: TUniCheckBox;
    pCbxLineaAutomatica: TUniCheckBox;
    cbSoloProveedor: TUniCheckBox;
    pcxCoDistri: TUniCheckBox;
    cbSumando: TUniCheckBox;
    cbUnchecked: TUniCheckBox;
    cbxDevolTeorica: TUniCheckBox;
    cbCerrarCodigos: TUniCheckBox;
    cbAbrirCerrados: TUniCheckBox;
    cbVerSumando: TUniCheckBox;
    UniLabel63: TUniLabel;
    edEtiquetas: TUniDBEdit;
    btListados: TUniBitBtn;
    BtnPrecios: TUniBitBtn;
    btSalirConfiguracion: TUniBitBtn;
    PageAbrirCerrarCodigos: TUniPageControl;
    UniTabSheet3: TUniTabSheet;
    UniLabel64: TUniLabel;
    edCerrarHastaFecha: TUniDBDateTimePicker;
    UniLabel65: TUniLabel;
    edNumeroCerrar: TUniDBEdit;
    btCerrarCodigos: TUniBitBtn;
    lbRegistros: TUniLabel;
    LabelMensajeCerrar: TUniLabel;
    lbRegistrosAcabados: TUniLabel;
    lbRegistrosEnProceso: TUniLabel;
    ProgressBar1: TUniProgressBar;
    UniTabSheet4: TUniTabSheet;
    btAbrirCodigos: TUniBitBtn;
    btBuscarNumero: TUniBitBtn;
    edUltimoNumeroCierre: TUniDBEdit;
    UniLabel66: TUniLabel;
    LabelMensajeAbrir: TUniLabel;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormDevolucionAlbaran: TFormDevolucionAlbaran;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti;

function FormDevolucionAlbaran: TFormDevolucionAlbaran;
begin
  Result := TFormDevolucionAlbaran(DMppal.GetFormInstance(TFormDevolucionAlbaran));

end;


end.
