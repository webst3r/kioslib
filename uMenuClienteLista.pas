unit uMenuClienteLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormMenuClienteLista = class(TUniForm)
    UniDBGrid1: TUniDBGrid;
    dsCabe: TDataSource;
    procedure UniDBGrid1DblClick(Sender: TObject);


  private
    procedure ProAbrirCabe(zEmpresa, zCliente: Integer);
    { Private declarations }

  public
    { Public declarations }

  vTipo : String;

  vInicioForm : Boolean;

  vAreaNegocio, vActuacion, vCliente, vTipoCliente:Integer;
  vFechaAlta, vParar:String;

  vNomCabe,
  vNomKeyCabe1,vNomKeyCabe2,
  vWhereCabe,vOrderCabe : String;
  vFiltroEspecial : String;

  vFechaDesde : TDate;

  vOEmpresa, vOCliente : Integer;

  vSwNou              : Boolean;
  vCodiNou, vCodiVell : Integer;
  vSwBusca,  vSwTFacturacion, vSwTPago   : Boolean;

  vImpoTotLin, vValorMovi : Double;
  vRegisH,   vRegisC    : Integer;

  end;

function FormMenuClienteLista: TFormMenuClienteLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuCliente, uDMMenuCliente, uMenuClienteFicha;

function FormMenuClienteLista: TFormMenuClienteLista;
begin
  Result := TFormMenuClienteLista(DMppal.GetFormInstance(TFormMenuClienteLista));

end;


procedure TFormMenuClienteLista.UniDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ProAbrirCabe  (vOEmpresa, DMMenuCliente.sqlCabeID_CLIENTE.AsInteger);
  FormMenuClienteFicha.Parent := FormMenuCliente.tabFichaCliente;
  FormMenuCliente.pcDetalle.ActivePage := FormMenuCliente.tabFichaCliente;
end;

procedure TFormMenuClienteLista.ProAbrirCabe  (zEmpresa,zCliente:Integer);
var
  I : Integer;
begin
  vWhereCabe := ' Where ID_CLIENTE = ' + IntToStr(zCliente);
  vFiltroEspecial := '';
  vOrderCabe := ' Order by ID_CLIENTE ';
 // if FormPrincipal.cbClienteNombre.Checked then
 // vOrderCabe      := ' Order by NOMBRE ';

  DMMenuCliente.sqlCabeS.Close;
  DMMenuCliente.sqlCabeS.sql.text := Copy(DMMenuCliente.sqlCabe.sql.text,1,
                    pos('WHERE', Uppercase(DMMenuCliente.sqlCabe.sql.text))-1)
                 + vWhereCabe + vFiltroEspecial + vOrderCabe;
  DMMenuCliente.sqlCabeS.Open;
  DMMenuCliente.sqlCabeS.First;

  FormMenuClienteFicha.pCodi.Text := IntToStr(DMMenuCliente.sqlCabeSID_CLIENTE.AsInteger);

end;


end.
