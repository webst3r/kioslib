object DMMenuArti: TDMMenuArti
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 536
  Width = 848
  object sqlListaArticulo: TFDQuery
    Connection = DMppal.FDConnection1
    FetchOptions.AssignedValues = [evRowsetSize]
    FetchOptions.RowsetSize = 9999
    SQL.Strings = (
      'Select A.ID_ARTICULO, A.BARRAS,A.ADENDUM, A.DESCRIPCION,'
      ' A.PRECIOCOSTE,'
      ''
      ' A.PRECIOCOSTETOT,A.PRECIO1,A.PRECIO2, '
      ''
      ''
      ' A.REFEPROVE'
      ''
      ' from FMARTICULO A'
      ''
      ''
      ' where A.EDITORIAL IS NOT NULL'
      ' order by A.DESCRIPCION')
    Left = 27
    Top = 16
    object sqlListaArticuloID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlListaArticuloBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlListaArticuloADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlListaArticuloDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlListaArticuloPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = ',0.00'
    end
    object sqlListaArticuloPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
      DisplayFormat = ',0.00'
    end
    object sqlListaArticuloPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
      DisplayFormat = ',0.00'
    end
    object sqlListaArticuloPRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
      DisplayFormat = ',0.00'
    end
    object sqlListaArticuloREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
  end
  object sqlFichaArticulo: TFDQuery
    BeforeEdit = sqlFichaArticuloBeforeEdit
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select A.*, B.DESCRIPCION as NOMAUTOR, E.DESCRIPCION as NOMEDITO' +
        'R,'
      '       TC.DESCRIPCION as CONTENIDO,'
      '       TP.DESCRIPCION as TIPODEPRODUCTO,'
      '       TM.DESCRIPCION as NOMTEMATICA,'
      '       P.NOMBRE'
      ' from FMARTICULO A'
      
        ' left outer join FTIPOSI B  on (B.TTIPO='#39'TAUTOR'#39' and B.NORDEN = ' +
        'A.AUTOR)'
      
        ' left outer join FTIPOSI E  on (E.TTIPO='#39'TEDITOR'#39' and E.NORDEN =' +
        ' A.EDITOR)'
      
        ' left outer join FTIPOSI TC on (TC.TTIPO='#39'TCONTENI'#39' and TC.NORDE' +
        'N = A.TCONTENIDO)'
      
        ' left outer join FTIPOSI TP on (TP.TTIPO='#39'TPRODU'#39' and TP.NORDEN ' +
        '= A.TPRODUCTO)'
      
        ' left outer join FTIPOSI TM on (TM.TTIPO='#39'TTEMA'#39' and TM.NORDEN =' +
        ' A.TEMATICA)'
      ''
      
        ' left outer join FTIPOSI TG on (TG.TTIPO='#39'TGRUPO'#39' and TG.NORDEN ' +
        '= A.GRUPO)'
      ' '
      ' left outer join fmprove  P on (P.ID_PROVEEDOR = A.EDITORIAL)'
      ' where A.ID_ARTICULO = 0'
      ' order by A.DESCRIPCION')
    Left = 26
    Top = 64
    object sqlFichaArticuloID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFichaArticuloTBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlFichaArticuloBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlFichaArticuloADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlFichaArticuloFECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlFichaArticuloNADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlFichaArticuloDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlFichaArticuloDESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlFichaArticuloIBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlFichaArticuloISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlFichaArticuloEDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlFichaArticuloPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloPRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloTPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloTPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloTPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlFichaArticuloTPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlFichaArticuloTPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlFichaArticuloTPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlFichaArticuloTPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlFichaArticuloPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = '0.0000'
    end
    object sqlFichaArticuloPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
      DisplayFormat = '0.0000'
    end
    object sqlFichaArticuloPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
      DisplayFormat = '0.0000'
    end
    object sqlFichaArticuloPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
      DisplayFormat = '0.0000'
    end
    object sqlFichaArticuloMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloMARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
      DisplayFormat = '0.00'
    end
    object sqlFichaArticuloEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlFichaArticuloREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlFichaArticuloPERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlFichaArticuloCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlFichaArticuloTIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlFichaArticuloTIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlFichaArticuloSWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlFichaArticuloSWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlFichaArticuloSWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlFichaArticuloSWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlFichaArticuloSWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlFichaArticuloSWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlFichaArticuloTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlFichaArticuloTPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlFichaArticuloTCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlFichaArticuloTEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlFichaArticuloAUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlFichaArticuloOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlFichaArticuloSTOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlFichaArticuloSTOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlFichaArticuloFAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlFichaArticuloFAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlFichaArticuloORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlFichaArticuloSTOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlFichaArticuloSTOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlFichaArticuloSTOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlFichaArticuloREPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlFichaArticuloUNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlFichaArticuloUNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlFichaArticuloIMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlFichaArticuloCOD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlFichaArticuloGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlFichaArticuloSUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlFichaArticuloSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlFichaArticuloFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlFichaArticuloFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlFichaArticuloFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlFichaArticuloNOMAUTOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMAUTOR'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticuloNOMEDITOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMEDITOR'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticuloCONTENIDO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTENIDO'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticuloTIPODEPRODUCTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TIPODEPRODUCTO'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticuloNOMTEMATICA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMTEMATICA'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticuloNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlFichaArticuloTPCIVA1: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCIVA1'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCIVA'
      KeyFields = 'TIVA'
      Lookup = True
    end
    object sqlFichaArticuloTPCRE1: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCRE1'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCRE'
      KeyFields = 'TIVA'
      Lookup = True
    end
    object sqlFichaArticuloTPCIVA2: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCIVA2'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCIVA'
      KeyFields = 'TIVA2'
      Lookup = True
    end
    object sqlFichaArticuloTPCRE2: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCRE2'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCRE'
      KeyFields = 'TIVA2'
      Lookup = True
    end
  end
  object sqlArtiLink: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_ARTICULO, A.DESCRIPCION, A.EDITORIAL, P.NOMBRE, A.B' +
        'ARRAS, A.ADENDUM, A.TBARRAS, A.FAMILIAVENTA, A.ORDENVENTA'
      ', A.REFEPROVE'
      ', A.FECHABAJA'
      ', A.NADENDUM'
      ', A.STOCKMINIMOESTANTE as STOCK'
      ' From FMARTICULO A'
      ' left join FMPROVE P on (P.id_proveedor = A.Editorial)'
      ' where A.ID_ARTICULO is not null'
      ' Order by A.ID_ARTICULO')
    Left = 25
    Top = 112
    object sqlArtiLinkID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArtiLinkDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArtiLinkEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArtiLinkNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlArtiLinkBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArtiLinkADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArtiLinkTBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArtiLinkFAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArtiLinkORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArtiLinkREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArtiLinkFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArtiLinkNADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArtiLinkSTOCK: TSingleField
      FieldName = 'STOCK'
      Origin = 'STOCKMINIMOESTANTE'
    end
  end
  object sqlEditorialS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select ID_PROVEEDOR, TPROVEEDOR, NOMBRE'
      ' from FMPROVE'
      ' where ID_PROVEEDOR is not null'
      ' and (TPROVEEDOR=0 or TPROVEEDOR=1 or TPROVEEDOR=2)'
      'order by NOMBRE')
    Left = 25
    Top = 168
    object sqlEditorialSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlEditorialSTPROVEEDOR: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
    object sqlEditorialSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
  end
  object sqlPeriodi: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION, MARGEN, CADUCIDAD'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TPERIODI'#39
      'ORDER BY DESCRIPCION'
      '')
    Left = 25
    Top = 232
    object sqlPeriodiTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlPeriodiNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPeriodiDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlPeriodiMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlPeriodiCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
  end
  object sqlArTitulo: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select ID_ARTITULO, ID_ARTICULO, ADENDUM, TITULO, IMAGEN, SWALTA' +
        'BAJA, FECHABAJA, FECHAALTA, FECHAULTI'
      ' from FMARTITULO'
      ' where ID_ARTITULO is not null'
      ' Order by ID_ARTITULO')
    Left = 25
    Top = 288
    object sqlArTituloID_ARTITULO: TIntegerField
      FieldName = 'ID_ARTITULO'
      Origin = 'ID_ARTITULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArTituloID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      Required = True
    end
    object sqlArTituloADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArTituloTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 40
    end
    object sqlArTituloIMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArTituloSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArTituloFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArTituloFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArTituloFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
  end
  object sqlPreus: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select ID_ARTICULO, SWACTIVO, TDIASEMA, NUM, PRECIO1, PRECIOCOST' +
        'E, PRECIOCOSTETOT, MARGEN, TPCENCARTE1, TIVA, PRECIO2, PRECIOCOS' +
        'TE2, PRECIOCOSTETOT2, MARGEN2, TPCENCARTE2, TIVA2, FECHAPRECIO'
      ' from FMPRECIOS'
      'where ID_ARTICULO = 0')
    Left = 25
    Top = 344
    object sqlPreusID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPreusSWACTIVO: TSmallintField
      FieldName = 'SWACTIVO'
      Origin = 'SWACTIVO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPreusTDIASEMA: TSmallintField
      FieldName = 'TDIASEMA'
      Origin = 'TDIASEMA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPreusNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPreusPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlPreusPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlPreusPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlPreusMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlPreusTPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlPreusTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlPreusPRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlPreusPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlPreusPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlPreusMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlPreusTPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlPreusTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlPreusFECHAPRECIO: TDateField
      FieldName = 'FECHAPRECIO'
      Origin = 'FECHAPRECIO'
    end
  end
  object sqlTConteni1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TCONTENI'#39
      'ORDER BY NORDEN')
    Left = 25
    Top = 400
    object sqlTConteni1TTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTConteni1NORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTConteni1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlTTema1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TTEMA'#39
      'ORDER BY NORDEN')
    Left = 97
    Top = 400
    object sqlTTema1TTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTTema1NORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTTema1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlTAutor: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TAUTOR'#39
      'ORDER BY NORDEN')
    Left = 161
    Top = 408
    object sqlTAutorTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTAutorNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTAutorDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlTEditor: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TEDITOR'#39
      'ORDER BY NORDEN')
    Left = 225
    Top = 408
    object sqlTEditorTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTEditorNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTEditorDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlTProdu: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION, MARGEN, CADUCIDAD'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TPRODU'#39
      'ORDER BY NORDEN')
    Left = 281
    Top = 408
    object sqlTProduTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTProduNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTProduDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlTProduMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlTProduCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
  end
  object sqlPeriodicidad: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TPERIODI'#39
      'ORDER BY NORDEN')
    Left = 25
    Top = 456
    object sqlPeriodicidadTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlPeriodicidadNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPeriodicidadDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlTGrupo: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION, MARGEN, CADUCIDAD'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TGRUPO'#39
      'ORDER BY NORDEN')
    Left = 321
    Top = 408
    object sqlTGrupoTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTGrupoNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTGrupoDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlTGrupoMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlTGrupoCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
  end
  object sqlFamiVenta1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TFAMIVTA'#39
      'ORDER BY NORDEN'
      '')
    Left = 97
    Top = 456
    object sqlFamiVenta1TTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlFamiVenta1NORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFamiVenta1DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object sqlVarisSubGrupo: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION, MARGEN, CADUCIDAD'
      ' from FTIPOSI'
      'Where TTIPO = :TTIPO'
      'ORDER BY DESCRIPCION'
      '')
    Left = 97
    Top = 232
    ParamData = <
      item
        Name = 'TTIPO'
        DataType = ftString
        ParamType = ptInput
        Size = 10
        Value = Null
      end>
    object StringField1: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object SingleField1: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
  end
  object sqlTipoProdu: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select  TTIPO, NORDEN, DESCRIPCION, MARGEN, CADUCIDAD'
      ' from FTIPOSI'
      'Where TTIPO = '#39'TPRODU'#39
      'ORDER BY DESCRIPCION'
      '')
    Left = 97
    Top = 288
    object sqlTipoProduTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTipoProduNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTipoProduDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlTipoProduMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlTipoProduCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
  end
  object SP_GEN_ARTICULO: TFDStoredProc
    Connection = DMppal.FDConnection1
    StoredProcName = 'SP_GEN_ARTICULO'
    Left = 192
    Top = 8
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlTIVA1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select * from FGTIVA'
      'Order by TIVA')
    Left = 185
    Top = 72
    object sqlTIVA1TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTIVA1TIPOIVA: TStringField
      FieldName = 'TIPOIVA'
      Origin = 'TIPOIVA'
      Size = 35
    end
    object sqlTIVA1TPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlTIVA1CTACONTABLE: TStringField
      FieldName = 'CTACONTABLE'
      Origin = 'CTACONTABLE'
      Size = 12
    end
    object sqlTIVA1TPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlTIVA1CTARECARGO: TStringField
      FieldName = 'CTARECARGO'
      Origin = 'CTARECARGO'
      Size = 12
    end
    object sqlTIVA1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlTIVA1FECHAUTI: TDateField
      FieldName = 'FECHAUTI'
      Origin = 'FECHAUTI'
    end
    object sqlTIVA1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlTIVA1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlTIVA1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlTIVA1VALDESDE: TDateField
      FieldName = 'VALDESDE'
      Origin = 'VALDESDE'
    end
    object sqlTIVA1VALHASTA: TDateField
      FieldName = 'VALHASTA'
      Origin = 'VALHASTA'
    end
  end
  object sqlFichaArticulo0: TFDQuery
    BeforeEdit = sqlFichaArticuloBeforeEdit
    Connection = DMppal.FDConnection0
    SQL.Strings = (
      
        'Select A.*, B.DESCRIPCION as NOMAUTOR, E.DESCRIPCION as NOMEDITO' +
        'R,'
      '       TC.DESCRIPCION as CONTENIDO,'
      '       TP.DESCRIPCION as TIPODEPRODUCTO,'
      '       TM.DESCRIPCION as NOMTEMATICA,'
      '       P.NOMBRE'
      ' from FMARTICULO A'
      
        ' left outer join FTIPOSI B  on (B.TTIPO='#39'TAUTOR'#39' and B.NORDEN = ' +
        'A.AUTOR)'
      
        ' left outer join FTIPOSI E  on (E.TTIPO='#39'TEDITOR'#39' and E.NORDEN =' +
        ' A.EDITOR)'
      
        ' left outer join FTIPOSI TC on (TC.TTIPO='#39'TCONTENI'#39' and TC.NORDE' +
        'N = A.TCONTENIDO)'
      
        ' left outer join FTIPOSI TP on (TP.TTIPO='#39'TPRODU'#39' and TP.NORDEN ' +
        '= A.TPRODUCTO)'
      
        ' left outer join FTIPOSI TM on (TM.TTIPO='#39'TTEMA'#39' and TM.NORDEN =' +
        ' A.TEMATICA)'
      ''
      
        ' left outer join FTIPOSI TG on (TG.TTIPO='#39'TGRUPO'#39' and TG.NORDEN ' +
        '= A.GRUPO)'
      ' '
      ' left outer join fmprove  P on (P.ID_PROVEEDOR = A.EDITORIAL)'
      ' where A.ID_ARTICULO = 0'
      ' order by A.DESCRIPCION')
    Left = 106
    Top = 64
    object sqlFichaArticulo0ID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlFichaArticulo0TBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlFichaArticulo0BARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlFichaArticulo0ADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlFichaArticulo0FECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlFichaArticulo0NADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlFichaArticulo0DESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlFichaArticulo0DESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlFichaArticulo0IBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlFichaArticulo0ISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlFichaArticulo0EDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlFichaArticulo0PRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlFichaArticulo0PRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlFichaArticulo0PRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlFichaArticulo0PRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlFichaArticulo0PRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlFichaArticulo0PRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlFichaArticulo0PRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlFichaArticulo0TPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlFichaArticulo0TPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlFichaArticulo0TPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlFichaArticulo0TPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlFichaArticulo0TPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlFichaArticulo0TPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlFichaArticulo0TPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlFichaArticulo0PRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlFichaArticulo0PRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlFichaArticulo0PRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlFichaArticulo0PRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlFichaArticulo0MARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlFichaArticulo0MARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlFichaArticulo0MARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlFichaArticulo0EDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlFichaArticulo0REFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlFichaArticulo0PERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlFichaArticulo0CADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlFichaArticulo0TIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlFichaArticulo0TIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlFichaArticulo0SWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlFichaArticulo0SWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlFichaArticulo0SWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlFichaArticulo0SWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlFichaArticulo0SWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlFichaArticulo0SWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlFichaArticulo0TIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlFichaArticulo0TPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlFichaArticulo0TCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlFichaArticulo0TEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlFichaArticulo0AUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlFichaArticulo0OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlFichaArticulo0STOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlFichaArticulo0STOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlFichaArticulo0FAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlFichaArticulo0FAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlFichaArticulo0ORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlFichaArticulo0STOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlFichaArticulo0STOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlFichaArticulo0STOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlFichaArticulo0REPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlFichaArticulo0UNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlFichaArticulo0UNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlFichaArticulo0IMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlFichaArticulo0COD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlFichaArticulo0GRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlFichaArticulo0SUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlFichaArticulo0SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlFichaArticulo0FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlFichaArticulo0FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlFichaArticulo0FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlFichaArticulo0NOMAUTOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMAUTOR'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticulo0NOMEDITOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMEDITOR'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticulo0CONTENIDO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CONTENIDO'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticulo0TIPODEPRODUCTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TIPODEPRODUCTO'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticulo0NOMTEMATICA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMTEMATICA'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object sqlFichaArticulo0NOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlFichaArticulo0TPCIVA1: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCIVA1'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCIVA'
      KeyFields = 'TIVA'
      Lookup = True
    end
    object sqlFichaArticulo0TPCRE1: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCRE1'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCRE'
      KeyFields = 'TIVA'
      Lookup = True
    end
    object sqlFichaArticulo0TPCIVA2: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCIVA2'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCIVA'
      KeyFields = 'TIVA2'
      Lookup = True
    end
    object sqlFichaArticulo0TPCRE2: TFloatField
      FieldKind = fkLookup
      FieldName = 'TPCRE2'
      LookupDataSet = sqlTIVA1
      LookupKeyFields = 'TIVA'
      LookupResultField = 'TPCRE'
      KeyFields = 'TIVA2'
      Lookup = True
    end
  end
  object SP_GEN_ARTICULO0: TFDStoredProc
    Connection = DMppal.FDConnection0
    StoredProcName = 'SP_GEN_ARTICULO'
    Left = 320
    Top = 8
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlComprobarBarras: TFDQuery
    BeforeEdit = sqlFichaArticuloBeforeEdit
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select A.* '
      'from FMARTICULO A'
      'WHERE A.BARRAS = '#39#39
      'order by A.DESCRIPCION')
    Left = 114
    Top = 120
    object sqlComprobarBarrasID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlComprobarBarrasTBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlComprobarBarrasBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlComprobarBarrasADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlComprobarBarrasFECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlComprobarBarrasNADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlComprobarBarrasDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlComprobarBarrasDESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlComprobarBarrasIBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlComprobarBarrasISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlComprobarBarrasEDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlComprobarBarrasPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlComprobarBarrasPRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlComprobarBarrasPRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlComprobarBarrasPRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlComprobarBarrasPRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlComprobarBarrasPRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlComprobarBarrasPRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlComprobarBarrasTPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlComprobarBarrasTPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlComprobarBarrasTPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlComprobarBarrasTPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlComprobarBarrasTPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlComprobarBarrasTPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlComprobarBarrasTPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlComprobarBarrasPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlComprobarBarrasPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlComprobarBarrasPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlComprobarBarrasPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlComprobarBarrasMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlComprobarBarrasMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlComprobarBarrasMARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlComprobarBarrasEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlComprobarBarrasREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlComprobarBarrasPERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlComprobarBarrasCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlComprobarBarrasTIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlComprobarBarrasTIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlComprobarBarrasSWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlComprobarBarrasSWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlComprobarBarrasSWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlComprobarBarrasSWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlComprobarBarrasSWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlComprobarBarrasSWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlComprobarBarrasTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlComprobarBarrasTPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlComprobarBarrasTCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlComprobarBarrasTEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlComprobarBarrasAUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlComprobarBarrasOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlComprobarBarrasSTOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlComprobarBarrasSTOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlComprobarBarrasFAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlComprobarBarrasFAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlComprobarBarrasORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlComprobarBarrasSTOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlComprobarBarrasSTOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlComprobarBarrasSTOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlComprobarBarrasREPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlComprobarBarrasUNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlComprobarBarrasUNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlComprobarBarrasIMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlComprobarBarrasCOD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlComprobarBarrasGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlComprobarBarrasSUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlComprobarBarrasSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlComprobarBarrasFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlComprobarBarrasFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlComprobarBarrasFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
  end
end
