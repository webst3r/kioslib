unit uDevolNuevo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox;

type
  TFormDevolNuevo = class(TUniForm)
    dsCabe1: TDataSource;
    dsProveS: TDataSource;
    lbAccion: TUniLabel;
    pcDetalle: TUniPageControl;
    tabEdit: TUniTabSheet;
    dsCabeNuevo: TDataSource;
    cbDistri: TUniDBLookupComboBox;
    UniLabel3: TUniLabel;
    edPaquete: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    btGrabarGrupo: TUniButton;
    btCancelar: TUniButton;
    edFecha: TUniDBDateTimePicker;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniLabel8: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    btGrabarIndividual: TUniButton;
    tabNuevo: TUniTabSheet;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    UniLabel1: TUniLabel;
    edPaqueteNuevo: TUniDBEdit;
    UniLabel2: TUniLabel;
    UniLabel9: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    btAceptarAgru: TUniButton;
    UniButton2: TUniButton;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniLabel10: TUniLabel;
    UniLabel11: TUniLabel;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniLabel12: TUniLabel;
    UniDBEdit5: TUniDBEdit;
    btAceptarIndv: TUniButton;

    procedure btCancelarClick(Sender: TObject);
    procedure cbDistriChange(Sender: TObject);
    procedure btGrabarIndividualClick(Sender: TObject);
    procedure btGrabarClick(Sender: TObject);
    procedure btGrabarGrupoClick(Sender: TObject);

  private
    procedure RutCompruebaCampos;


    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    //vTipo : integer;
    vHora : Double;

    //swEdit : Boolean;

    procedure RutInicioForm(swEdit: Boolean; vTipo: Integer);

  end;

function FormDevolNuevo: TFormDevolNuevo;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDevolLista, uDevolMenu, uDMDevol, uDevolFicha;

function FormDevolNuevo: TFormDevolNuevo;
begin
  Result := TFormDevolNuevo(DMppal.GetFormInstance(TFormDevolNuevo));

end;

procedure TFormDevolNuevo.btGrabarClick(Sender: TObject);
begin
  //se usa en caso de ser una modificacion/edicion
  if DMDevolucion.sqlCabeNuevo.State = dsEdit then DMDevolucion.sqlCabeNuevo.Post;
  Self.Close;
end;

procedure TFormDevolNuevo.btGrabarGrupoClick(Sender: TObject);
var
  vIDCabe : String;
begin
  RutCompruebaCampos;

  DMDevolucion.GrabarCabeNuevo(22);
  vIDCabe := ' > 0 ';
  DMDevolucion.RutAbrirAlbaran(vIDCabe,' = 22 ',1,FormDevolLista.edDesdeFecha.DateTime,FormDevolLista.edHastaFecha.DateTime,True);

  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then FormDevolLista.btFichaAgrupadaClick(nil);


  Self.Close;

  if DMPpal.swCrear then
  begin
    DMDevolucion.RutLocateCabe(DMPpal.vID);
    DMDevolucion.RutLeerCodigoBarras;
    DMDevolucion.sqlLinea.Refresh;
    DMppal.swCrear := false;
  end;
end;

procedure TFormDevolNuevo.RutCompruebaCampos;
begin
  if (DMDevolucion.sqlCabeNuevoNOMBRE.AsString = '')
      or (DMDevolucion.sqlCabeNuevoDOCTOPROVE.AsString = '')
      or (DMDevolucion.sqlProveSMAXPAQUETE.AsInteger <= 0) then Abort;
end;

procedure TFormDevolNuevo.btGrabarIndividualClick(Sender: TObject);
var
  vIDCabe : String;
begin
  RutCompruebaCampos;

  DMDevolucion.GrabarCabeNuevo(2);

  vIDCabe := ' = ' + IntToStr(DMDevolucion.SP_GEN_STOCABE.Params[0].Value);

  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then FormDevolLista.btFichaIndividualClick(nil);

  DMDevolucion.RutAbrirAlbaran(vIDCabe,' = 2' ,0,FormDevolLista.edDesdeFecha.DateTime,FormDevolLista.edHastaFecha.DateTime,true);

//  DMDevolucion.RutAbrirAlbaran();
  Self.Close;
  FormDevolFicha.pBarrasA.SetFocus;
  FormDevolFicha.swUnico := true;
end;

procedure TFormDevolNuevo.btCancelarClick(Sender: TObject);
begin
  DMDevolucion.CancelarCabeNuevo;
 // if not swEdit then  DMDevolucion.RutBorrarDevolucion;//sw para controlar si entra a modificar o nuevo
  Self.Close;
end;

procedure TFormDevolNuevo.cbDistriChange(Sender: TObject);
begin
  DMDevolucion.RutGrabarProvee;

//    DMDevolucion.sqlCabeNuevoMAXPAQUETE.AsInteger   := DMDevolucion.sqlProveSMAXPAQUETE.AsInteger;

//    DMDevolucion.sqlCabeNuevoID_PROVEEDOR.AsInteger := DMDevolucion.sqlProveSID_PROVEEDOR.AsInteger;
//    DMDevolucion.sqlCabeNuevoNIF.AsString           := DMDevolucion.sqlProveSNIF.AsString;
end;

procedure TFormDevolNuevo.RutInicioForm(swEdit : Boolean; vTipo : Integer);
begin
  pcDetalle.ActivePage := tabNuevo;
  lbAccion.Caption := 'Nueva devolucion';


  if swEdit then
  begin
    pcDetalle.ActivePage := tabEdit;
    btGrabarIndividual.Visible := False;
    btGrabarGrupo.Caption := 'Aceptar';
    btGrabarGrupo.OnClick := btGrabarClick;
    lbAccion.Caption := 'Modificar devolucion';
  end;

 // 0 = Agrupada ; 1 = Individual
  if vTipo = 0 then
  begin
    btAceptarAgru.Top  := 143;
    btAceptarAgru.Left := 320;

    btAceptarIndv.Visible := False;
  end;

  if vTipo = 1 then
  begin
    btAceptarIndv.Top  := 143;
    btAceptarIndv.Left := 320;

    btAceptarAgru.Visible := False;
  end;
end;

end.

