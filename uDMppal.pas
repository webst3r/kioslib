unit uDMppal;

interface

uses
  Dialogs,
  uniGUIMainModule, SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Datasnap.Provider, Datasnap.DBClient,
  FireDAC.Comp.DataSet,
  uniPageControl, Variants,
  uniDBGrid,
  System.IniFiles, uniGUIForm,
  Windows, math, uniGUIBaseClasses, uniGUIClasses, uniImageList;      //copyfile


type
  TDMppal = class(TUniGUIMainModule)
    sqlHisArtiDev1: TFDQuery;
    sqlUsuario: TFDQuery;
    FDConnection0: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    sqlHisArtiCom1: TFDQuery;
    sqlUsu: TFDQuery;
    sqlUsuID: TStringField;
    sqlUsuPASS: TStringField;
    sqlUsuGRUPO: TIntegerField;
    sqlUsuNOMBRE: TStringField;
    sqlUsuFORMINICIO: TStringField;
    sqlUsuDEPARTAMENTO: TIntegerField;
    sqlUsuPATHACTUORIG: TStringField;
    sqlUsuPATHACTUDEST: TStringField;
    sqlUsuVARIFICACIONAUTO: TSmallintField;
    sqlUsuMOSTRARVENTANA: TSmallintField;
    sqlUsuACTUALIZACIONAUTO: TSmallintField;
    sqlUsuFTPUSUARIO: TStringField;
    sqlUsuFTPPASSWORD: TStringField;
    sqlUsuCONTROLTELEFONO: TStringField;
    sqlUsuENVIARMENSAJE: TStringField;
    sqlUsuREVISARCAMBIOS: TStringField;
    sqlUsuPATHUSUARIODB: TStringField;
    sqlUsuMENSAJE: TBlobField;
    sqlUsuGRUPO1: TStringField;
    sqlUsuGRUPO2: TStringField;
    sqlUsuGRUPO3: TStringField;
    sqlUsuGRUPO4: TStringField;
    sqlUsuGRUPO5: TStringField;
    sqlUsuVISIBLE1: TSmallintField;
    sqlUsuVISIBLE2: TSmallintField;
    sqlUsuVISIBLE3: TSmallintField;
    sqlUsuVISIBLE4: TSmallintField;
    sqlUsuVISIBLE5: TSmallintField;
    sqlUsuIDIOMA: TIntegerField;
    sqlUsuSUSTITUIR: TBlobField;
    sqlUsuNUMEROUSUARIO: TIntegerField;
    sqlUsuREPRESENTANTE: TStringField;
    sqlUsuAux: TFDQuery;
    sqlUsuAuxID: TStringField;
    sqlUsuAuxPASS: TStringField;
    sqlUsuAuxGRUPO: TIntegerField;
    sqlUsuAuxNOMBRE: TStringField;
    sqlUsuAuxFORMINICIO: TStringField;
    sqlUsuAuxDEPARTAMENTO: TIntegerField;
    sqlUsuAuxPATHACTUORIG: TStringField;
    sqlUsuAuxPATHACTUDEST: TStringField;
    sqlUsuAuxVARIFICACIONAUTO: TSmallintField;
    sqlUsuAuxMOSTRARVENTANA: TSmallintField;
    sqlUsuAuxACTUALIZACIONAUTO: TSmallintField;
    sqlUsuAuxFTPUSUARIO: TStringField;
    sqlUsuAuxFTPPASSWORD: TStringField;
    sqlUsuAuxCONTROLTELEFONO: TStringField;
    sqlUsuAuxENVIARMENSAJE: TStringField;
    sqlUsuAuxREVISARCAMBIOS: TStringField;
    sqlUsuAuxPATHUSUARIODB: TStringField;
    sqlUsuAuxMENSAJE: TBlobField;
    sqlUsuAuxGRUPO1: TStringField;
    sqlUsuAuxGRUPO2: TStringField;
    sqlUsuAuxGRUPO3: TStringField;
    sqlUsuAuxGRUPO4: TStringField;
    sqlUsuAuxGRUPO5: TStringField;
    sqlUsuAuxVISIBLE1: TSmallintField;
    sqlUsuAuxVISIBLE2: TSmallintField;
    sqlUsuAuxVISIBLE3: TSmallintField;
    sqlUsuAuxVISIBLE4: TSmallintField;
    sqlUsuAuxVISIBLE5: TSmallintField;
    sqlUsuAuxIDIOMA: TIntegerField;
    sqlUsuAuxSUSTITUIR: TBlobField;
    sqlUsuAuxNUMEROUSUARIO: TIntegerField;
    sqlUsuAuxREPRESENTANTE: TStringField;
    sqlTipoNOSIOtros: TFDQuery;
    sqlArticulos: TFDQuery;
    sqlUsuarioID: TStringField;
    sqlUsuarioPASS: TStringField;
    sqlUsuarioNUMEROUSUARIO: TIntegerField;
    sqlUsuarioGRUPO: TIntegerField;
    sqlUsuarioGRUPO1: TStringField;
    sqlUsuarioGRUPO2: TStringField;
    sqlUsuarioGRUPO3: TStringField;
    sqlUsuarioVISIBLE1: TSmallintField;
    sqlUsuarioVISIBLE2: TSmallintField;
    sqlUsuarioVISIBLE3: TSmallintField;
    sqlUsuarioVISIBLE4: TSmallintField;
    sqlUsuarioVISIBLE5: TSmallintField;
    sqlUsuarioNOMBRE: TStringField;
    sqlArti1: TFDQuery;
    SP_GEN_HISARTI1: TFDStoredProc;
    FDConnection1: TFDConnection;
    sqlHisArtiDev0: TFDQuery;
    sqlArti0: TFDQuery;
    SP_GEN_HISARTI0: TFDStoredProc;
    sqlHisArtiCom0: TFDQuery;
    sqlUpdate: TFDQuery;
    SP_GEN_ARTICULO: TFDStoredProc;
    sqlArti0ID_ARTICULO: TIntegerField;
    sqlArti0TBARRAS: TIntegerField;
    sqlArti0BARRAS: TStringField;
    sqlArti0ADENDUM: TStringField;
    sqlArti0FECHAADENDUM: TDateField;
    sqlArti0NADENDUM: TIntegerField;
    sqlArti0DESCRIPCION: TStringField;
    sqlArti0DESCRIPCION2: TStringField;
    sqlArti0IBS: TStringField;
    sqlArti0ISBN: TStringField;
    sqlArti0EDITOR: TIntegerField;
    sqlArti0PRECIO1: TSingleField;
    sqlArti0PRECIO2: TSingleField;
    sqlArti0PRECIO3: TSingleField;
    sqlArti0PRECIO4: TSingleField;
    sqlArti0PRECIO5: TSingleField;
    sqlArti0PRECIO6: TSingleField;
    sqlArti0PRECIO7: TSingleField;
    sqlArti0TPCENCARTE1: TSingleField;
    sqlArti0TPCENCARTE2: TSingleField;
    sqlArti0TPCENCARTE3: TSingleField;
    sqlArti0TPCENCARTE4: TSingleField;
    sqlArti0TPCENCARTE5: TSingleField;
    sqlArti0TPCENCARTE6: TSingleField;
    sqlArti0TPCENCARTE7: TSingleField;
    sqlArti0PRECIOCOSTE: TSingleField;
    sqlArti0PRECIOCOSTE2: TSingleField;
    sqlArti0PRECIOCOSTETOT: TSingleField;
    sqlArti0PRECIOCOSTETOT2: TSingleField;
    sqlArti0MARGEN: TSingleField;
    sqlArti0MARGEN2: TSingleField;
    sqlArti0MARGENMAXIMO: TSingleField;
    sqlArti0EDITORIAL: TIntegerField;
    sqlArti0REFEPROVE: TStringField;
    sqlArti0PERIODICIDAD: TIntegerField;
    sqlArti0CADUCIDAD: TIntegerField;
    sqlArti0TIVA: TIntegerField;
    sqlArti0TIVA2: TIntegerField;
    sqlArti0SWRECARGO: TSmallintField;
    sqlArti0SWACTIVADO: TSmallintField;
    sqlArti0SWINGRESO: TSmallintField;
    sqlArti0SWCONTROLFECHA: TSmallintField;
    sqlArti0SWTPRECIO: TSmallintField;
    sqlArti0SWORIGEN: TIntegerField;
    sqlArti0TIPO: TStringField;
    sqlArti0TPRODUCTO: TIntegerField;
    sqlArti0TCONTENIDO: TIntegerField;
    sqlArti0TEMATICA: TIntegerField;
    sqlArti0AUTOR: TIntegerField;
    sqlArti0OBSERVACIONES: TMemoField;
    sqlArti0STOCKMINIMO: TSingleField;
    sqlArti0STOCKMAXIMO: TSingleField;
    sqlArti0FAMILIA: TStringField;
    sqlArti0FAMILIAVENTA: TIntegerField;
    sqlArti0ORDENVENTA: TIntegerField;
    sqlArti0STOCKMINIMOESTANTE: TSingleField;
    sqlArti0STOCKMAXIMOESTANTE: TSingleField;
    sqlArti0STOCKVENTAESTANTE: TSingleField;
    sqlArti0REPONERESTANTE: TSingleField;
    sqlArti0UNIDADESCOMPRA: TSingleField;
    sqlArti0UNIDADESVENTA: TSingleField;
    sqlArti0IMAGEN: TStringField;
    sqlArti0COD_ARTICU: TStringField;
    sqlArti0GRUPO: TIntegerField;
    sqlArti0SUBGRUPO: TIntegerField;
    sqlArti0SWALTABAJA: TSmallintField;
    sqlArti0FECHABAJA: TDateField;
    sqlArti0FECHAALTA: TDateField;
    sqlArti0FECHAULTI: TDateField;
    sqlArti0NOMPROVE: TStringField;
    sqlArti1ID_ARTICULO: TIntegerField;
    sqlArti1TBARRAS: TIntegerField;
    sqlArti1BARRAS: TStringField;
    sqlArti1ADENDUM: TStringField;
    sqlArti1FECHAADENDUM: TDateField;
    sqlArti1NADENDUM: TIntegerField;
    sqlArti1DESCRIPCION: TStringField;
    sqlArti1DESCRIPCION2: TStringField;
    sqlArti1IBS: TStringField;
    sqlArti1ISBN: TStringField;
    sqlArti1EDITOR: TIntegerField;
    sqlArti1PRECIO1: TSingleField;
    sqlArti1PRECIO2: TSingleField;
    sqlArti1PRECIO3: TSingleField;
    sqlArti1PRECIO4: TSingleField;
    sqlArti1PRECIO5: TSingleField;
    sqlArti1PRECIO6: TSingleField;
    sqlArti1PRECIO7: TSingleField;
    sqlArti1TPCENCARTE1: TSingleField;
    sqlArti1TPCENCARTE2: TSingleField;
    sqlArti1TPCENCARTE3: TSingleField;
    sqlArti1TPCENCARTE4: TSingleField;
    sqlArti1TPCENCARTE5: TSingleField;
    sqlArti1TPCENCARTE6: TSingleField;
    sqlArti1TPCENCARTE7: TSingleField;
    sqlArti1PRECIOCOSTE: TSingleField;
    sqlArti1PRECIOCOSTE2: TSingleField;
    sqlArti1PRECIOCOSTETOT: TSingleField;
    sqlArti1PRECIOCOSTETOT2: TSingleField;
    sqlArti1MARGEN: TSingleField;
    sqlArti1MARGEN2: TSingleField;
    sqlArti1MARGENMAXIMO: TSingleField;
    sqlArti1EDITORIAL: TIntegerField;
    sqlArti1REFEPROVE: TStringField;
    sqlArti1PERIODICIDAD: TIntegerField;
    sqlArti1CADUCIDAD: TIntegerField;
    sqlArti1TIVA: TIntegerField;
    sqlArti1TIVA2: TIntegerField;
    sqlArti1SWRECARGO: TSmallintField;
    sqlArti1SWACTIVADO: TSmallintField;
    sqlArti1SWINGRESO: TSmallintField;
    sqlArti1SWCONTROLFECHA: TSmallintField;
    sqlArti1SWTPRECIO: TSmallintField;
    sqlArti1SWORIGEN: TIntegerField;
    sqlArti1TIPO: TStringField;
    sqlArti1TPRODUCTO: TIntegerField;
    sqlArti1TCONTENIDO: TIntegerField;
    sqlArti1TEMATICA: TIntegerField;
    sqlArti1AUTOR: TIntegerField;
    sqlArti1OBSERVACIONES: TMemoField;
    sqlArti1STOCKMINIMO: TSingleField;
    sqlArti1STOCKMAXIMO: TSingleField;
    sqlArti1FAMILIA: TStringField;
    sqlArti1FAMILIAVENTA: TIntegerField;
    sqlArti1ORDENVENTA: TIntegerField;
    sqlArti1STOCKMINIMOESTANTE: TSingleField;
    sqlArti1STOCKMAXIMOESTANTE: TSingleField;
    sqlArti1STOCKVENTAESTANTE: TSingleField;
    sqlArti1REPONERESTANTE: TSingleField;
    sqlArti1UNIDADESCOMPRA: TSingleField;
    sqlArti1UNIDADESVENTA: TSingleField;
    sqlArti1IMAGEN: TStringField;
    sqlArti1COD_ARTICU: TStringField;
    sqlArti1GRUPO: TIntegerField;
    sqlArti1SUBGRUPO: TIntegerField;
    sqlArti1SWALTABAJA: TSmallintField;
    sqlArti1FECHABAJA: TDateField;
    sqlArti1FECHAALTA: TDateField;
    sqlArti1FECHAULTI: TDateField;
    sqlArti1NOMPROVE: TStringField;
    sqlUpdate2: TFDQuery;
    sqlMensaje: TFDQuery;
    sqlMensajeID_MENSAJE: TIntegerField;
    sqlMensajeFECHA: TDateField;
    sqlMensajeHORA: TTimeField;
    sqlMensajeMENSAJE: TMemoField;
    sqlMensajeTPV: TIntegerField;
    SP_GEN_MENSAJE_ID: TFDStoredProc;
    sqlMensajesGeneral: TFDQuery;
    sqlMensajesGeneralID_MENSAJE: TIntegerField;
    sqlMensajesGeneralFECHA: TDateField;
    sqlMensajesGeneralHORA: TTimeField;
    sqlMensajesGeneralMENSAJE: TMemoField;
    sqlMensajesGeneralTPV: TIntegerField;
    sqlUsuarioTIPOPASSW: TIntegerField;
    sqlUsuarioCONTROLTELEFONO: TStringField;
    sqlUsuarioFECHAPASSWORD: TDateField;
    sqlUsuarioACTUALIZACIONAUTO: TSmallintField;
    sqlUsuRUTAORIGEN: TStringField;
    sqlUsuNALMACEN: TIntegerField;
    sqlUsuTIPOPASSW: TIntegerField;
    sqlUsuFECHAPASSWORD: TDateField;
    sqlProve: TFDQuery;
    sqlProveedor2: TFDQuery;
    sqlProveedor0: TFDQuery;
    sqlHisArtiProve: TFDQuery;
    sqlHisArtiProveIDSTOCABE: TIntegerField;
    sqlHisArtiProveSWTIPODOCU: TSmallintField;
    sqlHisArtiProveNDOCSTOCK: TIntegerField;
    sqlHisArtiProveFECHA: TDateField;
    sqlHisArtiProveNALMACEN: TIntegerField;
    sqlHisArtiProveNALMACENORIGEN: TIntegerField;
    sqlHisArtiProveNALMACENDESTINO: TIntegerField;
    sqlHisArtiProveID_PROVEEDOR: TIntegerField;
    sqlHisArtiProveID_DIRECCION: TIntegerField;
    sqlHisArtiProveFABRICANTE: TStringField;
    sqlHisArtiProveSWDOCTOPROVE: TSmallintField;
    sqlHisArtiProveDOCTOPROVE: TStringField;
    sqlHisArtiProveDOCTOPROVEFECHA: TDateField;
    sqlHisArtiProveFECHACARGO: TDateField;
    sqlHisArtiProveFECHAABONO: TDateField;
    sqlHisArtiProveNOMBRE: TStringField;
    sqlHisArtiProveNIF: TStringField;
    sqlHisArtiProveMENSAJEAVISO: TStringField;
    sqlHisArtiProveOBSERVACIONES: TMemoField;
    sqlHisArtiProvePAQUETE: TIntegerField;
    sqlHisArtiProveSWDEVOLUCION: TSmallintField;
    sqlHisArtiProveSWCERRADO: TSmallintField;
    sqlHisArtiProveSWFRATIENDA: TSmallintField;
    sqlHisArtiProveDEVOLUSWTIPOINC: TSmallintField;
    sqlHisArtiProveDEVOLUFECHASEL: TDateField;
    sqlHisArtiProveDEVOLUDOCTOPROVE: TStringField;
    sqlHisArtiProveDEVOLUIDSTOCABE: TIntegerField;
    sqlHisArtiProveID_FRAPROVE: TIntegerField;
    sqlHisArtiProveSWMARCA: TSmallintField;
    sqlHisArtiProveSWMARCA2: TSmallintField;
    sqlHisArtiProveSWMARCA3: TSmallintField;
    sqlHisArtiProveSWMARCA4: TSmallintField;
    sqlHisArtiProveSWMARCA5: TSmallintField;
    sqlHisArtiProveSEMANA: TSmallintField;
    sqlHisArtiProveSWALTABAJA: TSmallintField;
    sqlHisArtiProveFECHABAJA: TDateField;
    sqlHisArtiProveFECHAALTA: TDateField;
    sqlHisArtiProveHORAALTA: TTimeField;
    sqlHisArtiProveFECHAULTI: TDateField;
    sqlHisArtiProveHORAULTI: TTimeField;
    sqlHisArtiProveUSUULTI: TStringField;
    sqlHisArtiProveNOTAULTI: TStringField;
    sqlArticulos2: TFDQuery;
    IntegerField23: TIntegerField;
    IntegerField24: TIntegerField;
    StringField30: TStringField;
    StringField31: TStringField;
    DateField5: TDateField;
    IntegerField25: TIntegerField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    IntegerField26: TIntegerField;
    SingleField30: TSingleField;
    SingleField31: TSingleField;
    SingleField32: TSingleField;
    SingleField33: TSingleField;
    SingleField34: TSingleField;
    SingleField35: TSingleField;
    SingleField36: TSingleField;
    SingleField37: TSingleField;
    SingleField38: TSingleField;
    SingleField39: TSingleField;
    SingleField40: TSingleField;
    SingleField41: TSingleField;
    SingleField42: TSingleField;
    SingleField43: TSingleField;
    SingleField44: TSingleField;
    SingleField45: TSingleField;
    SingleField46: TSingleField;
    SingleField47: TSingleField;
    SingleField48: TSingleField;
    SingleField49: TSingleField;
    SingleField50: TSingleField;
    IntegerField27: TIntegerField;
    StringField36: TStringField;
    IntegerField28: TIntegerField;
    IntegerField29: TIntegerField;
    IntegerField30: TIntegerField;
    IntegerField31: TIntegerField;
    SmallintField15: TSmallintField;
    SmallintField16: TSmallintField;
    SmallintField17: TSmallintField;
    SmallintField18: TSmallintField;
    SmallintField19: TSmallintField;
    IntegerField32: TIntegerField;
    StringField37: TStringField;
    IntegerField33: TIntegerField;
    IntegerField34: TIntegerField;
    IntegerField35: TIntegerField;
    IntegerField36: TIntegerField;
    MemoField2: TMemoField;
    SingleField51: TSingleField;
    SingleField52: TSingleField;
    StringField38: TStringField;
    IntegerField37: TIntegerField;
    IntegerField38: TIntegerField;
    SingleField53: TSingleField;
    SingleField54: TSingleField;
    SingleField55: TSingleField;
    SingleField56: TSingleField;
    SingleField57: TSingleField;
    SingleField58: TSingleField;
    StringField39: TStringField;
    StringField40: TStringField;
    IntegerField39: TIntegerField;
    IntegerField40: TIntegerField;
    SmallintField20: TSmallintField;
    DateField6: TDateField;
    DateField7: TDateField;
    DateField8: TDateField;
    sqlArticulosBARRAS: TStringField;
    sqlArticulosADENDUM: TStringField;
    sqlArticulosDESCRIPCION: TStringField;
    sqlArticulosPRECIO1: TSingleField;
    sqlArticulosPRECIOCOSTE: TSingleField;
    sqlArticulosEDITORIAL: TIntegerField;
    sqlArticulosREFEPROVE: TStringField;
    sqlArticulosNOMBREEDITORIAL: TStringField;
    sqlTBMem: TFDMemTable;
    sqlTBMemCampo: TStringField;
    sqlArticulosID_ARTICULO: TIntegerField;
    sqlFGTIVA: TFDQuery;
    sqlFGTIVATIVA: TIntegerField;
    sqlFGTIVATIPOIVA: TStringField;
    sqlFGTIVATPCIVA: TSingleField;
    sqlFGTIVACTACONTABLE: TStringField;
    sqlFGTIVATPCRE: TSingleField;
    sqlFGTIVACTARECARGO: TStringField;
    sqlFGTIVAFECHAALTA: TDateField;
    sqlFGTIVAFECHAUTI: TDateField;
    sqlFGTIVAHORAULTI: TTimeField;
    sqlFGTIVAUSUULTI: TStringField;
    sqlFGTIVANOTAULTI: TStringField;
    sqlFGTIVAVALDESDE: TDateField;
    sqlFGTIVAVALHASTA: TDateField;
    ImgListPrincipal: TUniNativeImageList;
    sqlProveID_PROVEEDOR: TIntegerField;
    sqlProveNOMBRE: TStringField;
    sqlProveREFEPROVEEDOR: TStringField;
    sqlProveRUTA: TStringField;
    sqlProveTDTO: TIntegerField;
    sqlEmpresa: TFDQuery;
    sqlEmpresaNREFERENCIA: TIntegerField;
    sqlEmpresaNOMBRE: TStringField;
    sqlEmpresaNOMBRE2: TStringField;
    sqlEmpresaTVIA: TStringField;
    sqlEmpresaVIANOMBRE: TStringField;
    sqlEmpresaVIANUM: TStringField;
    sqlEmpresaVIABLOC: TStringField;
    sqlEmpresaVIABIS: TStringField;
    sqlEmpresaVIAESCA: TStringField;
    sqlEmpresaVIAPISO: TStringField;
    sqlEmpresaVIAPUERTA: TStringField;
    sqlEmpresaDIRECCION: TStringField;
    sqlEmpresaDIRECCION2: TStringField;
    sqlEmpresaPOBLACION: TStringField;
    sqlEmpresaPROVINCIA: TStringField;
    sqlEmpresaCPOSTAL: TStringField;
    sqlEmpresaCPAIS: TStringField;
    sqlEmpresaCPROVINCIA: TStringField;
    sqlEmpresaNIF: TStringField;
    sqlEmpresaCONTACTO1NOMRE: TStringField;
    sqlEmpresaCONTACTO1CARGO: TStringField;
    sqlEmpresaCONTACTO2NOMBRE: TStringField;
    sqlEmpresaCONTACTO2CARGO: TStringField;
    sqlEmpresaTELEFONO1: TStringField;
    sqlEmpresaTELEFONO2: TStringField;
    sqlEmpresaMOVIL: TStringField;
    sqlEmpresaFAX: TStringField;
    sqlEmpresaEMAIL: TStringField;
    sqlEmpresaWEB: TStringField;
    sqlEmpresaMENSAJEAVISO: TStringField;
    sqlEmpresaOBSERVACIONES: TMemoField;
    sqlEmpresaNACTIVIDAD: TIntegerField;
    sqlEmpresaNSECTOR: TIntegerField;
    sqlEmpresaTPCIVA1: TSingleField;
    sqlEmpresaTPCIVA2: TSingleField;
    sqlEmpresaTPCIVA3: TSingleField;
    sqlEmpresaTPCRE1: TSingleField;
    sqlEmpresaTPCRE2: TSingleField;
    sqlEmpresaTPCRE3: TSingleField;
    sqlEmpresaNPEDIDO: TIntegerField;
    sqlEmpresaNPEDIPROVE: TIntegerField;
    sqlEmpresaNALBARAN: TIntegerField;
    sqlEmpresaNALBASERVI: TIntegerField;
    sqlEmpresaNALBACOLECTOR: TIntegerField;
    sqlEmpresaNALBALMA: TIntegerField;
    sqlEmpresaNALBAREPARA: TIntegerField;
    sqlEmpresaNFACTURANORMAL: TIntegerField;
    sqlEmpresaNFACTURASERVI: TIntegerField;
    sqlEmpresaNFACTURACOLECTOR: TIntegerField;
    sqlEmpresaNFACTURATIENDA: TIntegerField;
    sqlEmpresaNFACTURAREPARA: TIntegerField;
    sqlEmpresaNUMTICKET: TIntegerField;
    sqlEmpresaNCOMPRA: TIntegerField;
    sqlEmpresaNTRASPASO: TIntegerField;
    sqlEmpresaREGISTROMERCANTIL: TStringField;
    sqlEmpresaPIEFRANORMAL: TMemoField;
    sqlEmpresaPIEFRASERVI: TMemoField;
    sqlEmpresaPIEFRACOLLECTOR: TMemoField;
    sqlEmpresaPIEFRATIENDA: TMemoField;
    sqlEmpresaPIEFRAREPARA: TMemoField;
    sqlEmpresaIMPTOPEFRAIDE: TSingleField;
    sqlEmpresaCTACONTAVTA1: TStringField;
    sqlEmpresaCTACONTAVTA2: TStringField;
    sqlEmpresaCTACONTAVTA3: TStringField;
    sqlEmpresaCTACONTAVTA4: TStringField;
    sqlEmpresaCTACONTAVTA5: TStringField;
    sqlEmpresaCTACONTAIVA1: TStringField;
    sqlEmpresaCTACONTAIVA2: TStringField;
    sqlEmpresaCTACONTAIVA3: TStringField;
    sqlEmpresaCTACONTARE1: TStringField;
    sqlEmpresaCTACONTARE2: TStringField;
    sqlEmpresaCTACONTARE3: TStringField;
    sqlEmpresaTPCINCRECOMPRACESIO: TSingleField;
    sqlEmpresaTPCINCRECOMPRAVENTA: TSingleField;
    sqlEmpresaNALMACEN: TIntegerField;
    sqlEmpresaRUTAFTP: TStringField;
    sqlEmpresaRUTAIMAGENFTP: TStringField;
    sqlEmpresaRUTAIMAGENPC: TStringField;
    sqlEmpresaRUTACAMPO: TIntegerField;
    sqlEmpresaRUTAAVISOSFTP: TStringField;
    sqlEmpresaRUTAAVISOSPC: TStringField;
    sqlEmpresaHOSTFTP: TStringField;
    sqlEmpresaUSUFTP: TStringField;
    sqlEmpresaPASSFTP: TStringField;
    sqlEmpresaDIASRECEGEN: TSmallintField;
    sqlEmpresaTRANSPORTE: TSmallintField;
    sqlEmpresaSWRECARGOE: TSmallintField;
    sqlEmpresaSWALTABAJA: TSmallintField;
    sqlEmpresaFECHABAJA: TDateField;
    sqlEmpresaFECHAALTA: TDateField;
    sqlEmpresaHORAALTA: TTimeField;
    sqlEmpresaFECHAULTI: TDateField;
    sqlEmpresaHORAULTI: TTimeField;
    sqlEmpresaUSUULTI: TStringField;
    sqlEmpresaNOTAULTI: TStringField;
    procedure UniGUIMainModuleCreate(Sender: TObject);
    procedure UniGUIMainModuleNewComponent(AComponent: TComponent);


  private
    procedure RutLeerTipoIVA(zTIVA: Integer);

    function RutTexFechaSQL(zTexGuia, zData: String): String;

    function RutTpcDeR(zCampA, zCampB: Double; zDecim: Integer): Double;






    { Private declarations }
  public
    { Public declarations }
//    LoggedUser : string;

    vStatus : String;

    vProveedorAuxiliares : string;


    vHost, vUser, vPass, vFrom, vBack  : string;
    vPort : Integer;
    vSujeto, vEmails, vDestinatario, vMailing : string;
    vBody : TStringList;

    vg_Barres, vg_Adendum, vBarres, vAdendum : String;
    vg_Empresa : Integer;
    vBeneficio, vBeneficio2 : Double;

  vDesdeFecha, vHastaFecha : TDate;

    vID : Integer;
  swCrear : Boolean;
  vTTercero, vTTContacto : string;
  vPath : string;

  vANY, vMES, vDIA : Integer;
  vANY1, vMES1, vDIA1 : Integer;
  vMES2, vDIA2 : Integer;
  vFecha : TDate;

  vAdmin : Boolean;
  vBD : Integer;

    vCampoFilterGrid, vValorFilterGrid : string;
    vGrid   : TUniDBGrid;
    vForm   : String;
    vColumn : TUniDBGridColumn;
    vGridX, vGridY : integer;

    //control
    swConsArti, swHisArti,
    swDevolucionLista, swDevolucionFicha,
    swArtiLista, swArtiFicha,
    swRecepcionLista, swRecepcionFicha,
    swDisitribuidoraLista, swDisitribuidoraFicha,
    swCuadreLista, swCuadreFicha        : String;



    procedure RutConsArti(vBusqueda: String; vEditorial : Integer);

    procedure RutAbrirTiposFICHA;
    procedure RutAbrirTipoNOSIOtros(vTipo:String; vOrden2: Integer);
    procedure RutCalculaMargesCaps(zTabCab: TDataSet);
    procedure RutAbrirUsuarios(vWhere:String);
    procedure RutLeerArticulos;
    Function  GetMaxNumber(Tabla, Campo : String):Integer;
    procedure ExportExcel(ADataSet: TDataSet);
    procedure ExportExcelGrid(ADataSet: TUniDbgrid);

    procedure RutHisArtiLin_CargaIVA_A(zTabHis: TFDQuery; zTxTIVA, zTxIVa,
      zTxRE: String; zTipo: Integer);
    procedure RutCrearSorting(vDataset: TClientdataset);
    procedure RutCrearSortingSQL(vDataset: TFDQuery);
    procedure SortColumn(vDataset: TClientdataset; const FieldName: string;
      Dir: Boolean);
    procedure SortColumnSQL(vDataset: TFDQuery; const FieldName: string;
      Dir: Boolean);
    procedure ProSortidaPreus(zTipo: Integer);

    function RutValidarUsuario(vUsu, vPass: String): Boolean;
    function RutTexFechas_SQL(zTexData, zDataA, zDataZ: String): String;
    procedure RutSepararCodigoBarras(vCodbarres: String);
    procedure RutAbrirTipos;
    procedure RutAbrirTiposSQL;
    procedure RutInicioForm;
    procedure RutEnviarMensaje(vMensaje: String);
    procedure RutAbrirMensaje;
    procedure RutAbrirMensajes(vDate : TDate);


    procedure RutActualizarbaseDatos;
    Function RutConectarBD(vNumUsuario : String) : Boolean;
    procedure RutAbrirTablaNuevoUsu;
    procedure RutGrabarUsuario(vPass : String; vModi : Boolean);
    Function RutCalculaImportes(vTabla :TFDQuery; vCanti:String; vDecimals,vTipo:Integer):Integer;
    function RutComprobarTopeCantitat(zTabLine: TDataSet; zTope,
      zDefault: Double; zCamp, zTipo: String): Boolean;
    function RutCalculaLinHisto_N(zTabHis: TFDQuery; zTxCanti: String;
      zDecimals, zTipo: Integer): Integer;

    Function RutCopiaBD(vNumUsuario : String) : Boolean;
    function RutGetMAXTPV: Integer;
    Function RutGrabarUsuarioTPV(vPass, vUsu : String; vNumTPV,vTipo : Integer; vModi : Boolean) : Boolean;
    procedure RutBorrarUsuarioTPV(vUsu: String);
    procedure RutPasarCampos(vTablaOrigen, vTablaDestino: TFDQuery; vTipo, vIDProve : Integer);


    function RutEdiInse(vTabla: TDataSet): Boolean;
    function RutExisteCampo(vTabla: TDataSet; vCampo: String): Boolean;
    function RutTpcR_ImpoTPC(vCamp, vTpc: Double; vDecim: Integer): Double;
    procedure RutCalculaLinHistoComisio(vTabla: TFDQuery; vDecimals,
      vTipo: Integer);
    function RutTpcDe(vCamp, vCamp2: Double; vDecim: Integer): Double;
    function CheckTablename(const TableName: string): Boolean;
    procedure CreartablasNuevas;

    procedure RutCapturaGrid( vForm: string;
      vGridOrigen: TUniDBGrid; Column: TUniDBGridColumn; X, Y, vPopUp: Integer);

    procedure RutGrabarConfiguracionDistri(vMarinaPuntoVenta, vMarinaRuta,
      vMarinaMargen, vLogisticaPuntoVenta, vLogisticaRuta, vLogisticaMargen,
      vSadePuntoVenta, vSadeRuta, vSadeMargen, vSgelPuntoVenta, vSgelRuta,
      vSgelMargen: String);



  end;

function DMppal: TDMppal;



implementation

{$R *.dfm}

uses
  uMenu, frxClass, uConstVar,
  UniGUIVars, ServerModule, uniGUIApplication, uNativeXLSExport, uDMMenuArti, uDMDevol,
  uDevolNuevo, uDevolFicha, uDMRecepcion;

function DMppal: TDMppal;
begin

  Result := TDMppal(UniApplication.UniMainModule)
end;

//INICI
//------------------------------------------------------------------------------

//uArticulos
procedure TDMppal.RutInicioForm;
begin
//abrir tablas
  //sqlArticulos.Close;
  //sqlArticulos.Open;

end;

//consultas
procedure TDMppal.RutConsArti(vBusqueda : String; vEditorial : Integer);
var
  vWhere,vAnd, vOr : String;
begin
//abrir tablas
  //sqlArticulos.Close;
  //sqlArticulos.Open;
  vWhere := 'WHERE FA.EDITORIAL > 0';
  if vEditorial <> 0 then   vWhere := 'WHERE FA.EDITORIAL = ' + IntToStr(vEditorial);

  vOr   := '';
  if vBusqueda <> '' then
  begin
    //vWhere := 'WHERE FA.ID_ARTICULO LIKE '      + QuotedStr('%'+vBusqueda+'%');
    vOr   := vOr + ' AND (UPPER(FA.BARRAS)    LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ' OR UPPER(FA.ADENDUM)     LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ' OR UPPER(FA.DESCRIPCION) LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ' OR UPPER(FP.NOMBRE)      LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
    vOr   := vOr + ')'
  end;

  with sqlArticulos do
  begin
    Close;
    sqlArticulos.SQL.Text := Copy(SQL.Text, 1,
                pos('WHERE FA.EDITORIAL', Uppercase(SQL.Text))-1)
              + vWhere
              + vOr
              + ' ORDER BY 1';
    Open;

  end;
end;


//------------------------------------------------------------------------------

function TDMppal.RutValidarUsuario(vUsu, vPass:String):Boolean;
var
  vWhere : string;
  vCalculo1, vCalculo2 : string;
begin
  Result := False;

  vWhere := 'WHERE U.ID = ' + QuotedStr(vUsu);// + ' and U.PASS = ' + QuotedStr(vPass);
//  if vUsu = '???' then
//  vWhere := 'WHERE ID > ' + QuotedStr('');


  with sqlUsuario do
  begin
    Close;
    sqlUsuario.SQL.Text := Copy(SQL.Text, 1,
                pos('WHERE', Uppercase(SQL.Text))-1)
              + vWhere
              + ' ORDER BY 1';
    Open;

    if RecordCount = 1 then
    begin
      // FormMenu.vComercialConsulta:= ' ';
       vCalculo2 := vPass;
       if sqlUsuarioTIPOPASSW.AsInteger = 1 then
          vCalculo2 := IntToStr(RutCalcularPassword(vPass));

       if sqlUsuarioPASS.AsString <> vCalculo2 then
        exit;
       vBD := FieldByName('VISIBLE1').AsInteger;
       Result := True;
    end;

   end;

end;

Function TDMppal.RutTexFechaSQL (zTexGuia,zData:String):String;
var wData : TDate;
Begin
 Result := '';
 if zData = '' Then  Exit;
 Try
   wData := StrToDate(zData);
 Except
   Exit;
 end;
 Result := zTexGuia + '''' + FormatDateTime('mm/dd/yyyy',wData) + '''';
end;

Function TDMppal.RutTexFechas_SQL (zTexData,zDataA,zDataZ:String):String;
var wData : TDate;
wTexData  : String;
Begin
 Result := '';
 if (zDataA = '') and (zDataZ = '') Then Exit;

 if (zDataA <> '') and (zDataZ <> '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' between ', zDataA)
              + RutTexFechaSQL(' and ', zDataZ) + ' ) ';
 end;

 if (zDataA  = '') and (zDataZ <> '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' <= ', zDataZ) + ' ) ';
 end;

 if (zDataA <> '') and (zDataZ  = '') Then
 begin
  wTexData  :=  RutTexFechaSQL(' and ( ' + zTexData + ' >= ', zDataA) + ' ) ';
 end;

 Result := wTexData;

end;


Function TDMppal.RutConectarBD(vNumUsuario : String) : Boolean;
var
  vPath1, vPath2, vPath3 : String;
  vIni   : TStringList;
  ifIniFile: TInifile;
  sFichero : String;
begin
  vPath    := UniServerModule.StartPath;
  sFichero := vPath + 'KIOSLIB.ini';

  sFichero  := UniServerModule.vFichero;

  ifIniFile := TIniFile.Create(sFichero);
  vIni      := TStringList.Create;

  Result := False;

  if FileExists(sFichero) then
  begin
    with ifIniFile do
    begin
      if SectionExists('BDPATH') then
      begin
        ReadSectionValues('BDPATH', vIni);
        vPath1 := ReadString ('BDPATH','Path1', '');
//        vPath2 := ReadString ('BDPATH','Path2', '');
      end;
    end;
  end;
  ifIniFile.Free;
  vIni.Free;
  //FAPVP_1.FDB
//  vPath3 := copy(vPath1,1,pos('FAPVP',uppercase(vPath1))-1) + 'FAPVP_' + vNumUsuario + '.FDB';
  vPath3 := copy(vPath1,1,pos('0.FDB',uppercase(vPath1))-1) + vNumUsuario + '.FDB';
  if (vNumUsuario = '999999') and (sqlUsuarioGRUPO.AsInteger = 9) then
  begin
    vAdmin := True;
    //vPath3 := copy(vPath1,1,pos('.FDB',uppercase(vPath1))-1) + '0' + '.FDB';
    vPath3 := vPath1;
  end;

  with FDConnection0 do
  begin
    Close;
    Params.Values['Database'] := vPath1;
  end;

  with FDConnection1 do
  begin
    Close;
    Params.Values['Database'] := vPath3;

    try
      Open();
      result := True;
    except
      //raise Exception.Create('No se pudo conectar a la Base de Datos');
      Result := False;
    end;
  end;

end;









procedure TDMppal.RutAbrirTiposFICHA;
var
  vNivelSegu, vStringLike, vNoEstado : String;
begin

  RutAbrirTiposSQL;

end;

procedure TDMppal.RutAbrirTipos;
begin

  RutAbrirTiposSQL;

end;



procedure TDMppal.RutLeerArticulos;
begin


  sqlArti1.close;
  sqlArti1.SQL.Text :=  Copy(sqlArti1.SQL.Text,1,
         pos('WHERE', Uppercase(sqlArti1.SQL.Text))-1)
         + 'WHERE A.BARRAS = '  + QuotedStr(vg_Barres)
         + ' ORDER BY A.BARRAS';
  sqlArti1.Open;


  sqlArti0.close;
  sqlArti0.SQL.Text :=  Copy(sqlArti0.SQL.Text,1,
         pos('WHERE', Uppercase(sqlArti0.SQL.Text))-1)
         + 'WHERE A.BARRAS = '  + QuotedStr(vg_Barres)
         + ' ORDER BY A.BARRAS';
  sqlArti0.Open;

end;


procedure TDMppal.RutAbrirTiposSQL;
var
  vAsociados : String;
begin

 //sqlMTipos.Close;
  //sqlMTipos.Open;

 {
  sqlTrabajos.Close;
  sqlTrabajos.Open;

  sqlTiempo.Close;
  sqlTiempo.Open;

  sqlEmpresa.Close;
  sqlEmpresa.Open;
  }
end;



procedure TDMppal.RutAbrirTipoNOSIOtros(vTipo:String; vOrden2:Integer);
var
  vSql, vSql2 : String;
begin
//  vTipo := 'TIPONOSIOTROS';

  vSql := ' Select T.* From FDTIPOS T'
        + ' where T.CODIGO = (Select FM.CODIGO from FMTIPOS FM where FM.DESCRIPCION = ' + QuotedStr(vTipo) + ')'
        + ' and ((T.ORDEN2 = ' + IntToStr(vOrden2) + ') or (T.ORDEN2 = -1))'
        + ' order by 1';

  with DMppal.sqlTipoNOSIOtros do
  begin
    Close;
    SQL.Clear;
    SQL.Add(vSQl);
    Open;
  end;
end;




procedure TDMppal.RutPasarCampos(vTablaOrigen, vTablaDestino : TFDQuery; vTipo, vIDProve : Integer);
begin

  if vTipo >= 1 then
  begin
    vTablaDestino.FieldByName('ID_PROVEEDOR').AsInteger  := vIDProve;
    vTablaDestino.FieldByName('ID_ARTICULO').AsInteger   := vTablaOrigen.FieldByName('ID_ARTICULO').AsInteger;
    vTablaDestino.FieldByName('ADENDUM').AsString        := vg_Adendum;

    vTablaDestino.FieldByName('REFEPROVE').AsString      := vTablaOrigen.FieldByName('REFEPROVE').AsString;
    vTablaDestino.FieldByName('IDSTOCABE').AsInteger     := DMDevolucion.sqlCabe1IDSTOCABE.AsInteger;
    vTablaDestino.FieldByName('PRECIOCOSTE').AsFloat     := vTablaOrigen.FieldByName('PRECIOCOSTE').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTE2').AsFloat    := vTablaOrigen.FieldByName('PRECIOCOSTE2').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTETOT').AsFloat  := vTablaOrigen.FieldByName('PRECIOCOSTETOT').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOSTETOT2').AsFloat := vTablaOrigen.FieldByName('PRECIOCOSTETOT2').AsFloat;
    vTablaDestino.FieldByName('TIVA2').AsInteger         := vTablaOrigen.FieldByName('TIVA2').AsInteger;
    vTablaDestino.FieldByName('TIVA').AsInteger          := vTablaOrigen.FieldByName('TIVA').AsInteger;
    vTablaDestino.FieldByName('MARGEN').AsFloat          := vTablaOrigen.FieldByName('MARGEN').AsFloat;
    vTablaDestino.FieldByName('MARGEN2').AsFloat         := vTablaOrigen.FieldByName('MARGEN2').AsFloat;
    vTablaDestino.FieldByName('PRECIOVENTA').AsFloat     := vTablaOrigen.FieldByName('PRECIO1').AsFloat;
    vTablaDestino.FieldByName('PRECIOVENTA2').AsFloat    := vTablaOrigen.FieldByName('PRECIO2').AsFloat;
    vTablaDestino.FieldByName('FECHADEVOL').AsDateTime   := DMDevolucion.sqlCabe1FECHA.AsDateTime;
    vTablaDestino.FieldByName('FECHAAVISO').AsDateTime   := vTablaDestino.FieldByName('FECHADEVOL').AsDateTime - 7;
    vTablaDestino.FieldByName('FECHA').AsDateTime        := now;
    vTablaDestino.FieldByName('HORA').AsDateTime         := now;
    vTablaDestino.FieldByName('SWES').AsString           := 'S';
    vTablaDestino.FieldByName('SWTIPOFRA').AsInteger     := 0;
    vTablaDestino.FieldByName('NFACTURA').AsInteger      := 0;
    vTablaDestino.FieldByName('NALBARAN').AsInteger      := 0;
  end;

  if vTipo >= 2 then
  begin


    vTablaDestino.FieldByName('IDDEVOLUCABE').AsInteger  := vTablaOrigen.FieldByName('IDDEVOLUCABE').AsInteger;
    vTablaDestino.FieldByName('PAQUETE').AsInteger       := vTablaOrigen.FieldByName('PAQUETE').AsInteger;
    vTablaDestino.FieldByName('ID_HISARTI01').AsInteger  := vTablaOrigen.FieldByName('ID_HISARTI01').AsInteger;
    vTablaDestino.FieldByName('ID_DIARIA').AsInteger     := vTablaOrigen.FieldByName('ID_DIARIA').AsInteger;
    vTablaDestino.FieldByName('PARTIDA').AsInteger       := vTablaOrigen.FieldByName('PARTIDA').AsInteger;
    vTablaDestino.FieldByName('NRECLAMACION').AsInteger  := vTablaOrigen.FieldByName('NRECLAMACION').AsInteger;
    vTablaDestino.FieldByName('ID_FRAPROVE').AsInteger   := vTablaOrigen.FieldByName('ID_FRAPROVE').AsInteger;
    vTablaDestino.FieldByName('ID_CLIENTE').AsInteger    := vTablaOrigen.FieldByName('ID_CLIENTE').AsInteger;
    vTablaDestino.FieldByName('NLINEA').AsInteger        := vTablaOrigen.FieldByName('NLINEA').AsInteger;
    vTablaDestino.FieldByName('DEVUELTOS').AsFloat       := vTablaDestino.FieldByName('CANTIDAD').AsFloat;
    vTablaDestino.FieldByName('VENDIDOS').AsFloat        := vTablaOrigen.FieldByName('VENDIDOS').AsFloat;
    vTablaDestino.FieldByName('MERMA').AsFloat           := vTablaOrigen.FieldByName('MERMA').AsFloat;
    vTablaDestino.FieldByName('CARGO').AsFloat           := vTablaOrigen.FieldByName('CARGO').AsFloat;
    vTablaDestino.FieldByName('ABONO').AsFloat           := vTablaOrigen.FieldByName('ABONO').AsFloat;
    vTablaDestino.FieldByName('RECLAMADO').AsFloat       := vTablaOrigen.FieldByName('RECLAMADO').AsFloat;
    vTablaDestino.FieldByName('CANTIENALBA').AsFloat     := vTablaOrigen.FieldByName('CANTIENALBA').AsFloat;
    vTablaDestino.FieldByName('CANTISUSCRI').AsFloat     := vTablaOrigen.FieldByName('CANTISUSCRI').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOMPRA').AsFloat    := vTablaOrigen.FieldByName('PRECIOCOMPRA').AsFloat;
    vTablaDestino.FieldByName('PRECIOCOMPRA2').AsFloat   := vTablaOrigen.FieldByName('PRECIOCOMPRA2').AsFloat;
    vTablaDestino.FieldByName('SWDTO').AsInteger         := vTablaOrigen.FieldByName('SWDTO').AsInteger;
    vTablaDestino.FieldByName('TPCDTO').AsFloat          := vTablaOrigen.FieldByName('TPCDTO').AsFloat;
    vTablaDestino.FieldByName('TPCIVA').AsFloat          := vTablaOrigen.FieldByName('TPCIVA').AsFloat;
    vTablaDestino.FieldByName('TPCRE').AsFloat           := vTablaOrigen.FieldByName('TPCRE').AsFloat;
    vTablaDestino.FieldByName('TPCRE2').AsFloat          := vTablaOrigen.FieldByName('TPCRE2').AsFloat;
    vTablaDestino.FieldByName('IMPOBRUTO').AsFloat       := vTablaOrigen.FieldByName('IMPOBRUTO').AsFloat;
    vTablaDestino.FieldByName('IMPODTO').AsFloat         := vTablaOrigen.FieldByName('IMPODTO').AsFloat;
    vTablaDestino.FieldByName('IMPOBASE').AsFloat        := vTablaOrigen.FieldByName('IMPOBASE').AsFloat;
    vTablaDestino.FieldByName('IMPOIVA').AsFloat         := vTablaOrigen.FieldByName('IMPOIVA').AsFloat;
    vTablaDestino.FieldByName('IMPORE').AsFloat          := vTablaOrigen.FieldByName('IMPORE').AsFloat;
    vTablaDestino.FieldByName('IMPOBASE2').AsFloat       := vTablaOrigen.FieldByName('IMPOBASE2').AsFloat;
    vTablaDestino.FieldByName('IMPOIVA2').AsFloat        := vTablaOrigen.FieldByName('IMPOIVA2').AsFloat;
    vTablaDestino.FieldByName('IMPORE2').AsFloat         := vTablaOrigen.FieldByName('IMPORE2').AsFloat;
    vTablaDestino.FieldByName('IMPOTOTLIN').AsFloat      := vTablaOrigen.FieldByName('IMPOTOTLIN').AsFloat;
    vTablaDestino.FieldByName('ENTRADAS').AsFloat        := vTablaOrigen.FieldByName('ENTRADAS').AsFloat;
    vTablaDestino.FieldByName('SALIDAS').AsFloat         := vTablaOrigen.FieldByName('SALIDAS').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTE').AsFloat      := vTablaOrigen.FieldByName('VALORCOSTE').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTE2').AsFloat     := vTablaOrigen.FieldByName('VALORCOSTE2').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTETOT').AsFloat   := vTablaOrigen.FieldByName('VALORCOSTETOT').AsFloat;
    vTablaDestino.FieldByName('VALORCOSTETOT2').AsFloat  := vTablaOrigen.FieldByName('VALORCOSTETOT2').AsFloat;
    vTablaDestino.FieldByName('VALORCOMPRA').AsFloat     := vTablaOrigen.FieldByName('VALORCOMPRA').AsFloat;
    vTablaDestino.FieldByName('VALORCOMPRA2').AsFloat    := vTablaOrigen.FieldByName('VALORCOMPRA2').AsFloat;
    vTablaDestino.FieldByName('VALORVENTA').AsFloat      := vTablaOrigen.FieldByName('VALORVENTA').AsFloat;
    vTablaDestino.FieldByName('VALORVENTA2').AsFloat     := vTablaOrigen.FieldByName('VALORVENTA2').AsFloat;
    vTablaDestino.FieldByName('VALORMOVI').AsFloat       := vTablaOrigen.FieldByName('VALORMOVI').AsFloat;
    vTablaDestino.FieldByName('TPCCOMISION').AsFloat     := vTablaOrigen.FieldByName('TPCCOMISION').AsFloat;
    vTablaDestino.FieldByName('IMPOCOMISION').AsFloat    := vTablaOrigen.FieldByName('IMPOCOMISION').AsFloat;
    vTablaDestino.FieldByName('ENCARTE').AsFloat         := vTablaOrigen.FieldByName('ENCARTE').AsFloat;
    vTablaDestino.FieldByName('ENCARTE2').AsFloat        := vTablaOrigen.FieldByName('ENCARTE2').AsFloat;
    vTablaDestino.FieldByName('TURNO').AsInteger         := vTablaOrigen.FieldByName('TURNO').AsInteger;
    vTablaDestino.FieldByName('NALMACEN').AsInteger      := vTablaOrigen.FieldByName('NALMACEN').AsInteger;
    vTablaDestino.FieldByName('SWALTABAJA').AsInteger    := vTablaOrigen.FieldByName('SWALTABAJA').AsInteger;
    vTablaDestino.FieldByName('SWPDTEPAGO').AsInteger    := vTablaOrigen.FieldByName('SWPDTEPAGO').AsInteger;
    vTablaDestino.FieldByName('SWESTADO').AsInteger      := vTablaOrigen.FieldByName('SWESTADO').AsInteger;
    vTablaDestino.FieldByName('SWDEVOLUCION').AsInteger  := vTablaOrigen.FieldByName('SWDEVOLUCION').AsInteger;
    vTablaDestino.FieldByName('SWCERRADO').AsInteger     := vTablaOrigen.FieldByName('SWCERRADO').AsInteger;
    vTablaDestino.FieldByName('SWMARCA').AsInteger       := vTablaOrigen.FieldByName('SWMARCA').AsInteger;
    vTablaDestino.FieldByName('SWMARCA2').AsInteger      := vTablaOrigen.FieldByName('SWMARCA2').AsInteger;
    vTablaDestino.FieldByName('SWMARCA3').AsInteger      := vTablaOrigen.FieldByName('SWMARCA3').AsInteger;
    vTablaDestino.FieldByName('SWMARCA4').AsInteger      := vTablaOrigen.FieldByName('SWMARCA4').AsInteger;
    vTablaDestino.FieldByName('SWMARCA5').AsInteger      := vTablaOrigen.FieldByName('SWMARCA5').AsInteger;
    vTablaDestino.FieldByName('SEMANA').AsInteger        := vTablaOrigen.FieldByName('SEMANA').AsInteger;
    vTablaDestino.FieldByName('TIPOVENTA').AsInteger     := vTablaOrigen.FieldByName('TIPOVENTA').AsInteger;
    vTablaDestino.FieldByName('FECHACARGO').AsDateTime   := vTablaOrigen.FieldByName('FECHACARGO').AsDateTime;
    vTablaDestino.FieldByName('FECHAABONO').AsDateTime   := vTablaOrigen.FieldByName('FECHAABONO').AsDateTime;
    vTablaDestino.FieldByName('SWRECLAMA').AsInteger     := vTablaOrigen.FieldByName('SWRECLAMA').AsInteger;
    vTablaDestino.FieldByName('ID_HISARTIRE').AsInteger  := vTablaOrigen.FieldByName('ID_HISARTIRE').AsInteger;
    vTablaDestino.FieldByName('SWACABADO').AsInteger     := vTablaOrigen.FieldByName('SWACABADO').AsInteger;

  end;

  if vTablaDestino = DMDevolucion.sqlLinea then
  begin
    vTablaDestino.FieldByName('CANTIDAD').AsFloat        := 1;
    vTablaDestino.FieldByName('CLAVE').AsString          := '54';
    vTablaDestino.FieldByName('DEVUELTOS').AsFloat       := vTablaDestino.FieldByName('CANTIDAD').AsFloat;
  end;

  FormDevolFicha.pTIva1Exit(nil);



end;



procedure TDMppal.RutSepararCodigoBarras(vCodbarres:String);
var     wSize1   : Integer;
begin
  wSize1 := length(vCodBarres);
  if wSize1 > 18 then wSize1 := 18;

  if wSize1 > 13 Then
  begin
    vg_Barres  := copy(vCodBarres,1,13);
    vg_Adendum := copy(vCodBarres,14,(wSize1 - 13));
  end else
  begin
    vg_Barres  := vCodBarres;
    vg_Adendum := '';
  end;
  if wSize1 = 9 Then
  begin
    vg_Barres  := copy(vCodBarres,1,7);
    vg_Adendum := copy(vCodBarres,8,2);
  end;
end;

Procedure TDMPpal.RutLeerTipoIVA      (zTIVA:Integer);
var wTIVA : Integer;
begin
  wTIVA := zTIVA;
  if DMPpal.sqlFGTIVA.Active Then
  begin
    if   DMPpal.sqlFGTIVA.FieldByName('TIVA').AsInteger = zTIVA Then Exit
    else DMPpal.sqlFGTIVA.Close;
  end;
  DMPpal.sqlFGTIVA.Params.ParamByName('TIVA').AsInteger := zTIVA;
  DMPpal.sqlFGTIVA.Open;
End;

Procedure  TDMppal.RutHisArtiLin_CargaIVA_A   (zTabHis:TFDQuery; zTxTIVA,zTxIVa,zTxRE:String; zTipo:Integer);
Begin
  if (zTxTIVA = '') or (zTxIVa = '')     Then Exit;     // Si no l'hi envien Nom de Tipus d'IVA o % -- Fora

  if RutExisteCampo  (zTabHis, zTxTIVA)  Then   // Si existeix Nom de Tipus d'IVA -- Fora
          RutLeerTipoIVA (zTabHis.FieldByName(zTxTIVA).AsInteger)
  else    Exit;

  if RutExisteCampo  (zTabHis,zTxIVa) Then              // Si existeix Nom de % -- Fora
          zTabHis.FieldByName(zTxIVa).AsFloat := DMPpal.sqlFGTIVA.FieldByName('TPCIVA').AsFloat;

  if zTxRE  = '' Then Exit;

  if RutExisteCampo  (zTabHis,zTxRE) Then               // Si existeix Nom de % RE -- Fora
  begin
      if zTipo = 1 Then
           zTabHis.FieldByName(zTxRE).AsFloat := DMPpal.sqlFGTIVA.FieldByName('TPCRE').AsFloat
      else zTabHis.FieldByName(zTxRE).AsFloat := RoundTo(0,-2);
  end;
end;


Function TDMppal.RutCalculaImportes (vTabla :TFDQuery; vCanti:String; vDecimals,vTipo:Integer):Integer;
var vTxCanti : String;
   wDecimals : Integer;
   vImporteT, vImporte1, vImporte2  : Double;
begin
  if RutEdiInse(vTabla) = False Then Exit;

  if  (vCanti <> 'CANTIDAD') and (vCanti <> 'CANTIENALBA')  Then vTxCanti := 'CANTIDAD'
  else                                                               vTxCanti := vCanti;
  wDecimals := vDecimals;
  vImporte2 := 0;

//   -------------------------------------------------------  Import segons Compra o venta
  vImporte1 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA').AsFloat), wDecimals);
  if RutExisteCampo  (vTabla,'PRECIOVENTA2') Then
  vImporte2 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA2').AsFloat), wDecimals);

  if vTipo = 0 Then            //   'PRECIOCOMPRA'
  begin
    vImporte1 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOMPRA').AsFloat), wDecimals);
    if RutExisteCampo  (vTabla,'PRECIOCOMPRA2') Then
    vImporte2 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOMPRA2').AsFloat), wDecimals);
  end;

  if vTipo = 1 Then            //   'PRECIOVENTA'
  begin
    vImporte1 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA').AsFloat),  wDecimals);
    if RutExisteCampo  (vTabla,'PRECIOVENTA2') Then
    vImporte2 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA2').AsFloat), wDecimals);
  end;

  if vTipo = 2 Then            //   'PRECIOCOSTE'
  begin
    vImporte1 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOSTE').AsFloat),  wDecimals);
    if RutExisteCampo  (vTabla,'PRECIOCOSTE2') Then
    vImporte2 := RoundTo( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOSTE2').AsFloat), wDecimals);
  end;

  vImporteT  := vImporte1 + vImporte2;

//   -------------------------------------------------------  Cost
//   vTabla.FieldByName('PRECIOCOSTE').AsFloat    := RutRedondeo(vTabla.FieldByName('PRECIOCOMPRA').AsFloat,2);

//   -------------------------------------------- Calcula: Brut.Net,IVA,Base,Dto
   vTabla.FieldByName('IMPOBRUTO').AsFloat      :=
        RoundTo( (vImporteT - vTabla.FieldByName('IMPODTO').AsFloat), wDecimals);

   vTabla.FieldByName('IMPOIVA').AsFloat        :=
        RutTpcR_ImpoTPC (vImporte1, vTabla.FieldByName('TPCIVA').AsFloat ,wDecimals);

   vTabla.FieldByName('IMPORE').AsFloat        :=
        RutTpcR_ImpoTPC (vImporte1, vTabla.FieldByName('TPCRE').AsFloat ,wDecimals);

   vTabla.FieldByName('IMPOBASE').AsFloat       :=
        RoundTo( (vImporte1 - (vTabla.FieldByName('IMPOIVA').AsFloat
                                +  vTabla.FieldByName('IMPORE').AsFloat)), wDecimals);


//   if (RutExisteCampo (vTabla,'IMPOIVA2')) and (RutExisteCampo (vTabla,'TPCIVA2'))
   vTabla.FieldByName('IMPOIVA2').AsFloat        :=
        RutTpcR_ImpoTPC (vImporte2, vTabla.FieldByName('TPCIVA2').AsFloat ,wDecimals);

   vTabla.FieldByName('IMPORE2').AsFloat        :=
        RutTpcR_ImpoTPC (vImporte2, vTabla.FieldByName('TPCRE2').AsFloat ,wDecimals);

   vTabla.FieldByName('IMPOBASE2').AsFloat       :=
        RoundTo( (vImporte2 - (vTabla.FieldByName('IMPOIVA2').AsFloat
                                +  vTabla.FieldByName('IMPORE2').AsFloat)), wDecimals);

   vTabla.FieldByName('IMPOTOTLIN').AsFloat     :=
        RoundTo( (vTabla.FieldByName('IMPOBASE').AsFloat + vTabla.FieldByName('IMPOIVA').AsFloat
                    + vTabla.FieldByName('IMPORE').AsFloat
                    + vTabla.FieldByName('IMPOBASE2').AsFloat + vTabla.FieldByName('IMPOIVA2').AsFloat
                    + vTabla.FieldByName('IMPORE2').AsFloat), wDecimals);

//   -------------------------------------------------------  ------------------
   vTabla.FieldByName('ENTRADAS').AsFloat       := RoundTo(0,-2);
   vTabla.FieldByName('SALIDAS').AsFloat        := RoundTo(0,-2);
   vTabla.FieldByName('VALORCOSTE').AsFloat     := RoundTo(0,-2);
   vTabla.FieldByName('VALORCOMPRA').AsFloat    := RoundTo(0,-2);
   vTabla.FieldByName('VALORVENTA').AsFloat     := RoundTo(0,-2);
   vTabla.FieldByName('VALORMOVI').AsFloat      := RoundTo(0,-2);
   vTabla.FieldByName('IMPOCOMISION').AsFloat   := RoundTo(0,-2);

//   -------------------------------------------------------  Entradas / Salidas
   if vTabla.FieldByName('SWES').AsString  = 'E' Then
      vTabla.FieldByName('ENTRADAS').AsFloat    := RoundTo(vTabla.FieldByName(vTxCanti).AsFloat,wDecimals);
   if vTabla.FieldByName('SWES').AsString  = 'S' Then
      vTabla.FieldByName('SALIDAS').AsFloat     := RoundTo(vTabla.FieldByName(vTxCanti).AsFloat,wDecimals);

//   -----------------------------------------------------------  Valor Comision
   RutCalculaLinHistoComisio (vTabla, vDecimals,vTipo);

//   --------------------------------------------------------------  Valor Coste
   vTabla.FieldByName('VALORCOSTE').AsFloat     := ((vTabla.FieldByName(vTxCanti).AsFloat     -  vTabla.FieldByName('CANTISUSCRI').AsFloat) * vTabla.FieldByName('PRECIOCOSTE').AsFloat);
   vTabla.FieldByName('VALORCOSTE').AsFloat     :=   vTabla.FieldByName('VALORCOSTE').AsFloat - (vTabla.FieldByName('CANTISUSCRI').AsFloat  * vBeneficio);
   vTabla.FieldByName('VALORCOSTE').AsFloat     :=   RoundTo ( vTabla.FieldByName('VALORCOSTE').AsFloat, wDecimals);

   vTabla.FieldByName('VALORCOSTE2').AsFloat    := 0;
   if vTabla.FieldByName('PRECIOCOSTE2').AsFloat > 0 then
   begin
   vTabla.FieldByName('VALORCOSTE2').AsFloat    := ((vTabla.FieldByName(vTxCanti).AsFloat      -  vTabla.FieldByName('CANTISUSCRI').AsFloat) * vTabla.FieldByName('PRECIOCOSTE2').AsFloat);
   vTabla.FieldByName('VALORCOSTE2').AsFloat    :=   vTabla.FieldByName('VALORCOSTE2').AsFloat - (vTabla.FieldByName('CANTISUSCRI').AsFloat  * vBeneficio);
   vTabla.FieldByName('VALORCOSTE2').AsFloat    :=   RoundTo ( vTabla.FieldByName('VALORCOSTE2').AsFloat, wDecimals);
   end;

   vTabla.FieldByName('VALORCOSTETOT').AsFloat  := RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOSTETOT').AsFloat),  wDecimals);
   vTabla.FieldByName('VALORCOSTETOT2').AsFloat := RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOSTETOT2').AsFloat), wDecimals);

//   -------------------------------------------------------------  Valor Compra
     if vTabla.FieldByName('CLAVE').AsString       = '01' Then      //  Compra
     begin
        vTabla.FieldByName('VALORCOMPRA').AsFloat :=
                RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOMPRA').AsFloat), wDecimals);
        vTabla.FieldByName('VALORCOMPRA2').AsFloat :=
                RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOCOMPRA2').AsFloat), wDecimals);
     end;
//   --------------------------------------------------------------  Valor Venta
     if  (vTabla.FieldByName('CLAVE').AsString       = '51')        //  Venta por TPV
      or (vTabla.FieldByName('CLAVE').AsString       = '52')        //  Venta por TPV Factura ?
      or (vTabla.FieldByName('CLAVE').AsString       = '53') Then   //  Venta por Calculo
     begin
       vTabla.FieldByName('VALORVENTA').AsFloat  := RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA').AsFloat), wDecimals);
       vTabla.FieldByName('VALORVENTA2').AsFloat := RoundTo ( (vTabla.FieldByName(vTxCanti).AsFloat * vTabla.FieldByName('PRECIOVENTA2').AsFloat),wDecimals);
     end;
//   -------------------------------------------------------------  Valor Movimiento
     vTabla.FieldByName('VALORMOVI').AsFloat := RoundTo((vTabla.FieldByName('IMPOBASE').AsFloat
                                                           + vTabla.FieldByName('IMPOBASE2').AsFloat),2);
//   if vTabla.FieldByName('SWES').AsString      = 'E' Then

     if vTabla.FieldByName('SWES').AsString      = 'S' Then
        vTabla.FieldByName('VALORMOVI').AsFloat := RoundTo ( (vTabla.FieldByName('VALORMOVI').AsFloat * -1 ),2);

end;


procedure TDMppal.RutCalculaLinHistoComisio (vTabla:TFDQuery; vDecimals,vTipo:Integer);
Begin
 if vTabla.FieldByName('TPCCOMISION').AsFloat <> 0 Then
    vTabla.FieldByName('IMPOCOMISION').AsFloat   :=
      RutTpcDe (vTabla.FieldByName('IMPOBASE').AsFloat, vTabla.FieldByName('TPCCOMISION').AsFloat, vDecimals);
end;


Function TDMppal.RutTpcDe(vCamp, vCamp2:Double; vDecim:Integer):Double;
var wCamp : Double;
begin
 Result := 0;
 wCamp := (vCamp * vCamp2);
 if (wCamp = 0) Then Exit;
 Try    wCamp := wCamp / 100;
 except wCamp := 0;
 end;
 Result := RoundTo(wCamp,vDecim);
end;

function  TDMppal.RutExisteCampo(vTabla:TDataSet; vCampo:String):Boolean;
var wInd   : Integer;
    wField : TField;
Begin
 Result   := False;
 For wInd := 0 to vTabla.FieldCount -1 do
 Begin
     Result := False;
     wField := vTabla.Fields[ wInd ];
     if UpperCase(wField.FieldName) = UpperCase(vCampo) Then Begin
        Result := True;
        Exit;
     end;
 end;
end;

function  TDMppal.RutEdiInse(vTabla:TDataSet):Boolean;
begin
 if (vTabla.Active = False)  Then
 begin
    Result := False;
    Exit;
 end;
 if (vTabla.State = dsEdit) or (vTabla.State = dsInsert) Then Result := True
 else                                                     Result := False;
end;


function TDMppal.RutTpcR_ImpoTPC (vCamp,vTpc:Double; vDecim:Integer):Double;
var wCamp, wTpc : Double;
begin
 Result := 0;
 if (vCamp = 0) or (vTpc = 0) Then Exit;
 wTpc  := (vTpc / 100) + 1;

 Try    wCamp := vCamp / wTpc;
 except wCamp := 0;
 end;


 Result := RoundTo( (vCamp - wCamp),vDecim);
end;



function TDMppal.GetMaxNumber(Tabla, Campo : String):Integer;
begin

  Result := 0;
  with sqlUpdate do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select max(' + Campo + ') as ultimo from ' + Tabla);
    Open;
    if RecordCount > 0 then Result := Fieldbyname('ultimo').AsInteger;
  end;

end;

procedure TDMppal.ExportExcel(ADataSet:TDataSet);
var
  fname:string;
begin
{
 if DMppal.cdsUsuarioVISIBLE1.AsInteger = 1 then
 begin
  fname := UniServerModule.LocalCachePath + 'E' + FormatDateTime('hhmmss', Now()) + '.xls'; // Create a unique name for report.
  DataSetToXLS(ADataSet, fname);
  UniSession.SendFile(fname);
 end;

 if DMppal.cdsUsuarioVISIBLE1.AsInteger = 2 then
 begin
    fname := 'F' + FormatDateTime('hhmmss', Now()) + '.csv'; // Create a unique name for report.
    ExportarCSV(ADataSet,UniServerModule.LocalCachePath + fname);
    unisession.SendFile(UniServerModule.LocalCachePath + fname);
 end;
}
end;

procedure TDMppal.ExportExcelGrid(ADataSet:TUniDbgrid);
var
  fname:string;
begin


 if DMppal.sqlUsuarioVISIBLE1.AsInteger = 1 then
 begin
  fname := UniServerModule.LocalCachePath + 'E' + FormatDateTime('hhmmss', Now()) + '.xls'; // Create a unique name for report.
  GridToXLS(ADataSet, fname);
  UniSession.SendFile(fname);
 end;

 if DMppal.sqlUsuarioVISIBLE1.AsInteger = 2 then
 begin
    fname := 'F' + FormatDateTime('hhmmss', Now()) + '.csv'; // Create a unique name for report.
    ExportarCSVGrid(ADataSet,UniServerModule.LocalCachePath + fname);
    unisession.SendFile(UniServerModule.LocalCachePath + fname);
 end;


end;



//------------------------------------------------------------------------------



procedure TDMppal.RutAbrirUsuarios(vWhere:String);
var
  vWhereT : String;
begin
  if vWhere > ' ' then
     vWhereT := ' and NumeroUsuario = ' + vWhere;

  with sqlUsu do
  begin
    Close;
    sqlUsu.sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE ID', Uppercase(SQL.Text))-1)
                 + 'WHERE ID >= ' + QuotedStr(' ')
                 + vWhereT
                 + ' ORDER BY 1';
    Open;
    if recordcount = 0 then
    begin
      Append;
      Fieldbyname('ID').AsString             := vWhere;
      Fieldbyname('PASS').AsString           := vWhere;
      FieldByName('NumeroUsuario').AsInteger := StrToIntDef(vWhere,0);
      FieldByName('Grupo').AsInteger         := 1;
      Post;
    end;
  end;



end;


//Sorts i Filtros
//------------------------------------------------------------------------------
procedure TDMppal.SortColumn(vDataset: TClientdataset; const FieldName: string; Dir: Boolean);
begin
  if Dir then
    vDataset.IndexName := FieldName+'_index_asc'
  else
    vDataset.IndexName := FieldName+'_index_des';
end;

procedure TDMppal.SortColumnSQL(vDataset: TFDQuery; const FieldName: string; Dir: Boolean);
begin
  if Dir then
    vDataset.IndexName := FieldName+'_index_asc'
  else
    vDataset.IndexName := FieldName+'_index_des';
end;





procedure TDMppal.RutCrearSorting(vDataset:TClientdataset);
var
  I: Integer;
  IndexnameAsc : string;
  IndexnameDes : string;
begin
  with vDataset do
  begin
    for I := 0 to FieldCount-1 do
    begin
      IndexnameAsc := Fields[I].FieldName+'_index_asc';
      IndexnameDes := Fields[I].FieldName+'_index_des';
      IndexDefs.Add(IndexnameAsc, Fields[I].FieldName, []);
      IndexDefs.Add(IndexnameDes, Fields[I].FieldName, [ixDescending]);
    end;
  end;
end;

procedure TDMppal.RutCrearSortingSQL(vDataset:TFDQuery);
var
  I: Integer;
  IndexnameAsc : string;
  IndexnameDes : string;
begin
  with vDataset do
  begin
    for I := 0 to FieldCount-1 do
    begin
      IndexnameAsc := Fields[I].FieldName+'_index_asc';
      IndexnameDes := Fields[I].FieldName+'_index_des';
      IndexDefs.Add(IndexnameAsc, Fields[I].FieldName, []);
      IndexDefs.Add(IndexnameDes, Fields[I].FieldName, [ixDescending]);
    end;
  end;
end;



procedure TDMppal.UniGUIMainModuleCreate(Sender: TObject);
var
  vPath1, vPath2, vPath3 : String;
  vIni   : TStringList;
  ifIniFile: TInifile;
  sFichero : String;
begin

  vBody := TStringList.Create;

  vHost := 'mail.sogemedi.com';
  vPort := 587;
  vUser := 'info@sogemedi.com';
  vPass := 'PASSWORD';
  vFrom := 'info@sogemedi.com';
  vBack := 'info@sogemedi.com';

 //BLOQUEO PARA MOVER ESTO A OTRA RUTINA QUE FUNCIONARA DESPUES DE HABER ENCONTRADO AL USUARIO
  vPath    := UniServerModule.StartPath;
  sFichero := vPath + 'KIOSLIB.ini';

  sFichero  := UniServerModule.vFichero;

  ifIniFile := TIniFile.Create(sFichero);
  vIni      := TStringList.Create;


  if FileExists(sFichero) then
  begin
    with ifIniFile do
    begin
      if SectionExists('BDPATH') then
      begin
        ReadSectionValues('BDPATH', vIni);
        vPath1 := ReadString ('BDPATH','Path1', '');
//        vPath2 := ReadString ('BDPATH','Path2', '');
      end;
    end;
  end;
  ifIniFile.Free;
  vIni.Free;
  //FAPVP_1.FDB
//  vPath3 := copy(vPath1,1,pos('FAPVP',uppercase(vPath1))-1) + 'FAPVP_' + sqlUsuarioNUMEROUSUARIO.AsString + '.FDB';
  with FDConnection0 do
  begin
    Close;
    Params.Values['Database'] := vPath1;
  end;

{
  with FDConnection2 do
  begin
    Close;
    Params.Values['Database'] := vPath2;
  end;
}


  RutCrearSortingSQL(DMppal.sqlArticulos);


//   RutActualizarbaseDatos;
  //RutConectarBD('0');
end;

//----------------------------


procedure TDMppal.UniGUIMainModuleNewComponent(AComponent: TComponent);
begin
  if AComponent is TfrxReport then
  begin
    (AComponent as TfrxReport).EngineOptions.UseGlobalDataSetList := False;
  end;
end;



//-------------------------------------------------
//COMPROBAR TABLAS y CAMPOS en BD
//-------------------------------------------------


procedure TDMPpal.RutActualizarbaseDatos;
begin
//poner por orden los mas nuevos arriba

  CreartablasNuevas;

//29/10/2018 ejemplo
  RutCargarCampoTabla(DMPpal.FDConnection0, DMPpal.sqlUpdate2, 'FMENSAJE', 'TPV',  'INTEGER',    '0' );
//30/10/2018
  RutCargarCampoTabla(DMPpal.FDConnection0, DMPpal.sqlUpdate2, 'FMENSAJE', 'TIPOPASSW',  'INTEGER',    '0' );
//27/11/2018
  RutCargarCampoTabla(DMPpal.FDConnection1, DMPpal.sqlUpdate2, 'FMPROVE', 'MAXPAQUETE',  'INTEGER',    '0' );

//blob text ejemplo de creacion
//  RutCargarCampoTabla(DMPpal.FDConnection1, DMPpal.sqlUpdate2, 'FMENSAJE', 'TPV1',  'BLOB SUB_TYPE 1 SEGMENT SIZE 80',    '0' );

end;
procedure TDMppal.CreartablasNuevas;
var
  I : Integer;
begin
  // mirar si existe la tabla

   if not CheckTablename('FMENSAJE') then
   begin

  //sino existe crearla


     with DMPpal.sqlUpdate2 do
     begin
       Close;
       SQL.Text := 'CREATE TABLE FMENSAJE ('
                   + ' ID_MENSAJE INTEGER NOT NULL,'
                   + ' FECHA DATE, '
                   + ' HORA TIME, '
                   + ' MENSAJE BLOB SUB_TYPE 1 SEGMENT SIZE 80)';
                   //+ ' TPV INTEGER)';
       ExecSQL();
//* Create Primary Key... */
       Close;

       SQL.Text := 'ALTER TABLE FMENSAJE ADD CONSTRAINT PK_FMENSAJE PRIMARY KEY (ID_MENSAJE);';
       ExecSQL();
       Close;
//* Create Gen... */
       SQL.Text := 'CREATE SEQUENCE GEN_MENSAJE';
       ExecSQL();
       Close;
//* Create Procedure... */
       SQL.Text :=  'create or alter procedure sp_gen_mensaje_id'
                  + ' returns ('
                  + ' id integer)'
                  + ' as'
                  + ' begin'
                  + ' /* Procedure Text */'
                  + ' ID = GEN_ID(GEN_MENSAJE, 1);'
                  + ' suspend;'
                  + ' end';
       ExecSQL();
       Close;
     end;
   end;

    //sino existen los registros crear registros en FDTIPOS

 { with DMPpal.sqlUpdate do
  begin
    for i:= 0 to MemoInsert.Lines.Count -1 do
    begin
      Close;
      CommandText := MemoInsert.Lines[i];
      ExecSQL();
//      Inc(i);
    end;
  end;
}

end;

function TDMppal.CheckTablename(const TableName:string):Boolean;
var Tablas:TStringList;
begin
  Tablas := TStringList.Create;

  Result := False;
{----------------------------NOMBRES DE TABLAS Y VISTAS ----------------------}
    DMPpal.sqlUpdate2.Close;
    DMPpal.sqlUpdate2.SQL.Text := 'SELECT RDB$RELATION_NAME FROM RDB$RELATIONS' //+ {do not localize}
                                +' WHERE RDB$SYSTEM_FLAG = 0'; {do not localize}
    DMPpal.sqlUpdate2.open;

    if DMPpal.sqlUpdate2.Locate('RDB$RELATION_NAME',TableName,[locaseinsensitive]) then
    Result := True;
end;

//-------------------------------------------------
//FIN COMPROBAR TABLAS y CAMPOS en BD
//-------------------------------------------------


//-------------------------------------------------
//MENSAJES
//-------------------------------------------------
procedure TDMppal.RutAbrirMensaje;
var
  vSQL : String;
begin
//rutina para ver mensaje en MEMO

  with sqlMensaje do
  begin
    vSQL := Copy(SQL.Text, 1,
                    pos('WHERE FECHA', Uppercase(SQL.Text))-1)
                 + 'WHERE FECHA = ' + QuotedStr(FormatDateTime('mm/dd/yy',now))
                 + ' ORDER BY FECHA,HORA';
    Close;
    SQL.Text  := vSQL;
    Open;
    Last;
  end;


end;

procedure TDMppal.RutAbrirMensajes(vDate : TDate);
begin
//rutina para ver mensajes en grid
  with sqlMensajesGeneral do
  begin
    Close;
    SQL.Text  := Copy(SQL.Text, 1,
                    pos('WHERE FECHA', Uppercase(SQL.Text))-1)
                 + 'WHERE FECHA = ' + QuotedStr(FormatDateTime('mm/dd/yy',vDate))
                 + ' ORDER BY FECHA,HORA DESC';
    Open;

  end;
end;

procedure TDMppal.RutEnviarMensaje(vMensaje : String);
begin
  SP_GEN_MENSAJE_ID.ExecProc;

  sqlMensajesGeneral.Append;
  sqlMensajesGeneralID_MENSAJE.AsInteger := SP_GEN_MENSAJE_ID.Params[0].Value;
  sqlMensajesGeneralFECHA.AsDateTime     := now;
  sqlMensajesGeneralHORA.AsDateTime      := now;
  sqlMensajesGeneralMENSAJE.AsString     := vMensaje;
  sqlMensajesGeneralTPV.AsInteger        := sqlUsuarioNUMEROUSUARIO.AsInteger;

  sqlMensajesGeneral.Post;
  sqlMensajesGeneral.Refresh;

end;



//-------------------------------------------------
//FIN MENSAJES
//-------------------------------------------------



//-------------------------------------------------
//Nuevo usuario
//-------------------------------------------------

procedure TDMppal.RutGrabarUsuario(vPass : String; vModi : Boolean);
begin
//Rutina para guardar usuario hecho por Administrador
  sqlUsuTIPOPASSW.AsInteger  := 1;
  sqlUsuNOMBRE.AsString := sqlUsuID.AsString;

  if vModi then sqlUsuPASS.AsInteger := RutCalcularPassword(vPass);

  sqlUsu.Post;

end;

procedure TDMppal.RutAbrirTablaNuevoUsu;
begin
  DMppal.sqlUsu.Close;
  DMppal.sqlUsu.Open;
end;

Function TDMppal.RutGetMAXTPV : Integer;
begin
  with sqlUpdate2 do
  begin
    Close;
    SQL.Text := 'select max(u.numerousuario) as MaxTPV'
              + ' from fgmusuario u'
              + ' where u.numerousuario <> 999999';
    Open;

    result := FieldByName('MaxTPV').AsInteger;
  end;
end;

Function TDMppal.RutGrabarUsuarioTPV(vPass, vUsu : String; vNumTPV,vTipo : Integer; vModi : Boolean) : Boolean;
begin
//Rut para grabar usuario creado por Usuario mismo
  Result := False;

  try
    sqlUsu.Append;

    sqlUsuID.AsString                   := vUsu;
    sqlUsuTIPOPASSW.AsInteger           := 1;
    sqlUsuNOMBRE.AsString               := vUsu;
    sqlUsuNUMEROUSUARIO.AsInteger       := vNumTPV;
    sqlUsuFECHAPASSWORD.AsDateTime      := Now;
    sqlUsuACTUALIZACIONAUTO.AsInteger   := 0;
    sqlUsuVISIBLE1.AsInteger            := vTipo;

    if vModi then sqlUsuPASS.AsInteger  := RutCalcularPassword(vPass);

    sqlUsu.Post;
    Result := True;

  except
    sqlusu.Cancel;
    Result := False;
    //raise Exception.Create('Ya existe este usuario');

  end;

end;



procedure TDMppal.RutBorrarUsuarioTPV( vUsu : String);
var
  vSQL : String;
begin
//Rut para borrar usuario en caso de error de copia de base de datos
  vSQL := ' delete from FGMUSUARIO where ID = ' + QuotedStr(vUsu);

  with sqlUpdate2 do
  begin
    Close;
    SQL.Text := vSQL;
    ExecSQL;
  end;


end;
//-------------------------------------------------
// FIN Nuevo usuario
//-------------------------------------------------

//-------------------------------------------------
// Copia BD
//-------------------------------------------------
Function TDMppal.RutCopiaBD(vNumUsuario : String) : Boolean;
var
  vPath1, vPath2, vPath3 : String;
  vIni   : TStringList;
  ifIniFile: TInifile;
  sFichero, fOrigen, fDestino : String;
begin
  vPath    := UniServerModule.StartPath;
  sFichero := vPath + 'KIOSLIB.ini';

  sFichero  := UniServerModule.vFichero;

  ifIniFile := TIniFile.Create(sFichero);
  vIni      := TStringList.Create;

  Result := False;

  if FileExists(sFichero) then
  begin
    with ifIniFile do
    begin
      if SectionExists('BDPATH') then
      begin
        ReadSectionValues('BDPATH', vIni);
        vPath1 := ReadString ('BDPATH','Path1', '');
        vPath2 := ReadString ('BDPATH','Path2', '');
      end;
    end;
  end;
  ifIniFile.Free;
  vIni.Free;

  if FileExists(vPath2) then
  begin
    fDestino := copy(vPath2,1,Pos('LIMPIA',UpperCase(vPath2))-1) + vNumUsuario + '.FDB';
    if not FileExists(fDestino) then
    begin
      if CopyFile(Pchar(vPath2),Pchar(fDestino),False) then Result := True;      
    end;
  end;

end;
//-------------------------------------------------
// FIN Copia BD
//-------------------------------------------------


//-------------------------------------------------
// CONFIGURACION
//-------------------------------------------------
procedure TDMppal.RutGrabarConfiguracionDistri(vMarinaPuntoVenta, vMarinaRuta, vMarinaMargen,
                                       vLogisticaPuntoVenta, vLogisticaRuta, vLogisticaMargen,
                                       vSadePuntoVenta, vSadeRuta, vSadeMargen,
                                       vSgelPuntoVenta, vSgelRuta, vSgelMargen : String);
begin


end;

//-------------------------------------------------
// uLoadFiltro
//-------------------------------------------------
procedure TDMppal.RutCapturaGrid(vForm:string; vGridOrigen:TUniDBGrid; Column: TUniDBGridColumn; X,
  Y: Integer; vPopUp:Integer);
begin

  DMppal.vGrid   := vGridOrigen;
  DMppal.vForm   := UpperCase(vForm);

  if vPopUp = 0 then exit;

  DMppal.vColumn := Column;
  DMppal.vGridX  := X;
  DMppal.vGridY  := Y;

  DMppal.vCampoFilterGrid := Column.FieldName;
  DMppal.vValorFilterGrid := Column.Field.AsString;

//  if vPopUpMenu.Tag = 1 then vPopUpMenu.Popup(X, Y);

end;

procedure TDMppal.RutCalculaMargesCaps (zTabCab:TDataSet);
var wVALVENTA, wVALCOSTE, wMARGEN, wTPC : Double;
begin
 wVALVENTA  := 0;
 wVALCOSTE  := 0;
 wMARGEN    := 0;
 wTPC       := 0;

//    ---------------------------------------------------------     Marge 1

 if  (RutExisteCampo(zTabCab,'VALVENTA'))
 and (RutExisteCampo(zTabCab,'VALCOSTERE'))
 and (RutExisteCampo(zTabCab,'ImpMargen1')) Then
 begin
     zTabCab.FieldByName('ImpMargen1').AsFloat := zTabCab.FieldByName('VALVENTA').AsFloat - zTabCab.FieldByName('VALCOSTERE').AsFloat;
     wVALVENTA  := wVALVENTA + zTabCab.FieldByName('VALVENTA').AsFloat;
     wVALCOSTE  := wVALCOSTE + zTabCab.FieldByName('VALCOSTERE').AsFloat;
     wMARGEN    := wMARGEN   + zTabCab.FieldByName('ImpMargen1').AsFloat;
     IF RutExisteCampo(zTabCab,'TpcMargen1') Then
           zTabCab.FieldByName('TpcMargen1').AsFloat := RutTpcDeR (zTabCab.FieldByName('VALVENTA').AsFloat, zTabCab.FieldByName('ImpMargen1').AsFloat,-2);
 end;

//    ---------------------------------------------------------     Marge 2

 if  (RutExisteCampo(zTabCab,'VALVENTA2'))
 and (RutExisteCampo(zTabCab,'VALCOSTERE2'))
 and (RutExisteCampo(zTabCab,'ImpMargen2')) Then
 begin
     zTabCab.FieldByName('ImpMargen2').AsFloat := zTabCab.FieldByName('VALVENTA2').AsFloat - zTabCab.FieldByName('VALCOSTERE2').AsFloat;
     wVALVENTA  := wVALVENTA + zTabCab.FieldByName('VALVENTA2').AsFloat;
     wVALCOSTE  := wVALCOSTE + zTabCab.FieldByName('VALCOSTERE2').AsFloat;
     wMARGEN    := wMARGEN   + zTabCab.FieldByName('ImpMargen2').AsFloat;

     IF RutExisteCampo(zTabCab,'TpcMargen2') Then
           zTabCab.FieldByName('TpcMargen2').AsFloat := RutTpcDeR (zTabCab.FieldByName('VALVENTA2').AsFloat, zTabCab.FieldByName('ImpMargen2').AsFloat,-2);
 end;


end;

Function TDMppal.RutTpcDeR (zCampA,zCampB:Double; zDecim:Integer):Double;
var wCamp : Double;
begin
 Result := 0;
 if (zCampA = 0) or (zCampB = 0) Then Exit;
 Try    wCamp := (zCampA * 100) / zCampB;
 except wCamp := 0;
 end;
 Result := RoundTo(wCamp,zDecim);
end;

procedure TDMppal.ProSortidaPreus (zTipo:Integer);
begin
   if DMppal.RutEdiInse(DMDevolucion.sqllinea) = False Then Exit;

  if DMppal.rutcomprobartopecantitat (DMDevolucion.sqlLinea,1000,0,'PRECIOVENTA','SS') = False Then
  begin
      if FormDevolFicha.pPreu.CanFocus then FormDevolFicha.pPreu.SetFocus;
      Exit;
  end;

  DMDevolucion.sqlLinea.FieldByName('PRECIOCOMPRA').AsFloat := DMDevolucion.sqlLinea.FieldByName('PRECIOVENTA').AsFloat;

  DMMenuArti.RutCalculaCost_Taula     (DMDevolucion.sqllinea, '1H');
  DMMenuArti.RutCalculaCost_Taula_Tot (DMDevolucion.sqllinea, '1H');
  DMMenuArti.RutCalculaCost_Taula     (DMDevolucion.sqllinea, '2H');
  DMMenuArti.RutCalculaCost_Taula_Tot (DMDevolucion.sqllinea, '2H');

//a  RutCalculaLinHisto_N     (DMDevoluA.cdsHistoAnte, 'CANTIENALBA', 2, 0);
//a  RutDevolu_CalculoVendidos (DMDevoluA.cdsHistoAnte, 'COMPRAHISTO');
//  DMDevolucion.RutDevolu_CalculoVendidos_A (DMDevolucion.sqllinea, FormDevoluA.cbRestoVentas1.Checked);
//  if BtnGrabaLin.Enabled Then BtnGrabaLin.SetFocus;

end;

Function TDMppal.RutCalculaLinHisto_N (zTabHis:TFDQuery; zTxCanti:String; zDecimals,zTipo:Integer):Integer;
var wTxCanti : String;
   wDecimals : Integer;
   wImporteT, wImporte1, wImporte2  : Double;
begin
  if DMppal.RutEdiInse(zTabHis) = False Then Exit;

  if  (zTxCanti <> 'CANTIDAD') and (zTxCanti <> 'CANTIENALBA')  Then wTxCanti := 'CANTIDAD'
  else                                                               wTxCanti := zTxCanti;
  wDecimals := zDecimals;
  wImporte2 := 0;

//   -------------------------------------------------------  Import segons Compra o venta
  wImporte1 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA').AsFloat), wDecimals);
  if RutExisteCampo  (zTabHis,'PRECIOVENTA2') Then
  wImporte2 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA2').AsFloat), wDecimals);

  if zTipo = 0 Then            //   'PRECIOCOMPRA'
  begin
    wImporte1 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOMPRA').AsFloat), wDecimals);
    if RutExisteCampo  (zTabHis,'PRECIOCOMPRA2') Then
    wImporte2 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOMPRA2').AsFloat), wDecimals);
  end;

  if zTipo = 1 Then            //   'PRECIOVENTA'
  begin
    wImporte1 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA').AsFloat),  wDecimals);
    if RutExisteCampo  (zTabHis,'PRECIOVENTA2') Then
    wImporte2 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA2').AsFloat), wDecimals);
  end;

  if zTipo = 2 Then            //   'PRECIOCOSTE'
  begin
    wImporte1 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOSTE').AsFloat),  wDecimals);
    if RutExisteCampo  (zTabHis,'PRECIOCOSTE2') Then
    wImporte2 := RoundTo( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOSTE2').AsFloat), wDecimals);
  end;

  wImporteT  := wImporte1 + wImporte2;

//   -------------------------------------------------------  Cost
//   zTabHis.FieldByName('PRECIOCOSTE').AsFloat    := RutRedondeo(zTabHis.FieldByName('PRECIOCOMPRA').AsFloat,2);

//   -------------------------------------------- Calcula: Brut.Net,IVA,Base,Dto
   zTabHis.FieldByName('IMPOBRUTO').AsFloat      :=
        RoundTo( (wImporteT - zTabHis.FieldByName('IMPODTO').AsFloat), wDecimals);

   zTabHis.FieldByName('IMPOIVA').AsFloat        :=
        RutTpcR_ImpoTPC (wImporte1, zTabHis.FieldByName('TPCIVA').AsFloat ,wDecimals);

   zTabHis.FieldByName('IMPORE').AsFloat        :=
        RutTpcR_ImpoTPC (wImporte1, zTabHis.FieldByName('TPCRE').AsFloat ,wDecimals);

   zTabHis.FieldByName('IMPOBASE').AsFloat       :=
        RoundTo( (wImporte1 - (zTabHis.FieldByName('IMPOIVA').AsFloat
                                +  zTabHis.FieldByName('IMPORE').AsFloat)), wDecimals);


//   if (RutExisteCampo (zTabHis,'IMPOIVA2')) and (RutExisteCampo (zTabHis,'TPCIVA2'))
   zTabHis.FieldByName('IMPOIVA2').AsFloat        :=
        RutTpcR_ImpoTPC (wImporte2, zTabHis.FieldByName('TPCIVA2').AsFloat ,wDecimals);

   zTabHis.FieldByName('IMPORE2').AsFloat        :=
        RutTpcR_ImpoTPC (wImporte2, zTabHis.FieldByName('TPCRE2').AsFloat ,wDecimals);

   zTabHis.FieldByName('IMPOBASE2').AsFloat       :=
        RoundTo( (wImporte2 - (zTabHis.FieldByName('IMPOIVA2').AsFloat
                                +  zTabHis.FieldByName('IMPORE2').AsFloat)), wDecimals);

   zTabHis.FieldByName('IMPOTOTLIN').AsFloat     :=
        RoundTo( (zTabHis.FieldByName('IMPOBASE').AsFloat + zTabHis.FieldByName('IMPOIVA').AsFloat
                    + zTabHis.FieldByName('IMPORE').AsFloat
                    + zTabHis.FieldByName('IMPOBASE2').AsFloat + zTabHis.FieldByName('IMPOIVA2').AsFloat
                    + zTabHis.FieldByName('IMPORE2').AsFloat), wDecimals);

//   -------------------------------------------------------  ------------------
   zTabHis.FieldByName('ENTRADAS').AsFloat       := RoundTo(0,2);
   zTabHis.FieldByName('SALIDAS').AsFloat        := RoundTo(0,2);
   zTabHis.FieldByName('VALORCOSTE').AsFloat     := RoundTo(0,2);
   zTabHis.FieldByName('VALORCOMPRA').AsFloat    := RoundTo(0,2);
   zTabHis.FieldByName('VALORVENTA').AsFloat     := RoundTo(0,2);
   zTabHis.FieldByName('VALORMOVI').AsFloat      := RoundTo(0,2);
   zTabHis.FieldByName('IMPOCOMISION').AsFloat   := RoundTo(0,2);

//   -------------------------------------------------------  Entradas / Salidas
   if zTabHis.FieldByName('SWES').AsString  = 'E' Then
      zTabHis.FieldByName('ENTRADAS').AsFloat    := RoundTo(zTabHis.FieldByName(wTxCanti).AsFloat,wDecimals);
   if zTabHis.FieldByName('SWES').AsString  = 'S' Then
      zTabHis.FieldByName('SALIDAS').AsFloat     := RoundTo(zTabHis.FieldByName(wTxCanti).AsFloat,wDecimals);

//   -----------------------------------------------------------  Valor Comision
   RutCalculaLinHistoComisio (zTabHis, zDecimals,zTipo);

//   --------------------------------------------------------------  Valor Coste
   zTabHis.FieldByName('VALORCOSTE').AsFloat     := ((zTabHis.FieldByName(wTxCanti).AsFloat     -  zTabHis.FieldByName('CANTISUSCRI').AsFloat) * zTabHis.FieldByName('PRECIOCOSTE').AsFloat);
   zTabHis.FieldByName('VALORCOSTE').AsFloat     :=   zTabHis.FieldByName('VALORCOSTE').AsFloat - (zTabHis.FieldByName('CANTISUSCRI').AsFloat  * DMMenuRecepcion.wBeneficio);
   zTabHis.FieldByName('VALORCOSTE').AsFloat     :=   RoundTo ( zTabHis.FieldByName('VALORCOSTE').AsFloat, wDecimals);

   zTabHis.FieldByName('VALORCOSTE2').AsFloat    := 0;
   if zTabHis.FieldByName('PRECIOCOSTE2').AsFloat > 0 then
   begin
   zTabHis.FieldByName('VALORCOSTE2').AsFloat    := ((zTabHis.FieldByName(wTxCanti).AsFloat      -  zTabHis.FieldByName('CANTISUSCRI').AsFloat) * zTabHis.FieldByName('PRECIOCOSTE2').AsFloat);
   zTabHis.FieldByName('VALORCOSTE2').AsFloat    :=   zTabHis.FieldByName('VALORCOSTE2').AsFloat - (zTabHis.FieldByName('CANTISUSCRI').AsFloat  * DMMenuRecepcion.wBeneficio);
   zTabHis.FieldByName('VALORCOSTE2').AsFloat    :=   RoundTo ( zTabHis.FieldByName('VALORCOSTE2').AsFloat, wDecimals);
   end;

   zTabHis.FieldByName('VALORCOSTETOT').AsFloat  := RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOSTETOT').AsFloat),  wDecimals);
   zTabHis.FieldByName('VALORCOSTETOT2').AsFloat := RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOSTETOT2').AsFloat), wDecimals);

//   -------------------------------------------------------------  Valor Compra
     if zTabHis.FieldByName('CLAVE').AsString       = '01' Then      //  Compra
     begin
        zTabHis.FieldByName('VALORCOMPRA').AsFloat :=
                RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOMPRA').AsFloat), wDecimals);
        zTabHis.FieldByName('VALORCOMPRA2').AsFloat :=
                RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOCOMPRA2').AsFloat), wDecimals);
     end;
//   --------------------------------------------------------------  Valor Venta
     if  (zTabHis.FieldByName('CLAVE').AsString       = '51')        //  Venta por TPV
      or (zTabHis.FieldByName('CLAVE').AsString       = '52')        //  Venta por TPV Factura ?
      or (zTabHis.FieldByName('CLAVE').AsString       = '53') Then   //  Venta por Calculo
     begin
       zTabHis.FieldByName('VALORVENTA').AsFloat  := RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA').AsFloat), wDecimals);
       zTabHis.FieldByName('VALORVENTA2').AsFloat := RoundTo ( (zTabHis.FieldByName(wTxCanti).AsFloat * zTabHis.FieldByName('PRECIOVENTA2').AsFloat),wDecimals);
     end;
//   -------------------------------------------------------------  Valor Movimiento
     zTabHis.FieldByName('VALORMOVI').AsFloat := RoundTo((zTabHis.FieldByName('IMPOBASE').AsFloat
                                                           + zTabHis.FieldByName('IMPOBASE2').AsFloat),2);
//   if zTabHis.FieldByName('SWES').AsString      = 'E' Then

     if zTabHis.FieldByName('SWES').AsString      = 'S' Then
        zTabHis.FieldByName('VALORMOVI').AsFloat := RoundTo ( (zTabHis.FieldByName('VALORMOVI').AsFloat * -1 ),2);

end;

Function TDMPpal.RutComprobarTopeCantitat (zTabLine:TDataSet; zTope,zDefault:Double; zCamp,zTipo:String):Boolean;
var wTipoA, wTipoB : String;
begin

  if   Length(zTipo) > 1 Then
  begin
        wTipoA := Copy(zTipo,1,1);
        wTipoB := Copy(zTipo,2,1);
  end else
  begin
        wTipoA := zTipo;
        wTipoB := 'N';
  end;

  if   zTabLine.FieldByName(zCamp).AsFloat > zTope Then
  begin
      if wTipoA = 'S' Then ShowMessage('No se permite que ' + zCamp + ' sea superior a ' + FormatFloat(',0.00',zTope));
      if wTipoB = 'S' Then zTabLine.FieldByName(zCamp).AsFloat := zDefault;
      Result := False;
      Exit;
  end;
  Result := True;
end;
//-------------------------------------------------
// FIN uLoadFiltro
//-------------------------------------------------


initialization
  RegisterMainModuleClass(TDMppal);
end.
