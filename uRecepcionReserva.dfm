object FormReservas: TFormReservas
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 611
  ClientWidth = 1024
  Caption = 'FormReservas'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniDBGrid1: TUniDBGrid
    Left = 0
    Top = 0
    Width = 1024
    Height = 611
    Hint = ''
    ShowHint = True
    DataSource = dsReservaRecepcion
    LoadMask.Message = 'Loading data...'
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Columns = <
      item
        FieldName = 'ID_ARTICULO'
        Title.Caption = 'ID'
        Width = 64
      end
      item
        FieldName = 'Descripcion'
        Title.Caption = 'Descripcion'
        Width = 304
      end
      item
        FieldName = 'BARRAS18'
        Title.Caption = 'Barras'
        Width = 112
      end
      item
        FieldName = 'CANTIRESERVA'
        Title.Caption = 'Canti a Reservar'
        Width = 85
      end
      item
        FieldName = 'CANTIRECI'
        Title.Caption = 'Canti.Recibida'
        Width = 73
      end>
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsReservaRecepcion: TDataSource
    DataSet = DMMenuRecepcion.sqlReservasRecepcion
    Left = 960
    Top = 496
  end
end
