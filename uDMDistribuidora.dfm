object DMDistribuidora: TDMDistribuidora
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 653
  Width = 978
  object sqlCabe: TFDQuery
    BeforeEdit = sqlCabeBeforeEdit
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FMPROVE'
      'where ID_PROVEEDOR = 0'
      'Order by ID_PROVEEDOR')
    Left = 104
    Top = 128
    object sqlCabeID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeTPROVEEDOR: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
    object sqlCabeNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlCabeNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlCabeDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeDIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlCabePOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabePROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlCabeCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object sqlCabeCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabeCONTACTO1NOMBRE: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlCabeCONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlCabeTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlCabeEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlCabeWEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlCabeMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlCabeBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object sqlCabeAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object sqlCabeDC: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object sqlCabeCUENTABANCO: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object sqlCabeTEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlCabeEFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlCabeFRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlCabeDIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object sqlCabeDIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlCabeDIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlCabeDIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlCabeTDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object sqlCabeTPAGO: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object sqlCabeSWTFACTURACION: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object sqlCabeSWBLOQUEO: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object sqlCabeREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlCabeTRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlCabeRUTA: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object sqlCabePROVEEDORHOSTING: TIntegerField
      FieldName = 'PROVEEDORHOSTING'
      Origin = 'PROVEEDORHOSTING'
    end
    object sqlCabeNOMBREHOSTING: TStringField
      FieldName = 'NOMBREHOSTING'
      Origin = 'NOMBREHOSTING'
      Size = 40
    end
    object sqlCabeULTIMAFECHADESCARGA: TDateField
      FieldName = 'ULTIMAFECHADESCARGA'
      Origin = 'ULTIMAFECHADESCARGA'
    end
    object sqlCabeULTIMAFECHAENVIO: TDateField
      FieldName = 'ULTIMAFECHAENVIO'
      Origin = 'ULTIMAFECHAENVIO'
    end
    object sqlCabeCOLUMNASEXCEL: TStringField
      FieldName = 'COLUMNASEXCEL'
      Origin = 'COLUMNASEXCEL'
      Size = 30
    end
    object sqlCabeDESDEFILA: TIntegerField
      FieldName = 'DESDEFILA'
      Origin = 'DESDEFILA'
    end
    object sqlCabeSWRECIBIR: TSmallintField
      FieldName = 'SWRECIBIR'
      Origin = 'SWRECIBIR'
    end
    object sqlCabeSWENVIAR: TSmallintField
      FieldName = 'SWENVIAR'
      Origin = 'SWENVIAR'
    end
    object sqlCabeSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCabeFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCabeFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCabeHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCabeFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCabeHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCabeUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCabeNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlCabeMAXPAQUETE: TIntegerField
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 127
    Top = 4
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 210
    Top = 4
  end
  object sqlPoblacionS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select A.CODIGO,A.POSTAL,A.POBLACION,A.CPROV,B.PROVINCIA,A.CPOST' +
        'AL,A.CPAIS'
      'from  FPOBLA A'
      'left outer join FPROVI B on (A.CPROV = B.CPROVI)'
      'Order by CPOSTAL,POBLACION')
    Left = 112
    Top = 328
    object sqlPoblacionSCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPoblacionSPOSTAL: TIntegerField
      FieldName = 'POSTAL'
      Origin = 'POSTAL'
    end
    object sqlPoblacionSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 30
    end
    object sqlPoblacionSCPROV: TIntegerField
      FieldName = 'CPROV'
      Origin = 'CPROV'
    end
    object sqlPoblacionSPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object sqlPoblacionSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlPoblacionSCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      Size = 3
    end
  end
  object sqlCabeS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_PROVEEDOR,A.NOMBRE,A.DIRECCION,A.POBLACION,A.PROVIN' +
        'CIA,A.CPOSTAL,A.CPROVINCIA,'
      '   A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL'
      ',A.FAX,A.TPROVEEDOR'
      'From FMPROVE A'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 32
    Top = 128
    object sqlCabeSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabeSPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlCabeSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabeSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeSFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlCabeSTPROVEEDOR: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
  end
  object sqlUpdate: TFDQuery
    Connection = DMppal.FDConnection1
    Left = 303
    Top = 4
  end
  object SP_GEN_PROVEEDOR: TFDStoredProc
    Connection = DMppal.FDConnection1
    StoredProcName = 'SP_GEN_PROVEEDOR'
    Left = 248
    Top = 328
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
end
