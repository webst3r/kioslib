unit uLoadFile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniTimer, uniLabel,
  uniHTMLFrame, uniMemo, uniPanel, uniButton, uniBitBtn,
  Vcl.Imaging.jpeg;

type
  TFormLoadFile = class(TUniForm)
    pnlDropbox: TUniPanel;
    mmoFiles: TUniMemo;
    hfrDropbox: TUniHTMLFrame;
    lblDropbox: TUniLabel;
    tmrDone: TUniTimer;
    tmrUpdateFrameName: TUniTimer;
    btSalirUpload: TUniBitBtn;
    btBorrarFile: TUniBitBtn;
    procedure tmrUpdateFrameNameTimer(Sender: TObject);
    procedure tmrDoneTimer(Sender: TObject);
    procedure hfrDropboxAjaxEvent(Sender: TComponent; EventName: string;
      Params: TUniStrings);
    procedure btBorrarFileClick(Sender: TObject);
  private
    { Private declarations }
    FUploadPath: String;
    FCounter: Integer;
    FFileList: TStringList;
    vResult : String;

    procedure StrToStream(const Str: AnsiString; Stream: TStream);
    procedure DCallBack(Sender: TComponent; Res: Integer);
    function RutReducirImagen(vFichero, vExtension: String;
      vSize: Integer): String;

  public
    { Public declarations }
    procedure RutInicioForm;
    procedure RutCerrarForm;
  end;

function FormLoadFile: TFormLoadFile;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormLoadFile: TFormLoadFile;
begin
  Result := TFormLoadFile(DMppal.GetFormInstance(TFormLoadFile));
end;



function CryptStringToBinary(pszString: PansiChar; cchString: DWORD; dwFlags: DWORD;
  pbBinary: PByte; var pcbBinary: DWORD; pdwSkip: PDWORD;
  pdwFlags: PDWORD): BOOL; stdcall;
  external 'Crypt32.dll' name 'CryptStringToBinaryA';



procedure TFormLoadFile.StrToStream(const Str: AnsiString; Stream: TStream);
var
  Count:  DWORD;
  Buffer: PByte;
begin
  Count:= 0;
  if CryptStringToBinary(PAnsiChar(Str), Length(Str), 1, nil, Count, nil, nil) then begin
    GetMem(Buffer, Count);
    try
      if CryptStringToBinary(PAnsiChar(Str), Length(Str), 1, Buffer, Count, nil, nil) then
        Stream.WriteBuffer(Buffer^, Count);
    finally
      FreeMem(Buffer);
    end;
  end;
end;



procedure TFormLoadFile.tmrDoneTimer(Sender: TObject);
begin
  // check on "RunOnce"-Timer FCounter AGAIN (cause of asynchronous there could be new files after timer):
  if FCounter <= 0 then
  begin
//    vResult := '32';
//    MessageDlg('Carga Finalizada, Pulsar Condiciones para actualizar', mtConfirmation, mbYesNo, DCallBack);
  end;
end;

procedure TFormLoadFile.tmrUpdateFrameNameTimer(Sender: TObject);
var
  S: String;
begin
  // set "Frame"-vars in script after showing (FrameName can be different) and then set "run script":
  S := hfrDropbox.HTML.Text;
  if self.Name <> 'fraMultiUpload' then begin
    S := StringReplace(S, 'fraMultiUpload.', self.Name + '.', [rfReplaceAll]);
  end;
  S := StringReplace(S, '//!!! var', 'var', [rfReplaceAll]);
  S := StringReplace(S, 'var RUN = false;', 'var RUN = true;', []);
  hfrDropbox.HTML.Text := S;

end;

procedure TFormLoadFile.RutCerrarForm;
begin
  FFileList.Free;
end;

procedure TFormLoadFile.RutInicioForm;
var
  BasePath: String;
begin

  mmoFiles.Lines.Clear;

  //get BasePath:
  BasePath := UniServerModule.ServerRoot;
  if BasePath = '' then
    BasePath := ExtractFilePath(ParamStr(0));
  if BasePath[Length(BasePath)] <> '\' then
    BasePath := BasePath + '\';
  //set and create the server upload folder:


//  FUploadPath:= BasePath + 'Uploads\';
//  ForceDirectories(FUploadPath);




  //init vars:
  FCounter := 0;
  FFileList := TStringList.Create;
  hfrDropbox.Width := 0;
  hfrDropbox.Height := 0;
end;


procedure TFormLoadFile.DCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes : begin
              if vResult = '31' then
              begin
                if FileExists(FormMenu.FFolderFile) then
                   DeleteFile(FormMenu.FFolderFile);
              end;

            end;
    mrNo  : vResult := 'mbNo';
  end;
end;

procedure TFormLoadFile.btBorrarFileClick(Sender: TObject);
begin
    vResult := '31';
    MessageDlg('Deseas borrar el fichero -> ' + FormMenu.FFolderFile + ' ', mtConfirmation, mbYesNo, DCallBack);
end;

procedure TFormLoadFile.hfrDropboxAjaxEvent(Sender: TComponent; EventName: string; Params: TUniStrings);
var
  FileName: String;
  FileSize: String;
  FileType: String;
  p: Integer;
  F: TFileStream;
  S: String;
  Size: Integer;
begin

  FUploadPath:= FormMenu.FFolderUploadFile;
//  ForceDirectories(FUploadPath);


  //upload init:
  If EventName = 'uploadingfile' then begin
    Inc(FCounter);
    if FCounter = 1 then begin
      lblDropbox.Visible := false;
      mmoFiles.Visible := true;
    end;
    FileName := Params.values['filename'];
    FileSize := Params.values['filesize'];
    FileType := Params.values['filetype'];
    if FFileList.IndexOf(FileName) < 0 then begin
      FFileList.Add(FileName);
      mmoFiles.Lines.Add('... 000% of ' + FileName + ' (' + FileSize + ' Bytes)');
    end else begin
      mmoFiles.Lines[FFileList.IndexOf(FileName)] := '... 000% of ' + FileName + ' (' + FileSize + ' Bytes)';
    end;
  end;

  //progress file uploading:
  if EventName = 'uploadingprogress' then begin
    FileName := Params.values['filename'];
    FileSize := Params.values['filesize'];
    FileType := Params.values['filetype'];
    p := StrToIntDef(Params.Values['percent'], 0);
    mmoFiles.Lines[FFileList.IndexOf(FileName)] := '... ' + Format('%3.3d', [p]) + '% of ' + FileName + ' (' + FileSize + ' Bytes)';
  end;

  //file uploaded:
  if eventname = 'uploadedfile' then begin
    Dec(FCounter);
    FileName := Params.values['filename'];
    FileSize := Params.values['filesize'];
    FileType := Params.values['filetype'];
    mmoFiles.Lines[FFileList.IndexOf(FileName)] := '??? ERROR: Abnormal ??? ' + FileName + ' (' + FileSize + ' Bytes)'; //will be only displayed after exception!
    Size := StrToIntDef(FileSize, 0);
    F := TFileStream.Create(FUploadpath + FileName, fmCreate);
    try
      S := Params.Values['content'];
      p := Pos('base64,', S);
      if p <= 0 then begin
        mmoFiles.Lines[FFileList.IndexOf(FileName)] := '??? ERROR: Content ??? ' + FileName + ' (' + FileSize + ' Bytes)';
      end else begin
        S := Copy(S, p + 7, Length(S) - p - 6);
        StrToStream(AnsiString(S), F);
        if (F.Size <> Size) then begin
          mmoFiles.Lines[FFileList.IndexOf(FileName)] := '??? ERROR: Filesize ??? ' + FileName + ' (' + FileSize + ' Bytes)';
        end else begin
          mmoFiles.Lines[FFileList.IndexOf(FileName)] := FileName + ' (' + FileSize + ' Bytes)';
        end;
      end;
    finally
      F.Free;
    end;
    if FCounter <= 0 then begin
      tmrDone.Enabled:= true;
    end;
//ya esta cargada la imagen

  if (StrToIntDef(FileSize,0) > 500000) or (FormMenu.swReducirImagen = True) then
     RutReducirImagen(FUploadpath + FileName, '.jpg', 600);

  end;

end;


//IMAGE
//------------------------------------------------------------------------------
function TFormLoadFile.RutReducirImagen(vFichero, vExtension: String; vSize : Integer):String;
var
  bmp: TBitmap;
  jpg: TJpegImage;
  scale: Double;
  vExt, vFicheroAux : String;

begin

  vExt := ExtractFileExt(vFichero);
  if UpperCase(vExt)  <> '.JPG' then exit;


  if vExtension = '' then vExtension := '.JPG';
  if vSize      = 0  then vSize      := 500;
  vFicheroAux  := ChangeFileext(vFichero, '_t.JPG');
  Result       := ChangeFileext(vFichero, vExtension);

 // Abrir la imagen
    jpg := TJpegImage.Create;
    try
     // Cargar la imagen
      while FileExists(vFicheroAux) do
      begin
        DeleteFile(vFicheroAux);
      end;
      CopyFile(pchar(vfichero), pChar(vFicheroAux), True);
      while not FileExists(vFicheroAux) do
      begin

      end;
      jpg.Loadfromfile(vFicheroAux);
      if jpg.Height > jpg.Width then
        scale := vSize / jpg.Height
      else
        scale := vSize / jpg.Width;
      bmp := TBitmap.Create;
      try

        bmp.Width := Round(jpg.Width * scale);
        bmp.Height := Round(jpg.Height * scale);
        bmp.Canvas.StretchDraw(bmp.Canvas.Cliprect, jpg);

        jpg.Assign(bmp);

        jpg.SaveToFile(Result);
        if FileExists(vFicheroAux) then DeleteFile(vFicheroAux);
      finally
        bmp.free;
      end;
    finally
      jpg.free;
    end;

end;


end.
