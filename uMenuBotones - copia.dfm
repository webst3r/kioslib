object FormBotones: TFormBotones
  Left = 0
  Top = 0
  ClientHeight = 293
  ClientWidth = 892
  Caption = 'FormBotones'
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  PixelsPerInch = 96
  TextHeight = 13
  object UniSplitter1: TUniSplitter
    Left = 0
    Top = 0
    Width = 0
    Height = 293
    Hint = ''
    Align = alLeft
    ParentColor = False
    Color = clBtnFace
    ExplicitHeight = 92
  end
  object pnlBts2: TUniPanel
    Left = 0
    Top = 0
    Width = 892
    Height = 293
    Hint = ''
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    BorderStyle = ubsNone
    Caption = ''
    ExplicitWidth = 705
    ExplicitHeight = 92
    object UniPageControl1: TUniPageControl
      Left = 0
      Top = 0
      Width = 892
      Height = 293
      Hint = ''
      ActivePage = UniTabSheet1
      Align = alClient
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 1
      ExplicitLeft = 360
      ExplicitTop = 72
      ExplicitWidth = 289
      ExplicitHeight = 193
      object UniTabSheet1: TUniTabSheet
        Hint = ''
        Caption = 'tabNuevaDevol'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 289
        ExplicitHeight = 193
        object UniDBLookupComboBox1: TUniDBLookupComboBox
          Left = 97
          Top = 17
          Width = 145
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          ListField = 'NOMBRE'
          KeyField = 'ID_PROVEEDOR'
          ListFieldIndex = 0
          TabOrder = 0
          Color = clWindow
        end
        object UniLabel3: TUniLabel
          Left = 32
          Top = 20
          Width = 60
          Height = 13
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          Caption = 'Distribuidora'
          TabOrder = 1
        end
        object UniDBEdit1: TUniDBEdit
          Left = 97
          Top = 45
          Width = 121
          Height = 22
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          TabOrder = 2
        end
        object UniLabel4: TUniLabel
          Left = 52
          Top = 48
          Width = 40
          Height = 13
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          Caption = 'Paquete'
          TabOrder = 3
        end
        object UniLabel5: TUniLabel
          Left = 29
          Top = 76
          Width = 63
          Height = 13
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          Caption = 'Max Paquete'
          TabOrder = 4
        end
        object UniDBEdit2: TUniDBEdit
          Left = 97
          Top = 73
          Width = 121
          Height = 22
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          TabOrder = 5
        end
      end
      object UniTabSheet2: TUniTabSheet
        Hint = ''
        Caption = 'UniTabSheet2'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 289
        ExplicitHeight = 193
      end
      object UniTabSheet3: TUniTabSheet
        Hint = ''
        Caption = 'UniTabSheet3'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 289
        ExplicitHeight = 193
      end
    end
  end
end
