unit uDMManteTPV;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDMManteTPV = class(TDataModule)

  private

    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    end;

function DMManteTPV: TDMManteTPV;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal;

function DMManteTPV: TDMManteTPV;
begin
  Result := TDMManteTPV(DMppal.GetModuleInstance(TDMManteTPV));
end;


initialization
  RegisterModuleClass(TDMManteTPV);





end.



