unit uDevolFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox;

type
  TFormDevolFicha = class(TUniForm)
    ImgNativeList: TUniNativeImageList;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    UniBitBtn29: TUniBitBtn;
    btModificar: TUniBitBtn;
    UniBitBtn31: TUniBitBtn;
    UniBitBtn17: TUniBitBtn;
    UniBitBtn22: TUniBitBtn;
    UniBitBtn23: TUniBitBtn;
    UniBitBtn32: TUniBitBtn;
    btLista: TUniBitBtn;
    btOpciones: TUniBitBtn;
    btCerrarFicha: TUniBitBtn;
    dsLinea: TDataSource;
    dsCabe1: TDataSource;
    dsHistoAnteF: TDataSource;
    dsHistoAnteFT: TDataSource;
    dsHistoDevolver: TDataSource;
    dsHistoDevolverProve: TDataSource;
    dsCabeSCap: TDataSource;
    dsProveS: TDataSource;
    UniPanel1: TUniPanel;
    UniPageControl3: TUniPageControl;
    TabSheet6: TUniTabSheet;
    pnlCBarras: TUniPanel;
    UniLabel9: TUniLabel;
    pBarrasA: TUniEdit;
    pAdendumEntrar: TUniDBEdit;
    UniLabel10: TUniLabel;
    pCantidad: TUniDBEdit;
    UniLabel11: TUniLabel;
    BtnSumarCanti: TUniBitBtn;
    BtnRestarCanti: TUniBitBtn;
    pGraDescripcion: TUniDBEdit;
    UniLabel59: TUniLabel;
    UniLabel39: TUniLabel;
    UniLabel40: TUniLabel;
    pPrecio: TUniDBEdit;
    pPreu: TUniDBEdit;
    UniDBEdit37: TUniDBEdit;
    pPreu2: TUniDBEdit;
    btMasInfo: TUniBitBtn;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    UniLabel37: TUniLabel;
    UniDBEdit30: TUniDBEdit;
    paStock: TUniDBEdit;
    pMerma: TUniDBEdit;
    UniDBEdit33: TUniDBEdit;
    UniDBEdit34: TUniDBEdit;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    pFeAbono: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    pnlTotales: TUniPanel;
    pMargen1: TUniDBEdit;
    pMargen2: TUniDBEdit;
    pEncarte2: TUniDBEdit;
    pEncarte1: TUniDBEdit;
    UniLabel41: TUniLabel;
    UniLabel42: TUniLabel;
    pTIva1: TUniDBEdit;
    pTIva2: TUniDBEdit;
    UniDBEdit47: TUniDBEdit;
    UniDBEdit48: TUniDBEdit;
    UniDBEdit49: TUniDBEdit;
    UniDBEdit50: TUniDBEdit;
    UniLabel43: TUniLabel;
    UniLabel44: TUniLabel;
    UniLabel45: TUniLabel;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel46: TUniLabel;
    UniLabel47: TUniLabel;
    GridPreSeleccio: TUniDBGrid;
    dsCabeNuevo: TDataSource;
    UniDBEdit1: TUniDBEdit;
    UniLabel3: TUniLabel;
    dsLineaTotal: TDataSource;
    UniPanel2: TUniPanel;
    btArriba: TUniBitBtn;
    btAnularLinea: TUniButton;
    UniEdit1: TUniDBEdit;
    UniLabel4: TUniLabel;
    btAbajoTodo: TUniBitBtn;
    btAbajo: TUniBitBtn;
    btArribaTodo: TUniBitBtn;
    edMensajes: TUniEdit;
    UniEdit2: TUniEdit;
    UniDBEdit55: TUniDBEdit;
    UniDBEdit56: TUniDBEdit;
    UniDBEdit57: TUniDBEdit;
    UniDBEdit58: TUniDBEdit;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniDBEdit59: TUniDBEdit;
    UniDBEdit60: TUniDBEdit;
    UniDBEdit61: TUniDBEdit;
    UniDBEdit62: TUniDBEdit;
    UniLabel50: TUniLabel;
    UniLabel51: TUniLabel;
    UniLabel52: TUniLabel;
    UniLabel53: TUniLabel;
    UniLabel54: TUniLabel;
    UniDBText5: TUniDBText;
    UniDBText6: TUniDBText;
    UniDBText7: TUniDBText;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniDBText8: TUniDBText;
    UniDBText9: TUniDBText;
    UniPanel3: TUniPanel;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniPanel4: TUniPanel;
    gridCabe1: TUniDBGrid;
    pnlNavigator: TUniPanel;
    btSumarPaquete: TUniBitBtn;
    btRestarPaquete: TUniBitBtn;
    btBuscarArti: TUniBitBtn;
    procedure btListaClick(Sender: TObject);
    procedure btOpcionesClick(Sender: TObject);
    procedure btMasInfoClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure btNuevoClick(Sender: TObject);
    procedure btCerrarFichaClick(Sender: TObject);
    procedure pBarrasAExit(Sender: TObject);
    procedure GridPreSeleccioColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure pBarrasAEnter(Sender: TObject);
    procedure BtnSumarCantiClick(Sender: TObject);
    procedure BtnRestarCantiClick(Sender: TObject);
    procedure pCantidadExit(Sender: TObject);
    procedure pCantidadEnter(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure btAnularLineaClick(Sender: TObject);
    procedure pAdendumEntrarEnter(Sender: TObject);
    procedure pAdendumEntrarExit(Sender: TObject);
    procedure btAbajoTodoClick(Sender: TObject);
    procedure btAbajoClick(Sender: TObject);
    procedure btArribaClick(Sender: TObject);
    procedure btArribaTodoClick(Sender: TObject);
    procedure btRestarPaqueteClick(Sender: TObject);
    procedure btBuscarArtiClick(Sender: TObject);


  private
    procedure RutLeerProveedor(vID: Integer);



    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    vIDProve : Integer;
    swClicPnl : Boolean;
     swUnico : Boolean;

     swExiste :Integer ;

     procedure RutMensajeArticulo(vTipoMensaje : Integer);
     procedure RutMensajeDistri;
  end;

function FormDevolFicha: TFormDevolFicha;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDevolLista,  uDMDevol, uDevolNuevo, uDevolMenu,
  ufrFicha, uConstVar, uListaArti, uConsArti;

function FormDevolFicha: TFormDevolFicha;
begin
  Result := TFormDevolFicha(DMppal.GetFormInstance(TFormDevolFicha));

end;

procedure TFormDevolFicha.btImprimirClick(Sender: TObject);
begin
  FormfrFicha.vModifica    := False;
  FormfrFicha.InvNum       := '1';
  FormfrFicha.vTipoInforme := 'iDevol';
  FormfrFicha.vExportPDF   := False;
  FormfrFicha.ShowModal;

  DMDevolucion.RutCalcularCamposInforme;

end;

procedure TFormDevolFicha.btListaClick(Sender: TObject);
begin
  FormDevolMenu.RutAbrirLista;
end;

procedure TFormDevolFicha.btNuevoClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirTablasNuevo;
  DMDevolucion.RutNuevaCabe;
  FormDevolNuevo.swEdit := False;
  FormDevolNuevo.ShowModal();

end;

procedure TFormDevolFicha.btOpcionesClick(Sender: TObject);
begin
   UniPanel27.Visible := not unipanel27.visible;
end;

procedure TFormDevolFicha.btRestarPaqueteClick(Sender: TObject);
begin
  DMDevolucion.sqlCabe1.Edit;
  DMDevolucion.sqlCabe1PAQUETE.AsInteger := DMDevolucion.sqlCabe1PAQUETE.AsInteger - 1;
  DMDevolucion.sqlCabe1.Post;

  DMDevolucion.sqlCabe1.Refresh;
end;

procedure TFormDevolFicha.GridPreSeleccioColumnSummary(
  Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'nomrep') then
  begin
    if Column.AuxValue = NULL then Column.AuxValue:='';
    Column.AuxValue    := 'Total';
  end else
  begin
    if Column.AuxValue = NULL then Column.AuxValue:=0.0;
    Column.AuxValue    := Column.AuxValue + Column.Field.AsFloat;
  end;
end;

procedure TFormDevolFicha.pAdendumEntrarEnter(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clLime;
end;

procedure TFormDevolFicha.pAdendumEntrarExit(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clWindow;


  if (StrToIntDef(pAdendumEntrar.Text,0) > 0) and (DMDevolucion.sqlLinea.RecordCount > 0) then
  begin
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaADENDUM.AsInteger := StrToInt(pAdendumEntrar.Text);
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

    DMDevolucion.sqlLinea.Refresh;
  end;

  pCantidad.SetFocus;
end;

procedure TFormDevolFicha.pBarrasAEnter(Sender: TObject);
begin
  (sender as TUniEdit).Color := clLime;
  pBarrasA.Text  := '';
  pAdendumEntrar.Text  := '';
end;


procedure TFormDevolFicha.pBarrasAExit(Sender: TObject);
begin
  //(sender as TUniEdit).Color := clWindow;


  if pBarrasA.Text > '' then
  begin
    DMppal.RutSepararCodigoBarras(pBarrasA.Text);




      DMDevolucion.RutLeerCodigoBarras;
      DMDevolucion.sqlLinea.Refresh;
      DMDevolucion.sqlLineaTotal.Refresh;
      DMDevolucion.RutLocateLinea(DMppal.sqlHisArtiDev1.FieldByName('ID_HISARTI').AsInteger);


      if DMDevolucion.swNoExiste = true then pBarrasA.SetFocus
      else  pCantidad.SetFocus;




  end;
  DMDevolucion.sqlCabe1.Refresh; //para actualizar el cabe1 el totalDevueltos

end;

procedure TFormDevolFicha.RutMensajeDistri;
begin
  if swUnico then
  begin
    RutMensajeArticulo(2);
    pBarrasA.SetFocus;
  end else
  begin
    FormMenu.vResult := '2';
    RutLeerProveedor(DMppal.vID);
    MessageDlg('La Distribuidora no existe en la devolucion, �quieres crear una?', mtConfirmation, mbYesNo,FormMenu.DCallBack);
    pCantidad.SetFocus;

  end;


end;


procedure TFormDevolFicha.RutLeerProveedor(vID:Integer);
begin

  with DMppal.sqlProveedor2 do
  begin
    Close;
    SQL.Text := 'Select * ' +
                'From FMPROVE ' +
                'Where ID_PROVEEDOR = ' + IntToStr(vID);


    Open;
  end;

end;

procedure TFormDevolFicha.RutMensajeArticulo(vTipoMensaje : Integer);
begin
    if vTipomensaje = 1 then
    begin
      edMensajes.Text := 'Correcto';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 2 then
    begin
     edMensajes.Text := '�La Distribuidora del articulo no existe en la devolucion!';
     edMensajes.Color := clYellow;
    end;

    if vTipoMensaje = 3 then
    begin
      edMensajes.Text := 'No has Comprado el Articulo';
      edMensajes.Color := clYellow;
    end;

    if vTipoMensaje = 4 then
    begin
      edMensajes.Text := '�No existe ninguna distribuidora para este articulo!';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 5 then
    begin
      edMensajes.Text := '�Este Articulo no existe!';
      edMensajes.Color := clRed;
      FormMenu.vResult := '5';
      MessageDlg('� Quieres Crear un Nuevo Articulo ?',mtConfirmation, mbYesNo, FormMenu.DCallBack);

    end;

    if vTipoMensaje = 6 then
    begin
      edMensajes.Text := 'Devoluci�n Cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 7 then
    begin
      edMensajes.Text := 'Devoluci�n abierta';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 8  then
    begin
      edMensajes.Text := 'No se permite anular la linea si esta la devoluci�n cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 9  then
    begin
      edMensajes.Text := 'Se han Actualizado los precios';
      edMensajes.Color := clYellow;
    end;
end;



procedure TFormDevolFicha.pCantidadEnter(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clLime;
end;



procedure TFormDevolFicha.pCantidadExit(Sender: TObject);
begin


  (sender as TUniDBEdit).Color := clWindow;
  if pCantidad.Text = '' then
  begin
    pCantidad.Text := IntToStr(0);
  end;

  if (StrToIntDef(pCantidad.Text,0) > 0) and (DMDevolucion.sqlLinea.RecordCount > 0) then
  begin
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaCANTIDAD.AsInteger := StrToInt(pCantidad.Text);
    Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
    DMDevolucion.sqlLinea.Post;

    DMDevolucion.sqlLinea.Refresh;
  end;

  pBarrasA.SetFocus;


end;

procedure TFormDevolFicha.btAbajoClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Next;
end;

procedure TFormDevolFicha.btAbajoTodoClick(Sender: TObject);
begin
  DMDevolucion.sqlCabe1.Edit;
  DMDevolucion.sqlCabe1PAQUETE.AsInteger := DMDevolucion.sqlCabe1PAQUETE.AsInteger + 1;
  DMDevolucion.sqlCabe1.Post;

  DMDevolucion.sqlCabe1.Refresh;
  DMDevolucion.sqlLinea.Refresh;
end;

procedure TFormDevolFicha.btArribaClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Prior;
end;

procedure TFormDevolFicha.btArribaTodoClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.First;
end;

procedure TFormDevolFicha.btCerrarFichaClick(Sender: TObject);
begin
//  if FormDevolucion.swDevoAlbaran = 1 then FormDevolucion.pcDetalle.ActivePage := FormDevolucion.tabAlbaranDevolucion;
  FormDevolMenu.swDevoAlbaran := 0;

  self.Close;
end;

procedure TFormDevolFicha.btAnularLineaClick(Sender: TObject);
begin
  if DMDevolucion.sqlLinea.RecordCount = 0 then exit;

  if (DMDevolucion.sqlLinea.FieldByName('SWCERRADO').AsInteger = 1)
  or (DMDevolucion.sqlLinea.FieldByName('SWDEVOLUCION').AsInteger > 2) Then
           RutMensajeArticulo(8)  //'No se permite anular linea si esta cerrada')
     else  DMDevolucion.sqlLinea.Delete;;
end;

procedure TFormDevolFicha.UniFormCreate(Sender: TObject);
begin
  swClicPnl := True;
  DMppal.swCrear := false;
  pnlTotales.Height := 0;

  with gridCabe1 do
  begin
    Columns[0].Width := 160;
    Columns[1].Width := 64;
    Columns[2].Width := 64;
    Columns[3].Width := 64;
    Columns[4].Width := 64;
  end;
end;

procedure TFormDevolFicha.btMasInfoClick(Sender: TObject);
var
  vHeight : integer;
begin
  vHeight := 60;
  if swClicPnl then
  begin
    pnlTotales.Height := vHeight;
    GridPreSeleccio.Height := GridPreSeleccio.Height - vHeight;
  end
  else
  begin
    pnlTotales.Height := 0;
    GridPreSeleccio.Height := GridPreSeleccio.Height + vHeight;
  end;
  swClicPnl := not swClicPnl;
end;



procedure TFormDevolFicha.BtnRestarCantiClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Edit;
  DMDevolucion.sqlLineaCANTIDAD.AsInteger  := DMDevolucion.sqlLineaCANTIDAD.AsInteger - 1;
  DMDevolucion.sqlLineaDEVUELTOS.AsInteger := DMDevolucion.sqlLineaDEVUELTOS.AsInteger - 1;
  Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
  DMDevolucion.sqlLinea.Post;
  DMDevolucion.sqlLinea.Refresh;
end;

procedure TFormDevolFicha.BtnSumarCantiClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Edit;
  DMDevolucion.sqlLineaCANTIDAD.AsInteger  := DMDevolucion.sqlLineaCANTIDAD.AsInteger + 1;
  DMDevolucion.sqlLineaDEVUELTOS.AsInteger := DMDevolucion.sqlLineaDEVUELTOS.AsInteger + 1;
  Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
  DMDevolucion.sqlLinea.Post;
  DMDevolucion.sqlLinea.Refresh;
end;

procedure TFormDevolFicha.btBuscarArtiClick(Sender: TObject);
begin
  FormMenu.RutAbrirConsArti;
  FormConsArti.RutInicioShow('devolucion');
  FormConsArti.vIdEditorial := 0;
end;


end.
