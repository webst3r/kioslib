object FormHistoArti: TFormHistoArti
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 660
  ClientWidth = 1000
  Caption = ''
  OnShow = UniFormShow
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  BorderIcons = [biSystemMenu]
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBts: TUniPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 121
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object edBarras: TUniDBEdit
      Left = 264
      Top = 24
      Width = 185
      Height = 22
      Hint = ''
      ShowHint = True
      ParentFont = False
      Font.Height = -13
      TabOrder = 1
    end
    object edAdendum: TUniDBEdit
      Left = 449
      Top = 24
      Width = 65
      Height = 22
      Hint = ''
      ShowHint = True
      ParentFont = False
      Font.Height = -13
      TabOrder = 2
    end
    object edDescripcion: TUniDBEdit
      Left = 514
      Top = 24
      Width = 255
      Height = 22
      Hint = ''
      ShowHint = True
      ParentFont = False
      Font.Height = -13
      TabOrder = 3
    end
    object UniLabel1: TUniLabel
      Left = 328
      Top = 8
      Width = 42
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'C.barras'
      TabOrder = 4
    end
    object UniLabel2: TUniLabel
      Left = 519
      Top = 5
      Width = 93
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Descripci'#243'n Art'#237'culo'
      TabOrder = 5
    end
    object UniLabel3: TUniLabel
      Left = 467
      Top = 5
      Width = 21
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Num'
      TabOrder = 6
    end
    object lbArti: TUniLabel
      Left = 712
      Top = 5
      Width = 25
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'lbArti'
      TabOrder = 7
    end
    object btnSumaCabe: TUniButton
      Left = 608
      Top = 72
      Width = 55
      Height = 44
      Hint = ''
      ShowHint = True
      Caption = 'Sumar'
      TabOrder = 8
      OnClick = btnSumaCabeClick
    end
    object edDesdeFecha: TUniDateTimePicker
      Left = 431
      Top = 72
      Width = 113
      Hint = ''
      ShowHint = True
      DateTime = 43431.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 9
      ParentFont = False
      Font.Height = -13
    end
    object edHastaFecha: TUniDateTimePicker
      Left = 431
      Top = 94
      Width = 113
      Hint = ''
      ShowHint = True
      DateTime = 43431.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 10
      ParentFont = False
      Font.Height = -13
    end
    object btDevolucion: TUniButton
      Left = 725
      Top = 72
      Width = 55
      Height = 44
      Hint = ''
      ShowHint = True
      Caption = 'Devol'
      TabOrder = 11
      OnClick = btDevolucionClick
    end
    object btRecepcion: TUniButton
      Left = 666
      Top = 72
      Width = 55
      Height = 44
      Hint = ''
      ShowHint = True
      Caption = 'Recep'
      TabOrder = 12
    end
    object UniLabel4: TUniLabel
      Left = 395
      Top = 76
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 13
    end
    object UniLabel5: TUniLabel
      Left = 397
      Top = 98
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 14
    end
    object btEjecutar: TUniButton
      Left = 550
      Top = 72
      Width = 55
      Height = 44
      Hint = ''
      ShowHint = True
      Caption = 'Ejecutar'
      TabOrder = 15
      OnClick = btEjecutarClick
    end
    object rgSeleStock: TUniRadioGroup
      Left = 5
      Top = 72
      Width = 244
      Height = 39
      Hint = ''
      ShowHint = True
      Items.Strings = (
        'Todos'
        'Stock + / -'
        'Solo +')
      ItemIndex = 1
      Caption = ''
      TabOrder = 16
      Columns = 3
      Vertical = False
      OnClick = rgSeleStockClick
    end
  end
  object GridStock: TUniDBGrid
    Left = 0
    Top = 121
    Width = 488
    Height = 539
    Hint = ''
    ShowHint = True
    DataSource = dsStock
    WebOptions.Paged = False
    LoadMask.Message = 'Loading data...'
    Align = alLeft
    Anchors = [akLeft, akTop, akBottom]
    Font.Height = -13
    ParentFont = False
    TabOrder = 1
    Summary.Enabled = True
    OnColumnSummary = GridStockColumnSummary
    OnColumnSummaryResult = GridStockColumnSummaryResult
    Columns = <
      item
        FieldName = 'ADENDUM'
        Title.Caption = 'Num.'
        Width = 64
        Font.Height = -13
        ShowSummary = True
      end
      item
        FieldName = 'ENTRADAS'
        Title.Caption = 'Compras'
        Width = 65
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'VENTAS'
        Title.Caption = 'Ventas'
        Width = 65
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'VENTASAUTO'
        Title.Caption = 'VentasAuto'
        Width = 78
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'DEVOLUCIONES'
        Title.Caption = 'Devolucion'
        Width = 80
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'MERMA'
        Title.Caption = 'Merma'
        Width = 57
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end
      item
        FieldName = 'STOCK'
        Title.Caption = 'Stock'
        Width = 52
        Font.Height = -13
        ReadOnly = True
        ShowSummary = True
      end>
  end
  object GridHisto: TUniDBGrid
    Left = 488
    Top = 121
    Width = 512
    Height = 539
    Hint = ''
    ShowHint = True
    DataSource = dsHisArti
    ReadOnly = True
    WebOptions.Paged = False
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    Summary.Enabled = True
    OnDrawColumnCell = GridHistoDrawColumnCell
    OnColumnSummary = GridHistoColumnSummary
    OnColumnSummaryResult = GridHistoColumnSummaryResult
    Columns = <
      item
        FieldName = 'SWES'
        Title.Caption = 'ES'
        Width = 16
      end
      item
        FieldName = 'FECHA'
        Title.Caption = 'Fecha'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'HORA'
        Title.Caption = 'Hora'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'CLAVE'
        Title.Caption = 'Clave'
        Width = 31
      end
      item
        FieldName = 'ADENDUM'
        Title.Caption = 'Num'
        Width = 34
      end
      item
        FieldName = 'ENTRADAS'
        Title.Caption = 'Entradas'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'SALIDAS'
        Title.Caption = 'Salidas'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'SWESTADO'
        Title.Caption = 'Ind.Esta'
        Width = 64
      end
      item
        FieldName = 'SWTIPOFRA'
        Title.Caption = 'Ind.T.Fra.'
        Width = 64
      end
      item
        FieldName = 'NFACTURA'
        Title.Caption = 'N.Factura'
        Width = 64
      end
      item
        FieldName = 'NALBARAN'
        Title.Caption = 'N.Ticket'
        Width = 64
      end
      item
        FieldName = 'PRECIOCOMPRA'
        Title.Caption = 'Pr.Compra'
        Width = 64
      end
      item
        FieldName = 'PRECIOCOSTE'
        Title.Caption = 'Pr.Coste'
        Width = 64
      end
      item
        FieldName = 'PRECIOVENTA'
        Title.Caption = 'Pr.Venta'
        Width = 64
      end
      item
        FieldName = 'TPCIVA'
        Title.Caption = '%IVA'
        Width = 64
      end
      item
        FieldName = 'TPCRE'
        Title.Caption = '%R.E.'
        Width = 64
      end
      item
        FieldName = 'IMPOTOTLIN'
        Title.Caption = 'Imp.Tot.'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'VALORCOSTE'
        Title.Caption = 'Val.Coste'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'VALORCOMPRA'
        Title.Caption = 'Val.Compra'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'VALORVENTA'
        Title.Caption = 'Val.Venta'
        Width = 64
        ShowSummary = True
      end
      item
        FieldName = 'ID_CLIENTE'
        Title.Caption = 'Id.Clie'
        Width = 64
      end
      item
        FieldName = 'NOMCLIE'
        Title.Caption = 'Nombre Cliente'
        Width = 244
        ReadOnly = True
      end
      item
        FieldName = 'ID_PROVEEDOR'
        Title.Caption = 'Id.Prove'
        Width = 64
      end
      item
        FieldName = 'NOMPROVE'
        Title.Caption = 'Nombre Proveedor'
        Width = 244
        ReadOnly = True
      end
      item
        FieldName = 'IDSTOCABE'
        Title.Caption = 'IDRecep'
        Width = 64
      end
      item
        FieldName = 'CANTIENALBA'
        Title.Caption = 'Recibido'
        Width = 64
      end
      item
        FieldName = 'TIPOVENTA'
        Title.Caption = 'TipoVta'
        Width = 64
      end>
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsStock: TDataSource
    DataSet = DMHistoArti.sqlStock
    Left = 952
    Top = 592
  end
  object dsHisArti: TDataSource
    DataSet = DMHistoArti.sqlHisArti
    Left = 952
    Top = 536
  end
end
