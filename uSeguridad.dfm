object UniForm1: TUniForm1
  Left = 0
  Top = 0
  ClientHeight = 667
  ClientWidth = 1104
  Caption = 'UniForm1'
  BorderStyle = bsNone
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object UniPageControl1: TUniPageControl
    Left = 0
    Top = 0
    Width = 1104
    Height = 667
    Hint = ''
    ActivePage = tabDevolucion
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 651
    ExplicitHeight = 338
    object tabDevolucion: TUniTabSheet
      Hint = ''
      Caption = 'tabDevolucion'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 651
      ExplicitHeight = 338
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 1096
        Height = 105
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        ExplicitTop = 8
        ExplicitWidth = 643
        object UniPanel2: TUniPanel
          Left = 1
          Top = 1
          Width = 1094
          Height = 44
          Hint = ''
          ShowHint = True
          Align = alTop
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          Caption = ''
          ExplicitLeft = 3
          ExplicitTop = 50
          ExplicitWidth = 558
          object UniDBEdit1: TUniDBEdit
            Left = 10
            Top = 19
            Width = 40
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'ID_PROVEEDOR'
            TabOrder = 1
          end
          object UniLabel1: TUniLabel
            Left = 156
            Top = 3
            Width = 54
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Distribuidor'
            TabOrder = 2
          end
          object UniDBEdit2: TUniDBEdit
            Left = 49
            Top = 19
            Width = 279
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'NOMBRE'
            TabOrder = 3
          end
          object UniDBEdit3: TUniDBEdit
            Left = 327
            Top = 19
            Width = 83
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'NDOCSTOCK'
            TabOrder = 4
          end
          object UniDBEdit4: TUniDBEdit
            Left = 409
            Top = 19
            Width = 40
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'PAQUETE'
            TabOrder = 5
          end
          object UniDBDateTimePicker1: TUniDBDateTimePicker
            Left = 448
            Top = 19
            Width = 100
            Hint = ''
            ShowHint = True
            DataField = 'FECHA'
            DateTime = 43374.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 6
          end
          object UniLabel2: TUniLabel
            Left = 329
            Top = 3
            Width = 68
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'N. Documento'
            TabOrder = 7
          end
          object UniLabel3: TUniLabel
            Left = 409
            Top = 3
            Width = 40
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Paquete'
            TabOrder = 8
          end
          object UniLabel4: TUniLabel
            Left = 480
            Top = 3
            Width = 29
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Fecha'
            TabOrder = 9
          end
        end
        object UniPanel3: TUniPanel
          Left = 567
          Top = 0
          Width = 286
          Height = 49
          Hint = ''
          ShowHint = True
          TabOrder = 2
          Caption = ''
          object pFechaPreDevolu: TUniDBDateTimePicker
            Left = 159
            Top = 19
            Width = 100
            Hint = ''
            ShowHint = True
            DataField = 'DEVOLUFECHASEL'
            DateTime = 43374.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 1
          end
          object UniLabel7: TUniLabel
            Left = 10
            Top = 14
            Width = 143
            Height = 23
            Hint = ''
            ShowHint = True
            Caption = 'Pre Devolucion'
            ParentFont = False
            Font.Height = -19
            Font.Style = [fsBold]
            TabOrder = 2
          end
          object UniLabel8: TUniLabel
            Left = 164
            Top = 4
            Width = 84
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Fecha Devolucion'
            TabOrder = 3
          end
        end
        object rgOrdenado: TUniDBRadioGroup
          Left = 3
          Top = 50
          Width = 223
          Height = 45
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 3
          Items.Strings = (
            'Descripci'#243'n'
            'Paquete')
          Columns = 2
        end
        object fcPanel3: TUniPanel
          Left = 232
          Top = 50
          Width = 81
          Height = 45
          Hint = ''
          ShowHint = True
          TabOrder = 4
          Caption = ''
          object UniLabel5: TUniLabel
            Left = 11
            Top = 1
            Width = 54
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'N. Paquete'
            TabOrder = 1
          end
          object UniDBEdit5: TUniDBEdit
            Left = 18
            Top = 17
            Width = 40
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
        end
        object Rgpaquetes: TUniDBRadioGroup
          Left = 319
          Top = 50
          Width = 141
          Height = 45
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 5
          Items.Strings = (
            'Todos'
            'Uno :')
          Columns = 2
        end
        object UniPanel4: TUniPanel
          Left = 459
          Top = 50
          Width = 81
          Height = 45
          Hint = ''
          ShowHint = True
          TabOrder = 6
          Caption = ''
          object UniLabel6: TUniLabel
            Left = 19
            Top = 3
            Width = 21
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Num'
            TabOrder = 1
          end
          object UniDBEdit6: TUniDBEdit
            Left = 18
            Top = 17
            Width = 40
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
        end
        object UniBitBtn1: TUniBitBtn
          Left = 560
          Top = 53
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Utilidades'
          TabOrder = 7
        end
      end
      object UniPanel16: TUniPanel
        Left = 0
        Top = 105
        Width = 1096
        Height = 58
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        Caption = ''
        ExplicitTop = 202
        ExplicitWidth = 1040
        object UniBitBtn13: TUniBitBtn
          Left = 2
          Top = 13
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Anula'
          TabOrder = 1
        end
        object pMargen1: TUniDBEdit
          Left = 181
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN'
          TabOrder = 2
        end
        object pMargen2: TUniDBEdit
          Left = 181
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN2'
          TabOrder = 3
        end
        object pEncarte2: TUniDBEdit
          Left = 240
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE2'
          TabOrder = 4
        end
        object pEncarte1: TUniDBEdit
          Left = 240
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE'
          TabOrder = 5
        end
        object UniLabel41: TUniLabel
          Left = 188
          Top = 3
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Margen'
          TabOrder = 6
        end
        object UniLabel42: TUniLabel
          Left = 244
          Top = 3
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Encarte'
          TabOrder = 7
        end
        object pTIva1: TUniDBEdit
          Left = 303
          Top = 17
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA'
          TabOrder = 8
        end
        object pTIva2: TUniDBEdit
          Left = 303
          Top = 37
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA2'
          TabOrder = 9
        end
        object UniDBEdit47: TUniDBEdit
          Left = 330
          Top = 17
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA'
          TabOrder = 10
        end
        object UniDBEdit48: TUniDBEdit
          Left = 374
          Top = 17
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE'
          TabOrder = 11
        end
        object UniDBEdit49: TUniDBEdit
          Left = 330
          Top = 37
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA2'
          TabOrder = 12
        end
        object UniDBEdit50: TUniDBEdit
          Left = 374
          Top = 37
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE2'
          TabOrder = 13
        end
        object UniLabel43: TUniLabel
          Left = 305
          Top = 3
          Width = 20
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Tipo'
          TabOrder = 14
        end
        object UniLabel44: TUniLabel
          Left = 335
          Top = 3
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '%IVA'
          TabOrder = 15
        end
        object UniLabel45: TUniLabel
          Left = 382
          Top = 3
          Width = 27
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '% RE'
          TabOrder = 16
        end
        object UniDBEdit51: TUniDBEdit
          Left = 419
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE'
          TabOrder = 17
        end
        object UniDBEdit52: TUniDBEdit
          Left = 481
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT'
          TabOrder = 18
        end
        object UniDBEdit53: TUniDBEdit
          Left = 419
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE2'
          TabOrder = 19
        end
        object UniDBEdit54: TUniDBEdit
          Left = 481
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT2'
          TabOrder = 20
        end
        object UniLabel46: TUniLabel
          Left = 429
          Top = 3
          Width = 42
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Coste'
          TabOrder = 21
        end
        object UniLabel47: TUniLabel
          Left = 485
          Top = 3
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Cte+RE'
          TabOrder = 22
        end
        object UniPanel17: TUniPanel
          Left = 545
          Top = 0
          Width = 369
          Height = 57
          Hint = ''
          ShowHint = True
          TabOrder = 23
          Caption = ''
          object UniDBEdit55: TUniDBEdit
            Left = 5
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA'
            TabOrder = 1
          end
          object UniDBEdit56: TUniDBEdit
            Left = 70
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE'
            TabOrder = 2
          end
          object UniDBEdit57: TUniDBEdit
            Left = 5
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA2'
            TabOrder = 3
          end
          object UniDBEdit58: TUniDBEdit
            Left = 70
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE2'
            TabOrder = 4
          end
          object UniLabel48: TUniLabel
            Left = 76
            Top = 1
            Width = 38
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste'
            TabOrder = 5
          end
          object UniLabel49: TUniLabel
            Left = 9
            Top = 1
            Width = 47
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'v.Compra'
            TabOrder = 6
          end
          object UniDBEdit59: TUniDBEdit
            Left = 135
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT'
            TabOrder = 7
          end
          object UniDBEdit60: TUniDBEdit
            Left = 200
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA'
            TabOrder = 8
          end
          object UniDBEdit61: TUniDBEdit
            Left = 135
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT2'
            TabOrder = 9
          end
          object UniDBEdit62: TUniDBEdit
            Left = 200
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA2'
            TabOrder = 10
          end
          object UniLabel50: TUniLabel
            Left = 136
            Top = 1
            Width = 65
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste + RE'
            TabOrder = 11
          end
          object UniLabel51: TUniLabel
            Left = 216
            Top = 1
            Width = 30
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Vta.'
            TabOrder = 12
          end
          object UniLabel52: TUniLabel
            Left = 275
            Top = 13
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Venta'
            TabOrder = 13
          end
          object UniLabel53: TUniLabel
            Left = 275
            Top = 26
            Width = 52
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Devolucion'
            TabOrder = 14
          end
          object UniLabel54: TUniLabel
            Left = 275
            Top = 39
            Width = 32
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Merma'
            TabOrder = 15
          end
          object UniDBText5: TUniDBText
            Left = 305
            Top = 13
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'VENTAHISTO'
          end
          object UniDBText6: TUniDBText
            Left = 305
            Top = 26
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'DEVOLHISTO'
          end
          object UniDBText7: TUniDBText
            Left = 305
            Top = 39
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'MERMAHISTO'
          end
        end
        object UniLabel55: TUniLabel
          Left = 917
          Top = 4
          Width = 25
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Alba.'
          TabOrder = 24
        end
        object UniLabel56: TUniLabel
          Left = 1003
          Top = 4
          Width = 24
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total'
          TabOrder = 25
        end
        object UniLabel57: TUniLabel
          Left = 937
          Top = 27
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 26
        end
        object UniDBText8: TUniDBText
          Left = 945
          Top = 4
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'CANTIENALBA'
        end
        object UniDBText9: TUniDBText
          Left = 979
          Top = 27
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'COMPRAHISTO'
        end
      end
      object BtnNouAdendum: TUniBitBtn
        Left = 2
        Top = 239
        Width = 55
        Height = 27
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Num'
        TabOrder = 2
      end
      object BtnVerVtaAuto: TUniBitBtn
        Left = 58
        Top = 194
        Width = 55
        Height = 42
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Vta Auto'
        TabOrder = 3
      end
      object btVerDirectoDevolucion: TUniBitBtn
        Left = 2
        Top = 194
        Width = 55
        Height = 42
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Directo'
        TabOrder = 4
      end
    end
  end
end
