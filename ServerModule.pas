unit ServerModule;

interface

uses
  Classes, SysUtils, uniGUIServer, uniGUIMainModule, uniGUIApplication, uIdCustomHTTPServer,
  uniGUITypes, dialogs,
  System.IniFiles;

type
  TUniServerModule = class(TUniGUIServerModule)
    procedure UniGUIServerModuleBeforeInit(Sender: TObject);
    procedure UniGUIServerModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure FirstInit; override;
  public
    { Public declarations }

    vFichero, vPortViewDoc : String;
  end;

function UniServerModule: TUniServerModule;

implementation

{$R *.dfm}

uses
  UniGUIVars;

function UniServerModule: TUniServerModule;
begin
  Result:=TUniServerModule(UniGUIServerInstance);
end;

procedure TUniServerModule.FirstInit;
begin
  InitServerModule(Self);
end;

procedure TUniServerModule.UniGUIServerModuleBeforeInit(Sender: TObject);
var
  vPath1, vPort : String;
  vIni   : TStringList;
  ifIniFile: TInifile;
  sFichero, vIniFile : String;
begin

  vPath1    := UniServerModule.StartPath;

  vIniFile := '';
//  vIniFile := Dialogs.InputBox('Archivo a ejecutar', '1=RED 2=OTRO', '');

  sFichero := vPath1 + 'KIOSLIB' + vIniFile + '.ini';

  ifIniFile := TIniFile.Create(sFichero);
  vIni      := TStringList.Create;
  vFichero  := sFichero;
  if FileExists(sFichero) then
  begin
    with ifIniFile do
    begin
      if SectionExists('OPTIONS') then
      begin
        ReadSectionValues('OPTIONS', vIni);
        vPort        := ReadString ('OPTIONS','Port1', '');
        vPortViewDoc := ReadString ('OPTIONS','PortViewDoc', '');
        UniServerModule.Port  := StrToInt(vPort);
        UniServerModule.Title := ReadString ('OPTIONS','Title', '');
      end;
    end;
  end;
  ifIniFile.Free;
  vIni.Free;
end;

{
procedure ExploreWeb(page:PChar);
var Returnvalue: integer;
begin
  ReturnValue := ShellExecute(0, 'open', page, nil, nil,SW_SHOWNORMAL);
  if ReturnValue <= 32 then
     begin
     case Returnvalue of
       0 : MessageBox(0,'Error: Out of memory','Error',0);
       ERROR_FILE_NOT_FOUND: MessageBox(0,'Error: File not found','Error',0);
       ERROR_PATH_NOT_FOUND: MessageBox(0,'Error: Directory not found','Error',0);
       ERROR_BAD_FORMAT    : MessageBox(0,'Error: Wrong format in EXE','Error',0);
     else
       MessageBox(0,PChar('Error Nr: '+IntToStr(Returnvalue)+' inShellExecute'),'Error',0)
     end;
  end;
end;
}
procedure TUniServerModule.UniGUIServerModuleCreate(Sender: TObject);
begin
  MimeTable.AddMimeType('msg', 'application/vnd.ms-outlook', False);
  MimeTable.AddMimeType('eml', 'application/vnd.ms-outlook', False);

//  if self.StandAloneServer then //if you need this, if not, remove it.
//     ExploreWeb('http://localhost:8077'); //auto start browser
end;

initialization
  RegisterServerModuleClass(TUniServerModule);
end.
