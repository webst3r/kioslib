object FormMenuRecepcion: TFormMenuRecepcion
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 568
  ClientWidth = 1016
  Caption = 'FormMenuRecepcion'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalle: TUniPageControl
    Left = 0
    Top = 92
    Width = 1016
    Height = 476
    Hint = ''
    ShowHint = True
    ActivePage = tabListaRecepcion
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    ClientEvents.ExtEvents.Strings = (
      
        'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
        'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
        'Style("border-width", 0);'#13#10'}')
    TabOrder = 0
    object tabListaRecepcion: TUniTabSheet
      Hint = ''
      ShowHint = True
      AlignmentControl = uniAlignmentClient
      ParentAlignmentControl = False
      Caption = 'tabListaRecepcion'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
          'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
          'Style("border-width", 0);'#13#10'}')
      Layout = 'fit'
    end
    object tabAlbaranRecepcion: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabAlbaranRecepcion'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
          'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
          'Style("border-width", 0);'#13#10'}')
      Layout = 'fit'
    end
    object tabReservas: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabReservas'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
          'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
          'Style("border-width", 0);'#13#10'}')
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 92
    Hint = ''
    Visible = False
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    ClientEvents.ExtEvents.Strings = (
      
        'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
        'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
        'Style("border-width", 0);'#13#10'}')
    Caption = ''
    object btBuscar: TUniBitBtn
      Left = 3
      Top = 3
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Buscar'
      TabOrder = 1
    end
    object UniBitBtn2: TUniBitBtn
      Left = 3
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Modificar'
      TabOrder = 2
    end
    object btNuevoAlbaran: TUniBitBtn
      Left = 61
      Top = 3
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Nuevo'
      TabOrder = 3
    end
    object btCerrarAlba: TUniBitBtn
      Left = 61
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Cerrar'
      TabOrder = 4
    end
    object btRefrescarImagenes: TUniBitBtn
      Left = 119
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Actualiza'
      TabOrder = 5
    end
    object btExcelBuscar: TUniBitBtn
      Left = 178
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Cargar'
      TabOrder = 6
    end
    object btModificarTarifas: TUniBitBtn
      Left = 532
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Modificar'
      TabOrder = 7
    end
    object btGrabarTarifas: TUniBitBtn
      Left = 592
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Tarifas'
      TabOrder = 8
    end
    object btVerUltiCompra: TUniBitBtn
      Left = 652
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Compras'
      TabOrder = 9
    end
    object btConfigurar: TUniBitBtn
      Left = 753
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Utilidades'
      TabOrder = 10
    end
    object btReservas: TUniBitBtn
      Left = 812
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Reservas'
      TabOrder = 11
    end
    object btListarDocu: TUniBitBtn
      Left = 812
      Top = 3
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Imprimir'
      TabOrder = 12
    end
    object btSalir: TUniBitBtn
      Left = 870
      Top = 3
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Salir'
      TabOrder = 13
    end
    object UniPanel2: TUniPanel
      Left = 119
      Top = 3
      Width = 688
      Height = 44
      Hint = ''
      ShowHint = True
      TabOrder = 14
      Caption = ''
      object UniLabel1: TUniLabel
        Left = 30
        Top = 5
        Width = 28
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Docto'
        TabOrder = 1
      end
      object UniLabel2: TUniLabel
        Left = 82
        Top = 5
        Width = 60
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Distribuidora'
        TabOrder = 2
      end
      object UniLabel4: TUniLabel
        Left = 263
        Top = 5
        Width = 27
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Sem :'
        TabOrder = 3
      end
      object UniLabel6: TUniLabel
        Left = 358
        Top = 5
        Width = 29
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha'
        TabOrder = 4
      end
      object UniLabel7: TUniLabel
        Left = 430
        Top = 5
        Width = 47
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Su Docto.'
        TabOrder = 5
      end
      object UniLabel8: TUniLabel
        Left = 517
        Top = 5
        Width = 44
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'de Fecha'
        TabOrder = 6
      end
      object UniLabel9: TUniLabel
        Left = 588
        Top = 5
        Width = 71
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha Encargo'
        TabOrder = 7
      end
      object UniDBText1: TUniDBText
        Left = 150
        Top = 5
        Width = 56
        Height = 13
        Hint = ''
        ShowHint = True
      end
      object UniDBText2: TUniDBText
        Left = 293
        Top = 5
        Width = 56
        Height = 13
        Hint = ''
        ShowHint = True
      end
      object UniDBEdit1: TUniDBEdit
        Left = 10
        Top = 19
        Width = 69
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'IDSTOCABE'
        DataSource = dsListaRecepcion1
        TabOrder = 10
      end
      object UniDBEdit2: TUniDBEdit
        Left = 80
        Top = 19
        Width = 256
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'NOMBRE'
        DataSource = dsListaRecepcion1
        TabOrder = 11
      end
      object UniDBEdit3: TUniDBEdit
        Left = 337
        Top = 19
        Width = 80
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'FECHA'
        DataSource = dsListaRecepcion1
        TabOrder = 12
      end
      object UniDBEdit4: TUniDBEdit
        Left = 417
        Top = 19
        Width = 80
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'DOCTOPROVE'
        DataSource = dsListaRecepcion1
        TabOrder = 13
      end
      object UniDBEdit5: TUniDBEdit
        Left = 505
        Top = 19
        Width = 80
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'DOCTOPROVEFECHA'
        DataSource = dsListaRecepcion1
        TabOrder = 14
      end
      object UniDBEdit6: TUniDBEdit
        Left = 585
        Top = 19
        Width = 80
        Height = 20
        Hint = ''
        ShowHint = True
        DataField = 'FECHACARGO'
        DataSource = dsListaRecepcion1
        TabOrder = 15
      end
    end
    object UniBitBtn1: TUniBitBtn
      Left = 288
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Lista'
      TabOrder = 15
      OnClick = UniBitBtn1Click
    end
    object UniBitBtn3: TUniBitBtn
      Left = 345
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Albaran'
      TabOrder = 16
      OnClick = UniBitBtn3Click
    end
    object UniBitBtn4: TUniBitBtn
      Left = 402
      Top = 47
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Reservas'
      TabOrder = 17
      OnClick = UniBitBtn4Click
    end
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsListaRecepcion1: TDataSource
    DataSet = DMMenuRecepcion.sqlListaRecepcion1
    Left = 948
    Top = 412
  end
end
