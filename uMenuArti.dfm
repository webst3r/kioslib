object FormMenuArti: TFormMenuArti
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1100
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalle: TUniPageControl
    Left = 0
    Top = 57
    Width = 1100
    Height = 558
    Hint = ''
    ShowHint = True
    ActivePage = tabFichaArti
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tabListaArti: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabListaArti'
      Layout = 'fit'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
    object tabFichaArti: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabFichaArti'
      Layout = 'fit'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
    object tabConsArti: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabConsArti'
      Layout = 'fit'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1100
    Height = 57
    Hint = ''
    Visible = False
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Caption = ''
    object UniButton1: TUniButton
      Left = 40
      Top = 5
      Width = 81
      Height = 41
      Hint = ''
      ShowHint = True
      Caption = 'Lista'
      TabOrder = 1
    end
    object UniButton2: TUniButton
      Left = 136
      Top = 5
      Width = 81
      Height = 41
      Hint = ''
      ShowHint = True
      Caption = 'Ficha'
      TabOrder = 2
    end
    object UniButton3: TUniButton
      Left = 232
      Top = 5
      Width = 81
      Height = 41
      Hint = ''
      ShowHint = True
      Caption = 'Consultar'
      TabOrder = 3
    end
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
end
