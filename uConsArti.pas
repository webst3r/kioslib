unit uConsArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormConsArti = class(TUniForm)
    tmSession: TUniTimer;
    gridArti: TUniDBGrid;
    dsArtiLink: TDataSource;
    pnlBts: TUniPanel;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    edBusqueda: TUniEdit;
    cbVarias: TUniCheckBox;
    lbConexion: TUniLabel;
    UniHiddenPanel1: TUniHiddenPanel;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    lbDevolucion: TUniLabel;
    btDevolucion: TUniBitBtn;
    btHistoArti: TUniBitBtn;
    lbHistorico: TUniLabel;
    btVerFicha: TUniBitBtn;
    edFicha: TUniLabel;
    rgBD: TUniRadioGroup;
    btBusquedaDevol: TUniBitBtn;
    lbBusquedaDevol: TUniLabel;
    procedure btListaClick(Sender: TObject);
    procedure btFichaClick(Sender: TObject);
    procedure edBusquedaChange(Sender: TObject);
    procedure gridArtiColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure UniFormCreate(Sender: TObject);
    procedure edBusquedaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btHistoArtiClick(Sender: TObject);
    procedure btDevolucionClick(Sender: TObject);
    procedure btVerFichaClick(Sender: TObject);
    procedure btBusquedaDevolClick(Sender: TObject);


  private
    procedure RutDevolucion;
    procedure RutConsFichaArti;


    { Private declarations }

  public
    { Public declarations }

    vIdEditorial : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2, vStringOrigen : string;
    vHora : Double;

    procedure RutInicioShow;

  end;

function FormConsArti: TFormConsArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuArti, uDMHistoArti,
  uDevolLista, uDevolFicha, uDevolMenu, uHistoArti, uFichaArti, uDMDevol;

function FormConsArti: TFormConsArti;
begin
  Result := TFormConsArti(DMppal.GetFormInstance(TFormConsArti));

end;


procedure TFormConsArti.btBusquedaDevolClick(Sender: TObject);
begin
  if DMppal.sqlArticulos.RecordCount <> 0  then
  begin
    FormMenu.RutAbrirDevolucion;
    if (FormMenu.swDevolucion = 1) and (FormDevolMenu.swDevoLista = 1) then
    begin
      FormDevolLista.edBusquedaArti.Text    := DMppal.sqlArticulosDESCRIPCION.AsString;
      FormDevolLista.lbNumeroArti.Caption   := DMppal.sqlArticulosID_ARTICULO.AsString;
      FormDevolLista.RutBuscarArti;

    end;
  end;
end;

procedure TFormConsArti.btDevolucionClick(Sender: TObject);
begin
//  DMppal.swDevolucionFicha := self.Name;
  RutDevolucion;
  FormDevolFicha.pBarrasA.SetFocus;
end;

procedure TFormConsArti.btFichaClick(Sender: TObject);
begin
  //FormMenuArti.RutAbrirFichaArti;
end;

procedure TFormConsArti.btHistoArtiClick(Sender: TObject);
begin
  if DMppal.sqlArticulos.RecordCount <> 0 then
  FormMenu.RutAbrirHistoArtiTablas(DMppal.sqlArticulos.FieldByName('ID_ARTICULO').AsInteger,
                                    DMppal.sqlArticulos.FieldByName('BARRAS').AsString,
                                    DMppal.sqlArticulos.FieldByName('ADENDUM').AsString,
                                    DMppal.sqlArticulos.FieldByName('DESCRIPCION').AsString,
                                    'consulta');
  DMppal.swHisArti := self.Name;
end;
procedure TFormConsArti.btListaClick(Sender: TObject);
begin
  //FormMenuArti.RutAbrirListaArti;
end;

procedure TFormConsArti.btVerFichaClick(Sender: TObject);
begin
  if DMppal.sqlArticulos.RecordCount = 0 then exit;

  DMMenuArti.swCreando := false;
  RutConsFichaArti;
  DMMenuArti.RutAbrirFichaArticulos(DMppal.sqlArticulosID_ARTICULO.AsString);
  FormManteArti.RutHabitlitarBTs(False);
  FormManteArti.RutShow;
  FormMenu.lbFormNombre.Caption := FormMenu.tabArticulos.Caption;
end;

procedure TFormConsArti.RutConsFichaArti;
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;


  FormMenuArti.Parent := FormMenu.tabArticulos;
  FormMenuArti.Align  := alClient;
  FormMenuArti.Show();

  FormMenuArti.pcDetalle.ActivePage := FormMenuArti.tabFichaArti;


  if FormMenu.swFicha = 0 then
  begin
    FormManteArti.Parent := FormMenuArti.tabFichaArti;
    FormManteArti.Align := alClient;
    FormManteArti.Show();
    //FormManteArti.vStringOrigen := 'consArti';
    FormMenu.swFicha := 1;
  end;

  DMppal.swArtiFicha := self.Name;
end;
procedure TFormConsArti.gridArtiColumnSort(Column: TUniDBGridColumn;
  Direction: Boolean);
begin
  DMppal.SortColumnSQL(DMppal.sqlArticulos, Column.FieldName, Direction);
end;

procedure TFormConsArti.edBusquedaChange(Sender: TObject);
begin
  if length(edBusqueda.Text) >= 5 then DMppal.RutConsArti(edBusqueda.Text,0);
end;

procedure TFormConsArti.edBusquedaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (length(edBusqueda.Text) < 5) then DMppal.RutConsArti(edBusqueda.Text,0); //para identificar cuando es un enter
end;

procedure TFormConsArti.UniFormCreate(Sender: TObject);
begin
  if DMppal.vBD = 0 then
  begin
    DMppal.sqlArticulos.Connection := DMppal.FDConnection0;
    lbConexion.Caption := 'Esta conectado la base de datos General';
  end;
  if DMppal.vBD = 1 then
  begin
    DMppal.sqlArticulos.Connection := DMppal.FDConnection1;
    lbConexion.Caption := 'Esta conectado la base de datos Personal';
  end;

  with gridArti do
  begin
    Columns[0].Width := 120;
    Columns[1].Width := 210;
    Columns[2].Width := 55;
    Columns[3].Width := 65;
    Columns[4].Width := 70;
  end;
end;

procedure TFormConsArti.RutInicioShow;
begin
  //vStringOrigen := vOrigen;

  if DMppal.swConsArti = 'FormDevolFicha' then
  begin
   btDevolucion.Visible := True;
   lbDevolucion.Visible := True;

   btHistoArti.Visible := True;
   lbHistorico.Visible := True;
   { btAccion.ImageIndex := 24;
    btAccion.OnClick    := RutDevolucion;
    lbAccion.Caption    := 'Agregar Devolucion';
    lbAccion.OnClick    := RutDevolucion; }
  end;


  if DMppal.swConsArti = 'FormListaArti' then
  begin
    {btAccion.visible := False;
    //btAccion.OnClick    := nil;
    lbAccion.Visible := False;
    cbVarias.Visible := False;}
  end;

  if DMppal.swConsArti = 'FormHistoArti' then
  begin
    btHistoArti.Visible := True;
    lbHistorico.Visible := True;
  end;

  if DMppal.swConsArti = 'FormDevolLista' then
  begin
    btBusquedaDevol.Visible := True;
    lbBusquedaDevol.Visible := True;
  end;

  edBusqueda.SetFocus;
  //DMppal.RutConsArti('',vIdEditorial);
end;
{
procedure TFormConsArti.RutInicioShow(vOrigen : String);
begin
  if FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion then
  begin
    if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion then
    begin
      btAccion.ImageIndex := 23;
      btAccion.OnClick    := RutDevolucion;
      lbAccion.Caption    := 'Agregar Devolucion';
      lbAccion.OnClick    := RutDevolucion;
    end;
  end;

  if FormMenu.pcDetalle.ActivePage = FormMenu.tabArticulos then
  begin
    btAccion.visible := False;
    //btAccion.OnClick    := nil;
    lbAccion.Visible := False;
    cbVarias.Visible := False;
  end;



  edBusqueda.SetFocus;
  //DMppal.RutConsArti('',vIdEditorial);
end;
}

//--------------------------
//Eventos clic
//--------------------------
procedure TFormConsArti.RutDevolucion;
begin
  if DMppal.sqlArticulos.RecordCount <> 0 then
  begin
    FormMenu.RutAbrirDevolucion;
    FormDevolMenu.RutAbrirAlbaranTab;

    FormDevolFicha.pBarrasA.Text := DMppal.sqlArticulosBARRAS.AsString;
    DMMenuArti.RutLeerIdArti(DMppal.sqlArticulosID_ARTICULO.AsInteger);
    DMDevolucion.swRepetido :=  true;
    FormDevolFicha.pBarrasAExit(nil);
    //if not cbVarias.Checked then Self.Close;
  end;
end;

//--------------------------
//Fin Eventos clic
//--------------------------
end.

