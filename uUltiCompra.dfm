object FormUltiCompra: TFormUltiCompra
  Left = 0
  Top = 0
  ClientHeight = 323
  ClientWidth = 568
  Caption = ''
  BorderStyle = bsSizeToolWin
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 323
    Hint = ''
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    BorderStyle = ubsNone
    Caption = ''
    ExplicitWidth = 497
    ExplicitHeight = 130
    object btCancelar: TUniButton
      Left = 444
      Top = 271
      Width = 75
      Height = 38
      Hint = ''
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btCancelarClick
    end
    object lbMensaje: TUniLabel
      Left = 16
      Top = 21
      Width = 295
      Height = 18
      Hint = ''
      Caption = 'Ultimas Compras/Ventas/Devoluciones'
      ParentFont = False
      Font.Color = clNavy
      Font.Height = -15
      Font.Style = [fsBold]
      TabOrder = 2
    end
    object btOk: TUniButton
      Left = 363
      Top = 271
      Width = 75
      Height = 38
      Hint = ''
      Caption = 'Aceptar'
      TabOrder = 3
      OnClick = btOkClick
    end
    object pnPieCompras: TUniPanel
      Left = 3
      Top = 67
      Width = 563
      Height = 192
      Hint = ''
      ShowHint = True
      ParentShowHint = False
      TabOrder = 4
      Caption = ''
      object GridUltiCompra: TUniDBGrid
        Left = 1
        Top = 1
        Width = 561
        Height = 190
        Hint = ''
        ShowHint = True
        DataSource = dsUltiCompra
        WebOptions.Paged = False
        WebOptions.FetchAll = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
        Columns = <
          item
            FieldName = 'ADENDUM'
            Title.Caption = 'Num'
            Width = 66
          end
          item
            FieldName = 'FECHA'
            Title.Caption = 'Fecha'
            Width = 83
          end
          item
            FieldName = 'ENTRADAS'
            Title.Caption = 'Compras'
            Width = 66
            ReadOnly = True
          end
          item
            FieldName = 'VENTAS'
            Title.Caption = 'Ventas'
            Width = 61
            ReadOnly = True
          end
          item
            FieldName = 'DEVOLUCIONES'
            Title.Caption = 'Devoluciones'
            Width = 92
            ReadOnly = True
          end
          item
            FieldName = 'MERMA'
            Title.Caption = 'Merma'
            Width = 61
            ReadOnly = True
          end
          item
            FieldName = 'STOCK'
            Title.Caption = 'Stock'
            Width = 56
            ReadOnly = True
          end>
      end
    end
  end
  object dsUltiCompra: TDataSource
    DataSet = DMUltiCompra.sqlUltiCompra
    Left = 20
    Top = 299
  end
  object ImgList: TUniNativeImageList
    Left = 543
    Top = 139
    Images = {
      02000000FFFFFF1F045D01000089504E470D0A1A0A0000000D49484452000000
      18000000180803000000D7A9CDCA0000000373424954080808DBE14FE0000000
      0970485973000006A5000006A501179997DD0000001974455874536F66747761
      7265007777772E696E6B73636170652E6F72679BEE3C1A00000033504C5445FF
      FFFFCC6633DC5D46D55A4AD6584BD6594BD75A4AD75A4AD75A4AD7594AD75A4A
      D75A4AD75A4ADF7A6DE08175FBEFEEFCF3F2212726F90000000C74524E530005
      16304B6A7F80B2D0E6FAF55ABB46000000844944415428CF75924912C3200C04
      87C532CB98F8FFAFCD010224F1F485AA16258124A0E3A3A55292458F1D77560E
      AAB9E543E6460E1F7F547E518F71FFC793350080CBFC233B00271F30C0F74457
      EBA65D3D99471CE2D5B6838C306E91E969485C91E5995066C176DFD3B3E8804C
      258BCBE7CA0FCA96E826CAB6EB41E9D1EA65785A9F37CEC01529745D0F7E0000
      000049454E44AE426082FFFFFF1F04F901000089504E470D0A1A0A0000000D49
      48445200000018000000180803000000D7A9CDCA0000000373424954080808DB
      E14FE00000000970485973000006A5000006A501179997DD0000001974455874
      536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
      99504C5445FFFFFF33999923AE8B25AF8A25AD8824AD8924AF8926AD8725AD88
      25AE8825AE8825AE8825AE8827AF8928AF8928AF8A2BB08B2CB18C2EB18D38B5
      9239B5933BB6943CB7953DB79544B99948BB9B4BBC9D52BFA157C0A359C1A45D
      C3A664C6AB6FCAB170CAB179CDB680D0BA88D3BE90D6C2A1DCCCABE0D1B7E4D8
      C1E8DDD0EEE5D3EEE7D9F1EADFF3EEE4F5F0E6F6F1E7F6F2E9F7F3F7FCFB3CCE
      D0FA0000000C74524E53000516304B6A7F80B2D0E6FAF55ABB46000000BA4944
      415428CF7D52D70283200C444464A47BEF5DBB6BFBFF1F5729A888A5F740E002
      979004210D4C2813825182918D209660206950F221070B3CCCF94842053232F7
      1D3EF37CDF041C6AE02A4EEC904DB5D02C4F47A8715A2B318C88F3E0786E2943
      10ADF2FB6B17B416ABF09B7B4F6F1812CA7486FAB84CC7E686D08E65BA5066FA
      9A41E1D052F3E70E60F458159A2C0F3EB824FDDBB60C468B74DBC9FB606541AC
      0F4EEC6AE17A498CD29F227ACBEE6F94BFB5FE61F8353E1FEB3C17E7A6193DB8
      0000000049454E44AE426082}
  end
end
