unit uMensajes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormMensajes = class(TUniForm)
    dsArticulo: TDataSource;
    ImgList: TUniNativeImageList;
    UniPanel1: TUniPanel;
    btConfirmar: TUniButton;
    btCancelar: TUniButton;
    lbMensaje: TUniLabel;
    btOk: TUniButton;
    procedure btConfirmarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);


  private





    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    vNuevoUsu : Boolean;
    vg_Tipo : Integer;
    vg_Form : String;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swModificar, swNuevo : Boolean;
    procedure RutInicioForm(vTipo:Integer; vForm, vBotones, swMensaTexto : String);
    procedure RutRespuestas(vTipoRespuesta : Boolean);

  end;

function FormMensajes: TFormMensajes;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule, uNuevoUsuario, uMenuBotones,
  uMenu, uDMMenuArti, uMenuArti, uListaArti, uDMDistribuidora, uDistribuidoraMenu, uDMDevol,
  uDevolFicha, uDevolLista, uDevolNuevo,
  uFichaArti;

function FormMensajes: TFormMensajes;
begin
  Result := TFormMensajes(DMppal.GetFormInstance(TFormMensajes));

end;

procedure TFormMensajes.btCancelarClick(Sender: TObject);
begin
  RutRespuestas(false);
end;

procedure TFormMensajes.btConfirmarClick(Sender: TObject);
begin
  RutRespuestas(true);
end;

procedure TFormMensajes.btOkClick(Sender: TObject);
begin
  RutRespuestas(false);
end;

procedure TFormMensajes.RutInicioForm(vTipo:Integer; vForm, vBotones, swMensaTexto : String);
begin
  vg_Tipo := vTipo;
  vg_Form := vForm;
  lbMensaje.Text := swMensaTexto;

  if vBotones = 'ACEPTAR' then
  begin
    btOk.Visible         := true;
    btConfirmar.Visible  := false;
    btCancelar.Visible   := false;
  end;

  if vBotones = 'ACEPTAROK' then
  begin
    btOk.Visible         := true;
    btOk.OnClick         := btConfirmarClick;
    btConfirmar.Visible  := false;
    btCancelar.Visible   := false;
  end;

  if vBotones = 'SINO' then
  begin
    btOk.Visible        := false;
    btConfirmar.Visible := true;
    btCancelar.Visible  := true;
    btConfirmar.Caption := 'Si';
    btCancelar.Caption  := 'No';
  end;

  if vBotones = 'ACEPTARCANC' then
  begin
    btOk.Visible        := false;
    btConfirmar.Visible := true;
    btCancelar.Visible  := true;
    btConfirmar.Caption := 'Aceptar';
    btCancelar.Caption  := 'Cancelar';
  end;
end;

procedure TFormMensajes.RutRespuestas(vTipoRespuesta : Boolean);
begin
  if vTipoRespuesta then
  begin
    if vg_Form = 'FORMDEVOLFICHA' then
    begin
      if vg_Tipo = 2 then
      begin
         //FormDevolFicha.btNuevoClick(nil);

          //FormDevolLista.btNuevaDevolucionClick(nil);
          DMDevolucion.RutAbrirTablasNuevo;//abre sqlDevolNuevo
          DMDevolucion.RutNuevaCabe(1);
          FormDevolNuevo.RutInicioForm(False,0);
          FormDevolNuevo.ShowModal();

      end;

      if vg_Tipo = 3 then
      begin
         DMMenuArti.RutNuevoArti;
         DMppal.swArtiFicha := 'FormDevolFicha';
         //FormManteArti.vStringOrigen := 'fichaDevol';

      end;

      if vg_Tipo = 4 then
      begin
         FormDevolFicha.RutAnularLinea;

      end;
    end;
    if vg_form = 'FORMLISTAARTI' then
    begin
      if vg_Tipo = 1 then
      begin
        DMMenuArti.sqlListaArticulo.EnableControls;
      end;
    end;

    if vg_form = 'FORMMANTEARTI' then
    begin
      if vg_Tipo = 1 then
      begin
        DMMenuArti.sqlFichaArticulo.Delete;
        DMMenuArti.sqlListaArticulo.Refresh;
        FormMenuArti.RutAbrirListaArti;

      end;
    end;

    if vg_Form = 'FORMDISTRIBUIDORAFICHA' then
    begin
      if vg_Tipo = 1 then
      begin
        DMDistribuidora.RutBorrarDistribuidora;
        FormDistribuidoraMenu.RutAbrirListaDistribuidora;

      end;
    end;

    if vg_Form = 'FORMNUEVOUSUARIO' then
    begin
      if vg_Tipo = 4 then
      begin
        FormNuevoUsuario.RutBorrarUsuario;

      end;

      if vg_Tipo = 5 then
      begin
        FormNuevoUsuario.RutSetFocus;
      end;
    end;

    if vg_Form = 'FORMBOTONES' then
    begin
      if vg_Tipo = 2 then
      begin
        DMDevolucion.RutBorrarDevolucion;
        if DMDevolucion.sqlCabe1.RecordCount = 0 then FormMenu.btAtrasClick(nil);
        FormBotones.btCerrarMenuClick(nil);


      end;


    end;

    self.Close;

  end else
  begin

  end;

  self.Close;
end;




end.

