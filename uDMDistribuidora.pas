unit uDMDistribuidora;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMDistribuidora = class(TDataModule)
    sqlCabe: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    sqlPoblacionS: TFDQuery;
    sqlCabeS: TFDQuery;
    sqlUpdate: TFDQuery;
    sqlCabeSID_PROVEEDOR: TIntegerField;
    sqlCabeSNOMBRE: TStringField;
    sqlCabeSDIRECCION: TStringField;
    sqlCabeSPOBLACION: TStringField;
    sqlCabeSPROVINCIA: TStringField;
    sqlCabeSCPOSTAL: TStringField;
    sqlCabeSCPROVINCIA: TStringField;
    sqlCabeSNIF: TStringField;
    sqlCabeSTELEFONO1: TStringField;
    sqlCabeSTELEFONO2: TStringField;
    sqlCabeSMOVIL: TStringField;
    sqlCabeSFAX: TStringField;
    sqlCabeSTPROVEEDOR: TSmallintField;
    sqlCabeID_PROVEEDOR: TIntegerField;
    sqlCabeTPROVEEDOR: TSmallintField;
    sqlCabeNOMBRE: TStringField;
    sqlCabeNIF: TStringField;
    sqlCabeID_DIRECCION: TIntegerField;
    sqlCabeNOMBRE2: TStringField;
    sqlCabeDIRECCION: TStringField;
    sqlCabeDIRECCION2: TStringField;
    sqlCabePOBLACION: TStringField;
    sqlCabePROVINCIA: TStringField;
    sqlCabeCPOSTAL: TStringField;
    sqlCabeCPAIS: TStringField;
    sqlCabeCPROVINCIA: TStringField;
    sqlCabeCONTACTO1NOMBRE: TStringField;
    sqlCabeCONTACTO1CARGO: TStringField;
    sqlCabeCONTACTO2NOMBRE: TStringField;
    sqlCabeCONTACTO2CARGO: TStringField;
    sqlCabeTELEFONO1: TStringField;
    sqlCabeTELEFONO2: TStringField;
    sqlCabeMOVIL: TStringField;
    sqlCabeFAX: TStringField;
    sqlCabeEMAIL: TStringField;
    sqlCabeWEB: TStringField;
    sqlCabeMENSAJEAVISO: TStringField;
    sqlCabeOBSERVACIONES: TMemoField;
    sqlCabeBANCO: TStringField;
    sqlCabeAGENCIA: TStringField;
    sqlCabeDC: TStringField;
    sqlCabeCUENTABANCO: TStringField;
    sqlCabeTEFECTO: TSmallintField;
    sqlCabeEFECTOS: TSmallintField;
    sqlCabeFRECUENCIA: TSmallintField;
    sqlCabeDIASPRIMERVTO: TSmallintField;
    sqlCabeDIAFIJO1: TSmallintField;
    sqlCabeDIAFIJO2: TSmallintField;
    sqlCabeDIAFIJO3: TSmallintField;
    sqlCabeTDTO: TIntegerField;
    sqlCabeTPAGO: TIntegerField;
    sqlCabeSWTFACTURACION: TSmallintField;
    sqlCabeSWBLOQUEO: TSmallintField;
    sqlCabeREFEPROVEEDOR: TStringField;
    sqlCabeTRANSPORTE: TSmallintField;
    sqlCabeRUTA: TStringField;
    sqlCabePROVEEDORHOSTING: TIntegerField;
    sqlCabeNOMBREHOSTING: TStringField;
    sqlCabeULTIMAFECHADESCARGA: TDateField;
    sqlCabeULTIMAFECHAENVIO: TDateField;
    sqlCabeCOLUMNASEXCEL: TStringField;
    sqlCabeDESDEFILA: TIntegerField;
    sqlCabeSWRECIBIR: TSmallintField;
    sqlCabeSWENVIAR: TSmallintField;
    sqlCabeSWALTABAJA: TSmallintField;
    sqlCabeFECHABAJA: TDateField;
    sqlCabeFECHAALTA: TDateField;
    sqlCabeHORAALTA: TTimeField;
    sqlCabeFECHAULTI: TDateField;
    sqlCabeHORAULTI: TTimeField;
    sqlCabeUSUULTI: TStringField;
    sqlCabeNOTAULTI: TStringField;
    sqlCabeMAXPAQUETE: TIntegerField;
    SP_GEN_PROVEEDOR: TFDStoredProc;
    sqlPoblacionSCODIGO: TIntegerField;
    sqlPoblacionSPOSTAL: TIntegerField;
    sqlPoblacionSPOBLACION: TStringField;
    sqlPoblacionSCPROV: TIntegerField;
    sqlPoblacionSPROVINCIA: TStringField;
    sqlPoblacionSCPOSTAL: TStringField;
    sqlPoblacionSCPAIS: TStringField;
    procedure sqlCabeBeforeEdit(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);

  private



    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    procedure RutAbrirTablas;
    procedure RutAbrirCabeS(vIDCabe, swTipo: Integer; vLike : String);
    procedure RutAbrirCabe(vIDCabe, swTipo: Integer);
    procedure RutBorrarDistribuidora;

    procedure RutNuevaDisitrbuidora;

    Function RutComprobarRegistros(vID : Integer) : Boolean;

    end;

function DMDistribuidora: TDMDistribuidora;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uDistribuidoraFicha;

function DMDistribuidora: TDMDistribuidora;
begin
  Result := TDMDistribuidora(DMppal.GetModuleInstance(TDMDistribuidora));
end;

procedure TDMDistribuidora.DataModuleCreate(Sender: TObject);
begin
  DMppal.RutCrearSortingSQL(sqlCabeS);
end;

procedure TDMDistribuidora.RutAbrirTablas;
begin
  RutAbrirCabeS(0,-1,'');

  sqlPoblacionS.Close;
  sqlPoblacionS.Open();
end;
procedure TDMDistribuidora.RutAbrirCabeS(vIDCabe, swTipo : Integer; vLike : String);
var
  vWhere, vFiltroTipo, vFiltroLike : string;
begin
  vWhere := ' is not null ';
  if vIdCabe <> 0 then vWhere := ' = ' + IntToStr(vIDCabe);

  vFiltroTipo := ' > ' + IntToStr(swTipo) + ')';
 // if (swTipo = 1) or (swTipo = 3) then vFiltroTipo := ' = ' + IntToStr(swTipo);
  if swTipo = 0 Then vFiltroTipo := ' = 0 or TPROVEEDOR = 1) ';
  if swTipo = 1 Then vFiltroTipo := ' = 1 ) ';
  if swTipo = 2 Then vFiltroTipo := ' = 1 or TPROVEEDOR = 2) ';
  if swTipo = 3 Then vFiltroTipo := ' = 3 ) ';

  vFiltroLike := '';
  if vLike <> '' then
    vFiltroLike := ' AND UPPER(NOMBRE) LIKE ' + QuotedStr('%'+UpperCase(vLike)+'%');
                 //+ ' OR (NOMBRE LIKE ' + QuotedStr('%'+UpperCase(vLike)+'%') + ')';

   with sqlCabeS do
  begin
    Close;
    sqlCabeS.SQL.Text :=  Copy(sqlCabeS.SQL.Text,1,
                      pos('WHERE ID_PROVEEDOR', Uppercase(SQL.Text))-1)
                  + 'WHERE ID_PROVEEDOR ' + vWhere
                  + ' AND (TPROVEEDOR '   + vFiltroTipo
                  +  vFiltroLike
                  + ' Order by NOMBRE ' ;
    Open;
    vWhere := SQL.Text;
  end;

end;

procedure TDMDistribuidora.RutAbrirCabe(vIDCabe, swTipo : Integer);
var
  vWhere, vFiltroTipo : string;
begin
  vWhere := ' is not null ';
  if vIdCabe <> 0 then vWhere := ' = ' + IntToStr(vIDCabe);

  vFiltroTipo := ' > ' + IntToStr(swTipo) + ')';
 // if (swTipo = 1) or (swTipo = 3) then vFiltroTipo := ' = ' + IntToStr(swTipo);

  if swTipo = 0 Then vFiltroTipo := ' = 0 or TPROVEEDOR = 1) ';
  if swTipo = 1 Then vFiltroTipo := ' = 1 ) ';
  if swTipo = 2 Then vFiltroTipo := ' = 1 or TPROVEEDOR = 2) ';
  if swTipo = 3 Then vFiltroTipo := ' = 3 ) ';

   with sqlCabe do
  begin
    Close;
    SQL.Text :=  Copy(sqlCabe.SQL.Text,1,
                      pos('WHERE ID_PROVEEDOR', Uppercase(SQL.Text))-1)
                  + 'WHERE ID_PROVEEDOR ' + vWhere
                  + ' AND (TPROVEEDOR '   + vFiltroTipo
                  + ' Order by NOMBRE ' ;
    Open;
  end;

end;


procedure TDMDistribuidora.RutNuevaDisitrbuidora;
begin
  sqlCabe.Close;
  sqlCabe.Open();

  SP_GEN_PROVEEDOR.ExecProc;
  sqlCabe.Append;
  sqlCabeID_PROVEEDOR.AsInteger := SP_GEN_PROVEEDOR.Params[0].Value;
  sqlCabeTPROVEEDOR.AsInteger   := 1;

end;


procedure TDMDistribuidora.sqlCabeBeforeEdit(DataSet: TDataSet);
begin
  if FormDistribuidoraFicha.btGrabarDistribuidora.Visible = False then abort
//en programa antiguo tiene stb_Pie donde guarda 'Debe editar el registro' o 'En Edicion' uManProveDM linea 329

end;

procedure TDMDistribuidora.RutBorrarDistribuidora;
begin
  sqlCabe.Delete;
  sqlCabeS.Refresh;
end;

Function TDMDistribuidora.RutComprobarRegistros(vID : Integer) : Boolean;
var
  vSQL, vSQL2 : String;
begin
  vSQL := 'select count(a.id_articulo) as Total'
        + ' from fmarticulo a'
        + ' where a.editorial = ' + IntToStr(vID);

  vSQL2 := 'select count(h.id_hisarti) as Total'
        + ' from fvhisarti h'
        + ' where h.id_proveedor = ' + IntToStr(vID);

  Result := False;
  with sqlUpdate do
  begin
    Close;
    SQL.Text := vSQL;
    Open;
    if FieldByName('Total').AsInteger <> 0 then Result := True;
  end;

  with sqlUpdate do
  begin
    Close;
    SQL.Text := vSQL2;
    Open;
    if FieldByName('Total').AsInteger <> 0 then Result := True;
  end;

end;

initialization
  RegisterModuleClass(TDMDistribuidora);





end.


