object FormBotones: TFormBotones
  Left = 100
  Top = 100
  ClientHeight = 460
  ClientWidth = 703
  Caption = 'FormBotones'
  OnShow = UniFormShow
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  PixelsPerInch = 96
  TextHeight = 13
  object UniSplitter1: TUniSplitter
    Left = 0
    Top = 215
    Width = 0
    Height = 245
    Hint = ''
    Align = alLeft
    ParentColor = False
    Color = clBtnFace
  end
  object pnlBts2: TUniPanel
    Left = 0
    Top = 0
    Width = 703
    Height = 188
    Hint = ''
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    BorderStyle = ubsSolid
    Caption = ''
    object btArticulos: TUniSpeedButton
      Left = 3
      Top = 65
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 18
      TabOrder = 1
      OnClick = btArticulosClick
    end
    object btAgregarUsuario: TUniSpeedButton
      Left = 510
      Top = 127
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 22
      TabOrder = 2
      OnClick = btAgregarUsuarioClick
    end
    object btCerrarMenu: TUniSpeedButton
      Left = 510
      Top = 4
      Width = 60
      Height = 55
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 10
      TabOrder = 3
      OnClick = btCerrarMenuClick
    end
    object btRecepcion: TUniSpeedButton
      Left = 276
      Top = 4
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 17
      TabOrder = 4
      OnClick = btRecepcionClick
    end
    object btDevolucion: TUniSpeedButton
      Left = 276
      Top = 65
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 21
      TabOrder = 5
      OnClick = btDevolucionClick
    end
    object lbArticulos: TUniLabel
      Left = 69
      Top = 85
      Width = 62
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Articulos'
      ParentFont = False
      Font.Height = -16
      TabOrder = 6
      OnClick = btArticulosClick
    end
    object lbUsuarios: TUniLabel
      Left = 576
      Top = 147
      Width = 115
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Usuarios'
      ParentFont = False
      Font.Height = -16
      TabOrder = 7
      OnClick = btAgregarUsuarioClick
    end
    object lbDevolucion: TUniLabel
      Left = 342
      Top = 85
      Width = 93
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Devoluciones'
      ParentFont = False
      Font.Height = -16
      TabOrder = 8
      OnClick = btDevolucionClick
    end
    object lbRecepcion: TUniLabel
      Left = 342
      Top = 24
      Width = 86
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Recepciones'
      ParentFont = False
      Font.Height = -16
      TabOrder = 9
      OnClick = btRecepcionClick
    end
    object lbDistribuidores: TUniLabel
      Left = 69
      Top = 24
      Width = 98
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Distribuidoras'
      ParentFont = False
      Font.Height = -16
      TabOrder = 10
      OnClick = btDistribuidoresClick
    end
    object btDistribuidores: TUniSpeedButton
      Left = 3
      Top = 3
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 23
      TabOrder = 11
      OnClick = btDistribuidoresClick
    end
    object btConsArti: TUniSpeedButton
      Left = 3
      Top = 127
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 12
      TabOrder = 12
      OnClick = btConsArtiClick
    end
    object lbConsArti: TUniLabel
      Left = 69
      Top = 147
      Width = 190
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Consulta Articulos'
      ParentFont = False
      Font.Height = -16
      TabOrder = 13
      OnClick = btConsArtiClick
    end
    object btHistorico: TUniSpeedButton
      Left = 510
      Top = 65
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 33
      TabOrder = 14
      OnClick = btHistoricoClick
    end
    object lbHistorico: TUniLabel
      Left = 576
      Top = 86
      Width = 115
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      AutoSize = False
      Caption = 'Historico'
      ParentFont = False
      Font.Height = -16
      TabOrder = 15
      OnClick = btHistoricoClick
    end
    object btCuadre: TUniSpeedButton
      Left = 276
      Top = 127
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Caption = ''
      ParentColor = False
      Color = clWindow
      Images = DMppal.ImgListPrincipal
      ImageIndex = 3
      TabOrder = 16
      OnClick = btCuadreClick
    end
    object lbCuadre: TUniLabel
      Left = 342
      Top = 147
      Width = 139
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Cuadre'
      ParentFont = False
      Font.Height = -16
      TabOrder = 17
      OnClick = btCuadreClick
    end
    object UniLabel20: TUniLabel
      Left = 576
      Top = 24
      Width = 115
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      AutoSize = False
      Caption = 'Cerrar'
      ParentFont = False
      Font.Height = -16
      TabOrder = 18
      OnClick = btCerrarMenuClick
    end
  end
  object pcBts: TUniPageControl
    Left = 0
    Top = 215
    Width = 703
    Height = 245
    Hint = ''
    ActivePage = tabBtArticulos
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    object tabBtDevolucion: TUniTabSheet
      Hint = ''
      Caption = 'tabBtDevolucion'
      object btNuevo: TUniBitBtn
        Left = 3
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 0
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
        OnClick = btNuevoClick
      end
      object btBorrarCS: TUniBitBtn
        Tag = 99
        Left = 3
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
        OnClick = btBorrarCSClick
      end
      object lbCrearDevol: TUniLabel
        Left = 64
        Top = 15
        Width = 152
        Height = 29
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Crear una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 2
        OnClick = btNuevoClick
      end
      object lbBorrarDevol: TUniLabel
        Left = 64
        Top = 57
        Width = 158
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Borrar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 3
        OnClick = btBorrarCSClick
      end
      object btModificar: TUniBitBtn
        Left = 3
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        Images = DMppal.ImgListPrincipal
        ImageIndex = 4
        OnClick = btModificarClick
      end
      object lbModifcarDevol: TUniLabel
        Left = 64
        Top = 96
        Width = 178
        Height = 34
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Modificar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 5
        OnClick = btModificarClick
      end
      object btAbrirDevol: TUniBitBtn
        Left = 362
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 6
        Images = DMppal.ImgListPrincipal
        ImageIndex = 16
        OnClick = btAbrirDevolClick
      end
      object lbAbrirDevol: TUniLabel
        Left = 431
        Top = 14
        Width = 150
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Abrir una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 7
        OnClick = btAbrirDevolClick
      end
      object btCerrarDevol: TUniBitBtn
        Left = 362
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 8
        Images = DMppal.ImgListPrincipal
        ImageIndex = 15
        OnClick = btCerrarDevolClick
      end
      object lbCerrarDevol: TUniLabel
        Left = 431
        Top = 57
        Width = 158
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Cerrar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 9
        OnClick = btCerrarDevolClick
      end
      object btImprimir: TUniBitBtn
        Left = 362
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 10
        Images = DMppal.ImgListPrincipal
        ImageIndex = 8
        OnClick = btImprimirClick
      end
      object lbImprimir: TUniLabel
        Left = 431
        Top = 100
        Width = 123
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Imprimir Albaran'
        ParentFont = False
        Font.Height = -16
        TabOrder = 11
        OnClick = btImprimirClick
      end
      object btVerFichaArti: TUniBitBtn
        Left = 3
        Top = 131
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 12
        IconAlign = iaTop
        Images = DMppal.ImgListPrincipal
        ImageIndex = 13
        OnClick = btVerFichaArtiClick
      end
      object UniLabel21: TUniLabel
        Left = 64
        Top = 139
        Width = 178
        Height = 34
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Ver Ficha Articulo'
        ParentFont = False
        Font.Height = -16
        TabOrder = 13
        OnClick = btVerFichaArtiClick
      end
      object btVerUltimasCompras: TUniBitBtn
        Left = 362
        Top = 131
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 14
        Images = DMppal.ImgListPrincipal
        ImageIndex = 40
        OnClick = btVerUltimasComprasClick
      end
      object UniLabel22: TUniLabel
        Left = 431
        Top = 143
        Width = 169
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Ver Ultimas Compras'
        ParentFont = False
        Font.Height = -16
        TabOrder = 15
        OnClick = btVerUltimasComprasClick
      end
      object btVerPaquetes: TUniBitBtn
        Left = 3
        Top = 174
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 16
        IconAlign = iaTop
        Images = DMppal.ImgListPrincipal
        ImageIndex = 2
        OnClick = btVerPaquetesClick
      end
      object UniLabel23: TUniLabel
        Left = 64
        Top = 182
        Width = 238
        Height = 34
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Ver Paquetes de la Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 17
        OnClick = btVerPaquetesClick
      end
      object btModificarDescripcionCodi: TUniBitBtn
        Left = 362
        Top = 174
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 18
        Images = DMppal.ImgListPrincipal
        ImageIndex = 3
        OnClick = btModificarDescripcionCodiClick
      end
      object UniLabel24: TUniLabel
        Left = 431
        Top = 186
        Width = 247
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Modificar Descripcion i Cod.Distri'
        ParentFont = False
        Font.Height = -16
        TabOrder = 19
        OnClick = btModificarDescripcionCodiClick
      end
    end
    object tabBtRecepcion: TUniTabSheet
      Hint = ''
      Caption = 'tabBtRecepcion'
    end
    object tabListaAbiertos: TUniTabSheet
      Hint = ''
      Caption = 'tabListaAbiertos'
      object UniLabel4: TUniLabel
        Left = 3
        Top = 3
        Width = 94
        Height = 13
        Hint = ''
        Caption = 'Programas Abiertos'
        TabOrder = 0
      end
      object btCerrarDevolFicha: TUniButton
        Left = 94
        Top = 63
        Width = 55
        Height = 42
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 1
        ImageIndex = 11
        IconAlign = iaBottom
        OnClick = btCerrarDevolFichaClick
      end
      object btIrDevolFicha: TUniButton
        Left = 94
        Top = 22
        Width = 55
        Height = 42
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 2
        ImageIndex = 5
        IconAlign = iaTop
        OnClick = btIrDevolFichaClick
      end
      object lbDevolFichaCerrar: TUniLabel
        Left = 2
        Top = 63
        Width = 86
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'Cerrar Devoluci'#243'n'
        TabOrder = 3
      end
      object lbIrDevolFicha: TUniLabel
        Left = 25
        Top = 22
        Width = 63
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'Ir Devoluci'#243'n'
        TabOrder = 4
      end
      object btIrRecepcionFicha: TUniButton
        Left = 247
        Top = 22
        Width = 55
        Height = 42
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 5
        ImageIndex = 5
        IconAlign = iaTop
      end
      object lbIrRecepcionFichar: TUniLabel
        Left = 181
        Top = 22
        Width = 60
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'Ir Recepcion'
        TabOrder = 6
      end
      object lbRecepcionFichaCerrar: TUniLabel
        Left = 158
        Top = 63
        Width = 83
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'Cerrar Recepcion'
        TabOrder = 7
      end
      object btCerrarRecepcionFicha: TUniButton
        Left = 247
        Top = 63
        Width = 55
        Height = 42
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 8
        ImageIndex = 11
        IconAlign = iaBottom
      end
      object btCerrarAPP: TUniButton
        Left = 623
        Top = 30
        Width = 55
        Height = 42
        Hint = ''
        Caption = ''
        TabOrder = 9
        ImageIndex = 11
        IconAlign = iaTop
        OnClick = btCerrarAPPClick
      end
      object btSalir: TUniLabel
        Left = 597
        Top = 42
        Width = 20
        Height = 13
        Hint = ''
        Caption = 'Salir'
        TabOrder = 10
      end
    end
    object tabPruebasCreacionBTsEjecucion: TUniTabSheet
      Hint = ''
      TabVisible = False
      Caption = 'tabPruebasCreacionBTsEjecucion'
      object sbContainer: TUniScrollBox
        Left = 0
        Top = 0
        Width = 695
        Height = 217
        Hint = ''
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
    object tabBtClientes: TUniTabSheet
      Hint = ''
      Caption = 'tabBtClientes'
      object UniBitBtn1: TUniBitBtn
        Left = 11
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 0
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
      end
      object UniBitBtn2: TUniBitBtn
        Tag = 99
        Left = 11
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
      end
      object UniLabel1: TUniLabel
        Left = 91
        Top = 25
        Width = 91
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Crear Cliente'
        ParentFont = False
        Font.Height = -16
        TabOrder = 2
      end
      object UniLabel2: TUniLabel
        Left = 91
        Top = 68
        Width = 97
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Borrar Cliente'
        ParentFont = False
        Font.Height = -16
        TabOrder = 3
      end
      object UniBitBtn3: TUniBitBtn
        Left = 11
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        ImageIndex = 6
      end
      object UniLabel3: TUniLabel
        Left = 91
        Top = 111
        Width = 117
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Modificar Cliente'
        ParentFont = False
        Font.Height = -16
        TabOrder = 5
      end
      object UniBitBtn4: TUniBitBtn
        Left = 370
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 6
        ImageIndex = 14
      end
      object UniLabel5: TUniLabel
        Left = 450
        Top = 25
        Width = 150
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Abrir una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 7
      end
      object UniBitBtn5: TUniBitBtn
        Left = 370
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 8
        ImageIndex = 15
      end
      object UniLabel6: TUniLabel
        Left = 450
        Top = 68
        Width = 158
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Cerrar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 9
      end
      object UniBitBtn6: TUniBitBtn
        Left = 370
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 10
        ImageIndex = 10
      end
      object UniLabel7: TUniLabel
        Left = 450
        Top = 111
        Width = 123
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Imprimir Albaran'
        ParentFont = False
        Font.Height = -16
        TabOrder = 11
      end
    end
    object tabBtArticulos: TUniTabSheet
      Hint = ''
      Caption = 'tabBtArticulos'
      object UniBitBtn7: TUniBitBtn
        Left = 19
        Top = 0
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 0
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
      end
      object UniBitBtn8: TUniBitBtn
        Tag = 99
        Left = 19
        Top = 43
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
      end
      object UniLabel8: TUniLabel
        Left = 99
        Top = 23
        Width = 98
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Crear Articulo'
        ParentFont = False
        Font.Height = -16
        TabOrder = 2
      end
      object UniLabel9: TUniLabel
        Left = 99
        Top = 66
        Width = 104
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Borrar Articulo'
        ParentFont = False
        Font.Height = -16
        TabOrder = 3
      end
      object UniBitBtn9: TUniBitBtn
        Left = 19
        Top = 86
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        ImageIndex = 6
      end
      object UniLabel10: TUniLabel
        Left = 99
        Top = 109
        Width = 124
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Caption = 'Modificar Articulo'
        ParentFont = False
        Font.Height = -16
        TabOrder = 5
      end
      object UniBitBtn10: TUniBitBtn
        Left = 378
        Top = 0
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 6
        ImageIndex = 14
      end
      object UniLabel11: TUniLabel
        Left = 458
        Top = 23
        Width = 150
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Abrir una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 7
      end
      object UniBitBtn11: TUniBitBtn
        Left = 378
        Top = 43
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 8
        ImageIndex = 15
      end
      object UniLabel12: TUniLabel
        Left = 458
        Top = 66
        Width = 158
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Cerrar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 9
      end
      object UniBitBtn12: TUniBitBtn
        Left = 378
        Top = 86
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 10
        ImageIndex = 10
      end
      object UniLabel13: TUniLabel
        Left = 458
        Top = 109
        Width = 123
        Height = 19
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        Caption = 'Imprimir Albaran'
        ParentFont = False
        Font.Height = -16
        TabOrder = 11
      end
    end
    object tabBtDistribuidores: TUniTabSheet
      Hint = ''
      Caption = 'tabBtDistribuidores'
      object btNuevaDistribuidora: TUniBitBtn
        Left = 3
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 0
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
        OnClick = btNuevaDistribuidoraClick
      end
      object btBorrarDistribuidora: TUniBitBtn
        Tag = 99
        Left = 3
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
        OnClick = btBorrarDistribuidoraClick
      end
      object UniLabel14: TUniLabel
        Left = 66
        Top = 14
        Width = 175
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Crear una Distribuidora'
        ParentFont = False
        Font.Height = -16
        TabOrder = 2
        OnClick = btNuevaDistribuidoraClick
      end
      object UniLabel15: TUniLabel
        Left = 66
        Top = 57
        Width = 191
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Borrar una Distribuidora'
        ParentFont = False
        Font.Height = -16
        TabOrder = 3
        OnClick = btBorrarDistribuidoraClick
      end
      object btModificarDistribuidora: TUniBitBtn
        Left = 3
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = 'Cerrar'
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        Images = DMppal.ImgListPrincipal
        ImageIndex = 4
        OnClick = btModificarDistribuidoraClick
      end
      object UniLabel16: TUniLabel
        Left = 64
        Top = 100
        Width = 191
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        AutoSize = False
        Caption = 'Modificar una Distribuidora'
        ParentFont = False
        Font.Height = -16
        TabOrder = 5
        OnClick = btModificarDistribuidoraClick
      end
      object UniBitBtn16: TUniBitBtn
        Left = 362
        Top = 2
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 6
        ImageIndex = 14
      end
      object UniLabel17: TUniLabel
        Left = 423
        Top = 14
        Width = 150
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        AutoSize = False
        Caption = 'Abrir una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 7
      end
      object UniBitBtn17: TUniBitBtn
        Left = 362
        Top = 45
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 8
        ImageIndex = 15
      end
      object UniLabel18: TUniLabel
        Left = 423
        Top = 57
        Width = 158
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        AutoSize = False
        Caption = 'Cerrar una Devolucion'
        ParentFont = False
        Font.Height = -16
        TabOrder = 9
      end
      object UniBitBtn18: TUniBitBtn
        Left = 362
        Top = 88
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        ShowHint = True
        ParentShowHint = False
        Caption = ''
        TabOrder = 10
        ImageIndex = 10
      end
      object UniLabel19: TUniLabel
        Left = 423
        Top = 100
        Width = 123
        Height = 30
        Cursor = crHandPoint
        Hint = ''
        Visible = False
        AutoSize = False
        Caption = 'Imprimir Albaran'
        ParentFont = False
        Font.Height = -16
        TabOrder = 11
      end
    end
  end
  object pnlMensaje: TUniPanel
    Left = 0
    Top = 188
    Width = 703
    Height = 27
    Hint = ''
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    Caption = ''
    object lbMensaje: TUniLabel
      Left = 6
      Top = 3
      Width = 70
      Height = 19
      Hint = ''
      Visible = False
      Caption = 'lbMensaje'
      ParentFont = False
      Font.Height = -16
      ParentColor = False
      Color = clDefault
      TabOrder = 1
    end
  end
  object UniHiddenPanel1: TUniHiddenPanel
    Left = 667
    Top = 241
    Width = 703
    Height = 229
    Hint = ''
    Visible = True
    object UniSpeedButton2: TUniSpeedButton
      Left = 6
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6010000424DF60100000000000076000000280000001C000000180000000100
        0400000000008001000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0088FFEEEEE0EE
        E0990EEE07EEEEE800008888FFFFFEEEE0000EEEE00007780000888888888FEE
        077780EEEE0788880000888888888F000000780EE07888880000888888888F33
        3333078007888888000088888888F3373333307078888888000088888888F300
        0333330078888888000088888888F337333333307888888800008888888F3300
        333333307888888800008888888F303337773333078888880000888888F00073
        0000733300788888000088888F0444030444073330788888000088888F044403
        0444077730078888000088888F00000000000000700788880000888888FF3333
        333330000007888800008888888F3333333330000007888800008888888F3333
        333300000007888800008888888FF333333000000008888800008888888F0000
        000080000078888800008888888FF0000000080007888888000088888888FF00
        0000008F788888880000888888888FF0000000F788888888000088888888888F
        FFFFFF8888888888000088888888888888888888888888880000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 1
    end
    object UniSpeedButton3: TUniSpeedButton
      Left = 59
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888848880888880888884488088000888884444880088
        0888844444488080008844444488008888084484488000088808488488800088
        8880488888800088800848888880000088088488888000000080888888880000
        0000888888888000008888888888888888888888888888888888}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 2
    end
    object UniSpeedButton4: TUniSpeedButton
      Left = 112
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFF00000000000000FF000FFFFFFFF000FF0000FFFFFF0000FF0F000FFFF00
        0F0FF0FF000FF000FF0FF0FFF000000FFF0FF0FFFF0000FFFF0FF0FFFF0000FF
        FF0FF0FFF000000FFF0FF0FF000FF000FF0FF0F000FFFF000F0FF0000FFFFFF0
        000FF000FFFFFFFF000FF00000000000000FFFFFFFFFFFFFFFFF}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 3
    end
    object UniSpeedButton5: TUniSpeedButton
      Left = 165
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888800000000000088880FFF0BB0FFF088880FFF0BB0FFF088880FFF0BB0FF
        F088880FFF0BB0FFF088880FFF0000FFF088880FFFFFFFFFF08880B0FFFFFFFF
        0B08880B0FFFFFF0B0888880B0FFFF0B088888880B0FF0B00888888880B00B01
        08888888880BB081088888888880088108888888888888888888}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 4
    end
    object UniSpeedButton9: TUniSpeedButton
      Left = 377
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555000050000005555544F00BFBFBF0555544F0BFBF0000055544F0FBFBFBFB
        F05544F0BFBF0000000544F0F000FBFBF00544F0B0B000000F000000F0F000FB
        FB0F555500BFBFBFB0F455555500FBFB0F44555555550000F44455555555550F
        4444555555555550044455555555555550045555555555555550}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 5
    end
    object UniSpeedButton10: TUniSpeedButton
      Left = 536
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888000000000000008808887FF8FF8FF088088877F87F87F08807777788788
        78088088870087F87F088088870E0FF87F0880777780E0888808800000FF0E08
        FF088088880F80E0FF08088888808F0E0F080F8555808FF0E0080F8588804444
        0E080F858880000000E080FFF808888888008800008888888888}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 6
    end
    object UniSpeedButton11: TUniSpeedButton
      Left = 589
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        36010000424D3601000000000000760000002800000011000000100000000100
        040000000000C000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333300000000
        00003000000033330FFFFFFFFFF03000000033330F000F0F00F0300000003333
        0FFFFFFFFFF03000000033330F00F00F00F03000000033330FFFFFFFFFF03000
        0000333300FFFF00F0F0300000003330FB00F0F0FFF030000000330FB0BF0FB0
        F0F03000000030FBFBF0FB0FFFF03000000030BFBF0FB0FFFFF03000000000FB
        FBFB0FFF000070000000E0BFBFB000FF0FF070000000EE0BFB0BFB0F0F037000
        0000EEE0BFBF00FF003370000000EEEE00000000033370000000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 7
    end
    object UniSpeedButton12: TUniSpeedButton
      Left = 640
      Top = 4
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888888888888000888888888888033008888888888030B30888888888030B
        0B088888880030B0B000880000330B0B07F080333300B0B0FFF0038B8B3B0B07
        F7F008BB3B83B0FFFFF00BB3B3BB00F7F7F077303B3B80FFFFF0080803B3B0F7
        F7F00B703B3B80CCCCC080BB7BB8000000008800700088888888}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 8
    end
    object UniSpeedButton15: TUniSpeedButton
      Left = 112
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF007F7F7F00FF00FF00FF00FF00FF00FF007F7F7F00FF00FF00FF00FF00FF00
        FF007F7F7F00FF00FF00FF00FF00FF00FF007F7F7F00FF00FF00FF00FF00FF00
        FF007F7F7F00FF00FF007F7F7F00FF00FF007F7F7F00FF00FF007F7F7F00FF00
        FF007F7F7F00FF00FF007F7F7F00FF00FF007F7F7F00FF00FF007F7F7F007F7F
        7F00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000FF00FF00FF00
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007F7F
        7F00000000000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF000000FF00FF00FF00FF00
        FF0000000000FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF007F7F7F007F7F
        7F00000000007F7F7F007F7F7F007F7F7F000000FF007F7F7F007F7F7F007F7F
        7F007F7F7F007F7F7F000000FF007F7F7F007F7F7F007F7F7F00FF00FF00FF00
        FF0000000000FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF007F7F
        7F0000000000FF00FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00
        FF00FF00FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF000000
        FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007F7F7F007F7F
        7F00000000007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F
        7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F00FF00FF00FF00
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007F7F
        7F0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007F7F7F007F7F
        7F00000000007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F
        7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F007F7F7F00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 9
    end
    object UniSpeedButton16: TUniSpeedButton
      Left = 165
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        EE000000424DEE000000000000007600000028000000100000000F0000000100
        04000000000078000000120B0000120B00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000000FFFFF
        FFF00FF7FF0F0000FFF009F79F0FFFFFFFF00777770F000000F00FF7FF0FFFFF
        FFF009F79F0F000000F00777770FFFFFFFF00FF7FF0F000FFFF009F79F0FFFFF
        00000777770F00FF0F000FFFFF0FFFFF00F00FFFFF00000009F0000000000000
        0000CCCCCC8888CCCCCCCCCCCCCCCCCCCCCC}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 10
    end
    object UniSpeedButton17: TUniSpeedButton
      Left = 218
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00999999999999
        99999C4F0F44400004499CC0F040088880099CC0F008888000099CCF00888007
        88099CCC0888077780499CC0FF80777880499CC0FF00877804499C0FF077F870
        44499C0FF0777F8444499C0F077770F8F4499C0F07770CC084499C007700CCF8
        F4499CC000CCCCCCCC499CCCCCCCCCCCCCC99999999999999999}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 11
    end
    object UniSpeedButton18: TUniSpeedButton
      Left = 271
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777700000007777777777777777700000007700000000000007700000007033
        3333333333307000000070FF3F3F3F3F3F307000000070F00000000000307000
        000070FF3F3F3F3F3F307000000070F00000000000307000000070FF3F3F3F3F
        3F70700000007700000000000007700000007777077777777777700000007777
        0707070707777000000077777070707070777000000077777777777777077000
        0000777777777777770770000000777777777777770770000000777777777777
        777770000000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 12
    end
    object UniSpeedButton19: TUniSpeedButton
      Left = 324
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        A6080000424DA608000000000000360000002800000024000000140000000100
        1800000000007008000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFCFDFFFFFFFFF9FAFAFCFDFDF3F3F3E9E8
        E7FFFFFFF4FAFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFBFFFFFFFF
        FF837F7F8B8285272A29000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF767F7F000000441717020000010000645C5C5B5D5E9F
        7C7CF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFFF6FBFF
        FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C7471000000140A0A4F262647
        23232613130502020E0707000000643E3E2B2121656969FFFFFFFFFFFFFFFFFF
        FFFFFFF5FFFFFFFFFFFDFEFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFFFF706B6902
        000F1909011C0E0E3B1C1C090505512828422020542929060101461E20381716
        0000002B03030000001C2424D7D6D6E7E5E5F1F2F2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF89888E360501001737151620170B0942201F2211114723233F1E20
        3F221B443230452F202B1215020000421E1E361B1B0E02023A0D0D0000002C2F
        2F989EA09B9B9BBFC5C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF8FEFFFFFFFF2F49481207131C0C0D002952001128180601
        4C2525412122441F1B421E1C40221A483433392A1B0000003E28284442424118
        184322224618182440400401010800000000000F1010636565716869A89A9B98
        9192FFFFFFFAFDFFFBFEFFFFFFFFFFFFFFFFFFFFFFFFFF133A603D0E01081120
        000D1D000204000000180A0A4A23244F201600203C2B1F3E3721240200004C25
        260B0504170D0D462B2B411F1F3F1919322323BEC1C11A00000000000000001F
        0A0A0000000000000000002C13130A1111B9D6D6FFFFFFFFFFFFFFFFFFFFFFFF
        414A41000000000000000000000000000000000000181E1E4B34353E1B11451F
        40341942574F68613F3B3E1A19160A0A150A0A4C2424321919432121160A0A00
        0000000000361B1B391C1C000000030202190C0C120808000000310E0E907D7E
        FFFFFFFFFFFFFFFFFF908E8E0000000000000000000000000000000000000000
        00181C1C4B33333F1C154320342920352F1C1C451E1D4322224D26263E1F1F2D
        16160000000000000000000000000000000000000000000000002C1515170B0B
        0402020904040500006C7373FFFFFFFFFFFFFFFFFF5C5C5C0000000000000000
        00000000000000000000000000171C1C4B32323F1C1C3F201D45221F351A190C
        05060F0707190C0C0A05050000002E1616000000000000000000000000000000
        0000000000000000000000002F1616110707000000777777FFFFFFFFFFFFFFFF
        FF5454540000000000000000000000000000000000000000003A2927442D2D3F
        1717421D1D3D1A1A0E01013111113A1818130606000000000000090202000000
        0000000000000000000000000000000000000000000000000501010000000000
        00777777FFFFFFFFFFFFFFFFFF55555500000000000000000000000000000008
        07045B29423F3F463F44414044443F42424041404458584156564448483E3434
        0F0808130B0B0209090505050503030000000000000000000000000000000000
        00000000000000000000000000767777FFFFFFFEFEFFFFFFFF55555500000000
        00000000010409131E0C09062345511B113F304240444140403D403838404D59
        4068653F61613E62624665655869697E6C6C2D615E5951516B4A4A2C1F1F3022
        22282E2E0D1717130E0E1404040000000000000000000000008D8180FFFFFFFE
        FEFFFFFFFF5454540000000000000000010A080E051021062040551D113E4545
        40413C40394840525740596440625F3F5F6039606068606072605E5F6058655E
        747E60598760608863638C62628287876874756C62656E3E3E454E4E4C404029
        2F2F0000009B8F90FFFFFFFFFFFFFFFFFF5A5356000000000000000000000000
        000000000D391E0B1243211D3A3E403B2F333B6060375E5D335B5B355B5C2F5D
        5D675E5A8C5F6A83587E8165807D6A767B787A7C79787D787876757673777471
        61466C5A61A5A5A4E6EDEFF5F6F6E4E4E4F0F1F1FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3DBD79E9C9C4F767854745D5854535051
        51737A7AA4757593716C7E6667535E5981536676777B8286818C95958F77778F
        8D8D848686D7D7D7FFFFFFFFFFFFFFFFFFFAFFFFFCFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAFDFFFCFEFFEDFAFFFEFEFFFFFFFFFFFFFFF7FDFFFDFEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAFFF1DAD8DCB7B5B5AAAFAEA4
        A6A5D7D3D4FBFEFFFFFFFFFFFFFFFFFFFFFBFEFFF3FBFFF9FCFFFBFEFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FDFFFEFFFFFBFDFFFFFEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFFFDFEFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 13
    end
    object UniSpeedButton20: TUniSpeedButton
      Left = 377
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777700007777777778888877777700007777777000008887777700007777
        77ECCCCC088777770000777777EC6EEC088777770000777777EC08EC08877777
        0000777777EC006C088777770000777777ECCCCC088777770000777777ECCCCC
        088877770000777777ECCCCC08888777000077777CCCCCCCC088887700007777
        CCCCCCCCCC0888870000777CCCCCCCCCCCC08887000077ECCCCCCCCCCCCC0887
        000077ECCCC0877ECCCC0887000077ECCC088777ECCC0877000077ECCC088777
        ECCC07770000777ECC077777ECC0777700007777EE677777EE67777700007777
        77777777777777770000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 14
    end
    object UniSpeedButton21: TUniSpeedButton
      Left = 430
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        7E120000424D7E12000000000000360000002800000027000000270000000100
        18000000000048120000130B0000130B00000000000000000000EBEBEBD4D4D4
        D8D8D8D8D8D8D8D8D8D7D7D7D2D2D2CCCCCCC9C9C9C9C9C9C9C9C9C9C9C9C9C9
        CAC9C9CAC9C9CACAC9CACACACACACACACACACACACACACACACBCACACBCACACBCA
        CACBCBCACBCBCACBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCDCDCDD2D2D3D7D7D7
        D8D8D8D8D8D8D8D8D8D2D2D2F3F3F30000008686860000000000000000000000
        0000000000000030302D46454442444142434041424041414040413F41413F3F
        403E3F403D3F3F3D3E3F3D3D3E3C3D3D3B3D3D3B3C3D3B3C3C393B3C3A3B3C39
        3A3B393A3A37393A38393A383A3B382A2A280000000000000000000000000000
        00000000959595000000919191000000000000000000272725C1C3C0FFFFFDF6
        F8F6F2F3F1F2F3F1F2F3F2F2F3F1F2F3F0F1F2F1F1F1F0F1F2EFF0F1F0F0F1EF
        EFEFEEEEEEEDEDEEEDECECEBEBEBEBE9EAE8EAE9E6E7E8E5E6E6E5E4E5E2E3E4
        E1E2E3DFDFE0DDE2E2DFE8E9E5B5B7B3272726000000000000000000A3A3A300
        00009191910000000000005C5D5CFFFFFFFFFFFFF5F5F5F8F8F8FFFFFEFBFBF1
        F4F4E9EFF2E1EBEBDBE8E8D3E1E1CAE2E2C9DBDCC2D6D4BDD5D5BCD5D5BBD4D3
        BAD1D2B6D4D5BADADAC2D6D7C1DBDCC6DCDDCBE0E0D2E3E4D6E7E8DCEDEEE8E2
        E3E2DDDEDBE4E4E2F7F8F65B5B5A000000000000A3A3A3000000919191000000
        333333FFFFFFFFFFFFFDFDFDFFFFFFFEFEF08A898C44468D4443923837932B2C
        962221A01B1A9E1718A31412A40F0EA20E0CA50D0CA70D0BA30D0CA1100D9F13
        159E15159A1C1B9721228D2D2D893938843A3C816A6B77EAEAD6F4F4F4ECEDEC
        EEEEEDFFFFFF3F403F000000A3A3A30000008F8F8F000000D7D7D7FFFFFFFDFD
        FDFDFDFEFFFFFF6868920000C80000CD0000CA0000CC0000CC0000CC0000CC00
        00CB0000CB0000CB0000CB0000CB0000CB0000CC0000CC0000CC0000CD0000CC
        0000CD0000CC0000CD0000CE0000D1474594FFFFF6FFFFFFFCFCFCFFFFFFECEC
        EC000000A0A0A00000008A8A8A161616FFFFFFFDFDFDFDFDFDFFFFFFE2E3CB00
        00C40000CE0000CD0000CC0000CC0000CA0000CC0000CA0000CB0000CB0000CD
        0000CC0000CB0000CC0000CB0000CB0000C90000CC0000CB0000CC0000CB0000
        CA0000CB0000CC0000CAC0C1BDFFFFFFFDFDFDFDFDFDFFFFFF2B2B2B98989800
        00008383835A5A5AFFFFFFFDFDFDFDFDFDFFFFFF9291C30000CF0000D00000D0
        0E10CD3A3BCD0505CC0000CE0000CC5556D70000CA1E1ECC2A2BCC0000CF2A2C
        CC191CCB0E0FCC5051D80000CA0000D00000CD2727D15657D90000CA0000D000
        00CF726EBCFFFFFFFDFDFDFCFCFCFFFFFF7777778E8E8E000000808080747474
        FFFFFFFDFDFDFDFDFDFFFFFF7977BE0000D60000D60000D55858DAFFFFFF2625
        D30000D67377DEFFFFFFF0F3F3EDEAF1D7D8F40000C9D6D6F2FAF5F8FFFFF3FF
        FFFF6F6CDB0000D47979DEFFFFFFFFFFFEFFFFF70C0CD10000D84A48BAFFFFFE
        FDFCFDFCFCFCFFFFFF9292928B8B8B000000808080727272FFFFFFFDFDFDFDFC
        FDFFFFFF635DBC0000DC0000D90000D95454DCFFFFFF2221D60000D99999DFEF
        F1F61111C5F1EFF4D1D1F50000CED2D3F4D8D9F20000C1E9EBF4BDBBEC0000D1
        FFFFFA9495E30101C2FFFFFF7473E00000DC3532BEFFFFFBFDFDFDFCFCFCFFFF
        FF9090908B8B8B000000808080727272FFFFFFFDFDFDFCFCFDFFFFFF4043C400
        00E40000E20000E25554E0FFFFFF2222DA0000E0999AE5ECECF60000D7CCCDF0
        D4D4F60000D7D5D4F4CCCFF50000D3D8D7F4BDBEF00000D5FFFFFE5659DC0000
        DCB3B4E84E4FDD0000E51917C8FFFFF6FDFDFEFCFCFCFFFFFF9090908B8B8B00
        0000808080727272FFFFFFFDFDFDFCFDFDFFFFFB3233CB0000EA0000E70000E9
        5554E4FFFFFF2222E10000E79A9BE6EEEFF70000DED1D1F2D4D5F70000DDD5D4
        F5CFD3F50000DBDADAF5BEBFF10000E0FFFFFD8685E43536E83031E21616E100
        00EB090AD1FFFFF0FDFDFEFCFCFCFFFFFF9090908B8B8B000000808080727272
        FFFFFFFDFDFDFCFCFDFFFFF82E2AD00000F10000ED0000EE5655E6FFFFFF2424
        E60000EF9A9CE9EFEFF60000E4D1D2F2D4D5F80000E4D4D4F6CFD3F70000E2D9
        D9F7BFC0F10000E4FFFFF9FFFFFBFFFFF8FFFFFE7E80EB0000F00504DBFFFFEF
        FDFDFEFCFCFCFFFFFF9090908B8B8B000000808080727272FFFFFFFDFDFDFDFD
        FDFFFFFB302DD30000F70201F30000F55655EBFFFFFF2525EA0000F59B9CEBF0
        EFF70000EBD2D1F2D6D6F70000EBD4D4F7CFD2F80000EAD8D7F6C0C2F40000E8
        FFFFFB605EE40000E4FFFFFA7C7CED0000F70507DFFFFFF0FDFDFEFCFCFCFFFF
        FF9090908B8B8B000000808080727272FFFFFFFCFCFCFCFCFCFFFFFB383AD900
        00FC0907F90000FA5959EFFFFFFE2927EF0000FB9C9DEDF0F0F80000F2D3D3F5
        D7D6F90000F1D1D1F9DFE2F70808EEEDEDF7C1C1F40000F2FFFFF7969AF01314
        F0FFFFFC6B6BEB0000FC0D0CE4FFFFF0FCFCFDFCFCFCFFFFFF9090908B8B8B00
        0000808080727272FFFFFFFDFDFDFDFDFDFFFFFE4A48DD0000FF0B0BFD0000FE
        5C5CF2FFFFFD2D2DF30000FFACAEEEFFFFF70000F7E5E6F7EAEAF90000F6D0CF
        F6ECEEF7F1F2F0FFFFFC7575EA0000FE8788E8FFFFFBFFFFFAEEF0EF0C0FF005
        0EFF1D1FE5FFFFF4FDFDFEFDFDFDFFFFFF9090908B8B8B000000808080727272
        FFFFFFFEFEFEFEFEFEFFFFFF6567DB0000FF0B0AFB0000FF5355F1FFFFFE2322
        F10000FF1F1EEB2C2AE70506FB2827E92828E90000F8D5D5F9C9CCFA0302E841
        40EB0506F60D0DFD0000FA2C2CEA4545EB0B10F00A17FF0F20FF3738DDFFFFFA
        FEFEFEFDFDFDFFFFFF9090908B8B8B000000808080727272FFFFFFFDFDFDFDFD
        FDFFFFFF8683DE0000FF2124F57777F6A7A5F4FFFFFB8B8AF27C7EF61112FA0C
        0CFF1212FD0D0DFF0D0EFF0000F9D6D7FBD5D6F90000F90A0BFF1111FE1312FC
        1314FC0E11FF0B15FE1522FF1D31FC1D38FF4F4FDCFFFFFCFDFDFEFDFDFDFFFF
        FF9090908B8B8B0000007E7E7E727272FFFFFFFDFDFDFDFDFDFFFFFFA9A7E800
        00FF4243F3FFFFFAFFFFFCFFFFFBFFFFFDFFFFFB2122F51213FD1615FC1515FC
        1616FC0000F8E0DFF6DADBF70000F71616FD1616FE1619FD171FFC1A25FD1E2E
        FD253BFE2F4CFE2E4DFE7472DFFFFFFFFDFDFDFDFDFDFFFFFF9090908A8A8A00
        00009A9A9A6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFF7F9EE0000FA1918F82E2EEC
        2B29EE2A29EF2A29EF2E2DEE1717F91918FD1818FD1818FD1918FC1517FC2B2B
        EE2928F11515FD1A1BFC1A21FD1C25FD1F2DFC2539FD2C48FE395AFE6187FD2A
        36FCCCCEEAFFFFFFFFFFFFFFFFFFFFFFFF9191918989890000009F9F9F6B6B6B
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8B8AF10000FB0808FF0C0CFF0F10FE1111
        FE1212FE1717FD1818FC1818FC1818FC1818FC1818FC1616FF1818FF1B20FF1D
        25FD1F2CFE2438FE2B48FD3759FE4D6EFD6E94FE6178F95E58F0FFFFFBFFFFFF
        FFFFFFFFFFFFFFFFFF9191918A8A8A0000009E9E9E6B6B6BFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDBFBFF87777F85E5FF54A47F93B3BFC3737FD3231FC2D
        2DF92B2BFA292AF92A2AFB2B2DFC2B2BFB2B2DFA2E31F93238FB3740FC3F49FD
        4852FD585FFA6E78F3878AF6A9A6F7FFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF9191918A8A8A0000009E9E9E6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFEFFFFFFFFFFFEFFFFFAFFFFF9FFFFFD
        FFFFFEFFFFF9FFFFFFFFFFFBFFFFFAFFFFFBFFFFFDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9191918A8A8A00
        00009E9E9E6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF7373745F5F5FF8F8F7FFFFFFECECEB6767664444439696
        95FFFFFFFFFFFE5F5F5F5D5D5DB0B0B04B4B4BD3D3D2FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9191918A8A8A0000009E9E9E6C6C6C
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF4E4E4E363636F7F7F7FFFFFF5C5C5C3030305B5B5B1F1F1FBDBDBDECECEC21
        21214E4E4E3E3E3E272727C6C6C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF9191918A8A8A0000009E9E9E6C6C6CFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF565656404040F9
        F9F9FEFEFE3B3B3B7D7D7DFFFFFF373737898989F3F3F31E1E1ED0D0D0C6C6C6
        232323CACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF9191918989890000009E9E9E6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5A5A5A444444F9F9F9FBFBFB3F3F3F
        858585FFFFFF3E3E3E8A8A8AF4F4F4232323C1C1C1CECECE242424CBCBCBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF92929289898900
        00009E9E9E6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF646464464646FBFBFBFCFCFC424242868686FFFFFF4141
        418C8C8CF5F4F4272626C3C3C3CECECE282828CCCCCCFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9292928989890000009E9E9E6C6C6C
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF5555553D3D3DE9E9E9FEFEFE4747478A8A8AFFFFFF4545458E8E8EF5F5F52B
        2B2BC4C4C4D1D1D12D2D2DCECECEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF9292928989890000009C9C9C6C6C6CFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5444444434343AA
        AAAAFFFFFF4C4C4C8C8C8CFFFFFF484848929292F4F4F4303030C5C5C5D1D1D1
        313131CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF939393898989000000ADADAD6E6E6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8F5151515858586C6C6CFFFFFF636363
        5454548F8F8F393939BFBFBFF0F0F0333333C5C5C5D2D2D2343434CFCFCFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95959589898900
        0000A7A7A7575757FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF5959597C7C7CA2A2A2494949F6F6F6F0F0F06363634141419191
        91FFFFFFE9E9E93F3F3FCCCCCCD8D8D8404040D2D2D2FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7C7C7C8E8E8E000000AAAAAA141414
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDE4949
        49A1A1A1DDDDDD3E3E3EC4C4C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF2E2E2E989898000000868686272727D6D6D6FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2A2A23E3E3EE1E1E1F7F7F754
        5454828282FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0
        F0000000A0A0A00000008D8D8D070707363636FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF888888777777FAFAFAFFFFFF8B8B8B808080FCFCFC
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF505050000000A3A3A300
        0000919191000000000000696969FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF797979000000000000A3A3A3000000919191000000
        000000000000393939DBDBDBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5
        454545000000000000000000A3A3A30000008686860000000000000000000000
        0000000012121251515169696966666666666666666666666666666666666666
        6666666666666666666666666666666666666666666666666666666666666666
        6666666666666666666666666868685555551717170000000000000000000000
        00000000959595000000EBEBEBD4D4D4D8D8D8D8D8D8D8D8D8D6D6D6D0D0D0C7
        C7C7C3C3C3C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4
        C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4
        C4C4C4C4C4C4C4C6C6C6D0D0D0D6D6D6D8D8D8D8D8D8D8D8D8D2D2D2F3F3F300
        0000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 15
    end
    object UniSpeedButton22: TUniSpeedButton
      Left = 483
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000013000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333336633
        3333333000003333333644633333333000003333333E66433333333000003333
        3333E66333333330000033333333333333333330000033333333446333333330
        00003333333666433333333000003333333E66433333333000003333333E6664
        33333330000033333333E666433333300000333333333E666433333000003333
        344333E666433330000033336664333E6643333000003333E666444666433330
        000033333E666666666333300000333333EE666666333330000033333333EEEE
        E33333300000333333333333333333300000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 16
    end
    object UniSpeedButton24: TUniSpeedButton
      Left = 589
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00EEEEEEEEEEEE
        EEEEEEEE0000EEEEEE0000000000000E0000EEEEEE0788888888880E0000EEEE
        EE0F77777777780E0000EEEEEE0F99777777780E0000EEEEEE0FFFFFFFFFF70E
        0000EEEEEE0000000000000E0000EEEEEEEEEEEEEEEEEEEE0000EEEEEEEEEEEE
        EEE99EEE0000EEEEEEEEEEEEEE9999EE0000EEEEEEEEEEEEE999999E0000EEEE
        EEEEEEEEEEE99EEE0000EEEEEEEEEEEEEEE99EEE0000E0000000000000899EEE
        0000E0788888888880998EEE0000E0F7777777778098EEEE0000E0F997777777
        80EEEEEE0000E0FFFFFFFFFF70EEEEEE0000E0000000000000EEEEEE0000EEEE
        EEEEEEEEEEEEEEEE0000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 17
    end
    object UniSpeedButton13: TUniSpeedButton
      Left = 6
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00AAAAAAAAAAAA
        AAAAA0000000AAAAAAAAAAAAAAAAA0000000AAAAAAAAAAAAAAAAA0000000AA00
        00000000AAAAA0000000AA03333333030AA0A0000000AA033333330330AAA000
        0000AA0000000003330AA0000000AA03333333033330A0000000AA0333333303
        3330A0000000AA00000000033330A0000000AA03333333033330A0000000AA03
        333333033330A0000000AA00000000033330A0000000AA03333333303330A000
        0000AAA0333333330330A0000000AAAA000000000000A0000000AAAAAAAAAAAA
        AAAAA0000000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 18
      OnClick = UniSpeedButton13Click
    end
    object btMostrarBotones: TUniSpeedButton
      Left = 59
      Top = 44
      Width = 55
      Height = 42
      Hint = ''
      Glyph.Data = {
        16020000424D160200000000000076000000280000001A0000001A0000000100
        040000000000A001000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777700000077777777777777777777777777000000700000000000
        0000077777777700000070EFEF6FEFE6EFEF607777777700000070FEFE6EFEF6
        FEFE660777777700000070EFEF6FEFE6EFEF660077777700000070FEFE6EFEF6
        FEFE6606077777000000706666666660FFFF660600777700000070FEFE6EFEF6
        0EFEF60606077700000070EFEF6FEFE66000000606077700000070FEFE6EFEF6
        6066666006077700000070EFEF6FEFE660666F660607770000007066660FFFF6
        6066FE6600077700000070EFEF6066EF606FEF6606077700000070FEFE660000
        00FFFF6606077700000070EFEF660666660FEFE606000700000070FEFE660666
        F6600000060F6000000070FFFF66066FE660666660FEF6000000770EFEF606FE
        F660666E0FEFEF000000777066660FFFF66066EF60FEF600000077770EFEF6FE
        FE606EFE660F600000007777706666666660FFFF6600070000007777770EFEF6
        FEFE6EFEF6077700000077777770000000000000000777000000777777777777
        7777777777777700000077777777777777777777777777000000}
      Caption = ''
      ParentColor = False
      Color = clWindow
      TabOrder = 19
      OnClick = btMostrarBotonesClick
    end
    object btClientes: TUniSpeedButton
      Left = 309
      Top = 104
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      Caption = ''
      ParentColor = False
      Color = clWindow
      ImageIndex = 19
      TabOrder = 20
      OnClick = btClientesClick
    end
    object UniSpeedButton8: TUniSpeedButton
      Left = 3
      Top = 98
      Width = 60
      Height = 55
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      Caption = ''
      ParentColor = False
      Color = clWindow
      ImageIndex = 18
      TabOrder = 21
      OnClick = UniSpeedButton8Click
    end
    object lbClientes: TUniLabel
      Left = 375
      Top = 118
      Width = 55
      Height = 19
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      Caption = 'Clientes'
      ParentFont = False
      Font.Height = -16
      TabOrder = 22
      OnClick = btClientesClick
    end
    object lbTPV: TUniLabel
      Left = 69
      Top = 118
      Width = 29
      Height = 35
      Cursor = crHandPoint
      Hint = ''
      Visible = False
      AutoSize = False
      Caption = 'TPV'
      ParentFont = False
      Font.Height = -16
      TabOrder = 23
      OnClick = UniSpeedButton8Click
    end
  end
end
