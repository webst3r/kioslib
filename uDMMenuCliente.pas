unit uDMMenuCliente;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMMenuCliente = class(TDataModule)
    sqlCabe: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDConnection1: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    sqlCabeID_CLIENTE: TIntegerField;
    sqlCabeNOMBRE: TStringField;
    sqlCabeNIF: TStringField;
    sqlCabeID_DIRECCION: TIntegerField;
    sqlCabeNOMBRE2: TStringField;
    sqlCabeDIRECCION: TStringField;
    sqlCabeDIRECCION2: TStringField;
    sqlCabePOBLACION: TStringField;
    sqlCabeCPOSTAL: TStringField;
    sqlCabeCPROVINCIA: TStringField;
    sqlCabePROVINCIA: TStringField;
    sqlCabeCONTACTO1NOMBRE: TStringField;
    sqlCabeCONTACTO1CARGO: TStringField;
    sqlCabeCONTACTO2NOMBRE: TStringField;
    sqlCabeCONTACTO2CARGO: TStringField;
    sqlCabeTELEFONO1: TStringField;
    sqlCabeTELEFONO2: TStringField;
    sqlCabeMOVIL: TStringField;
    sqlCabeFAX: TStringField;
    sqlCabeEMAIL: TStringField;
    sqlCabeWEB: TStringField;
    sqlCabeMENSAJEAVISO: TStringField;
    sqlCabeOBSERVACIONES: TMemoField;
    sqlCabeBANCO: TStringField;
    sqlCabeAGENCIA: TStringField;
    sqlCabeDC: TStringField;
    sqlCabeCUENTABANCO: TStringField;
    sqlCabeTDTO: TIntegerField;
    sqlCabeTIVACLI: TSmallintField;
    sqlCabeTPAGO: TIntegerField;
    sqlCabeDIAFIJO1: TSmallintField;
    sqlCabeDIAFIJO2: TSmallintField;
    sqlCabeDIAFIJO3: TSmallintField;
    sqlCabeSWBLOQUEO: TSmallintField;
    sqlCabeSWTFACTURACION: TSmallintField;
    sqlCabeSWAGRUARTIFRA: TSmallintField;
    sqlCabeREFEPROVEEDOR: TStringField;
    sqlCabeTRANSPORTE: TSmallintField;
    sqlCabeSWALTABAJA: TSmallintField;
    sqlCabeFECHABAJA: TDateField;
    sqlCabeFECHAALTA: TDateField;
    sqlCabeHORAALTA: TTimeField;
    sqlCabeFECHAULTI: TDateField;
    sqlCabeHORAULTI: TTimeField;
    sqlCabeUSUULTI: TStringField;
    sqlCabeNOTAULTI: TStringField;
    sqlPoblacionS: TFDQuery;
    sqlTFacturacio: TFDQuery;
    FDQuery3: TFDQuery;
    FDQuery4: TFDQuery;
    sqlTPago: TFDQuery;
    FDQuery6: TFDQuery;
    FDQuery7: TFDQuery;
    sqlCabeS: TFDQuery;
    sqlPoblacionSCODIGO: TIntegerField;
    sqlPoblacionSPOSTAL: TIntegerField;
    sqlPoblacionSPOBLACION: TStringField;
    sqlPoblacionSCPROV: TIntegerField;
    sqlPoblacionSPROVINCIA: TStringField;
    sqlPoblacionSCPOSTAL: TStringField;
    sqlPoblacionSCPAIS: TStringField;
    sqlTFacturacioTTIPO: TStringField;
    sqlTFacturacioNORDEN: TIntegerField;
    sqlTFacturacioDESCRIPCION: TStringField;
    sqlCabeSID_CLIENTE: TIntegerField;
    sqlCabeSNOMBRE: TStringField;
    sqlCabeSDIRECCION: TStringField;
    sqlCabeSPOBLACION: TStringField;
    sqlCabeSPROVINCIA: TStringField;
    sqlCabeSCPOSTAL: TStringField;
    sqlCabeSCPROVINCIA: TStringField;
    sqlCabeSNIF: TStringField;
    sqlCabeSTELEFONO1: TStringField;
    sqlCabeSTELEFONO2: TStringField;
    sqlCabeSMOVIL: TStringField;
    sqlCabeSFAX: TStringField;
    sqlUpdate: TFDQuery;
    sqlTPagoID_TPAGO: TIntegerField;
    sqlTPagoDESCRIPCION: TStringField;
    sqlTPagoTEFECTO: TSmallintField;
    sqlTPagoEFECTOS: TSmallintField;
    sqlTPagoFRECUENCIA: TSmallintField;
    sqlTPagoDIASPRIMERVTO: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);

  private
    procedure RutAbrirTablas;

    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    end;

function DMMenuCliente: TDMMenuCliente;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal;

function DMMenuCliente: TDMMenuCliente;
begin
  Result := TDMMenuCliente(DMppal.GetModuleInstance(TDMMenuCliente));
end;


procedure TDMMenuCliente.DataModuleCreate(Sender: TObject);
begin
  RutAbrirTablas;
end;

procedure TDMMenuCliente.RutAbrirTablas;
begin
  sqlCabe.Close;
  sqlCabe.Open();

  sqlPoblacionS.Close;
  sqlPoblacionS.Open();

  sqlTFacturacio.Close;
  sqlTFacturacio.Open();

  sqlTPago.Close;
  sqlTPago.Open();
end;

initialization
  RegisterModuleClass(TDMMenuCliente);





end.


