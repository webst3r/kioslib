unit uUltiCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormUltiCompra = class(TUniForm)
    dsUltiCompra: TDataSource;
    ImgList: TUniNativeImageList;
    UniPanel1: TUniPanel;
    btCancelar: TUniButton;
    lbMensaje: TUniLabel;
    btOk: TUniButton;
    pnPieCompras: TUniPanel;
    GridUltiCompra: TUniDBGrid;
    procedure btCancelarClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);


  private
    procedure RutDevolucion;




    { Private declarations }

  public
    { Public declarations }

     end;

function FormUltiCompra: TFormUltiCompra;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule, uNuevoUsuario, uMenuBotones,
  uMenu, uDevolFicha, uMenuArti, uListaArti, uDistribuidoraMenu, uDMUltiCompra, uDMMenuArti, uDMDevol;

function FormUltiCompra: TFormUltiCompra;
begin
  Result := TFormUltiCompra(DMppal.GetFormInstance(TFormUltiCompra));

end;



procedure TFormUltiCompra.btCancelarClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFormUltiCompra.btOkClick(Sender: TObject);
begin
  RutDevolucion;
end;

procedure TFormUltiCompra.RutDevolucion;
begin
  if DMUltiCompra.sqlUltiCompra.RecordCount <> 0 then
  begin
  {
    FormDevolFicha.pBarrasA.Text := DMUltiCompra.sqlUltiCompraBARRAS.AsString + DMUltiCompra.sqlUltiCompraADENDUM.AsString;
    DMMenuArti.RutLeerIdArti(DMUltiCompra.sqlUltiCompraID_ARTICULO.AsInteger);
    DMDevolucion.swRepetido :=  true;
    FormDevolFicha.pBarrasAExit(nil);

                 }
    DMDevolucion.sqlLinea.Edit;
    DMDevolucion.sqlLineaADENDUM.AsString := DMUltiCompra.sqlUltiCompraADENDUM.AsString;
    DMDevolucion.sqlLinea.Post;

    self.Close;
  end;
end;

end.

