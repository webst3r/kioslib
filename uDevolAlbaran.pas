unit uDevolAlbaran;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox;

type
  TFormDevolucionAlbaran = class(TUniForm)
    ImgNativeList: TUniNativeImageList;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    UniBitBtn29: TUniBitBtn;
    btModificar: TUniBitBtn;
    UniBitBtn31: TUniBitBtn;
    UniBitBtn17: TUniBitBtn;
    UniBitBtn22: TUniBitBtn;
    UniBitBtn23: TUniBitBtn;
    UniBitBtn32: TUniBitBtn;
    btLista: TUniBitBtn;
    btOpciones: TUniBitBtn;
    btCerrarFicha: TUniBitBtn;
    dsLinea: TDataSource;
    dsCabe1: TDataSource;
    dsHistoAnteF: TDataSource;
    dsHistoAnteFT: TDataSource;
    dsHistoDevolver: TDataSource;
    dsHistoDevolverProve: TDataSource;
    dsCabeSCap: TDataSource;
    dsProveS: TDataSource;
    UniPanel1: TUniPanel;
    UniPageControl3: TUniPageControl;
    TabSheet6: TUniTabSheet;
    UniPanel5: TUniPanel;
    UniLabel9: TUniLabel;
    pBarrasA: TUniEdit;
    pAdendumEntrar: TUniDBEdit;
    UniLabel10: TUniLabel;
    pCantidad: TUniDBEdit;
    UniLabel11: TUniLabel;
    BtnSumarCanti: TUniBitBtn;
    BtnRestarCanti: TUniBitBtn;
    pGraDescripcion: TUniDBEdit;
    UniLabel59: TUniLabel;
    UniLabel39: TUniLabel;
    UniLabel40: TUniLabel;
    pPrecio: TUniDBEdit;
    pPreu: TUniDBEdit;
    UniDBEdit37: TUniDBEdit;
    pPreu2: TUniDBEdit;
    btMasInfo: TUniBitBtn;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    UniLabel37: TUniLabel;
    UniDBEdit30: TUniDBEdit;
    paStock: TUniDBEdit;
    pMerma: TUniDBEdit;
    UniDBEdit33: TUniDBEdit;
    UniDBEdit34: TUniDBEdit;
    UniDBEdit35: TUniDBEdit;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    pFeAbono: TUniDBDateTimePicker;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    pnlTotales: TUniPanel;
    pMargen1: TUniDBEdit;
    pMargen2: TUniDBEdit;
    pEncarte2: TUniDBEdit;
    pEncarte1: TUniDBEdit;
    UniLabel41: TUniLabel;
    UniLabel42: TUniLabel;
    pTIva1: TUniDBEdit;
    pTIva2: TUniDBEdit;
    UniDBEdit47: TUniDBEdit;
    UniDBEdit48: TUniDBEdit;
    UniDBEdit49: TUniDBEdit;
    UniDBEdit50: TUniDBEdit;
    UniLabel43: TUniLabel;
    UniLabel44: TUniLabel;
    UniLabel45: TUniLabel;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel46: TUniLabel;
    UniLabel47: TUniLabel;
    UniPanel17: TUniPanel;
    UniDBEdit55: TUniDBEdit;
    UniDBEdit56: TUniDBEdit;
    UniDBEdit57: TUniDBEdit;
    UniDBEdit58: TUniDBEdit;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniDBEdit59: TUniDBEdit;
    UniDBEdit60: TUniDBEdit;
    UniDBEdit61: TUniDBEdit;
    UniDBEdit62: TUniDBEdit;
    UniLabel50: TUniLabel;
    UniLabel51: TUniLabel;
    UniLabel52: TUniLabel;
    UniLabel53: TUniLabel;
    UniLabel54: TUniLabel;
    UniDBText5: TUniDBText;
    UniDBText6: TUniDBText;
    UniDBText7: TUniDBText;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniDBText8: TUniDBText;
    UniDBText9: TUniDBText;
    GridPreSeleccio: TUniDBGrid;
    dsCabeNuevo: TDataSource;
    pnlNavigator: TUniPanel;
    UniDBGrid1: TUniDBGrid;
    btNuevo: TUniBitBtn;
    btBorrarCS: TUniBitBtn;
    UniDBEdit1: TUniDBEdit;
    UniLabel3: TUniLabel;
    dsLineaTotal: TDataSource;
    UniPanel2: TUniPanel;
    procedure btListaClick(Sender: TObject);
    procedure btOpcionesClick(Sender: TObject);
    procedure btMasInfoClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure btNuevoClick(Sender: TObject);
    procedure btCerrarFichaClick(Sender: TObject);
    procedure pBarrasAExit(Sender: TObject);
    procedure GridPreSeleccioColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);


  private

    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swClicPnl : Boolean;

  end;

function FormDevolucionAlbaran: TFormDevolucionAlbaran;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDevolLista,  uDMDevol, uDevolNuevo, uDevolMenu;

function FormDevolucionAlbaran: TFormDevolucionAlbaran;
begin
  Result := TFormDevolucionAlbaran(DMppal.GetFormInstance(TFormDevolucionAlbaran));

end;

procedure TFormDevolucionAlbaran.btListaClick(Sender: TObject);
begin
  FormDevolucion.RutAbrirLista;
end;

procedure TFormDevolucionAlbaran.btNuevoClick(Sender: TObject);
begin
  DMDevolucion.RutAbrirTablasNuevo;
  DMDevolucion.RutNuevaCabe;
  FormDevolucionNuevo.swEdit := False;
  FormDevolucionNuevo.ShowModal();

end;

procedure TFormDevolucionAlbaran.btOpcionesClick(Sender: TObject);
begin
   UniPanel27.Visible := not unipanel27.visible;
end;

procedure TFormDevolucionAlbaran.GridPreSeleccioColumnSummary(
  Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'nomrep') then
  begin
    if Column.AuxValue = NULL then Column.AuxValue:='';
    Column.AuxValue    := 'Total';
  end else
  begin
    if Column.AuxValue = NULL then Column.AuxValue:=0.0;
    Column.AuxValue    := Column.AuxValue + Column.Field.AsFloat;
  end;
end;

procedure TFormDevolucionAlbaran.pBarrasAExit(Sender: TObject);
begin
  if pBarrasA.Text > '' then
  begin
    DMppal.RutLeerCodigoBarras(pBarrasA.Text, pAdendumEntrar.Text);
    DMDevolucion.sqlLinea.Refresh;
    DMDevolucion.sqlLineaTotal.Refresh;
  end else
  exit;

end;

procedure TFormDevolucionAlbaran.btCerrarFichaClick(Sender: TObject);
begin
//  if FormDevolucion.swDevoAlbaran = 1 then FormDevolucion.pcDetalle.ActivePage := FormDevolucion.tabAlbaranDevolucion;
  FormDevolucion.swDevoAlbaran := 0;

  self.Close;
end;

procedure TFormDevolucionAlbaran.UniFormCreate(Sender: TObject);
begin
  swClicPnl := True;
  pnlTotales.Height := 0;
end;

procedure TFormDevolucionAlbaran.btMasInfoClick(Sender: TObject);
var
  vHeight : integer;
begin
  vHeight := 60;
  if swClicPnl then
  begin
    pnlTotales.Height := vHeight;
    GridPreSeleccio.Height := GridPreSeleccio.Height - vHeight;
  end
  else
  begin
    pnlTotales.Height := 0;
    GridPreSeleccio.Height := GridPreSeleccio.Height + vHeight;
  end;
  swClicPnl := not swClicPnl;
end;



end.
