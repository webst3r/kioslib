unit uDMDevol;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Variants;

type
  TDMDevolucion = class(TDataModule)
    sqlCabeS2: TFDQuery;
    sqlCabeS2IDSTOCABE: TIntegerField;
    sqlCabeS2SWTIPODOCU: TSmallintField;
    sqlCabeS2NDOCSTOCK: TIntegerField;
    sqlCabeS2FECHA: TDateField;
    sqlCabeS2NALMACEN: TIntegerField;
    sqlCabeS2ID_PROVEEDOR: TIntegerField;
    sqlCabeS2DOCTOPROVE: TStringField;
    sqlCabeS2DOCTOPROVEFECHA: TDateField;
    sqlCabeS2FECHACARGO: TDateField;
    sqlCabeS2FECHAABONO: TDateField;
    sqlCabeS2NOMBRE: TStringField;
    sqlCabeS2NIF: TStringField;
    sqlCabeS2MENSAJEAVISO: TStringField;
    sqlCabeS2SWDEVOLUCION: TSmallintField;
    sqlCabeS2PAQUETE: TIntegerField;
    sqlCabeS2REFEPROVEEDOR: TStringField;
    sqlCabeS2RUTA: TStringField;
    sqlCabe1: TFDQuery;
    sqlLinea: TFDQuery;
    sqlLineaID_HISARTI: TIntegerField;
    sqlLineaSWES: TStringField;
    sqlLineaFECHA: TDateField;
    sqlLineaHORA: TTimeField;
    sqlLineaCLAVE: TStringField;
    sqlLineaID_ARTICULO: TIntegerField;
    sqlLineaADENDUM: TStringField;
    sqlLineaID_PROVEEDOR: TIntegerField;
    sqlLineaREFEPROVE: TStringField;
    sqlLineaIDSTOCABE: TIntegerField;
    sqlLineaIDDEVOLUCABE: TIntegerField;
    sqlLineaPAQUETE: TIntegerField;
    sqlLineaID_HISARTI01: TIntegerField;
    sqlLineaID_DIARIA: TIntegerField;
    sqlLineaPARTIDA: TIntegerField;
    sqlLineaNRECLAMACION: TIntegerField;
    sqlLineaID_FRAPROVE: TIntegerField;
    sqlLineaID_CLIENTE: TIntegerField;
    sqlLineaSWTIPOFRA: TIntegerField;
    sqlLineaNFACTURA: TIntegerField;
    sqlLineaNALBARAN: TIntegerField;
    sqlLineaNLINEA: TIntegerField;
    sqlLineaDEVUELTOS: TSingleField;
    sqlLineaVENDIDOS: TSingleField;
    sqlLineaMERMA: TSingleField;
    sqlLineaCARGO: TSingleField;
    sqlLineaABONO: TSingleField;
    sqlLineaRECLAMADO: TSingleField;
    sqlLineaCANTIDAD: TSingleField;
    sqlLineaCANTIENALBA: TSingleField;
    sqlLineaCANTISUSCRI: TSingleField;
    sqlLineaPRECIOCOMPRA: TSingleField;
    sqlLineaPRECIOCOMPRA2: TSingleField;
    sqlLineaPRECIOCOSTE: TSingleField;
    sqlLineaPRECIOCOSTE2: TSingleField;
    sqlLineaPRECIOCOSTETOT: TSingleField;
    sqlLineaPRECIOCOSTETOT2: TSingleField;
    sqlLineaPRECIOVENTA: TSingleField;
    sqlLineaPRECIOVENTA2: TSingleField;
    sqlLineaSWDTO: TSmallintField;
    sqlLineaTPCDTO: TSingleField;
    sqlLineaTIVA: TSmallintField;
    sqlLineaTPCIVA: TSingleField;
    sqlLineaTPCRE: TSingleField;
    sqlLineaTIVA2: TSmallintField;
    sqlLineaTPCIVA2: TSingleField;
    sqlLineaTPCRE2: TSingleField;
    sqlLineaIMPOBRUTO: TSingleField;
    sqlLineaIMPODTO: TSingleField;
    sqlLineaIMPOBASE: TSingleField;
    sqlLineaIMPOIVA: TSingleField;
    sqlLineaIMPORE: TSingleField;
    sqlLineaIMPOBASE2: TSingleField;
    sqlLineaIMPOIVA2: TSingleField;
    sqlLineaIMPORE2: TSingleField;
    sqlLineaIMPOTOTLIN: TSingleField;
    sqlLineaENTRADAS: TSingleField;
    sqlLineaSALIDAS: TSingleField;
    sqlLineaVALORCOSTE: TSingleField;
    sqlLineaVALORCOSTE2: TSingleField;
    sqlLineaVALORCOSTETOT: TSingleField;
    sqlLineaVALORCOSTETOT2: TSingleField;
    sqlLineaVALORCOMPRA: TSingleField;
    sqlLineaVALORCOMPRA2: TSingleField;
    sqlLineaVALORVENTA: TSingleField;
    sqlLineaVALORVENTA2: TSingleField;
    sqlLineaVALORMOVI: TSingleField;
    sqlLineaTPCCOMISION: TSingleField;
    sqlLineaIMPOCOMISION: TSingleField;
    sqlLineaMARGEN: TSingleField;
    sqlLineaMARGEN2: TSingleField;
    sqlLineaENCARTE: TSingleField;
    sqlLineaENCARTE2: TSingleField;
    sqlLineaTURNO: TSmallintField;
    sqlLineaNALMACEN: TIntegerField;
    sqlLineaSWALTABAJA: TSmallintField;
    sqlLineaSWPDTEPAGO: TSmallintField;
    sqlLineaSWESTADO: TSmallintField;
    sqlLineaSWDEVOLUCION: TSmallintField;
    sqlLineaSWCERRADO: TSmallintField;
    sqlLineaSWMARCA: TSmallintField;
    sqlLineaSWMARCA2: TSmallintField;
    sqlLineaSWMARCA3: TSmallintField;
    sqlLineaSWMARCA4: TSmallintField;
    sqlLineaSWMARCA5: TSmallintField;
    sqlLineaSEMANA: TSmallintField;
    sqlLineaTIPOVENTA: TSmallintField;
    sqlLineaFECHAAVISO: TDateField;
    sqlLineaFECHADEVOL: TDateField;
    sqlLineaFECHACARGO: TDateField;
    sqlLineaFECHAABONO: TDateField;
    sqlLineaSWRECLAMA: TSmallintField;
    sqlLineaID_HISARTIRE: TIntegerField;
    sqlLineaSWACABADO: TSmallintField;
    sqlLineaBARRAS: TStringField;
    sqlLineaDESCRIPCION: TStringField;
    sqlLineaTBARRAS: TIntegerField;
    sqlLineaFECHACOMPRA: TDateField;
    sqlLineaCOMPRAHISTO: TFloatField;
    sqlLineaVENTAHISTO: TFloatField;
    sqlLineaDEVOLHISTO: TFloatField;
    sqlLineaMERMAHISTO: TFloatField;
    sqlLineaNOMBRE: TStringField;
    sqlCabe1IDSTOCABE: TIntegerField;
    sqlCabe1SWTIPODOCU: TSmallintField;
    sqlCabe1NDOCSTOCK: TIntegerField;
    sqlCabe1FECHA: TDateField;
    sqlCabe1NALMACEN: TIntegerField;
    sqlCabe1NALMACENORIGEN: TIntegerField;
    sqlCabe1NALMACENDESTINO: TIntegerField;
    sqlCabe1ID_PROVEEDOR: TIntegerField;
    sqlCabe1FABRICANTE: TStringField;
    sqlCabe1SWDOCTOPROVE: TSmallintField;
    sqlCabe1DOCTOPROVE: TStringField;
    sqlCabe1DOCTOPROVEFECHA: TDateField;
    sqlCabe1FECHACARGO: TDateField;
    sqlCabe1FECHAABONO: TDateField;
    sqlCabe1NOMBRE: TStringField;
    sqlCabe1NIF: TStringField;
    sqlCabe1MENSAJEAVISO: TStringField;
    sqlCabe1OBSERVACIONES: TMemoField;
    sqlCabe1PAQUETE: TIntegerField;
    sqlCabe1SWDEVOLUCION: TSmallintField;
    sqlCabe1SWCERRADO: TSmallintField;
    sqlCabe1SWFRATIENDA: TSmallintField;
    sqlCabe1DEVOLUSWTIPOINC: TSmallintField;
    sqlCabe1DEVOLUFECHASEL: TDateField;
    sqlCabe1DEVOLUDOCTOPROVE: TStringField;
    sqlCabe1DEVOLUIDSTOCABE: TIntegerField;
    sqlCabe1ID_FRAPROVE: TIntegerField;
    sqlCabe1SWMARCA: TSmallintField;
    sqlCabe1SWMARCA2: TSmallintField;
    sqlCabe1SWMARCA3: TSmallintField;
    sqlCabe1SWMARCA4: TSmallintField;
    sqlCabe1SWMARCA5: TSmallintField;
    sqlCabe1SEMANA: TSmallintField;
    sqlCabe1SWALTABAJA: TSmallintField;
    sqlCabe1FECHABAJA: TDateField;
    sqlCabe1FECHAALTA: TDateField;
    sqlCabe1HORAALTA: TTimeField;
    sqlCabe1FECHAULTI: TDateField;
    sqlCabe1HORAULTI: TTimeField;
    sqlCabe1USUULTI: TStringField;
    sqlCabe1NOTAULTI: TStringField;
    sqlHistoAnteF: TFDQuery;
    sqlHistoAnteFID_ARTICULO: TIntegerField;
    sqlHistoAnteFADENDUM: TStringField;
    sqlHistoAnteFBARRAS: TStringField;
    sqlHistoAnteFDESCRIPCION: TStringField;
    sqlHistoAnteFTBARRAS: TIntegerField;
    sqlHistoAnteFREFEPROVE: TStringField;
    sqlHistoAnteFPRECIOVENTA: TSingleField;
    sqlHistoAnteFPRECIOVENTA2: TSingleField;
    sqlHistoAnteFFECHADEVOL: TDateField;
    sqlHistoAnteFID_PROVEEDOR: TIntegerField;
    sqlHistoAnteFNOMBRE: TStringField;
    sqlHistoAnteFREGISTROS: TIntegerField;
    sqlHistoAnteFCOMPRAS: TFloatField;
    sqlHistoAnteFFECHACOMPRA: TDateField;
    sqlHistoAnteFCOMPRAHISTO: TFloatField;
    sqlHistoAnteFVENTAHISTO: TFloatField;
    sqlHistoAnteFDEVOLHISTO: TFloatField;
    sqlHistoAnteFMERMAHISTO: TFloatField;
    sqlLineaPrecioTotal: TFloatField;
    sqlHistoAnteFA_Devolver: TFloatField;
    sqlHistoAnteFA_Merma: TFloatField;
    sqlHistoAnteFA_Stock: TFloatField;
    sqlHistoAnteFA_Ventas: TFloatField;
    sqlHistoAnteFDevueltos: TFloatField;
    sqlHistoAnteFswPasado: TIntegerField;
    sqlHistoAnteFPrecioTotal: TFloatField;
    sqlHistoAnteFT: TFDQuery;
    sqlHistoAnteFTID_HISARTI: TIntegerField;
    sqlHistoAnteFTID_ARTICULO: TIntegerField;
    sqlHistoAnteFTADENDUM: TStringField;
    sqlHistoAnteFTBARRAS2: TStringField;
    sqlHistoAnteFTDESCRIPCION: TStringField;
    sqlHistoAnteFTTBARRAS: TIntegerField;
    sqlHistoAnteFTREFEPROVE: TStringField;
    sqlHistoAnteFTPRECIOVENTA: TSingleField;
    sqlHistoAnteFTPRECIOVENTA2: TSingleField;
    sqlHistoAnteFTCANTIDAD: TSingleField;
    sqlHistoAnteFTID_PROVEEDOR: TIntegerField;
    sqlHistoAnteFTNOMBRE: TStringField;
    sqlHistoDevolver: TFDQuery;
    sqlHistoDevolverID_HISARTI: TIntegerField;
    sqlHistoDevolverSWES: TStringField;
    sqlHistoDevolverFECHA: TDateField;
    sqlHistoDevolverHORA: TTimeField;
    sqlHistoDevolverCLAVE: TStringField;
    sqlHistoDevolverID_ARTICULO: TIntegerField;
    sqlHistoDevolverADENDUM: TStringField;
    sqlHistoDevolverID_PROVEEDOR: TIntegerField;
    sqlHistoDevolverREFEPROVE: TStringField;
    sqlHistoDevolverIDSTOCABE: TIntegerField;
    sqlHistoDevolverIDDEVOLUCABE: TIntegerField;
    sqlHistoDevolverPAQUETE: TIntegerField;
    sqlHistoDevolverID_HISARTI01: TIntegerField;
    sqlHistoDevolverID_DIARIA: TIntegerField;
    sqlHistoDevolverPARTIDA: TIntegerField;
    sqlHistoDevolverNRECLAMACION: TIntegerField;
    sqlHistoDevolverID_FRAPROVE: TIntegerField;
    sqlHistoDevolverID_CLIENTE: TIntegerField;
    sqlHistoDevolverSWTIPOFRA: TIntegerField;
    sqlHistoDevolverNFACTURA: TIntegerField;
    sqlHistoDevolverNALBARAN: TIntegerField;
    sqlHistoDevolverNLINEA: TIntegerField;
    sqlHistoDevolverDEVUELTOS: TSingleField;
    sqlHistoDevolverVENDIDOS: TSingleField;
    sqlHistoDevolverMERMA: TSingleField;
    sqlHistoDevolverCARGO: TSingleField;
    sqlHistoDevolverABONO: TSingleField;
    sqlHistoDevolverRECLAMADO: TSingleField;
    sqlHistoDevolverCANTIDAD: TSingleField;
    sqlHistoDevolverCANTIENALBA: TSingleField;
    sqlHistoDevolverCANTISUSCRI: TSingleField;
    sqlHistoDevolverPRECIOCOMPRA: TSingleField;
    sqlHistoDevolverPRECIOCOMPRA2: TSingleField;
    sqlHistoDevolverPRECIOCOSTE: TSingleField;
    sqlHistoDevolverPRECIOCOSTE2: TSingleField;
    sqlHistoDevolverPRECIOCOSTETOT: TSingleField;
    sqlHistoDevolverPRECIOCOSTETOT2: TSingleField;
    sqlHistoDevolverPRECIOVENTA: TSingleField;
    sqlHistoDevolverPRECIOVENTA2: TSingleField;
    sqlHistoDevolverSWDTO: TSmallintField;
    sqlHistoDevolverTPCDTO: TSingleField;
    sqlHistoDevolverTIVA: TSmallintField;
    sqlHistoDevolverTPCIVA: TSingleField;
    sqlHistoDevolverTPCRE: TSingleField;
    sqlHistoDevolverTIVA2: TSmallintField;
    sqlHistoDevolverTPCIVA2: TSingleField;
    sqlHistoDevolverTPCRE2: TSingleField;
    sqlHistoDevolverIMPOBRUTO: TSingleField;
    sqlHistoDevolverIMPODTO: TSingleField;
    sqlHistoDevolverIMPOBASE: TSingleField;
    sqlHistoDevolverIMPOIVA: TSingleField;
    sqlHistoDevolverIMPORE: TSingleField;
    sqlHistoDevolverIMPOBASE2: TSingleField;
    sqlHistoDevolverIMPOIVA2: TSingleField;
    sqlHistoDevolverIMPORE2: TSingleField;
    sqlHistoDevolverIMPOTOTLIN: TSingleField;
    sqlHistoDevolverENTRADAS: TSingleField;
    sqlHistoDevolverSALIDAS: TSingleField;
    sqlHistoDevolverVALORCOSTE: TSingleField;
    sqlHistoDevolverVALORCOSTE2: TSingleField;
    sqlHistoDevolverVALORCOSTETOT: TSingleField;
    sqlHistoDevolverVALORCOSTETOT2: TSingleField;
    sqlHistoDevolverVALORCOMPRA: TSingleField;
    sqlHistoDevolverVALORCOMPRA2: TSingleField;
    sqlHistoDevolverVALORVENTA: TSingleField;
    sqlHistoDevolverVALORVENTA2: TSingleField;
    sqlHistoDevolverVALORMOVI: TSingleField;
    sqlHistoDevolverTPCCOMISION: TSingleField;
    sqlHistoDevolverIMPOCOMISION: TSingleField;
    sqlHistoDevolverMARGEN: TSingleField;
    sqlHistoDevolverMARGEN2: TSingleField;
    sqlHistoDevolverENCARTE: TSingleField;
    sqlHistoDevolverENCARTE2: TSingleField;
    sqlHistoDevolverTURNO: TSmallintField;
    sqlHistoDevolverNALMACEN: TIntegerField;
    sqlHistoDevolverSWALTABAJA: TSmallintField;
    sqlHistoDevolverSWPDTEPAGO: TSmallintField;
    sqlHistoDevolverSWESTADO: TSmallintField;
    sqlHistoDevolverSWDEVOLUCION: TSmallintField;
    sqlHistoDevolverSWCERRADO: TSmallintField;
    sqlHistoDevolverSWMARCA: TSmallintField;
    sqlHistoDevolverSWMARCA2: TSmallintField;
    sqlHistoDevolverSWMARCA3: TSmallintField;
    sqlHistoDevolverSWMARCA4: TSmallintField;
    sqlHistoDevolverSWMARCA5: TSmallintField;
    sqlHistoDevolverSEMANA: TSmallintField;
    sqlHistoDevolverTIPOVENTA: TSmallintField;
    sqlHistoDevolverFECHAAVISO: TDateField;
    sqlHistoDevolverFECHADEVOL: TDateField;
    sqlHistoDevolverFECHACARGO: TDateField;
    sqlHistoDevolverFECHAABONO: TDateField;
    sqlHistoDevolverSWRECLAMA: TSmallintField;
    sqlHistoDevolverID_HISARTIRE: TIntegerField;
    sqlHistoDevolverSWACABADO: TSmallintField;
    sqlHistoDevolverBARRAS: TStringField;
    sqlHistoDevolverDESCRIPCION: TStringField;
    sqlHistoDevolverTBARRAS: TIntegerField;
    sqlHistoDevolverNOMBRE: TStringField;
    sqlHistoDevolverADEVOLVER: TFloatField;
    sqlHistoDevolverProve: TFDQuery;
    sqlHistoDevolverProveID_HISARTI: TIntegerField;
    sqlHistoDevolverProveSWES: TStringField;
    sqlHistoDevolverProveFECHA: TDateField;
    sqlHistoDevolverProveHORA: TTimeField;
    sqlHistoDevolverProveCLAVE: TStringField;
    sqlHistoDevolverProveID_ARTICULO: TIntegerField;
    sqlHistoDevolverProveADENDUM: TStringField;
    sqlHistoDevolverProveID_PROVEEDOR: TIntegerField;
    sqlHistoDevolverProveREFEPROVE: TStringField;
    sqlHistoDevolverProveIDSTOCABE: TIntegerField;
    sqlHistoDevolverProveIDDEVOLUCABE: TIntegerField;
    sqlHistoDevolverProvePAQUETE: TIntegerField;
    sqlHistoDevolverProveID_HISARTI01: TIntegerField;
    sqlHistoDevolverProveID_DIARIA: TIntegerField;
    sqlHistoDevolverProvePARTIDA: TIntegerField;
    sqlHistoDevolverProveNRECLAMACION: TIntegerField;
    sqlHistoDevolverProveID_FRAPROVE: TIntegerField;
    sqlHistoDevolverProveID_CLIENTE: TIntegerField;
    sqlHistoDevolverProveSWTIPOFRA: TIntegerField;
    sqlHistoDevolverProveNFACTURA: TIntegerField;
    sqlHistoDevolverProveNALBARAN: TIntegerField;
    sqlHistoDevolverProveNLINEA: TIntegerField;
    sqlHistoDevolverProveDEVUELTOS: TSingleField;
    sqlHistoDevolverProveVENDIDOS: TSingleField;
    sqlHistoDevolverProveMERMA: TSingleField;
    sqlHistoDevolverProveCARGO: TSingleField;
    sqlHistoDevolverProveABONO: TSingleField;
    sqlHistoDevolverProveRECLAMADO: TSingleField;
    sqlHistoDevolverProveCANTIDAD: TSingleField;
    sqlHistoDevolverProveCANTIENALBA: TSingleField;
    sqlHistoDevolverProveCANTISUSCRI: TSingleField;
    sqlHistoDevolverProvePRECIOCOMPRA: TSingleField;
    sqlHistoDevolverProvePRECIOCOMPRA2: TSingleField;
    sqlHistoDevolverProvePRECIOCOSTE: TSingleField;
    sqlHistoDevolverProvePRECIOCOSTE2: TSingleField;
    sqlHistoDevolverProvePRECIOCOSTETOT: TSingleField;
    sqlHistoDevolverProvePRECIOCOSTETOT2: TSingleField;
    sqlHistoDevolverProvePRECIOVENTA: TSingleField;
    sqlHistoDevolverProvePRECIOVENTA2: TSingleField;
    sqlHistoDevolverProveSWDTO: TSmallintField;
    sqlHistoDevolverProveTPCDTO: TSingleField;
    sqlHistoDevolverProveTIVA: TSmallintField;
    sqlHistoDevolverProveTPCIVA: TSingleField;
    sqlHistoDevolverProveTPCRE: TSingleField;
    sqlHistoDevolverProveTIVA2: TSmallintField;
    sqlHistoDevolverProveTPCIVA2: TSingleField;
    sqlHistoDevolverProveTPCRE2: TSingleField;
    sqlHistoDevolverProveIMPOBRUTO: TSingleField;
    sqlHistoDevolverProveIMPODTO: TSingleField;
    sqlHistoDevolverProveIMPOBASE: TSingleField;
    sqlHistoDevolverProveIMPOIVA: TSingleField;
    sqlHistoDevolverProveIMPORE: TSingleField;
    sqlHistoDevolverProveIMPOBASE2: TSingleField;
    sqlHistoDevolverProveIMPOIVA2: TSingleField;
    sqlHistoDevolverProveIMPORE2: TSingleField;
    sqlHistoDevolverProveIMPOTOTLIN: TSingleField;
    sqlHistoDevolverProveENTRADAS: TSingleField;
    sqlHistoDevolverProveSALIDAS: TSingleField;
    sqlHistoDevolverProveVALORCOSTE: TSingleField;
    sqlHistoDevolverProveVALORCOSTE2: TSingleField;
    sqlHistoDevolverProveVALORCOSTETOT: TSingleField;
    sqlHistoDevolverProveVALORCOSTETOT2: TSingleField;
    sqlHistoDevolverProveVALORCOMPRA: TSingleField;
    sqlHistoDevolverProveVALORCOMPRA2: TSingleField;
    sqlHistoDevolverProveVALORVENTA: TSingleField;
    sqlHistoDevolverProveVALORVENTA2: TSingleField;
    sqlHistoDevolverProveVALORMOVI: TSingleField;
    sqlHistoDevolverProveTPCCOMISION: TSingleField;
    sqlHistoDevolverProveIMPOCOMISION: TSingleField;
    sqlHistoDevolverProveMARGEN: TSingleField;
    sqlHistoDevolverProveMARGEN2: TSingleField;
    sqlHistoDevolverProveENCARTE: TSingleField;
    sqlHistoDevolverProveENCARTE2: TSingleField;
    sqlHistoDevolverProveTURNO: TSmallintField;
    sqlHistoDevolverProveNALMACEN: TIntegerField;
    sqlHistoDevolverProveSWALTABAJA: TSmallintField;
    sqlHistoDevolverProveSWPDTEPAGO: TSmallintField;
    sqlHistoDevolverProveSWESTADO: TSmallintField;
    sqlHistoDevolverProveSWDEVOLUCION: TSmallintField;
    sqlHistoDevolverProveSWCERRADO: TSmallintField;
    sqlHistoDevolverProveSWMARCA: TSmallintField;
    sqlHistoDevolverProveSWMARCA2: TSmallintField;
    sqlHistoDevolverProveSWMARCA3: TSmallintField;
    sqlHistoDevolverProveSWMARCA4: TSmallintField;
    sqlHistoDevolverProveSWMARCA5: TSmallintField;
    sqlHistoDevolverProveSEMANA: TSmallintField;
    sqlHistoDevolverProveTIPOVENTA: TSmallintField;
    sqlHistoDevolverProveFECHAAVISO: TDateField;
    sqlHistoDevolverProveFECHADEVOL: TDateField;
    sqlHistoDevolverProveFECHACARGO: TDateField;
    sqlHistoDevolverProveFECHAABONO: TDateField;
    sqlHistoDevolverProveSWRECLAMA: TSmallintField;
    sqlHistoDevolverProveID_HISARTIRE: TIntegerField;
    sqlHistoDevolverProveSWACABADO: TSmallintField;
    sqlHistoDevolverProveBARRAS: TStringField;
    sqlHistoDevolverProveDESCRIPCION: TStringField;
    sqlHistoDevolverProveTBARRAS: TIntegerField;
    sqlHistoDevolverProveNOMBRE: TStringField;
    sqlCabeSCap: TFDQuery;
    sqlCabeSCapIDSTOCABE: TIntegerField;
    sqlCabeSCapSWTIPODOCU: TSmallintField;
    sqlCabeSCapNDOCSTOCK: TIntegerField;
    sqlCabeSCapFECHA: TDateField;
    sqlCabeSCapNALMACEN: TIntegerField;
    sqlCabeSCapID_PROVEEDOR: TIntegerField;
    sqlCabeSCapDOCTOPROVE: TStringField;
    sqlCabeSCapDOCTOPROVEFECHA: TDateField;
    sqlCabeSCapFECHACARGO: TDateField;
    sqlCabeSCapFECHAABONO: TDateField;
    sqlCabeSCapNOMBRE: TStringField;
    sqlCabeSCapNIF: TStringField;
    sqlCabeSCapMENSAJEAVISO: TStringField;
    sqlCabeSCapSWDEVOLUCION: TSmallintField;
    sqlProveS: TFDQuery;
    sqlProveSID_PROVEEDOR: TIntegerField;
    sqlProveSNOMBRE: TStringField;
    sqlProveSDIRECCION: TStringField;
    sqlProveSPOBLACION: TStringField;
    sqlProveSPROVINCIA: TStringField;
    sqlProveSCPOSTAL: TStringField;
    sqlProveSCPROVINCIA: TStringField;
    sqlProveSNIF: TStringField;
    sqlProveSTELEFONO1: TStringField;
    sqlProveSTELEFONO2: TStringField;
    sqlProveSMOVIL: TStringField;
    sqlProveSREFEPROVEEDOR: TStringField;
    sqlProveSRUTA: TStringField;
    sqlHistoAnteFDevolTeorica: TFloatField;
    sqlProve1: TFDQuery;
    sqlProve1ID_PROVEEDOR: TIntegerField;
    sqlProve1TPROVEEDOR: TSmallintField;
    sqlProve1NOMBRE: TStringField;
    sqlProve1NIF: TStringField;
    sqlProve1ID_DIRECCION: TIntegerField;
    sqlProve1NOMBRE2: TStringField;
    sqlProve1DIRECCION: TStringField;
    sqlProve1DIRECCION2: TStringField;
    sqlProve1POBLACION: TStringField;
    sqlProve1PROVINCIA: TStringField;
    sqlProve1CPOSTAL: TStringField;
    sqlProve1CPAIS: TStringField;
    sqlProve1CPROVINCIA: TStringField;
    sqlProve1CONTACTO1NOMBRE: TStringField;
    sqlProve1CONTACTO1CARGO: TStringField;
    sqlProve1CONTACTO2NOMBRE: TStringField;
    sqlProve1CONTACTO2CARGO: TStringField;
    sqlProve1TELEFONO1: TStringField;
    sqlProve1TELEFONO2: TStringField;
    sqlProve1MOVIL: TStringField;
    sqlProve1FAX: TStringField;
    sqlProve1EMAIL: TStringField;
    sqlProve1WEB: TStringField;
    sqlProve1MENSAJEAVISO: TStringField;
    sqlProve1OBSERVACIONES: TMemoField;
    sqlProve1BANCO: TStringField;
    sqlProve1AGENCIA: TStringField;
    sqlProve1DC: TStringField;
    sqlProve1CUENTABANCO: TStringField;
    sqlProve1TEFECTO: TSmallintField;
    sqlProve1EFECTOS: TSmallintField;
    sqlProve1FRECUENCIA: TSmallintField;
    sqlProve1DIASPRIMERVTO: TSmallintField;
    sqlProve1DIAFIJO1: TSmallintField;
    sqlProve1DIAFIJO2: TSmallintField;
    sqlProve1DIAFIJO3: TSmallintField;
    sqlProve1TDTO: TIntegerField;
    sqlProve1TPAGO: TIntegerField;
    sqlProve1SWTFACTURACION: TSmallintField;
    sqlProve1SWBLOQUEO: TSmallintField;
    sqlProve1REFEPROVEEDOR: TStringField;
    sqlProve1TRANSPORTE: TSmallintField;
    sqlProve1RUTA: TStringField;
    sqlProve1PROVEEDORHOSTING: TIntegerField;
    sqlProve1NOMBREHOSTING: TStringField;
    sqlProve1ULTIMAFECHADESCARGA: TDateField;
    sqlProve1ULTIMAFECHAENVIO: TDateField;
    sqlProve1COLUMNASEXCEL: TStringField;
    sqlProve1DESDEFILA: TIntegerField;
    sqlProve1SWRECIBIR: TSmallintField;
    sqlProve1SWENVIAR: TSmallintField;
    sqlProve1SWALTABAJA: TSmallintField;
    sqlProve1FECHABAJA: TDateField;
    sqlProve1FECHAALTA: TDateField;
    sqlProve1HORAALTA: TTimeField;
    sqlProve1FECHAULTI: TDateField;
    sqlProve1HORAULTI: TTimeField;
    sqlProve1USUULTI: TStringField;
    sqlProve1NOTAULTI: TStringField;
    sqlEmpresa: TFDQuery;
    sqlEmpresaNREFERENCIA: TIntegerField;
    sqlEmpresaNOMBRE: TStringField;
    sqlEmpresaNOMBRE2: TStringField;
    sqlEmpresaTVIA: TStringField;
    sqlEmpresaVIANOMBRE: TStringField;
    sqlEmpresaVIANUM: TStringField;
    sqlEmpresaVIABLOC: TStringField;
    sqlEmpresaVIABIS: TStringField;
    sqlEmpresaVIAESCA: TStringField;
    sqlEmpresaVIAPISO: TStringField;
    sqlEmpresaVIAPUERTA: TStringField;
    sqlEmpresaDIRECCION: TStringField;
    sqlEmpresaDIRECCION2: TStringField;
    sqlEmpresaPOBLACION: TStringField;
    sqlEmpresaPROVINCIA: TStringField;
    sqlEmpresaCPOSTAL: TStringField;
    sqlEmpresaCPAIS: TStringField;
    sqlEmpresaCPROVINCIA: TStringField;
    sqlEmpresaNIF: TStringField;
    sqlEmpresaCONTACTO1NOMRE: TStringField;
    sqlEmpresaCONTACTO1CARGO: TStringField;
    sqlEmpresaCONTACTO2NOMBRE: TStringField;
    sqlEmpresaCONTACTO2CARGO: TStringField;
    sqlEmpresaTELEFONO1: TStringField;
    sqlEmpresaTELEFONO2: TStringField;
    sqlEmpresaMOVIL: TStringField;
    sqlEmpresaFAX: TStringField;
    sqlEmpresaEMAIL: TStringField;
    sqlEmpresaWEB: TStringField;
    sqlEmpresaMENSAJEAVISO: TStringField;
    sqlEmpresaOBSERVACIONES: TMemoField;
    sqlEmpresaNACTIVIDAD: TIntegerField;
    sqlEmpresaNSECTOR: TIntegerField;
    sqlEmpresaTPCIVA1: TSingleField;
    sqlEmpresaTPCIVA2: TSingleField;
    sqlEmpresaTPCIVA3: TSingleField;
    sqlEmpresaTPCRE1: TSingleField;
    sqlEmpresaTPCRE2: TSingleField;
    sqlEmpresaTPCRE3: TSingleField;
    sqlEmpresaNPEDIDO: TIntegerField;
    sqlEmpresaNPEDIPROVE: TIntegerField;
    sqlEmpresaNALBARAN: TIntegerField;
    sqlEmpresaNALBASERVI: TIntegerField;
    sqlEmpresaNALBACOLECTOR: TIntegerField;
    sqlEmpresaNALBALMA: TIntegerField;
    sqlEmpresaNALBAREPARA: TIntegerField;
    sqlEmpresaNFACTURANORMAL: TIntegerField;
    sqlEmpresaNFACTURASERVI: TIntegerField;
    sqlEmpresaNFACTURACOLECTOR: TIntegerField;
    sqlEmpresaNFACTURATIENDA: TIntegerField;
    sqlEmpresaNFACTURAREPARA: TIntegerField;
    sqlEmpresaNUMTICKET: TIntegerField;
    sqlEmpresaNCOMPRA: TIntegerField;
    sqlEmpresaNTRASPASO: TIntegerField;
    sqlEmpresaREGISTROMERCANTIL: TStringField;
    sqlEmpresaPIEFRANORMAL: TMemoField;
    sqlEmpresaPIEFRASERVI: TMemoField;
    sqlEmpresaPIEFRACOLLECTOR: TMemoField;
    sqlEmpresaPIEFRATIENDA: TMemoField;
    sqlEmpresaPIEFRAREPARA: TMemoField;
    sqlEmpresaIMPTOPEFRAIDE: TSingleField;
    sqlEmpresaCTACONTAVTA1: TStringField;
    sqlEmpresaCTACONTAVTA2: TStringField;
    sqlEmpresaCTACONTAVTA3: TStringField;
    sqlEmpresaCTACONTAVTA4: TStringField;
    sqlEmpresaCTACONTAVTA5: TStringField;
    sqlEmpresaCTACONTAIVA1: TStringField;
    sqlEmpresaCTACONTAIVA2: TStringField;
    sqlEmpresaCTACONTAIVA3: TStringField;
    sqlEmpresaCTACONTARE1: TStringField;
    sqlEmpresaCTACONTARE2: TStringField;
    sqlEmpresaCTACONTARE3: TStringField;
    sqlEmpresaTPCINCRECOMPRACESIO: TSingleField;
    sqlEmpresaTPCINCRECOMPRAVENTA: TSingleField;
    sqlEmpresaNALMACEN: TIntegerField;
    sqlEmpresaRUTAFTP: TStringField;
    sqlEmpresaRUTAIMAGENFTP: TStringField;
    sqlEmpresaRUTAIMAGENPC: TStringField;
    sqlEmpresaRUTACAMPO: TIntegerField;
    sqlEmpresaRUTAAVISOSFTP: TStringField;
    sqlEmpresaRUTAAVISOSPC: TStringField;
    sqlEmpresaHOSTFTP: TStringField;
    sqlEmpresaUSUFTP: TStringField;
    sqlEmpresaPASSFTP: TStringField;
    sqlEmpresaDIASRECEGEN: TSmallintField;
    sqlEmpresaTRANSPORTE: TSmallintField;
    sqlEmpresaSWRECARGOE: TSmallintField;
    sqlEmpresaSWALTABAJA: TSmallintField;
    sqlEmpresaFECHABAJA: TDateField;
    sqlEmpresaFECHAALTA: TDateField;
    sqlEmpresaHORAALTA: TTimeField;
    sqlEmpresaFECHAULTI: TDateField;
    sqlEmpresaHORAULTI: TTimeField;
    sqlEmpresaUSUULTI: TStringField;
    sqlEmpresaNOTAULTI: TStringField;
    sqlHistoAnte2: TFDQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DateField1: TDateField;
    TimeField1: TTimeField;
    StringField2: TStringField;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    IntegerField3: TIntegerField;
    StringField4: TStringField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    IntegerField15: TIntegerField;
    IntegerField16: TIntegerField;
    SingleField1: TSingleField;
    SingleField2: TSingleField;
    SingleField3: TSingleField;
    SingleField4: TSingleField;
    SingleField5: TSingleField;
    SingleField6: TSingleField;
    SingleField7: TSingleField;
    SingleField8: TSingleField;
    SingleField9: TSingleField;
    SingleField10: TSingleField;
    SingleField11: TSingleField;
    SingleField12: TSingleField;
    SingleField13: TSingleField;
    SingleField14: TSingleField;
    SingleField15: TSingleField;
    SingleField16: TSingleField;
    SingleField17: TSingleField;
    SmallintField1: TSmallintField;
    SingleField18: TSingleField;
    SmallintField2: TSmallintField;
    SingleField19: TSingleField;
    SingleField20: TSingleField;
    SmallintField3: TSmallintField;
    SingleField21: TSingleField;
    SingleField22: TSingleField;
    SingleField23: TSingleField;
    SingleField24: TSingleField;
    SingleField25: TSingleField;
    SingleField26: TSingleField;
    SingleField27: TSingleField;
    SingleField28: TSingleField;
    SingleField29: TSingleField;
    SingleField30: TSingleField;
    SingleField31: TSingleField;
    SingleField32: TSingleField;
    SingleField33: TSingleField;
    SingleField34: TSingleField;
    SingleField35: TSingleField;
    SingleField36: TSingleField;
    SingleField37: TSingleField;
    SingleField38: TSingleField;
    SingleField39: TSingleField;
    SingleField40: TSingleField;
    SingleField41: TSingleField;
    SingleField42: TSingleField;
    SingleField43: TSingleField;
    SingleField44: TSingleField;
    SingleField45: TSingleField;
    SingleField46: TSingleField;
    SingleField47: TSingleField;
    SingleField48: TSingleField;
    SmallintField4: TSmallintField;
    IntegerField17: TIntegerField;
    SmallintField5: TSmallintField;
    SmallintField6: TSmallintField;
    SmallintField7: TSmallintField;
    SmallintField8: TSmallintField;
    SmallintField9: TSmallintField;
    SmallintField10: TSmallintField;
    SmallintField11: TSmallintField;
    SmallintField12: TSmallintField;
    SmallintField13: TSmallintField;
    SmallintField14: TSmallintField;
    SmallintField15: TSmallintField;
    SmallintField16: TSmallintField;
    DateField2: TDateField;
    DateField3: TDateField;
    DateField4: TDateField;
    DateField5: TDateField;
    SmallintField17: TSmallintField;
    IntegerField18: TIntegerField;
    SmallintField18: TSmallintField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField19: TIntegerField;
    DateField6: TDateField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    StringField7: TStringField;
    FloatField5: TFloatField;
    sqlListaDistri: TFDQuery;
    IntegerField20: TIntegerField;
    SmallintField19: TSmallintField;
    StringField8: TStringField;
    StringField9: TStringField;
    IntegerField21: TIntegerField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    MemoField1: TMemoField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    SmallintField20: TSmallintField;
    SmallintField21: TSmallintField;
    SmallintField22: TSmallintField;
    SmallintField23: TSmallintField;
    SmallintField24: TSmallintField;
    SmallintField25: TSmallintField;
    SmallintField26: TSmallintField;
    IntegerField22: TIntegerField;
    IntegerField23: TIntegerField;
    SmallintField27: TSmallintField;
    SmallintField28: TSmallintField;
    StringField33: TStringField;
    SmallintField29: TSmallintField;
    StringField34: TStringField;
    IntegerField24: TIntegerField;
    StringField35: TStringField;
    DateField7: TDateField;
    DateField8: TDateField;
    StringField36: TStringField;
    IntegerField25: TIntegerField;
    SmallintField30: TSmallintField;
    SmallintField31: TSmallintField;
    SmallintField32: TSmallintField;
    DateField9: TDateField;
    DateField10: TDateField;
    TimeField2: TTimeField;
    DateField11: TDateField;
    TimeField3: TTimeField;
    StringField37: TStringField;
    StringField38: TStringField;
    sqlCabeNuevo: TFDQuery;
    sqlCabeNuevoIDSTOCABE: TIntegerField;
    sqlCabeNuevoSWTIPODOCU: TSmallintField;
    sqlCabeNuevoNOMBRE: TStringField;
    sqlCabeNuevoPAQUETE: TIntegerField;
    SP_GEN_STOCABE: TFDStoredProc;
    sqlProveSMAXPAQUETE: TIntegerField;
    sqlCabeNuevoFECHA: TDateField;
    sqlCabeNuevoDOCTOPROVE: TStringField;
    sqlCabeNuevoDOCTOPROVEFECHA: TDateField;
    sqlCabeNuevoID_PROVEEDOR: TIntegerField;
    sqlCabeNuevoNIF: TStringField;
    sqlCabeNuevoMAXPAQUETE: TIntegerField;
    sqlCabe1ID_DIRECCION: TIntegerField;
    sqlCabe1MAXPAQUETE: TIntegerField;
    sqlLineaTotal: TFDQuery;
    sqlLineaTotalTOTAL: TFloatField;
    sqlCabe1REFEPROVEEDOR: TStringField;
    sqlCabe1RUTA: TStringField;
    sqlCabe1TextoPaquete: TStringField;
    sqlCabe1TotalDevueltos: TFloatField;
    sqlHisto: TFDQuery;
    sqlHisArtiCabe1: TFDQuery;
    sqlHisArtiCabe1IDSTOCABE: TIntegerField;
    sqlHisArtiCabe1SWTIPODOCU: TSmallintField;
    sqlHisArtiCabe1NDOCSTOCK: TIntegerField;
    sqlHisArtiCabe1FECHA: TDateField;
    sqlHisArtiCabe1NALMACEN: TIntegerField;
    sqlHisArtiCabe1NALMACENORIGEN: TIntegerField;
    sqlHisArtiCabe1NALMACENDESTINO: TIntegerField;
    sqlHisArtiCabe1ID_PROVEEDOR: TIntegerField;
    sqlHisArtiCabe1ID_DIRECCION: TIntegerField;
    sqlHisArtiCabe1FABRICANTE: TStringField;
    sqlHisArtiCabe1SWDOCTOPROVE: TSmallintField;
    sqlHisArtiCabe1DOCTOPROVE: TStringField;
    sqlHisArtiCabe1DOCTOPROVEFECHA: TDateField;
    sqlHisArtiCabe1FECHACARGO: TDateField;
    sqlHisArtiCabe1FECHAABONO: TDateField;
    sqlHisArtiCabe1NOMBRE: TStringField;
    sqlHisArtiCabe1NIF: TStringField;
    sqlHisArtiCabe1MENSAJEAVISO: TStringField;
    sqlHisArtiCabe1OBSERVACIONES: TMemoField;
    sqlHisArtiCabe1PAQUETE: TIntegerField;
    sqlHisArtiCabe1SWDEVOLUCION: TSmallintField;
    sqlHisArtiCabe1SWCERRADO: TSmallintField;
    sqlHisArtiCabe1SWFRATIENDA: TSmallintField;
    sqlHisArtiCabe1DEVOLUSWTIPOINC: TSmallintField;
    sqlHisArtiCabe1DEVOLUFECHASEL: TDateField;
    sqlHisArtiCabe1DEVOLUDOCTOPROVE: TStringField;
    sqlHisArtiCabe1DEVOLUIDSTOCABE: TIntegerField;
    sqlHisArtiCabe1ID_FRAPROVE: TIntegerField;
    sqlHisArtiCabe1SWMARCA: TSmallintField;
    sqlHisArtiCabe1SWMARCA2: TSmallintField;
    sqlHisArtiCabe1SWMARCA3: TSmallintField;
    sqlHisArtiCabe1SWMARCA4: TSmallintField;
    sqlHisArtiCabe1SWMARCA5: TSmallintField;
    sqlHisArtiCabe1SEMANA: TSmallintField;
    sqlHisArtiCabe1SWALTABAJA: TSmallintField;
    sqlHisArtiCabe1FECHABAJA: TDateField;
    sqlHisArtiCabe1FECHAALTA: TDateField;
    sqlHisArtiCabe1HORAALTA: TTimeField;
    sqlHisArtiCabe1FECHAULTI: TDateField;
    sqlHisArtiCabe1HORAULTI: TTimeField;
    sqlHisArtiCabe1USUULTI: TStringField;
    sqlHisArtiCabe1NOTAULTI: TStringField;
    sqlHisArtiCabe1MAXPAQUETE: TIntegerField;
    sqlcabe1COPIA: TFDQuery;
    IntegerField26: TIntegerField;
    SmallintField33: TSmallintField;
    IntegerField27: TIntegerField;
    DateField12: TDateField;
    IntegerField28: TIntegerField;
    IntegerField29: TIntegerField;
    IntegerField30: TIntegerField;
    IntegerField31: TIntegerField;
    StringField39: TStringField;
    SmallintField34: TSmallintField;
    StringField40: TStringField;
    DateField13: TDateField;
    DateField14: TDateField;
    DateField15: TDateField;
    StringField41: TStringField;
    StringField42: TStringField;
    StringField43: TStringField;
    MemoField2: TMemoField;
    IntegerField32: TIntegerField;
    SmallintField35: TSmallintField;
    SmallintField36: TSmallintField;
    SmallintField37: TSmallintField;
    SmallintField38: TSmallintField;
    DateField16: TDateField;
    StringField44: TStringField;
    IntegerField33: TIntegerField;
    IntegerField34: TIntegerField;
    SmallintField39: TSmallintField;
    SmallintField40: TSmallintField;
    SmallintField41: TSmallintField;
    SmallintField42: TSmallintField;
    SmallintField43: TSmallintField;
    SmallintField44: TSmallintField;
    SmallintField45: TSmallintField;
    DateField17: TDateField;
    DateField18: TDateField;
    TimeField4: TTimeField;
    DateField19: TDateField;
    TimeField5: TTimeField;
    StringField45: TStringField;
    StringField46: TStringField;
    IntegerField35: TIntegerField;
    IntegerField36: TIntegerField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    FloatField6: TFloatField;
    sqlCabe1TOTALDEVOL: TFloatField;
    sqlCabeS: TFDQuery;
    sqlCabeSIDSTOCABE: TIntegerField;
    sqlCabeSSWTIPODOCU: TSmallintField;
    sqlCabeSNDOCSTOCK: TIntegerField;
    sqlCabeSFECHA: TDateField;
    sqlCabeSNALMACEN: TIntegerField;
    sqlCabeSID_PROVEEDOR: TIntegerField;
    sqlCabeSDOCTOPROVE: TStringField;
    sqlCabeSDOCTOPROVEFECHA: TDateField;
    sqlCabeSFECHACARGO: TDateField;
    sqlCabeSFECHAABONO: TDateField;
    sqlCabeSNOMBRE: TStringField;
    sqlCabeSNIF: TStringField;
    sqlCabeSMENSAJEAVISO: TStringField;
    sqlCabeSSWDEVOLUCION: TSmallintField;
    sqlCabeSPAQUETE: TIntegerField;
    sqlCabeSREFEPROVEEDOR: TStringField;
    sqlCabeSRUTA: TStringField;
    sqlCabeSTOTALPAQUETES: TIntegerField;
    sqlCabeSULTIMOPAQUETE: TIntegerField;
    sqlCabeSTOTALDEVUELTOS: TFloatField;
    sqlPaquetes: TFDQuery;
    sqlPaquetesIDSTOCABE: TIntegerField;
    sqlPaquetesPAQUETE: TIntegerField;
    sqlPaquetesEJEMPLARES: TFloatField;
    sqlPaquetesDISTRIBUIDORA: TStringField;
    sqlArticulo: TFDQuery;
    sqlArticuloID_ARTICULO: TIntegerField;
    sqlArticuloTBARRAS: TIntegerField;
    sqlArticuloBARRAS: TStringField;
    sqlArticuloADENDUM: TStringField;
    sqlArticuloFECHAADENDUM: TDateField;
    sqlArticuloNADENDUM: TIntegerField;
    sqlArticuloDESCRIPCION: TStringField;
    sqlArticuloDESCRIPCION2: TStringField;
    sqlArticuloIBS: TStringField;
    sqlArticuloISBN: TStringField;
    sqlArticuloEDITOR: TIntegerField;
    sqlArticuloPRECIO1: TSingleField;
    sqlArticuloPRECIO2: TSingleField;
    sqlArticuloPRECIO3: TSingleField;
    sqlArticuloPRECIO4: TSingleField;
    sqlArticuloPRECIO5: TSingleField;
    sqlArticuloPRECIO6: TSingleField;
    sqlArticuloPRECIO7: TSingleField;
    sqlArticuloTPCENCARTE1: TSingleField;
    sqlArticuloTPCENCARTE2: TSingleField;
    sqlArticuloTPCENCARTE3: TSingleField;
    sqlArticuloTPCENCARTE4: TSingleField;
    sqlArticuloTPCENCARTE5: TSingleField;
    sqlArticuloTPCENCARTE6: TSingleField;
    sqlArticuloTPCENCARTE7: TSingleField;
    sqlArticuloPRECIOCOSTE: TSingleField;
    sqlArticuloPRECIOCOSTE2: TSingleField;
    sqlArticuloPRECIOCOSTETOT: TSingleField;
    sqlArticuloPRECIOCOSTETOT2: TSingleField;
    sqlArticuloMARGEN: TSingleField;
    sqlArticuloMARGEN2: TSingleField;
    sqlArticuloMARGENMAXIMO: TSingleField;
    sqlArticuloEDITORIAL: TIntegerField;
    sqlArticuloREFEPROVE: TStringField;
    sqlArticuloPERIODICIDAD: TIntegerField;
    sqlArticuloCADUCIDAD: TIntegerField;
    sqlArticuloTIVA: TIntegerField;
    sqlArticuloTIVA2: TIntegerField;
    sqlArticuloSWRECARGO: TSmallintField;
    sqlArticuloSWACTIVADO: TSmallintField;
    sqlArticuloSWINGRESO: TSmallintField;
    sqlArticuloSWCONTROLFECHA: TSmallintField;
    sqlArticuloSWTPRECIO: TSmallintField;
    sqlArticuloSWORIGEN: TIntegerField;
    sqlArticuloTIPO: TStringField;
    sqlArticuloTPRODUCTO: TIntegerField;
    sqlArticuloTCONTENIDO: TIntegerField;
    sqlArticuloTEMATICA: TIntegerField;
    sqlArticuloAUTOR: TIntegerField;
    sqlArticuloOBSERVACIONES: TMemoField;
    sqlArticuloSTOCKMINIMO: TSingleField;
    sqlArticuloSTOCKMAXIMO: TSingleField;
    sqlArticuloFAMILIA: TStringField;
    sqlArticuloFAMILIAVENTA: TIntegerField;
    sqlArticuloORDENVENTA: TIntegerField;
    sqlArticuloSTOCKMINIMOESTANTE: TSingleField;
    sqlArticuloSTOCKMAXIMOESTANTE: TSingleField;
    sqlArticuloSTOCKVENTAESTANTE: TSingleField;
    sqlArticuloREPONERESTANTE: TSingleField;
    sqlArticuloUNIDADESCOMPRA: TSingleField;
    sqlArticuloUNIDADESVENTA: TSingleField;
    sqlArticuloIMAGEN: TStringField;
    sqlArticuloCOD_ARTICU: TStringField;
    sqlArticuloGRUPO: TIntegerField;
    sqlArticuloSUBGRUPO: TIntegerField;
    sqlArticuloSWALTABAJA: TSmallintField;
    sqlArticuloFECHABAJA: TDateField;
    sqlArticuloFECHAALTA: TDateField;
    sqlArticuloFECHAULTI: TDateField;
    sqlCabe1TOTALPAQUETES: TIntegerField;
    sqlCabeNuevoSWDEVOLUCION: TSmallintField;
    procedure sqlCabe1AfterScroll(DataSet: TDataSet);
    procedure sqlProveSAfterScroll(DataSet: TDataSet);
    procedure sqlLineaCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);

  private
    procedure RutAbrirLineaTotal(vID_STOCABE : Integer);
    function RutExisteProveedor(vTabla: TFDQuery; vCampo: String): Boolean;



    { Private declarations }
  public
    { Public declarations }
    vID : Integer;
    swNoExiste, swRepetido, swBusquedaArti : Boolean;

    procedure RutLocateLinea(vID: Integer);
    procedure RutLocateCabe(vID: Integer);
    procedure RutAbrirCabeS(swDevol : Integer; vDesde,vHasta : TDate; vFiltroFecha : Boolean; vBusqueda : String);

    procedure RutAbrirHisArtiCabe1;
    procedure RutCalcularCamposInforme;
    procedure RutAbrirAlbaran(vIDCabe,vTipoDoc : String; swDevol : Integer; vDesde,vHasta : TDate; vFiltroFecha : Boolean);

    procedure RutAbrirTablas;
    procedure RutAbrirLinea(vID_Proveedor : Integer);
    procedure RutLeerCodigoBarras;
    procedure RutAbrirTablasNuevo;

    procedure RutNuevaCabe(vAltaDistri : Integer);
    procedure GrabarCabeNuevo(vTipo : Integer);
    procedure CancelarCabeNuevo;
    Procedure RutBorrarDevolucion;

    procedure RutGrabarProvee;
    procedure RutPonerCampos;
    procedure RutAbrirCerrarDevol(vIDCabe,swDevol: Integer);
    procedure RutScrollCabe1;

    procedure RutComprobarPaquete;
    procedure RutDevolu_CalculoVendidos_A(zTabHisto: TFDQuery;
      zVendidos: Boolean);

    procedure RutAbrirPaquetes(vIdProve, vIdCabe: Integer);
    procedure RutCambiarPaquete(vNum: Integer);
    end;

function DMDevolucion: TDMDevolucion;

implementation
{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uDevolMenu, uMenuBotones, uDevolLista, uDevolFicha,
  uConstVar, uEscogerArti ,uMensajes;

function DMDevolucion: TDMDevolucion;
begin
  Result := TDMDevolucion(DMppal.GetModuleInstance(TDMDevolucion));
end;

procedure TDMDevolucion.RutAbrirTablas;
begin

  //RutAbrirCabeS(1);

  sqlProveS.Close;
  sqlProveS.Open;
end;

procedure TDMDevolucion.RutAbrirTablasNuevo;
begin
//  sqlCabe1.Close;
//  sqlCabe1.Open;

  sqlCabeNuevo.Close;
  sqlCabeNuevo.Open();
end;

procedure TDMDevolucion.RutAbrirCabeS(swDevol : Integer; vDesde,vHasta : TDate; vFiltroFecha : Boolean; vBusqueda : String);
var
  vWhere,vStringFecha, vLike : String;
begin
  vWhere := ' > ' + IntToStr(swDevol);
  if (swDevol = 1) or (swDevol = 3) then vWhere := ' = ' + IntToStr(swDevol);

  vStringFecha := ' ';
  if vFiltroFecha then
    vStringFecha := ' AND S.FECHA BETWEEN ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vDesde)) + ' AND ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vHasta));


  vLike   := '';
  if vBusqueda <> '' then
  begin
    if not swBusquedaArti then
    begin
      //vWhere := 'WHERE FA.ID_ARTICULO LIKE '      + QuotedStr('%'+vBusqueda+'%');
      vLike   := vLike + ' AND (UPPER(S.IDSTOCABE)  LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
      vLike   := vLike + ' OR UPPER(S.NOMBRE)       LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
      vLike   := vLike + ' OR UPPER(S.DOCTOPROVE)   LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
     //vLike   := vLike + ' OR UPPER(S.NOMBRE)      LIKE '   + QuotedStr('%'+UpperCase(vBusqueda)+'%');
      vLike   := vLike + ')'
    end
    else
    begin
      vLike := 'AND EXISTS (SELECT H.ID_ARTICULO FROM FVHISARTI H WHERE H.IDSTOCABE = S.IDSTOCABE AND H.ID_ARTICULO = ' + vBusqueda + ')';
    end;
  end;

  with sqlCabeS do
  begin
    Close;
    sqlCabeS.SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE S.SWDEVOLUCION', Uppercase(SQL.Text))-1)
                  + 'WHERE S.SWDEVOLUCION ' + vWhere
                  + ' and (S.SWTIPODOCU = 2 or S.SWTIPODOCU = 22)'
                  + vStringFecha
                  + vLike
                  + ' Order by S.FECHA DESC ' ;
    Open;
  end;
end;



procedure TDMDevolucion.RutDevolu_CalculoVendidos_A (zTabHisto:TFDQuery; zVendidos:Boolean);
begin
 if DMppal.RutEdiInse(zTabHisto) = False Then Exit;

 if zVendidos then
 begin
   zTabHisto.FieldByName('VENDIDOS').AsFloat := 0;
   exit;
 end;
 zTabHisto.FieldByName('VENDIDOS').AsFloat :=
    zTabHisto.FieldByName('COMPRAHISTO').AsFloat - zTabHisto.FieldByName('CANTIENALBA').AsFloat
  - zTabHisto.FieldByName('VENTAHISTO').AsFloat  - zTabHisto.FieldByName('DEVOLHISTO').AsFloat
  - zTabHisto.FieldByName('DEVUELTOS').AsFloat   - zTabHisto.FieldByName('MERMAHISTO').AsFloat
  - zTabHisto.FieldByName('MERMA').AsFloat;

  if (zTabHisto.FieldByName('VENDIDOS').AsFloat    < 0) and
     (zTabHisto.FieldByName('CANTIENALBA').AsFloat > 0) then
  begin
     zTabHisto.FieldByName('CANTIENALBA').AsFloat :=
     zTabHisto.FieldByName('CANTIENALBA').AsFloat + zTabHisto.FieldByName('VENDIDOS').AsFloat;
     zTabHisto.FieldByName('VENDIDOS').AsFloat := 0;
     if zTabHisto.FieldByName('CANTIENALBA').AsFloat < 0 then
     begin
     zTabHisto.FieldByName('VENDIDOS').AsFloat := zTabHisto.FieldByName('CANTIENALBA').AsFloat;
     zTabHisto.FieldByName('CANTIENALBA').AsFloat := 0;
     end;
  end;

end;

procedure TDMDevolucion.RutAbrirHisArtiCabe1;
begin
//nueva rutina, copia de RutAbrirCabe1
   with sqlHisArtiCabe1 do
  begin
    Close;
    SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE', Uppercase(SQL.Text))-1)
                  + 'WHERE IDSTOCABE =  ' + IntToStr(DMDevolucion.sqlCabeSIDSTOCABE.AsInteger)
                  + ' Order by IDSTOCABE ' ;
    Open;
  end;
end;

procedure TDMDevolucion.RutCambiarPaquete(vNum : Integer);
begin
  if DMDevolucion.sqlCabe1.RecordCount <> 0 then
  begin
    DMDevolucion.sqlCabe1.Edit;
    DMDevolucion.sqlCabe1PAQUETE.AsInteger := vNum;
    DMDevolucion.sqlCabe1.Post;

    DMDevolucion.sqlCabe1.Refresh;
    DMDevolucion.RutScrollCabe1;
  end;
end;

procedure TDMDevolucion.RutAbrirAlbaran(vIDCabe,vTipoDoc : String; swDevol : Integer; vDesde,vHasta : TDate; vFiltroFecha : Boolean);
//a vIDCabe deben llegar '= IDSTOCABE' o '> 0' segun sea una consulta para tipo 2 o 22
var
  vWhere, vStringFecha : string;
begin
  vStringFecha := ' ';
  if vFiltroFecha then
    vStringFecha := ' AND FC.FECHA BETWEEN ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vDesde)) + ' AND ' + QuotedStr(FormatDateTime('mm/dd/yyyy', vHasta));

  vWhere := ' > ' + IntToStr(swDevol);
  if (swDevol = 1) or (swDevol = 3) then vWhere := ' = ' + IntToStr(swDevol);

   with sqlCabe1 do
  begin
    Close;
    sqlCabe1.SQL.Text :=  Copy(sqlCabe1.SQL.Text,1,
                      pos('WHERE FC.IDSTOCABE', Uppercase(SQL.Text))-1)
                  + 'WHERE FC.IDSTOCABE ' + vIDCabe
                  + ' AND (FC.SWTIPODOCU ' + vTipoDoc + ')'
                  + vStringFecha
                  + ' AND FC.SWDEVOLUCION ' + vWhere
                  + ' Order by FC.NOMBRE ' ;
    Open;
  end;

  with sqlHisArtiCabe1 do
  begin
    Close;
    SQL.Text := sqlCabe1.SQL.Text;
    Open;
  end;

end;

procedure TDMDevolucion.sqlCabe1AfterScroll(DataSet: TDataSet);
begin
  RutScrollCabe1;
end;


procedure TDMDevolucion.RutScrollCabe1;
begin
  if sqlCabe1.RecordCount <> 0 then
  begin
    RutAbrirLinea(sqlCabe1ID_PROVEEDOR.AsInteger);
    RutAbrirLineaTotal(sqlCabe1IDSTOCABE.AsInteger);

  end else
  begin
    RutAbrirLinea(0);
    RutAbrirLineaTotal(0);

  end;

  RutComprobarPaquete;
end;

procedure TDMDevolucion.sqlLineaCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('PrecioTotal').AsFloat :=
  DataSet.FieldByName('PrecioVenta').AsFloat +
  DataSet.FieldByName('PrecioVenta2').AsFloat;
end;

procedure TDMDevolucion.RutAbrirLinea(vID_Proveedor : Integer);
var
  wSeleF, wSeleA, wSeleB, wSeleC, wOrden : String;
begin

  wSeleF := '  and (h.IDSTOCABE = ' + IntToStr(DMDevolucion.sqlCabe1IDSTOCABE.AsInteger) + ' ) ';

  wSeleA := '  and (H.SWCERRADO = 0 or H.SWCERRADO is null)  and  (A.swaltabaja = 0 or A.swaltabaja is null) ';  // and A.swactivado = 1  and A.swcontrolfecha = 1 ';
  wSeleB := ''; //  '  and A.periodicidad = 5   ' ;  //  Ull Periodicidar 5 = Diaris
  wSeleC := '  and ( H.clave = ' + '''' + '54' + '''' + ' ) ';   //  Clau 54 Devolucions

  wOrden := ' Order by H.ID_PROVEEDOR,A.DESCRIPCION,A.BARRAS,H.ADENDUM ';

  with DMDevolucion.sqlLinea do
  begin
    Close;
    sqlLinea.SQL.Text := Copy(SQL.Text,1,
                pos('WHERE H.ID_HISARTI', Uppercase(SQL.Text))-1)
                + 'WHERE H.ID_HISARTI is not null And H.ID_PROVEEDOR = '  + IntToStr(vID_Proveedor)
                + ' AND H.PAQUETE = ' + IntToStr(DMDevolucion.sqlCabe1PAQUETE.AsInteger)
                + wSeleF
                + wSeleA
                + wSeleB
                + wSeleC
                + wOrden;
    Open;
    if FormDevolLista.lbNumeroArti.Caption <> '' then Locate('ID_ARTICULO',FormDevolLista.lbNumeroArti.Caption,[]);
  end;

end;

procedure TDMDevolucion.RutLocateLinea(vID:Integer);
begin
 if sqlLinea.RecordCount > 0 then

  sqlLinea.Locate('ID_HISARTI', vID, [])
  else

end;

procedure TDMDevolucion.RutLocateCabe(vID:Integer);
begin

  sqlCabe1.Locate('ID_PROVEEDOR', vID, []);

end;


procedure TDMDevolucion.RutAbrirLineaTotal(vID_STOCABE : Integer);
begin



  with DMDevolucion.sqlLineaTotal do
  begin
    Close;
    sqlLineaTotal.SQL.Text := Copy(SQL.Text,1,
                pos('WHERE IDSTOCABE', Uppercase(SQL.Text))-1)
                + 'WHERE IDSTOCABE = ' + IntToStr(vID_STOCABE)
                + ' AND PAQUETE = ' + IntToStr(DMDevolucion.sqlCabe1PAQUETE.AsInteger);
    Open();
  end;

end;


procedure TDMDevolucion.RutComprobarPaquete;
begin


  if sqlCabe1.RecordCount > 0 then
  begin
    if sqlLineaTotalTOTAL.AsInteger > DMDevolucion.sqlCabe1MAXPAQUETE.AsInteger then
    begin
      FormDevolFicha.lbPaquetes.Visible := true;
      FormDevolFicha.lbPaquetes.Caption := 'Ha sobrepasado el limite de paquetes';
    end else FormDevolFicha.lbPaquetes.Visible := false;
  end;
end;


procedure TDMDevolucion.sqlProveSAfterScroll(DataSet: TDataSet);
begin
//no pude colocar aqui la funcion por que siempre pasaba por el after scroll y daba error y opte por hacerla funcion y llamarla en el onCloseUp del lookCombo
 { if DMDevolucion.sqlCabeNuevo.State = dsEdit then
  begin
    DMDevolucion.sqlCabeNuevoMAXPAQUETE.AsInteger   := DMDevolucion.sqlProveSMAXPAQUETE.AsInteger;

    DMDevolucion.sqlCabeNuevoID_PROVEEDOR.AsInteger := DMDevolucion.sqlProveSID_PROVEEDOR.AsInteger;
    DMDevolucion.sqlCabeNuevoMAXPAQUETE.AsInteger   := DMDevolucion.sqlProveSMAXPAQUETE.AsInteger;
    DMDevolucion.sqlCabeNuevoNIF.AsInteger          := DMDevolucion.sqlProveSNIF.AsInteger;

  end;}
end;

procedure TDMDevolucion.RutGrabarProvee;
begin
    DMDevolucion.sqlCabeNuevoMAXPAQUETE.AsInteger   := DMDevolucion.sqlProveSMAXPAQUETE.AsInteger;
    DMDevolucion.sqlCabeNuevoID_PROVEEDOR.AsInteger := DMDevolucion.sqlProveSID_PROVEEDOR.AsInteger;
    DMDevolucion.sqlCabeNuevoNIF.AsString           := DMDevolucion.sqlProveSNIF.AsString;

    if (DMDevolucion.sqlCabeNuevoPAQUETE.AsInteger = null) or (DMDevolucion.sqlCabeNuevoPAQUETE.AsInteger = 0) then
       DMDevolucion.sqlCabeNuevoPAQUETE.AsInteger:= 1;//para iniciar paquete a 1 en caso de no tener nada
end;

procedure TDMDevolucion.RutNuevaCabe(vAltaDistri : Integer);
begin
 DMDevolucion.sqlCabeNuevo.Append;

  DMDevolucion.SP_GEN_STOCABE.ExecProc;
  DMDevolucion.sqlCabeNuevoIDSTOCABE.AsInteger    := DMDevolucion.SP_GEN_STOCABE.Params[0].Value;

  //DMDevolucion.sqlCabeNuevoSWTIPODOCU.AsInteger   := 22;
  DMDevolucion.sqlCabeNuevoSWDEVOLUCION.AsInteger := 1;
  if vAltaDistri = 1 then
  begin
    DMDevolucion.sqlCabeNuevoID_PROVEEDOR.AsInteger := DMppal.sqlProveedor2.FieldByName('ID_PROVEEDOR').AsInteger;
    DMDevolucion.sqlCabeNuevoNOMBRE.AsString        := DMppal.sqlProveedor2.fieldByname('NOMBRE').asString;
    DMDevolucion.sqlCabeNuevoMAXPAQUETE.AsString    := DMppal.sqlProveedor2.fieldByname('MAXPAQUETE').asString;
  end;
  DMDevolucion.sqlCabeNuevoFECHA.AsDateTime       := now;

//  DMDevolucion.sqlCabeNuevo.Post;
end;

procedure TDMDevolucion.RutPonerCampos;
begin
  DMDevolucion.sqlCabe1.edit;
  DMDevolucion.sqlCabe1ID_PROVEEDOR.AsInteger := dmppal.sqlProveedor2.FieldByName('ID_PROVEEDOR').AsInteger;
  DMDevolucion.sqlCabe1NOMBRE.AsString        := DMppal.sqlProveedor2.fieldByname('NOMBRE').asString;
  DMDevolucion.sqlCabe1MAXPAQUETE.AsString    := DMppal.sqlProveedor2.fieldByname('MAXPAQUETE').asString;
  DMDevolucion.sqlCabe1.Post;

end;


procedure TDMDevolucion.GrabarCabeNuevo(vTipo : Integer);
begin
 if DMDevolucion.sqlCabeNuevo.State <> dsBrowse then
  begin
    //if sqlCabeNuevo.State <> dsEdit then abort;
    DMDevolucion.sqlCabeNuevo.Edit;
    DMDevolucion.sqlCabeNuevoSWTIPODOCU.AsInteger := vTipo;
    DMDevolucion.sqlCabeNuevo.Post;

    if FormDevolMenu.swDevoAlbaran <> 0 then DMDevolucion.sqlCabe1.Refresh;

  end;

  if DMDevolucion.sqlProveS.State = dsEdit then
  begin
    DMDevolucion.sqlProveS.Post;
  end;

  sqlCabeS.Refresh;
end;

procedure TDMDevolucion.CancelarCabeNuevo;
begin
  if DMDevolucion.sqlCabeNuevo.State <> dsBrowse then DMDevolucion.sqlCabeNuevo.Cancel;
end;



procedure TDMDevolucion.RutCalcularCamposInforme;
begin

  with DMDevolucion.sqlHisto do
  begin
    close;
    SQL.Text := 'select count(distinct(paquete)) as TOTAL from FVHISARTI where IDDEVOLUCABE = '
                + DMDevolucion.sqlCabe1IDSTOCABE.AsString
                + ' and CLAVE = 54 ';
    Open;
  end;

  DMDevolucion.sqlCabe1.edit;
  DMDevolucion.sqlCabe1TextoPaquete.AsString := 'Total Paquetes = ' + DMDevolucion.sqlCabe1TOTALPAQUETES.AsString; // ' '; //Total Paquetes : ' + IntToStr(DMSDevoluA.sqlHisto.recordcount);
  DMDevolucion.sqlCabe1TotalDevueltos.AsFloat := DMDevolucion.sqlLineaTotalTOTAL.AsFloat;
  DMDevolucion.sqlCabe1.Post;

  DMppal.vg_Empresa := 1;
  DMDevolucion.sqlEMPRESA.Close;
  DMDevolucion.sqlEMPRESA.Params.ParamByName('NREFERENCIA').AsInteger := DMppal.vg_Empresa;
  DMDevolucion.sqlEMPRESA.Open;
end;



Procedure TDMDevolucion.RutBorrarDevolucion;
begin
//TODO hacer que devuelva un boolean para mostrar mensaje de que tiene registros y no se puede borrar

  sqlCabe1.Delete;
  sqlCabe1.Refresh;
  //actualizar lista
  sqlCabeS.Refresh;
end;

procedure TDMDevolucion.RutAbrirCerrarDevol(vIDCabe,swDevol : Integer);
begin
  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabListaDevolucion then
  begin
    sqlCabeS.Edit;
    sqlCabeSSWDEVOLUCION.AsInteger := swDevol;
    if swDevol = 3 then sqlCabe1SWTIPODOCU.AsInteger := 2;

    sqlCabeS.Post;
//    sqlCabeS.Refresh;
  end;

  if FormDevolMenu.pcDetalle.ActivePage = FormDevolMenu.tabAlbaranDevolucion then
  begin
    sqlCabe1.Edit;
    sqlCabe1SWDEVOLUCION.AsInteger := swDevol;
    if swDevol = 3 then sqlCabe1SWTIPODOCU.AsInteger := 2;

    sqlCabe1.Post;
    sqlCabe1.Refresh;

    //FormDevolMenu.RutAbrirLista;//cuando cerramos una devol, como es individual volvemos a la lista
    FormDevolLista.RutAlbaranClick(' = ' + IntToStr(vIDCabe),swDevol);

    if swDevol = 1 then
    begin
      FormDevolFicha.pnlCBarras.enabled       := True;
      FormDevolFicha.GridPreSeleccio.ReadOnly := False;
    end;
    if swDevol = 3 then
    begin
      FormDevolFicha.pnlCBarras.enabled       := False;
      FormDevolFicha.GridPreSeleccio.ReadOnly := True;
    end;
  end;
  sqlCabeS.Refresh;
end;


procedure TDMDevolucion.RutLeerCodigoBarras;
var
  swNoExiste, swPrecios, swAlta: Boolean;
begin


  DMPpal.vBarres  :=  DMPpal.vg_Barres;
  DMPpal.vAdendum :=  DMPpal.vg_Adendum;
  if swRepetido = false then// si sw repetido es igual a false pues lees sino no haces nada
  DMppal.RutLeerArticulos
  else;

  //Si no existeix en la BD local i si en la General copiem el registre
  swAlta := false;
  if (DMPpal.sqlArti1.RecordCount = 0) and (DMPpal.sqlArti0.RecordCount > 0) then
  begin
    with DMppal.sqlArti0 do
    begin
      First;
      while not eof do
      begin
        DMPpal.sqlArti1.Append;
        CopyRecordDataset(DMPpal.sqlArti0, DMPpal.sqlArti1, 0);


        DMPpal.sqlArti1.Post;

        Next;
      end;

    end;
    swAlta := true;
  end;

 //--------- Comparem preus i actualitzem ------------//
 if (DMppal.sqlArti1.RecordCount > 0) and (DMppal.sqlArti0.RecordCount > 0) then
  begin
    if DMppal.sqlArti1.FieldByName('PRECIO1').AsFloat <> DMppal.sqlArti0.FieldByName('PRECIO1').AsFloat then
    begin
      with DMppal.sqlArti1 do
      begin
        First;
        while not eof do
        begin
          Edit;
          FieldByName('PRECIO1').AsFloat := DMppal.sqlArti0.FieldByName('PRECIO1').AsFloat;
          Post;
          Next;
        end;
      end;
      swPrecios := true;
    end else swPrecios := false;
  end;

  if (swRepetido = true) or (DMppal.sqlArti0.RecordCount <= 1) then //if el recordcount es igual o mas peque�o que 1 sigue sino abre el form
  begin
   swNoExiste := false;


   DMPpal.sqlHisArtiDev1.Close;
   DMPpal.sqlHisArtiDev1.SQL.Text := ' select * '
                + ' from FVHISARTI H'
                + ' left join fmarticulo A on (A.ID_ARTICULO = H.ID_ARTICULO)'
                + ' where H.id_articulo = ' + IntToStr( DMPpal.sqlArti1ID_ARTICULO.AsInteger)   //cambiat el arti1 per arti0 (probes)
                + ' and A.BARRAS  =  '      + QuotedStr( DMPpal.vBarres)
                + ' and H.Adendum =  '      + QuotedStr( DMPpal.vAdendum)
                + ' and H.clave   = 54';
   DMPpal.sqlHisArtiDev1.Open();


    //Si existeix a la devolucio suma 1 a la quantitat i a devueltos
    if DMPpal.sqlHisArtiDev1.RecordCount > 0 then
    begin
      if RutExisteProveedor( DMPpal.sqlHisArtiDev1, 'ID_PROVEEDOR') then
      begin

        if sqllinea.Locate('IDSTOCABE; ID_ARTICULO; ADENDUM; PAQUETE',
                                        VarArrayOf([

                                        sqlCabe1IDSTOCABE.AsInteger,
                                        DMPpal.sqlHisArtiDev1.FieldByName('ID_ARTICULO').AsInteger,
                                        DMPpal.vg_Adendum,
                                        sqlCabe1PAQUETE.AsInteger  ]),
                                        []) then



        begin
          sqlLinea.Edit;
          sqlLinea.FieldByName('CANTIDAD').AsInteger  := sqlLinea.FieldByName('CANTIDAD').AsInteger + 1;
          sqlLinea.FieldByName('DEVUELTOS').AsInteger := sqlLinea.FieldByName('DEVUELTOS').AsInteger + 1;
          FormDevolFicha.RutMensajeArticulo(1);
        end else
        begin
          sqlLinea.Append;
          DMPpal.Sp_gen_Hisarti1.execproc;
          sqlLinea.FieldByName('ID_HISARTI').AsInteger := DMPpal.SP_GEN_HISARTI1.Params[0].Value;

          DMPpal.RutPasarCampos(DMPpal.sqlHisArtiDev1, sqlLinea, 2, DMPpal.sqlHisArtiDev1.FieldByName('ID_PROVEEDOR').AsInteger);


          sqlLinea.FieldByName('IDSTOCABE').AsInteger := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
          sqlLinea.FieldByName('ADENDUM').AsString := DMPpal.vg_adendum;
          sqlLinea.FieldByName('PAQUETE').AsInteger := sqlCabe1PAQUETE.AsInteger;


          DMPpal.RutCalculaImportes(sqlLinea, 'CANTIDAD', -2, 0);

          if swAlta then FormDevolFicha.RutMensajeArticulo(10)
          else FormDevolFicha.RutMensajeArticulo(1);

          if swPrecios then FormDevolFicha.RutMensajeArticulo(9)
          else if swAlta = false then FormDevolFicha.RutMensajeArticulo(1);




        end;

        sqlLinea.Post;
        swNoExiste := false;
        swRepetido := false;
        exit;
      end else
      begin
        DMPpal.vID := DMPpal.sqlHisArtiDev1.FieldByName('ID_PROVEEDOR').asInteger;
        DMPpal.swCrear := true;
        FormDevolFicha.RutMensajeDistri;
        swNoExiste := false;
      end;
    end else  swNoExiste := true;


    if swNoExiste = true then
    begin


      DMPpal.sqlHisArtiCom1.Close;
      DMPpal.sqlHisArtiCom1.SQL.Text := ' select *'
                + ' from FVHISARTI H'
                + ' left join fmarticulo A on (A.ID_ARTICULO = H.ID_ARTICULO)'
                + ' where H.id_articulo = ' + IntToStr(DMPpal.sqlArti1ID_ARTICULO.AsInteger) //cambiat el arti1 per arti0 (probes)
                + ' and A.BARRAS = '        + QuotedStr(DMPpal.vBarres)
                + ' and H.Adendum =  '      + QuotedStr(DMPpal.vAdendum)
                + ' and H.clave = 01 ';

      DMPpal.sqlHisArtiCom1.Open();
      //Mirem si existeix a compres i si existeix creem el registre a devolucions
      if DMPpal.sqlHisArtiCom1.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlHisArtiCom1, 'ID_PROVEEDOR') then
        begin

          if sqlLinea.Locate('IDSTOCABE; ID_ARTICULO; ADENDUM; PAQUETE',

                                        VarArrayOf([
                                        sqlCabe1IDSTOCABE.AsInteger,
                                        DMPpal.sqlHisArtiCom1.FieldByName('ID_ARTICULO').AsInteger,
                                        DMPpal.vg_Adendum,
                                        sqlCabe1PAQUETE.AsInteger]),

                                        []) then
          begin
            sqlLinea.Edit;
            sqlLinea.FieldByName('CANTIDAD').AsInteger   := sqlLinea.FieldByName('CANTIDAD').AsInteger + 1;
            sqlLinea.FieldByName('DEVUELTOS').AsInteger  := sqlLinea.FieldByName('DEVUELTOS').AsInteger + 1;
            FormDevolFicha.RutMensajeArticulo(1);
          end else
          begin
            sqlLinea.Append;
            DMPpal.Sp_gen_Hisarti1.execproc;
            sqlLinea.FieldByName('ID_HISARTI').AsInteger := DMPpal.SP_GEN_HISARTI1.Params[0].Value;

            DMPpal.RutPasarCampos(DMPpal.sqlHisArtiCom1, sqlLinea, 2,DMPpal.sqlHisArtiCom1.FieldByName('ID_PROVEEDOR').AsInteger);


            sqlLinea.FieldByName('IDSTOCABE').AsInteger := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlLinea.FieldByName('ADENDUM').AsString  := DMPpal.vg_adendum;
            sqlLinea.FieldByName('PAQUETE').AsInteger := sqlCabe1PAQUETE.AsInteger;


            DMPpal.RutCalculaImportes(sqlLinea, 'CANTIDAD', -2, 0);


            if swAlta then FormDevolFicha.RutMensajeArticulo(10)
            else FormDevolFicha.RutMensajeArticulo(1);

            if swPrecios then FormDevolFicha.RutMensajeArticulo(9)
            else if swAlta = false then FormDevolFicha.RutMensajeArticulo(1);


          end;

          sqlLinea.Post;
          swNoExiste := false;
          swRepetido := false;
          exit;
        end else
        begin
          DMPpal.vID := DMPpal.sqlHisArtiCom1.FieldByName('ID_PROVEEDOR').asInteger;
          FormDevolFicha.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;
      end else  swNoExiste := true;
    end;


    if swNoExiste = true then
    begin

      DMPpal.sqlHisArtiCom0.Close;
      DMPpal.sqlHisArtiCom0.SQL.Text := ' select *'
              + ' from FVHISARTI H'
              + ' left join fmarticulo A on (A.ID_ARTICULO = H.ID_ARTICULO)'
              + ' where H.id_articulo = ' + IntToStr(DMPpal.sqlArti0ID_ARTICULO.AsInteger)
              + ' and A.BARRAS = '        + QuotedStr(DMPpal.vBarres)
              + ' and H.Adendum =  '      + QuotedStr(DMPpal.vAdendum)
              + ' and H.clave = 01 ';

      DMPpal.sqlHisArtiCom0.Open();
        //Si no existeix mirem a la BD general si existeix la compra i si existeix creem registre de devoluci�
      if DMPpal.sqlHisArtiCom0.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlHisArtiCom0, 'ID_PROVEEDOR') then
        begin

          if sqlLinea.Locate('IDSTOCABE; ID_ARTICULO; ADENDUM; PAQUETE',

                                        VarArrayOf([

                                        sqlCabe1IDSTOCABE.AsInteger,
                                        DMPpal.sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
                                        DMPpal.vg_Adendum,
                                        sqlCabe1PAQUETE.AsInteger  ]),

                                        []) then
          begin
            sqlLinea.Edit;
            sqlLinea.FieldByName('CANTIDAD').AsInteger   := sqlLinea.FieldByName('CANTIDAD').AsInteger + 1;
            sqlLinea.FieldByName('DEVUELTOS').AsInteger  := sqlLinea.FieldByName('DEVUELTOS').AsInteger + 1;
            FormDevolFicha.RutMensajeArticulo(1);

          end else
          begin
            sqlLinea.Append;
            DMPpal.sp_gen_Hisarti1.execproc;
            sqlLinea.FieldByName('ID_HISARTI').AsInteger := DMPpal.SP_GEN_HISARTI1.Params[0].Value;

            DMPpal.RutPasarCampos(DMPpal.sqlHisArtiCom0, sqlLinea, 2, DMPpal.sqlHisArtiCom0.FieldByName('ID_PROVEEDOR').AsInteger);


            sqlLinea.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlLinea.FieldByName('ADENDUM').AsString     := DMPpal.vg_adendum;
            sqlLinea.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;


            DMPpal.RutCalculaImportes(sqlLinea, 'CANTIDAD', -2, 0);


            if swAlta then FormDevolFicha.RutMensajeArticulo(10)
            else FormDevolFicha.RutMensajeArticulo(1);

            if swPrecios then FormDevolFicha.RutMensajeArticulo(9)
            else if swAlta = false then FormDevolFicha.RutMensajeArticulo(1);


          end;
            DMDevolucion.sqllinea.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          DMPpal.vID := DMPpal.sqlHisArtiCom0.FieldByName('ID_PROVEEDOR').asInteger;
          FormDevolFicha.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;
      end else  swNoExiste := true;
    end;


    if swNoExiste = true then
    begin
      if DMPpal.sqlArti1.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlArti1, 'EDITORIAL') then
        begin


            sqlLinea.Append;
            DMPpal.sp_gen_Hisarti1.execproc;
            sqlLinea.FieldByName('ID_HISARTI').AsInteger := DMPpal.SP_GEN_HISARTI1.Params[0].Value;

            DMPpal.RutPasarCampos(DMPpal.sqlArti1, sqlLinea, 1, DMPpal.sqlArti1EDITORIAL.AsInteger);
            sqlLinea.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlLinea.FieldByName('ADENDUM').AsString     := DMPpal.vg_adendum;
            sqlLinea.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;
            DMPpal.RutCalculaImportes(sqlLinea, 'CANTIDAD', -2, 0);

            if swAlta then FormDevolFicha.RutMensajeArticulo(10)
            else FormDevolFicha.RutMensajeArticulo(1);


            if swPrecios then FormDevolFicha.RutMensajeArticulo(9)
            else if swAlta = false then FormDevolFicha.RutMensajeArticulo(1);



            sqlLinea.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          DMPpal.vID := DMPpal.sqlArti1.FieldByName('EDITORIAL').asInteger;
          FormDevolFicha.RutMensajeDistri;
          DMPpal.swCrear := true;
          swNoExiste := false;
        end;
      end else  swNoExiste := true;

    end;

    if swNoExiste = true then
    begin
      if DMPpal.sqlArti0.RecordCount > 0 then
      begin
        if RutExisteProveedor(DMPpal.sqlArti0, 'EDITORIAL') then
        begin


            sqlLinea.Append;
            DMPpal.sp_gen_Hisarti1.execproc;
            sqllinea.FieldByName('ID_HISARTI').AsInteger :=  DMPpal.SP_GEN_HISARTI1.Params[0].Value;

            DMPpal.RutPasarCampos( DMPpal.sqlArti0, sqlLinea, 1,  DMPpal.sqlArti0EDITORIAL.AsInteger);
            sqlLinea.FieldByName('IDSTOCABE').AsInteger  := sqlCabe1IDSTOCABE.AsInteger;
//                                        sqlHisArtiCom0.FieldByName('ID_ARTICULO').AsInteger,
            sqlLinea.FieldByName('ADENDUM').AsString     := DMPpal.vg_adendum;
            sqlLinea.FieldByName('PAQUETE').AsInteger    := sqlCabe1PAQUETE.AsInteger;
            DMPpal.RutCalculaImportes(sqlLinea, 'CANTIDAD', -2, 0);

            if swAlta then FormDevolFicha.RutMensajeArticulo(10)
            else FormDevolFicha.RutMensajeArticulo(1);


            if swPrecios then FormDevolFicha.RutMensajeArticulo(9)
            else if swAlta = false then FormDevolFicha.RutMensajeArticulo(1);



            sqlLinea.Post;
            swNoExiste := false;
            swRepetido := false;
            exit;
        end else
        begin
          DMPpal.vID :=  DMPpal.sqlArti0.FieldByName('EDITORIAL').asInteger;
          DMPpal.swCrear := true;
          FormDevolFicha.RutMensajeDistri;
          swNoExiste := false;
        end;

      end else  swNoExiste := true;
    end;

    if swNoExiste = true then FormDevolFicha.RutMensajeArticulo(5);

  end else
  begin
    swRepetido := true;
    FormEscogerArti.gridUsuarios.DataSource := FormEscogerArti.dsArticulo;
    FormEscogerArti.lbBarras.Caption := 'Seleccione uno';
    FormEscogerArti.ShowModal();
  end;
end;

function TDMDevolucion.RutExisteProveedor(vTabla : TFDQuery; vCampo : String):Boolean;
begin

  if DMDevolucion.sqlHisArtiCabe1.Locate('ID_PROVEEDOR', vTabla.FieldByName(vCampo).AsInteger,[]) then
  begin
    Result := true;
    DMDevolucion.sqlCabe1.Locate('IDSTOCABE', DMDevolucion.sqlHisArtiCabe1IDSTOCABE.AsInteger, []);
  end else
  begin
    Result := false;

  end;

end;


procedure TDMDevolucion.RutAbrirPaquetes(vIdProve, vIdCabe : Integer);
begin
  with sqlPaquetes do
  begin
    Close;
    sqlPaquetes.SQL.Text :=  Copy(SQL.Text,1,
                      pos('WHERE H.IDSTOCABE', Uppercase(SQL.Text))-1)
                  + 'WHERE H.IDSTOCABE = '   + IntToStr(vIdCabe)
                  + ' AND H.ID_PROVEEDOR = ' + IntToStr(vIdProve)
                  + ' AND H.CLAVE = '        + QuotedStr('54')
                  + ' GROUP BY H.IDSTOCABE, H.PAQUETE, P.NOMBRE'
                  + ' Order by 1 ' ;
    Open;
  end;
end;

procedure TDMDevolucion.DataModuleCreate(Sender: TObject);
begin
  DMppal.RutCrearSortingSQL(sqlCabeS);
//  DMppal.RutCrearSortingSQL(sqlCabe1);
//  DMppal.RutCrearSortingSQL(sqlLinea);
end;

initialization
  RegisterModuleClass(TDMDevolucion);


end.


