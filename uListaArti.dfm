object FormListaArti: TFormListaArti
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 580
  ClientWidth = 1054
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1054
    Height = 56
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object btFicha: TUniBitBtn
      AlignWithMargins = True
      Left = 5
      Top = 6
      Width = 50
      Height = 45
      Hint = ''
      Margins.Bottom = 0
      ShowHint = True
      Caption = ''
      TabOrder = 1
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 13
      OnClick = btFichaClick
    end
    object btConsulta: TUniBitBtn
      AlignWithMargins = True
      Left = 62
      Top = 6
      Width = 53
      Height = 45
      Hint = 'Consultas'
      Margins.Bottom = 0
      ShowHint = True
      Caption = ''
      TabOrder = 2
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 12
      OnClick = btConsultaClick
    end
    object edBuscar: TUniEdit
      Left = 302
      Top = 29
      Width = 276
      Hint = ''
      ShowHint = True
      Text = ''
      TabOrder = 3
      OnChange = edBuscarChange
      OnKeyUp = edBuscarKeyUp
    end
    object UniLabel9: TUniLabel
      Left = 264
      Top = 32
      Width = 32
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Buscar'
      TabOrder = 4
    end
    object btNuevoArticulo: TUniBitBtn
      Left = 183
      Top = 6
      Width = 55
      Height = 45
      Hint = 'Nuevo'
      ShowHint = True
      Glyph.Data = {
        16020000424D160200000000000076000000280000001A0000001A0000000100
        040000000000A001000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777700000077777777777777777777777777000000777777777777
        77777777777777000000777777777778888877777777770000007777777777FA
        AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
        AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
        AAAA87777777770000007777777777FAAAAA877777777700000077778888888A
        AAAA8888888777000000777FAAAAAAAAAAAAAAAAAAA877000000777FAAAAAAAA
        AAAAAAAAAAA877000000777FAAAAAAAAAAAAAAAAAAA877000000777FAAAAAAAA
        AAAAAAAAAAA8770000007777FFFFFFFAAAAA8FFFFFF7770000007777777777FA
        AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
        AAAA87777777770000007777777777FAAAAA87777777770000007777777777FA
        AAAA87777777770000007777777777FAAAAA877777777700000077777777777F
        FFFF777777777700000077777777777777777777777777000000777777777777
        7777777777777700000077777777777777777777777777000000}
      Caption = ''
      TabOrder = 5
      OnClick = btNuevoArticuloClick
    end
    object btHistorico: TUniBitBtn
      AlignWithMargins = True
      Left = 121
      Top = 6
      Width = 53
      Height = 45
      Hint = 'Historico'
      Margins.Bottom = 0
      ShowHint = True
      Caption = ''
      TabOrder = 6
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 33
      OnClick = btHistoricoClick
    end
  end
  object GridListaArti: TUniDBGrid
    Left = 0
    Top = 56
    Width = 1054
    Height = 524
    Hint = ''
    ShowHint = True
    DataSource = dsListaArticulo
    ReadOnly = True
    WebOptions.PageSize = 20
    WebOptions.FetchAll = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Height = -13
    ParentFont = False
    TabOrder = 1
    OnColumnSort = GridListaArtiColumnSort
    OnDblClick = GridListaArtiDblClick
    Columns = <
      item
        FieldName = 'ID_ARTICULO'
        Title.Caption = 'ID'
        Width = 64
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'BARRAS'
        Title.Caption = 'C.Barras'
        Width = 113
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'ADENDUM'
        Title.Caption = 'Num'
        Width = 53
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'DESCRIPCION'
        Title.Caption = 'Descripcion'
        Width = 258
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'PRECIO1'
        Title.Caption = 'PVP'
        Width = 52
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'PRECIO2'
        Title.Caption = 'PVP2'
        Width = 52
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'PRECIOCOSTE'
        Title.Caption = 'Pr.Coste'
        Width = 63
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'PRECIOCOSTETOT'
        Title.Caption = 'Pr.Coste + RE'
        Width = 91
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'REFEPROVE'
        Title.Caption = 'RefeProve'
        Width = 89
        Font.Height = -13
        Sortable = True
      end>
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsListaArticulo: TDataSource
    DataSet = DMMenuArti.sqlListaArticulo
    Left = 952
    Top = 368
  end
end
