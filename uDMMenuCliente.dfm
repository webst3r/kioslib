object DMMenuCliente: TDMMenuCliente
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 653
  Width = 978
  object sqlCabe: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * From FMCLIENTES'
      'where ID_CLIENTE is not null'
      'Order by ID_CLIENTE')
    Left = 40
    Top = 112
    object sqlCabeID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlCabeNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlCabeDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeDIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlCabePOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabeCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabePROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlCabeCONTACTO1NOMBRE: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlCabeCONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlCabeCONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlCabeTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlCabeEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlCabeWEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlCabeMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlCabeBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object sqlCabeAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object sqlCabeDC: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object sqlCabeCUENTABANCO: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object sqlCabeTDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object sqlCabeTIVACLI: TSmallintField
      FieldName = 'TIVACLI'
      Origin = 'TIVACLI'
    end
    object sqlCabeTPAGO: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object sqlCabeDIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlCabeDIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlCabeDIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlCabeSWBLOQUEO: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object sqlCabeSWTFACTURACION: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object sqlCabeSWAGRUARTIFRA: TSmallintField
      FieldName = 'SWAGRUARTIFRA'
      Origin = 'SWAGRUARTIFRA'
    end
    object sqlCabeREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlCabeTRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlCabeSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCabeFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCabeFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCabeHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCabeFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCabeHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCabeUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCabeNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 127
    Top = 4
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Dades\Trial\Dat\FAPVP.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    Connected = True
    LoginPrompt = False
    Left = 41
    Top = 4
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 210
    Top = 4
  end
  object sqlPoblacionS: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'Select A.CODIGO,A.POSTAL,A.POBLACION,A.CPROV,B.PROVINCIA,A.CPOST' +
        'AL,A.CPAIS'
      'from  FPOBLA A'
      'left outer join FPROVI B on (A.CPROV = B.CPROVI)'
      'Order by CPOSTAL,POBLACION')
    Left = 96
    Top = 120
    object sqlPoblacionSCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlPoblacionSPOSTAL: TIntegerField
      FieldName = 'POSTAL'
      Origin = 'POSTAL'
    end
    object sqlPoblacionSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 30
    end
    object sqlPoblacionSCPROV: TIntegerField
      FieldName = 'CPROV'
      Origin = 'CPROV'
    end
    object sqlPoblacionSPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object sqlPoblacionSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlPoblacionSCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      Size = 3
    end
  end
  object sqlTFacturacio: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select TTIPO, NORDEN, DESCRIPCION from FTIPOSI'
      'where TTIPO = '#39'TFACTURACI'#39
      'Order by NORDEN')
    Left = 176
    Top = 120
    object sqlTFacturacioTTIPO: TStringField
      FieldName = 'TTIPO'
      Origin = 'TTIPO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object sqlTFacturacioNORDEN: TIntegerField
      FieldName = 'NORDEN'
      Origin = 'NORDEN'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTFacturacioDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
  end
  object FDQuery3: TFDQuery
    Connection = FDConnection1
    Left = 112
    Top = 328
  end
  object FDQuery4: TFDQuery
    Connection = FDConnection1
    Left = 104
    Top = 272
  end
  object sqlTPago: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'select ID_TPAGO, DESCRIPCION, TEFECTO, EFECTOS, FRECUENCIA, DIAS' +
        'PRIMERVTO from FTPAGO'
      'where ID_TPAGO is not null'
      'Order by ID_TPAGO')
    Left = 232
    Top = 128
    object sqlTPagoID_TPAGO: TIntegerField
      FieldName = 'ID_TPAGO'
      Origin = 'ID_TPAGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlTPagoDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 50
    end
    object sqlTPagoTEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlTPagoEFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlTPagoFRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlTPagoDIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
  end
  object FDQuery6: TFDQuery
    Connection = FDConnection1
    Left = 24
    Top = 312
  end
  object FDQuery7: TFDQuery
    Connection = FDConnection1
    Left = 32
    Top = 256
  end
  object sqlCabeS: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_CLIENTE,A.NOMBRE,A.DIRECCION,A.POBLACION,P.PROVINCI' +
        'A,A.CPOSTAL,A.CPROVINCIA,'
      '   A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL, A.FAX'
      'From FMCLIENTES A'
      ' left outer join'
      ' FPROVI P on (A.CPROVINCIA = P.CPROVI)'
      'where ID_CLIENTE is not null'
      'Order by ID_CLIENTE')
    Left = 32
    Top = 200
    object sqlCabeSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlCabeSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlCabeSPROVINCIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object sqlCabeSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlCabeSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlCabeSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlCabeSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlCabeSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlCabeSFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
  end
  object sqlUpdate: TFDQuery
    Connection = FDConnection1
    Left = 303
    Top = 4
  end
end
