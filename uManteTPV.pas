unit uManteTPV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormManteTPV = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    panelIzquierdo: TUniPanel;
    pcTpvVentas: TUniPageControl;
    tabTpvVenta: TUniTabSheet;
    UniPanel1: TUniPanel;
    btNuevaVenta: TUniBitBtn;
    btVerArticulo: TUniBitBtn;
    btBorrarArticulo: TUniBitBtn;
    btConsultaArticulos: TUniBitBtn;
    btModificar: TUniBitBtn;
    btAnularLinea: TUniBitBtn;
    lbStatus: TUniLabel;
    edArticulo: TUniDBEdit;
    btSinTarifas: TUniBitBtn;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    btTiquetsEmesos: TUniBitBtn;
    btReservas: TUniBitBtn;
    btVentaDown: TUniBitBtn;
    edVenta: TUniDBEdit;
    btVentaUp: TUniBitBtn;
    btVerVentas: TUniBitBtn;
    btVerMensajes: TUniBitBtn;
    UniLabel1: TUniLabel;
    UniPanel2: TUniPanel;
    panelPagar: TUniPanel;
    btEfectivo: TUniBitBtn;
    btTarjeta: TUniBitBtn;
    btAsignarCliente: TUniBitBtn;
    btAnularUltimaVenta: TUniBitBtn;
    btImprimir: TUniBitBtn;
    btSinImprimir: TUniBitBtn;
    sp1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniSpeedButton3: TUniSpeedButton;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton6: TUniSpeedButton;
    UniSpeedButton7: TUniSpeedButton;
    UniSpeedButton8: TUniSpeedButton;
    UniSpeedButton9: TUniSpeedButton;
    UniSpeedButton10: TUniSpeedButton;
    UniSpeedButton11: TUniSpeedButton;
    UniSpeedButton12: TUniSpeedButton;
    UniPanel3: TUniPanel;
    SpeedButton27: TUniSpeedButton;
    btFinTiket: TUniBitBtn;
    UniLabel2: TUniLabel;
    edImportTotal: TUniDBEdit;
    btFinVenta: TUniBitBtn;
    btAnulaVenta: TUniBitBtn;
    GridLineas: TUniDBGrid;
    procedure uni6_10Click(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure UniSpeedButton7Click(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

   swManteTpv : Integer;

  end;

function FormManteTPV: TFormManteTPV;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uVentasTPV, uDMManteTPV;

function FormManteTPV: TFormManteTPV;
begin
  Result := TFormManteTPV(DMppal.GetFormInstance(TFormManteTPV));

end;


procedure TFormManteTPV.uni6_10Click(Sender: TObject);
begin
  showmessage('hola');
end;

procedure TFormManteTPV.UniFormShow(Sender: TObject);
begin

  if swManteTpv = 0 then
  begin
    FormVentasTPV.Parent := tabTpvVenta;
    FormVentasTPV.Align := alClient;
    FormVentasTPV.Show();
    swManteTpv := 1;
  end;

end;

procedure TFormManteTPV.UniSpeedButton7Click(Sender: TObject);
begin
  if edImportTotal.Text = '1' then
     edImportTotal.Text := '2' else
     edImportTotal.Text := '1';
end;

end.
