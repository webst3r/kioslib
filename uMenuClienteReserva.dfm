object FormMenuClienteReserva: TFormMenuClienteReserva
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 615
  ClientWidth = 1100
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TUniPanel
    Left = 0
    Top = 0
    Width = 1100
    Height = 73
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object UniLabel1: TUniLabel
      Left = 3
      Top = 6
      Width = 47
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'N. Cliente'
      TabOrder = 1
    end
    object UniDBEdit1: TUniDBEdit
      Left = 3
      Top = 22
      Width = 78
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 2
    end
    object UniLabel13: TUniLabel
      Left = 80
      Top = 6
      Width = 29
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'N.I.F.'
      TabOrder = 3
    end
    object UniDBEdit15: TUniDBEdit
      Left = 80
      Top = 22
      Width = 218
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 4
    end
    object UniDBRadioGroup1: TUniDBRadioGroup
      Left = 304
      Top = 3
      Width = 137
      Height = 65
      Hint = ''
      ShowHint = True
      Caption = 'Ver Reservas de'
      TabOrder = 5
      Items.Strings = (
        'Cliente Actual'
        'Todos los Clientes')
      Values.Strings = (
        '0'
        '1')
    end
    object UniDBDateTimePicker1: TUniDBDateTimePicker
      Left = 483
      Top = 43
      Width = 94
      Hint = ''
      ShowHint = True
      DateTime = 43369.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 6
    end
    object UniDBDateTimePicker2: TUniDBDateTimePicker
      Left = 483
      Top = 22
      Width = 94
      Hint = ''
      ShowHint = True
      DateTime = 43369.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 7
    end
    object UniLabel2: TUniLabel
      Left = 483
      Top = 6
      Width = 29
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Fecha'
      TabOrder = 8
    end
    object UniLabel3: TUniLabel
      Left = 452
      Top = 22
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 9
    end
    object UniLabel4: TUniLabel
      Left = 452
      Top = 41
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 10
    end
    object UniDBNavigator1: TUniDBNavigator
      Left = 3
      Top = 45
      Width = 295
      Height = 25
      Hint = ''
      ShowHint = True
      TabOrder = 11
    end
  end
  object UniPageControl1: TUniPageControl
    Left = 0
    Top = 73
    Width = 1100
    Height = 542
    Hint = ''
    ShowHint = True
    ActivePage = tabNuevo
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ExplicitTop = 76
    ExplicitWidth = 289
    ExplicitHeight = 193
    object tabTablas: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabTablas'
      object UniDBGrid1: TUniDBGrid
        Left = 0
        Top = 195
        Width = 1092
        Height = 316
        Hint = ''
        ShowHint = True
        LoadMask.Message = 'Loading data...'
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
      end
      object UniDBGrid2: TUniDBGrid
        Left = 0
        Top = 0
        Width = 1092
        Height = 195
        Hint = ''
        ShowHint = True
        LoadMask.Message = 'Loading data...'
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
      end
    end
    object tabNuevo: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'tabNuevo'
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 1092
        Height = 265
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        object UniLabel5: TUniLabel
          Left = 60
          Top = 14
          Width = 45
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'C. Barras'
          TabOrder = 1
        end
        object UniDBEdit2: TUniDBEdit
          Left = 60
          Top = 30
          Width = 110
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 2
        end
        object UniLabel6: TUniLabel
          Left = 169
          Top = 14
          Width = 93
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Descripcion Articulo'
          TabOrder = 3
        end
        object UniDBEdit3: TUniDBEdit
          Left = 169
          Top = 30
          Width = 218
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 4
        end
        object UniLabel7: TUniLabel
          Left = 479
          Top = 14
          Width = 40
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Reserva'
          TabOrder = 5
        end
        object UniDBEdit4: TUniDBEdit
          Left = 479
          Top = 30
          Width = 46
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 6
        end
        object UniLabel8: TUniLabel
          Left = 386
          Top = 14
          Width = 43
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cantidad'
          TabOrder = 7
        end
        object UniDBEdit5: TUniDBEdit
          Left = 386
          Top = 30
          Width = 49
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 8
        end
        object UniLabel9: TUniLabel
          Left = 610
          Top = 14
          Width = 111
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ultima Fecha Generada'
          TabOrder = 9
        end
        object UniLabel10: TUniLabel
          Left = 443
          Top = 33
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ej. N'#170
          TabOrder = 10
        end
        object UniDBEdit8: TUniDBEdit
          Left = 526
          Top = 30
          Width = 71
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 11
        end
        object UniDBLookupComboBox1: TUniDBLookupComboBox
          Left = 610
          Top = 30
          Width = 145
          Hint = ''
          ShowHint = True
          ListFieldIndex = 0
          TabOrder = 12
          Color = clWindow
        end
        object UniDBRadioGroup2: TUniDBRadioGroup
          Left = 137
          Top = 80
          Width = 151
          Height = 155
          Hint = ''
          ShowHint = True
          Caption = 'Festivo'
          ParentFont = False
          Font.Color = clRed
          TabOrder = 13
          Items.Strings = (
            '0. Segun Dia Sem.'
            '1. Siempre'
            '2. Nunca')
          Values.Strings = (
            '0'
            '1'
            '2')
        end
        object chLunes: TUniCheckBox
          Left = 60
          Top = 80
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Lunes'
          TabOrder = 14
        end
        object chMartes: TUniCheckBox
          Left = 60
          Top = 103
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Martes'
          TabOrder = 15
        end
        object chMiercoles: TUniCheckBox
          Left = 60
          Top = 126
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Miercoles'
          TabOrder = 16
        end
        object chJueves: TUniCheckBox
          Left = 60
          Top = 149
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Jueves'
          TabOrder = 17
        end
        object chViernes: TUniCheckBox
          Left = 60
          Top = 172
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Viernes'
          TabOrder = 18
        end
        object chSabado: TUniCheckBox
          Left = 60
          Top = 195
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Sabado'
          TabOrder = 19
        end
        object chDomingo: TUniCheckBox
          Left = 60
          Top = 218
          Width = 67
          Height = 17
          Hint = ''
          ShowHint = True
          Caption = 'Domingo'
          TabOrder = 20
        end
        object UniGroupBox1: TUniGroupBox
          Left = 310
          Top = 80
          Width = 151
          Height = 70
          Hint = ''
          ShowHint = True
          Caption = 'Fechas Petici'#243'n'
          TabOrder = 21
          object UniDBDateTimePicker3: TUniDBDateTimePicker
            Left = 51
            Top = 43
            Width = 94
            Hint = ''
            ShowHint = True
            DateTime = 43369.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 1
          end
          object UniDBDateTimePicker4: TUniDBDateTimePicker
            Left = 51
            Top = 22
            Width = 94
            Hint = ''
            ShowHint = True
            DateTime = 43369.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 2
          end
          object UniLabel11: TUniLabel
            Left = 15
            Top = 22
            Width = 30
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Desde'
            TabOrder = 3
          end
          object UniLabel12: TUniLabel
            Left = 17
            Top = 43
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Hasta'
            TabOrder = 4
          end
        end
        object UniGroupBox2: TUniGroupBox
          Left = 310
          Top = 151
          Width = 151
          Height = 71
          Hint = ''
          ShowHint = True
          Caption = 'Fechas Excluidas'
          TabOrder = 22
          object UniDBDateTimePicker5: TUniDBDateTimePicker
            Left = 51
            Top = 43
            Width = 94
            Hint = ''
            ShowHint = True
            DateTime = 43369.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 1
          end
          object UniDBDateTimePicker6: TUniDBDateTimePicker
            Left = 51
            Top = 22
            Width = 94
            Hint = ''
            ShowHint = True
            DateTime = 43369.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 2
          end
          object UniLabel14: TUniLabel
            Left = 15
            Top = 22
            Width = 30
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Desde'
            TabOrder = 3
          end
          object UniLabel15: TUniLabel
            Left = 17
            Top = 43
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Hasta'
            TabOrder = 4
          end
        end
        object UniDBRadioGroup3: TUniDBRadioGroup
          Left = 496
          Top = 80
          Width = 137
          Height = 155
          Hint = ''
          ShowHint = True
          Caption = 'Tipo de Aviso'
          ParentFont = False
          TabOrder = 23
          Items.Strings = (
            'Personal'
            'Por telefono'
            'Por Email'
            'Entrega Domicilio')
          Values.Strings = (
            '0'
            '1'
            '2')
        end
        object UniBitBtn5: TUniBitBtn
          Left = 666
          Top = 68
          Width = 55
          Height = 42
          Hint = 'Grabar'
          ShowHint = True
          Caption = ''
          TabOrder = 24
          IconAlign = iaTop
          ImageIndex = 48
        end
        object UniBitBtn1: TUniBitBtn
          Left = 666
          Top = 120
          Width = 55
          Height = 42
          Hint = 'Cancelar'
          ShowHint = True
          Caption = ''
          TabOrder = 25
          IconAlign = iaTop
          ImageIndex = 47
        end
        object UniBitBtn6: TUniBitBtn
          Left = 666
          Top = 172
          Width = 55
          Height = 42
          Hint = 'Cerrar Ventana'
          ShowHint = True
          Caption = ''
          TabOrder = 26
          IconAlign = iaTop
          ImageIndex = 52
        end
        object UniBitBtn7: TUniBitBtn
          Left = 3
          Top = 14
          Width = 56
          Height = 42
          Hint = 'Articulo'
          ShowHint = True
          Glyph.Data = {
            F6060000424DF606000000000000360000002800000018000000180000000100
            180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA0CDE94D9B
            C6D4A14CF8E8D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            A0CDE90179C40D91D26AA2BADEC08CFFFFFFFFFFFFFFFFFFB8DBFB62AFF7379A
            F562AFF79BCDFAD5EAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA0CDE90179C409AFE880E7FFB3D9EE61ABDAFFFFFFFFFFFFFFFFFF
            1A8BF31A8BF33AB6F73AB6F72AA0F51A8BF31A8BF353A8F6A6C7E3ECC795E6B5
            72DE9C42E4AE65EAC087D7D5C70179C44CCBEC97EFFFC0E3F456A6D7F0F7FCFF
            FFFFFFFFFFFFFFFF53A8F62095F43AACF658DEFB5AE2FC5AE2FC4ED1F973A6B1
            D29A4CD9A55ACCB893C0C5C1C6B89CD1A96BDE9A3E608C932697D0C7E6F456A6
            D7F0F7FCFFFFFFFFFFFFFFFFFFFFFFFF53A8F637B5F835A5F661EDFE16D9FC24
            D6FA60C4C9DE9A3FD4BC7BC8EEDACBF1F0C6EAF5BAE2F0AED9E1BCBC9EE0A249
            88A5A01F89D1B7DAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF53A8F63CB6F81DA3
            F684F3FE1CE4FC1BDDF6C4A255CFAC62A3E8D8B1EAE0C4EFE7CAF0E9C4EFE3C2
            F2DBBFEED7CCC58EDE9A3E459CD71D8BF01A8BF2AAD4FBFFFFFFFFFFFFFFFFFF
            2892F445C1F91E9EF588F3FE20E2F74CCCC8DE9A3EB2CFA3A4E5CFAAE9DCB5EC
            E3B7EAE3ADE5DEAAE1D9B0DDCEBAD8BBDAA04AA4B9915FDFF53FBBF78DC5F9FF
            FFFFFFFFFFFFFFFF1A8BF356E0FD1C8CF384F3FD30E2F364C4B1DE9A3EB0CC9D
            A7DBBCA6E3D0A9E6D9ABE5DAA8E1D7A9DCCFAED1BDB7C29FCFA760B99B6442B3
            F04BCBF88DC5F9FFFFFFFFFFFFFFFFFF1A8BF356E0FD1A8BF374F1FD3AE2F177
            C3AADE9A3EC1C289BCD3AEBADFC7B9E4D2BEE6D8BCE3D5BEDBC8C2CEB0C8BC8D
            D3A458BC9B612F97EC4BCBF88DC5F9FFFFFFFFFFFFFFFFFF1A8BF356E0FD1A8B
            F38BF4FD39E2F272C7B4E3A95BDFCEA0D8D4AFD3DDC0D2E3CED2E5D4D3E3D1D6
            DCC3DCD2AFE1CAA1E7BF87A59C7A2F97EB5AC7F98DC5F9FFFFFFFFFFFFE3F1FE
            1E90F35FE2FD1D94F482E2FB35E4F74ED6DCCF9F4DEBCD9FEADAB6E7DFC2E7E6
            D0E8E9D8E7E7D5E8E0C7EAD8B9ECD5B1E5B26C789FA92D97ED61BEF98DC5F9FF
            FFFFFFFFFFC6E2FC2DA1F55FE2FD209EF569D9FB31E6FB3EDDEE80BFA5E2A657
            F2E0C4F4E8D4F6EEDEF6F0E3F5EFE1F4EAD9F3E5D0ECC795BA9C6447A0DB379E
            F259ADF88DC5F9FFFFFFFFFFFFC6E2FC2DA1F55FE2FD209EF567D9FC3EE8FE34
            E1F941D9EB8ABB9CE3AC60F3DEC1FBF6EEFBF7F0FBF7EFF9F0E3EDC997CEA664
            82B5CA41A6ED41A6F760B1F970B7F8FFFFFFFFFFFFC6E2FC2DA1F55FE2FD209E
            F557D2FB4DEAFF30E3FE35DEF944D6EB7CC1B2BAAD74DCAC64E8BA7DE7B573D1
            AB6BADB9A492C8DF91CDF08ECCF96FBBFB77BEFC53A8F6FFFFFFFFFFFFB8DBFB
            2DA1F55FE2FD26B1F845BFF94DEAFF2FE3FF31E0FE36DBF946D5F05ACFE36ACC
            DD75CADB7CCADD80CBE481CDEF85CEF78CD0FC93D0FDA9D6FC98CDFC53A8F6FF
            FFFFFFFFFF8DC5F942B8F85FE2FD2CB3F843BEF94DEAFF2FE3FF30E0FF32DDFE
            3BD9FC47D6F953D3F75ED1F668D1F770D0F979D1FC82D0FE8BD1FE92CFFDB2DC
            FDA3D2FC53A8F6FFFFFFFFFFFF8DC5F942B8F85FE2FD2FB3F835A5F670D3FA59
            D1FA5EDBFC6DE9FD74EBFD6AE6FE6FE8FE6BE1FE71DEFE73D8FE78D3FE81D1FF
            8BD1FF92CFFDB2DCFDA3D2FC53A8F6FFFFFFFFFFFF8DC5F942B8F85FE2FD40D6
            FD21AFF81DADF819ACF8189AF6169AF61599F62898F4309EF439A4F547B3F751
            B9F861C6F96CCDFA77D0FB88D7FCA8E0FCBAE5FD53A8F6FFFFFFFFFFFF8DC5F9
            2496F42BA0F527A0F5239FF5209EF51C9DF5189BF6149EF604C8FF08C3FF0DBE
            FF12BAFF16ADFC1AA7FC1DA4FC1E9CF92098F91D90F553A8F662B5F76BB8F7FF
            FFFFFFFFFFF2F8FEC6E2FCC6E2FCC6E2FCC6E2FCC6E2FCC6E2FCC6E2FC2892F4
            0BB1FA07C5FF0BC0FF10BBFF15B7FF19B2FF1EADFF23A9FF27A4FF1E90F6C6E2
            FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFD5EAFD379AF51A8BF31A8BF31A8BF31A8BF31A8BF31A8BF31A8BF3
            1A8BF31A8BF3D5EAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          Caption = ''
          TabOrder = 27
          IconAlign = iaTop
        end
        object UniLabel16: TUniLabel
          Left = 137
          Top = 239
          Width = 75
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Observaciones:'
          TabOrder = 28
        end
        object UniDBEdit6: TUniDBEdit
          Left = 221
          Top = 236
          Width = 412
          Height = 22
          Hint = ''
          ShowHint = True
          TabOrder = 29
        end
      end
    end
  end
  object dsTrabajos: TDataSource
    Left = 8
    Top = 760
  end
  object dsTiempo: TDataSource
    Left = 72
    Top = 760
  end
  object dsEmpresa: TDataSource
    Left = 8
    Top = 816
  end
  object dsSumTiempo: TDataSource
    Left = 128
    Top = 760
  end
  object dsTiempoCliente: TDataSource
    Left = 72
    Top = 808
  end
  object dsTodosTiempos: TDataSource
    Left = 120
    Top = 808
  end
  object dsEmpresa2: TDataSource
    Left = 8
    Top = 708
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
end
