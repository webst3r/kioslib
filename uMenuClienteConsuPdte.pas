unit uMenuClienteConsuPdte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormMenuClienteConsuPdte = class(TUniForm)
    dsTrabajos: TDataSource;
    dsTiempo: TDataSource;
    dsEmpresa: TDataSource;
    dsSumTiempo: TDataSource;
    dsTiempoCliente: TDataSource;
    dsTodosTiempos: TDataSource;
    dsEmpresa2: TDataSource;
    tmSession: TUniTimer;
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniBitBtn1: TUniBitBtn;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniDBRadioGroup2: TUniDBRadioGroup;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    btSalir: TUniBitBtn;
    UniSpeedButton1: TUniSpeedButton;
    UniPanel2: TUniPanel;
    UniPanel3: TUniPanel;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniLabel2: TUniLabel;
    UniDBGrid1: TUniDBGrid;
    UniPanel4: TUniPanel;
    UniPanel5: TUniPanel;
    UniDBGrid2: TUniDBGrid;
    UniContainerPanel1: TUniContainerPanel;
    UniLabel6: TUniLabel;
    btListar: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    btObservaciones: TUniBitBtn;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniLabel5: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniLabel7: TUniLabel;
    UniPanel6: TUniPanel;


  private
    { Private declarations }

  public
    { Public declarations }



  end;

function FormMenuClienteConsuPdte: TFormMenuClienteConsuPdte;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti;

function FormMenuClienteConsuPdte: TFormMenuClienteConsuPdte;
begin
  Result := TFormMenuClienteConsuPdte(DMppal.GetFormInstance(TFormMenuClienteConsuPdte));

end;


end.
