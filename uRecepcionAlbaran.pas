﻿unit uRecepcionAlbaran;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBCheckBox, uniImageList,
  uniHTMLMemo, math;

type
  TFormAlbaranRecepcion = class(TUniForm)
    tmSession: TUniTimer;
    UniPageControl3: TUniPageControl;
    UniTabSheet7: TUniTabSheet;
    UniTabSheet8: TUniTabSheet;
    UniPageControl1: TUniPageControl;
    tabDatosDocu: TUniTabSheet;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniButton1: TUniButton;
    UniGroupBox1: TUniGroupBox;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniDBCheckBox1: TUniDBCheckBox;
    UniDBMemo1: TUniDBMemo;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    tabDupliEntrada: TUniTabSheet;
    UniPanel8: TUniPanel;
    UniLabel13: TUniLabel;
    UniLabel14: TUniLabel;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniDBDateTimePicker4: TUniDBDateTimePicker;
    UniEdit1: TUniEdit;
    UniDBGrid1: TUniDBGrid;
    UniDBGrid2: TUniDBGrid;
    tabCambiarDistri: TUniTabSheet;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniLabel15: TUniLabel;
    UniLabel16: TUniLabel;
    dsHisArti: TDataSource;
    dsUltiCompra: TDataSource;
    dsHisArtiTotResumen: TDataSource;
    dsCabe1: TDataSource;
    UniDBRadioGroup1: TUniDBRadioGroup;
    dsAntiguos: TDataSource;
    dsAntiguosLines: TDataSource;
    dsProveS: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    BtnEditar: TUniBitBtn;
    UniBitBtn30: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniPanel26: TUniPanel;
    pnlOpciones: TUniPanel;
    UniBitBtn17: TUniBitBtn;
    UniBitBtn22: TUniBitBtn;
    UniBitBtn23: TUniBitBtn;
    UniBitBtn32: TUniBitBtn;
    btOpciones: TUniBitBtn;
    btLista: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    btImprimir: TUniBitBtn;
    dsArticulo: TDataSource;
    UniPanel2: TUniPanel;
    btNuevaLinea: TUniBitBtn;
    btBorrarLinea: TUniBitBtn;
    btReclamar: TUniBitBtn;
    btResumenCargos: TUniBitBtn;
    btPrecios: TUniBitBtn;
    btEditarLinea: TUniBitBtn;
    btPrior: TUniSpeedButton;
    btNext: TUniSpeedButton;
    UniPanel4: TUniPanel;
    btBorrar: TUniSpeedButton;
    btNuevoAdendum: TUniSpeedButton;
    btDirectoDevolucion: TUniSpeedButton;
    btRecuperaDevolucion: TUniSpeedButton;
    UniDBCheckBox4: TUniDBCheckBox;
    UniLabel51: TUniLabel;
    edStock: TUniDBEdit;
    edCantiReal: TUniDBEdit;
    edReserva: TUniDBEdit;
    UniLabel58: TUniLabel;
    UniLabel59: TUniLabel;
    UniLabel60: TUniLabel;
    edStockMaximo: TUniDBEdit;
    UniDBEdit59: TUniDBEdit;
    UniLabel61: TUniLabel;
    UniLabel62: TUniLabel;
    UniLabel64: TUniLabel;
    UniLabel63: TUniLabel;
    edAcumCompras: TUniDBEdit;
    edAcumDevol: TUniDBEdit;
    UniPanel1: TUniPanel;
    UniDBEdit89: TUniDBEdit;
    UniLabel78: TUniLabel;
    UniDBEdit90: TUniDBEdit;
    UniDBEdit91: TUniDBEdit;
    UniDBEdit92: TUniDBEdit;
    UniDBEdit93: TUniDBEdit;
    UniDBEdit94: TUniDBEdit;
    UniLabel79: TUniLabel;
    UniLabel80: TUniLabel;
    UniPanel19: TUniPanel;
    UniLabel81: TUniLabel;
    UniPanel20: TUniPanel;
    edNumInferior: TUniDBEdit;
    btNuminferiorOk: TUniBitBtn;
    btNumInferiorNo: TUniBitBtn;
    UniPanel3: TUniPanel;
    UniPanel13: TUniPanel;
    UniLabel47: TUniLabel;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniLabel50: TUniLabel;
    edBarras: TUniEdit;
    edAdendum: TUniDBEdit;
    edCantiEnAlba: TUniDBEdit;
    edSuscripcion: TUniDBEdit;
    btSumarRecepcion: TUniSpeedButton;
    btRestarRecepcion: TUniSpeedButton;
    UniDBEdit50: TUniDBEdit;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniLabel52: TUniLabel;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel53: TUniLabel;
    UniDBEdit55: TUniDBEdit;
    btGrabaLinea: TUniBitBtn;
    btCancelaLinea: TUniBitBtn;
    edAbono: TUniDBDateTimePicker;
    edCargo: TUniDBDateTimePicker;
    edAvisosDevol: TUniDBDateTimePicker;
    edDevolucion: TUniDBDateTimePicker;
    UniLabel54: TUniLabel;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniLabel1: TUniLabel;
    pnlTotales: TUniPanel;
    UniLabel66: TUniLabel;
    UniLabel69: TUniLabel;
    UniLabel70: TUniLabel;
    UniLabel72: TUniLabel;
    UniLabel73: TUniLabel;
    UniLabel74: TUniLabel;
    UniLabel75: TUniLabel;
    UniLabel76: TUniLabel;
    UniDBEdit68: TUniDBEdit;
    btTecNume: TUniBitBtn;
    btActualizaArticulo: TUniSpeedButton;
    UniLabel67: TUniLabel;
    UniLabel68: TUniLabel;
    edPreu: TUniDBEdit;
    edPreu2: TUniDBEdit;
    edVentaCoste: TUniEdit;
    edMargen: TUniDBEdit;
    edMargen2: TUniDBEdit;
    edMargenCoste: TUniEdit;
    edTIva2: TUniDBEdit;
    edTIva1: TUniDBEdit;
    UniDBEdit79: TUniDBEdit;
    UniDBEdit80: TUniDBEdit;
    UniDBEdit83: TUniDBEdit;
    UniDBEdit84: TUniDBEdit;
    UniDBEdit81: TUniDBEdit;
    UniDBEdit82: TUniDBEdit;
    edCosteDire: TUniDBEdit;
    UniDBEdit86: TUniDBEdit;
    UniDBEdit87: TUniDBEdit;
    edSumarCoste: TUniEdit;
    UniLabel77: TUniLabel;
    UniSpeedButton34: TUniSpeedButton;
    UniPageControl4: TUniPageControl;
    UniTabSheet9: TUniTabSheet;
    GridLinea: TUniDBGrid;
    UniTabSheet10: TUniTabSheet;
    GridResumen: TUniDBGrid;
    pnPieCompras: TUniPanel;
    GridUltiCompra: TUniDBGrid;
    edEncarte1: TUniDBEdit;
    edEncarte2: TUniDBEdit;
    UniLabel71: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit9: TUniDBEdit;
    UniLabel17: TUniLabel;
    UniLabel18: TUniLabel;
    btHistoArti: TUniBitBtn;
    btVerFicha: TUniBitBtn;
    edMensajes: TUniEdit;
    procedure btOpcionesClick(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure btListaClick(Sender: TObject);
    procedure edPreuExit(Sender: TObject);
    procedure edPreu2Exit(Sender: TObject);
    procedure edTIva1Exit(Sender: TObject);
    procedure edTIva2Exit(Sender: TObject);
    procedure edCosteDireExit(Sender: TObject);
    procedure edSumarCosteExit(Sender: TObject);
    procedure edCantiRealExit(Sender: TObject);
    procedure GridLineaDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure edCantiEnAlbaExit(Sender: TObject);
    procedure edCantiEnAlbaEnter(Sender: TObject);
    procedure btSumarRecepcionClick(Sender: TObject);
    procedure btRestarRecepcionClick(Sender: TObject);
    procedure btTecNumeClick(Sender: TObject);
    procedure UniSpeedButton34Click(Sender: TObject);
    procedure edBarrasEnter(Sender: TObject);
    procedure edBarrasExit(Sender: TObject);
    procedure edTIva1Enter(Sender: TObject);
    procedure btHistoArtiClick(Sender: TObject);
    procedure btVerFichaClick(Sender: TObject);
    procedure edTIva2Enter(Sender: TObject);
    procedure edAdendumExit(Sender: TObject);


  private
    procedure ProCalcularPreuCost1;
    procedure ProLlegirArticle(vIdArti: Integer);
    function RutFormatearAdendum(vAdendum: String; vTBarras: Integer): String;

    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;
    vTIVA , vTIVA2: Integer;
    vCantidad : Real;
    vAden : String;

    swUnico, swClicPnl : Boolean;

     procedure RutMensajeArticulo(vTipoMensaje: Integer);
     procedure RutMensajeDistri;
  end;

function FormAlbaranRecepcion: TFormAlbaranRecepcion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,uDevolFicha,
  uMenu, uDMRecepcion, ufrFicha, uMenuRecepcion, uDMMenuArti, uMenuArti, uFichaArti, uMensajes;

function FormAlbaranRecepcion: TFormAlbaranRecepcion;
begin
  Result := TFormAlbaranRecepcion(DMppal.GetFormInstance(TFormAlbaranRecepcion));

end;


procedure TFormAlbaranRecepcion.btHistoArtiClick(Sender: TObject);
begin
  if (DMMenuRecepcion.sqlHisArti.RecordCount <> 0)
  then FormMenu.RutAbrirHistoArtiTablas(DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsInteger,
                                    DMMenuRecepcion.sqlHisArtiBARRAS.AsString,
                                    DMMenuRecepcion.sqlHisArtiADENDUM.AsString,
                                    DMMenuRecepcion.sqlHisArtiDESCRIPCION.AsString,
                                    'Recepcion');
  DMppal.swHisArti := self.Name;
end;

procedure TFormAlbaranRecepcion.btImprimirClick(Sender: TObject);
begin
  FormfrFicha.vModifica    := False;
  FormfrFicha.InvNum       := '1';
  FormfrFicha.vTipoInforme := 'iRecepcion';
  FormfrFicha.vExportPDF   := False;
  FormfrFicha.ShowModal;
end;

procedure TFormAlbaranRecepcion.btListaClick(Sender: TObject);
begin
  FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabListaRecepcion;
end;

procedure TFormAlbaranRecepcion.btOpcionesClick(Sender: TObject);
begin
  pnlOpciones.Visible := not pnlOpciones.Visible;
end;

procedure TFormAlbaranRecepcion.btRestarRecepcionClick(Sender: TObject);
begin
  DMMenuRecepcion.sqlHisArti.Edit;
  DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsInteger := DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsInteger - 1;
  DMMenuRecepcion.sqlHisArti.Post;
  edCantiEnAlbaExit(nil);
  {Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
  DMDevolucion.sqlLinea.Post;

  DMDevolucion.sqlLineaTotal.Refresh;
  DMDevolucion.RutComprobarPaquete;}
end;

procedure TFormAlbaranRecepcion.btSumarRecepcionClick(Sender: TObject);
begin
  DMMenuRecepcion.sqlHisArti.Edit;
  DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsInteger := DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsInteger + 1;
  DMMenuRecepcion.sqlHisArti.Post;
  edCantiEnAlbaExit(nil);
  {Dmppal.RutCalculaImportes(DMDevolucion.sqlLinea, 'CANTIDAD', -2, 0);
  DMDevolucion.sqlLinea.Post;

  DMDevolucion.sqlLineaTotal.Refresh;
  DMDevolucion.RutComprobarPaquete;}
end;

procedure TFormAlbaranRecepcion.ProCalcularPreuCost1;
begin
  inherited;
  if DMPpal.RutEdiInse(DMMenuRecepcion.sqlHisArti) = False Then Exit;
  DMMenuArti.RutCalculaCost_Taula     (DMMenuRecepcion.sqlHisArti, '1H');
  DMMenuArti.RutCalculaCost_Taula_Tot (DMMenuRecepcion.sqlHisArti, '1H');
end;


procedure TFormAlbaranRecepcion.UniSpeedButton34Click(Sender: TObject);
begin
  BtTecNume.Enabled   := True;
  edCosteDire.Visible   := True;
  edMargenCoste.Visible := True;
  edSumarCoste.Visible := True;
  edCosteDire.SetFocus;
end;

procedure TFormAlbaranRecepcion.btTecNumeClick(Sender: TObject);
var
  wMarge, wVenta, wAux1, wAux2 : real;
begin
  inherited;

  if edCosteDire.Visible = False then
  begin
    exit;
  end;

  if StrToFloatDef(edVentaCoste.Text,0) > 0 then
  begin
    if DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOVENTA').AsFloat = 0 then exit;
//    if DMManArtiB.cdsCabe.FieldByName('TPCIVA1').AsFloat = 0 then exit;
    wVenta := StrToFloatDef(edVentaCoste.Text,0);

    wMarge := DMMenuArti.RutCalculMarge_A  (wVenta,
                                 DMMenuRecepcion.sqlHisArti.FieldByName('TPCIVA').AsFloat,
                                 DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOSTE').AsFloat);


    if DMPpal.RutEdiInse (DMMenuRecepcion.sqlHisArti) = False Then DMMenuRecepcion.sqlHisArti.Edit;
    wMarge := RoundTo(wMarge,-2);
    DMMenuRecepcion.sqlHisArti.FieldByName('MARGEN').AsFloat := wMarge;
    DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOVENTA').AsFloat := wVenta;

    ProCalcularPreuCost1;

    edCantiEnAlbaExit(nil);
    exit;
  end;

  wMarge := StrToFloatDef(edMargenCoste.Text, 0);

  wVenta :=  (DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOSTE').AsFloat * wMarge / 100);
  wVenta :=  wVenta + DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOSTE').AsFloat;

  wAux1  :=  (DMMenuRecepcion.sqlHisArti.FieldByName('TPCIVA').AsFloat * wVenta / 100);
  wAux2  :=  (DMMenuRecepcion.sqlHisArti.FieldByName('TPCRE').AsFloat  * wVenta / 100);
  wVenta :=  wVenta + wAux1 + wAux2;

  wVenta := RoundTo(wVenta,-2);

  if DMPpal.RutEdiInse (DMMenuRecepcion.sqlHisArti) = False Then DMMenuRecepcion.sqlHisArti.Edit;
  DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOVENTA').AsFloat := wVenta;

  edCosteDireExit(nil);
  DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOSTE').AsFloat := RoundTo(DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOSTE').AsFloat, -4);

  edCantiEnAlbaExit(nil);

end;

procedure TFormAlbaranRecepcion.btVerFichaClick(Sender: TObject);
begin
  DMMenuArti.swCreando := false;
  DMMenuArti.RutAbrirFichaArticulos(DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsString);
  FormMenuArti.RutAbrirDevolArti;
  FormManteArti.RutHabitlitarBTs(False);
  //para saber donde volver atras en caso de no venir de la lista
//  FormManteArti.vStringOrigen := 'fichaRecepcion';
  DMppal.swArtiFicha := self.Name;
  FormManteArti.RutShow;
end;

procedure TFormAlbaranRecepcion.edAdendumExit(Sender: TObject);
begin
  (sender as TUniDBEdit).Color := clWindow;

  ProLlegirArticle(DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsInteger);

  vAden := edAdendum.Text;
  vAden := RutFormatearAdendum(vAden, DMMenuRecepcion.sqlHisArtiTBARRAS.asInteger);
  if (StrToIntDef(edAdendum.Text,0) > 0) and (DMMenuRecepcion.sqlHisArti.RecordCount > 0) then
  begin
    DMMenuRecepcion.sqlHisArti.Edit;
    DMMenuRecepcion.sqlHisArtiADENDUM.AsString := vAden;
    Dmppal.RutCalculaImportes(DMMenuRecepcion.sqlHisArti, 'CANTIDAD', -2, 0);
    DMMenuRecepcion.sqlHisArti.Post;

  end;

end;

Function TFormAlbaranRecepcion.RutFormatearAdendum  (vAdendum:String; vTBarras:Integer):String;
var
  vAden : Integer;
begin
//  Result := '';

  if (vTBarras = 7) or
     (vTBarras = 9) or
     (vTBarras = 15) then
  begin
      vAden := StrToIntDef(vAdendum,0);
      Result := FormatFloat('00', vAden);
      exit;
  end;

  if (vTBarras = 13) then
  begin
    if vAdendum = '' then
    begin
      result := '';
    end else
    begin
      vAden := StrToIntDef(vAdendum,0);
//      Result := FormatFloat('#####', vAden);
      Result := FormatFloat('00000', vAden);
    end;
    exit;
  end;

  if (vTBarras = 18) then
  begin

      vAden := StrToIntDef(vAdendum,0);
      Result := FormatFloat('00000', vAden);
      exit;
  end;


end;

procedure TFormAlbaranRecepcion.ProLlegirArticle (vIdArti: Integer);
begin
  DMMenuRecepcion.RutAbrirArti(vIdArti);
end;

procedure TFormAlbaranRecepcion.edBarrasEnter(Sender: TObject);
begin
  (sender as TUniEdit).Color := clLime;
  edBarras.Text  := '';
end;

procedure TFormAlbaranRecepcion.edBarrasExit(Sender: TObject);
begin
   edBarras.Color := clWindow;


  if edBarras.Text > '' then
  begin
    if length(edbarras.Text) > 7 then
    begin
      DMppal.RutSepararCodigoBarras(edBarras.Text);


    end else
    begin



    end;


  end;
end;

procedure TFormAlbaranRecepcion.RutMensajeArticulo(vTipoMensaje : Integer);
begin
    if vTipomensaje = 1 then
    begin
      edMensajes.Text := 'Correcto';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 3 then
    begin
      edMensajes.Text := 'No has Comprado el Articulo';
      edMensajes.Color := clYellow;
    end;

    if vTipoMensaje = 4 then
    begin
      edMensajes.Text := '¡No existe ninguna distribuidora para este articulo!';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 5 then
    begin
      edMensajes.Text := '¡Este Articulo no existe!';
      edMensajes.Color := clRed;
      FormMensajes.RutInicioForm(3, 'FORMDEVOLFICHA', 'ACEPTARCANC','¿Quieres Crear un Nuevo Articulo?');
      FormMensajes.ShowModal();


    end;

    if vTipoMensaje = 6 then
    begin
      edMensajes.Text := 'Devolución Cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 7 then
    begin
      edMensajes.Text := 'Devolución abierta';
      edMensajes.Color := clLime;
    end;

    if vTipoMensaje = 8  then
    begin
      edMensajes.Text := 'No se permite anular la linea si esta la devolución cerrada';
      edMensajes.Color := clRed;
    end;

    if vTipoMensaje = 9  then
    begin
      edMensajes.Text := 'Se han Actualizado los precios';
      edMensajes.Color := clYellow;
    end;
end;

procedure TFormAlbaranRecepcion.RutMensajeDistri;
begin
  if swUnico then
  begin
    FormMensajes.RutInicioForm(1, 'FORMDEVOLFICHA', 'ACEPTAR','La Distribuidora del articulo no existe en la devolucion');
    FormMensajes.ShowModal();
    edBarras.SetFocus;
  end else
  begin
    DMMenuRecepcion.RutLeerProveedor(DMppal.vID);
    FormMensajes.RutInicioForm(2, 'FORMDEVOLFICHA', 'ACEPTARCANC','La Distribuidora del articulo no existe en la devolucion, quieres añadirla?');
    FormMensajes.ShowModal();
    edCantiEnAlba.SetFocus;

  end;


end;


procedure TFormAlbaranRecepcion.edCantiEnAlbaEnter(Sender: TObject);
begin
  vCantidad := DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsFloat;
end;

procedure TFormAlbaranRecepcion.edCantiEnAlbaExit(Sender: TObject);
begin
  DMMenuRecepcion.sqlHisArti.edit;
  DMMenuRecepcion.ProCalcularAdevolver;

  if DMppal.RutComprobarTopeCantitat (DMMenuRecepcion.sqlHisArti,100000,0,'CANTIENALBA','SS') = False Then
  begin
       DMMenuRecepcion.sqlHisArtiCANTIENALBA.AsFloat := vCantidad;
       edCantiEnAlba.SetFocus;
       Exit;
  end;

   DMMenuRecepcion.sqlHisArti.FieldByName('CANTIDAD').AsFloat := DMMenuRecepcion.sqlHisArti.FieldByName('CANTIENALBA').AsFloat;
   DMPpal.RutCalculaLinHisto_N (DMMenuRecepcion.sqlHisArti, 'CANTIENALBA',-2, 0);
   if DMMenuRecepcion.sqlHisArti.State = dsbrowse then DMMenuRecepcion.sqlHisArti.edit;
   DMMenuRecepcion.ProSortidaPreus (1);   // 'PRECIOVENTA'          pPreu.SetFocus;
  if (DMMenuRecepcion.sqlArticulos1TBARRAS.asinteger = 18) and
     (DMMenuRecepcion.sqlArticulos1NADENDUM.AsInteger = 0) and
     (DMMenuRecepcion.sqlHisArtiADENDUM.AsString = '') then
     if edAdendum.CanFocus then edAdendum.SetFocus;
end;

procedure TFormAlbaranRecepcion.edCantiRealExit(Sender: TObject);
begin
  if DMppal.RutComprobarTopeCantitat (DMMenuRecepcion.sqlHisArti,1000,0,'CANTIDAD','SS') = False Then
  begin
       edCantiReal.SetFocus;
       Exit;
  end;
  DMPpal.RutCalculaLinHisto_N (DMMenuRecepcion.sqlHisArti, 'CANTIENALBA', -2, 0);
end;

procedure TFormAlbaranRecepcion.edCosteDireExit(Sender: TObject);
var
  wMarge : Double;
begin
  if  StrToFloatDef(edSumarCoste.Text, 0) <> 0 then
    edPreu2.Text := FormatFloat(',0.00', (StrToFloatDef(edCosteDire.Text,0) + StrToFloatDef(edSumarCoste.Text, 0)));

//  pCosteDire.Visible := False;
  if DMMenuRecepcion.sqlHisArtiPRECIOVENTA.AsFloat = 0 then exit;
  if DMMenuRecepcion.sqlHisArtiTPCIVA.AsFloat = 0 then exit;

  wMarge := DMMenuArti.RutCalculMarge_A  (DMMenuRecepcion.sqlHisArtiPRECIOVENTA.AsFloat,
                               DMMenuRecepcion.sqlHisArtiTPCIVA.AsFloat,
                               DMMenuRecepcion.sqlHisArtiPRECIOCOSTE.AsFloat);


  if DMPpal.RutEdiInse (DMMenuArti.sqlFichaArticulo) = False Then DMMenuRecepcion.sqlHisArti.Edit;
  DMMenuRecepcion.sqlHisArtiMARGEN.AsFloat := wMarge;
  wMarge := RoundTo(wMarge,2);
  DMMenuRecepcion.ProCalcularPreuCost1;
end;

procedure TFormAlbaranRecepcion.edPreu2Exit(Sender: TObject);
begin
  if Dmppal.RutEdiInse(DMMenuRecepcion.sqlHisArti) = False Then Exit;
  DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOMPRA2').AsFloat := DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOVENTA2').AsFloat;
  DMMenuRecepcion.ProSortidaPreus (1);
end;

procedure TFormAlbaranRecepcion.edPreuExit(Sender: TObject);
begin
  if DMppal.RutComprobarTopeCantitat (DMMenuRecepcion.sqlHisArti,100000,
                               DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOMPRA').AsFloat,
                               'PRECIOVENTA','SS') = False Then begin end;

  if DMppal.RutEdiInse(DMMenuRecepcion.sqlHisArti) = False Then Exit;
  DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOCOMPRA').AsFloat := DMMenuRecepcion.sqlHisArti.FieldByName('PRECIOVENTA').AsFloat;
  DMMenuRecepcion.ProSortidaPreus (1);   // 'PRECIOVENTA'          pPreu.SetFocus;


end;

procedure TFormAlbaranRecepcion.edSumarCosteExit(Sender: TObject);
begin
  edVentaCoste.Text := FormatFloat(',0.00', (StrToFloatDef(edCosteDire.Text,0) + StrToFloatDef(edSumarCoste.Text, 0)));
end;

procedure TFormAlbaranRecepcion.edTIva1Enter(Sender: TObject);
begin
  vTIVA := DMMenuRecepcion.sqlHisArtiTIVA.AsInteger;
end;

procedure TFormAlbaranRecepcion.edTIva1Exit(Sender: TObject);
begin
  if vTIVA <> DMMenuRecepcion.sqlHisArti.FieldByName('TIVA').AsInteger Then
  begin
    Dmppal.RutHisArtiLin_CargaIVA_A  (DMMenuRecepcion.sqlHisArti,'TIVA','TPCIVA','TPCRE', 1);
    DMMenuRecepcion.ProSortidaPreus (1);
    DMPpal.RutCalculaLinHisto_N      (DMMenuRecepcion.sqlHisArti, 'CANTIENALBA', 2, 0);
  end;
end;

procedure TFormAlbaranRecepcion.edTIva2Enter(Sender: TObject);
begin
  vTIVA2 := DMMenuRecepcion.sqlHisArtiTIVA2.AsInteger;
end;

procedure TFormAlbaranRecepcion.edTIva2Exit(Sender: TObject);
begin
  if vTIVA2 <> DMMenuRecepcion.sqlHisArti.FieldByName('TIVA2').AsInteger Then
  begin
    Dmppal.RutHisArtiLin_CargaIVA_A  (DMMenuRecepcion.sqlHisArti,'TIVA2','TPCIVA2','TPCRE2', 1);
    DMMenuRecepcion.ProSortidaPreus (1);
    DMPpal.RutCalculaLinHisto_N      (DMMenuRecepcion.sqlHisArti, 'CANTIENALBA', 2, 0);
  end;
end;

procedure TFormAlbaranRecepcion.GridLineaDrawColumnCell(Sender: TObject; ACol,
  ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if (Column.FieldName = 'CANTIDAD') and
       (DMMenuRecepcion.sqlHisArti.FieldByName('NRECLAMACION').AsInteger <> 0) then

       Attribs.Color := clYellow;

  if (Column.FieldName = 'CANTIDAD') and
     (DMMenuRecepcion.sqlHisArti.FieldByName('NRECLAMACION').AsInteger = 0) and
     (DMMenuRecepcion.sqlHisArti.FieldByName('CANTIENALBA').AsInteger <>
      DMMenuRecepcion.sqlHisArti.FieldByName('CANTIDAD').AsInteger) then

       Attribs.Color := clRed;

end;



end.
