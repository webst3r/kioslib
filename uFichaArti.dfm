object FormManteArti: TFormManteArti
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 660
  ClientWidth = 1063
  Caption = 'FormManteArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  NavigateKeys.Enabled = True
  NavigateKeys.Next.Key = 13
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPageControl2: TUniPageControl
    Left = 0
    Top = 473
    Width = 1063
    Height = 187
    Hint = ''
    Visible = False
    ShowHint = True
    ActivePage = UniTabSheet3
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object UniTabSheet3: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'Precios'
      object UniPanel3: TUniPanel
        Left = 0
        Top = 0
        Width = 1055
        Height = 77
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
      end
      object PageControl: TUniPageControl
        Left = 0
        Top = 77
        Width = 1055
        Height = 236
        Hint = ''
        Visible = False
        ShowHint = True
        ActivePage = UniTabSheet8
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        object UniTabSheet8: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Precios s/Dia Semana'
          object UniDBNavigator1: TUniDBNavigator
            Left = 0
            Top = 1
            Width = 241
            Height = 25
            Hint = ''
            ShowHint = True
            TabOrder = 0
          end
          object GridPreus: TUniDBGrid
            Left = 0
            Top = 26
            Width = 1049
            Height = 179
            Hint = ''
            ShowHint = True
            DataSource = dsPreus
            LoadMask.Message = 'Loading data...'
            TabOrder = 1
            Columns = <
              item
                FieldName = 'TDIASEMA'
                Title.Caption = 'DS'
                Width = 64
              end
              item
                FieldName = 'PRECIO1'
                Title.Caption = 'PVP'
                Width = 64
              end
              item
                FieldName = 'MARGEN'
                Title.Caption = '%Marg.'
                Width = 64
              end
              item
                FieldName = 'TPCENCARTE1'
                Title.Caption = '%Enc.'
                Width = 75
              end
              item
                FieldName = 'TIVA'
                Title.Caption = 'IVA'
                Width = 64
              end
              item
                FieldName = 'PRECIOCOSTE'
                Title.Caption = 'Pr.Coste'
                Width = 75
              end
              item
                FieldName = 'PRECIOCOSTETOT'
                Title.Caption = 'Cost+Re'
                Width = 95
              end
              item
                FieldName = 'PRECIO2'
                Title.Caption = 'PVP'
                Width = 64
              end
              item
                FieldName = 'MARGEN2'
                Title.Caption = '%Marg.'
                Width = 64
              end
              item
                FieldName = 'TPCENCARTE2'
                Title.Caption = '%Enc.'
                Width = 75
              end
              item
                FieldName = 'TIVA2'
                Title.Caption = 'IVA'
                Width = 64
              end
              item
                FieldName = 'PRECIOCOSTE2'
                Title.Caption = 'Pr.Coste'
                Width = 81
              end
              item
                FieldName = 'PRECIOCOSTETOT2'
                Title.Caption = 'Cost+Re'
                Width = 101
              end
              item
                FieldName = 'FECHAPRECIO'
                Title.Caption = 'Fecha Precio'
                Width = 75
              end>
          end
        end
      end
    end
  end
  object pnBotonera: TUniPanel
    Left = 0
    Top = 0
    Width = 1063
    Height = 55
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    Caption = ''
    object UniBitBtn2: TUniBitBtn
      Left = 447
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Duplicar'
      Visible = False
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        DAD8D7A8A3A1A7A2A0A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3
        A1A8A3A1A7A19FABA8A7E3E3E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFBC6D3DC75409C2540EC2560FC15711C15913C15B16C15D18C05E1CC0
        611FC06323C06626CA6E2B975B33B8B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFD66627F4B690EEC8B7F2CEBAF5D2C0F6D4C3F8D8C5F9DAC7
        FBDBC7FDDCC8FEDDC9FEE0CDFFD5B6BA6E3BB6B4B4FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFCD6833EAD1C4E2E6F0DDD4D5DDD1D0E1D7D5E4DC
        D7E7DED9EADFD9EDE0DBEDE0DBEFE3DEEED4C4A95E317C6F699A928EA29E9DE9
        E9E9FFFFFFFFFFFF0000FFFFFFFFFFFFCB6832EED1C2DBCFCFCE6123E05F19DC
        631CDC651CDC671DDC6A1EDC6D21DC6E25DD722ADD7229D66C24CD6926D87630
        905B39CBCBCBFFFFFFFFFFFF0000FFFFFFFFFFFFC96833F0D7C9DCC9C6D45C16
        E5AC8FE2B9A6E7BEA9E8C2ADEAC5AFECC9B1EDC9B2EFCBB3F1CCB5F5D3BEFAD7
        C3FFC198A8683DCBCBCBFFFFFFFFFFFF0000FFFFFFFFFFFFC66834F2DBCCDECD
        C9D16727E6D6D1E3E5EEDFD7D7E1D7D6E5DCDBE8E1DDEAE2DEEEE4DFF0E5E1F0
        E6E2F2EAE7EDBFA69A5730847C799C9593B3B1B10000FFFFFFFFFFFFC66834F5
        DED0E2D1CED16526EAD8D2DAC4BCD25D1ADA5C18D8611AD8621BD9631BD8661C
        D8691FD86C23D86E28D86C24D16621C96524D5712E8667540000FFFFFFFFFFFF
        C46736F7E2D3E2D3D0CF6628EDE0DBDCBAABDA6B25E0B29DDEB4A0E1B7A1E2B9
        A5E4BCA8E5BFABE8C1AEEAC3AFEBC7B1EFCBB6F3CEB9FAB180926C530000FFFF
        FFFFFFFFC26739F9E4D5E4D5D3CE672BF0E4DFDDBDAEDA7A3FE5E1E5E4E3E9E9
        E8EDECECF1EFF1F6F3F6FAF6F9FEF9FEFFFBFFFFFDFFFFFFFFFFF9C6A58E6952
        0000FFFFFFFFFFFFBF663BFBEEE1E7E1E3CD682EF3E6E1DFC0B2D9783DE8DDDB
        E7DFE0EBE3E3EEE8E7F1ECEBF4F0EFF7F3F3F9F6F6FBF9F9FDFDFDFFFFFFF8C3
        A18E6A550000FFFFFFFFFFFFBF673BF3B78ADBA286CA662EF5ECE7E0C3B6D87A
        40EBE1E0EBE5E7EEE9E9F1ECECF4F0F0F7F4F4FAF7F7FBFAFAFDFCFCFFFFFFFF
        FFFFF7C5A58C6B560000FFFFFFFFFFFFC56B3AFFA13BED7925CA652DF8F1ECE2
        C5B7D87A43EFE7E5EEEAECF2EDEDF4F0F0F7F3F3F9F6F6FCF9F9FDFCFCFEFEFE
        FFFFFFFFFFFFF8C6A78C6C590000FFFFFFFFFFFFB57666C9825BB86B49C66734
        FDFCF8E3CFC8D77B45F1EAE9F1EEEFF4F0F0F6F3F3F9F6F6FBF9F9FDFCFCFEFD
        FDFFFFFFFFFFFFFFFFFFF7C6AA8C6B5B0000FFFFFFFFFFFFF7F2F2F0E8E7DDCB
        CAC46430F1B58AD68E69D67D48F4EFEEF4F1F3F7F3F3F9F5F6FCF9F9FDFBFBFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFF6C8AB8A6C5D0000FFFFFFFFFFFFFFFFFFFF
        FFFFE9DFDFD06E2FFFA33DEB7521D57E4CF6F2F2F6F5F6F9F6F6FBF9F9FDFBFB
        FEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7C8AD896C5F0000FFFFFFFFFFFF
        FFFFFFFFFFFFEFE6E6AD624DBA7451B15C38D58152FBFDFDFAFFFFFDFFFFFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CEB7896C600000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF6F1F0F2EBEBD5BBB7D3743DF1B894EDB897EE
        B696EEB796EFB796EEB796EEB796EEB796EEB796EEB896EFB794F2AA7F887067
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDC7C5E38134FF9C38
        FF9639FF9A3EFF9D44FFA049FFA44FFFA755FFAB5AFFAF60FFB266FFB769FFCC
        8B8B77720000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D6D5B56D
        50BF7A53BE7A59BE7C5DBE7F62BE8166BE846ABE876FBF8974BE8A77BD8977BD
        8D7EBF8C7FA998980000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = ''
      TabStop = False
      TabOrder = 1
    end
    object UniBitBtn10: TUniBitBtn
      Left = 501
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Crear Tipos'
      Visible = False
      ShowHint = True
      Caption = 'Crear'
      TabStop = False
      TabOrder = 2
    end
    object UniBitBtn11: TUniBitBtn
      Left = 555
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Selec Tipos'
      Visible = False
      ShowHint = True
      Caption = 'Tipos'
      TabStop = False
      TabOrder = 3
    end
    object btNuevoArticulo: TUniBitBtn
      Left = 8
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Nuevo'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 4
      Images = DMppal.ImgListPrincipal
      ImageIndex = 34
      OnClick = btNuevoArticuloClick
    end
    object btModificarArti: TUniBitBtn
      Left = 116
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Modificar'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 5
      Images = DMppal.ImgListPrincipal
      ImageIndex = 3
      OnClick = btModificarArtiClick
    end
    object btConfirmarArti: TUniBitBtn
      Left = 170
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Aplicar'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 6
      Images = DMppal.ImgListPrincipal
      ImageIndex = 38
      OnClick = btConfirmarArtiClick
    end
    object btCancelarArti: TUniBitBtn
      Left = 224
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Cancelar'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 7
      Images = DMppal.ImgListPrincipal
      ImageIndex = 39
      OnClick = btCancelarArtiClick
    end
    object btBorrarArti: TUniBitBtn
      Left = 62
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Anular'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 8
      Images = DMppal.ImgListPrincipal
      ImageIndex = 35
      OnClick = btBorrarArtiClick
    end
    object btHistorico: TUniBitBtn
      Left = 355
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Historico'
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 9
      Images = DMppal.ImgListPrincipal
      ImageIndex = 33
      OnClick = btHistoricoClick
    end
    object UniBitBtn1: TUniBitBtn
      Left = 919
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Anterior'
      Visible = False
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 10
      ImageIndex = 36
    end
    object UniBitBtn3: TUniBitBtn
      Left = 973
      Top = 4
      Width = 55
      Height = 42
      Hint = 'Siguiente'
      Visible = False
      ShowHint = True
      Caption = ''
      TabStop = False
      TabOrder = 11
      ImageIndex = 37
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 55
    Width = 1063
    Height = 418
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object UniLabel1: TUniLabel
      Left = 38
      Top = 87
      Width = 54
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Descripci'#243'n'
      TabOrder = 21
    end
    object UniLabel3: TUniLabel
      Left = 32
      Top = 119
      Width = 60
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Distribuidora'
      TabOrder = 22
    end
    object UniLabel4: TUniLabel
      Left = 26
      Top = 150
      Width = 66
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Tipo Producto'
      TabOrder = 23
    end
    object UniLabel5: TUniLabel
      Left = 328
      Top = 184
      Width = 73
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Dias Caducidad'
      TabOrder = 24
    end
    object UniLabel16: TUniLabel
      Left = 352
      Top = 150
      Width = 50
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 's/Ref.[F5]'
      TabOrder = 25
    end
    object UniLabel18: TUniLabel
      Left = 35
      Top = 184
      Width = 57
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Periodicidad'
      TabOrder = 26
    end
    object UniDBEdit1: TUniDBEdit
      Left = 95
      Top = 83
      Width = 432
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'DESCRIPCION'
      DataSource = dsFichaArticulo
      CharCase = ecUpperCase
      ParentFont = False
      Font.Height = -13
      TabOrder = 2
    end
    object UniDBEdit4: TUniDBEdit
      Left = 404
      Top = 146
      Width = 123
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'REFEPROVE'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 6
    end
    object edDiasCadu: TUniDBEdit
      Left = 404
      Top = 180
      Width = 49
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'CADUCIDAD'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 7
    end
    object UniDBLookupComboBox1: TUniDBLookupComboBox
      Left = 95
      Top = 113
      Width = 432
      Height = 24
      Hint = ''
      ShowHint = True
      ListField = 'NOMBRE'
      ListSource = dsEditorialS
      KeyField = 'ID_PROVEEDOR'
      ListFieldIndex = 0
      ClearButton = True
      DataField = 'EDITORIAL'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      AnyMatch = True
      TabOrder = 3
      Color = clWindow
      RemoteQuery = True
      Style = csDropDown
    end
    object edTipoProducto: TUniDBLookupComboBox
      Left = 95
      Top = 146
      Width = 219
      Height = 24
      Hint = ''
      ShowHint = True
      ListField = 'DESCRIPCION'
      ListSource = dsTipoProdu
      KeyField = 'NORDEN'
      ListFieldIndex = 0
      DataField = 'TPRODUCTO'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      AnyMatch = True
      TabOrder = 4
      Color = clWindow
      Style = csDropDown
      OnChange = edTipoProductoChange
    end
    object edPeriodicidad: TUniDBLookupComboBox
      Left = 95
      Top = 180
      Width = 219
      Height = 24
      Hint = ''
      ShowHint = True
      ListField = 'DESCRIPCION'
      ListSource = dsPeriodicidad
      KeyField = 'NORDEN'
      ListFieldIndex = 0
      DataField = 'PERIODICIDAD'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      AnyMatch = True
      TabOrder = 5
      Color = clWindow
      Style = csDropDown
    end
    object edBarras: TUniDBEdit
      Left = 95
      Top = 53
      Width = 211
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'BARRAS'
      DataSource = dsFichaArticulo
      MaxLength = 18
      ParentFont = False
      Font.Height = -13
      TabOrder = 1
      Color = 15520476
    end
    object rgTipoBarras: TUniDBRadioGroup
      Left = 312
      Top = 6
      Width = 215
      Height = 73
      Hint = ''
      ShowHint = True
      DataField = 'TBARRAS'
      DataSource = dsFichaArticulo
      Caption = 'Tipo Codigo de Barras'
      ParentFont = False
      Font.Height = -13
      TabOrder = 27
      Items.Strings = (
        '0'
        '7'
        '9'
        '13'
        '15'
        '18')
      Columns = 3
    end
    object UniLabel2: TUniLabel
      Left = 25
      Top = 56
      Width = 67
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Codigo Barras'
      TabOrder = 28
    end
    object btAceptar: TUniButton
      Left = 541
      Top = 27
      Width = 55
      Height = 42
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = ''
      TabOrder = 20
      Images = DMppal.ImgListPrincipal
      ImageIndex = 38
      OnClick = btAceptarClick
    end
    object btCancelar: TUniButton
      Left = 628
      Top = 27
      Width = 55
      Height = 42
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = ''
      TabOrder = 29
      Images = DMppal.ImgListPrincipal
      ImageIndex = 39
      OnClick = btCancelarClick
    end
    object edPreu: TUniDBEdit
      Left = 79
      Top = 251
      Width = 69
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIO1'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 8
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edPreuExit
      OnEnter = edPreuEnter
      OnKeyPress = edPreuKeyPress
    end
    object edMargen: TUniDBEdit
      Left = 152
      Top = 251
      Width = 68
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'MARGEN'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 9
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edMargenExit
      OnEnter = edMargenEnter
      OnKeyPress = edPreuKeyPress
    end
    object edMargen2: TUniEdit
      Left = 152
      Top = 285
      Width = 68
      Height = 29
      Hint = ''
      Visible = False
      ShowHint = True
      Text = ''
      ParentFont = False
      Font.Height = -13
      TabOrder = 17
      OnEnter = edMargen2Enter
      OnKeyPress = edPreuKeyPress
    end
    object edEncarte1: TUniDBEdit
      Left = 855
      Top = 119
      Width = 40
      Height = 25
      Hint = ''
      Visible = False
      ShowHint = True
      DataField = 'TPCENCARTE1'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 10
      OnExit = edEncarte1Exit
    end
    object UniLabel69: TUniLabel
      Left = 104
      Top = 234
      Width = 21
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'PVP '
      TabOrder = 30
    end
    object UniLabel70: TUniLabel
      Left = 163
      Top = 234
      Width = 47
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = '%Margen'
      TabOrder = 31
    end
    object UniLabel71: TUniLabel
      Left = 851
      Top = 104
      Width = 48
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = '%Encarte'
      TabOrder = 32
    end
    object edTIva1: TUniDBEdit
      Left = 223
      Top = 251
      Width = 38
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TIVA'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 11
      OnExit = edTIva1Exit
      OnEnter = edTIva1Enter
    end
    object edTPCIVA1: TUniDBEdit
      Left = 265
      Top = 251
      Width = 50
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TPCIVA1'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 12
    end
    object edTPCRE1: TUniDBEdit
      Left = 320
      Top = 251
      Width = 51
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TPCRE1'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 13
      OnEnter = edTPCRE1Enter
    end
    object edCoste1: TUniDBEdit
      Left = 375
      Top = 251
      Width = 74
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIOCOSTE'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 14
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edCoste1Exit
      OnEnter = edCoste1Enter
      OnKeyPress = edPreuKeyPress
    end
    object edCosteDire: TUniDBEdit
      Left = 375
      Top = 285
      Width = 66
      Height = 29
      Hint = ''
      Visible = False
      ShowHint = True
      DataField = 'PRECIOCOSTE'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 18
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edCosteDireExit
      OnEnter = edCosteDireEnter
      OnKeyPress = edPreuKeyPress
    end
    object edSumarCoste: TUniEdit
      Left = 455
      Top = 285
      Width = 77
      Height = 29
      Hint = ''
      Visible = False
      ShowHint = True
      Text = ''
      ParentFont = False
      Font.Height = -13
      TabOrder = 19
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edSumarCosteExit
      OnEnter = edSumarCosteEnter
      OnKeyPress = edPreuKeyPress
    end
    object UniLabel73: TUniLabel
      Left = 277
      Top = 234
      Width = 25
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'I.V.A'
      TabOrder = 33
    end
    object UniLabel74: TUniLabel
      Left = 329
      Top = 234
      Width = 17
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'RE.'
      TabOrder = 34
    end
    object UniLabel75: TUniLabel
      Left = 389
      Top = 234
      Width = 42
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Pr.Coste'
      TabOrder = 35
    end
    object UniLabel76: TUniLabel
      Left = 467
      Top = 234
      Width = 49
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Coste+RE'
      TabOrder = 36
    end
    object UniLabel77: TUniLabel
      Left = 442
      Top = 289
      Width = 12
      Height = 19
      Hint = ''
      ShowHint = True
      Caption = '+'
      ParentFont = False
      Font.Height = -16
      TabOrder = 37
    end
    object UniSpeedButton34: TUniSpeedButton
      Left = 281
      Top = 285
      Width = 87
      Height = 29
      Hint = ''
      ShowHint = True
      Caption = 'In.Coste'
      ParentColor = False
      Color = clWindow
      TabOrder = 38
      OnClick = UniSpeedButton34Click
    end
    object UniDBEdit15: TUniDBEdit
      Left = 455
      Top = 251
      Width = 77
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIOCOSTETOT'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 15
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnEnter = UniDBEdit15Enter
      OnKeyPress = edPreuKeyPress
    end
    object UniBitBtn15: TUniBitBtn
      Left = 541
      Top = 372
      Width = 55
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Asociado'
      TabOrder = 39
    end
    object UniLabel37: TUniLabel
      Left = 83
      Top = 218
      Width = 449
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 
        '________________________________Precio Normal___________________' +
        '_____________'
      TabOrder = 40
    end
    object btTecNume: TUniBitBtn
      Left = 19
      Top = 272
      Width = 57
      Height = 42
      Hint = ''
      ShowHint = True
      Caption = 'Margen'
      TabOrder = 41
      OnClick = btTecNumeClick
    end
    object edPreu2: TUniEdit
      Left = 79
      Top = 285
      Width = 69
      Height = 29
      Hint = ''
      ShowHint = True
      Text = ''
      ParentFont = False
      Font.Height = -13
      TabOrder = 42
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnEnter = edPreu2Enter
      OnKeyPress = edPreuKeyPress
    end
    object UniLabel30: TUniLabel
      Left = 104
      Top = 335
      Width = 21
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'PVP '
      TabOrder = 43
    end
    object UniLabel31: TUniLabel
      Left = 163
      Top = 335
      Width = 47
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = '%Margen'
      TabOrder = 44
    end
    object UniLabel32: TUniLabel
      Left = 855
      Top = 164
      Width = 48
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = '%Encarte'
      TabOrder = 45
    end
    object UniLabel33: TUniLabel
      Left = 276
      Top = 335
      Width = 25
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'I.V.A'
      TabOrder = 46
    end
    object UniLabel34: TUniLabel
      Left = 337
      Top = 335
      Width = 17
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'RE.'
      TabOrder = 47
    end
    object UniLabel35: TUniLabel
      Left = 389
      Top = 335
      Width = 42
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Pr.Coste'
      TabOrder = 48
    end
    object UniLabel36: TUniLabel
      Left = 467
      Top = 335
      Width = 49
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Coste+RE'
      TabOrder = 49
    end
    object edPVP2: TUniDBEdit
      Left = 79
      Top = 350
      Width = 69
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIO2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 50
      ClientEvents.ExtEvents.Strings = (
        'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  '#13#10'}')
      OnExit = edPVP2Exit
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit17: TUniDBEdit
      Left = 79
      Top = 385
      Width = 168
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'COD_ARTICU'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 51
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit18: TUniDBEdit
      Left = 152
      Top = 350
      Width = 68
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'MARGEN2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 52
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edPVP2Exit
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit20: TUniDBEdit
      Left = 855
      Top = 179
      Width = 40
      Height = 20
      Hint = ''
      Visible = False
      ShowHint = True
      DataField = 'TPCENCARTE2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 53
      OnExit = edPVP2Exit
    end
    object edTIVA2: TUniDBEdit
      Left = 224
      Top = 350
      Width = 37
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TIVA2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 54
      OnExit = edTIVA2Exit
    end
    object UniDBEdit22: TUniDBEdit
      Left = 265
      Top = 350
      Width = 49
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TPCIVA2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 55
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit23: TUniDBEdit
      Left = 320
      Top = 350
      Width = 51
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'TPCRE2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 56
      OnKeyPress = edPreuKeyPress
    end
    object edCoste2: TUniDBEdit
      Left = 375
      Top = 350
      Width = 74
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIOCOSTE2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 57
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnExit = edCoste2Exit
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit26: TUniDBEdit
      Left = 251
      Top = 385
      Width = 281
      Height = 29
      Hint = ''
      ShowHint = True
      ParentFont = False
      Font.Height = -13
      TabOrder = 58
      OnKeyPress = edPreuKeyPress
    end
    object UniDBEdit27: TUniDBEdit
      Left = 455
      Top = 350
      Width = 77
      Height = 29
      Hint = ''
      ShowHint = True
      DataField = 'PRECIOCOSTETOT2'
      DataSource = dsFichaArticulo
      ParentFont = False
      Font.Height = -13
      TabOrder = 59
      ClientEvents.ExtEvents.Strings = (
        
          'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
          ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
          'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
          'opEvent() ;'#13#10'   } '#13#10'}')
      OnKeyPress = edPreuKeyPress
    end
    object UniLabel38: TUniLabel
      Left = 80
      Top = 319
      Width = 452
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 
        '________________________________Precio Adicional________________' +
        '_______________'
      TabOrder = 60
    end
    object UniLabel6: TUniLabel
      Left = 232
      Top = 234
      Width = 20
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Tipo'
      TabOrder = 61
    end
    object UniLabel7: TUniLabel
      Left = 232
      Top = 335
      Width = 20
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Tipo'
      TabOrder = 62
    end
    object edCodbarras: TUniEdit
      Left = 95
      Top = 22
      Width = 211
      Hint = ''
      Visible = False
      ShowHint = True
      MaxLength = 18
      Text = ''
      ParentFont = False
      Font.Height = -13
      TabOrder = 0
      OnExit = edCodbarrasExit
      OnEnter = edCodbarrasEnter
    end
    object lbCodigoBarras: TUniLabel
      Left = 95
      Top = 3
      Width = 115
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Entrar Codigo de Barras'
      ParentFont = False
      Font.Color = clNavy
      TabOrder = 63
    end
    object lbGrabar: TUniLabel
      Left = 532
      Top = 8
      Width = 72
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Grabar Art'#237'culo'
      TabOrder = 64
    end
    object lbVolver: TUniLabel
      Left = 613
      Top = 8
      Width = 94
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Volver a Devolucion'
      TabOrder = 65
    end
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
  object dsFichaArticulo: TDataSource
    DataSet = DMMenuArti.sqlFichaArticulo
    Left = 984
    Top = 518
  end
  object dsEditorialS: TDataSource
    DataSet = DMMenuArti.sqlEditorialS
    Left = 984
    Top = 486
  end
  object dsArTitulo: TDataSource
    DataSet = DMMenuArti.sqlArTitulo
    Left = 984
    Top = 446
  end
  object dsPreus: TDataSource
    DataSet = DMMenuArti.sqlPreus
    Left = 984
    Top = 406
  end
  object dsPeriodicidad: TDataSource
    DataSet = DMMenuArti.sqlPeriodi
    Left = 992
    Top = 558
  end
  object dsVarisSubGrupo: TDataSource
    DataSet = DMMenuArti.sqlVarisSubGrupo
    Left = 920
    Top = 582
  end
  object dsTipoProdu: TDataSource
    DataSet = DMMenuArti.sqlTProdu
    Left = 928
    Top = 534
  end
end
