unit uLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniCheckBox, uniButton, uniEdit,
  uniLabel, uniGUIBaseClasses, uniPanel;

type
  TUniLoginForm2 = class(TUniLoginForm)
    UniContainerPanel4: TUniContainerPanel;
    UniLabel20: TUniLabel;
    edUsuario: TUniEdit;
    edPassword: TUniEdit;
    UniLabel21: TUniLabel;
    edAceptarLogin: TUniButton;
    lbPath: TUniLabel;
    cbRecuerdame: TUniCheckBox;
    btLogout: TUniButton;
    cbDesconectar: TUniCheckBox;
    edNuevaPass: TUniEdit;
    edConfirmarPass: TUniEdit;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    btCambiarPass: TUniButton;
    UniLabel3: TUniLabel;
    lbNuevoUsuario: TUniLabel;
    procedure edAceptarLoginClick(Sender: TObject);
    procedure UniLoginFormShow(Sender: TObject);
    procedure btLogoutClick(Sender: TObject);
    procedure edUsuarioEnter(Sender: TObject);
    procedure edPasswordEnter(Sender: TObject);
    procedure cbDesconectarClick(Sender: TObject);
    procedure btCambiarPassClick(Sender: TObject);
    procedure lbNuevoUsuarioClick(Sender: TObject);
  private
    function SumarFecha(Fecha: TDateTime; Anos, Meses,
      Dias: Integer): TDateTime;
    { Private declarations }
  public
    { Public declarations }
  end;

function UniLoginForm2: TUniLoginForm2;

implementation

{$R *.dfm}

uses
  uniGUIVars,uConstVar, uMenu, uDMppal, uNuevoUsuario, uniGUIApplication, uMensajes;

function UniLoginForm2: TUniLoginForm2;
begin
  Result := TUniLoginForm2(DMppal.GetFormInstance(TUniLoginForm2));
end;

procedure TUniLoginForm2.btCambiarPassClick(Sender: TObject);
var
  vNuevaPassword : String;
begin
  if edNuevaPass.Text = edConfirmarPass.Text then
  begin

    vNuevaPassword := edConfirmarPass.Text;
    if DMppal.sqlUsuarioTIPOPASSW.AsInteger = 1 then
      vNuevaPassword := IntToStr(RutCalcularPassword(edConfirmarPass.Text));

    DMppal.sqlUsuario.Edit;

    DMppal.sqlUsuarioPASS.AsString            := vNuevaPassword;
    DMppal.sqlUsuarioFECHAPASSWORD.AsDateTime := now -1;

    DMppal.sqlUsuario.Post;

    UniLoginForm2.Height := 267;
    edPassword.Text := edNuevaPass.Text;

    edAceptarLoginClick(nil);
  end else
  begin
    FormMenu.vResult := '99';
    FormMensajes.RutInicioForm(3, UpperCase(self.Name), 'ACEPTAR', '�Las contrase�as no coinciden!');
    FormMensajes.ShowModal();
    edNuevaPass.Text := '';
    edNuevaPass.Text := '';
  end;


end;

procedure TUniLoginForm2.btLogoutClick(Sender: TObject);
begin
  UniApplication.Cookies.SetCookie('_loginname_kioslib','',Date-1);
  UniApplication.Cookies.SetCookie('_pwd_kioslib','',Date-1);
  edUsuario.Text  := '';
  edPassword.Text := '';
  edUsuario.SetFocus;
  //UniApplication.Restart;
end;

procedure TUniLoginForm2.cbDesconectarClick(Sender: TObject);
begin
  btLogoutClick(nil);
end;

procedure TUniLoginForm2.edAceptarLoginClick(Sender: TObject);
var
  vDias : Integer;
begin
  //edAceptarLogin.Enabled := False; //NO ENTIENDO POR QUE PONERLO A FALSE
  lbPath.Visible := True;
  lbPath.Caption := 'Correcto, esperando conexi�n...';

  if DMppal.RutValidarUsuario(edUsuario.Text, edPassword.Text) then
  begin
    vDias := DMppal.sqlUsuarioACTUALIZACIONAUTO.AsInteger;
    if vDias = 0 then vDias := 90;

    if now > SumarFecha(DMppal.sqlUsuarioFECHAPASSWORD.AsDateTime,0,0,vDias) then
    begin

    UniLoginForm2.Height := 465;
    exit;

    end;

    if DMppal.RutConectarBD(IntToStr(DMppal.sqlUsuarioNUMEROUSUARIO.AsInteger)) then
    begin
      DMppal.RutActualizarbaseDatos;

      if cbRecuerdame.Checked then
      begin
        UniApplication.Cookies.SetCookie('_loginname_kioslib', edUsuario.Text, Date + 7.0); // Expires 7 days from now
        UniApplication.Cookies.SetCookie('_pwd_kioslib', edPassword.Text, Date + 7.0);
      end;

      FormMenu.Show;
      FormMenu.WindowState := wsMaximized;

    end
    else
    begin
      //AQUI VA LA CARGA DEL NUEVO FORMULARIO CON EL MENSAJE DE ERROR
    //  MessageDlg('No se pudo conectar',mtWarning, [mbOK], nil);


      FormMensajes.RutInicioForm(1, UpperCase(self.Name), 'ACEPTAR', '�No se pudo Conectar a la Base de datos!');
      FormMensajes.ShowModal();
    end;
  end
  else
  begin
    edAceptarLogin.Enabled := True;
    FormMensajes.RutInicioForm(2, UpperCase(self.Name), 'ACEPTAR', 'Usuario Incorrecto');
    FormMensajes.ShowModal();
    btLogoutClick(nil);
  end;
end;

function TUniLoginForm2.SumarFecha(Fecha:TDateTime; Anos,Meses,Dias:Integer):TDateTime;
var dia1,mes1,ano1: integer;
    dia2,mes2,ano2: word;
begin
  DecodeDate(Fecha, ano2, mes2, dia2);
  ano1 := (ano2 + Anos);
  mes1 := (mes2 + Meses - 1);
  dia1 := (dia2 + Dias - 1);

  if (mes1 > 0) then begin
      ano1 := (ano1 + (mes1 div 12));
      mes1 := ((mes1 mod 12) + 1);
  end
  else mes1 := 1;

  result := EncodeDate(ano1,mes1,1) + dia1;
end;

procedure TUniLoginForm2.edPasswordEnter(Sender: TObject);
begin
  lbPath.Visible := False;
  edAceptarLogin.Enabled := True;
end;

procedure TUniLoginForm2.edUsuarioEnter(Sender: TObject);
begin
  lbPath.Visible := False;
  edAceptarLogin.Enabled := True;
end;

procedure TUniLoginForm2.lbNuevoUsuarioClick(Sender: TObject);
begin
  FormNuevoUsuario.ShowModal();
  FormNuevoUsuario.vNuevoUsu := True;

end;

procedure TUniLoginForm2.UniLoginFormShow(Sender: TObject);
var
  S1, S2 : string;
begin

  edAceptarLogin.Enabled := True;

  S1 := UniApplication.Cookies.Values['_loginname_kioslib'];
  S2 := UniApplication.Cookies.Values['_pwd_kioslib'];

  edUsuario.Text := S1;
  edpassword.Text:= S2;

  UniLoginForm2.Height := 240;
end;

initialization
  RegisterAppFormClass(TUniLoginForm2);

end.
