object FormDistribuidoraLista: TFormDistribuidoraLista
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 560
  ClientWidth = 1000
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gridDistribuidoraLista: TUniDBGrid
    Left = 0
    Top = 50
    Width = 1000
    Height = 510
    Hint = ''
    ShowHint = True
    ClientEvents.UniEvents.Strings = (
      
        'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls=' +
        '"mGridCliente";'#13#10'  config.itemHeight = 30;'#13#10'  config.headerConta' +
        'iner = {height: 30};'#13#10'}')
    DataSource = dsCabeS
    ReadOnly = True
    WebOptions.Paged = False
    WebOptions.FetchAll = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Height = -13
    ParentFont = False
    TabOrder = 0
    OnColumnSort = gridDistribuidoraListaColumnSort
    Columns = <
      item
        FieldName = 'ID_PROVEEDOR'
        Title.Alignment = taCenter
        Title.Caption = 'Id'
        Width = 64
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'NOMBRE'
        Title.Caption = 'Nombre'
        Width = 244
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'DIRECCION'
        Title.Caption = 'Direcci'#243'n'
        Width = 244
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'POBLACION'
        Title.Caption = 'Poblaci'#243'n'
        Width = 244
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'PROVINCIA'
        Title.Caption = 'Provincia'
        Width = 244
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'CPOSTAL'
        Title.Alignment = taCenter
        Title.Caption = 'C.Postal'
        Width = 52
        Font.Height = -13
        Alignment = taCenter
        Sortable = True
      end
      item
        FieldName = 'NIF'
        Title.Caption = 'NIF'
        Width = 94
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'TELEFONO1'
        Title.Caption = 'Telefono'
        Width = 94
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'MOVIL'
        Title.Caption = 'Movil'
        Width = 94
        Font.Height = -13
        Sortable = True
      end
      item
        FieldName = 'FAX'
        Title.Caption = 'Fax'
        Width = 94
        Font.Height = -13
        Sortable = True
      end>
  end
  object pnlBtsLista: TUniPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 50
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    BorderStyle = ubsSingle
    Caption = ''
    object BtnDevolEnCurso: TUniBitBtn
      Left = 244
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888800000088888008888888888800000088880FF000000000080000008880
        F0080FFFFFFF08000000880F0FF00F00000F0800000080F0F0080FFFFFFF0800
        0000880F0FF00F00000F0800000080F0F0080FFFFFFF08000000880F0FB00F00
        F0000800000080F0FBFB0FFFF0F088000000880FBFBF0FFFF0088800000080FB
        FBFB00000088880000008800BFBFBFBF088888000000888800FBFBF088888800
        000088888800B808888888000000888888880088888888000000888888888888
        888888000000888888888888888888000000}
      Caption = ''
      TabOrder = 1
      ImageIndex = 13
    end
    object BtnDevolEnviadas: TUniBitBtn
      Left = 305
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 2
    end
    object spTodas: TUniBitBtn
      Left = 366
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77770000000000000007FFFFFFFFFFFFFFF00000F000F00F000F70910BB30E60
        330070910BB30E60330070910BB30E60330070910BB30E60330070910BB30E60
        330070910BB30E603300709100000E603300709100FF0E6000007000010F0E60
        0F00700F0100000000007700F00700FF00777770000770000077}
      Caption = ''
      TabOrder = 3
    end
    object BtnDocumentoCabe: TUniBitBtn
      Left = 427
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888000000080000000000000008000000080F8FFFFFFFFFFF0800000008089
        9FFF899998F08000000080F98FFFF8888FF08000000080FFFFFFFFFFFFF08000
        000080F7F447844447F08000000080F8F888F88888F08000000080F8F7788777
        78F08000000080F7F747847747F08000000080FFFFFFF8888FF0800000008080
        0007FFFFFFF08000000080F8FF8FFFFFFFF08000000080866666F88888808000
        000080E767EEEEEEEE708000000080E8E7EEEEEEEE8080000000800000000000
        000080000000}
      Caption = ''
      TabOrder = 4
    end
    object UniLabel85: TUniLabel
      Left = 504
      Top = 310
      Width = 54
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Documento'
      TabOrder = 5
    end
    object edDocumentoProve: TUniDBEdit
      Left = 496
      Top = 323
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 6
    end
    object spVerDocumentoProveedor: TUniBitBtn
      Left = 657
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000F0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0087FFFFFFFFFFFF0B3087FFFFFFFFFFF0BB0087FF
        FFFFFFFF0BB3008FFFFFFFFFF0BBB008FFFFFFFFF00BBB007FFFFFFF00BBB007
        FFF0FFFFF00BBB007FF0FFFFFFF00BB007F0FFFFFFFFF00B0070FFFFFFFFFFF0
        00F0FFFFFFFFFFFFFFF0}
      Caption = ''
      TabOrder = 7
      ImageIndex = 13
    end
    object btEtiquetasDocumento: TUniBitBtn
      Left = 718
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42050000424D4205000000000000360000002800000016000000130000000100
        1800000000000C05000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FFFFFF000000000000000000000000FFFFFFC0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C00000000000000000000000000000000000000000000000C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C0000000C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0008000008000C0C0
        C00000FF0000FFC0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C00000
        00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0000000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFC0C0C0C0
        C0C0808080000000000000808080808080808080808080808080808080808080
        8080808080808080808080808080800000000000008080800000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000FFFFFFFFFFFF00008080
        80000000000000808080808080808080000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000FFFFFF000000
        000000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000
        0000FFFFFF000000000000000000000000000000000000000000000000FFFFFF
        000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000000000FFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF000000FFFFFF0000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000
        000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000}
      Caption = ''
      TabOrder = 8
    end
    object btInicioConfiguracion: TUniBitBtn
      Left = 786
      Top = 318
      Width = 34
      Height = 27
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000013000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        3333333000003243342222224433333000003224422222222243333000003222
        222AAAAA222433300000322222A33333A222433000003222223333333A224330
        00003222222333333A44433000003AAAAAAA3333333333300000333333333333
        3333333000003333333333334444443000003A444333333A2222243000003A22
        43333333A2222430000033A22433333442222430000033A22244444222222430
        0000333A2222222222AA243000003333AA222222AA33A3300000333333AAAAAA
        333333300000333333333333333333300000}
      Caption = ''
      TabOrder = 9
    end
    object BtnPreProve: TUniBitBtn
      Left = 838
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000007777777777777777770000007777777777770007770000007444
        4400000006007700000074FFFF08880600080700000074F008000060EE070700
        000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
        00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
        080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
        000074F444F444F444F477000000744444444444444477000000777777777777
        777777000000777777777777777777000000}
      Caption = ''
      TabOrder = 10
    end
    object UniDateTimePicker1: TUniDateTimePicker
      Left = 103
      Top = 310
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 11
    end
    object UniDateTimePicker2: TUniDateTimePicker
      Left = 103
      Top = 331
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 12
    end
    object UniLabel86: TUniLabel
      Left = 103
      Top = 296
      Width = 43
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'En Curso'
      TabOrder = 13
    end
    object UniLabel87: TUniLabel
      Left = 68
      Top = 310
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 14
    end
    object UniLabel88: TUniLabel
      Left = 68
      Top = 331
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 15
    end
    object btFicha: TUniBitBtn
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 55
      Height = 42
      Hint = 'Ficha'
      Margins.Bottom = 0
      ShowHint = True
      Caption = ''
      TabOrder = 16
      IconAlign = iaTop
      Images = DMppal.ImgListPrincipal
      ImageIndex = 13
      OnClick = btFichaClick
    end
    object RgTProve: TUniRadioGroup
      Left = 588
      Top = 0
      Width = 313
      Height = 47
      Hint = ''
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Visible = False
      ShowHint = True
      Items.Strings = (
        'Distribuidor'
        'Dis. y Edit.'
        'Editorial'
        'Otros')
      Caption = 'Tipo Proveedor'
      TabOrder = 17
      Columns = 4
      Vertical = False
      OnClick = RgTProveClick
      OnChangeValue = RgTProveChangeValue
    end
    object edFiltroCabe: TUniEdit
      Left = 173
      Top = 16
      Width = 272
      Hint = ''
      ShowHint = True
      Text = ''
      TabOrder = 18
      OnChange = edFiltroCabeChange
    end
    object UniLabel1: TUniLabel
      Left = 143
      Top = 19
      Width = 24
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Filtro'
      TabOrder = 19
    end
    object btNuevaDistribuidora: TUniBitBtn
      Left = 66
      Top = 6
      Width = 55
      Height = 42
      Hint = 'Nueva Distribuidora'
      ShowHint = True
      Caption = ''
      TabOrder = 20
      Images = DMppal.ImgListPrincipal
      ImageIndex = 34
      OnClick = btNuevaDistribuidoraClick
    end
  end
  object dsCabeS: TDataSource
    DataSet = DMDistribuidora.sqlCabeS
    Left = 880
    Top = 360
  end
end
