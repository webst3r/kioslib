unit uAlbaranRecepcion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBCheckBox;

type
  TFormAlbaran = class(TUniForm)
    tmSession: TUniTimer;
    UniPageControl3: TUniPageControl;
    UniTabSheet7: TUniTabSheet;
    UniPanel13: TUniPanel;
    UniPageControl4: TUniPageControl;
    UniTabSheet9: TUniTabSheet;
    UniTabSheet10: TUniTabSheet;
    GridLinea: TUniDBGrid;
    GridResumen: TUniDBGrid;
    UniLabel47: TUniLabel;
    UniLabel48: TUniLabel;
    UniLabel49: TUniLabel;
    UniLabel50: TUniLabel;
    edBarras: TUniDBEdit;
    edAdendum: TUniDBEdit;
    edCantiEnAlba: TUniDBEdit;
    edSuscripcion: TUniDBEdit;
    btSumarRecepcion: TUniSpeedButton;
    btRestarRecepcion: TUniSpeedButton;
    UniDBEdit50: TUniDBEdit;
    UniDBEdit51: TUniDBEdit;
    UniDBEdit52: TUniDBEdit;
    UniLabel52: TUniLabel;
    UniDBEdit53: TUniDBEdit;
    UniDBEdit54: TUniDBEdit;
    UniLabel53: TUniLabel;
    UniDBEdit55: TUniDBEdit;
    btPrior: TUniSpeedButton;
    btNext: TUniSpeedButton;
    btGrabaLinea: TUniBitBtn;
    btCancelaLinea: TUniBitBtn;
    edAbono: TUniDBDateTimePicker;
    edCargo: TUniDBDateTimePicker;
    edAvisosDevol: TUniDBDateTimePicker;
    edDevolucion: TUniDBDateTimePicker;
    UniLabel54: TUniLabel;
    UniLabel55: TUniLabel;
    UniLabel56: TUniLabel;
    UniLabel57: TUniLabel;
    UniPanel19: TUniPanel;
    UniLabel81: TUniLabel;
    UniPanel20: TUniPanel;
    edNumInferior: TUniDBEdit;
    btNuminferiorOk: TUniBitBtn;
    btNumInferiorNo: TUniBitBtn;
    UniPanel1: TUniPanel;
    UniDBEdit89: TUniDBEdit;
    UniLabel78: TUniLabel;
    UniDBEdit90: TUniDBEdit;
    UniDBEdit91: TUniDBEdit;
    UniDBEdit92: TUniDBEdit;
    UniDBEdit93: TUniDBEdit;
    UniDBEdit94: TUniDBEdit;
    UniLabel79: TUniLabel;
    UniLabel80: TUniLabel;
    UniPanel2: TUniPanel;
    btNuevaLinea: TUniBitBtn;
    btBorrarLinea: TUniBitBtn;
    btReclamar: TUniBitBtn;
    btResumenCargos: TUniBitBtn;
    btPrecios: TUniBitBtn;
    btEditarLinea: TUniBitBtn;
    UniPanel3: TUniPanel;
    UniLabel66: TUniLabel;
    UniDBEdit68: TUniDBEdit;
    btTecNume: TUniBitBtn;
    btActualizaArticulo: TUniSpeedButton;
    UniLabel67: TUniLabel;
    UniLabel68: TUniLabel;
    edPreu: TUniDBEdit;
    edPreu2: TUniDBEdit;
    edVentaCoste: TUniDBEdit;
    edMargen: TUniDBEdit;
    edMargen2: TUniDBEdit;
    MargenCoste: TUniDBEdit;
    edEncarte2: TUniDBEdit;
    edEncarte1: TUniDBEdit;
    UniLabel69: TUniLabel;
    UniLabel70: TUniLabel;
    UniLabel71: TUniLabel;
    edTIva2: TUniDBEdit;
    edTIva1: TUniDBEdit;
    UniLabel72: TUniLabel;
    UniDBEdit79: TUniDBEdit;
    UniDBEdit80: TUniDBEdit;
    UniDBEdit83: TUniDBEdit;
    UniDBEdit84: TUniDBEdit;
    UniDBEdit81: TUniDBEdit;
    UniDBEdit82: TUniDBEdit;
    edCosteDire: TUniDBEdit;
    UniDBEdit86: TUniDBEdit;
    UniLabel73: TUniLabel;
    UniLabel74: TUniLabel;
    UniLabel75: TUniLabel;
    UniLabel76: TUniLabel;
    UniDBEdit87: TUniDBEdit;
    edSumarCoste: TUniDBEdit;
    UniLabel77: TUniLabel;
    UniSpeedButton34: TUniSpeedButton;
    UniPanel4: TUniPanel;
    btBorrar: TUniSpeedButton;
    btNuevoAdendum: TUniSpeedButton;
    btDirectoDevolucion: TUniSpeedButton;
    btRecuperaDevolucion: TUniSpeedButton;
    UniDBCheckBox4: TUniDBCheckBox;
    UniLabel51: TUniLabel;
    edStock: TUniDBEdit;
    edCantiReal: TUniDBEdit;
    edReserva: TUniDBEdit;
    UniLabel58: TUniLabel;
    UniLabel59: TUniLabel;
    UniLabel60: TUniLabel;
    edStockMaximo: TUniDBEdit;
    UniDBEdit59: TUniDBEdit;
    UniLabel61: TUniLabel;
    UniLabel62: TUniLabel;
    UniLabel64: TUniLabel;
    UniLabel63: TUniLabel;
    edAcumCompras: TUniDBEdit;
    edAcumDevol: TUniDBEdit;
    UniPanel5: TUniPanel;
    GridUltiCompra: TUniDBGrid;
    UniLabel65: TUniLabel;
    UniDBEdit63: TUniDBEdit;
    UniDBEdit64: TUniDBEdit;
    UniDBEdit65: TUniDBEdit;
    UniDBEdit66: TUniDBEdit;
    UniDBEdit67: TUniDBEdit;
    UniTabSheet8: TUniTabSheet;
    UniPageControl1: TUniPageControl;
    tabDatosDocu: TUniTabSheet;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniButton1: TUniButton;
    UniGroupBox1: TUniGroupBox;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniRadioGroup1: TUniRadioGroup;
    UniDBCheckBox1: TUniDBCheckBox;
    UniDBMemo1: TUniDBMemo;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    tabDupliEntrada: TUniTabSheet;
    UniPanel8: TUniPanel;
    UniLabel13: TUniLabel;
    UniLabel14: TUniLabel;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniDBDateTimePicker4: TUniDBDateTimePicker;
    UniEdit1: TUniEdit;
    UniDBGrid1: TUniDBGrid;
    UniDBGrid2: TUniDBGrid;
    tabCambiarDistri: TUniTabSheet;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniLabel15: TUniLabel;
    UniLabel16: TUniLabel;


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormAlbaran: TFormAlbaran;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormAlbaran: TFormAlbaran;
begin
  Result := TFormAlbaran(DMppal.GetFormInstance(TFormAlbaran));

end;


end.
