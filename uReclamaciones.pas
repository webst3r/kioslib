unit uReclamaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage;

type
  TFormReclamaciones = class(TUniForm)
    pnltop: TUniPanel;
    RGTipo: TUniRadioGroup;
    RgDistribu: TUniRadioGroup;
    RgTReclama: TUniRadioGroup;
    UniPanel1: TUniPanel;
    BtnBuscaDistribui: TUniButton;
    pDistriCodi: TUniDBEdit;
    pDistriNom: TUniDBEdit;
    edDesdeFecha: TUniDateTimePicker;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    edHastaFecha: TUniDateTimePicker;
    UniPanel2: TUniPanel;
    UniDBGrid1: TUniDBGrid;
    UniPanel4: TUniPanel;
    BtnReclama: TUniButton;
    UniPanel3: TUniPanel;
    UniDBGrid2: TUniDBGrid;
    dsReclamaS: TDataSource;
    dsReclama1: TDataSource;
    BtnSeleccionar: TUniBitBtn;
    LblDada: TUniLabel;
    procedure BtnSeleccionarClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure RgTReclamaClick(Sender: TObject);
    procedure RgDistribuClick(Sender: TObject);
    procedure edDesdeFechaExit(Sender: TObject);
    procedure UniFormShow(Sender: TObject);


  private


    { Private declarations }

  public
    { Public declarations }
    procedure RutInicioForm;

  end;

function FormReclamaciones: TFormReclamaciones;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMReclamaciones;

function FormReclamaciones: TFormReclamaciones;
begin
  Result := TFormReclamaciones(DMppal.GetFormInstance(TFormReclamaciones));

end;


procedure TFormReclamaciones.RutInicioForm ;
begin

  LblDada.Caption := '';

  RGTipo.ItemIndex := 2;
  RgDistribu.ItemIndex := 1;
  RgTReclama.ItemIndex := 2;

  edDesdeFecha.DateTime := Now - 15;
  edHastaFecha.DateTime := Now;


  BtnSeleccionarClick(nil);
end;




procedure TFormReclamaciones.UniFormShow(Sender: TObject);
begin
  RutInicioForm;//temporalmente pongo hasta saber que poner
end;

procedure TFormReclamaciones.edDesdeFechaExit(Sender: TObject);
begin
  if edHastaFecha.Text = '' Then  edHastaFecha.DateTime := edDesdeFecha.DateTime;
  if edHastaFecha.DateTime < edDesdeFecha.DateTime Then  edHastaFecha.DateTime := edDesdeFecha.DateTime
end;

procedure TFormReclamaciones.BtnSeleccionarClick(Sender: TObject);
begin
  DMReclamaciones.ProObrirHistoric(RgTReclama.ItemIndex, RGTipo.ItemIndex, RgDistribu.ItemIndex, edDesdeFecha.Text, edHastaFecha.Text);
end;

procedure TFormReclamaciones.RgDistribuClick(Sender: TObject);
begin
  DMReclamaciones.ProObrirHistoric(RgTReclama.ItemIndex, RGTipo.ItemIndex, RgDistribu.ItemIndex,edDesdeFecha.Text, edHastaFecha.Text);
end;

procedure TFormReclamaciones.RGTipoClick(Sender: TObject);
begin
  DMReclamaciones.ProObrirHistoric(RgTReclama.ItemIndex, RGTipo.ItemIndex, RgDistribu.ItemIndex,edDesdeFecha.Text, edHastaFecha.Text);
end;

procedure TFormReclamaciones.RgTReclamaClick(Sender: TObject);
begin
  DMReclamaciones.ProObrirHistoric(RgTReclama.ItemIndex, RGTipo.ItemIndex, RgDistribu.ItemIndex,edDesdeFecha.Text, edHastaFecha.Text);
end;

end.
