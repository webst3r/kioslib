unit uDMHistoArti;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMHistoArti = class(TDataModule)
    sqlStock: TFDQuery;
    sqlStockID_ARTICULO: TIntegerField;
    sqlStockTBARRAS: TIntegerField;
    sqlStockBARRAS: TStringField;
    sqlStockADENDUM: TStringField;
    sqlStockDESCRIPCION: TStringField;
    sqlStockENTRADAS: TFloatField;
    sqlStockVENTAS: TFloatField;
    sqlStockVENTASAUTO: TFloatField;
    sqlStockDEVOLUCIONES: TFloatField;
    sqlStockMERMA: TFloatField;
    sqlStockSTOCK: TFloatField;
    sqlStockMOVTOS: TIntegerField;
    sqlHisArti: TFDQuery;
    sqlHisArtiID_HISARTI: TIntegerField;
    sqlHisArtiSWES: TStringField;
    sqlHisArtiFECHA: TDateField;
    sqlHisArtiHORA: TTimeField;
    sqlHisArtiCLAVE: TStringField;
    sqlHisArtiID_ARTICULO: TIntegerField;
    sqlHisArtiADENDUM: TStringField;
    sqlHisArtiID_CLIENTE: TIntegerField;
    sqlHisArtiSWTIPOFRA: TIntegerField;
    sqlHisArtiNFACTURA: TIntegerField;
    sqlHisArtiNALBARAN: TIntegerField;
    sqlHisArtiNLINEA: TIntegerField;
    sqlHisArtiDEVUELTOS: TSingleField;
    sqlHisArtiVENDIDOS: TSingleField;
    sqlHisArtiCANTIDAD: TSingleField;
    sqlHisArtiCANTIENALBA: TSingleField;
    sqlHisArtiPRECIOCOMPRA: TSingleField;
    sqlHisArtiPRECIOCOSTE: TSingleField;
    sqlHisArtiPRECIOVENTA: TSingleField;
    sqlHisArtiSWDTO: TSmallintField;
    sqlHisArtiTPCDTO: TSingleField;
    sqlHisArtiTIVA: TSmallintField;
    sqlHisArtiTPCIVA: TSingleField;
    sqlHisArtiTPCRE: TSingleField;
    sqlHisArtiIMPOBRUTO: TSingleField;
    sqlHisArtiIMPODTO: TSingleField;
    sqlHisArtiIMPOBASE: TSingleField;
    sqlHisArtiIMPOIVA: TSingleField;
    sqlHisArtiIMPORE: TSingleField;
    sqlHisArtiIMPOTOTLIN: TSingleField;
    sqlHisArtiENTRADAS: TSingleField;
    sqlHisArtiSALIDAS: TSingleField;
    sqlHisArtiVALORCOSTE: TSingleField;
    sqlHisArtiVALORCOMPRA: TSingleField;
    sqlHisArtiVALORVENTA: TSingleField;
    sqlHisArtiVALORMOVI: TSingleField;
    sqlHisArtiID_PROVEEDOR: TIntegerField;
    sqlHisArtiIDSTOCABE: TIntegerField;
    sqlHisArtiTPCCOMISION: TSingleField;
    sqlHisArtiIMPOCOMISION: TSingleField;
    sqlHisArtiENCARTE: TSingleField;
    sqlHisArtiID_DIARIA: TIntegerField;
    sqlHisArtiTURNO: TSmallintField;
    sqlHisArtiNALMACEN: TIntegerField;
    sqlHisArtiNRECLAMACION: TIntegerField;
    sqlHisArtiSWPDTEPAGO: TSmallintField;
    sqlHisArtiSWESTADO: TSmallintField;
    sqlHisArtiSWCERRADO: TSmallintField;
    sqlHisArtiFECHAAVISO: TDateField;
    sqlHisArtiFECHADEVOL: TDateField;
    sqlHisArtiTIPOVENTA: TSmallintField;
    sqlHisArtiNOMCLIE: TStringField;
    sqlHisArtiNOMPROVE: TStringField;
    sqlArticuloS: TFDQuery;
    sqlArticuloSID_ARTICULO: TIntegerField;
    sqlArticuloSTBARRAS: TIntegerField;
    sqlArticuloSBARRAS: TStringField;
    sqlArticuloSADENDUM: TStringField;
    sqlArticuloSDESCRIPCION: TStringField;
    sqlArticuloSIBS: TStringField;
    sqlArticuloSEDITORIAL: TIntegerField;
    sqlHisArtiPAQUETE: TIntegerField;
    procedure sqlHisArtiAfterScroll(DataSet: TDataSet);
    procedure sqlStockAfterScroll(DataSet: TDataSet);

  private
    procedure ProActivarArticles(vProveedor: Integer);
    procedure ProTrobatArticle(vArticulo : Integer; vBarras, vAdendum, vDesc : String);

    procedure RutScrollRegistros(vArticulo: Integer);
    procedure ProSeleHisArti(vArticulo: Integer);
    procedure RutVerBotonesReceDevol;



    { Private declarations }
  public
  vID_Articulo : Integer;

  vsCompras, vsVentas, vsVentasAuto,
  vsDevolucion, vsMerma, vsStock : Double;

  vEntradas, vSalidas, vStock    : Double;
  vImpoTotlin, vValorCoste, vValorCompra, vValorVenta  : Double;

  vRegisH                        : Integer;

  swSQL : Boolean;
    { Public declarations }

    procedure RutAbrirHistoricoArti(vArticulo : Integer; vBarras, vAdendum, vDesc, vDataA, vDataz : String);
    procedure ProSeleStock(vArticulo : Integer);

    end;

function DMHistoArti: TDMHistoArti;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uMenu, uHistoArti;

function DMHistoArti: TDMHistoArti;
begin
  Result := TDMHistoArti(DMppal.GetModuleInstance(TDMHistoArti));
end;

procedure TDMHistoArti.RutAbrirHistoricoArti(vArticulo : Integer; vBarras, vAdendum, vDesc, vDataA, vDataz : String);
begin
  swSQL := False;
  ProActivarArticles(0);
  sqlArticuloS.Locate('ID_ARTICULO', vArticulo, []);
  ProTrobatArticle(vArticulo, vBarras, vAdendum, vDesc);
  ProSeleStock(vID_Articulo);

end;




procedure TDMHistoArti.ProTrobatArticle(vArticulo : Integer; vBarras, vAdendum, vDesc : String);
begin
  FormHistoArti.edBarras.Text       := vBarras;
  FormHistoArti.edAdendum.Text      := vAdendum;
  FormHistoArti.edDescripcion.Text  := vDesc;
  vID_ARTICULO                      := vArticulo;
  FormHistoArti.lbArti.Caption      := IntToStr(vID_ARTICULO);


end;


procedure TDMHistoArti.ProSeleStock(vArticulo : Integer);
var  wSeleA, wSeleB, wSeleC, wOrder : String;
begin


  swSQL := true;




  with sqlStock do
  begin
    close;
    sqlStock.SQL.Text := Copy(SQL.Text,1,
                  pos('WHERE H.ID_ARTICULO IS NOT NULL', Uppercase(SQL.Text))-1)
                    + 'WHERE H.ID_ARTICULO IS NOT NULL'
                    + ' and H.ID_ARTICULO = ' + IntToStr(vArticulo)
               //     + vCerrado
//                    + ' and (Select Count(*) From FVHISARTI H1 '
//                    + ' where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adendum ) <> 0 '

                    + ' group by H.ID_ARTICULO,A.TBARRAS, A.BARRAS, H.ADENDUM, A.DESCRIPCION '
                    + ' order by H.ID_ARTICULO,A.TBARRAS, A.BARRAS, H.ADENDUM, A.DESCRIPCION ';
    open;
    FormHistoArti.rgSeleStock.ItemIndex := 1;
    first;
    vsCompras:= 0;vsVentas:= 0;vsVentasAuto:= 0;vsDevolucion:= 0;vsMerma:= 0;vsStock:= 0;

    while not eof do
    begin
      vsCompras    := vsCompras + fieldbyname('Entradas').Asfloat;
      vsVentas     := vsVentas + fieldbyname('Ventas').Asfloat;
      vsVentasAuto := vsVentasAuto + fieldbyname('VentasAuto').Asfloat;
      vsDevolucion := vsDevolucion + fieldbyname('Devoluciones').Asfloat;
      vsMerma      := vsMerma + fieldbyname('Merma').Asfloat;
      vsStock      := vsStock + fieldbyname('Stock').Asfloat;
      next;
    end;

  end;

 //fer el total de abaix GRIDUPDATEFOOTER

  FormHistoArti.edAdendum.Text := sqlStockADENDUM.AsString;
  swSQL := false;
  RutScrollRegistros(vArticulo);
  RutVerBotonesReceDevol;
end;


procedure TDMHistoArti.RutScrollRegistros(vArticulo:Integer);
begin
  FormHistoArti.edAdendum.Text := sqlStockADENDUM.AsString;
  if sqlStockID_ARTICULO.AsInteger > 0 then
  begin
    ProSeleHisArti    (sqlStockID_ARTICULO.AsInteger);
    FormHistoArti.BtnSumaCabeClick(nil);
  end else
  begin
    ProSeleHisArti    (vArticulo);
    FormHistoArti.BtnSumaCabeClick(nil);
  end;
end;


procedure TDMHistoArti.RutVerBotonesReceDevol;
begin
    FormHistoArti.btRecepcion.enabled  := False;
    FormHistoArti.btDevolucion.enabled := False;
  if sqlHisArti.FieldByName('Clave').AsString = '01' then
  begin
    FormHistoArti.btRecepcion.enabled  := True;
    FormHistoArti.btDevolucion.enabled := False;
  end;
  if sqlHisArti.FieldByName('Clave').AsString = '54' then
  begin
    FormHistoArti.btRecepcion.enabled  := False;
    FormHistoArti.btDevolucion.enabled := True;
  end;
end;


procedure TDMHistoArti.sqlHisArtiAfterScroll(DataSet: TDataSet);
begin
  if swSQL then exit;
  RutVerBotonesReceDevol;
end;

procedure TDMHistoArti.sqlStockAfterScroll(DataSet: TDataSet);
begin
  RutScrollRegistros(sqlStockID_ARTICULO.AsInteger);
end;

procedure TDMHistoArti.ProSeleHisArti  (vArticulo:Integer);
var  wSeleA, wSeleB, wSeleC, wClaves, wSeleFechas, wOrder, vCerrado : String;
begin
  inherited;

  vCerrado := ' ';
  //if cbIncluidoCerrados.checked = False then vCerrado := ' and ((H.SWACABADO is null) or (H.SWACABADO = 0)) ';

  wClaves := ' and ((Clave between ' + QuotedStr('01') + ' and ' + QuotedStr('40') + ')'
          +  ' or  (Clave between ' + QuotedStr('51') + ' and ' + QuotedStr('90') + '))';

  wSeleA := 'WHERE H.ID_HISARTI is not null  ';
  wSeleB := '';
  if FormHistoArti.edDesdeFecha.Text > '' then
  begin
    wSeleFechas := ' and (H.FECHA between ' + QuotedStr(FormatDateTime('mm/dd/yyyy', FormHistoArti.edDesdeFecha.DateTime))
                 + ' and '                  + QuotedStr(FormatDateTime('mm/dd/yyyy', FormHistoArti.edHastaFecha.DateTime))
                 + ') ';
  end;

 // if cbSoloCompras.Checked then wSeleB := wSeleB + ' and CLAVE = ' + '''' + '01' + '''';
  wSeleC := '';
  wOrder := ' Order by H.FECHA,H.HORA,H.ID_HISARTI ';


  wSeleB := wSeleB + ' and H.ID_ARTICULO = ' + IntToStr(vArticulo);


  if FormHistoArti.edAdendum.Text > '' then
  begin
     wSeleB := wSeleB + ' and H.ADENDUM = ' + QuotedStr(FormHistoArti.edAdendum.Text);
  end else
  begin
     wSeleB := wSeleB + ' and H.ADENDUM = ' + QuotedStr(FormHistoArti.edAdendum.Text);
  end;

  if sqlHisArti.Active Then sqlHisArti.Close;

  with sqlHisArti do
  begin

    sqlHisArti.sql.Text := Copy(sql.Text,1,
                    pos('WHERE', Uppercase(sql.Text))-1)

                   + wSeleA
                   + wSeleB
                   + wSeleFechas
                   + wClaves
               //    + vCerrado
                   + wOrder;

    Open;

  end;

  //gridupdatefooter

  sqlHisArti.last;
end;


procedure TDMHistoArti.ProActivarArticles(vProveedor : Integer);
var
  Prove : String;
begin
  if sqlArticulos.Active  Then sqlArticulos.Close;

//  vProveedorSel := zProveedor;

  if vProveedor = 0 Then  Prove := ''
  else                    Prove := ' and EDITORIAL = ' + IntToStr(vProveedor);

  with sqlArticuloS do
  begin
   sqlArticuloS.SQL.Text := Copy(SQL.Text,1,
                   pos('WHERE', Uppercase(SQL.Text))-1)
            +  ' where ID_ARTICULO is not null  and (SWALTABAJA = 0 or SWALTABAJA is null) '
            +   Prove
            +  ' Order by ID_ARTICULO ';
   Open;
  end;


end;


initialization
  RegisterModuleClass(TDMHistoArti);





end.


