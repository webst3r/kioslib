unit uNuevoUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormNuevoUsuario = class(TUniForm)
    dsUsuario: TDataSource;
    pcDetalle: TUniPageControl;
    tabGrid: TUniTabSheet;
    tabModificar: TUniTabSheet;
    edNombre: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    btGrabarModificacion: TUniButton;
    btCancelar: TUniButton;
    UniLabel8: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBEdit5: TUniDBEdit;
    UniLabel3: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    gridUsuarios: TUniDBGrid;
    UniPanel1: TUniPanel;
    navUsu: TUniDBNavigator;
    btSalir: TUniBitBtn;
    tabModificarPassword: TUniTabSheet;
    UniButton1: TUniButton;
    UniButton2: TUniButton;
    UniLabel9: TUniLabel;
    UniEdit1: TUniEdit;
    UniEdit2: TUniEdit;
    UniLabel10: TUniLabel;
    cbModificarPass: TUniDBCheckBox;
    UniDBCheckBox1: TUniDBCheckBox;
    UniLabel1: TUniLabel;
    UniLabel11: TUniLabel;
    pnlDatos: TUniPanel;
    UniLabel2: TUniLabel;
    UniLabel6: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniLabel7: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    UniLabel12: TUniLabel;
    UniDBEdit6: TUniDBEdit;
    UniLabel13: TUniLabel;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit9: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniLabel14: TUniLabel;
    pnlModiPass: TUniPanel;
    UniLabel15: TUniLabel;
    edNuevaPass: TUniEdit;
    edConfirmarPass: TUniEdit;
    UniLabel16: TUniLabel;
    btAppend: TUniBitBtn;
    btDelete: TUniBitBtn;
    btPost: TUniBitBtn;
    btCancel: TUniBitBtn;
    tabNuevoUsuario: TUniTabSheet;
    UniLabel17: TUniLabel;
    edPasswNueva: TUniEdit;
    edPasswConfirmar: TUniEdit;
    UniLabel18: TUniLabel;
    UniLabel19: TUniLabel;
    UniLabel20: TUniLabel;
    btGrabarNuevoUsu: TUniButton;
    btSalirReg: TUniButton;
    UniLabel21: TUniLabel;
    edIDUsuario: TUniEdit;
    edNumTPV: TUniEdit;
    lbEmailValido: TUniLabel;
    rgUsoBD: TUniRadioGroup;
    lbPassword: TUniLabel;
    tabConfiguracionDistri: TUniTabSheet;
    dsProve: TDataSource;
    UniLabel22: TUniLabel;
    UniLabel23: TUniLabel;
    UniLabel24: TUniLabel;
    UniLabel25: TUniLabel;
    lbMarina: TUniLabel;
    lbLogistica: TUniLabel;
    lbSade: TUniLabel;
    lbSgel: TUniLabel;
    edMarinaRuta: TUniEdit;
    edMarinaMargen: TUniEdit;
    UniLabel26: TUniLabel;
    UniCheckBox1: TUniCheckBox;
    UniCheckBox2: TUniCheckBox;
    UniCheckBox3: TUniCheckBox;
    UniCheckBox4: TUniCheckBox;
    btGrabarConfiguracion: TUniButton;
    btCancelarConfiguracion: TUniButton;
    edMarinaPuntoVenta: TUniEdit;
    edLogisticaPuntoVenta: TUniEdit;
    edLogisticaRuta: TUniEdit;
    edLogisticaMargen: TUniEdit;
    edSadePuntoVenta: TUniEdit;
    edSadeRuta: TUniEdit;
    edSadeMargen: TUniEdit;
    edSgelPuntoVenta: TUniEdit;
    edSgelRuta: TUniEdit;
    edSgelMargen: TUniEdit;
    tabConfiguracionEmpresa: TUniTabSheet;
    UniLabel27: TUniLabel;
    UniLabel28: TUniLabel;
    UniDBEdit10: TUniDBEdit;
    UniLabel29: TUniLabel;
    UniDBEdit11: TUniDBEdit;
    UniLabel30: TUniLabel;
    UniDBEdit12: TUniDBEdit;
    UniLabel31: TUniLabel;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniDBEdit15: TUniDBEdit;
    UniLabel34: TUniLabel;
    UniDBEdit16: TUniDBEdit;
    UniDBEdit17: TUniDBEdit;
    btGrabarDatosEmpresa: TUniButton;
    btCancelarDatosEmpresa: TUniButton;
    UniLabel35: TUniLabel;
    procedure btGrabarModificacionClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure gridUsuariosDblClick(Sender: TObject);
    procedure btSalirClick(Sender: TObject);
    procedure cbModificarPassClick(Sender: TObject);
    procedure btPostClick(Sender: TObject);
    procedure btAppendClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btGrabarNuevoUsuClick(Sender: TObject);
    procedure pcDetalleChange(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure btSalirRegClick(Sender: TObject);
    procedure edIDUsuarioChange(Sender: TObject);
    procedure edPasswConfirmarChange(Sender: TObject);
    procedure btGrabarConfiguracionClick(Sender: TObject);


  private
    function ValidEmail(email: string): boolean;
    procedure RutCamposObligatorios;


    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    vNuevoUsu : Boolean;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swModificar, swNuevo : Boolean;
    procedure RutCambiarPestana;
    procedure RutBorrarUsuario;
    procedure RutSetFocus;

  end;

function FormNuevoUsuario: TFormNuevoUsuario;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uLogin, uMensajes;

function FormNuevoUsuario: TFormNuevoUsuario;
begin
  Result := TFormNuevoUsuario(DMppal.GetFormInstance(TFormNuevoUsuario));

end;



procedure TFormNuevoUsuario.btGrabarModificacionClick(Sender: TObject);
begin

//  if cbModificarPass.Checked then  DMppal.sqlUsuPASS.AsInteger := RutCalcularPassword(edPassword.Text);

//  DMppal.sqlUsuTIPOPASSW.AsInteger  := 1;
//  DMppal.sqlUsuNOMBRE.AsString      := edNombre.Text;
//  DMppal.sqlUsu.Post;

  if cbModificarPass.Checked then
  begin
    if edNuevaPass.Text = edConfirmarPass.Text then
    begin
      DMppal.RutGrabarUsuario(edNuevaPass.Text,True);
      pcDetalle.ActivePage := tabGrid;

    cbModificarPass.Checked := False;
    edNuevaPass.Text     := '';
    edConfirmarPass.Text := '';
    cbModificarPassClick(nil);
    end
    else
    begin
     // FormMenu.vResult := '99';
      FormMensajes.RutInicioForm(1, UpperCase(self.name), 'ACEPTAR', '�Las contrase�as no coinciden!');
      FormMensajes.ShowModal();
    end;

  end
  else
  begin
    DMppal.RutGrabarUsuario(edNuevaPass.Text,False);
    pcDetalle.ActivePage := tabGrid;
  end;
end;

procedure TFormNuevoUsuario.btGrabarNuevoUsuClick(Sender: TObject);
begin
  if edIDUsuario.Text <> '' then
  begin
    if edPasswNueva.Text = edPasswConfirmar.Text then
    begin
      if DMppal.RutGrabarUsuarioTPV(edPasswNueva.Text, edIDUsuario.Text, StrToInt(edNumTPV.Text), rgUsoBD.ItemIndex,True) then
      begin
        //rutina de creacion/copia de BD a partir de la vaacia
        if DMppal.RutCopiaBD(edNumTPV.Text) then
        begin
          //CONFIGURACION DE DISTRIBUIDORAS DE LA BASE DE DATOS RECIEN CREADA
          DMPpal.RutGrabarConfiguracionDistri(edMarinaPuntoVenta.Text, edMarinaRuta.Text, edMarinaMargen.Text,
                                       edLogisticaPuntoVenta.Text, edLogisticaRuta.Text, edLogisticaMargen.Text,
                                       edSadePuntoVenta.Text, edSadeRuta.Text, edSadeMargen.Text,
                                       edSgelPuntoVenta.Text, edSgelRuta.Text, edSgelMargen.Text);


          UniLoginForm2.edUsuario.Text       := edIDUsuario.Text;
          UniLoginForm2.edPassword.Text      := edPasswNueva.Text;
          UniLoginForm2.cbRecuerdame.Checked := True;
        end
        else
        begin
         // FormMenu.vResult := '99';
          FormMensajes.RutInicioForm(2, UpperCase(self.name), 'ACEPTAR', '�Error al crear la base de datos!');
          FormMensajes.ShowModal();
          DMppal.RutBorrarUsuarioTPV(edIDUsuario.Text);

        end;

        edPasswNueva.Text     := '';
        edPasswConfirmar.Text := '';

      end
      else
      begin
       // FormMenu.vResult := '99';
        FormMensajes.RutInicioForm(5, UpperCase(self.name), 'ACEPTAROK', '�El usuario ya existe!');
        FormMensajes.ShowModal();
      end;

     // btSalirClick(nil);
    end
    else
    begin
      FormMensajes.RutInicioForm(3, UpperCase(self.name), 'ACEPTAR', '�Las contrase�as no coinciden!');
      FormMensajes.ShowModal();
    end;
  end
  else
  begin
    FormMensajes.RutInicioForm(3, UpperCase(self.name), 'ACEPTAR', '�Introduzca un usuario!');
    FormMensajes.ShowModal();
  end;
end;

procedure TFormNuevoUsuario.RutSetFocus;
begin
  edIDUsuario.Text := '';
 //FormNuevoUsuario.edIDUsuario.SelectAll;

  edIDUsuario.SetFocus;
end;


procedure TFormNuevoUsuario.btCancelarClick(Sender: TObject);
begin
  DMppal.sqlUsu.Cancel;
  pcDetalle.ActivePage := tabGrid;
end;

procedure TFormNuevoUsuario.btDeleteClick(Sender: TObject);
begin
  FormMensajes.RutInicioForm(4, UpperCase(self.name), 'ACEPTARCANC', '�Quieres borrar el usuario?');
  FormMensajes.ShowModal();


end;

procedure TFormNuevoUsuario.btAppendClick(Sender: TObject);
begin
  DMppal.sqlUsu.Append;
 // DMppal.sqlUsuNUMEROUSUARIO.AsInteger := DMppal.RutGetMAXTPV + 1;
end;

procedure TFormNuevoUsuario.btPostClick(Sender: TObject);
begin
 DMppal.RutGrabarUsuario(DMppal.sqlUsuPASS.AsString,True);
end;

procedure TFormNuevoUsuario.gridUsuariosDblClick(Sender: TObject);
begin
  swModificar := True;
  swNuevo     := False;
  RutCambiarPestana;
end;

procedure TFormNuevoUsuario.pcDetalleChange(Sender: TObject);
begin
  if pcDetalle.ActivePage = tabNuevoUsuario then
  begin
    edNumTPV.Text := IntToStr(DMppal.RutGetMAXTPV + 1);

  end;

end;

procedure TFormNuevoUsuario.RutBorrarUsuario;
begin
  DMppal.sqlUsu.Delete;
end;

procedure TFormNuevoUsuario.RutCambiarPestana;
begin
  DMppal.sqlUsu.Edit;
  pcDetalle.ActivePage := tabModificar;

end;

procedure TFormNuevoUsuario.cbModificarPassClick(Sender: TObject);
begin

  if cbModificarPass.Checked then
  begin
    pnlModiPass.Visible := cbModificarPass.Checked;
    pnlDatos.Top :=  pnlModiPass.Top + pnlModiPass.Height;
  end
  else
  begin
    pnlModiPass.Visible := cbModificarPass.Checked;
    pnlDatos.Top :=  pnlModiPass.Top;
  end;
end;

procedure TFormNuevoUsuario.UniFormCreate(Sender: TObject);
begin
  pcDetalle.ActivePage := tabGrid;
  DMppal.RutAbrirTablaNuevoUsu;

  gridUsuarios.Columns[0].Width := 120;
  gridUsuarios.Columns[1].Width := 120;
  gridUsuarios.Columns[2].Width := 70;
  gridUsuarios.Columns[3].Width := 53;
  gridUsuarios.Columns[4].Width := 70;
  gridUsuarios.Columns[5].Width := 43;

  cbModificarPassClick(nil);
end;

procedure TFormNuevoUsuario.UniFormShow(Sender: TObject);
begin
  if vNuevoUsu then
  begin
    pcDetalle.ActivePage := tabNuevoUsuario;
    edNumTPV.Text := IntToStr(DMppal.RutGetMAXTPV + 1);
    self.Height := 255;
    self.Width  := 410;
    edIDUsuario.SetFocus;
  end;
end;

procedure TFormNuevoUsuario.btSalirClick(Sender: TObject);
begin
  self.Close;
end;

procedure TFormNuevoUsuario.btSalirRegClick(Sender: TObject);
begin
  self.Close;
end;


procedure TFormNuevoUsuario.edIDUsuarioChange(Sender: TObject);
begin
 if ValidEmail(edIDUsuario.Text) then lbEmailValido.Caption := 'Email Valido'
  else lbEmailValido.Caption := 'Email Invalido';
  lbEmailValido.Visible := True;
end;

procedure TFormNuevoUsuario.edPasswConfirmarChange(Sender: TObject);
begin
  if edPasswConfirmar.Text <> '' then
  begin
    if edPasswNueva.Text <> edPasswConfirmar.Text then
    begin
      btGrabarNuevoUsu.Enabled := False;
      lbPassword.Caption := '�Las contrase�as no coinciden!';
      lbPassword.Visible := True;
    end
    else
    begin
      btGrabarNuevoUsu.Enabled := True;
      lbPassword.Visible := False;
    end;
  end;

end;

function TFormNuevoUsuario.ValidEmail(email: string): boolean;
   // Devuelve True si la direcci�n de email es v�lida
   const
     // Caracteres v�lidos en un "�tomo"
     atom_chars = [#33..#255] - ['(', ')', '<', '>', '@', ',', ';',
     ':', '\', '/', '"', '.', '[', ']', #127];
     // Caracteres v�lidos en una "cadena-entrecomillada"
     quoted_string_chars = [#0..#255] - ['"', #13, '\'];
     // Caracteres v�lidos en un subdominio
     letters = ['A'..'Z', 'a'..'z'];
     letters_digits = ['0'..'9', 'A'..'Z', 'a'..'z'];
     subdomain_chars = ['-', '0'..'9', 'A'..'Z', 'a'..'z'];
   type
     States = (STATE_BEGIN, STATE_ATOM, STATE_QTEXT,
     STATE_QCHAR, STATE_QUOTE, STATE_LOCAL_PERIOD,
     STATE_EXPECTING_SUBDOMAIN, STATE_SUBDOMAIN, STATE_HYPHEN);
   var
     State: States;
     i, n, subdomains: integer;
     c: char;
begin
  State := STATE_BEGIN;
  n := Length(email);
  i := 1;
  subdomains := 1;
  while (i <= n) do
    begin
      c := email[i];
      case State of
         STATE_BEGIN:
                  if c in atom_chars then
                  State := STATE_ATOM
                  else if c = '"' then
                  State := STATE_QTEXT
                  else break;
         STATE_ATOM:
                  if c = '@' then
                  State := STATE_EXPECTING_SUBDOMAIN
                  else if c = '.' then
                  State := STATE_LOCAL_PERIOD
                  else if not (c in atom_chars) then
                  break;
         STATE_QTEXT:
                  if c = '\' then
                  State := STATE_QCHAR
                  else if c = '"' then
                  State := STATE_QUOTE
                  else if not (c in quoted_string_chars) then
                  break;
         STATE_QCHAR:
                  State := STATE_QTEXT;
         STATE_QUOTE:
                  if c = '@' then
                  State := STATE_EXPECTING_SUBDOMAIN
                  else if c = '.' then
                  State := STATE_LOCAL_PERIOD
                  else break;
         STATE_LOCAL_PERIOD:
                  if c in atom_chars then
                  State := STATE_ATOM
                  else if c = '"' then
                  State := STATE_QTEXT
                              else break;
         STATE_EXPECTING_SUBDOMAIN:
                  if c in letters then
                  State := STATE_SUBDOMAIN
                  else break;
         STATE_SUBDOMAIN:
                  if c = '.' then
                  begin
                      inc(subdomains);
                      State := STATE_EXPECTING_SUBDOMAIN
                  end
                  else if c = '-' then
                  State := STATE_HYPHEN
                  else if not (c in letters_digits) then
                  break;
         STATE_HYPHEN:
                  if c in letters_digits then
                  State := STATE_SUBDOMAIN
                  else if c <> '-' then break;
      end;
      inc(i);
    end;
  if i <= n then
  Result := False
  else
  Result := (State = STATE_SUBDOMAIN) and (subdomains >= 2);
end;


procedure TFormNuevoUsuario.btGrabarConfiguracionClick(Sender: TObject);
begin
  RutCamposObligatorios;
end;

procedure TFormNuevoUsuario.RutCamposObligatorios;
begin
  if edMarinaPuntoVenta.Text = '' then edMarinaPuntoVenta.Color := clRed;
  if edMarinaRuta.Text       = '' then edMarinaRuta.Color := clRed;
  if edMarinaMargen.Text     = '' then edMarinaMargen.Color := clRed;

  if edLogisticaPuntoVenta.Text = '' then edLogisticaPuntoVenta.Color := clRed;
  if edLogisticaRuta.Text       = '' then edLogisticaRuta.Color := clRed;
  if edLogisticaMargen.Text     = '' then edLogisticaMargen.Color := clRed;

  if edSadePuntoVenta.Text = '' then edSadePuntoVenta.Color := clRed;
  if edSadeRuta.Text       = '' then edSadeRuta.Color := clRed;
  if edSadeMargen.Text     = '' then edSadeMargen.Color := clRed;

  if edSgelPuntoVenta.Text = '' then edSgelPuntoVenta.Color := clRed;
  if edSgelRuta.Text       = '' then edSgelRuta.Color := clRed;
  if edSgelMargen.Text     = '' then edSgelMargen.Color := clRed;
end;

end.
