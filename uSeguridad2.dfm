object UniForm2: TUniForm2
  Left = 0
  Top = 0
  ClientHeight = 605
  ClientWidth = 1051
  Caption = 'UniForm2'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object UniPageControl1: TUniPageControl
    Left = 0
    Top = 0
    Width = 1051
    Height = 605
    Hint = ''
    ActivePage = tabDevolAlbaranCopia
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitLeft = 24
    ExplicitTop = 8
    object tabDevolucion: TUniTabSheet
      Hint = ''
      Caption = 'tabDevolucion'
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 1043
        Height = 105
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Caption = ''
        object UniPanel2: TUniPanel
          Left = 1
          Top = 1
          Width = 1041
          Height = 44
          Hint = ''
          ShowHint = True
          Align = alTop
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          Caption = ''
          object UniDBEdit1: TUniDBEdit
            Left = 10
            Top = 19
            Width = 40
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'ID_PROVEEDOR'
            TabOrder = 1
          end
          object UniLabel1: TUniLabel
            Left = 156
            Top = 3
            Width = 54
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Distribuidor'
            TabOrder = 2
          end
          object UniDBEdit2: TUniDBEdit
            Left = 49
            Top = 19
            Width = 279
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'NOMBRE'
            TabOrder = 3
          end
          object UniDBEdit3: TUniDBEdit
            Left = 327
            Top = 19
            Width = 83
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'NDOCSTOCK'
            TabOrder = 4
          end
          object UniDBEdit4: TUniDBEdit
            Left = 409
            Top = 19
            Width = 40
            Height = 22
            Hint = ''
            ShowHint = True
            DataField = 'PAQUETE'
            TabOrder = 5
          end
          object UniDBDateTimePicker1: TUniDBDateTimePicker
            Left = 448
            Top = 19
            Width = 100
            Hint = ''
            ShowHint = True
            DataField = 'FECHA'
            DateTime = 43374.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 6
          end
          object UniLabel2: TUniLabel
            Left = 329
            Top = 3
            Width = 68
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'N. Documento'
            TabOrder = 7
          end
          object UniLabel3: TUniLabel
            Left = 409
            Top = 3
            Width = 40
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Paquete'
            TabOrder = 8
          end
          object UniLabel4: TUniLabel
            Left = 480
            Top = 3
            Width = 29
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Fecha'
            TabOrder = 9
          end
        end
        object UniPanel3: TUniPanel
          Left = 567
          Top = 0
          Width = 286
          Height = 49
          Hint = ''
          ShowHint = True
          TabOrder = 2
          Caption = ''
          object pFechaPreDevolu: TUniDBDateTimePicker
            Left = 159
            Top = 19
            Width = 100
            Hint = ''
            ShowHint = True
            DataField = 'DEVOLUFECHASEL'
            DateTime = 43374.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 1
          end
          object UniLabel7: TUniLabel
            Left = 10
            Top = 14
            Width = 143
            Height = 23
            Hint = ''
            ShowHint = True
            Caption = 'Pre Devolucion'
            ParentFont = False
            Font.Height = -19
            Font.Style = [fsBold]
            TabOrder = 2
          end
          object UniLabel8: TUniLabel
            Left = 164
            Top = 4
            Width = 84
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Fecha Devolucion'
            TabOrder = 3
          end
        end
        object rgOrdenado: TUniDBRadioGroup
          Left = 3
          Top = 50
          Width = 223
          Height = 45
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 3
          Items.Strings = (
            'Descripci'#243'n'
            'Paquete')
          Columns = 2
        end
        object fcPanel3: TUniPanel
          Left = 232
          Top = 50
          Width = 81
          Height = 45
          Hint = ''
          ShowHint = True
          TabOrder = 4
          Caption = ''
          object UniLabel5: TUniLabel
            Left = 11
            Top = 1
            Width = 54
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'N. Paquete'
            TabOrder = 1
          end
          object UniDBEdit5: TUniDBEdit
            Left = 18
            Top = 17
            Width = 40
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
        end
        object Rgpaquetes: TUniDBRadioGroup
          Left = 319
          Top = 50
          Width = 141
          Height = 45
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 5
          Items.Strings = (
            'Todos'
            'Uno :')
          Columns = 2
        end
        object UniPanel4: TUniPanel
          Left = 459
          Top = 50
          Width = 81
          Height = 45
          Hint = ''
          ShowHint = True
          TabOrder = 6
          Caption = ''
          object UniLabel6: TUniLabel
            Left = 19
            Top = 3
            Width = 21
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Num'
            TabOrder = 1
          end
          object UniDBEdit6: TUniDBEdit
            Left = 18
            Top = 17
            Width = 40
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
        end
        object UniBitBtn1: TUniBitBtn
          Left = 560
          Top = 53
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Utilidades'
          TabOrder = 7
        end
      end
      object UniPanel16: TUniPanel
        Left = 0
        Top = 105
        Width = 1043
        Height = 58
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        Caption = ''
        object UniBitBtn13: TUniBitBtn
          Left = 2
          Top = 13
          Width = 55
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Anula'
          TabOrder = 1
        end
        object pMargen1: TUniDBEdit
          Left = 181
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN'
          TabOrder = 2
        end
        object pMargen2: TUniDBEdit
          Left = 181
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MARGEN2'
          TabOrder = 3
        end
        object pEncarte2: TUniDBEdit
          Left = 240
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE2'
          TabOrder = 4
        end
        object pEncarte1: TUniDBEdit
          Left = 240
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ENCARTE'
          TabOrder = 5
        end
        object UniLabel41: TUniLabel
          Left = 188
          Top = 3
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Margen'
          TabOrder = 6
        end
        object UniLabel42: TUniLabel
          Left = 244
          Top = 3
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Encarte'
          TabOrder = 7
        end
        object pTIva1: TUniDBEdit
          Left = 303
          Top = 17
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA'
          TabOrder = 8
        end
        object pTIva2: TUniDBEdit
          Left = 303
          Top = 37
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TIVA2'
          TabOrder = 9
        end
        object UniDBEdit47: TUniDBEdit
          Left = 330
          Top = 17
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA'
          TabOrder = 10
        end
        object UniDBEdit48: TUniDBEdit
          Left = 374
          Top = 17
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE'
          TabOrder = 11
        end
        object UniDBEdit49: TUniDBEdit
          Left = 330
          Top = 37
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCIVA2'
          TabOrder = 12
        end
        object UniDBEdit50: TUniDBEdit
          Left = 374
          Top = 37
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'TPCRE2'
          TabOrder = 13
        end
        object UniLabel43: TUniLabel
          Left = 305
          Top = 3
          Width = 20
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Tipo'
          TabOrder = 14
        end
        object UniLabel44: TUniLabel
          Left = 335
          Top = 3
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '%IVA'
          TabOrder = 15
        end
        object UniLabel45: TUniLabel
          Left = 382
          Top = 3
          Width = 27
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '% RE'
          TabOrder = 16
        end
        object UniDBEdit51: TUniDBEdit
          Left = 419
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE'
          TabOrder = 17
        end
        object UniDBEdit52: TUniDBEdit
          Left = 481
          Top = 17
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT'
          TabOrder = 18
        end
        object UniDBEdit53: TUniDBEdit
          Left = 419
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTE2'
          TabOrder = 19
        end
        object UniDBEdit54: TUniDBEdit
          Left = 481
          Top = 37
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOCOSTETOT2'
          TabOrder = 20
        end
        object UniLabel46: TUniLabel
          Left = 429
          Top = 3
          Width = 42
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Coste'
          TabOrder = 21
        end
        object UniLabel47: TUniLabel
          Left = 485
          Top = 3
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Cte+RE'
          TabOrder = 22
        end
        object UniPanel17: TUniPanel
          Left = 545
          Top = 0
          Width = 369
          Height = 57
          Hint = ''
          ShowHint = True
          TabOrder = 23
          Caption = ''
          object UniDBEdit55: TUniDBEdit
            Left = 5
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA'
            TabOrder = 1
          end
          object UniDBEdit56: TUniDBEdit
            Left = 70
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE'
            TabOrder = 2
          end
          object UniDBEdit57: TUniDBEdit
            Left = 5
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOMPRA2'
            TabOrder = 3
          end
          object UniDBEdit58: TUniDBEdit
            Left = 70
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTE2'
            TabOrder = 4
          end
          object UniLabel48: TUniLabel
            Left = 76
            Top = 1
            Width = 38
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste'
            TabOrder = 5
          end
          object UniLabel49: TUniLabel
            Left = 9
            Top = 1
            Width = 47
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'v.Compra'
            TabOrder = 6
          end
          object UniDBEdit59: TUniDBEdit
            Left = 135
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT'
            TabOrder = 7
          end
          object UniDBEdit60: TUniDBEdit
            Left = 200
            Top = 15
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA'
            TabOrder = 8
          end
          object UniDBEdit61: TUniDBEdit
            Left = 135
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORCOSTETOT2'
            TabOrder = 9
          end
          object UniDBEdit62: TUniDBEdit
            Left = 200
            Top = 35
            Width = 65
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'VALORVENTA2'
            TabOrder = 10
          end
          object UniLabel50: TUniLabel
            Left = 136
            Top = 1
            Width = 65
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste + RE'
            TabOrder = 11
          end
          object UniLabel51: TUniLabel
            Left = 216
            Top = 1
            Width = 30
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Vta.'
            TabOrder = 12
          end
          object UniLabel52: TUniLabel
            Left = 275
            Top = 13
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Venta'
            TabOrder = 13
          end
          object UniLabel53: TUniLabel
            Left = 275
            Top = 26
            Width = 52
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Devolucion'
            TabOrder = 14
          end
          object UniLabel54: TUniLabel
            Left = 275
            Top = 39
            Width = 32
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Merma'
            TabOrder = 15
          end
          object UniDBText5: TUniDBText
            Left = 305
            Top = 13
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'VENTAHISTO'
          end
          object UniDBText6: TUniDBText
            Left = 305
            Top = 26
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'DEVOLHISTO'
          end
          object UniDBText7: TUniDBText
            Left = 305
            Top = 39
            Width = 56
            Height = 13
            Hint = ''
            ShowHint = True
            DataField = 'MERMAHISTO'
          end
        end
        object UniLabel55: TUniLabel
          Left = 917
          Top = 4
          Width = 25
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Alba.'
          TabOrder = 24
        end
        object UniLabel56: TUniLabel
          Left = 1003
          Top = 4
          Width = 24
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total'
          TabOrder = 25
        end
        object UniLabel57: TUniLabel
          Left = 937
          Top = 27
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 26
        end
        object UniDBText8: TUniDBText
          Left = 945
          Top = 4
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'CANTIENALBA'
        end
        object UniDBText9: TUniDBText
          Left = 979
          Top = 27
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'COMPRAHISTO'
        end
      end
      object BtnNouAdendum: TUniBitBtn
        Left = 2
        Top = 239
        Width = 55
        Height = 27
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Num'
        TabOrder = 2
      end
      object BtnVerVtaAuto: TUniBitBtn
        Left = 58
        Top = 194
        Width = 55
        Height = 42
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Vta Auto'
        TabOrder = 3
      end
      object btVerDirectoDevolucion: TUniBitBtn
        Left = 2
        Top = 194
        Width = 55
        Height = 42
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Directo'
        TabOrder = 4
      end
      object UniLabel12: TUniLabel
        Left = 150
        Top = 180
        Width = 70
        Height = 13
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'Total Devuelto'
        TabOrder = 5
      end
      object edTotalCantidad: TUniDBEdit
        Left = 157
        Top = 194
        Width = 57
        Height = 27
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        DataField = 'DEVUELTOS'
        TabOrder = 6
      end
      object pnlPendi: TUniPanel
        Left = 220
        Top = 181
        Width = 62
        Height = 57
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        TabOrder = 7
        Caption = ''
        Color = clLime
        object BtnPendi: TUniBitBtn
          Left = 3
          Top = 3
          Width = 56
          Height = 42
          Hint = ''
          ShowHint = True
          Caption = 'Pendiente'
          TabOrder = 1
        end
      end
      object pnlProveLine: TUniPanel
        Left = 288
        Top = 182
        Width = 306
        Height = 56
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        TabOrder = 8
        Caption = ''
        object UniDBEdit11: TUniDBEdit
          Left = 5
          Top = 22
          Width = 55
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'ID_PROVEEDOR'
          TabOrder = 1
        end
        object UniLabel13: TUniLabel
          Left = 47
          Top = 6
          Width = 173
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Proveedor de la PRE - DEVOLUCION'
          TabOrder = 2
        end
        object UniDBEdit12: TUniDBEdit
          Left = 59
          Top = 22
          Width = 242
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'NOMBRE'
          TabOrder = 3
        end
      end
      object LabelStatus: TUniLabel
        Left = 158
        Top = 231
        Width = 56
        Height = 13
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Caption = 'LabelStatus'
        TabOrder = 9
      end
      object UniPanel18: TUniPanel
        Left = 0
        Top = 275
        Width = 1043
        Height = 36
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        TabOrder = 10
        Caption = ''
        object UniLabel58: TUniLabel
          Left = 21
          Top = 0
          Width = 54
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cod.Barras'
          TabOrder = 1
        end
        object pGraBarras: TUniDBEdit
          Left = 2
          Top = 14
          Width = 138
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 2
        end
        object pGraAdendum: TUniDBEdit
          Left = 140
          Top = 14
          Width = 43
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 3
        end
        object pGraPaquete: TUniDBEdit
          Left = 456
          Top = 14
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 4
        end
        object UniLabel60: TUniLabel
          Left = 448
          Top = 0
          Width = 51
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'N.Paquete'
          TabOrder = 5
        end
        object pGraDevueltos: TUniDBEdit
          Left = 514
          Top = 14
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 6
        end
        object UniLabel61: TUniLabel
          Left = 509
          Top = 0
          Width = 48
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Devueltos'
          TabOrder = 7
        end
        object UniLabel62: TUniLabel
          Left = 585
          Top = 16
          Width = 171
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '< ---   Ultima Modificacion Realizada'
          TabOrder = 8
        end
      end
      object UniPanel12: TUniPanel
        Left = 0
        Top = 324
        Width = 1040
        Height = 134
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        TabOrder = 11
        Caption = ''
        object UniPanel15: TUniPanel
          Left = 425
          Top = 1
          Width = 136
          Height = 132
          Hint = ''
          ShowHint = True
          Align = alLeft
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 1
          Caption = ''
          object UniLabel31: TUniLabel
            Left = 42
            Top = 5
            Width = 38
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Importe'
            TabOrder = 1
          end
          object UniDBEdit29: TUniDBEdit
            Left = 15
            Top = 25
            Width = 106
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'IMPOTOTLIN'
            TabOrder = 2
          end
          object UniBitBtn11: TUniBitBtn
            Left = 15
            Top = 48
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            TabOrder = 3
          end
          object UniBitBtn12: TUniBitBtn
            Left = 71
            Top = 48
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            TabOrder = 4
          end
        end
        object PnlLineCodi: TUniPanel
          Left = 1
          Top = 1
          Width = 424
          Height = 132
          Hint = ''
          ShowHint = True
          Align = alLeft
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 2
          Caption = ''
          object UniLabel27: TUniLabel
            Left = 21
            Top = 5
            Width = 33
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Codigo'
            TabOrder = 1
          end
          object UniLabel28: TUniLabel
            Left = 5
            Top = 45
            Width = 49
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Ref.Distri.'
            TabOrder = 2
          end
          object UniDBEdit22: TUniDBEdit
            Left = 57
            Top = 1
            Width = 98
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'BARRAS'
            TabOrder = 3
          end
          object UniDBEdit23: TUniDBEdit
            Left = 155
            Top = 1
            Width = 98
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'ADENDUM'
            TabOrder = 4
          end
          object UniDBEdit24: TUniDBEdit
            Left = 253
            Top = 1
            Width = 49
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 5
          end
          object UniLabel29: TUniLabel
            Left = 308
            Top = 3
            Width = 51
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'N.Paquete'
            TabOrder = 6
          end
          object UniDBEdit25: TUniDBEdit
            Left = 362
            Top = 1
            Width = 45
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'PAQUETE'
            TabOrder = 7
          end
          object UniDBEdit26: TUniDBEdit
            Left = 57
            Top = 21
            Width = 351
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'DESCRIPCION'
            TabOrder = 8
          end
          object UniDBEdit27: TUniDBEdit
            Left = 57
            Top = 41
            Width = 80
            Height = 20
            Hint = ''
            ShowHint = True
            DataField = 'REFEPROVE'
            TabOrder = 9
          end
          object UniDBEdit28: TUniDBEdit
            Left = 137
            Top = 41
            Width = 271
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 10
          end
          object UniLabel30: TUniLabel
            Left = 108
            Top = 77
            Width = 281
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Ha sido actualizado el Art'#237'culo con Distribuidor / Referencia'
            TabOrder = 11
          end
        end
      end
    end
    object tabDevolAlbaranCopia: TUniTabSheet
      Hint = ''
      Caption = 'tabDevolAlbaranCopia'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 1051
      ExplicitHeight = 605
      object UniPageControl5: TUniPageControl
        Left = 0
        Top = 0
        Width = 1043
        Height = 577
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        ActivePage = UniTabSheet2
        TabBarVisible = False
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        ExplicitLeft = 48
        ExplicitTop = 76
        ExplicitWidth = 289
        ExplicitHeight = 193
        object UniTabSheet2: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'tabAlbaran'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 537
          object UniPanel24: TUniPanel
            Left = 0
            Top = 622
            Width = 1035
            Height = 75
            Hint = ''
            ShowHint = True
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            Caption = 'UniPanel1'
          end
          object UniPanel25: TUniPanel
            Left = 603
            Top = 279
            Width = 431
            Height = 259
            Hint = ''
            ShowHint = True
            TabOrder = 1
            Caption = ''
            object UniLabel107: TUniLabel
              Left = 13
              Top = 88
              Width = 66
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Obervaciones'
              TabOrder = 1
            end
            object UniDBMemo3: TUniDBMemo
              Left = 11
              Top = 106
              Width = 410
              Height = 89
              Hint = ''
              ShowHint = True
              DataField = 'OBSERVACIONES'
              ScrollBars = ssBoth
              TabOrder = 2
            end
            object UniBitBtn19: TUniBitBtn
              Left = 11
              Top = 204
              Width = 104
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Cerrar Paquete'
              TabOrder = 3
            end
            object UniBitBtn20: TUniBitBtn
              Left = 121
              Top = 204
              Width = 104
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Cancelar'
              TabOrder = 4
            end
            object UniLabel108: TUniLabel
              Left = 20
              Top = 14
              Width = 115
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Vas a cerrar el Paquete '
              TabOrder = 5
            end
            object UniLabel109: TUniLabel
              Left = 20
              Top = 37
              Width = 86
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'de la Distribuidora'
              TabOrder = 6
            end
            object UniLabel110: TUniLabel
              Left = 22
              Top = 59
              Width = 84
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha Devolucion'
              TabOrder = 7
            end
            object UniDBEdit69: TUniDBEdit
              Left = 139
              Top = 7
              Width = 67
              Height = 20
              Hint = ''
              ShowHint = True
              DataField = 'PAQUETE'
              TabOrder = 8
            end
            object UniDBEdit70: TUniDBEdit
              Left = 139
              Top = 30
              Width = 67
              Height = 20
              Hint = ''
              ShowHint = True
              DataField = 'NOMBRE'
              TabOrder = 9
            end
            object UniDBDateTimePicker2: TUniDBDateTimePicker
              Left = 139
              Top = 52
              Width = 93
              Hint = ''
              ShowHint = True
              DataField = 'FECHADEVOL'
              DateTime = 43374.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 10
            end
          end
          object UniPageControl6: TUniPageControl
            Left = 0
            Top = 0
            Width = 1035
            Height = 622
            Hint = ''
            ShowHint = True
            ActivePage = UniTabSheet7
            TabBarVisible = False
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 2
            ExplicitTop = -62
            ExplicitWidth = 1056
            object UniTabSheet7: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'TabSheet3'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 256
              ExplicitHeight = 128
              object UniPageControl7: TUniPageControl
                Left = 0
                Top = 0
                Width = 1027
                Height = 594
                Hint = ''
                ShowHint = True
                ActivePage = UniTabSheet12
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 0
                ExplicitWidth = 1048
                ExplicitHeight = 404
                object UniTabSheet8: TUniTabSheet
                  Hint = ''
                  ShowHint = True
                  Caption = 'Entrada'
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 256
                  ExplicitHeight = 376
                  object UniPanel26: TUniPanel
                    Left = 0
                    Top = 0
                    Width = 1019
                    Height = 158
                    Hint = ''
                    ShowHint = True
                    Align = alTop
                    Anchors = [akLeft, akTop, akRight]
                    TabOrder = 0
                    Caption = ''
                    ExplicitLeft = 3
                    ExplicitTop = 6
                    ExplicitWidth = 1040
                    object UniLabel111: TUniLabel
                      Left = 12
                      Top = 17
                      Width = 65
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'C.Barras [F5]'
                      TabOrder = 1
                    end
                    object UniDBEdit71: TUniDBEdit
                      Left = 79
                      Top = 12
                      Width = 231
                      Height = 27
                      Hint = ''
                      ShowHint = True
                      TabOrder = 2
                    end
                    object UniDBEdit72: TUniDBEdit
                      Left = 595
                      Top = 13
                      Width = 77
                      Height = 26
                      Hint = ''
                      ShowHint = True
                      DataField = 'ADENDUM'
                      TabOrder = 3
                    end
                    object UniLabel112: TUniLabel
                      Left = 617
                      Top = -2
                      Width = 41
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Num[F7]'
                      TabOrder = 4
                    end
                    object UniDBEdit73: TUniDBEdit
                      Left = 172
                      Top = 62
                      Width = 57
                      Height = 27
                      Hint = ''
                      ShowHint = True
                      TabOrder = 5
                    end
                    object UniLabel113: TUniLabel
                      Left = 175
                      Top = 45
                      Width = 43
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Cantidad'
                      TabOrder = 6
                    end
                    object UniBitBtn21: TUniBitBtn
                      Left = 246
                      Top = 62
                      Width = 55
                      Height = 39
                      Hint = ''
                      ShowHint = True
                      Caption = '+'
                      ParentFont = False
                      Font.Height = -24
                      TabOrder = 7
                    end
                    object UniBitBtn22: TUniBitBtn
                      Left = 305
                      Top = 62
                      Width = 55
                      Height = 39
                      Hint = ''
                      ShowHint = True
                      Caption = '-'
                      ParentFont = False
                      Font.Height = -32
                      TabOrder = 8
                    end
                    object UniDBEdit74: TUniDBEdit
                      Left = 316
                      Top = 13
                      Width = 273
                      Height = 26
                      Hint = ''
                      ShowHint = True
                      DataField = 'DESCRIPCION'
                      TabOrder = 9
                    end
                    object UniLabel114: TUniLabel
                      Left = 405
                      Top = -2
                      Width = 54
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Descripcion'
                      TabOrder = 10
                    end
                    object UniLabel115: TUniLabel
                      Left = 12
                      Top = 45
                      Width = 45
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Total PVP'
                      TabOrder = 11
                    end
                    object UniLabel116: TUniLabel
                      Left = 77
                      Top = 45
                      Width = 30
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'P.V.P.'
                      TabOrder = 12
                    end
                    object UniDBEdit75: TUniDBEdit
                      Left = 9
                      Top = 62
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PrecioTotal'
                      TabOrder = 13
                    end
                    object UniDBEdit76: TUniDBEdit
                      Left = 75
                      Top = 81
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOVENTA'
                      TabOrder = 14
                    end
                    object UniDBEdit77: TUniDBEdit
                      Left = 9
                      Top = 81
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOCOMPRA2'
                      TabOrder = 15
                    end
                    object UniDBEdit78: TUniDBEdit
                      Left = 75
                      Top = 62
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOVENTA2'
                      TabOrder = 16
                    end
                    object UniBitBtn23: TUniBitBtn
                      Left = 625
                      Top = 115
                      Width = 45
                      Height = 35
                      Hint = ''
                      ShowHint = True
                      Caption = ''
                      TabOrder = 17
                      ImageIndex = 55
                    end
                    object UniLabel117: TUniLabel
                      Left = 13
                      Top = 108
                      Width = 37
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Compra'
                      TabOrder = 18
                    end
                    object UniLabel118: TUniLabel
                      Left = 65
                      Top = 108
                      Width = 36
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'A Stock'
                      TabOrder = 19
                    end
                    object UniLabel119: TUniLabel
                      Left = 163
                      Top = 108
                      Width = 32
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Merma'
                      TabOrder = 20
                    end
                    object UniLabel120: TUniLabel
                      Left = 214
                      Top = 108
                      Width = 28
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Venta'
                      TabOrder = 21
                    end
                    object UniLabel121: TUniLabel
                      Left = 315
                      Top = 108
                      Width = 27
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Devol'
                      TabOrder = 22
                    end
                    object UniLabel122: TUniLabel
                      Left = 415
                      Top = 108
                      Width = 31
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Abono'
                      TabOrder = 23
                    end
                    object UniDBEdit79: TUniDBEdit
                      Left = 13
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'COMPRAHISTO'
                      TabOrder = 24
                    end
                    object UniDBEdit80: TUniDBEdit
                      Left = 64
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'CANTIENALBA'
                      TabOrder = 25
                    end
                    object UniDBEdit81: TUniDBEdit
                      Left = 163
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'MERMA'
                      TabOrder = 26
                    end
                    object UniDBEdit82: TUniDBEdit
                      Left = 214
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'VENDIDOS'
                      TabOrder = 27
                    end
                    object UniDBEdit83: TUniDBEdit
                      Left = 112
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'RECLAMADO'
                      TabOrder = 28
                    end
                    object UniDBEdit84: TUniDBEdit
                      Left = 262
                      Top = 126
                      Width = 49
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'ADENDUM'
                      TabOrder = 29
                    end
                    object UniDBDateTimePicker4: TUniDBDateTimePicker
                      Left = 315
                      Top = 126
                      Width = 98
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'FECHADEVOL'
                      DateTime = 43375.000000000000000000
                      DateFormat = 'dd/MM/yyyy'
                      TimeFormat = 'HH:mm:ss'
                      TabOrder = 30
                    end
                    object UniDBDateTimePicker5: TUniDBDateTimePicker
                      Left = 415
                      Top = 126
                      Width = 98
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'FECHAABONO'
                      DateTime = 43375.000000000000000000
                      DateFormat = 'dd/MM/yyyy'
                      TimeFormat = 'HH:mm:ss'
                      TabOrder = 31
                    end
                    object UniLabel123: TUniLabel
                      Left = 262
                      Top = 108
                      Width = 25
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Num.'
                      TabOrder = 32
                    end
                    object UniLabel124: TUniLabel
                      Left = 112
                      Top = 108
                      Width = 30
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Recla.'
                      TabOrder = 33
                    end
                  end
                  object UniPanel27: TUniPanel
                    Left = 0
                    Top = 158
                    Width = 1019
                    Height = 60
                    Hint = '60'
                    ShowHint = True
                    ParentShowHint = False
                    Align = alTop
                    Anchors = [akLeft, akTop, akRight]
                    TabOrder = 1
                    Caption = ''
                    object UniDBEdit85: TUniDBEdit
                      Left = 6
                      Top = 15
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'MARGEN'
                      TabOrder = 1
                    end
                    object UniDBEdit86: TUniDBEdit
                      Left = 6
                      Top = 35
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'MARGEN2'
                      TabOrder = 2
                    end
                    object UniDBEdit87: TUniDBEdit
                      Left = 65
                      Top = 35
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'ENCARTE2'
                      TabOrder = 3
                    end
                    object UniDBEdit88: TUniDBEdit
                      Left = 65
                      Top = 15
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'ENCARTE'
                      TabOrder = 4
                    end
                    object UniLabel125: TUniLabel
                      Left = 13
                      Top = 1
                      Width = 36
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Margen'
                      TabOrder = 5
                    end
                    object UniLabel126: TUniLabel
                      Left = 69
                      Top = 1
                      Width = 37
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Encarte'
                      TabOrder = 6
                    end
                    object UniDBEdit89: TUniDBEdit
                      Left = 128
                      Top = 15
                      Width = 27
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TIVA'
                      TabOrder = 7
                    end
                    object UniDBEdit90: TUniDBEdit
                      Left = 128
                      Top = 35
                      Width = 27
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TIVA2'
                      TabOrder = 8
                    end
                    object UniDBEdit91: TUniDBEdit
                      Left = 155
                      Top = 15
                      Width = 44
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TPCIVA'
                      TabOrder = 9
                    end
                    object UniDBEdit92: TUniDBEdit
                      Left = 199
                      Top = 15
                      Width = 44
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TPCRE'
                      TabOrder = 10
                    end
                    object UniDBEdit93: TUniDBEdit
                      Left = 155
                      Top = 35
                      Width = 44
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TPCIVA2'
                      TabOrder = 11
                    end
                    object UniDBEdit94: TUniDBEdit
                      Left = 199
                      Top = 35
                      Width = 44
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'TPCRE2'
                      TabOrder = 12
                    end
                    object UniLabel127: TUniLabel
                      Left = 130
                      Top = 1
                      Width = 20
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Tipo'
                      TabOrder = 13
                    end
                    object UniLabel128: TUniLabel
                      Left = 160
                      Top = 1
                      Width = 28
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = '%IVA'
                      TabOrder = 14
                    end
                    object UniLabel129: TUniLabel
                      Left = 207
                      Top = 1
                      Width = 27
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = '% RE'
                      TabOrder = 15
                    end
                    object UniDBEdit95: TUniDBEdit
                      Left = 244
                      Top = 15
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOCOSTE'
                      TabOrder = 16
                    end
                    object UniDBEdit96: TUniDBEdit
                      Left = 306
                      Top = 15
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOCOSTETOT'
                      TabOrder = 17
                    end
                    object UniDBEdit97: TUniDBEdit
                      Left = 244
                      Top = 35
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOCOSTE2'
                      TabOrder = 18
                    end
                    object UniDBEdit98: TUniDBEdit
                      Left = 306
                      Top = 35
                      Width = 60
                      Height = 20
                      Hint = ''
                      ShowHint = True
                      DataField = 'PRECIOCOSTETOT2'
                      TabOrder = 19
                    end
                    object UniLabel130: TUniLabel
                      Left = 254
                      Top = 1
                      Width = 42
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Pr.Coste'
                      TabOrder = 20
                    end
                    object UniLabel131: TUniLabel
                      Left = 310
                      Top = 1
                      Width = 52
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Pr.Cte+RE'
                      TabOrder = 21
                    end
                    object UniPanel28: TUniPanel
                      Left = 372
                      Top = -1
                      Width = 369
                      Height = 57
                      Hint = ''
                      ShowHint = True
                      TabOrder = 22
                      BorderStyle = ubsNone
                      Caption = ''
                      object UniDBEdit99: TUniDBEdit
                        Left = 5
                        Top = 17
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOMPRA'
                        TabOrder = 1
                      end
                      object UniDBEdit100: TUniDBEdit
                        Left = 70
                        Top = 17
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOSTE'
                        TabOrder = 2
                      end
                      object UniDBEdit101: TUniDBEdit
                        Left = 5
                        Top = 37
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOMPRA2'
                        TabOrder = 3
                      end
                      object UniDBEdit102: TUniDBEdit
                        Left = 70
                        Top = 37
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOSTE2'
                        TabOrder = 4
                      end
                      object UniLabel132: TUniLabel
                        Left = 76
                        Top = 3
                        Width = 38
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'V.Coste'
                        TabOrder = 5
                      end
                      object UniLabel133: TUniLabel
                        Left = 9
                        Top = 3
                        Width = 47
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'v.Compra'
                        TabOrder = 6
                      end
                      object UniDBEdit103: TUniDBEdit
                        Left = 135
                        Top = 17
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOSTETOT'
                        TabOrder = 7
                      end
                      object UniDBEdit104: TUniDBEdit
                        Left = 200
                        Top = 17
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORVENTA'
                        TabOrder = 8
                      end
                      object UniDBEdit105: TUniDBEdit
                        Left = 135
                        Top = 37
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORCOSTETOT2'
                        TabOrder = 9
                      end
                      object UniDBEdit106: TUniDBEdit
                        Left = 200
                        Top = 37
                        Width = 65
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        DataField = 'VALORVENTA2'
                        TabOrder = 10
                      end
                      object UniLabel134: TUniLabel
                        Left = 136
                        Top = 3
                        Width = 65
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'V.Coste + RE'
                        TabOrder = 11
                      end
                      object UniLabel135: TUniLabel
                        Left = 216
                        Top = 3
                        Width = 30
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'V.Vta.'
                        TabOrder = 12
                      end
                      object UniLabel136: TUniLabel
                        Left = 275
                        Top = 13
                        Width = 28
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'Venta'
                        TabOrder = 13
                      end
                      object UniLabel137: TUniLabel
                        Left = 275
                        Top = 26
                        Width = 52
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'Devolucion'
                        TabOrder = 14
                      end
                      object UniLabel138: TUniLabel
                        Left = 275
                        Top = 39
                        Width = 32
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'Merma'
                        TabOrder = 15
                      end
                      object UniDBText15: TUniDBText
                        Left = 305
                        Top = 13
                        Width = 62
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        DataField = 'VENTAHISTO'
                      end
                      object UniDBText16: TUniDBText
                        Left = 305
                        Top = 26
                        Width = 62
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        DataField = 'DEVOLHISTO'
                      end
                      object UniDBText17: TUniDBText
                        Left = 305
                        Top = 39
                        Width = 62
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        DataField = 'MERMAHISTO'
                      end
                    end
                    object UniLabel139: TUniLabel
                      Left = 742
                      Top = 2
                      Width = 25
                      Height = 13
                      Hint = ''
                      Visible = False
                      ShowHint = True
                      Caption = 'Alba.'
                      TabOrder = 23
                    end
                    object UniLabel140: TUniLabel
                      Left = 828
                      Top = 2
                      Width = 24
                      Height = 13
                      Hint = ''
                      Visible = False
                      ShowHint = True
                      Caption = 'Total'
                      TabOrder = 24
                    end
                    object UniLabel141: TUniLabel
                      Left = 762
                      Top = 25
                      Width = 37
                      Height = 13
                      Hint = ''
                      Visible = False
                      ShowHint = True
                      Caption = 'Compra'
                      TabOrder = 25
                    end
                    object UniDBText18: TUniDBText
                      Left = 770
                      Top = 2
                      Width = 62
                      Height = 13
                      Hint = ''
                      Visible = False
                      ShowHint = True
                      DataField = 'CANTIENALBA'
                    end
                    object UniDBText19: TUniDBText
                      Left = 804
                      Top = 25
                      Width = 62
                      Height = 13
                      Hint = ''
                      Visible = False
                      ShowHint = True
                      DataField = 'COMPRAHISTO'
                    end
                  end
                  object UniDBGrid3: TUniDBGrid
                    Left = 0
                    Top = 218
                    Width = 1019
                    Height = 348
                    Hint = '348'
                    ShowHint = True
                    ParentShowHint = False
                    WebOptions.FetchAll = True
                    LoadMask.Message = 'Loading data...'
                    Align = alTop
                    Anchors = [akLeft, akTop, akRight]
                    TabOrder = 2
                    Columns = <
                      item
                        FieldName = 'DEVUELTOS'
                        Title.Caption = 'Devueltos'
                        Width = 64
                      end
                      item
                        FieldName = 'DESCRIPCION'
                        Title.Caption = 'Descripcion'
                        Width = 244
                        ReadOnly = True
                      end
                      item
                        FieldName = 'ADENDUM'
                        Title.Caption = 'Num'
                        Width = 34
                      end
                      item
                        FieldName = 'PrecioTotal'
                        Title.Caption = 'Total PVP'
                        Width = 64
                      end
                      item
                        FieldName = 'REFEPROVE'
                        Title.Caption = 'Codi.Dist.'
                        Width = 124
                      end
                      item
                        FieldName = 'PAQUETE'
                        Title.Caption = 'Paquete'
                        Width = 64
                      end
                      item
                        FieldName = 'CANTIENALBA'
                        Title.Caption = 'A Stock'
                        Width = 64
                      end
                      item
                        FieldName = 'MERMA'
                        Title.Caption = 'Merma'
                        Width = 64
                      end
                      item
                        FieldName = 'COMPRAHISTO'
                        Title.Caption = 'His.Compra'
                        Width = 64
                        ReadOnly = True
                      end
                      item
                        FieldName = 'MERMAHISTO'
                        Title.Caption = 'His.Merma'
                        Width = 64
                        ReadOnly = True
                      end
                      item
                        FieldName = 'DEVOLHISTO'
                        Title.Caption = 'His.Devol.'
                        Width = 64
                        ReadOnly = True
                      end
                      item
                        FieldName = 'VENTAHISTO'
                        Title.Caption = 'His.Venta'
                        Width = 64
                        ReadOnly = True
                      end
                      item
                        FieldName = 'VENDIDOS'
                        Title.Caption = 'Vendidos'
                        Width = 64
                      end
                      item
                        FieldName = 'ID_ARTICULO'
                        Title.Caption = 'Id.Arti'
                        Width = 64
                      end
                      item
                        FieldName = 'RECLAMADO'
                        Title.Caption = 'Reclamado'
                        Width = 64
                      end
                      item
                        FieldName = 'FECHADEVOL'
                        Title.Caption = 'Fecha Devol'
                        Width = 64
                      end
                      item
                        FieldName = 'FECHAAVISO'
                        Title.Caption = 'Fecha Aviso'
                        Width = 64
                      end
                      item
                        FieldName = 'SWDEVOLUCION'
                        Title.Caption = 'Sit. Devol'
                        Width = 64
                      end
                      item
                        FieldName = 'FECHA'
                        Title.Caption = 'Fecha'
                        Width = 64
                      end
                      item
                        FieldName = 'IDDEVOLUCABE'
                        Title.Caption = 'IDDEVOLUCABE'
                        Width = 80
                      end
                      item
                        FieldName = 'IDSTOCABE'
                        Title.Caption = 'IDSTOCABE'
                        Width = 64
                      end
                      item
                        FieldName = 'ID_PROVEEDOR'
                        Title.Caption = 'ID_PROVEEDOR'
                        Width = 82
                      end
                      item
                        FieldName = 'NOMBRE'
                        Title.Caption = 'NOMBRE'
                        Width = 244
                        ReadOnly = True
                      end
                      item
                        FieldName = 'BARRAS'
                        Title.Caption = 'C. Barras'
                        Width = 82
                        ReadOnly = True
                      end
                      item
                        FieldName = 'CANTIDAD'
                        Title.Caption = 'CANTIDAD'
                        Width = 64
                      end
                      item
                        FieldName = 'FECHACOMPRA'
                        Title.Caption = 'FECHACOMPRA'
                        Width = 80
                        ReadOnly = True
                      end>
                  end
                end
                object UniTabSheet9: TUniTabSheet
                  Hint = ''
                  ShowHint = True
                  Caption = 'Pendiente'
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 256
                  ExplicitHeight = 376
                  object UniPanel29: TUniPanel
                    Left = 0
                    Top = 0
                    Width = 1019
                    Height = 140
                    Hint = ''
                    ShowHint = True
                    Align = alTop
                    Anchors = [akLeft, akTop, akRight]
                    TabOrder = 0
                    Caption = ''
                    ExplicitWidth = 1040
                    object UniPanel30: TUniPanel
                      Left = 1
                      Top = 1
                      Width = 627
                      Height = 138
                      Hint = ''
                      ShowHint = True
                      Align = alLeft
                      Anchors = [akLeft, akTop, akBottom]
                      TabOrder = 1
                      Caption = ''
                      object UniLabel142: TUniLabel
                        Left = 3
                        Top = 8
                        Width = 86
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'Localizar C.barras'
                        TabOrder = 1
                      end
                      object UniLabel143: TUniLabel
                        Left = 293
                        Top = 8
                        Width = 53
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'A Devolver'
                        TabOrder = 2
                      end
                      object UniDBText20: TUniDBText
                        Left = 217
                        Top = 9
                        Width = 62
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        DataField = 'ADENDUM'
                      end
                      object UniDBEdit107: TUniDBEdit
                        Left = 3
                        Top = 22
                        Width = 208
                        Height = 26
                        Hint = ''
                        ShowHint = True
                        TabOrder = 4
                      end
                      object UniDBEdit108: TUniDBEdit
                        Left = 211
                        Top = 22
                        Width = 70
                        Height = 26
                        Hint = ''
                        ShowHint = True
                        TabOrder = 5
                      end
                      object UniDBEdit109: TUniDBEdit
                        Left = 281
                        Top = 22
                        Width = 70
                        Height = 26
                        Hint = ''
                        ShowHint = True
                        DataField = 'A_Devolver'
                        TabOrder = 6
                      end
                      object UniSpeedButton6: TUniSpeedButton
                        Left = 351
                        Top = 6
                        Width = 50
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = '+'
                        ParentColor = False
                        Color = clWindow
                        TabOrder = 7
                      end
                      object UniSpeedButton7: TUniSpeedButton
                        Left = 400
                        Top = 6
                        Width = 50
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = '-'
                        ParentColor = False
                        Color = clWindow
                        TabOrder = 8
                      end
                      object UniBitBtn24: TUniBitBtn
                        Left = 454
                        Top = 6
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Detalle'
                        TabOrder = 9
                      end
                      object UniBitBtn25: TUniBitBtn
                        Left = 508
                        Top = 6
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Ult.Dev.'
                        TabOrder = 10
                      end
                      object UniBitBtn26: TUniBitBtn
                        Left = 563
                        Top = 6
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Todo Vta'
                        TabOrder = 11
                      end
                      object UniDBText21: TUniDBText
                        Left = 3
                        Top = 59
                        Width = 72
                        Height = 16
                        Hint = ''
                        ShowHint = True
                        DataField = 'DESCRIPCION'
                        ParentFont = False
                        Font.Height = -13
                      end
                      object UniPanel31: TUniPanel
                        Left = 0
                        Top = 80
                        Width = 281
                        Height = 58
                        Hint = ''
                        ShowHint = True
                        TabOrder = 13
                        Caption = ''
                        object UniLabel144: TUniLabel
                          Left = 3
                          Top = 3
                          Width = 32
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Prove:'
                          TabOrder = 1
                        end
                        object UniDBText22: TUniDBText
                          Left = 41
                          Top = 3
                          Width = 62
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          ParentFont = False
                        end
                        object UniDBText23: TUniDBText
                          Left = 100
                          Top = 3
                          Width = 62
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          ParentFont = False
                        end
                        object UniDBEdit110: TUniDBEdit
                          Left = 3
                          Top = 20
                          Width = 156
                          Height = 26
                          Hint = ''
                          ShowHint = True
                          TabOrder = 4
                        end
                        object UniBitBtn27: TUniBitBtn
                          Left = 162
                          Top = 20
                          Width = 27
                          Height = 26
                          Hint = ''
                          ShowHint = True
                          Caption = ''
                          TabOrder = 5
                        end
                        object UniCheckBox1: TUniCheckBox
                          Left = 192
                          Top = 3
                          Width = 87
                          Height = 17
                          Hint = ''
                          ShowHint = True
                          Caption = 'Pase Auto'
                          TabOrder = 6
                        end
                        object UniCheckBox2: TUniCheckBox
                          Left = 192
                          Top = 18
                          Width = 75
                          Height = 17
                          Hint = ''
                          ShowHint = True
                          Caption = 'Teorica'
                          TabOrder = 7
                        end
                        object UniCheckBox3: TUniCheckBox
                          Left = 192
                          Top = 33
                          Width = 82
                          Height = 17
                          Hint = ''
                          ShowHint = True
                          Caption = 'Ver m'#225's +'
                          TabOrder = 8
                        end
                      end
                      object UniBitBtn28: TUniBitBtn
                        Left = 563
                        Top = 93
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'UniBitBtn2'
                        TabOrder = 14
                      end
                    end
                    object UniPanel32: TUniPanel
                      Left = 628
                      Top = 2
                      Width = 313
                      Height = 137
                      Hint = ''
                      ShowHint = True
                      TabOrder = 2
                      Caption = ''
                      object UniGroupBox4: TUniGroupBox
                        Left = 2
                        Top = -3
                        Width = 307
                        Height = 88
                        Hint = ''
                        ShowHint = True
                        Caption = 'Seleccion por Fechas'
                        TabOrder = 1
                        object UniLabel145: TUniLabel
                          Left = 4
                          Top = 40
                          Width = 34
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Desde:'
                          TabOrder = 1
                        end
                        object UniLabel146: TUniLabel
                          Left = 4
                          Top = 62
                          Width = 32
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Hasta:'
                          TabOrder = 2
                        end
                        object UniDBDateTimePicker7: TUniDBDateTimePicker
                          Left = 42
                          Top = 36
                          Width = 96
                          Hint = ''
                          ShowHint = True
                          DateTime = 43375.000000000000000000
                          DateFormat = 'dd/MM/yyyy'
                          TimeFormat = 'HH:mm:ss'
                          TabOrder = 3
                        end
                        object UniDBDateTimePicker9: TUniDBDateTimePicker
                          Left = 42
                          Top = 58
                          Width = 96
                          Hint = ''
                          ShowHint = True
                          DateTime = 43375.000000000000000000
                          DateFormat = 'dd/MM/yyyy'
                          TimeFormat = 'HH:mm:ss'
                          TabOrder = 4
                        end
                        object UniDBDateTimePicker10: TUniDBDateTimePicker
                          Left = 138
                          Top = 36
                          Width = 96
                          Hint = ''
                          ShowHint = True
                          DateTime = 43375.000000000000000000
                          DateFormat = 'dd/MM/yyyy'
                          TimeFormat = 'HH:mm:ss'
                          TabOrder = 5
                        end
                        object UniDBDateTimePicker11: TUniDBDateTimePicker
                          Left = 138
                          Top = 58
                          Width = 96
                          Hint = ''
                          ShowHint = True
                          DateTime = 43375.000000000000000000
                          DateFormat = 'dd/MM/yyyy'
                          TimeFormat = 'HH:mm:ss'
                          TabOrder = 6
                        end
                        object UniBitBtn29: TUniBitBtn
                          Left = 236
                          Top = 39
                          Width = 55
                          Height = 42
                          Hint = ''
                          ShowHint = True
                          Caption = 'Ejecutar'
                          TabOrder = 7
                        end
                        object UniLabel147: TUniLabel
                          Left = 53
                          Top = 21
                          Width = 59
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'F.Recepcion'
                          TabOrder = 8
                        end
                        object UniLabel148: TUniLabel
                          Left = 150
                          Top = 21
                          Width = 62
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'F.Devolucion'
                          TabOrder = 9
                        end
                        object UniLabel149: TUniLabel
                          Left = 240
                          Top = 17
                          Width = 28
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Regs.'
                          TabOrder = 10
                        end
                      end
                      object UniSpeedButton8: TUniSpeedButton
                        Left = 1
                        Top = 92
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Traspas'
                        ParentColor = False
                        Color = clWindow
                        TabOrder = 2
                      end
                      object UniLabel150: TUniLabel
                        Left = 98
                        Top = 96
                        Width = 62
                        Height = 13
                        Hint = ''
                        ShowHint = True
                        Caption = 'Id Recepcion'
                        TabOrder = 3
                      end
                      object UniDBEdit111: TUniDBEdit
                        Left = 94
                        Top = 109
                        Width = 71
                        Height = 20
                        Hint = ''
                        ShowHint = True
                        TabOrder = 4
                      end
                      object UniSpeedButton9: TUniSpeedButton
                        Left = 181
                        Top = 92
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Teorico'
                        ParentColor = False
                        Color = clWindow
                        TabOrder = 5
                      end
                      object UniSpeedButton10: TUniSpeedButton
                        Left = 238
                        Top = 92
                        Width = 55
                        Height = 42
                        Hint = ''
                        ShowHint = True
                        Caption = 'Limpiar'
                        ParentColor = False
                        Color = clWindow
                        TabOrder = 6
                      end
                    end
                  end
                  object UniPageControl8: TUniPageControl
                    Left = 0
                    Top = 140
                    Width = 1019
                    Height = 426
                    Hint = ''
                    ShowHint = True
                    ActivePage = UniTabSheet10
                    Align = alClient
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    TabOrder = 1
                    ExplicitWidth = 1040
                    ExplicitHeight = 236
                    object UniTabSheet10: TUniTabSheet
                      Hint = ''
                      ShowHint = True
                      Caption = 'UniTabSheet1'
                      object UniDBGrid6: TUniDBGrid
                        Left = 0
                        Top = 0
                        Width = 1011
                        Height = 398
                        Hint = ''
                        ShowHint = True
                        LoadMask.Message = 'Loading data...'
                        Align = alClient
                        Anchors = [akLeft, akTop, akRight, akBottom]
                        TabOrder = 0
                        Columns = <
                          item
                            FieldName = 'DESCRIPCION'
                            Title.Caption = 'Descripcion'
                            Width = 244
                            ReadOnly = True
                          end
                          item
                            FieldName = 'BARRAS'
                            Title.Caption = 'C.Barras'
                            Width = 82
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ADENDUM'
                            Title.Caption = 'Num'
                            Width = 34
                          end
                          item
                            FieldName = 'PrecioTotal'
                            Title.Caption = 'TotalPVP'
                            Width = 64
                          end
                          item
                            FieldName = 'FECHACOMPRA'
                            Title.Caption = 'Compra'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'DevolTeorica'
                            Title.Caption = 'DevolTeorica'
                            Width = 66
                          end
                          item
                            FieldName = 'A_Devolver'
                            Title.Caption = 'A_Devolver'
                            Width = 64
                          end
                          item
                            FieldName = 'A_Merma'
                            Title.Caption = 'A_Merma'
                            Width = 64
                          end
                          item
                            FieldName = 'A_Venta'
                            Title.Caption = 'A_Venta'
                            Width = 64
                          end
                          item
                            FieldName = 'A_Stock'
                            Title.Caption = 'A_Stock'
                            Width = 64
                          end
                          item
                            FieldName = 'REGISTROS'
                            Title.Caption = 'Regs.'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'COMPRAS'
                            Title.Caption = 'Compras'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'VENTAHISTO'
                            Title.Caption = 'His.Venta'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'DEVOLHISTO'
                            Title.Caption = 'His.Devol'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'MERMAHISTO'
                            Title.Caption = 'His.Merma'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'COMPRAHISTO'
                            Title.Caption = 'CompraHisto'
                            Width = 65
                            ReadOnly = True
                          end
                          item
                            FieldName = 'PRECIOVENTA'
                            Title.Caption = 'Precio1'
                            Width = 64
                          end
                          item
                            FieldName = 'PRECIOVENTA2'
                            Title.Caption = 'Precio2'
                            Width = 64
                          end
                          item
                            FieldName = 'TBARRAS'
                            Title.Caption = 'T.Barras'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ID_PROVEEDOR'
                            Title.Caption = 'Proveedor'
                            Width = 64
                          end
                          item
                            FieldName = 'NOMBRE'
                            Title.Caption = 'Nombre Proveedor'
                            Width = 244
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ID_ARTICULO'
                            Title.Caption = 'ID_Articulo'
                            Width = 64
                          end
                          item
                            FieldName = 'FECHADEVOL'
                            Title.Caption = 'Devoluci'#243'n'
                            Width = 64
                          end>
                      end
                    end
                    object UniTabSheet11: TUniTabSheet
                      Hint = ''
                      ShowHint = True
                      Caption = 'UniTabSheet2'
                      object UniDBGrid7: TUniDBGrid
                        Left = 0
                        Top = 39
                        Width = 1011
                        Height = 359
                        Hint = ''
                        ShowHint = True
                        LoadMask.Message = 'Loading data...'
                        Align = alClient
                        Anchors = [akLeft, akTop, akRight, akBottom]
                        TabOrder = 0
                        Columns = <
                          item
                            FieldName = 'ID_HISARTI'
                            Title.Caption = 'N.Orden'
                            Width = 64
                          end
                          item
                            FieldName = 'DESCRIPCION'
                            Title.Caption = 'Descripcion'
                            Width = 244
                            ReadOnly = True
                          end
                          item
                            FieldName = 'BARRAS'
                            Title.Caption = 'C.Barras'
                            Width = 82
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ADENDUM'
                            Title.Caption = 'Num.'
                            Width = 34
                          end
                          item
                            FieldName = 'CANTIDAD'
                            Title.Caption = 'CANTIDAD'
                            Width = 64
                          end
                          item
                            FieldName = 'PRECIOVENTA'
                            Title.Caption = 'Precio1'
                            Width = 64
                          end
                          item
                            FieldName = 'TBARRAS'
                            Title.Caption = 'T.Barras'
                            Width = 64
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ID_PROVEEDOR'
                            Title.Caption = 'Proveedor'
                            Width = 64
                          end
                          item
                            FieldName = 'NOMBRE'
                            Title.Caption = 'Nombre Proveedor'
                            Width = 244
                            ReadOnly = True
                          end
                          item
                            FieldName = 'ID_ARTICULO'
                            Title.Caption = 'ID_Articulo'
                            Width = 64
                          end
                          item
                            FieldName = 'REFEPROVE'
                            Title.Caption = 'REFEPROVE'
                            Width = 124
                            ReadOnly = True
                          end>
                      end
                      object UniPanel33: TUniPanel
                        Left = 0
                        Top = 0
                        Width = 1011
                        Height = 39
                        Hint = ''
                        ShowHint = True
                        Align = alTop
                        Anchors = [akLeft, akTop, akRight]
                        TabOrder = 1
                        Caption = ''
                        object UniLabel151: TUniLabel
                          Left = 233
                          Top = 9
                          Width = 96
                          Height = 19
                          Hint = ''
                          ShowHint = True
                          Caption = 'Detalle devol.'
                          ParentFont = False
                          Font.Height = -16
                          TabOrder = 1
                        end
                        object UniBitBtn30: TUniBitBtn
                          Left = 335
                          Top = 8
                          Width = 129
                          Height = 29
                          Hint = ''
                          ShowHint = True
                          Caption = 'UniBitBtn8'
                          TabOrder = 2
                        end
                        object UniLabel152: TUniLabel
                          Left = 470
                          Top = 0
                          Width = 43
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Cantidad'
                          TabOrder = 3
                        end
                        object UniLabel153: TUniLabel
                          Left = 550
                          Top = 0
                          Width = 54
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'Descripcion'
                          TabOrder = 4
                        end
                        object UniLabel154: TUniLabel
                          Left = 834
                          Top = 1
                          Width = 42
                          Height = 13
                          Hint = ''
                          ShowHint = True
                          Caption = 'C.Barras'
                          TabOrder = 5
                        end
                        object UniDBEdit112: TUniDBEdit
                          Left = 470
                          Top = 13
                          Width = 63
                          Height = 22
                          Hint = ''
                          ShowHint = True
                          DataField = 'CANTIDAD'
                          TabOrder = 6
                        end
                        object UniDBNavigator2: TUniDBNavigator
                          Left = 0
                          Top = 2
                          Width = 225
                          Height = 37
                          Hint = ''
                          ShowHint = True
                          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
                          TabOrder = 7
                        end
                        object UniDBEdit113: TUniDBEdit
                          Left = 532
                          Top = 13
                          Width = 293
                          Height = 22
                          Hint = ''
                          ShowHint = True
                          DataField = 'DESCRIPCION'
                          TabOrder = 8
                        end
                        object UniDBEdit114: TUniDBEdit
                          Left = 829
                          Top = 13
                          Width = 104
                          Height = 22
                          Hint = ''
                          ShowHint = True
                          DataField = 'BARRAS'
                          TabOrder = 9
                        end
                        object UniDBEdit115: TUniDBEdit
                          Left = 936
                          Top = 13
                          Width = 42
                          Height = 22
                          Hint = ''
                          ShowHint = True
                          DataField = 'ADENDUM'
                          TabOrder = 10
                        end
                      end
                    end
                  end
                end
                object UniTabSheet12: TUniTabSheet
                  Hint = ''
                  ShowHint = True
                  Caption = 'Anulado'
                  object UniDBMemo4: TUniDBMemo
                    Left = 1
                    Top = 263
                    Width = 1005
                    Height = 193
                    Hint = ''
                    ShowHint = True
                    TabOrder = 0
                  end
                  object UniPanel34: TUniPanel
                    Left = 8
                    Top = 16
                    Width = 184
                    Height = 53
                    Hint = ''
                    ShowHint = True
                    TabOrder = 1
                    Caption = ''
                    object UniBitBtn31: TUniBitBtn
                      Left = 2
                      Top = 4
                      Width = 60
                      Height = 45
                      Hint = ''
                      ShowHint = True
                      Caption = 'Nueva'
                      TabOrder = 1
                    end
                    object UniBitBtn32: TUniBitBtn
                      Left = 61
                      Top = 4
                      Width = 60
                      Height = 45
                      Hint = ''
                      ShowHint = True
                      Caption = 'Modificar'
                      TabOrder = 2
                    end
                  end
                end
              end
            end
            object UniTabSheet13: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'TabDirectoDevolucion'
              object UniPanel35: TUniPanel
                Left = 0
                Top = 0
                Width = 1027
                Height = 49
                Hint = ''
                ShowHint = True
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 0
                Caption = ''
                object UniContainerPanel5: TUniContainerPanel
                  Left = 1
                  Top = 1
                  Width = 488
                  Height = 47
                  Hint = ''
                  ShowHint = True
                  ParentColor = False
                  Color = clRed
                  Align = alLeft
                  Anchors = [akLeft, akTop, akBottom]
                  TabOrder = 1
                  object UniBitBtn33: TUniBitBtn
                    Left = 8
                    Top = 4
                    Width = 125
                    Height = 42
                    Hint = ''
                    ShowHint = True
                    Caption = 'Cargar Devolucion'
                    TabOrder = 1
                  end
                  object UniBitBtn34: TUniBitBtn
                    Left = 152
                    Top = 4
                    Width = 125
                    Height = 42
                    Hint = ''
                    ShowHint = True
                    Caption = 'Volver a Devoluci'#243'n'
                    TabOrder = 2
                  end
                  object UniBitBtn35: TUniBitBtn
                    Left = 350
                    Top = 4
                    Width = 125
                    Height = 42
                    Hint = ''
                    ShowHint = True
                    Caption = 'Borrar Directo'
                    TabOrder = 3
                  end
                end
              end
              object UniDBGrid8: TUniDBGrid
                Left = 0
                Top = 49
                Width = 1027
                Height = 545
                Hint = ''
                ShowHint = True
                LoadMask.Message = 'Loading data...'
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 1
                Columns = <
                  item
                    FieldName = 'BARRAS'
                    Title.Caption = 'Barras'
                    Width = 82
                    ReadOnly = True
                  end
                  item
                    FieldName = 'ADENDUM'
                    Title.Caption = 'Num.'
                    Width = 34
                  end
                  item
                    FieldName = 'DESCRIPCION'
                    Title.Caption = 'Descripcion'
                    Width = 244
                    ReadOnly = True
                  end
                  item
                    FieldName = 'CANTIDAD'
                    Title.Caption = 'Cantidad'
                    Width = 64
                  end
                  item
                    FieldName = 'ABONO'
                    Title.Caption = 'A devolver'
                    Width = 64
                  end
                  item
                    FieldName = 'REFEPROVE'
                    Title.Caption = 'Codigo Proveedor'
                    Width = 124
                  end
                  item
                    FieldName = 'NOMBRE'
                    Title.Caption = 'Proveedor'
                    Width = 244
                    ReadOnly = True
                  end
                  item
                    FieldName = 'ID_ARTICULO'
                    Title.Caption = 'ID'
                    Width = 64
                  end>
              end
              object UniPanel36: TUniPanel
                Left = 632
                Top = 3
                Width = 112
                Height = 113
                Hint = ''
                ShowHint = True
                TabOrder = 2
                Caption = ''
                object UniBitBtn36: TUniBitBtn
                  Left = 8
                  Top = 7
                  Width = 97
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = 'Traspasar solo 1'
                  TabOrder = 1
                end
                object UniBitBtn37: TUniBitBtn
                  Left = 8
                  Top = 63
                  Width = 97
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = 'Traspasar Todos'
                  TabOrder = 2
                end
              end
            end
            object UniTabSheet14: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'TabPdteDevolu'
              object UniPanel37: TUniPanel
                Left = 0
                Top = 0
                Width = 1027
                Height = 49
                Hint = ''
                ShowHint = True
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 0
                Caption = ''
                object UniContainerPanel6: TUniContainerPanel
                  Left = 1
                  Top = 1
                  Width = 398
                  Height = 47
                  Hint = ''
                  ShowHint = True
                  ParentColor = False
                  Color = clLime
                  Align = alLeft
                  Anchors = [akLeft, akTop, akBottom]
                  TabOrder = 1
                  object UniBitBtn38: TUniBitBtn
                    Left = 8
                    Top = 4
                    Width = 125
                    Height = 42
                    Hint = ''
                    ShowHint = True
                    Caption = 'Cargar Devolucion'
                    TabOrder = 1
                  end
                  object UniBitBtn39: TUniBitBtn
                    Left = 256
                    Top = 4
                    Width = 125
                    Height = 42
                    Hint = ''
                    ShowHint = True
                    Caption = 'Volver a Devoluci'#243'n'
                    TabOrder = 2
                  end
                end
              end
              object UniDBGrid9: TUniDBGrid
                Left = 0
                Top = 49
                Width = 1027
                Height = 545
                Hint = ''
                ShowHint = True
                LoadMask.Message = 'Loading data...'
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 1
                Columns = <
                  item
                    FieldName = 'BARRAS'
                    Title.Caption = 'BARRAS'
                    Width = 82
                    ReadOnly = True
                  end
                  item
                    FieldName = 'ADENDUM'
                    Title.Caption = 'NUm'
                    Width = 34
                  end
                  item
                    FieldName = 'DESCRIPCION'
                    Title.Caption = 'DESCRIPCION'
                    Width = 244
                    ReadOnly = True
                  end
                  item
                    FieldName = 'ID_ARTICULO'
                    Title.Caption = 'IdArticulo'
                    Width = 64
                  end
                  item
                    FieldName = 'CANTIDAD'
                    Title.Caption = 'Cantidad'
                    Width = 64
                  end
                  item
                    FieldName = 'CANTIENALBA'
                    Title.Caption = 'CantiEnAlba'
                    Width = 64
                  end
                  item
                    FieldName = 'PAQUETE'
                    Title.Caption = 'Paquete'
                    Width = 64
                  end
                  item
                    FieldName = 'DEVUELTOS'
                    Title.Caption = 'Devueltos'
                    Width = 64
                  end
                  item
                    FieldName = 'VENDIDOS'
                    Title.Caption = 'Vendidos'
                    Width = 64
                  end
                  item
                    FieldName = 'MERMA'
                    Title.Caption = 'Merma'
                    Width = 64
                  end>
              end
            end
          end
        end
        object UniTabSheet15: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'tabUtilidades'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 537
          object UniPanel38: TUniPanel
            Left = 0
            Top = 0
            Width = 1035
            Height = 549
            Hint = ''
            ShowHint = True
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            Caption = ''
            ExplicitWidth = 1056
            ExplicitHeight = 537
            object UniBitBtn40: TUniBitBtn
              Left = 12
              Top = 10
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Reclama'
              TabOrder = 1
            end
            object UniCheckBox4: TUniCheckBox
              Left = 10
              Top = 86
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Todos los proveedores'
              TabOrder = 2
            end
            object UniCheckBox5: TUniCheckBox
              Left = 10
              Top = 62
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Checked = True
              Caption = 'L'#237'nea autom'#225'tica'
              TabOrder = 3
            end
            object UniCheckBox6: TUniCheckBox
              Left = 10
              Top = 110
              Width = 217
              Height = 17
              Hint = ''
              ShowHint = True
              Checked = True
              Caption = 'Devoluci'#243'n de solo este proveedor'
              TabOrder = 4
            end
            object UniCheckBox7: TUniCheckBox
              Left = 10
              Top = 134
              Width = 217
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Acceder por c'#243'digo de distribuidora'
              TabOrder = 5
            end
            object UniCheckBox8: TUniCheckBox
              Left = 10
              Top = 159
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Devolver Sumando'
              TabOrder = 6
            end
            object UniCheckBox9: TUniCheckBox
              Left = 10
              Top = 182
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Separando precios'
              TabOrder = 7
            end
            object UniCheckBox10: TUniCheckBox
              Left = 10
              Top = 207
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Checked = True
              Caption = 'Solo Devol.Teorica'
              TabOrder = 8
            end
            object UniCheckBox11: TUniCheckBox
              Left = 10
              Top = 231
              Width = 217
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Cerrar Compras ya vendidas / devueltas'
              TabOrder = 9
            end
            object UniCheckBox12: TUniCheckBox
              Left = 10
              Top = 256
              Width = 179
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Abrir Movimientos cerrados'
              TabOrder = 10
            end
            object UniCheckBox13: TUniCheckBox
              Left = 136
              Top = 159
              Width = 91
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'SUMANDO'
              TabOrder = 11
            end
            object UniLabel155: TUniLabel
              Left = 179
              Top = 8
              Width = 45
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Etiquetas'
              TabOrder = 12
            end
            object UniDBEdit116: TUniDBEdit
              Left = 181
              Top = 24
              Width = 40
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 13
            end
            object UniBitBtn41: TUniBitBtn
              Left = 228
              Top = 10
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Listados'
              TabOrder = 14
            end
            object UniBitBtn42: TUniBitBtn
              Left = 498
              Top = 12
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Precios'
              TabOrder = 15
            end
            object UniBitBtn43: TUniBitBtn
              Left = 585
              Top = 12
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Salir'
              TabOrder = 16
            end
            object UniPageControl9: TUniPageControl
              Left = 306
              Top = 56
              Width = 321
              Height = 161
              Hint = ''
              ShowHint = True
              ActivePage = UniTabSheet17
              TabOrder = 17
              object UniTabSheet16: TUniTabSheet
                Hint = ''
                ShowHint = True
                Caption = 'Cerrar Compras'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 256
                ExplicitHeight = 128
                object UniLabel156: TUniLabel
                  Left = 24
                  Top = 8
                  Width = 93
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Cerrar hasta Fecha'
                  TabOrder = 0
                end
                object UniDBDateTimePicker12: TUniDBDateTimePicker
                  Left = 21
                  Top = 24
                  Width = 116
                  Hint = ''
                  ShowHint = True
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 1
                end
                object UniLabel157: TUniLabel
                  Left = 168
                  Top = 8
                  Width = 78
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Numero a cerrar'
                  TabOrder = 2
                end
                object UniDBEdit117: TUniDBEdit
                  Left = 168
                  Top = 25
                  Width = 97
                  Height = 21
                  Hint = ''
                  ShowHint = True
                  TabOrder = 3
                end
                object UniBitBtn44: TUniBitBtn
                  Left = 22
                  Top = 56
                  Width = 117
                  Height = 33
                  Hint = ''
                  ShowHint = True
                  Caption = 'Cerrar C'#243'digos'
                  TabOrder = 4
                end
                object UniLabel158: TUniLabel
                  Left = 152
                  Top = 72
                  Width = 53
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'lbRegistros'
                  TabOrder = 5
                end
                object UniLabel159: TUniLabel
                  Left = 16
                  Top = 104
                  Width = 96
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'LabelMensajeCerrar'
                  TabOrder = 6
                end
                object UniLabel160: TUniLabel
                  Left = 152
                  Top = 104
                  Width = 53
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'lbRegistros'
                  TabOrder = 7
                end
                object UniLabel161: TUniLabel
                  Left = 152
                  Top = 120
                  Width = 53
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'lbRegistros'
                  TabOrder = 8
                end
                object UniProgressBar1: TUniProgressBar
                  Left = 152
                  Top = 88
                  Width = 81
                  Height = 17
                  Hint = ''
                  ShowHint = True
                  TabOrder = 9
                end
              end
              object UniTabSheet17: TUniTabSheet
                Hint = ''
                ShowHint = True
                Caption = 'Abrir Movimientos'
                object UniBitBtn45: TUniBitBtn
                  Left = 14
                  Top = 56
                  Width = 139
                  Height = 33
                  Hint = ''
                  ShowHint = True
                  Caption = 'Traspasar Todos'
                  TabOrder = 0
                end
                object UniBitBtn46: TUniBitBtn
                  Left = 8
                  Top = 24
                  Width = 145
                  Height = 25
                  Hint = ''
                  ShowHint = True
                  Caption = 'Buscar Ultimo Numero'
                  TabOrder = 1
                end
                object UniDBEdit118: TUniDBEdit
                  Left = 168
                  Top = 25
                  Width = 121
                  Height = 21
                  Hint = ''
                  ShowHint = True
                  TabOrder = 2
                end
                object UniLabel162: TUniLabel
                  Left = 168
                  Top = 8
                  Width = 111
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Ultimo Numero Cerrado'
                  TabOrder = 3
                end
                object UniLabel163: TUniLabel
                  Left = 16
                  Top = 104
                  Width = 88
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'LabelMensajeAbrir'
                  TabOrder = 4
                end
              end
            end
          end
        end
        object UniTabSheet18: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'tabAlbaranNuevoModif'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 537
          object UniContainerPanel7: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 1035
            Height = 49
            Hint = ''
            ShowHint = True
            ParentColor = False
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            object UniBitBtn47: TUniBitBtn
              Left = 3
              Top = 3
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 1
              ImageIndex = 13
            end
            object UniDBEdit119: TUniDBEdit
              Left = 64
              Top = 23
              Width = 54
              Height = 22
              Hint = ''
              ShowHint = True
              TabOrder = 2
            end
            object UniLabel164: TUniLabel
              Left = 64
              Top = 4
              Width = 60
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Distribuidora'
              TabOrder = 3
            end
            object UniDBEdit120: TUniDBEdit
              Left = 124
              Top = 23
              Width = 157
              Height = 22
              Hint = ''
              ShowHint = True
              TabOrder = 4
            end
            object UniBitBtn48: TUniBitBtn
              Left = 299
              Top = 3
              Width = 116
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Cambiar Distribuidora'
              TabOrder = 5
              ImageIndex = 13
            end
            object UniBitBtn49: TUniBitBtn
              Left = 420
              Top = 3
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = ''
              TabOrder = 6
              ImageIndex = 52
            end
          end
          object UniPageControl10: TUniPageControl
            Left = 0
            Top = 49
            Width = 1035
            Height = 602
            Hint = ''
            ShowHint = True
            ActivePage = UniTabSheet19
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            ExplicitTop = 47
            ExplicitWidth = 1056
            object UniTabSheet19: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'Datos Cabecera documento'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 256
              ExplicitHeight = 128
              object UniContainerPanel8: TUniContainerPanel
                Left = 0
                Top = 0
                Width = 1027
                Height = 47
                Hint = ''
                ShowHint = True
                ParentShowHint = False
                ParentColor = False
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 0
                ExplicitWidth = 1048
                object UniLabel165: TUniLabel
                  Left = 12
                  Top = 23
                  Width = 68
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'N. Documento'
                  TabOrder = 1
                end
                object UniDBEdit121: TUniDBEdit
                  Left = 86
                  Top = 18
                  Width = 89
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  DataField = 'NDOCSTOCK'
                  TabOrder = 2
                end
                object UniDBEdit122: TUniDBEdit
                  Left = 181
                  Top = 18
                  Width = 53
                  Height = 22
                  Hint = ''
                  ShowHint = True
                  DataField = 'PAQUETE'
                  TabOrder = 3
                end
                object UniLabel166: TUniLabel
                  Left = 86
                  Top = 3
                  Width = 68
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'N. Documento'
                  TabOrder = 4
                end
                object UniLabel167: TUniLabel
                  Left = 181
                  Top = 3
                  Width = 40
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Paquete'
                  TabOrder = 5
                end
                object UniDBDateTimePicker13: TUniDBDateTimePicker
                  Left = 256
                  Top = 18
                  Width = 120
                  Hint = ''
                  ShowHint = True
                  DataField = 'FECHA'
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 6
                end
                object UniDBText24: TUniDBText
                  Left = 382
                  Top = 23
                  Width = 62
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  DataField = 'IDSTOCABE'
                end
                object UniLabel168: TUniLabel
                  Left = 256
                  Top = 3
                  Width = 29
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Fecha'
                  TabOrder = 8
                end
              end
              object UniPanel39: TUniPanel
                Left = 0
                Top = 47
                Width = 1027
                Height = 175
                Hint = ''
                ShowHint = True
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 1
                Caption = ''
                ExplicitWidth = 1048
                object UniPanel40: TUniPanel
                  Left = 1
                  Top = 1
                  Width = 1025
                  Height = 49
                  Hint = ''
                  ShowHint = True
                  Align = alTop
                  Anchors = [akLeft, akTop, akRight]
                  TabOrder = 1
                  Caption = ''
                  ExplicitWidth = 1046
                  object UniLabel169: TUniLabel
                    Left = 7
                    Top = 24
                    Width = 75
                    Height = 13
                    Hint = ''
                    ShowHint = True
                    Caption = 'Doc. Proveedor'
                    TabOrder = 1
                  end
                  object UniDBEdit123: TUniDBEdit
                    Left = 86
                    Top = 19
                    Width = 148
                    Height = 22
                    Hint = ''
                    ShowHint = True
                    DataField = 'DOCTOPROVE'
                    TabOrder = 2
                  end
                  object UniLabel170: TUniLabel
                    Left = 256
                    Top = 3
                    Width = 44
                    Height = 13
                    Hint = ''
                    ShowHint = True
                    Caption = 'de Fecha'
                    TabOrder = 3
                  end
                  object UniDBText25: TUniDBText
                    Left = 382
                    Top = 24
                    Width = 62
                    Height = 13
                    Hint = ''
                    ShowHint = True
                  end
                  object UniDBDateTimePicker14: TUniDBDateTimePicker
                    Left = 256
                    Top = 19
                    Width = 120
                    Hint = ''
                    ShowHint = True
                    DataField = 'DOCTOPROVEFECHA'
                    DateTime = 43374.000000000000000000
                    DateFormat = 'dd/MM/yyyy'
                    TimeFormat = 'HH:mm:ss'
                    TabOrder = 5
                  end
                end
                object UniPanel41: TUniPanel
                  Left = 1
                  Top = 50
                  Width = 1025
                  Height = 99
                  Hint = ''
                  ShowHint = True
                  Align = alTop
                  Anchors = [akLeft, akTop, akRight]
                  TabOrder = 2
                  Caption = ''
                  ExplicitWidth = 1046
                  DesignSize = (
                    1025
                    99)
                  object UniDBRadioGroup1: TUniDBRadioGroup
                    Left = 3
                    Top = 3
                    Width = 170
                    Height = 87
                    Hint = ''
                    ShowHint = True
                    DataField = 'DEVOLUSWTIPOINC'
                    Caption = 'Selecion por'
                    TabOrder = 1
                    Items.Strings = (
                      'Fecha Aviso'
                      'Fecha Devolucion'
                      'Docto.Distribuidor')
                    Values.Strings = (
                      '0'
                      '1'
                      '2')
                  end
                  object UniGroupBox5: TUniGroupBox
                    Left = 183
                    Top = 6
                    Width = 193
                    Height = 84
                    Hint = ''
                    ShowHint = True
                    Caption = ''
                    TabOrder = 2
                    object UniLabel171: TUniLabel
                      Left = 7
                      Top = 3
                      Width = 84
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Fecha Devolucion'
                      TabOrder = 1
                    end
                    object UniDBDateTimePicker15: TUniDBDateTimePicker
                      Left = 7
                      Top = 37
                      Width = 120
                      Hint = ''
                      ShowHint = True
                      DataField = 'DEVOLUFECHASEL'
                      DateTime = 43374.000000000000000000
                      DateFormat = 'dd/MM/yyyy'
                      TimeFormat = 'HH:mm:ss'
                      TabOrder = 2
                    end
                  end
                  object UniGroupBox6: TUniGroupBox
                    Left = 367
                    Top = 6
                    Width = 193
                    Height = 84
                    Hint = ''
                    ShowHint = True
                    Caption = ''
                    Anchors = []
                    TabOrder = 3
                    object UniBitBtn50: TUniBitBtn
                      Left = 4
                      Top = 13
                      Width = 70
                      Height = 39
                      Hint = ''
                      ShowHint = True
                      Caption = 'Doc. Distri'
                      TabOrder = 1
                    end
                    object UniLabel172: TUniLabel
                      Left = 19
                      Top = 66
                      Width = 44
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'de Fecha'
                      TabOrder = 2
                    end
                    object UniDBDateTimePicker16: TUniDBDateTimePicker
                      Left = 79
                      Top = 57
                      Width = 100
                      Hint = ''
                      ShowHint = True
                      DataField = 'DOCTOPROVEFECHA'
                      DateTime = 43374.000000000000000000
                      DateFormat = 'dd/MM/yyyy'
                      TimeFormat = 'HH:mm:ss'
                      TabOrder = 3
                    end
                    object UniDBEdit124: TUniDBEdit
                      Left = 79
                      Top = 29
                      Width = 100
                      Height = 22
                      Hint = ''
                      ShowHint = True
                      DataField = 'DEVOLUDOCTOPROVE'
                      TabOrder = 4
                    end
                    object UniLabel173: TUniLabel
                      Left = 79
                      Top = 13
                      Width = 75
                      Height = 13
                      Hint = ''
                      ShowHint = True
                      Caption = 'Doc. Proveedor'
                      TabOrder = 5
                    end
                  end
                end
                object UniDBCheckBox1: TUniDBCheckBox
                  Left = 376
                  Top = 152
                  Width = 186
                  Height = 17
                  Hint = ''
                  ShowHint = True
                  Caption = 'Desactivar ventas autom'#225'ticas'
                  TabOrder = 3
                end
              end
              object UniPanel42: TUniPanel
                Left = 0
                Top = 222
                Width = 1027
                Height = 176
                Hint = ''
                ShowHint = True
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 2
                Caption = ''
                ExplicitWidth = 1048
                object UniLabel174: TUniLabel
                  Left = 3
                  Top = 6
                  Width = 66
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Obervaciones'
                  TabOrder = 1
                end
                object UniDBMemo5: TUniDBMemo
                  Left = 1
                  Top = 21
                  Width = 410
                  Height = 89
                  Hint = ''
                  ShowHint = True
                  DataField = 'OBSERVACIONES'
                  ScrollBars = ssBoth
                  TabOrder = 2
                end
                object UniLabel175: TUniLabel
                  Left = 3
                  Top = 120
                  Width = 57
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Vencimiento'
                  TabOrder = 3
                end
                object UniDBDateTimePicker17: TUniDBDateTimePicker
                  Left = 3
                  Top = 139
                  Width = 100
                  Hint = ''
                  ShowHint = True
                  DataField = 'FECHACARGO'
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 4
                end
                object UniLabel176: TUniLabel
                  Left = 109
                  Top = 120
                  Width = 63
                  Height = 13
                  Hint = ''
                  ShowHint = True
                  Caption = 'Fecha Abono'
                  TabOrder = 5
                end
                object UniDBDateTimePicker18: TUniDBDateTimePicker
                  Left = 109
                  Top = 139
                  Width = 100
                  Hint = ''
                  ShowHint = True
                  DataField = 'FECHAABONO'
                  DateTime = 43374.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 6
                end
                object UniBitBtn51: TUniBitBtn
                  Left = 223
                  Top = 119
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 7
                end
                object UniBitBtn52: TUniBitBtn
                  Left = 299
                  Top = 119
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 8
                end
                object UniBitBtn53: TUniBitBtn
                  Left = 366
                  Top = 119
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 9
                end
              end
            end
            object UniTabSheet20: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'Devoluciones del mismo cliente'
              object UniLabel177: TUniLabel
                Left = 0
                Top = 0
                Width = 1027
                Height = 13
                Hint = ''
                ShowHint = True
                Alignment = taCenter
                AutoSize = False
                Caption = 'Albaranes de Devolucion del Distribuidor seleccionado'
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 0
              end
              object UniDBGrid10: TUniDBGrid
                Left = 0
                Top = 13
                Width = 1027
                Height = 209
                Hint = ''
                ShowHint = True
                LoadMask.Message = 'Loading data...'
                Align = alTop
                Anchors = [akLeft, akTop, akRight]
                TabOrder = 1
                Columns = <
                  item
                    FieldName = 'IDSTOCABE'
                    Title.Caption = 'N.Devol'
                    Width = 64
                  end
                  item
                    FieldName = 'FECHA'
                    Title.Caption = 'Fecha'
                    Width = 64
                  end
                  item
                    FieldName = 'NDOCSTOCK'
                    Title.Caption = 'N.Docto.'
                    Width = 64
                  end
                  item
                    FieldName = 'FECHACARGO'
                    Title.Caption = 'Fecha Cargo'
                    Width = 65
                  end
                  item
                    FieldName = 'FECHAABONO'
                    Title.Caption = 'Fecha Abono'
                    Width = 67
                  end
                  item
                    FieldName = 'SWTIPODOCU'
                    Title.Caption = 'SWTIPODOCU'
                    Width = 73
                  end
                  item
                    FieldName = 'NALMACEN'
                    Title.Caption = 'NALMACEN'
                    Width = 64
                  end
                  item
                    FieldName = 'DOCTOPROVE'
                    Title.Caption = 'DOCTOPROVE'
                    Width = 124
                  end
                  item
                    FieldName = 'DOCTOPROVEFECHA'
                    Title.Caption = 'DOCTOPROVEFECHA'
                    Width = 106
                  end
                  item
                    FieldName = 'ID_PROVEEDOR'
                    Title.Caption = 'Cod.Prove'
                    Width = 64
                  end
                  item
                    FieldName = 'NOMBRE'
                    Title.Caption = 'Nombre Prove'
                    Width = 244
                  end
                  item
                    FieldName = 'NIF'
                    Title.Caption = 'NIF'
                    Width = 94
                  end>
              end
              object UniPanel43: TUniPanel
                Left = 0
                Top = 222
                Width = 1027
                Height = 352
                Hint = ''
                ShowHint = True
                Align = alClient
                Anchors = [akLeft, akTop, akRight, akBottom]
                TabOrder = 2
                Caption = ''
                object UniLabel178: TUniLabel
                  Left = 3
                  Top = 24
                  Width = 267
                  Height = 67
                  Hint = ''
                  ShowHint = True
                  AutoSize = False
                  Caption = 
                    'Hay varias devoluciones en curso de esta distribuidora Si desea ' +
                    'puede [ampliar] la devolucion en que est'#233' situado la rejilla o p' +
                    'ulsar un [nuevo] documento de devoluci'#243'n'
                  TabOrder = 1
                end
                object UniBitBtn54: TUniBitBtn
                  Left = 276
                  Top = 27
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 2
                  ImageIndex = 13
                end
                object UniBitBtn55: TUniBitBtn
                  Left = 337
                  Top = 27
                  Width = 55
                  Height = 42
                  Hint = ''
                  ShowHint = True
                  Caption = ''
                  TabOrder = 3
                  ImageIndex = 13
                end
              end
            end
            object UniTabSheet21: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'TabSheet15'
              object UniLabel179: TUniLabel
                Left = 26
                Top = 3
                Width = 66
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'ID Devolucion'
                TabOrder = 0
              end
              object UniLabel180: TUniLabel
                Left = 30
                Top = 38
                Width = 62
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Cambiar Por:'
                TabOrder = 1
              end
              object UniDBEdit125: TUniDBEdit
                Left = 115
                Top = 29
                Width = 121
                Height = 22
                Hint = ''
                ShowHint = True
                TabOrder = 2
              end
              object UniLabel181: TUniLabel
                Left = 36
                Top = 63
                Width = 56
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Contrase'#241'a'
                TabOrder = 3
              end
              object UniDBEdit126: TUniDBEdit
                Left = 115
                Top = 62
                Width = 121
                Height = 22
                Hint = ''
                ShowHint = True
                TabOrder = 4
              end
              object UniDBText26: TUniDBText
                Left = 115
                Top = 8
                Width = 62
                Height = 13
                Hint = ''
                ShowHint = True
                DataField = 'IDSTOCABE'
              end
              object UniBitBtn56: TUniBitBtn
                Left = 115
                Top = 90
                Width = 75
                Height = 25
                Hint = ''
                ShowHint = True
                Caption = 'Cambiar'
                TabOrder = 6
                ImageIndex = 13
              end
            end
            object UniTabSheet22: TUniTabSheet
              Hint = ''
              ShowHint = True
              Caption = 'Cambiar Distribuidora'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 256
              ExplicitHeight = 128
              object UniBitBtn57: TUniBitBtn
                Left = 3
                Top = 6
                Width = 126
                Height = 45
                Hint = ''
                ShowHint = True
                Caption = 'Cambiar Distribuidora'
                TabOrder = 1
                ImageIndex = 13
              end
              object UniDBEdit127: TUniDBEdit
                Left = 178
                Top = 29
                Width = 288
                Height = 22
                Hint = ''
                ShowHint = True
                DataField = 'NOMBRE'
                TabOrder = 2
              end
              object UniDBEdit128: TUniDBEdit
                Left = 135
                Top = 29
                Width = 47
                Height = 22
                Hint = ''
                ShowHint = True
                DataField = 'ID_PROVEEDOR'
                TabOrder = 3
              end
              object UniLabel182: TUniLabel
                Left = 135
                Top = 10
                Width = 94
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'Nueva Distribuidora'
                TabOrder = 4
              end
              object UniBitBtn58: TUniBitBtn
                Left = 72
                Top = 58
                Width = 149
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Cambiar Distribuidora'
                TabOrder = 5
                ImageIndex = 13
              end
              object UniBitBtn59: TUniBitBtn
                Left = 227
                Top = 58
                Width = 149
                Height = 42
                Hint = ''
                ShowHint = True
                Caption = 'Cambiar Distribuidora'
                TabOrder = 6
                ImageIndex = 13
              end
              object UniDBLookupComboBox2: TUniDBLookupComboBox
                Left = 3
                Top = 10
                Width = 30
                Hint = ''
                ShowHint = True
                ListFieldIndex = 0
                TabOrder = 0
                Color = clWindow
              end
              object UniLabel183: TUniLabel
                Left = 179
                Top = 121
                Width = 87
                Height = 13
                Hint = ''
                ShowHint = True
                Caption = 'LabelProcesoDistri'
                TabOrder = 7
              end
            end
          end
        end
      end
    end
  end
end
