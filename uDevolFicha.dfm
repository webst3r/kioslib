object FormDevolFicha: TFormDevolFicha
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 693
  ClientWidth = 997
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  KeyPreview = True
  NavigateKeys.Enabled = True
  NavigateKeys.Next.Key = 13
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBtsLista: TUniPanel
    Left = 0
    Top = 0
    Width = 997
    Height = 50
    Hint = ''
    Visible = False
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    BorderStyle = ubsSingle
    Caption = ''
    Color = clBtnShadow
    object BtnDevolEnCurso: TUniBitBtn
      Left = 244
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888800000088888008888888888800000088880FF000000000080000008880
        F0080FFFFFFF08000000880F0FF00F00000F0800000080F0F0080FFFFFFF0800
        0000880F0FF00F00000F0800000080F0F0080FFFFFFF08000000880F0FB00F00
        F0000800000080F0FBFB0FFFF0F088000000880FBFBF0FFFF0088800000080FB
        FBFB00000088880000008800BFBFBFBF088888000000888800FBFBF088888800
        000088888800B808888888000000888888880088888888000000888888888888
        888888000000888888888888888888000000}
      Caption = ''
      TabOrder = 1
      ImageIndex = 13
    end
    object BtnDevolEnviadas: TUniBitBtn
      Left = 305
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 2
    end
    object spTodas: TUniBitBtn
      Left = 366
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77770000000000000007FFFFFFFFFFFFFFF00000F000F00F000F70910BB30E60
        330070910BB30E60330070910BB30E60330070910BB30E60330070910BB30E60
        330070910BB30E603300709100000E603300709100FF0E6000007000010F0E60
        0F00700F0100000000007700F00700FF00777770000770000077}
      Caption = ''
      TabOrder = 3
    end
    object BtnDocumentoCabe: TUniBitBtn
      Left = 427
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888000000080000000000000008000000080F8FFFFFFFFFFF0800000008089
        9FFF899998F08000000080F98FFFF8888FF08000000080FFFFFFFFFFFFF08000
        000080F7F447844447F08000000080F8F888F88888F08000000080F8F7788777
        78F08000000080F7F747847747F08000000080FFFFFFF8888FF0800000008080
        0007FFFFFFF08000000080F8FF8FFFFFFFF08000000080866666F88888808000
        000080E767EEEEEEEE708000000080E8E7EEEEEEEE8080000000800000000000
        000080000000}
      Caption = ''
      TabOrder = 4
    end
    object UniLabel85: TUniLabel
      Left = 504
      Top = 310
      Width = 54
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Documento'
      TabOrder = 5
    end
    object edDocumentoProve: TUniDBEdit
      Left = 496
      Top = 323
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      TabOrder = 6
    end
    object spVerDocumentoProveedor: TUniBitBtn
      Left = 657
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000F0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0087FFFFFFFFFFFF0B3087FFFFFFFFFFF0BB0087FF
        FFFFFFFF0BB3008FFFFFFFFFF0BBB008FFFFFFFFF00BBB007FFFFFFF00BBB007
        FFF0FFFFF00BBB007FF0FFFFFFF00BB007F0FFFFFFFFF00B0070FFFFFFFFFFF0
        00F0FFFFFFFFFFFFFFF0}
      Caption = ''
      TabOrder = 7
      ImageIndex = 13
    end
    object btEtiquetasDocumento: TUniBitBtn
      Left = 718
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        42050000424D4205000000000000360000002800000016000000130000000100
        1800000000000C05000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FFFFFF000000000000000000000000FFFFFFC0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C00000000000000000000000000000000000000000000000C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C0000000C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0008000008000C0C0
        C00000FF0000FFC0C0C0C0C0C00000000000000000FFFFFFC0C0C0C0C0C00000
        00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0000000FFFFFFFFFFFFFFFFFF0000000000000000FFFFFFC0C0C0C0
        C0C0808080000000000000808080808080808080808080808080808080808080
        8080808080808080808080808080800000000000008080800000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000FFFFFFFFFFFF00008080
        80000000000000808080808080808080000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFF
        0000FFFFFFFFFFFF000000000000000000000000000000000000FFFFFF000000
        000000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000
        0000FFFFFF000000000000000000000000000000000000000000000000FFFFFF
        000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000000000FFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFF000000FFFFFF0000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000
        000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000}
      Caption = ''
      TabOrder = 8
    end
    object btInicioConfiguracion: TUniBitBtn
      Left = 786
      Top = 318
      Width = 34
      Height = 27
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000013000000120000000100
        040000000000D800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        3333333000003243342222224433333000003224422222222243333000003222
        222AAAAA222433300000322222A33333A222433000003222223333333A224330
        00003222222333333A44433000003AAAAAAA3333333333300000333333333333
        3333333000003333333333334444443000003A444333333A2222243000003A22
        43333333A2222430000033A22433333442222430000033A22244444222222430
        0000333A2222222222AA243000003333AA222222AA33A3300000333333AAAAAA
        333333300000333333333333333333300000}
      Caption = ''
      TabOrder = 9
    end
    object BtnPreProve: TUniBitBtn
      Left = 838
      Top = 310
      Width = 50
      Height = 35
      Hint = ''
      ShowHint = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000007777777777777777770000007777777777770007770000007444
        4400000006007700000074FFFF08880600080700000074F008000060EE070700
        000074FFFFF8060EE0047700000074F0088060EE00F47700000074FFFF060EE0
        00747700000074F00800EE0EE0047700000074FFFF0EE0F0EE047700000074F0
        080000F000047700000074FFFFFFFFFFFFF47700000074444444444444447700
        000074F444F444F444F477000000744444444444444477000000777777777777
        777777000000777777777777777777000000}
      Caption = ''
      TabOrder = 10
    end
    object UniDateTimePicker1: TUniDateTimePicker
      Left = 103
      Top = 310
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 11
    end
    object UniDateTimePicker2: TUniDateTimePicker
      Left = 103
      Top = 331
      Width = 120
      Hint = ''
      ShowHint = True
      DateTime = 43382.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 12
    end
    object UniLabel86: TUniLabel
      Left = 103
      Top = 296
      Width = 43
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'En Curso'
      TabOrder = 13
    end
    object UniLabel87: TUniLabel
      Left = 68
      Top = 310
      Width = 30
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde'
      TabOrder = 14
    end
    object UniLabel88: TUniLabel
      Left = 68
      Top = 331
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta'
      TabOrder = 15
    end
    object UniPanel24: TUniPanel
      Left = 56
      Top = 2
      Width = 522
      Height = 45
      Hint = ''
      ShowHint = True
      ParentShowHint = False
      TabOrder = 16
      BorderStyle = ubsNone
      ShowCaption = False
      Caption = 'Programa'
      Color = clBtnShadow
      object UniBitBtn24: TUniBitBtn
        Left = 6
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Nuevo'
        ShowHint = True
        Caption = ''
        TabOrder = 1
        IconAlign = iaTop
        ImageIndex = 0
      end
      object UniBitBtn29: TUniBitBtn
        Left = 69
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Anular'
        ShowHint = True
        Caption = ''
        TabOrder = 2
        IconAlign = iaTop
        ImageIndex = 7
      end
      object btModificar: TUniBitBtn
        Left = 132
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Modificar'
        ShowHint = True
        Caption = ''
        TabOrder = 3
        IconAlign = iaTop
        ImageIndex = 4
      end
      object UniBitBtn31: TUniBitBtn
        Left = 194
        Top = 5
        Width = 50
        Height = 35
        Hint = 'Cerrar'
        ShowHint = True
        Caption = ''
        TabOrder = 4
        IconAlign = iaTop
        ImageIndex = 9
      end
    end
    object UniPanel26: TUniPanel
      Left = 588
      Top = 2
      Width = 357
      Height = 45
      Hint = ''
      ShowHint = True
      ParentShowHint = False
      TabOrder = 17
      BorderStyle = ubsNone
      ShowCaption = False
      Caption = 'Opcional'
      Color = clBtnShadow
      object UniPanel27: TUniPanel
        Left = 3
        Top = 0
        Width = 231
        Height = 45
        Hint = ''
        Visible = False
        ShowHint = True
        TabOrder = 1
        BorderStyle = ubsNone
        ShowCaption = False
        Caption = 'Ficha'
        Color = clActiveBorder
        object UniBitBtn17: TUniBitBtn
          Left = 3
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Consulta Devoluciones'
          ShowHint = True
          Caption = ''
          TabOrder = 1
          IconAlign = iaTop
          ImageIndex = 12
        end
        object UniBitBtn22: TUniBitBtn
          Left = 65
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Nueva Devolucion'
          ShowHint = True
          Caption = ''
          TabOrder = 2
          IconAlign = iaTop
          ImageIndex = 2
        end
        object UniBitBtn23: TUniBitBtn
          Left = 121
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Buscar'
          ShowHint = True
          Caption = ''
          TabOrder = 3
          IconAlign = iaTop
          ImageIndex = 1
        end
        object UniBitBtn32: TUniBitBtn
          Left = 177
          Top = 5
          Width = 50
          Height = 35
          Hint = 'Imprimir'
          ShowHint = True
          Caption = ''
          TabOrder = 4
          IconAlign = iaTop
          ImageIndex = 8
        end
      end
      object btOpciones: TUniBitBtn
        Left = 322
        Top = 5
        Width = 35
        Height = 35
        Cursor = crHandPoint
        Hint = ''
        ShowHint = True
        Caption = ''
        TabOrder = 2
        Transparency = toFuchsia
        ImageIndex = 11
        OnClick = btOpcionesClick
      end
    end
    object btLista: TUniBitBtn
      AlignWithMargins = True
      Left = 3
      Top = 2
      Width = 50
      Height = 45
      Hint = ''
      Margins.Bottom = 0
      ShowHint = True
      Caption = 'Lista'
      TabOrder = 18
      IconAlign = iaTop
      ImageIndex = 4
      OnClick = btListaClick
    end
  end
  object UniPageControl3: TUniPageControl
    Left = 0
    Top = 142
    Width = 997
    Height = 551
    Hint = ''
    ShowHint = True
    ActivePage = TabSheet6
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    ClientEvents.ExtEvents.Strings = (
      
        'afterrender=function window.afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext' +
        '.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id)' +
        '.el.setStyle("border-width", 0);'#13#10'}')
    TabOrder = 1
    object TabSheet6: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'Entrada'
      object pnlCBarras: TUniPanel
        Left = 0
        Top = 0
        Width = 989
        Height = 146
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        BorderStyle = ubsNone
        Caption = ''
        object UniLabel9: TUniLabel
          Left = 73
          Top = 62
          Width = 42
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'C.Barras'
          TabOrder = 17
        end
        object pBarrasA: TUniEdit
          Left = 117
          Top = 53
          Width = 232
          Height = 31
          Hint = ''
          ShowHint = True
          MaxLength = 18
          Text = ''
          ParentFont = False
          Font.Height = -16
          TabOrder = 0
          OnExit = pBarrasAExit
          OnEnter = pBarrasAEnter
        end
        object pAdendumEntrar: TUniDBEdit
          Left = 742
          Top = 53
          Width = 69
          Height = 31
          Hint = ''
          ShowHint = True
          DataField = 'ADENDUM'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -16
          TabOrder = 4
          TabStop = False
          OnExit = pAdendumEntrarExit
          OnEnter = pAdendumEntrarEnter
        end
        object UniLabel10: TUniLabel
          Left = 750
          Top = 36
          Width = 41
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ejemplar'
          TabOrder = 18
        end
        object pCantidad: TUniDBEdit
          Left = 814
          Top = 53
          Width = 49
          Height = 31
          Hint = ''
          ShowHint = True
          DataField = 'DEVUELTOS'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -16
          TabOrder = 1
          InputMask.UnmaskText = True
          InputMask.RemoveWhiteSpace = True
          OnChange = pCantidadChange
          OnExit = pCantidadExit
          OnEnter = pCantidadEnter
        end
        object UniLabel11: TUniLabel
          Left = 815
          Top = 36
          Width = 43
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cantidad'
          TabOrder = 19
        end
        object BtnRestarCanti: TUniBitBtn
          Left = 931
          Top = 45
          Width = 55
          Height = 39
          Hint = 'Restar Cantidad'
          ShowHint = True
          Caption = '-'
          ParentFont = False
          Font.Height = -32
          TabStop = False
          TabOrder = 21
          OnClick = BtnRestarCantiClick
        end
        object pGraDescripcion: TUniDBEdit
          Left = 352
          Top = 53
          Width = 387
          Height = 31
          Hint = ''
          ShowHint = True
          DataField = 'DESCRIPCION'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 3
          TabStop = False
        end
        object UniLabel59: TUniLabel
          Left = 457
          Top = 36
          Width = 109
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Descripcion del articulo'
          TabOrder = 22
        end
        object UniLabel39: TUniLabel
          Left = 123
          Top = 87
          Width = 45
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total PVP'
          TabOrder = 23
        end
        object UniLabel40: TUniLabel
          Left = 189
          Top = 87
          Width = 30
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'P.V.P.'
          TabOrder = 24
        end
        object pPrecio: TUniDBEdit
          Left = 117
          Top = 102
          Width = 57
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PrecioTotal'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 5
          TabStop = False
          ReadOnly = True
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
        end
        object pPreu: TUniDBEdit
          Left = 176
          Top = 102
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOVENTA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 6
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreuExit
          OnEnter = pPreuEnter
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit37: TUniDBEdit
          Left = 117
          Top = 122
          Width = 57
          Height = 20
          Hint = ''
          Visible = False
          ShowHint = True
          DataField = 'PRECIOCOMPRA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 15
          TabStop = False
        end
        object pPreu2: TUniDBEdit
          Left = 176
          Top = 122
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'PRECIOVENTA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 7
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreu2Exit
          OnEnter = pPreu2Enter
          OnKeyPress = pPreuKeyPress
        end
        object btMasInfo: TUniBitBtn
          Left = -1
          Top = 6
          Width = 55
          Height = 41
          Hint = 'Mas Opciones Precios'
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 25
          Images = DMppal.ImgListPrincipal
          ImageIndex = 11
          OnClick = btMasInfoClick
        end
        object UniLabel32: TUniLabel
          Left = 247
          Top = 105
          Width = 37
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 26
        end
        object UniLabel33: TUniLabel
          Left = 299
          Top = 105
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'A Stock'
          TabOrder = 27
        end
        object UniLabel34: TUniLabel
          Left = 401
          Top = 105
          Width = 32
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Merma'
          TabOrder = 28
        end
        object UniLabel35: TUniLabel
          Left = 451
          Top = 105
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Venta'
          TabOrder = 29
        end
        object UniLabel36: TUniLabel
          Left = 511
          Top = 105
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Devolucion'
          TabOrder = 30
        end
        object UniLabel37: TUniLabel
          Left = 620
          Top = 105
          Width = 31
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Abono'
          TabOrder = 31
        end
        object UniDBEdit30: TUniDBEdit
          Left = 242
          Top = 122
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'COMPRAHISTO'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 8
          TabStop = False
        end
        object paStock: TUniDBEdit
          Left = 293
          Top = 122
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'CANTIENALBA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 9
          TabStop = False
        end
        object pMerma: TUniDBEdit
          Left = 395
          Top = 122
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'MERMA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 11
          TabStop = False
        end
        object UniDBEdit33: TUniDBEdit
          Left = 446
          Top = 122
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'VENDIDOS'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 12
          TabStop = False
        end
        object UniDBEdit34: TUniDBEdit
          Left = 344
          Top = 122
          Width = 49
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'RECLAMADO'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 10
          TabStop = False
        end
        object UniDBDateTimePicker6: TUniDBDateTimePicker
          Left = 497
          Top = 122
          Width = 98
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'FECHADEVOL'
          DataSource = dsLinea
          DateTime = 43375.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 13
          TabStop = False
          ParentFont = False
          Font.Height = -13
        end
        object pFeAbono: TUniDBDateTimePicker
          Left = 597
          Top = 122
          Width = 98
          Height = 20
          Hint = ''
          ShowHint = True
          DataField = 'FECHAABONO'
          DataSource = dsLinea
          DateTime = 43375.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 14
          TabStop = False
          ParentFont = False
          Font.Height = -13
        end
        object UniLabel2: TUniLabel
          Left = 350
          Top = 105
          Width = 30
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Recla.'
          TabOrder = 32
        end
        object btArriba: TUniBitBtn
          Left = 873
          Top = 103
          Width = 55
          Height = 41
          Hint = ''
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 33
          IconAlign = iaCenter
          Images = DMppal.ImgListPrincipal
          ImageIndex = 29
          OnClick = btArribaClick
        end
        object btAnularLinea: TUniButton
          Left = -1
          Top = 102
          Width = 55
          Height = 41
          Hint = 'Anular Linea'
          ShowHint = True
          Caption = 'Anular'
          TabStop = False
          TabOrder = 34
          Images = DMppal.ImgListPrincipal
          ImageIndex = 35
          IconAlign = iaTop
          OnClick = btAnularLineaClick
        end
        object UniEdit1: TUniDBEdit
          Left = 117
          Top = 8
          Width = 338
          Height = 25
          Hint = ''
          ShowHint = True
          DataField = 'NOMBRE'
          DataSource = dsCabe1
          MaxLength = 999999
          ParentFont = False
          Font.Height = -13
          TabOrder = 35
          TabStop = False
          ReadOnly = True
        end
        object UniLabel4: TUniLabel
          Left = 55
          Top = 13
          Width = 60
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Distribuidora'
          TabOrder = 36
        end
        object btAbajoTodo: TUniBitBtn
          Left = 761
          Top = 103
          Width = 55
          Height = 41
          Hint = ''
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 37
          IconAlign = iaTop
          Images = DMppal.ImgListPrincipal
          ImageIndex = 28
          OnClick = btAbajoTodoClick
        end
        object btAbajo: TUniBitBtn
          Left = 817
          Top = 103
          Width = 55
          Height = 41
          Hint = ''
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 38
          IconAlign = iaCenter
          Images = DMppal.ImgListPrincipal
          ImageIndex = 30
          OnClick = btAbajoClick
        end
        object btArribaTodo: TUniBitBtn
          Left = 929
          Top = 103
          Width = 55
          Height = 41
          Hint = ''
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 39
          IconAlign = iaTop
          Images = DMppal.ImgListPrincipal
          ImageIndex = 27
          OnClick = btArribaTodoClick
        end
        object edMensajes: TUniEdit
          Left = 460
          Top = 8
          Width = 526
          Height = 25
          Hint = ''
          ShowHint = True
          Alignment = taCenter
          Text = ''
          ParentFont = False
          Font.Height = -13
          Font.Style = [fsBold]
          TabOrder = 40
          TabStop = False
          ReadOnly = True
        end
        object UniEdit2: TUniEdit
          Left = 877
          Top = 56
          Width = 21
          Hint = ''
          ShowHint = True
          Text = 'UniEdit2'
          TabOrder = 2
          OnEnter = UniEdit2Enter
        end
        object BtnSumarCanti: TUniBitBtn
          Left = 871
          Top = 45
          Width = 55
          Height = 39
          Hint = 'Sumar Cantidad'
          ShowHint = True
          Caption = '+'
          ParentFont = False
          Font.Height = -24
          TabStop = False
          TabOrder = 20
          OnClick = BtnSumarCantiClick
        end
        object btBuscarArti: TUniBitBtn
          Left = -1
          Top = 54
          Width = 55
          Height = 41
          Hint = 'Buscar Articulo'
          ShowHint = True
          Caption = ''
          TabStop = False
          TabOrder = 41
          Images = DMppal.ImgListPrincipal
          ImageIndex = 12
          OnClick = btBuscarArtiClick
        end
        object UniDBText1: TUniDBText
          Left = 118
          Top = 36
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'BARRAS'
          DataSource = dsLinea
        end
      end
      object pnlTotales: TUniPanel
        Left = 0
        Top = 146
        Width = 989
        Height = 71
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        BorderStyle = ubsNone
        Caption = ''
        object pMargen1: TUniDBEdit
          Left = 242
          Top = 19
          Width = 57
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'MARGEN'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 0
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreuExit
          OnEnter = pMargen1Enter
          OnKeyPress = pPreuKeyPress
        end
        object pMargen2: TUniDBEdit
          Left = 242
          Top = 42
          Width = 57
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'MARGEN2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 1
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreuExit
          OnEnter = pMargen2Enter
          OnKeyPress = pPreuKeyPress
        end
        object UniLabel41: TUniLabel
          Left = 249
          Top = 3
          Width = 36
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Margen'
          TabOrder = 13
        end
        object pTIva1: TUniDBEdit
          Left = 301
          Top = 19
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TIVA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 2
          TabStop = False
          OnExit = pTIva1Exit
          OnEnter = pTIva1Enter
        end
        object pTIva2: TUniDBEdit
          Left = 301
          Top = 42
          Width = 27
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TIVA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 3
          TabStop = False
          OnExit = pTIva2Exit
          OnEnter = pTIva2Enter
        end
        object UniDBEdit47: TUniDBEdit
          Left = 330
          Top = 19
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TPCIVA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 4
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit48: TUniDBEdit
          Left = 376
          Top = 19
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TPCRE'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 6
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit49: TUniDBEdit
          Left = 330
          Top = 42
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TPCIVA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 5
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit50: TUniDBEdit
          Left = 376
          Top = 42
          Width = 44
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'TPCRE2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 7
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniLabel43: TUniLabel
          Left = 304
          Top = 3
          Width = 20
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Tipo'
          TabOrder = 14
        end
        object UniLabel44: TUniLabel
          Left = 334
          Top = 3
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '%IVA'
          TabOrder = 15
        end
        object UniLabel45: TUniLabel
          Left = 381
          Top = 3
          Width = 27
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = '% RE'
          TabOrder = 16
        end
        object UniDBEdit51: TUniDBEdit
          Left = 422
          Top = 19
          Width = 56
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'PRECIOCOSTE'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 8
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreuExit
          OnEnter = UniDBEdit51Enter
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit52: TUniDBEdit
          Left = 480
          Top = 19
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'PRECIOCOSTETOT'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 10
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit53: TUniDBEdit
          Left = 422
          Top = 42
          Width = 56
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'PRECIOCOSTE2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 9
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit54: TUniDBEdit
          Left = 480
          Top = 42
          Width = 60
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'PRECIOCOSTETOT2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 11
          TabStop = False
          OnExit = pPreuExit
          OnKeyPress = pPreuKeyPress
        end
        object UniLabel46: TUniLabel
          Left = 428
          Top = 3
          Width = 42
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Coste'
          TabOrder = 17
        end
        object UniLabel47: TUniLabel
          Left = 484
          Top = 3
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Pr.Cte+RE'
          TabOrder = 18
        end
        object UniDBEdit55: TUniDBEdit
          Left = 546
          Top = 19
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOMPRA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 19
          TabStop = False
        end
        object UniDBEdit56: TUniDBEdit
          Left = 613
          Top = 19
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOSTE'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 20
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit57: TUniDBEdit
          Left = 546
          Top = 42
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOMPRA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 21
          TabStop = False
        end
        object UniDBEdit58: TUniDBEdit
          Left = 613
          Top = 42
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOSTE2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 22
          TabStop = False
          OnKeyPress = pPreuKeyPress
        end
        object UniLabel48: TUniLabel
          Left = 622
          Top = 3
          Width = 38
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'V.Coste'
          TabOrder = 23
        end
        object UniLabel49: TUniLabel
          Left = 555
          Top = 3
          Width = 47
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'V.Compra'
          TabOrder = 24
        end
        object UniDBEdit59: TUniDBEdit
          Left = 680
          Top = 19
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOSTETOT'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 25
          TabStop = False
          ClientEvents.ExtEvents.Strings = (
            
              'keypress=function keypress(sender, e, eOpts)'#13#10'{'#13#10'  if (e.keyCode' +
              ' == '#39'46'#39' || e.charCode == '#39'46'#65279#39') {'#13#10'      '#13#10'      var val = send' +
              'er.getValue() ; '#13#10'      sender.setValue(val + '#39','#39') ;'#13#10'      e.st' +
              'opEvent() ;'#13#10'   } '#13#10'}')
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit60: TUniDBEdit
          Left = 747
          Top = 19
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORVENTA'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 26
          TabStop = False
        end
        object UniDBEdit61: TUniDBEdit
          Left = 680
          Top = 42
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORCOSTETOT2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 27
          TabStop = False
          OnKeyPress = pPreuKeyPress
        end
        object UniDBEdit62: TUniDBEdit
          Left = 747
          Top = 42
          Width = 65
          Height = 20
          Hint = ''
          ShowHint = True
          ParentShowHint = False
          DataField = 'VALORVENTA2'
          DataSource = dsLinea
          ParentFont = False
          Font.Height = -13
          TabOrder = 28
          TabStop = False
        end
        object UniLabel50: TUniLabel
          Left = 682
          Top = 3
          Width = 65
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'V.Coste + RE'
          TabOrder = 29
        end
        object UniLabel51: TUniLabel
          Left = 762
          Top = 3
          Width = 30
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'V.Vta.'
          TabOrder = 30
        end
        object UniLabel52: TUniLabel
          Left = 831
          Top = 15
          Width = 28
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Venta'
          TabOrder = 31
        end
        object UniLabel53: TUniLabel
          Left = 831
          Top = 28
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Devolucion'
          TabOrder = 32
        end
        object UniLabel54: TUniLabel
          Left = 831
          Top = 41
          Width = 32
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Merma'
          TabOrder = 33
        end
        object UniDBText5: TUniDBText
          Left = 865
          Top = 15
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'VENTAHISTO'
          DataSource = dsLinea
        end
        object UniDBText6: TUniDBText
          Left = 888
          Top = 28
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'DEVOLHISTO'
          DataSource = dsLinea
        end
        object UniDBText7: TUniDBText
          Left = 865
          Top = 41
          Width = 56
          Height = 13
          Hint = ''
          ShowHint = True
          DataField = 'MERMAHISTO'
          DataSource = dsLinea
        end
        object UniLabel55: TUniLabel
          Left = 1009
          Top = 14
          Width = 25
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Alba.'
          TabOrder = 37
        end
        object UniLabel56: TUniLabel
          Left = 1095
          Top = 14
          Width = 24
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Total'
          TabOrder = 38
        end
        object UniLabel57: TUniLabel
          Left = 1029
          Top = 37
          Width = 37
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Compra'
          TabOrder = 39
        end
        object UniDBText8: TUniDBText
          Left = 1037
          Top = 14
          Width = 56
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          DataField = 'CANTIENALBA'
          DataSource = dsLinea
        end
        object UniDBText9: TUniDBText
          Left = 1072
          Top = 33
          Width = 56
          Height = 13
          Hint = ''
          Visible = False
          ShowHint = True
          DataField = 'COMPRAHISTO'
          DataSource = dsLinea
        end
      end
      object GridPreSeleccio: TUniDBGrid
        Left = 0
        Top = 217
        Width = 989
        Height = 280
        Hint = ''
        ShowHint = True
        ParentShowHint = False
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'   config.cls' +
            '="mGridCliente";'#13#10'  config.itemHeight = 30;'#13#10'  config.headerCont' +
            'ainer = {height: 30};'#13#10'}')
        DataSource = dsLinea
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColumnMove, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow]
        ReadOnly = True
        WebOptions.Paged = False
        WebOptions.FetchAll = True
        Grouping.Enabled = True
        LoadMask.Message = 'Loading data...'
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        Columns = <
          item
            FieldName = 'DEVUELTOS'
            Title.Alignment = taCenter
            Title.Caption = 'Devuelto'
            Width = 64
          end
          item
            FieldName = 'DESCRIPCION'
            Title.Alignment = taCenter
            Title.Caption = 'Descripcion'
            Width = 244
            ReadOnly = True
          end
          item
            FieldName = 'ADENDUM'
            Title.Alignment = taCenter
            Title.Caption = 'Num'
            Width = 57
          end
          item
            FieldName = 'PrecioTotal'
            Title.Alignment = taCenter
            Title.Caption = 'PVP'
            Width = 44
          end
          item
            FieldName = 'REFEPROVE'
            Title.Alignment = taCenter
            Title.Caption = 'Codi.Dist.'
            Width = 70
          end
          item
            FieldName = 'PAQUETE'
            Title.Alignment = taCenter
            Title.Caption = 'Paquete'
            Width = 62
          end
          item
            FieldName = 'CANTIENALBA'
            Title.Alignment = taCenter
            Title.Caption = 'A Stock'
            Width = 60
          end
          item
            FieldName = 'MERMA'
            Title.Alignment = taCenter
            Title.Caption = 'Merma'
            Width = 66
          end
          item
            FieldName = 'COMPRAHISTO'
            Title.Alignment = taCenter
            Title.Caption = 'His.Compra'
            Width = 87
            ReadOnly = True
          end
          item
            FieldName = 'MERMAHISTO'
            Title.Alignment = taCenter
            Title.Caption = 'His.Merma'
            Width = 79
            ReadOnly = True
          end
          item
            FieldName = 'DEVOLHISTO'
            Title.Alignment = taCenter
            Title.Caption = 'His.Devol.'
            Width = 74
            ReadOnly = True
          end
          item
            FieldName = 'VENTAHISTO'
            Title.Alignment = taCenter
            Title.Caption = 'His.Venta'
            Width = 64
            ReadOnly = True
          end
          item
            FieldName = 'VENDIDOS'
            Title.Alignment = taCenter
            Title.Caption = 'Vendidos'
            Width = 67
          end
          item
            FieldName = 'ID_ARTICULO'
            Title.Alignment = taCenter
            Title.Caption = 'Id.Arti'
            Width = 56
          end
          item
            FieldName = 'RECLAMADO'
            Title.Alignment = taCenter
            Title.Caption = 'Reclamado'
            Width = 86
          end
          item
            FieldName = 'FECHADEVOL'
            Title.Alignment = taCenter
            Title.Caption = 'Fecha Devol'
            Width = 79
          end
          item
            FieldName = 'FECHAAVISO'
            Title.Alignment = taCenter
            Title.Caption = 'Fecha Aviso'
            Width = 74
          end
          item
            FieldName = 'SWDEVOLUCION'
            Title.Alignment = taCenter
            Title.Caption = 'Sit. Devol'
            Width = 64
          end
          item
            FieldName = 'FECHA'
            Title.Alignment = taCenter
            Title.Caption = 'Fecha'
            Width = 74
          end
          item
            FieldName = 'NOMBRE'
            Title.Alignment = taCenter
            Title.Caption = 'Distribuidora'
            Width = 191
            ReadOnly = True
          end
          item
            FieldName = 'BARRAS'
            Title.Alignment = taCenter
            Title.Caption = 'C. Barras'
            Width = 109
            ReadOnly = True
          end
          item
            FieldName = 'CANTIDAD'
            Title.Alignment = taCenter
            Title.Caption = 'Cantidad'
            Width = 74
          end
          item
            FieldName = 'FECHACOMPRA'
            Title.Alignment = taCenter
            Title.Caption = 'Fecha Compra'
            Width = 89
            ReadOnly = True
          end>
      end
      object UniPanel2: TUniPanel
        Left = 0
        Top = 497
        Width = 989
        Height = 36
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 3
        Caption = ''
        object UniLabel3: TUniLabel
          Left = 5
          Top = 9
          Width = 31
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Total: '
          TabOrder = 1
        end
        object UniDBEdit1: TUniDBEdit
          Left = 38
          Top = 5
          Width = 34
          Height = 22
          Hint = ''
          ShowHint = True
          DataField = 'TOTAL'
          DataSource = dsLineaTotal
          TabOrder = 2
          ReadOnly = True
        end
        object lbPaquetes: TUniLabel
          Left = 93
          Top = 8
          Width = 83
          Height = 16
          Hint = ''
          Visible = False
          ShowHint = True
          Caption = 'Distribuidora'
          ParentFont = False
          Font.Color = clRed
          Font.Height = -13
          Font.Style = [fsBold]
          TabOrder = 3
        end
      end
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 50
    Width = 997
    Height = 92
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    ClientEvents.ExtEvents.Strings = (
      
        'afterrender=function window.afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext' +
        '.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id)' +
        '.el.setStyle("border-width", 0);'#13#10'}')
    BorderStyle = ubsNone
    Caption = ''
    object UniPanel3: TUniPanel
      Left = 0
      Top = 0
      Width = 36
      Height = 92
      Hint = ''
      Visible = False
      ShowHint = True
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 1
      Caption = ''
      object UniBitBtn1: TUniBitBtn
        Left = 0
        Top = -1
        Width = 37
        Height = 29
        Hint = ''
        ShowHint = True
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
      end
      object UniBitBtn2: TUniBitBtn
        Tag = 99
        Left = 0
        Top = 30
        Width = 37
        Height = 29
        Hint = ''
        ShowHint = True
        Caption = ''
        TabOrder = 2
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
      end
    end
    object UniPanel4: TUniPanel
      Left = 36
      Top = 0
      Width = 472
      Height = 92
      Hint = ''
      ShowHint = True
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 2
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function window.afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext' +
          '.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id)' +
          '.el.setStyle("border-width", 0);'#13#10'}')
      BorderStyle = ubsNone
      Caption = 'UniPanel4'
      object gridCabe1: TUniDBGrid
        Left = 0
        Top = 0
        Width = 472
        Height = 92
        Hint = ''
        ShowHint = True
        ClientEvents.ExtEvents.Strings = (
          
            'afterrender=function window.afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext' +
            '.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id)' +
            '.el.setStyle("border-width", 0);'#13#10'}')
        DataSource = dsCabe1
        ReadOnly = True
        WebOptions.Paged = False
        WebOptions.FetchAll = True
        LoadMask.Message = 'Loading data...'
        BorderStyle = ubsNone
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
        TabStop = False
        Columns = <
          item
            FieldName = 'NOMBRE'
            Title.Caption = 'Distribuidora'
            Width = 244
          end
          item
            FieldName = 'DOCTOPROVE'
            Title.Caption = 'Documento'
            Width = 124
          end
          item
            FieldName = 'PAQUETE'
            Title.Caption = 'Paquete'
            Width = 64
          end
          item
            FieldName = 'MAXPAQUETE'
            Title.Caption = 'Max. Paq.'
            Width = 112
          end
          item
            FieldName = 'TOTALDEVOL'
            Title.Caption = 'Devueltos'
            Width = 64
            ReadOnly = True
          end>
      end
    end
    object pnlNavigator: TUniPanel
      Left = 508
      Top = 0
      Width = 67
      Height = 92
      Hint = ''
      ShowHint = True
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 3
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function window.afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext' +
          '.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id)' +
          '.el.setStyle("border-width", 0);'#13#10'}')
      BorderStyle = ubsNone
      Caption = ''
      object btSumarPaquete: TUniBitBtn
        Left = 6
        Top = 3
        Width = 55
        Height = 41
        Hint = 'Ver Siguiente Paquete'
        ShowHint = True
        Caption = ''
        TabStop = False
        TabOrder = 1
        IconAlign = iaTop
        Images = DMppal.ImgListPrincipal
        ImageIndex = 34
        OnClick = btSumarPaqueteClick
      end
      object btRestarPaquete: TUniBitBtn
        Tag = 99
        Left = 6
        Top = 51
        Width = 55
        Height = 41
        Hint = 'Ver Paquete Anterior'
        ShowHint = True
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = DMppal.ImgListPrincipal
        ImageIndex = 35
        OnClick = btRestarPaqueteClick
      end
    end
    object UniPanel5: TUniPanel
      Left = 575
      Top = 0
      Width = 256
      Height = 92
      Hint = ''
      ShowHint = True
      Align = alLeft
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 4
      BorderStyle = ubsNone
      Caption = ''
      object btHistoArti: TUniBitBtn
        Tag = 99
        Left = 0
        Top = 51
        Width = 55
        Height = 41
        Hint = 'Historico'
        ShowHint = True
        Caption = ''
        TabStop = False
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 33
        OnClick = btHistoArtiClick
      end
      object btVerFicha: TUniBitBtn
        Tag = 99
        Left = 61
        Top = 51
        Width = 55
        Height = 41
        Hint = 'Ver Ficha'
        ShowHint = True
        Caption = ''
        TabStop = False
        TabOrder = 2
        Images = DMppal.ImgListPrincipal
        ImageIndex = 13
        OnClick = btVerFichaClick
      end
      object btVerUltiCompra: TUniBitBtn
        Tag = 99
        Left = 122
        Top = 51
        Width = 55
        Height = 41
        Hint = 'Ver Ultimas Compras'
        ShowHint = True
        Caption = ''
        TabStop = False
        TabOrder = 3
        Images = DMppal.ImgListPrincipal
        ImageIndex = 40
        OnClick = btVerUltiCompraClick
      end
      object btPaquetes: TUniBitBtn
        Left = 0
        Top = 3
        Width = 55
        Height = 41
        Hint = 'Ver todos los Paquetes'
        ShowHint = True
        Caption = ''
        TabOrder = 4
        Images = DMppal.ImgListPrincipal
        ImageIndex = 2
        OnClick = btPaquetesClick
      end
      object btModificarDesc: TUniButton
        Left = 61
        Top = 3
        Width = 55
        Height = 41
        Hint = 'Modificar Descripcion i Cod.Distri'
        ShowHint = True
        Caption = ''
        TabOrder = 5
        Images = DMppal.ImgListPrincipal
        ImageIndex = 3
        OnClick = btModificarDescClick
      end
    end
    object pEncarte2: TUniDBEdit
      Left = 869
      Top = 63
      Width = 60
      Height = 20
      Hint = ''
      Visible = False
      ShowHint = True
      ParentShowHint = False
      DataField = 'ENCARTE2'
      DataSource = dsLinea
      ParentFont = False
      Font.Height = -13
      TabOrder = 5
      TabStop = False
      OnExit = pPreuExit
    end
    object pEncarte1: TUniDBEdit
      Left = 869
      Top = 40
      Width = 60
      Height = 20
      Hint = ''
      Visible = False
      ShowHint = True
      ParentShowHint = False
      DataField = 'ENCARTE'
      DataSource = dsLinea
      ParentFont = False
      Font.Height = -13
      TabOrder = 6
      TabStop = False
      OnExit = pPreuExit
    end
    object UniLabel42: TUniLabel
      Left = 873
      Top = 24
      Width = 37
      Height = 13
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Encarte'
      TabOrder = 7
    end
  end
  object dsLinea: TDataSource
    DataSet = DMDevolucion.sqlLinea
    Left = 196
    Top = 603
  end
  object dsCabe1: TDataSource
    DataSet = DMDevolucion.sqlCabe1
    Left = 156
    Top = 603
  end
  object dsHistoAnteF: TDataSource
    DataSet = DMDevolucion.sqlHistoAnteF
    Left = 356
    Top = 603
  end
  object dsHistoAnteFT: TDataSource
    DataSet = DMDevolucion.sqlHistoAnteFT
    Left = 556
    Top = 603
  end
  object dsHistoDevolver: TDataSource
    DataSet = DMDevolucion.sqlHistoDevolver
    Left = 620
    Top = 603
  end
  object dsHistoDevolverProve: TDataSource
    DataSet = DMDevolucion.sqlHistoDevolverProve
    Left = 412
    Top = 603
  end
  object dsCabeSCap: TDataSource
    DataSet = DMDevolucion.sqlCabeSCap
    Left = 244
    Top = 603
  end
  object dsProveS: TDataSource
    DataSet = DMDevolucion.sqlProveS
    Left = 300
    Top = 603
  end
  object dsCabeNuevo: TDataSource
    DataSet = DMDevolucion.sqlCabeNuevo
    Left = 108
    Top = 603
  end
  object dsLineaTotal: TDataSource
    DataSet = DMDevolucion.sqlLineaTotal
    Left = 492
    Top = 603
  end
end
