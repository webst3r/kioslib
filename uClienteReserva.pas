unit uClienteReserva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniDBCheckBox;

type
  TFormClienteReserva = class(TUniForm)
    tmSession: TUniTimer;
    pcDetalle: TUniPageControl;
    tabTablas: TUniTabSheet;
    GridAReservar: TUniDBGrid;
    tabNuevo: TUniTabSheet;
    UniPanel1: TUniPanel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniLabel6: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniLabel7: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    UniLabel8: TUniLabel;
    UniDBEdit5: TUniDBEdit;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniDBEdit8: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    UniDBRadioGroup2: TUniDBRadioGroup;
    chLunes: TUniDBCheckBox;
    chMartes: TUniDBCheckBox;
    chMiercoles: TUniDBCheckBox;
    chJueves: TUniDBCheckBox;
    chViernes: TUniDBCheckBox;
    chSabado: TUniDBCheckBox;
    chDomingo: TUniDBCheckBox;
    UniGroupBox1: TUniGroupBox;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniDBDateTimePicker4: TUniDBDateTimePicker;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniGroupBox2: TUniGroupBox;
    UniDBDateTimePicker5: TUniDBDateTimePicker;
    UniDBDateTimePicker6: TUniDBDateTimePicker;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    UniDBRadioGroup3: TUniDBRadioGroup;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniLabel16: TUniLabel;
    UniDBEdit6: TUniDBEdit;
    dsCLIEARTIS: TDataSource;
    ImgNativeList: TUniNativeImageList;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniPanel26: TUniPanel;
    UniPanel27: TUniPanel;
    UniBitBtn17: TUniBitBtn;
    UniBitBtn22: TUniBitBtn;
    UniBitBtn23: TUniBitBtn;
    UniBitBtn32: TUniBitBtn;
    btOpciones: TUniBitBtn;
    btFicha: TUniBitBtn;
    btLista: TUniBitBtn;
    pnl1: TUniPanel;
    UniLabel1: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniLabel13: TUniLabel;
    UniDBEdit15: TUniDBEdit;
    UniDBRadioGroup1: TUniDBRadioGroup;
    edHastaFechaReserva: TUniDBDateTimePicker;
    edDesdeFechaReserva: TUniDBDateTimePicker;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniDBNavigator1: TUniDBNavigator;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn24: TUniBitBtn;
    BtnEditar: TUniBitBtn;
    UniBitBtn30: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    dsReservaS: TDataSource;
    GridReservado: TUniDBGrid;
    dsCLIARTI1: TDataSource;
    dsCabe: TDataSource;
    RGSelDiasA: TUniPanel;
    RGSelDias: TUniRadioGroup;
    procedure UniBitBtn2Click(Sender: TObject);
    procedure UniBitBtn6Click(Sender: TObject);
    procedure btListaClick(Sender: TObject);
    procedure btFichaClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormClienteReserva: TFormClienteReserva;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uDMCliente, uCliente;

function FormClienteReserva: TFormClienteReserva;
begin
  Result := TFormClienteReserva(DMppal.GetFormInstance(TFormClienteReserva));

end;


procedure TFormClienteReserva.btFichaClick(Sender: TObject);
begin
  FormCliente.RutAbrirFichaClie;
end;

procedure TFormClienteReserva.btListaClick(Sender: TObject);
begin
  FormCliente.RutAbrirListaClie;
end;

procedure TFormClienteReserva.UniBitBtn2Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabNuevo;
end;

procedure TFormClienteReserva.UniBitBtn6Click(Sender: TObject);
begin
  pcDetalle.ActivePage := tabTablas;
end;

end.
