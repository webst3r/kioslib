unit uDistribuidoraMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormDistribuidoraMenu = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaDistribuidora: TUniTabSheet;
    tabFichaDistribuidora: TUniTabSheet;
    procedure UniBitBtn2Click(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swDistribuidoraLista, swDistribuidoraFicha : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    procedure RutAbrirFichaDistribuidora;
    procedure RutAbrirListaDistribuidora;

  end;

function FormDistribuidoraMenu: TFormDistribuidoraMenu;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMDistribuidora, uDistribuidoraLista, uDistribuidoraFicha;

function FormDistribuidoraMenu: TFormDistribuidoraMenu;
begin
  Result := TFormDistribuidoraMenu(DMppal.GetFormInstance(TFormDistribuidoraMenu));

end;

procedure TFormDistribuidoraMenu.RutAbrirFichaDistribuidora;
begin
  pcDetalle.ActivePage := tabFichaDistribuidora;
  if pcDetalle.ActivePage = tabFichaDistribuidora then
  begin
    if swDistribuidoraFicha = 0 then
    begin
      FormDistribuidoraFicha.Parent := tabFichaDistribuidora;
      FormDistribuidoraFicha.Show();
      swDistribuidoraFicha := 1;
    end;
  end;
  FormDistribuidoraFicha.pCodi.SetFocus;
end;

procedure TFormDistribuidoraMenu.RutAbrirListaDistribuidora;
begin
  pcDetalle.ActivePage := tabListaDistribuidora;
  if pcDetalle.ActivePage = tabListaDistribuidora then
  begin
    if swDistribuidoraLista = 0 then
    begin
      FormDistribuidoraLista.Parent := tabListaDistribuidora;
      FormDistribuidoraLista.Show();
      swDistribuidoraLista := 1;
    end;
  end;

  FormDistribuidoraLista.edFiltroCabe.SetFocus;
  DMPpal.RutCapturaGrid(FormDistribuidoraLista.Name, FormDistribuidoraLista.gridDistribuidoraLista, nil, 0, 0, 0);

end;



procedure TFormDistribuidoraMenu.UniBitBtn2Click(Sender: TObject);
begin
 // if pcDetalle.ActivePage = tabFichaCliente then


end;

end.


