unit uMenuCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormMenuCliente = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaCliente: TUniTabSheet;
    tabFichaCliente: TUniTabSheet;
    tabReservaCliente: TUniTabSheet;
    tabConsuPendiente: TUniTabSheet;
    pnlBotonera: TUniPanel;
    pnlBtsPesta�as: TUniPanel;
    btListaClie: TUniBitBtn;
    btFichaClie: TUniBitBtn;
    btReservaClie: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    pnlBotonesGeneral: TUniPanel;
    btVolver: TUniBitBtn;
    btSalir: TUniBitBtn;
    btBuscar: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    btCambiarCodigo: TUniButton;
    procedure pcDetalleChange(Sender: TObject);
    procedure btListaClieClick(Sender: TObject);
    procedure btFichaClieClick(Sender: TObject);
    procedure btReservaClieClick(Sender: TObject);
    procedure UniBitBtn2Click(Sender: TObject);
    procedure btCambiarCodigoClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swClieLista, swClieFicha, swClieReserva, swConsuPdte : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormMenuCliente: TFormMenuCliente;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuClienteLista, uMenuClienteFicha, uMenuClienteReserva;

function FormMenuCliente: TFormMenuCliente;
begin
  Result := TFormMenuCliente(DMppal.GetFormInstance(TFormMenuCliente));

end;

procedure TFormMenuCliente.btCambiarCodigoClick(Sender: TObject);
begin
  FormMenuClienteFicha.btCambiarCodigoClick(nil);
end;

procedure TFormMenuCliente.btFichaClieClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabFichaCliente;
  if pcDetalle.ActivePage = tabFichaCliente then
  begin
    if swClieFicha = 0 then
    begin
      FormMenuClienteFicha.Parent := tabFichaCliente;
      FormMenuClienteFicha.Show();
      swClieFicha := 1;
    end;
  end;
end;

procedure TFormMenuCliente.btListaClieClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaCliente;
  if pcDetalle.ActivePage = tabListaCliente then
  begin
    if swClieLista = 0 then
    begin
      FormMenuClienteLista.Parent := tabListaCliente;
      FormMenuClienteLista.Show();
      swClieLista := 1;
    end;
  end;
end;

procedure TFormMenuCliente.btReservaClieClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabReservaCliente;
  if pcDetalle.ActivePage = tabReservaCliente then
  begin
    if swClieReserva = 0 then
    begin
      FormMenuClienteReserva.Parent := tabReservaCliente;
      FormMenuClienteReserva.Show();
      swClieReserva := 1;
    end;
  end;
end;

procedure TFormMenuCliente.pcDetalleChange(Sender: TObject);
begin

  if pcDetalle.ActivePage = tabConsuPendiente then
  begin
    if swConsuPdte = 0 then
    begin
      //FormMenuClienteConsuPdte.Parent := tabConsuPendiente;
      //FormMenuClienteConsuPdte.Show();
      swConsuPdte := 1;
    end;
  end;
end;



procedure TFormMenuCliente.UniBitBtn2Click(Sender: TObject);
begin
  if pcDetalle.ActivePage = tabFichaCliente then


end;

end.
