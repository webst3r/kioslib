object frDMKioslib: TfrDMKioslib
  OldCreateOrder = False
  Height = 401
  Width = 711
  object frxPDFExport1: TfrxPDFExport
    FileName = 'C:\Users\Toni\Desktop\1111.pdf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 43130.701088159720000000
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 93
    Top = 14
  end
  object frxDBCabeS: TfrxDBDataset
    UserName = 'frxDBCabeS'
    CloseDataSource = False
    DataSource = dsCabeS
    BCDToCurrency = False
    Left = 357
    Top = 160
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 104
    Top = 64
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43389.594758090300000000
    ReportOptions.LastChange = 43445.716036400460000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnPreview = frxReport1Preview
    Left = 24
    Top = 16
    Datasets = <
      item
        DataSet = frxDBCabe1
        DataSetName = 'frxDBCabe1'
      end
      item
        DataSet = frxDBCabeS
        DataSetName = 'frxDBCabeS'
      end
      item
        DataSet = frxDBEmpresa
        DataSetName = 'frxDBEmpresa'
      end
      item
        DataSet = frxDBLinea
        DataSetName = 'frxDBLinea'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 434.645950000000000000
        Width = 718.110700000000000000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 211.653680000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo13: TfrxMemoView
          Top = 186.299320000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cod.Prov')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 86.929133860000000000
          Top = 186.299320000000000000
          Width = 166.299320000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descripci'#243'n')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 253.228346460000000000
          Top = 186.299320000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Recla')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 291.023622050000000000
          Top = 186.299320000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Num')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 343.937007870000000000
          Top = 186.299320000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PVP')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 374.173228346457000000
          Top = 186.299320000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dia')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 430.866141730000000000
          Top = 186.299320000000000000
          Width = 26.456710000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Paq')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 457.322834645669000000
          Top = 186.299320000000000000
          Width = 41.574830000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Coste')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 498.897637800000000000
          Top = 186.299320000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'T.Coste')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 551.811023620000000000
          Top = 186.299320000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '%I+R')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 623.622047240000000000
          Top = 186.299320000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 661.417322834646000000
          Top = 186.299320000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Tot PVP')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 52.913385830000000000
          Top = 186.299320000000000000
          Width = 34.015770000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cant.')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 582.047244094487900000
          Top = 186.299320000000000000
          Width = 41.574830000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'IVA+RE')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = -0.118120000000000000
          Top = 29.015770000000010000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'NOMBRE'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBEmpresa."NOMBRE"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = -0.118120000000000000
          Top = 49.328772500000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'NOMBRE2'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBEmpresa."NOMBRE2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = -0.338590000000000000
          Top = 69.641775000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'DIRECCION'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBEmpresa."DIRECCION"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 273.126160000000000000
          Top = 29.015770000000010000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Documento: ')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 623.622450000000000000
          Top = 29.015770000000010000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pagina [Page#]')
        end
        object Memo47: TfrxMemoView
          Left = -0.338590000000000000
          Top = 89.954777500000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'DIRECCION2'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBEmpresa."DIRECCION2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = -0.338590000000000000
          Top = 110.267780000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'POBLACION'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBEmpresa."POBLACION"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 136.826840000000000000
          Width = 714.331170000000000000
          Height = 37.795300000000000000
          DataField = 'OBSERVACIONES'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBCabe1."OBSERVACIONES"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 273.126160000000000000
          Top = 49.328772500000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Codigo TPV: ')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 316.141930000000000000
          Top = 69.641775000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Ruta: ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 256.889920000000000000
          Top = 110.362204724409400000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Total Unidades: [frxDBCabe1."TotalDevueltos"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 400.512060000000000000
          Top = 110.362204720000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fecha Abono: ')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 485.882190000000000000
          Top = 110.362204720000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'FECHAABONO'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."FECHAABONO"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 485.882190000000000000
          Top = 89.952755909999990000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'FECHACARGO'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."FECHACARGO"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 400.512060000000000000
          Top = 89.952755909999990000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fecha Cargo: ')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 351.937230000000000000
          Top = 29.015770000000010000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'DOCTOPROVE'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."DOCTOPROVE"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 351.937230000000000000
          Top = 49.328772500000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'REFEPROVEEDOR'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."REFEPROVEEDOR"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 351.937230000000000000
          Top = 69.641775000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'RUTA'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."RUTA"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 461.323130000000000000
          Top = 29.015770000000010000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fecha:')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 505.897960000000000000
          Top = 29.015770000000010000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'FECHA'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBCabe1."FECHA"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = -0.440940000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Devoluci'#243'n: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 86.488250000000000000
          Top = 0.661410000000000000
          Width = 536.693260000000000000
          Height = 18.897650000000000000
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBCabe1."IDSTOCABE"] [frxDBCabe1."NOMBRE"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object BarCode1: TfrxBarCodeView
          Left = 596.213050000000000000
          Top = 68.031540000000010000
          Width = 103.000000000000000000
          Height = 45.354360000000000000
          Visible = False
          BarType = bcCodeEAN13
          DataField = 'DOCTOPROVE'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Rotation = 0
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Line1: TfrxLineView
          Top = 204.756030000000000000
          Width = 718.110236220000000000
          Color = clBlack
          Diagonal = True
        end
        object Memo12: TfrxMemoView
          Left = 151.181200000000000000
          Top = 81.929190000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Visible = False
          AutoWidth = True
          DataField = 'TextoPaquete'
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBCabe1."TextoPaquete"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 256.889920000000000000
          Top = 89.952755909999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataSet = frxDBCabe1
          DataSetName = 'frxDBCabe1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Paquete [frxDBCabe1."PAQUETE"] de [frxDBCabeS."TOTALPAQUETES"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo1: TfrxMemoView
          Left = 151.181200000000000000
          Top = 62.149660000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Visible = False
          AutoWidth = True
          DataField = 'TOTALPAQUETES'
          DataSet = frxDBCabeS
          DataSetName = 'frxDBCabeS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBCabeS."TOTALPAQUETES"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        DataSet = frxDBLinea
        DataSetName = 'frxDBLinea'
        RowCount = 0
        Stretched = True
        object Memo25: TfrxMemoView
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataField = 'REFEPROVE'
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBLinea."REFEPROVE"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 86.929133860000000000
          Top = 3.779530000000022000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBLinea."DESCRIPCION"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 253.228346460000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'RECLAMADO'
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBLinea."RECLAMADO"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 291.023622050000000000
          Top = 3.779530000000022000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'ADENDUM'
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBLinea."ADENDUM"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 343.936873620000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[(<frxDBLinea."PRECIOVENTA"> + <frxDBLinea."PRECIOVENTA2">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 374.173228350000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'FECHADEVOL'
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBLinea."FECHADEVOL"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 430.866141730000000000
          Top = 3.779530000000022000
          Width = 26.456692910000000000
          Height = 18.897650000000000000
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBLinea."paquete"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 457.322834650000000000
          Top = 3.779530000000022000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDBLinea."PRECIOCOSTE"> + <frxDBLinea."PRECIOCOSTE2">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 498.897637800000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[<frxDBLinea."DEVUELTOS">  * (<frxDBLinea."PRECIOCOSTE"> + <frxD' +
              'BLinea."PRECIOCOSTE2">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 551.811023620000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDBLinea."TPCIVA"> + <frxDBLinea."TPCRE">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 582.047244090000000000
          Top = 3.779530000000022000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDBLinea."DEVUELTOS"> * ((<frxDBLinea."PRECIOCOSTE"> + <frxD' +
              'BLinea."PRECIOCOSTE2">) * ((<frxDBLinea."TPCIVA"> + <frxDBLinea.' +
              '"TPCRE">) / 100))]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 623.622047240000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDBLinea."DEVUELTOS"> * ((<frxDBLinea."PRECIOCOSTE"> + <frxD' +
              'BLinea."PRECIOCOSTE2">) + (<frxDBLinea."PRECIOCOSTE"> + <frxDBLi' +
              'nea."PRECIOCOSTE2">) * ((<frxDBLinea."TPCIVA"> + <frxDBLinea."TP' +
              'CRE">) / 100))]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 661.417322830000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[<frxDBLinea."DEVUELTOS"> * (<frxDBLinea."PRECIOVENTA"> + <frxDB' +
              'Linea."PRECIOVENTA2">)]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 52.913420000000000000
          Top = 3.779530000000022000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'DEVUELTOS'
          DataSet = frxDBLinea
          DataSetName = 'frxDBLinea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBLinea."DEVUELTOS"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 343.937230000000000000
        Width = 718.110700000000000000
        object Memo38: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 502.677167800000000000
          Top = 3.779530000000022000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[SUM(<frxDBLinea."DEVUELTOS">  * (<frxDBLinea."PRECIOCOSTE"> + <' +
              'frxDBLinea."PRECIOCOSTE2">))]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 582.047244090000000000
          Top = 3.779530000000022000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[SUM(<frxDBLinea."PRECIOVENTA"> + <frxDBLinea."PRECIOVENTA2">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 623.622047240000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDBLinea."DEVUELTOS">  * ((<frxDBLinea."PRECIOCOSTE"> + ' +
              '<frxDBLinea."PRECIOCOSTE2">) + (<frxDBLinea."PRECIOCOSTE"> + <fr' +
              'xDBLinea."PRECIOCOSTE2">) * ((<frxDBLinea."TPCIVA"> + <frxDBLine' +
              'a."TPCRE">) / 100)))]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 661.417322830000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[SUM(<frxDBLinea."DEVUELTOS"> * (<frxDBLinea."PRECIOVENTA"> + <f' +
              'rxDBLinea."PRECIOVENTA2">))]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 52.913420000000000000
          Top = 3.779530000000022000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDBLinea."DEVUELTOS">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779530000000022000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 96
    Top = 112
  end
  object dsCabeS: TDataSource
    DataSet = DMDevolucion.sqlCabeS
    Left = 359
    Top = 214
  end
  object dsHisArti: TDataSource
    DataSet = DMMenuRecepcion.sqlHisArti
    Left = 351
    Top = 80
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID_HISARTI=ID_HISARTI'
      'SWES=SWES'
      'FECHA=FECHA'
      'HORA=HORA'
      'CLAVE=CLAVE'
      'ID_ARTICULO=ID_ARTICULO'
      'ADENDUM=ADENDUM'
      'ID_PROVEEDOR=ID_PROVEEDOR'
      'REFEPROVE=REFEPROVE'
      'IDSTOCABE=IDSTOCABE'
      'IDDEVOLUCABE=IDDEVOLUCABE'
      'PAQUETE=PAQUETE'
      'ID_HISARTI01=ID_HISARTI01'
      'ID_DIARIA=ID_DIARIA'
      'PARTIDA=PARTIDA'
      'NRECLAMACION=NRECLAMACION'
      'ID_FRAPROVE=ID_FRAPROVE'
      'ID_CLIENTE=ID_CLIENTE'
      'SWTIPOFRA=SWTIPOFRA'
      'NFACTURA=NFACTURA'
      'NALBARAN=NALBARAN'
      'NLINEA=NLINEA'
      'DEVUELTOS=DEVUELTOS'
      'VENDIDOS=VENDIDOS'
      'MERMA=MERMA'
      'CARGO=CARGO'
      'ABONO=ABONO'
      'RECLAMADO=RECLAMADO'
      'CANTIDAD=CANTIDAD'
      'CANTIENALBA=CANTIENALBA'
      'CANTISUSCRI=CANTISUSCRI'
      'PRECIOCOMPRA=PRECIOCOMPRA'
      'PRECIOCOMPRA2=PRECIOCOMPRA2'
      'PRECIOCOSTE=PRECIOCOSTE'
      'PRECIOCOSTE2=PRECIOCOSTE2'
      'PRECIOCOSTETOT=PRECIOCOSTETOT'
      'PRECIOCOSTETOT2=PRECIOCOSTETOT2'
      'PRECIOVENTA=PRECIOVENTA'
      'PRECIOVENTA2=PRECIOVENTA2'
      'SWDTO=SWDTO'
      'TPCDTO=TPCDTO'
      'TIVA=TIVA'
      'TPCIVA=TPCIVA'
      'TPCRE=TPCRE'
      'TIVA2=TIVA2'
      'TPCIVA2=TPCIVA2'
      'TPCRE2=TPCRE2'
      'IMPOBRUTO=IMPOBRUTO'
      'IMPODTO=IMPODTO'
      'IMPOBASE=IMPOBASE'
      'IMPOIVA=IMPOIVA'
      'IMPORE=IMPORE'
      'IMPOBASE2=IMPOBASE2'
      'IMPOIVA2=IMPOIVA2'
      'IMPORE2=IMPORE2'
      'IMPOTOTLIN=IMPOTOTLIN'
      'ENTRADAS=ENTRADAS'
      'SALIDAS=SALIDAS'
      'VALORCOSTE=VALORCOSTE'
      'VALORCOSTE2=VALORCOSTE2'
      'VALORCOSTETOT=VALORCOSTETOT'
      'VALORCOSTETOT2=VALORCOSTETOT2'
      'VALORCOMPRA=VALORCOMPRA'
      'VALORCOMPRA2=VALORCOMPRA2'
      'VALORVENTA=VALORVENTA'
      'VALORVENTA2=VALORVENTA2'
      'VALORMOVI=VALORMOVI'
      'TPCCOMISION=TPCCOMISION'
      'IMPOCOMISION=IMPOCOMISION'
      'MARGEN=MARGEN'
      'MARGEN2=MARGEN2'
      'ENCARTE=ENCARTE'
      'ENCARTE2=ENCARTE2'
      'TURNO=TURNO'
      'NALMACEN=NALMACEN'
      'SWALTABAJA=SWALTABAJA'
      'SWPDTEPAGO=SWPDTEPAGO'
      'SWESTADO=SWESTADO'
      'SWDEVOLUCION=SWDEVOLUCION'
      'SWCERRADO=SWCERRADO'
      'SWMARCA=SWMARCA'
      'SWMARCA2=SWMARCA2'
      'SWMARCA3=SWMARCA3'
      'SWMARCA4=SWMARCA4'
      'SWMARCA5=SWMARCA5'
      'SEMANA=SEMANA'
      'TIPOVENTA=TIPOVENTA'
      'FECHAAVISO=FECHAAVISO'
      'FECHADEVOL=FECHADEVOL'
      'FECHACARGO=FECHACARGO'
      'FECHAABONO=FECHAABONO'
      'SWRECLAMA=SWRECLAMA'
      'ID_HISARTIRE=ID_HISARTIRE'
      'SWACABADO=SWACABADO'
      'BARRAS=BARRAS'
      'DESCRIPCION=DESCRIPCION'
      'TBARRAS=TBARRAS'
      'CosteTotal=CosteTotal'
      'PrecioTotal=PrecioTotal')
    DataSource = dsHisArti
    BCDToCurrency = False
    Left = 349
    Top = 24
  end
  object dsTerceDI: TDataSource
    Left = 647
    Top = 81
  end
  object frxDBDataset3: TfrxDBDataset
    UserName = 'frxDBDataset3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CODIGO=CODIGO'
      'CDIRECCION=CDIRECCION'
      'NOMBRE=NOMBRE'
      'NOMBRE2=NOMBRE2'
      'DIRECCION=DIRECCION'
      'DIRECCION2=DIRECCION2'
      'CPOSTAL=CPOSTAL'
      'POBLACION=POBLACION'
      'PROVINCIA=PROVINCIA'
      'CPAIS=CPAIS'
      'CPROVINCIA=CPROVINCIA'
      'CONTACTO1CARGO=CONTACTO1CARGO'
      'CONTACTO1NOMBRE=CONTACTO1NOMBRE'
      'CONTACTO2CARGO=CONTACTO2CARGO'
      'CONTACTO2NOMBRE=CONTACTO2NOMBRE'
      'TELEFONO1=TELEFONO1'
      'TELEFONO2=TELEFONO2'
      'FAX=FAX'
      'EMAIL=EMAIL'
      'MENSAJEAVISO=MENSAJEAVISO'
      'OBSERVACIONES=OBSERVACIONES'
      'BANCO=BANCO'
      'AGENCIA=AGENCIA'
      'DC=DC'
      'CUENTA=CUENTA'
      'TRANSPORTISTA=TRANSPORTISTA'
      'HORARIO=HORARIO'
      'TDIRECCION=TDIRECCION'
      'SWETIQUETA=SWETIQUETA'
      'FECHAALTA=FECHAALTA'
      'FECHAULTI=FECHAULTI'
      'HORAULTI=HORAULTI'
      'USUULTI=USUULTI'
      'NOTAULTI=NOTAULTI')
    DataSource = dsTerceDI
    BCDToCurrency = False
    Left = 645
    Top = 16
  end
  object dsLinea: TDataSource
    DataSet = DMDevolucion.sqlLinea
    Left = 519
    Top = 216
  end
  object frxDBEmpresa: TfrxDBDataset
    UserName = 'frxDBEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NREFERENCIA=NREFERENCIA'
      'NOMBRE=NOMBRE'
      'NOMBRE2=NOMBRE2'
      'TVIA=TVIA'
      'VIANOMBRE=VIANOMBRE'
      'VIANUM=VIANUM'
      'VIABLOC=VIABLOC'
      'VIABIS=VIABIS'
      'VIAESCA=VIAESCA'
      'VIAPISO=VIAPISO'
      'VIAPUERTA=VIAPUERTA'
      'DIRECCION=DIRECCION'
      'DIRECCION2=DIRECCION2'
      'POBLACION=POBLACION'
      'PROVINCIA=PROVINCIA'
      'CPOSTAL=CPOSTAL'
      'CPAIS=CPAIS'
      'CPROVINCIA=CPROVINCIA'
      'NIF=NIF'
      'CONTACTO1NOMRE=CONTACTO1NOMRE'
      'CONTACTO1CARGO=CONTACTO1CARGO'
      'CONTACTO2NOMBRE=CONTACTO2NOMBRE'
      'CONTACTO2CARGO=CONTACTO2CARGO'
      'TELEFONO1=TELEFONO1'
      'TELEFONO2=TELEFONO2'
      'MOVIL=MOVIL'
      'FAX=FAX'
      'EMAIL=EMAIL'
      'WEB=WEB'
      'MENSAJEAVISO=MENSAJEAVISO'
      'OBSERVACIONES=OBSERVACIONES'
      'NACTIVIDAD=NACTIVIDAD'
      'NSECTOR=NSECTOR'
      'TPCIVA1=TPCIVA1'
      'TPCIVA2=TPCIVA2'
      'TPCIVA3=TPCIVA3'
      'TPCRE1=TPCRE1'
      'TPCRE2=TPCRE2'
      'TPCRE3=TPCRE3'
      'NPEDIDO=NPEDIDO'
      'NPEDIPROVE=NPEDIPROVE'
      'NALBARAN=NALBARAN'
      'NALBASERVI=NALBASERVI'
      'NALBACOLECTOR=NALBACOLECTOR'
      'NALBALMA=NALBALMA'
      'NALBAREPARA=NALBAREPARA'
      'NFACTURANORMAL=NFACTURANORMAL'
      'NFACTURASERVI=NFACTURASERVI'
      'NFACTURACOLECTOR=NFACTURACOLECTOR'
      'NFACTURATIENDA=NFACTURATIENDA'
      'NFACTURAREPARA=NFACTURAREPARA'
      'NUMTICKET=NUMTICKET'
      'NCOMPRA=NCOMPRA'
      'NTRASPASO=NTRASPASO'
      'REGISTROMERCANTIL=REGISTROMERCANTIL'
      'PIEFRANORMAL=PIEFRANORMAL'
      'PIEFRASERVI=PIEFRASERVI'
      'PIEFRACOLLECTOR=PIEFRACOLLECTOR'
      'PIEFRATIENDA=PIEFRATIENDA'
      'PIEFRAREPARA=PIEFRAREPARA'
      'IMPTOPEFRAIDE=IMPTOPEFRAIDE'
      'CTACONTAVTA1=CTACONTAVTA1'
      'CTACONTAVTA2=CTACONTAVTA2'
      'CTACONTAVTA3=CTACONTAVTA3'
      'CTACONTAVTA4=CTACONTAVTA4'
      'CTACONTAVTA5=CTACONTAVTA5'
      'CTACONTAIVA1=CTACONTAIVA1'
      'CTACONTAIVA2=CTACONTAIVA2'
      'CTACONTAIVA3=CTACONTAIVA3'
      'CTACONTARE1=CTACONTARE1'
      'CTACONTARE2=CTACONTARE2'
      'CTACONTARE3=CTACONTARE3'
      'TPCINCRECOMPRACESIO=TPCINCRECOMPRACESIO'
      'TPCINCRECOMPRAVENTA=TPCINCRECOMPRAVENTA'
      'NALMACEN=NALMACEN'
      'RUTAFTP=RUTAFTP'
      'RUTAIMAGENFTP=RUTAIMAGENFTP'
      'RUTAIMAGENPC=RUTAIMAGENPC'
      'RUTACAMPO=RUTACAMPO'
      'RUTAAVISOSFTP=RUTAAVISOSFTP'
      'RUTAAVISOSPC=RUTAAVISOSPC'
      'HOSTFTP=HOSTFTP'
      'USUFTP=USUFTP'
      'PASSFTP=PASSFTP'
      'DIASRECEGEN=DIASRECEGEN'
      'TRANSPORTE=TRANSPORTE'
      'SWRECARGOE=SWRECARGOE'
      'SWALTABAJA=SWALTABAJA'
      'FECHABAJA=FECHABAJA'
      'FECHAALTA=FECHAALTA'
      'HORAALTA=HORAALTA'
      'FECHAULTI=FECHAULTI'
      'HORAULTI=HORAULTI'
      'USUULTI=USUULTI'
      'NOTAULTI=NOTAULTI')
    DataSource = dsEmpresa
    BCDToCurrency = False
    Left = 445
    Top = 168
  end
  object dsCabe1Devol: TDataSource
    DataSet = DMDevolucion.sqlCabe1
    Left = 591
    Top = 216
  end
  object frxDBLinea: TfrxDBDataset
    UserName = 'frxDBLinea'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID_HISARTI=ID_HISARTI'
      'SWES=SWES'
      'FECHA=FECHA'
      'HORA=HORA'
      'CLAVE=CLAVE'
      'ID_ARTICULO=ID_ARTICULO'
      'ADENDUM=ADENDUM'
      'ID_PROVEEDOR=ID_PROVEEDOR'
      'REFEPROVE=REFEPROVE'
      'IDSTOCABE=IDSTOCABE'
      'IDDEVOLUCABE=IDDEVOLUCABE'
      'PAQUETE=PAQUETE'
      'ID_HISARTI01=ID_HISARTI01'
      'ID_DIARIA=ID_DIARIA'
      'PARTIDA=PARTIDA'
      'NRECLAMACION=NRECLAMACION'
      'ID_FRAPROVE=ID_FRAPROVE'
      'ID_CLIENTE=ID_CLIENTE'
      'SWTIPOFRA=SWTIPOFRA'
      'NFACTURA=NFACTURA'
      'NALBARAN=NALBARAN'
      'NLINEA=NLINEA'
      'DEVUELTOS=DEVUELTOS'
      'VENDIDOS=VENDIDOS'
      'MERMA=MERMA'
      'CARGO=CARGO'
      'ABONO=ABONO'
      'RECLAMADO=RECLAMADO'
      'CANTIDAD=CANTIDAD'
      'CANTIENALBA=CANTIENALBA'
      'CANTISUSCRI=CANTISUSCRI'
      'PRECIOCOMPRA=PRECIOCOMPRA'
      'PRECIOCOMPRA2=PRECIOCOMPRA2'
      'PRECIOCOSTE=PRECIOCOSTE'
      'PRECIOCOSTE2=PRECIOCOSTE2'
      'PRECIOCOSTETOT=PRECIOCOSTETOT'
      'PRECIOCOSTETOT2=PRECIOCOSTETOT2'
      'PRECIOVENTA=PRECIOVENTA'
      'PRECIOVENTA2=PRECIOVENTA2'
      'SWDTO=SWDTO'
      'TPCDTO=TPCDTO'
      'TIVA=TIVA'
      'TPCIVA=TPCIVA'
      'TPCRE=TPCRE'
      'TIVA2=TIVA2'
      'TPCIVA2=TPCIVA2'
      'TPCRE2=TPCRE2'
      'IMPOBRUTO=IMPOBRUTO'
      'IMPODTO=IMPODTO'
      'IMPOBASE=IMPOBASE'
      'IMPOIVA=IMPOIVA'
      'IMPORE=IMPORE'
      'IMPOBASE2=IMPOBASE2'
      'IMPOIVA2=IMPOIVA2'
      'IMPORE2=IMPORE2'
      'IMPOTOTLIN=IMPOTOTLIN'
      'ENTRADAS=ENTRADAS'
      'SALIDAS=SALIDAS'
      'VALORCOSTE=VALORCOSTE'
      'VALORCOSTE2=VALORCOSTE2'
      'VALORCOSTETOT=VALORCOSTETOT'
      'VALORCOSTETOT2=VALORCOSTETOT2'
      'VALORCOMPRA=VALORCOMPRA'
      'VALORCOMPRA2=VALORCOMPRA2'
      'VALORVENTA=VALORVENTA'
      'VALORVENTA2=VALORVENTA2'
      'VALORMOVI=VALORMOVI'
      'TPCCOMISION=TPCCOMISION'
      'IMPOCOMISION=IMPOCOMISION'
      'MARGEN=MARGEN'
      'MARGEN2=MARGEN2'
      'ENCARTE=ENCARTE'
      'ENCARTE2=ENCARTE2'
      'TURNO=TURNO'
      'NALMACEN=NALMACEN'
      'SWALTABAJA=SWALTABAJA'
      'SWPDTEPAGO=SWPDTEPAGO'
      'SWESTADO=SWESTADO'
      'SWDEVOLUCION=SWDEVOLUCION'
      'SWCERRADO=SWCERRADO'
      'SWMARCA=SWMARCA'
      'SWMARCA2=SWMARCA2'
      'SWMARCA3=SWMARCA3'
      'SWMARCA4=SWMARCA4'
      'SWMARCA5=SWMARCA5'
      'SEMANA=SEMANA'
      'TIPOVENTA=TIPOVENTA'
      'FECHAAVISO=FECHAAVISO'
      'FECHADEVOL=FECHADEVOL'
      'FECHACARGO=FECHACARGO'
      'FECHAABONO=FECHAABONO'
      'SWRECLAMA=SWRECLAMA'
      'ID_HISARTIRE=ID_HISARTIRE'
      'SWACABADO=SWACABADO'
      'BARRAS=BARRAS'
      'DESCRIPCION=DESCRIPCION'
      'TBARRAS=TBARRAS'
      'FECHACOMPRA=FECHACOMPRA'
      'COMPRAHISTO=COMPRAHISTO'
      'VENTAHISTO=VENTAHISTO'
      'DEVOLHISTO=DEVOLHISTO'
      'MERMAHISTO=MERMAHISTO'
      'NOMBRE=NOMBRE'
      'PrecioTotal=PrecioTotal')
    DataSource = dsLinea
    BCDToCurrency = False
    Left = 525
    Top = 168
  end
  object dsEmpresa: TDataSource
    DataSet = DMDevolucion.sqlEmpresa
    Left = 447
    Top = 216
  end
  object frxDBCabe1: TfrxDBDataset
    UserName = 'frxDBCabe1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IDSTOCABE=IDSTOCABE'
      'SWTIPODOCU=SWTIPODOCU'
      'NDOCSTOCK=NDOCSTOCK'
      'FECHA=FECHA'
      'NALMACEN=NALMACEN'
      'NALMACENORIGEN=NALMACENORIGEN'
      'NALMACENDESTINO=NALMACENDESTINO'
      'ID_PROVEEDOR=ID_PROVEEDOR'
      'FABRICANTE=FABRICANTE'
      'SWDOCTOPROVE=SWDOCTOPROVE'
      'DOCTOPROVE=DOCTOPROVE'
      'DOCTOPROVEFECHA=DOCTOPROVEFECHA'
      'FECHACARGO=FECHACARGO'
      'FECHAABONO=FECHAABONO'
      'NOMBRE=NOMBRE'
      'NIF=NIF'
      'MENSAJEAVISO=MENSAJEAVISO'
      'OBSERVACIONES=OBSERVACIONES'
      'PAQUETE=PAQUETE'
      'SWDEVOLUCION=SWDEVOLUCION'
      'SWCERRADO=SWCERRADO'
      'SWFRATIENDA=SWFRATIENDA'
      'DEVOLUSWTIPOINC=DEVOLUSWTIPOINC'
      'DEVOLUFECHASEL=DEVOLUFECHASEL'
      'DEVOLUDOCTOPROVE=DEVOLUDOCTOPROVE'
      'DEVOLUIDSTOCABE=DEVOLUIDSTOCABE'
      'ID_FRAPROVE=ID_FRAPROVE'
      'SWMARCA=SWMARCA'
      'SWMARCA2=SWMARCA2'
      'SWMARCA3=SWMARCA3'
      'SWMARCA4=SWMARCA4'
      'SWMARCA5=SWMARCA5'
      'SEMANA=SEMANA'
      'SWALTABAJA=SWALTABAJA'
      'FECHABAJA=FECHABAJA'
      'FECHAALTA=FECHAALTA'
      'HORAALTA=HORAALTA'
      'FECHAULTI=FECHAULTI'
      'HORAULTI=HORAULTI'
      'USUULTI=USUULTI'
      'NOTAULTI=NOTAULTI'
      'ID_DIRECCION=ID_DIRECCION'
      'MAXPAQUETE=MAXPAQUETE'
      'REFEPROVEEDOR=REFEPROVEEDOR'
      'RUTA=RUTA'
      'TextoPaquete=TextoPaquete'
      'TotalDevueltos=TotalDevueltos')
    DataSource = dsCabe1Devol
    BCDToCurrency = False
    Left = 597
    Top = 168
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 88
    Top = 168
  end
end
