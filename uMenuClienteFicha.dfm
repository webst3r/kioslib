object FormMenuClienteFicha: TFormMenuClienteFicha
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 560
  ClientWidth = 1008
  Caption = 'FormMenuArti'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TUniPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Hint = ''
    Visible = False
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object btVolver: TUniBitBtn
      Left = 26
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Volver'
      ShowHint = True
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF109F1018A21820A52020A520
        20A52020A52020A52020A52020A52020A52020A520109F10FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00990098
        F8B89CF6BA96F5B595F5B595F5B595F5B595F5B596F5B596F6B6ACF9C6009900
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF00990093F2B366EC924FE8824EE7814EE7814EE7814EE7814FE88250
        E983A8F5C2009900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0099008EEEAE5FE58B47E07A46DF7946DF7946DF
        7946DF7947E07A48E17BA4F1BE009900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00990089E9A958DE853FD872
        3FD8723ED7713ED7713ED7713FD87240D973A0EDBA009900FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00990085
        E4A552D87F39D26C38D16B37D06A37D06A38D16B38D16B3AD36D9DE9B6009900
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF00990081E1A14DD37A33CC6633CC6633CC6633CC6633CC6633CC6634
        CD679AE6B3009900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0099007FDF9F4CD27933CC6633CC6633CC6633CC
        6633CC6633CC6633CC6699E6B3009900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0099007FDF9F4CD27939CD6B
        3CCE6D3CCE6D3CCE6D3CCE6D33CC6633CC6699E6B3009900FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00990085
        E1A46FDB935FD7875FD7875FD78760D7885FD7875AD68341CF7199E6B3009900
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0099008EE3AA7FE0A06EDC936FDD936FDD946FDD946FDD936EDC936D
        DC92ABEAC0009900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90D390009900
        009900009900009900009900039C068DE4AA8DE6AB7EE39F7EE3A07FE4A07FE4
        A17EE3A07EE3A07DE29FBEF0CE21A72320A52020A52020A52018A21800990090
        D390F0F9F030AC3016A61E85DFA190E4AC97E7B19BE8B5B8F0CB9AEAB58DE8AB
        8EE9AC8EE9AC8EE9AC8EE9AC8DE8AC8CE7ABB3EFC799E9B498E7B29EE8B6A3E7
        B934B13930AC30F0F9F0FFFFFFF0F9F030AC3059BE5BC5F0D2B7EFCAA6EDBDA7
        EEBE9EEDB89BEDB79CEEB79CEEB79CEEB79CEEB79CEDB79BEDB69AEBB599EAB4
        A0EBB9C6F1D448B74A30AC30F0F9F0FFFFFFFFFFFFFFFFFFF0F9F030AC3061C1
        63CDF3D8B6F1CAA7EFBFA8F0C0A9F1C1A9F2C1A9F2C2A9F2C2A9F2C1A9F1C1A8
        F1C0A7F0BFAEF0C4C9F3D64BB84D30AC30F0F9F0FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFF0F9F030AC3043B546D5F6DFC1F5D2B4F4C9B5F5CAB6F5CBB6F5CBB6F5
        CBB6F5CBB5F5CAB5F4CABAF4CED1F5DB40B44230AC30F0F9F0FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F9F030AC3048B749DFFAE7CCF8DBC1F7D3
        C2F8D4C2F8D4C2F9D4C2F8D4C2F7D3C6F8D7D7F7E141B44330AC30F0F9F0FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F9F030AC3042
        B544DFF9E6D3FAE0CEFADDCEFBDDCEFBDDCEFADDD2FAE0DEF9E542B54430AC30
        F0F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF0F9F030AC3043B545E4FAEADCFCE7D9FCE5D9FCE5DCFCE7DCFAE43C
        B43F30AC30F0F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F9F030AC303CB540E1FAE8E6FDEEE6FD
        EEE1FBE83CB54030AC30F0F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F9F030AC30
        3DB540E6FBEBE6FBEB3DB54030AC30F0F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF0F9F030AC3039B53E3DB54130AC30F0F9F0FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F9F030AC3030AC30F0F9F0FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Caption = ''
      TabOrder = 1
      IconAlign = iaTop
    end
    object btSalir: TUniBitBtn
      Left = 466
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Salir'
      ShowHint = True
      Caption = ''
      TabOrder = 2
      IconAlign = iaTop
      ImageIndex = 53
    end
    object btBuscar: TUniBitBtn
      Left = 89
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Buscar'
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C89D9DA0CECECEFDFDFD0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B21C46B51047B3424468CECECE
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B30B3AC0118CFF39CBFF276B
        B69D9DA10000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1B50C41C91089FF34BEFF5C
        F4FF396FBDC7C7CA0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2A2B60A3BCF118BFF34BEFF
        5BF2FF3573CDA1A1B5FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        ECECECC7C7C7B4B4B4B9B9B9D3D3D3F1F1F1FFFFFFB4B5C9164DD40E8AFF35BC
        FF5CF3FF3B7BD4A2A2B7FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFE9E9
        E99595956D6F6F6E6F716F70716163635354556B6B6AB5B5B5656CB98FD1EB50
        D5FA55EFFF3676D99F9FB7FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFCB
        CBCB7778799C9C9EC1BCB7D0C7BCD0C6BBC1B9B0A19D987070714446477E7E85
        EBDFD6ADF2FB3375DEA2A2BAFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        D3D3D37F8183CCC8C4EEE0CDFCEDD7FFF3DEFFF5E1FFF4DFF1E7D1CABEAD908D
        8BBBBCB897979D6971C5B4B4CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFF1F1F1868789D7D1CDFEE9D0FFF0DAFFEFDAFFEED9FFEED9FFEFDAFFF3DEFF
        F6DFDAC8B1928F8B353739B5B5B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFAFAFAFB6B5B8FDE8CFFFECD1FFEBD2FFEAD0FFEACEFFEAD0FFEAD0
        FFEAD1FFECD6FFF3DAC3B19C5E6061646463F2F2F2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFF96989AE8DDD3FFE6C5FFE9CEFFE5C6FFE3C3FFE2C1FFE2
        C1FFE3C3FFE5C7FFE7CBFFF1D8ECD6B78E867E404143D3D3D3FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFF9C9FA1FBE5D1FFE1BDFFDFBCFFDAB2FFD7ABFF
        D5A9FFD5A9FFD7ACFFDAB2FFDEBAFFE7C8FEE5C4AE9C8B444649B8B8B8FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFA2A5A8FDDFC4FFD7ACFFD2A5FFCF9B
        FFCC98FFCC98FFCC98FFCC98FFCE9BFFD2A5FFDCB4FFE1BABAA08C4A4C4FB2B2
        B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFA1A5A8FDDDC6FFCA95FFCC
        97FFCE9BFFD09FFFD2A3FFD2A3FFD0A0FFCE9AFFCB96FFD2A0FED2A4B29A874E
        5053C7C7C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF9FA2A4F2DDD1FF
        C58FFFD3A3FFD4AAFFD8B0FFD9B2FFD9B2FFD7AFFFD5ABFFD1A1FFD39EEDB885
        9A8C85525456ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFBCBDBD
        C8C7C9FFC9A5FFD5A8FFDFBCFFDFBFFFE1C2FFE1C2FFDFBEFFDBB7FFDBB3FFD4
        A0CC9B7974787A898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFF4F4F4999D9DE9D7D0FDBB94FFE1C1FFEED8FFECD6FFEBD3FFEAD1FFEBCEFF
        DEB6E2A37AA397915A5D5EE9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFDDDDDD9EA3A6E5CFC8F6B89BFBC9ADFEE3D0FFE8D6FDDEC8
        F7C0A0D89E81AE9E9A676C6EC9C9C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFDDDDDD9CA0A2C0BDBEDBB8ACE2AD9BE2A6
        91D5A190BF9F959A9B9D75797ACDCDCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFF6F6F6C2C2C3A5A9ABA1
        A7A8A0A4A6979C9E939799ACADADF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = ''
      TabOrder = 3
      IconAlign = iaTop
    end
    object UniBitBtn2: TUniBitBtn
      Left = 152
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Nuevo'
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        DACEC39F74509E71499E724B9E724B9E724B9E724B9E734B9E734B9E724BA375
        4BA0724CAB7A4F795A632076E8585784AC784FB69277FDFDFEFFFFFF0000FFFF
        FFFFFFFF99612AEABE86F0C798EFC391EFC290EFC28EEFC18DEFC08BEFBF8AEF
        C68ABA957D566390BE8F76AD909143C6FA84A1B3E2A171526796596BCDFFFFFF
        0000FFFFFFFFFFFFB27231FFFFF1FFFFFDFFFCF3FFFBF1FFFBEFFFF8ECFFF7E9
        FFF5E6FFFFE79EA5CA6DBCF578B0E96C6ABB4AC1F16A86D05F99DE22DFFF275A
        DBFDFDFF0000FFFFFFFFFFFFAF7031FFF9E8FFF9F2FFF4E9FFF3E6FFF1E4FFF0
        E1FFF0DFFFEEDDFFF6DDF9EAD48290C279CAEE4088DB50C5F22F96E437D5FD5B
        7FA6846996FFFFFF0000FFFFFFFFFFFFAF7032FFFAE9FFF9F5FFF5EBFFF4E9FF
        F3E6FFF1E4FFF1E1FDF3E0B2B5C980A4CA7891C63670CC7AEAFF7EF7FE60FDFF
        2D9DED596CAA3F6DB05674EA0000FFFFFFFFFFFFAF7031FFFAEAFFFBF7FFF6ED
        FFF5EBFFF4E9FFF3E6FFF1E4F9F0DF6589BE7CBBE587CCED71C1EC80EAFD90FF
        FF67FCFF3CCAF329D2F91FDBFF208AE70000FFFFFFFFFFFFAF7334FFFBEDFFFC
        FAFFF7F0FFF6EDFFF5EBFFF4E9FFF2E6FFF5E5D5CFD4AFB0CA8681BA396CC675
        E0FC6EE9FD55F0FF2C9DEE6A4C9057477FA1A6F00000FFFFFFFFFFFFAF804EFF
        FFF9FFFEFCFFFAF6FFF9F3FFF7EEFFF5EBFFF4E8FFF2E6FFFCE8E7E0DA769CD0
        85D4F33562C94EC1F03177D836D0F546AACA6D5797FFFFFF0000FFFFFFFFFFFF
        AF8151FFFFFBFFFFFEFFFDF9FFFBF8FFFAF6FFF9F1FFF6ECFFF3E8FFFFEA919A
        C65BAAE37698D28B84C147C2F27E9CD58391CD1BCBFB2461E8FDFDFF0000FFFF
        FFFFFFFFAF8151FFFFFBFFFFFFFFFDFAFFFCF9FFFBF8FFFBF6FFFAF4FFF7EFFF
        F9E9DEDBDC8995C8DFDAD9B2AFD12BA8ED7EAADAFEF0DB968AA5685A95FFFFFF
        0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFDFBFFFDFBFFFCFAFFFBF8FFFBF7
        FFFAF5FFF7F1FFF8EAFFFEE9FFFCE6D8D1D94D7BD0AEB3D4FFFFEAF5C68BAA78
        50FFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFEFEFFFEFCFFFDFBFFFD
        F9FFFCF8FFFBF6FFFBF5FFF8F2FFF3E9FFF2E5FFF6E5F8ECDEFFF0DDFFF7EAEC
        BF91A2754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFFFFFFFEFDFF
        FEFCFFFDFAFFFCF9FFFBF7FFFBF7FFFAF6FFF8F3FFF5EBFFF2E5FFF5E4FFF1E0
        FFF8EDECC092A3754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFFFFFFFFFF
        FFFFFFFFFFFEFFFEFCFFFDFBFFFCF9FFFBF8FFFBF7FFFAF5FFF9F3FFF5ECFFF2
        E6FFF1E2FFFAEFECC293A3754EFFFFFF0000FFFFFFFFFFFFAF8151FFFFFBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFCFFFDFAFFFCF9FFFBF8FFFBF6FFFAF5FF
        F9F4FFF6F1FFF3EAFFFCF3ECC394A3744EFFFFFF0000FFFFFFFFFFFFAF8151FF
        FFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFCFFFDFBFFFDFAFFFBF8
        FFFBF6FFFAF5FFF9F4FFF8F2FFFFFCECCFAEA37952FFFFFF0000FFFFFFFFFFFF
        AF8151FFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFEFCFFFD
        FAFFFCF9FFFBF8FFFBF6FFFAF4FFF9F2FFFFFDECD5B9A37B54FFFFFF0000FFFF
        FFFFFFFFAF8151FFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFEFFFDFCFFFDFAFFFCF9FFFBF8FFFBF6FFF9F4FFFFFFECD3B7A37A54FFFFFF
        0000FFFFFFFFFFFFB18353FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFDFCFFFCFBFFFCF9FFFFFFEED7BDA27A
        52FFFFFF0000FFFFFFFFFFFFA97B49FFFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFDFFFFFFEB
        D1B2A07954FFFFFF0000FFFFFFFFFFFFA38263B38352BA8E61B98C5DB98C5DB9
        8C5DB98C5DB98C5DB98C5DB98C5DB98C5DB98C5DB98C5CB98C5CB98B5CB98C5B
        BB8F609C6C3DDACDC3FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = ''
      TabOrder = 4
      IconAlign = iaTop
    end
    object UniBitBtn3: TUniBitBtn
      Left = 215
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Editar'
      ShowHint = True
      Caption = ''
      TabOrder = 5
      IconAlign = iaTop
      ImageIndex = 21
    end
    object UniBitBtn4: TUniBitBtn
      Left = 278
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Anular'
      ShowHint = True
      Caption = ''
      TabOrder = 6
      IconAlign = iaTop
      ImageIndex = 50
    end
    object UniBitBtn5: TUniBitBtn
      Left = 341
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Grabar'
      ShowHint = True
      Caption = ''
      TabOrder = 7
      IconAlign = iaTop
      ImageIndex = 48
    end
    object UniBitBtn1: TUniBitBtn
      Left = 403
      Top = 2
      Width = 55
      Height = 42
      Hint = 'Cancelar'
      ShowHint = True
      Caption = ''
      TabOrder = 8
      IconAlign = iaTop
      ImageIndex = 47
    end
    object btCambiarCodigo: TUniButton
      Left = 719
      Top = 2
      Width = 94
      Height = 46
      Hint = ''
      ShowHint = True
      Caption = 'Cambiar Codigo'
      TabOrder = 9
      OnClick = btCambiarCodigoClick
    end
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 508
    Hint = ''
    ShowHint = True
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    Caption = ''
    object UniLabel1: TUniLabel
      Left = 35
      Top = 9
      Width = 47
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'N. Cliente'
      TabOrder = 1
    end
    object pCodi: TUniDBEdit
      Left = 88
      Top = 6
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'ID_CLIENTE'
      DataSource = dsCabe
      TabOrder = 2
    end
    object UniLabel2: TUniLabel
      Left = 45
      Top = 30
      Width = 37
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Nombre'
      TabOrder = 3
    end
    object UniDBEdit2: TUniDBEdit
      Left = 88
      Top = 27
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'NOMBRE'
      DataSource = dsCabe
      TabOrder = 4
    end
    object UniDBEdit4: TUniDBEdit
      Left = 88
      Top = 48
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'NOMBRE2'
      DataSource = dsCabe
      TabOrder = 5
    end
    object UniLabel3: TUniLabel
      Left = 39
      Top = 72
      Width = 43
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Direccion'
      TabOrder = 6
    end
    object UniDBEdit3: TUniDBEdit
      Left = 88
      Top = 69
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'DIRECCION'
      DataSource = dsCabe
      TabOrder = 7
    end
    object UniDBEdit5: TUniDBEdit
      Left = 88
      Top = 90
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'DIRECCION2'
      DataSource = dsCabe
      TabOrder = 8
    end
    object UniLabel6: TUniLabel
      Left = 20
      Top = 114
      Width = 62
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'C.Pos.Pobla.'
      TabOrder = 9
    end
    object UniLabel7: TUniLabel
      Left = 23
      Top = 135
      Width = 59
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'C.Pro.Provi.'
      TabOrder = 10
    end
    object UniDBEdit7: TUniDBEdit
      Left = 88
      Top = 132
      Width = 93
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'CPROVINCIA'
      DataSource = dsCabe
      TabOrder = 11
    end
    object UniLabel8: TUniLabel
      Left = 35
      Top = 156
      Width = 47
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Telefonos'
      TabOrder = 12
    end
    object UniDBEdit8: TUniDBEdit
      Left = 88
      Top = 153
      Width = 123
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'TELEFONO1'
      DataSource = dsCabe
      TabOrder = 13
    end
    object UniLabel9: TUniLabel
      Left = 58
      Top = 177
      Width = 24
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Movil'
      TabOrder = 14
    end
    object UniDBEdit9: TUniDBEdit
      Left = 88
      Top = 174
      Width = 121
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'MOVIL'
      DataSource = dsCabe
      TabOrder = 15
    end
    object UniLabel4: TUniLabel
      Left = 54
      Top = 198
      Width = 28
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'E.Mail'
      TabOrder = 16
    end
    object UniDBEdit10: TUniDBEdit
      Left = 88
      Top = 195
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'EMAIL'
      DataSource = dsCabe
      TabOrder = 17
    end
    object UniLabel5: TUniLabel
      Left = 60
      Top = 219
      Width = 22
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Web'
      TabOrder = 18
    end
    object UniDBEdit11: TUniDBEdit
      Left = 88
      Top = 216
      Width = 321
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'WEB'
      DataSource = dsCabe
      TabOrder = 19
    end
    object UniLabel10: TUniLabel
      Left = 13
      Top = 240
      Width = 69
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Mensaje Aviso'
      TabOrder = 20
    end
    object UniDBEdit12: TUniDBEdit
      Left = 88
      Top = 237
      Width = 675
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'MENSAJEAVISO'
      DataSource = dsCabe
      TabOrder = 21
    end
    object UniLabel11: TUniLabel
      Left = 239
      Top = 177
      Width = 18
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Fax'
      TabOrder = 22
    end
    object UniDBEdit13: TUniDBEdit
      Left = 261
      Top = 174
      Width = 148
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'FAX'
      DataSource = dsCabe
      TabOrder = 23
    end
    object UniDBEdit14: TUniDBEdit
      Left = 210
      Top = 153
      Width = 199
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'TELEFONO2'
      DataSource = dsCabe
      TabOrder = 24
    end
    object UniDBMemo1: TUniDBMemo
      Left = 413
      Top = 30
      Width = 350
      Height = 204
      Hint = ''
      ShowHint = True
      DataField = 'OBSERVACIONES'
      DataSource = dsCabe
      TabOrder = 25
    end
    object UniLabel13: TUniLabel
      Left = 261
      Top = 6
      Width = 29
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'N.I.F.'
      TabOrder = 26
    end
    object UniDBEdit15: TUniDBEdit
      Left = 296
      Top = 6
      Width = 113
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'NIF'
      DataSource = dsCabe
      TabOrder = 27
    end
    object UniLabel14: TUniLabel
      Left = 413
      Top = 6
      Width = 71
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Observaciones'
      TabOrder = 28
    end
    object UniGroupBox1: TUniGroupBox
      Left = 23
      Top = 265
      Width = 744
      Height = 182
      Hint = ''
      ShowHint = True
      Caption = 'Datos Facturacion'
      TabOrder = 29
      object UniDBRadioGroup1: TUniDBRadioGroup
        Left = 560
        Top = 13
        Width = 118
        Height = 104
        Hint = ''
        ShowHint = True
        DataField = 'TIVACLI'
        DataSource = dsCabe
        Caption = 'T. IVA Cliente'
        TabOrder = 1
        Items.Strings = (
          'Exento'
          'Normal'
          'R. Equiv.')
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object UniLabel12: TUniLabel
        Left = 19
        Top = 16
        Width = 95
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Periodo Facturacion'
        TabOrder = 2
      end
      object UniDBEdit17: TUniDBEdit
        Left = 173
        Top = 16
        Width = 379
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DESCRIPCION'
        DataSource = dsTFacturacio
        TabOrder = 3
      end
      object UniDBEdit19: TUniDBEdit
        Left = 173
        Top = 37
        Width = 379
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DESCRIPCION'
        DataSource = dsTPago
        TabOrder = 4
      end
      object UniLabel15: TUniLabel
        Left = 67
        Top = 38
        Width = 47
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Tipo Pago'
        TabOrder = 5
      end
      object UniDBEdit20: TUniDBEdit
        Left = 121
        Top = 58
        Width = 53
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'TDTO'
        DataSource = dsCabe
        TabOrder = 6
      end
      object UniLabel16: TUniLabel
        Left = 63
        Top = 61
        Width = 51
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Tipo Tarifa'
        TabOrder = 7
      end
      object UniDBEdit21: TUniDBEdit
        Left = 466
        Top = 61
        Width = 28
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DIAFIJO1'
        DataSource = dsCabe
        TabOrder = 8
      end
      object UniDBEdit22: TUniDBEdit
        Left = 495
        Top = 61
        Width = 28
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DIAFIJO2'
        DataSource = dsCabe
        TabOrder = 9
      end
      object UniLabel17: TUniLabel
        Left = 375
        Top = 61
        Width = 86
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Dias Fijo de pago:'
        TabOrder = 10
      end
      object UniDBEdit23: TUniDBEdit
        Left = 464
        Top = 85
        Width = 88
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'REFEPROVEEDOR'
        DataSource = dsCabe
        TabOrder = 11
      end
      object UniDBEdit24: TUniDBEdit
        Left = 524
        Top = 61
        Width = 28
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DIAFIJO3'
        DataSource = dsCabe
        TabOrder = 12
      end
      object UniLabel18: TUniLabel
        Left = 381
        Top = 85
        Width = 80
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Refe Proveedor:'
        TabOrder = 13
      end
      object UniDBEdit25: TUniDBEdit
        Left = 121
        Top = 116
        Width = 49
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'BANCO'
        DataSource = dsCabe
        TabOrder = 14
      end
      object UniDBEdit26: TUniDBEdit
        Left = 169
        Top = 116
        Width = 49
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'AGENCIA'
        DataSource = dsCabe
        TabOrder = 15
      end
      object UniLabel19: TUniLabel
        Left = 121
        Top = 97
        Width = 29
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Banco'
        TabOrder = 16
      end
      object UniDBEdit27: TUniDBEdit
        Left = 217
        Top = 116
        Width = 34
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'DC'
        DataSource = dsCabe
        TabOrder = 17
      end
      object UniLabel20: TUniLabel
        Left = 169
        Top = 97
        Width = 38
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Agencia'
        TabOrder = 18
      end
      object UniLabel21: TUniLabel
        Left = 217
        Top = 97
        Width = 14
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'DC'
        TabOrder = 19
      end
      object UniLabel22: TUniLabel
        Left = 250
        Top = 97
        Width = 53
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Cta. Banco'
        TabOrder = 20
      end
      object UniDBEdit28: TUniDBEdit
        Left = 250
        Top = 116
        Width = 124
        Height = 22
        Hint = ''
        ShowHint = True
        DataField = 'CUENTABANCO'
        DataSource = dsCabe
        TabOrder = 21
      end
      object btValidarDC: TUniButton
        Left = 380
        Top = 116
        Width = 75
        Height = 22
        Hint = ''
        ShowHint = True
        Caption = 'Validar DC'
        TabOrder = 22
      end
      object lbResuValidacion: TUniLabel
        Left = 457
        Top = 119
        Width = 79
        Height = 13
        Hint = ''
        Visible = False
        ShowHint = True
        Caption = 'lbResuValidacion'
        TabOrder = 23
      end
      object UniDBRadioGroup2: TUniDBRadioGroup
        Left = 601
        Top = 28
        Width = 168
        Height = 48
        Hint = ''
        Visible = False
        ShowHint = True
        DataField = 'SWAGRUARTIFRA'
        DataSource = dsCabe
        Caption = 'Tipo de presentacion de factura'
        TabOrder = 24
        Items.Strings = (
          'Detalle cabecera y lineas de ticket'
          'Relacion cabeceras de ticket'
          'Relacion de articulos agrupados')
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object UniDBLookupComboBox2: TUniDBLookupComboBox
        Left = 121
        Top = 16
        Width = 53
        Hint = ''
        ShowHint = True
        ListField = 'NORDEN'
        ListSource = dsTFacturacio
        KeyField = 'NORDEN'
        ListFieldIndex = 0
        DataField = 'SWTFACTURACION'
        DataSource = dsCabe
        TabOrder = 25
        Color = clWindow
      end
      object UniDBLookupComboBox3: TUniDBLookupComboBox
        Left = 121
        Top = 37
        Width = 53
        Hint = ''
        ShowHint = True
        ListField = 'ID_TPAGO'
        ListSource = dsTPago
        KeyField = 'ID_TPAGO'
        ListFieldIndex = 0
        DataField = 'TPAGO'
        DataSource = dsCabe
        TabOrder = 26
        Color = clWindow
      end
      object UniDBCheckBox1: TUniDBCheckBox
        Left = 538
        Top = 120
        Width = 167
        Height = 17
        Hint = ''
        ShowHint = True
        DataField = 'SWBLOQUEO'
        DataSource = dsCabe
        Caption = 'Bloqueado (NO FACTURAR)'
        TabOrder = 27
      end
    end
    object PanelCambiarCodigo: TUniPanel
      Left = 769
      Top = 30
      Width = 161
      Height = 145
      Hint = ''
      Visible = False
      ShowHint = True
      TabOrder = 30
      Caption = ''
      object edCodigoAntiguo: TUniEdit
        Left = 5
        Top = 4
        Width = 89
        Height = 21
        Hint = ''
        ShowHint = True
        Text = 'edCodigoAntiguo'
        TabOrder = 1
      end
      object edCodigoNuevo: TUniEdit
        Left = 5
        Top = 40
        Width = 89
        Height = 21
        Hint = ''
        ShowHint = True
        Text = 'edCodigoAntiguo'
        TabOrder = 2
      end
      object ProgressBar1: TUniProgressBar
        Left = 99
        Top = 71
        Width = 57
        Height = 17
        Hint = ''
        ShowHint = True
        TabOrder = 3
      end
      object rgTipoCambioCodigo: TUniRadioGroup
        Left = 5
        Top = 64
        Width = 89
        Height = 77
        Hint = ''
        ShowHint = True
        Items.Strings = (
          'Cambiar'
          'Unificar')
        Caption = 'Tipo'
        TabOrder = 4
      end
      object UniLabel23: TUniLabel
        Left = 6
        Top = 27
        Width = 67
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Nuevo C'#243'digo'
        TabOrder = 5
      end
      object btEjecutarCambiarCodigo: TUniBitBtn
        Left = 98
        Top = 98
        Width = 60
        Height = 41
        Hint = 'Ejecutar'
        ShowHint = True
        Caption = ''
        TabOrder = 6
        IconAlign = iaTop
        ImageIndex = 48
        OnClick = btEjecutarCambiarCodigoClick
      end
    end
    object UniDBEdit29: TUniDBEdit
      Left = 180
      Top = 111
      Width = 229
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'POBLACION'
      DataSource = dsCabe
      TabOrder = 31
    end
    object UniDBLookupComboBox1: TUniDBLookupComboBox
      Left = 88
      Top = 111
      Width = 93
      Hint = ''
      ShowHint = True
      ListField = 'CPOSTAL'
      ListSource = dsPoblacionS
      KeyField = 'CPOSTAL'
      ListFieldIndex = 0
      DataField = 'CPOSTAL'
      DataSource = dsCabe
      TabOrder = 32
      Color = clWindow
    end
    object UniDBEdit6: TUniDBEdit
      Left = 180
      Top = 132
      Width = 229
      Height = 22
      Hint = ''
      ShowHint = True
      DataField = 'PROVINCIA'
      DataSource = dsCabe
      TabOrder = 33
    end
    object cbFijarPantalla: TUniCheckBox
      Left = 783
      Top = 383
      Width = 85
      Height = 17
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Fijar'
      TabOrder = 34
    end
    object BtnAnterior: TUniBitBtn
      Left = 766
      Top = 406
      Width = 60
      Height = 41
      Hint = 'Ejecutar'
      Visible = False
      ShowHint = True
      Caption = 'Anterior'
      TabOrder = 35
      IconAlign = iaTop
      ImageIndex = 48
    end
    object BtnSiguiente: TUniBitBtn
      Left = 824
      Top = 406
      Width = 60
      Height = 41
      Hint = 'Siguiente'
      Visible = False
      ShowHint = True
      Caption = 'Siguiente'
      TabOrder = 36
      IconAlign = iaTop
      ImageIndex = 48
    end
  end
  object dsCabe: TDataSource
    DataSet = DMMenuCliente.sqlCabe
    Left = 976
    Top = 80
  end
  object dsPoblacionS: TDataSource
    DataSet = DMMenuCliente.sqlPoblacionS
    Left = 972
    Top = 136
  end
  object dsTFacturacio: TDataSource
    DataSet = DMMenuCliente.sqlTFacturacio
    Left = 972
    Top = 192
  end
  object dsTPago: TDataSource
    DataSet = DMMenuCliente.sqlTPago
    Left = 972
    Top = 240
  end
end
