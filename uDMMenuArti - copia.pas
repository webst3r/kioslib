unit uDMMenuArti;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDMMenuCliente = class(TDataModule)

  private

    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    end;

function DMMenuCliente: TDMMenuCliente;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal;

function DMMenuCliente: TDMMenuCliente;
begin
  Result := TDMMenuCliente(DMppal.GetModuleInstance(TDMMenuCliente));
end;


initialization
  RegisterModuleClass(TDMMenuCliente);





end.


