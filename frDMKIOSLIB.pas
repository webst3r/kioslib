unit frDMKIOSLIB;

interface

uses
  SysUtils, Classes, frxGradient, frxClass, frxDBSet, frxExportPDF, Data.DB,
  frxDesgn, frxCross, frxBarcode;

type
  TfrDMKioslib = class(TDataModule)
    frxPDFExport1: TfrxPDFExport;
    frxDBCabeS: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxReport1: TfrxReport;
    frxDesigner1: TfrxDesigner;
    dsCabeS: TDataSource;
    dsHisArti: TDataSource;
    frxDBDataset2: TfrxDBDataset;
    dsTerceDI: TDataSource;
    frxDBDataset3: TfrxDBDataset;
    dsLinea: TDataSource;
    frxDBEmpresa: TfrxDBDataset;
    dsCabe1Devol: TDataSource;
    frxDBLinea: TfrxDBDataset;
    dsEmpresa: TDataSource;
    frxDBCabe1: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    procedure frxReport1Preview(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    vNumInfo : String;
    function GenReportPDF(const InvNum, vTipoInforme: string): string;
    function ModiReportPDF(const InvNum: string): string;

    function GenExportPDF(const InvNum: string): string;

  end;

implementation

uses
  uDmppal, ServerModule, uDMRecepcion, uDMDevol;

{$R *.dfm}


{ TfrDM }


procedure TfrDMKioslib.frxReport1Preview(Sender: TObject);
var
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  Picture1 : TfrxPictureView;
begin
// Add the TfrxCrossView to it




{
   Memo := TfrxReportPage(frxpReport1.FindObject('Memo4'));

   Memo.Text  := 'AAAAAAAA';    //(['Binnen 1 week'], ['Q1 2005'], [0.6]);

  if (Sender is TfrxMemoView) then
     if (Sender as TfrxMemoView).Name = 'Memo4' then
  Begin
    TfrxMemoView(Sender).Memo.Text := 'AAAAAAAA';    //(['Binnen 1 week'], ['Q1 2005'], [0.6]);
  end;
}

//  frxReport1.Variables.Variables['picture1'].loadfromfile := 'C:\Users\Toni\Documents\Embarcadero\Studio\Projects\Sogemedi\Win32\Debug\files\UR\FOTOS\1\UniImage1.jpg';
//  frxReport1.Variables.Variables['Memo4'].text := 'AAAAAAAAAAAAAA';

//   Memo := frxReport1.FindObject('Memo4') as TfrxMemoView;
//   if Memo <> nil then
//   begin
//     Memo.Text := 'AAAAAAAAAAAAA';
//   end;

{
   Memo := frxReport1.FindObject('Memo4') as TfrxMemoView;
   if Memo <> nil then
   begin
     Memo.Text := 'AAAAAAAAAAAAA';
   end;
}

end;


function TfrDMKioslib.GenExportPDF(const InvNum: string): string;
var
  FFolder : string;
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  vPicture : TfrxPictureView;
  Sr : TSearchRec;
  vStrList: TStringlist;
  I, J, vFotos : Integer;
  swFinal, swFoto : Boolean;

    Exp1: TfrxPDFExport;
    AUrl : string;
    P: Integer;
    OldFName, NewFName: String;

begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode := True;
    frxReport1.EngineOptions.EnableThreadSafe := True;
    frxReport1.EngineOptions.DestroyForms := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

    //frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');
    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\' + InvNum + '.fr3');

    frxPDFExport1.Background := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog := False;
    frxPDFExport1.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath := '';

    frxReport1.PreviewOptions.AllowEdit := False;




    frxReport1.PrepareReport;
    frxReport1.Export(frxPDFExport1);



  finally



  end;

end;

function TfrDMKioslib.GenReportPDF(const InvNum, vTipoInforme: string): string;
var
  vInformeFotos : Integer;

  vInt : Integer;

  FFolder : string;
  Page : TfrxReportPage;
  Memo, MemoCabecera, MemoDetalle : TfrxMemoView;
  vPicture : TfrxPictureView;
  vPage5, vPage6, vPage6b, vPage6c : Boolean;

  vPageF1,vPageF2,vPageF3,vPageF4,vPageF5,vPageF6,vPageF7,vPageF8,vPageF9,vPageF10,
  vPageF11,vPageF12,vPageF13,vPageF14,vPageF15,vPageF16,vPageF17,vPageF18,vPageF19,vPageF20 : Boolean;

  Sr : TSearchRec;
  vStrList: TStringlist;
  I, J, vFotos : Integer;
  swFinal, swFoto : Boolean;
  vTipoInmueble : Integer;
  vTotalInteger : Integer;

DataPage: TfrxDataPage;
Band: TfrxBand;
DataBand: TfrxMasterData;

  vStringRuta : String;

begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode           := True;
    frxReport1.EngineOptions.EnableThreadSafe     := True;
    frxReport1.EngineOptions.DestroyForms         := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

//    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');
    //frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\' + vTipoInforme + '\Informe' + InvNum + '.fr3');
//    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\' + vTipoInforme + '\Informe' + InvNum + '.fr3');

  //    vStringRuta := UniServerModule.FilesFolderPath+'Informes\' + vTipoInforme + '\Informe' + InvNum + '.fr3';

    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\' + vTipoInforme + InvNum + '.fr3');

    //vStringRuta := UniServerModule.FilesFolderPath+'Informes\' + vTipoInforme + '\Informe' + InvNum + '.fr3';


    frxPDFExport1.Background   := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog   := False;
    frxPDFExport1.FileName     := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath  := '';

    frxReport1.PreviewOptions.AllowEdit := False;






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//                        Comun a todos los informes
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------




   i := 0;
   FFolder := UniServerModule.FilesFolderPath+'IMAGES\';
   if FileExists(FFolder + 'Logo' + InvNum + '.png') then
   begin
     while (i < 30) do
     begin
       i := i + 1;
       vPicture := frxReport1.FindObject('Logo' + IntToStr(i)) as TfrxPictureView;
       if vPicture <> nil then
       begin
         vPicture.picture.loadfromfile(FFolder + 'Logo' + InvNum + '.png');
       end;
     end;
   end;




    frxReport1.PrepareReport(True);



















//frxReport1.ShowPreparedReport;  //por pantalla




//------------------------------------------------------------------------------

//    frxReport1.PrepareReport;
    frxReport1.Export(frxPDFExport1);

//------------------------------------------------------------------------------

  finally
  end;
end;



function TfrDMKioslib.ModiReportPDF(const InvNum: string): string;
var
  FFolder : string;
  Page : TfrxReportPage;
  Memo : TfrxMemoView;
  vPicture : TfrxPictureView;
begin
  try
    frxReport1.PrintOptions.ShowDialog := False;
    frxReport1.ShowProgress := false;

    frxReport1.EngineOptions.SilentMode := True;
    frxReport1.EngineOptions.EnableThreadSafe := True;
    frxReport1.EngineOptions.DestroyForms := False;
    frxReport1.EngineOptions.UseGlobalDataSetList := False;

    //frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\Informe' + InvNum + '.fr3');
    frxReport1.LoadFromFile(UniServerModule.FilesFolderPath+'Informes\' + InvNum + '.fr3');

    frxPDFExport1.Background := True;
    frxPDFExport1.ShowProgress := False;
    frxPDFExport1.ShowDialog := False;
    frxPDFExport1.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', Result, True);
    frxPDFExport1.DefaultPath := '';

    frxReport1.PreviewOptions.AllowEdit := False;

//VARIABLES
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

    frxReport1.DesignReport;
  finally
  end;
end;


end.
