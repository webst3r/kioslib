unit uFichaArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniDBCheckBox,
  uniImageList;

type
  TFormManteArti = class(TUniForm)
    tmSession: TUniTimer;
    UniPanel1: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniLabel6: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel11: TUniLabel;
    UniLabel14: TUniLabel;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    UniLabel18: TUniLabel;
    UniLabel19: TUniLabel;
    UniLabel20: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    edTipoProducto: TUniDBLookupComboBox;
    edContenido: TUniDBLookupComboBox;
    edTematica: TUniDBLookupComboBox;
    edAutor: TUniDBLookupComboBox;
    edEditor: TUniDBLookupComboBox;
    edPeriodicidad: TUniDBLookupComboBox;
    UniPageControl1: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniTabSheet2: TUniTabSheet;
    UniLabel9: TUniLabel;
    UniLabel10: TUniLabel;
    UniLabel15: TUniLabel;
    UniDBLookupComboBox8: TUniDBLookupComboBox;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniDBCheckBox1: TUniDBCheckBox;
    UniDBCheckBox2: TUniDBCheckBox;
    UniDBLookupComboBox9: TUniDBLookupComboBox;
    UniDBLookupComboBox10: TUniDBLookupComboBox;
    UniBitBtn14: TUniBitBtn;
    UniButton1: TUniButton;
    UniButton2: TUniButton;
    UniEdit2: TUniEdit;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    DbText: TUniDBText;
    UniPanel2: TUniPanel;
    UniEdit3: TUniEdit;
    UniLabel13: TUniLabel;
    UniPageControl2: TUniPageControl;
    UniTabSheet3: TUniTabSheet;
    UniTabSheet4: TUniTabSheet;
    UniPanel3: TUniPanel;
    UniTabSheet5: TUniTabSheet;
    UniTabSheet6: TUniTabSheet;
    UniTabSheet7: TUniTabSheet;
    UniTabSheet8: TUniTabSheet;
    UniLabel12: TUniLabel;
    UniDBRadioGroup1: TUniDBRadioGroup;
    UniLabel21: TUniLabel;
    UniLabel22: TUniLabel;
    UniLabel23: TUniLabel;
    UniLabel24: TUniLabel;
    UniLabel25: TUniLabel;
    UniLabel26: TUniLabel;
    UniLabel27: TUniLabel;
    UniLabel28: TUniLabel;
    UniLabel29: TUniLabel;
    UniDBEdit9: TUniDBEdit;
    UniDBEdit10: TUniDBEdit;
    UniDBLookupComboBox11: TUniDBLookupComboBox;
    UniDBEdit11: TUniDBEdit;
    UniDBEdit12: TUniDBEdit;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBCheckBox3: TUniDBCheckBox;
    UniDBCheckBox4: TUniDBCheckBox;
    UniDBCheckBox5: TUniDBCheckBox;
    UniDBCheckBox6: TUniDBCheckBox;
    UniDBCheckBox7: TUniDBCheckBox;
    PageControl: TUniPageControl;
    UniDBMemo1: TUniDBMemo;
    UniDBGrid1: TUniDBGrid;
    UniDBNavigator1: TUniDBNavigator;
    GridPreus: TUniDBGrid;
    btTecNume: TUniBitBtn;
    edPreu: TUniDBEdit;
    edPreu2: TUniDBEdit;
    edMargen: TUniDBEdit;
    edMargen2: TUniDBEdit;
    edEncarte1: TUniDBEdit;
    UniLabel69: TUniLabel;
    UniLabel70: TUniLabel;
    UniLabel71: TUniLabel;
    edTIva1: TUniDBEdit;
    UniDBEdit79: TUniDBEdit;
    UniDBEdit80: TUniDBEdit;
    UniDBEdit83: TUniDBEdit;
    UniDBEdit84: TUniDBEdit;
    UniDBEdit82: TUniDBEdit;
    UniLabel73: TUniLabel;
    UniLabel74: TUniLabel;
    UniLabel75: TUniLabel;
    UniLabel76: TUniLabel;
    UniLabel77: TUniLabel;
    UniSpeedButton34: TUniSpeedButton;
    UniDBEdit15: TUniDBEdit;
    UniLabel30: TUniLabel;
    UniLabel31: TUniLabel;
    UniLabel32: TUniLabel;
    UniLabel33: TUniLabel;
    UniLabel34: TUniLabel;
    UniLabel35: TUniLabel;
    UniLabel36: TUniLabel;
    UniDBEdit16: TUniDBEdit;
    UniDBEdit17: TUniDBEdit;
    UniDBEdit18: TUniDBEdit;
    UniDBEdit20: TUniDBEdit;
    UniDBEdit21: TUniDBEdit;
    UniDBEdit22: TUniDBEdit;
    UniDBEdit23: TUniDBEdit;
    UniDBEdit24: TUniDBEdit;
    UniDBEdit26: TUniDBEdit;
    UniDBEdit27: TUniDBEdit;
    UniBitBtn15: TUniBitBtn;
    UniLabel37: TUniLabel;
    UniLabel38: TUniLabel;
    UniEdit4: TUniEdit;
    UniImage1: TUniImage;
    UniLabel39: TUniLabel;
    UniDBEdit19: TUniDBEdit;
    UniCheckBox1: TUniCheckBox;
    UniBitBtn16: TUniBitBtn;
    UniBitBtn17: TUniBitBtn;
    dsFichaArticulo: TDataSource;
    UniDBEdit25: TUniDBEdit;
    UniDBEdit28: TUniDBEdit;
    UniDBRadioGroup2: TUniDBRadioGroup;
    UniDBRadioGroup3: TUniDBRadioGroup;
    dsEditorialS: TDataSource;
    dsArTitulo: TDataSource;
    dsPreus: TDataSource;
    dsVaris: TDataSource;
    dsVarisSubGrupo: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    UniPanel24: TUniPanel;
    UniBitBtn24: TUniBitBtn;
    BtnEditar: TUniBitBtn;
    UniBitBtn30: TUniBitBtn;
    btBuscar: TUniBitBtn;
    UniPanel26: TUniPanel;
    pnlopciones: TUniPanel;
    BtnGrabar: TUniBitBtn;
    BtnCancelar: TUniBitBtn;
    btOpciones: TUniBitBtn;
    btConsu: TUniBitBtn;
    btLista: TUniBitBtn;
    UniBitBtn18: TUniBitBtn;
    UniPanel4: TUniPanel;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn6: TUniBitBtn;
    UniBitBtn7: TUniBitBtn;
    UniBitBtn8: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    UniBitBtn10: TUniBitBtn;
    UniBitBtn11: TUniBitBtn;
    UniBitBtn12: TUniBitBtn;
    UniBitBtn13: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    pnlBtsGrabarCanc: TUniPanel;
    btGrabar1: TUniBitBtn;
    btCancelar1: TUniBitBtn;
    procedure UniDBRadioGroup3Click(Sender: TObject);
    procedure edContenidoEnter(Sender: TObject);
    procedure edTematicaEnter(Sender: TObject);
    procedure edAutorEnter(Sender: TObject);
    procedure edEditorEnter(Sender: TObject);
    procedure edTipoProductoEnter(Sender: TObject);
    procedure edPeriodicidadEnter(Sender: TObject);
    procedure btListaClick(Sender: TObject);
    procedure btConsuClick(Sender: TObject);
    procedure btOpcionesClick(Sender: TObject);
    procedure BtnEditarClick(Sender: TObject);
    procedure btGrabar1Click(Sender: TObject);
    procedure btCancelar1Click(Sender: TObject);
    procedure UniBitBtn18Click(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormManteArti: TFormManteArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uMenuArti, uListaArti, uConsArti, uDMMenuArti;

function FormManteArti: TFormManteArti;
begin
  Result := TFormManteArti(DMppal.GetFormInstance(TFormManteArti));

end;


procedure TFormManteArti.edTematicaEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TTEMA');
end;

procedure TFormManteArti.edTipoProductoEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TPRODU');
end;



procedure TFormManteArti.btConsuClick(Sender: TObject);
begin
  FormMenuArti.RutAbrirConsArti;
end;

procedure TFormManteArti.btListaClick(Sender: TObject);
begin
  FormMenuArti.pcDetalle.ActivePage := FormMenuArti.tabListaArti;
end;

procedure TFormManteArti.btGrabar1Click(Sender: TObject);
begin
  pnlBtsGrabarCanc.Visible := False;
end;

procedure TFormManteArti.btCancelar1Click(Sender: TObject);
begin
  pnlBtsGrabarCanc.Visible := False;
end;

procedure TFormManteArti.BtnEditarClick(Sender: TObject);
begin
  pnlBtsGrabarCanc.Visible := True;
end;

procedure TFormManteArti.btOpcionesClick(Sender: TObject);
begin
  pnlopciones.Visible := not pnlopciones.Visible;
end;

procedure TFormManteArti.edAutorEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TAUTOR');
end;

procedure TFormManteArti.edContenidoEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TCONTENI');
end;

procedure TFormManteArti.edEditorEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TEDITOR');
end;

procedure TFormManteArti.edPeriodicidadEnter(Sender: TObject);
begin
  DMMenuArti.RutActivarVaris_A('TPERIODI');
end;

procedure TFormManteArti.UniBitBtn18Click(Sender: TObject);
begin
  FormMenuArti.Close;
  FormMenu.lbFormNombre.Caption := '';
end;

procedure TFormManteArti.UniDBRadioGroup3Click(Sender: TObject);
var
wItem : Integer;
begin
  wItem := UniDBRadioGroup3.ItemIndex;
  DMMenuArti.RutAbrirPreus(wItem);
end;

end.

