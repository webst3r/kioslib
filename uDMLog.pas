unit uDMLog;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDMLog = class(TDataModule)
    sqlLog: TFDQuery;
    sqlLogID: TIntegerField;
    sqlLogUSUARIO: TStringField;
    sqlLogFECHA: TDateField;
    sqlLogHORAINICIO: TTimeField;
    sqlLogACCION: TIntegerField;
    sqlLogID_CONTROL: TIntegerField;
    sqlLogTITULO: TStringField;
    sqlLogUsuariosAcciones: TFDQuery;
    sqlLogUsuariosAccionesUSUARIO: TStringField;
    sqlLogUsuariosAccionesACCION: TIntegerField;
    sqlLogUsuariosAccionesTITULO: TStringField;
    sqlLogUsuariosAccionesTOTAL: TIntegerField;
    sqlLogAcciones: TFDQuery;
    sqlLogAccionesACCION: TIntegerField;
    sqlLogAccionesTITULO: TStringField;
    sqlLogAccionesTOTAL: TIntegerField;

  private

    { Private declarations }
  public
    { Public declarations }
    vID : Integer;

    procedure RutAbrirLogs(vDesde, vHasta: tdate);

    end;

function DMLog: TDMLog;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uLog;

function DMLog: TDMLog;
begin
  Result := TDMLog(DMppal.GetModuleInstance(TDMLog));
end;






//INICI
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
procedure TDMLog.RutAbrirLogs(vDesde : Tdate; vHasta : tdate);
var
  vWhere : string;
begin

  vWhere := '  between ' + QuotedStr( FormatDateTime('mm/dd/yyyy', vDesde) )
         +  ' and '      + QuotedStr( FormatDateTime('mm/dd/yyyy', vHasta) );

 if FormLog.pcDetalle.ActivePage = FormLog.tabListaLog then
 begin
  with DMLog.sqlLog do
  begin
    Close;
    DMLog.sqlLog.sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE FL.FECHA', Uppercase(SQL.Text))-1)
                 + 'WHERE FL.FECHA ' + vWhere
                 + ' ORDER BY 1,2,3,4';

    Open;
  end;

 end;

 if FormLog.pcDetalle.ActivePage = FormLog.tabLogporUsuarios then
 begin
  with DMLog.sqlLogUsuariosAcciones do
  begin
    Close;
    DMLog.sqlLogUsuariosAcciones.sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE FL.FECHA', Uppercase(SQL.Text))-1)
                 + 'WHERE FL.FECHA ' + vWhere
                 + ' GROUP BY FL.usuario, FL.accion, FLI.titulo '
                 + ' ORDER BY 1,2';

    Open;
  end;
 end;

 if FormLog.pcDetalle.ActivePage = FormLog.tabLogAcciones then
 begin

  with DMLog.sqlLogAcciones do
  begin
    Close;
    DMLog.sqlLogAcciones.sql.Text := Copy(SQL.Text, 1,
                    pos('WHERE FL.FECHA', Uppercase(SQL.Text))-1)
                 + 'WHERE FL.FECHA ' + vWhere
                 + ' GROUP BY FL.accion, FLI.titulo '
                 + ' ORDER BY 1,2';

    Open;
  end;

 end;
end;

//TIMER
//------------------------------------------------------------------------------







//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



initialization
  RegisterModuleClass(TDMLog);





end.


