object FormDistribuidoraFicha: TFormDistribuidoraFicha
  Left = 0
  Top = 0
  ClientHeight = 506
  ClientWidth = 1008
  Caption = ''
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  NavigateKeys.Enabled = True
  NavigateKeys.Next.Key = 13
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFicha: TUniPanel
    Left = 0
    Top = 55
    Width = 1008
    Height = 451
    Hint = ''
    ShowHint = True
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    BorderStyle = ubsNone
    Caption = ''
    ExplicitTop = 48
    ExplicitHeight = 510
    object UniLabel1: TUniLabel
      Left = 59
      Top = 8
      Width = 39
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Codigo'
      ParentFont = False
      Font.Height = -13
      TabOrder = 22
    end
    object pCodi: TUniDBEdit
      Left = 104
      Top = 6
      Width = 121
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'ID_PROVEEDOR'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 0
      ReadOnly = True
    end
    object UniLabel2: TUniLabel
      Left = 53
      Top = 36
      Width = 45
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Nombre'
      ParentFont = False
      Font.Height = -13
      TabOrder = 23
    end
    object UniDBEdit2: TUniDBEdit
      Left = 104
      Top = 32
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'NOMBRE'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 2
    end
    object UniDBEdit4: TUniDBEdit
      Left = 104
      Top = 58
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'NOMBRE2'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 3
    end
    object UniLabel3: TUniLabel
      Left = 46
      Top = 94
      Width = 52
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Direccion'
      ParentFont = False
      Font.Height = -13
      TabOrder = 24
    end
    object UniDBEdit3: TUniDBEdit
      Left = 104
      Top = 85
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'DIRECCION'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 4
    end
    object UniDBEdit5: TUniDBEdit
      Left = 104
      Top = 111
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'DIRECCION2'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 5
    end
    object UniLabel6: TUniLabel
      Left = 27
      Top = 140
      Width = 71
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'C.Pos.Pobla.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 25
    end
    object UniLabel7: TUniLabel
      Left = 559
      Top = 414
      Width = 67
      Height = 16
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'C.Pro.Provi.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 26
    end
    object UniDBEdit7: TUniDBEdit
      Left = 632
      Top = 412
      Width = 93
      Height = 24
      Hint = ''
      Visible = False
      ShowHint = True
      DataField = 'CPROVINCIA'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 7
    end
    object UniLabel8: TUniLabel
      Left = 42
      Top = 168
      Width = 56
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Telefonos'
      ParentFont = False
      Font.Height = -13
      TabOrder = 27
    end
    object UniDBEdit8: TUniDBEdit
      Left = 104
      Top = 166
      Width = 121
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'TELEFONO1'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 9
    end
    object UniLabel9: TUniLabel
      Left = 69
      Top = 194
      Width = 29
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Movil'
      ParentFont = False
      Font.Height = -13
      TabOrder = 28
    end
    object UniDBEdit9: TUniDBEdit
      Left = 104
      Top = 192
      Width = 121
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'MOVIL'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 11
    end
    object UniLabel4: TUniLabel
      Left = 64
      Top = 221
      Width = 34
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'E.Mail'
      ParentFont = False
      Font.Height = -13
      TabOrder = 29
    end
    object UniDBEdit10: TUniDBEdit
      Left = 104
      Top = 219
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'EMAIL'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 13
    end
    object UniLabel5: TUniLabel
      Left = 72
      Top = 247
      Width = 26
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Web'
      ParentFont = False
      Font.Height = -13
      TabOrder = 30
    end
    object UniDBEdit11: TUniDBEdit
      Left = 104
      Top = 245
      Width = 321
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'WEB'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 14
    end
    object UniLabel10: TUniLabel
      Left = 9
      Top = 274
      Width = 89
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'D'#237'as Devoluci'#243'n'
      ParentFont = False
      Font.Height = -13
      TabOrder = 31
    end
    object UniDBEdit12: TUniDBEdit
      Left = 104
      Top = 272
      Width = 87
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'DIAFIJO3'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 15
    end
    object UniLabel11: TUniLabel
      Left = 255
      Top = 194
      Width = 20
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Fax'
      ParentFont = False
      Font.Height = -13
      TabOrder = 32
    end
    object UniDBEdit13: TUniDBEdit
      Left = 277
      Top = 192
      Width = 148
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'FAX'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 12
    end
    object UniDBEdit14: TUniDBEdit
      Left = 224
      Top = 166
      Width = 201
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'TELEFONO2'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 10
    end
    object UniDBMemo1: TUniDBMemo
      Left = 434
      Top = 32
      Width = 350
      Height = 317
      Hint = ''
      ShowHint = True
      DataField = 'OBSERVACIONES'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 33
    end
    object UniLabel13: TUniLabel
      Left = 277
      Top = 8
      Width = 31
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'N.I.F.'
      ParentFont = False
      Font.Height = -13
      TabOrder = 34
    end
    object pNif: TUniDBEdit
      Left = 312
      Top = 6
      Width = 113
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'NIF'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 1
    end
    object UniLabel14: TUniLabel
      Left = 434
      Top = 9
      Width = 83
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Observaciones'
      ParentFont = False
      Font.Height = -13
      TabOrder = 35
    end
    object PanelCambiarCodigo: TUniPanel
      Left = 844
      Top = 5
      Width = 161
      Height = 145
      Hint = ''
      Visible = False
      ShowHint = True
      TabOrder = 36
      Caption = ''
      object edCodigoAntiguo: TUniEdit
        Left = 5
        Top = 4
        Width = 89
        Height = 21
        Hint = ''
        ShowHint = True
        Text = 'edCodigoAntiguo'
        TabOrder = 1
      end
      object edCodigoNuevo: TUniEdit
        Left = 5
        Top = 40
        Width = 89
        Height = 21
        Hint = ''
        ShowHint = True
        Text = 'edCodigoAntiguo'
        TabOrder = 2
      end
      object ProgressBar1: TUniProgressBar
        Left = 99
        Top = 71
        Width = 57
        Height = 17
        Hint = ''
        ShowHint = True
        TabOrder = 3
      end
      object rgTipoCambioCodigo: TUniRadioGroup
        Left = 5
        Top = 64
        Width = 89
        Height = 77
        Hint = ''
        ShowHint = True
        Items.Strings = (
          'Cambiar'
          'Unificar')
        Caption = 'Tipo'
        TabOrder = 4
      end
      object UniLabel23: TUniLabel
        Left = 6
        Top = 27
        Width = 67
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Nuevo C'#243'digo'
        TabOrder = 5
      end
      object btEjecutarCambiarCodigo: TUniBitBtn
        Left = 98
        Top = 98
        Width = 60
        Height = 41
        Hint = 'Ejecutar'
        ShowHint = True
        Caption = ''
        TabOrder = 6
        IconAlign = iaTop
        ImageIndex = 48
        OnClick = btEjecutarCambiarCodigoClick
      end
    end
    object UniDBEdit29: TUniDBEdit
      Left = 196
      Top = 138
      Width = 229
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'POBLACION'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 37
    end
    object pCPostalE: TUniDBLookupComboBox
      Left = 104
      Top = 138
      Width = 93
      Height = 24
      Hint = ''
      ShowHint = True
      ListField = 'CPOSTAL'
      ListSource = dsPoblacionS
      KeyField = 'CPOSTAL'
      ListFieldIndex = 0
      DataField = 'CPOSTAL'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      AnyMatch = True
      TabOrder = 6
      Color = clWindow
      Style = csDropDown
      OnCloseUp = pCPostalECloseUp
    end
    object UniDBEdit6: TUniDBEdit
      Left = 724
      Top = 412
      Width = 229
      Height = 24
      Hint = ''
      Visible = False
      ShowHint = True
      DataField = 'PROVINCIA'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 8
      Color = clGradientActiveCaption
    end
    object UniLabel24: TUniLabel
      Left = 11
      Top = 300
      Width = 87
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Punto de Venta'
      ParentFont = False
      Font.Height = -13
      TabOrder = 38
    end
    object UniDBEdit1: TUniDBEdit
      Left = 104
      Top = 298
      Width = 87
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'REFEPROVEEDOR'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 16
    end
    object UniLabel25: TUniLabel
      Left = 72
      Top = 327
      Width = 26
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Ruta'
      ParentFont = False
      Font.Height = -13
      TabOrder = 39
    end
    object UniDBEdit16: TUniDBEdit
      Left = 104
      Top = 325
      Width = 87
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'RUTA'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 17
    end
    object UniLabel26: TUniLabel
      Left = 200
      Top = 327
      Width = 63
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'D'#237'a Abono:'
      ParentFont = False
      Font.Height = -13
      TabOrder = 40
    end
    object UniDBEdit18: TUniDBEdit
      Left = 264
      Top = 325
      Width = 48
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'DIAFIJO2'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 20
    end
    object UniLabel27: TUniLabel
      Left = 215
      Top = 274
      Width = 48
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'Margen:'
      ParentFont = False
      Font.Height = -13
      TabOrder = 41
    end
    object UniDBEdit30: TUniDBEdit
      Left = 264
      Top = 272
      Width = 48
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'TDTO'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 18
    end
    object UniLabel28: TUniLabel
      Left = 202
      Top = 300
      Width = 61
      Height = 16
      Hint = ''
      ShowHint = True
      Caption = 'D'#237'a Cargo:'
      ParentFont = False
      Font.Height = -13
      TabOrder = 42
    end
    object UniDBEdit31: TUniDBEdit
      Left = 264
      Top = 298
      Width = 48
      Height = 24
      Hint = ''
      ShowHint = True
      DataField = 'DIAFIJO1'
      DataSource = dsCabe
      ParentFont = False
      Font.Height = -13
      TabOrder = 19
    end
    object edDiaSemanaCargo: TUniComboBox
      Left = 313
      Top = 298
      Width = 112
      Height = 24
      Hint = ''
      ShowHint = True
      Text = ''
      Items.Strings = (
        'Lunes'
        'Martes'
        'Miercoles'
        'Jueves'
        'Viernes'
        'Sabado'
        'Domingo')
      ParentFont = False
      Font.Height = -13
      TabOrder = 43
      OnCloseUp = edDiaSemanaCargoCloseUp
    end
    object edDiaSemanaAbono: TUniComboBox
      Left = 313
      Top = 325
      Width = 112
      Height = 24
      Hint = ''
      ShowHint = True
      Text = ''
      Items.Strings = (
        'Lunes'
        'Martes'
        'Miercoles'
        'Jueves'
        'Viernes'
        'Sabado'
        'Domingo')
      ParentFont = False
      Font.Height = -13
      TabOrder = 44
      OnCloseUp = edDiaSemanaAbonoCloseUp
    end
    object PanelBajaArticulos: TUniPanel
      Left = 844
      Top = 150
      Width = 161
      Height = 145
      Hint = ''
      Visible = False
      ShowHint = True
      TabOrder = 45
      Caption = ''
      object UniLabel29: TUniLabel
        Left = 3
        Top = 61
        Width = 57
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha Baja:'
        TabOrder = 1
      end
      object UniBitBtn1: TUniBitBtn
        Left = 34
        Top = 101
        Width = 60
        Height = 41
        Hint = 'Ejecutar'
        ShowHint = True
        Caption = ''
        TabOrder = 2
        IconAlign = iaTop
        ImageIndex = 48
        OnClick = btEjecutarCambiarCodigoClick
      end
      object edFechaBajaCompra: TUniDBDateTimePicker
        Left = 3
        Top = 35
        Width = 120
        Hint = ''
        ShowHint = True
        DateTime = 43419.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 3
      end
      object UniLabel30: TUniLabel
        Left = 3
        Top = 20
        Width = 69
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'Fecha Compra'
        TabOrder = 4
      end
      object UniLabel31: TUniLabel
        Left = 3
        Top = 3
        Width = 149
        Height = 13
        Hint = ''
        ShowHint = True
        Caption = 'No tengan Stock y anteriores a'
        TabOrder = 5
      end
      object edFechaBajanueva: TUniDBDateTimePicker
        Left = 3
        Top = 76
        Width = 120
        Hint = ''
        ShowHint = True
        DateTime = 43419.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
      end
    end
  end
  object pnlBts: TUniPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 55
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Caption = ''
    object UniBitBtn3: TUniBitBtn
      Left = 759
      Top = 7
      Width = 55
      Height = 42
      Hint = 'Duplicar'
      Visible = False
      ShowHint = True
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
        DAD8D7A8A3A1A7A2A0A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3A1A8A3
        A1A8A3A1A7A19FABA8A7E3E3E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFBC6D3DC75409C2540EC2560FC15711C15913C15B16C15D18C05E1CC0
        611FC06323C06626CA6E2B975B33B8B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFD66627F4B690EEC8B7F2CEBAF5D2C0F6D4C3F8D8C5F9DAC7
        FBDBC7FDDCC8FEDDC9FEE0CDFFD5B6BA6E3BB6B4B4FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFCD6833EAD1C4E2E6F0DDD4D5DDD1D0E1D7D5E4DC
        D7E7DED9EADFD9EDE0DBEDE0DBEFE3DEEED4C4A95E317C6F699A928EA29E9DE9
        E9E9FFFFFFFFFFFF0000FFFFFFFFFFFFCB6832EED1C2DBCFCFCE6123E05F19DC
        631CDC651CDC671DDC6A1EDC6D21DC6E25DD722ADD7229D66C24CD6926D87630
        905B39CBCBCBFFFFFFFFFFFF0000FFFFFFFFFFFFC96833F0D7C9DCC9C6D45C16
        E5AC8FE2B9A6E7BEA9E8C2ADEAC5AFECC9B1EDC9B2EFCBB3F1CCB5F5D3BEFAD7
        C3FFC198A8683DCBCBCBFFFFFFFFFFFF0000FFFFFFFFFFFFC66834F2DBCCDECD
        C9D16727E6D6D1E3E5EEDFD7D7E1D7D6E5DCDBE8E1DDEAE2DEEEE4DFF0E5E1F0
        E6E2F2EAE7EDBFA69A5730847C799C9593B3B1B10000FFFFFFFFFFFFC66834F5
        DED0E2D1CED16526EAD8D2DAC4BCD25D1ADA5C18D8611AD8621BD9631BD8661C
        D8691FD86C23D86E28D86C24D16621C96524D5712E8667540000FFFFFFFFFFFF
        C46736F7E2D3E2D3D0CF6628EDE0DBDCBAABDA6B25E0B29DDEB4A0E1B7A1E2B9
        A5E4BCA8E5BFABE8C1AEEAC3AFEBC7B1EFCBB6F3CEB9FAB180926C530000FFFF
        FFFFFFFFC26739F9E4D5E4D5D3CE672BF0E4DFDDBDAEDA7A3FE5E1E5E4E3E9E9
        E8EDECECF1EFF1F6F3F6FAF6F9FEF9FEFFFBFFFFFDFFFFFFFFFFF9C6A58E6952
        0000FFFFFFFFFFFFBF663BFBEEE1E7E1E3CD682EF3E6E1DFC0B2D9783DE8DDDB
        E7DFE0EBE3E3EEE8E7F1ECEBF4F0EFF7F3F3F9F6F6FBF9F9FDFDFDFFFFFFF8C3
        A18E6A550000FFFFFFFFFFFFBF673BF3B78ADBA286CA662EF5ECE7E0C3B6D87A
        40EBE1E0EBE5E7EEE9E9F1ECECF4F0F0F7F4F4FAF7F7FBFAFAFDFCFCFFFFFFFF
        FFFFF7C5A58C6B560000FFFFFFFFFFFFC56B3AFFA13BED7925CA652DF8F1ECE2
        C5B7D87A43EFE7E5EEEAECF2EDEDF4F0F0F7F3F3F9F6F6FCF9F9FDFCFCFEFEFE
        FFFFFFFFFFFFF8C6A78C6C590000FFFFFFFFFFFFB57666C9825BB86B49C66734
        FDFCF8E3CFC8D77B45F1EAE9F1EEEFF4F0F0F6F3F3F9F6F6FBF9F9FDFCFCFEFD
        FDFFFFFFFFFFFFFFFFFFF7C6AA8C6B5B0000FFFFFFFFFFFFF7F2F2F0E8E7DDCB
        CAC46430F1B58AD68E69D67D48F4EFEEF4F1F3F7F3F3F9F5F6FCF9F9FDFBFBFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFF6C8AB8A6C5D0000FFFFFFFFFFFFFFFFFFFF
        FFFFE9DFDFD06E2FFFA33DEB7521D57E4CF6F2F2F6F5F6F9F6F6FBF9F9FDFBFB
        FEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7C8AD896C5F0000FFFFFFFFFFFF
        FFFFFFFFFFFFEFE6E6AD624DBA7451B15C38D58152FBFDFDFAFFFFFDFFFFFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CEB7896C600000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF6F1F0F2EBEBD5BBB7D3743DF1B894EDB897EE
        B696EEB796EFB796EEB796EEB796EEB796EEB796EEB896EFB794F2AA7F887067
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDC7C5E38134FF9C38
        FF9639FF9A3EFF9D44FFA049FFA44FFFA755FFAB5AFFAF60FFB266FFB769FFCC
        8B8B77720000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5D6D5B56D
        50BF7A53BE7A59BE7C5DBE7F62BE8166BE846ABE876FBF8974BE8A77BD8977BD
        8D7EBF8C7FA998980000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      Caption = ''
      TabOrder = 1
    end
    object btEditarDistribuidora: TUniBitBtn
      Left = 117
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Modificar'
      ShowHint = True
      Caption = ''
      TabOrder = 2
      Images = DMppal.ImgListPrincipal
      ImageIndex = 3
      OnClick = btEditarDistribuidoraClick
    end
    object btGrabarDistribuidora: TUniBitBtn
      Left = 172
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Aplicar'
      ShowHint = True
      Caption = ''
      TabOrder = 3
      Images = DMppal.ImgListPrincipal
      ImageIndex = 38
      OnClick = btGrabarDistribuidoraClick
    end
    object btCancelarDistribuidora: TUniBitBtn
      Left = 227
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Cancelar'
      ShowHint = True
      Caption = ''
      TabOrder = 4
      Images = DMppal.ImgListPrincipal
      ImageIndex = 39
      OnClick = btCancelarDistribuidoraClick
    end
    object btBorrarDistribuidora: TUniBitBtn
      Left = 62
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Anular'
      ShowHint = True
      Caption = ''
      TabOrder = 5
      Images = DMppal.ImgListPrincipal
      ImageIndex = 35
      OnClick = btBorrarDistribuidoraClick
    end
    object UniBitBtn12: TUniBitBtn
      Left = 813
      Top = 7
      Width = 55
      Height = 42
      Hint = 'Crear Tipos'
      Visible = False
      ShowHint = True
      Caption = 'Crear'
      TabOrder = 6
    end
    object UniBitBtn13: TUniBitBtn
      Left = 867
      Top = 7
      Width = 55
      Height = 42
      Hint = 'Selec Tipos'
      Visible = False
      ShowHint = True
      Caption = 'Tipos'
      TabOrder = 7
    end
    object UniBitBtn14: TUniBitBtn
      Left = 922
      Top = 7
      Width = 55
      Height = 42
      Hint = ''
      Visible = False
      ShowHint = True
      Caption = 'Etiquetas'
      TabOrder = 8
    end
    object btNuevaDistribuidora: TUniBitBtn
      Left = 7
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Nueva Distribuidora'
      ShowHint = True
      Caption = ''
      TabOrder = 9
      Images = DMppal.ImgListPrincipal
      ImageIndex = 34
      OnClick = btNuevaDistribuidoraClick
    end
    object btAnterior: TUniBitBtn
      Left = 327
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Anterior'
      ShowHint = True
      Caption = ''
      TabOrder = 10
      Images = DMppal.ImgListPrincipal
      ImageIndex = 36
      OnClick = btAnteriorClick
    end
    object btSiguiente: TUniBitBtn
      Left = 386
      Top = 3
      Width = 55
      Height = 42
      Hint = 'Siguiente'
      ShowHint = True
      Caption = ''
      TabOrder = 11
      Images = DMppal.ImgListPrincipal
      ImageIndex = 37
      OnClick = btSiguienteClick
    end
  end
  object dsCabe: TDataSource
    DataSet = DMDistribuidora.sqlCabe
    Left = 968
    Top = 80
  end
  object dsPoblacionS: TDataSource
    DataSet = DMDistribuidora.sqlPoblacionS
    Left = 948
    Top = 136
  end
  object dsTFacturacio: TDataSource
    DataSet = DMCliente.sqlTFacturacio
    Left = 972
    Top = 200
  end
  object dsTPago: TDataSource
    DataSet = DMCliente.sqlTPago
    Left = 972
    Top = 240
  end
end
