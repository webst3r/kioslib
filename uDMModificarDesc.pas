unit uDMModificarDesc;

interface

uses
  SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI;

type
  TDMModificarDesc = class(TDataModule)
    sqlArticulos: TFDQuery;
    sqlArticulosDESCRIPCION: TStringField;
    sqlArticulosREFEPROVE: TStringField;
  
  private







    { Private declarations }
  public
   // procedure RutAbrirHisArti(vID: Integer);
       procedure RutAbrirArti(vID: Integer);
    { Public declarations }


    end;

function DMModificarDesc: TDMModificarDesc;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, uDMPpal, uMenu, uHistoArti;

function DMModificarDesc: TDMModificarDesc;
begin
  Result := TDMModificarDesc(DMppal.GetModuleInstance(TDMModificarDesc));
end;

 {
procedure TDMModificarDesc.RutAbrirHisArti(vID: Integer);
begin
  with sqlHisArti do
  begin
    close;
    SQL.Text  := Copy(SQL.Text, 1,
                    pos('WHERE H.ID_HISARTI', Uppercase(SQL.Text))-1)
                 + 'WHERE H.ID_HISARTI = ' + IntToStr(vID);

    Open;


  end;
end;

        }
procedure TDMModificarDesc.RutAbrirArti(vID: Integer);
begin
  with sqlArticulos do
  begin
    close;
    SQL.Text  := Copy(SQL.Text, 1,
                    pos('WHERE ID_ARTICULO', Uppercase(SQL.Text))-1)
                 + 'WHERE ID_ARTICULO = ' + IntToStr(vID);

    Open;


  end;
end;

initialization
  RegisterModuleClass(TDMModificarDesc);





end.


