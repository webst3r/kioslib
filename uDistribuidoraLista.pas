unit uDistribuidoraLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, Vcl.Menus,
  uniMainMenu;

type
  TFormDistribuidoraLista = class(TUniForm)
    gridDistribuidoraLista: TUniDBGrid;
    dsCabeS: TDataSource;
    pnlBtsLista: TUniPanel;
    BtnDevolEnCurso: TUniBitBtn;
    BtnDevolEnviadas: TUniBitBtn;
    spTodas: TUniBitBtn;
    BtnDocumentoCabe: TUniBitBtn;
    UniLabel85: TUniLabel;
    edDocumentoProve: TUniDBEdit;
    spVerDocumentoProveedor: TUniBitBtn;
    btEtiquetasDocumento: TUniBitBtn;
    btInicioConfiguracion: TUniBitBtn;
    BtnPreProve: TUniBitBtn;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniLabel86: TUniLabel;
    UniLabel87: TUniLabel;
    UniLabel88: TUniLabel;
    btFicha: TUniBitBtn;
    RgTProve: TUniRadioGroup;
    edFiltroCabe: TUniEdit;
    UniLabel1: TUniLabel;
    btNuevaDistribuidora: TUniBitBtn;
    procedure btReservaClick(Sender: TObject);
    procedure RgTProveChangeValue(Sender: TObject);
    procedure btFichaClick(Sender: TObject);
    procedure RgTProveClick(Sender: TObject);
    procedure edFiltroCabeChange(Sender: TObject);
    procedure btNuevaDistribuidoraClick(Sender: TObject);
    procedure gridDistribuidoraListaColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure UniFormCreate(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }

  vTipo : String;

  procedure RutFichaClick(vCabe : Integer);

  end;

function FormDistribuidoraLista: TFormDistribuidoraLista;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu
  ,uDMDistribuidora, uDistribuidoraMenu, uDistribuidoraFicha;

function FormDistribuidoraLista: TFormDistribuidoraLista;
begin
  Result := TFormDistribuidoraLista(DMppal.GetFormInstance(TFormDistribuidoraLista));

end;

procedure TFormDistribuidoraLista.btFichaClick(Sender: TObject);
begin
  RutFichaClick(DMDistribuidora.sqlCabeSID_PROVEEDOR.AsInteger);
  DMppal.swDisitribuidoraFicha := self.Name;
end;

procedure TFormDistribuidoraLista.RutFichaClick(vCabe : Integer);
begin
  DMDistribuidora.RutAbrirCabe(vCabe,-1);
  FormDistribuidoraMenu.RutAbrirFichaDistribuidora;
  FormDistribuidoraFicha.swNuevo := False;
  FormDistribuidoraFicha.RutHabilitarBTs(False);

end;

procedure TFormDistribuidoraLista.UniFormCreate(Sender: TObject);
begin
  with gridDistribuidoraLista do
  begin
    Columns[0].Width := 50;
    Columns[1].Width := 250;
    Columns[2].Width := 250;
    Columns[3].Width := 150;
    Columns[4].Width := 100;
    Columns[5].Width := 64;
    Columns[6].Width := 64;
    Columns[7].Width := 64;
    Columns[8].Width := 64;
    Columns[9].Width := 64;
  end;
end;

procedure TFormDistribuidoraLista.gridDistribuidoraListaColumnSort(Column: TUniDBGridColumn;
  Direction: Boolean);
begin
  DMppal.SortColumnSQL(DMDistribuidora.sqlCabeS, Column.FieldName, Direction);
end;

procedure TFormDistribuidoraLista.btNuevaDistribuidoraClick(Sender: TObject);
begin
  FormDistribuidoraMenu.RutAbrirFichaDistribuidora;
  FormDistribuidoraFicha.swNuevo := True;
  FormDistribuidoraFicha.RutHabilitarBTs(True);
  DMDistribuidora.RutNuevaDisitrbuidora;
end;

procedure TFormDistribuidoraLista.RgTProveChangeValue(Sender: TObject);
begin
  //DMDistribuidora.RutAbrirCabeS(0,RgTProve.ItemIndex);
end;


procedure TFormDistribuidoraLista.RgTProveClick(Sender: TObject);
begin
  DMDistribuidora.RutAbrirCabeS(0,RgTProve.ItemIndex,'');
end;


procedure TFormDistribuidoraLista.btReservaClick(Sender: TObject);
begin
//  FormCliente.RutAbrirReservaClie;
end;

procedure TFormDistribuidoraLista.edFiltroCabeChange(Sender: TObject);
begin
    DMDistribuidora.RutAbrirCabeS(0,-1,edFiltroCabe.Text);
end;


end.

