unit uCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormCliente = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaCliente: TUniTabSheet;
    tabFichaCliente: TUniTabSheet;
    tabReservaCliente: TUniTabSheet;
    tabConsuPendiente: TUniTabSheet;
    pnlBotonera: TUniPanel;
    pnlBtsPesta�as: TUniPanel;
    btFichaClie: TUniBitBtn;
    btReservaClie: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    pnlBotonesGeneral: TUniPanel;
    btSalir: TUniBitBtn;
    btListaClie: TUniBitBtn;
    btMenu: TUniBitBtn;
    procedure pcDetalleChange(Sender: TObject);
    procedure UniBitBtn2Click(Sender: TObject);
    procedure btCambiarCodigoClick(Sender: TObject);


  private

    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swClieLista, swClieFicha, swClieReserva, swConsuPdte : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    procedure RutAbrirFichaClie;
    procedure RutAbrirListaClie;
    procedure RutAbrirReservaClie;
  end;

function FormCliente: TFormCliente;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uClienteLista, uClienteFicha, uClienteReserva;

function FormCliente: TFormCliente;
begin
  Result := TFormCliente(DMppal.GetFormInstance(TFormCliente));

end;

procedure TFormCliente.btCambiarCodigoClick(Sender: TObject);
begin
  FormClienteFicha.btCambiarCodigoClick(nil);
end;

procedure TFormCliente.RutAbrirFichaClie;
begin
  pcDetalle.ActivePage := tabFichaCliente;
  if pcDetalle.ActivePage = tabFichaCliente then
  begin
    if swClieFicha = 0 then
    begin
      FormClienteFicha.Parent := tabFichaCliente;
      FormClienteFicha.Show();
      swClieFicha := 1;
    end;
  end;
end;

procedure TFormCliente.RutAbrirListaClie;
begin
  pcDetalle.ActivePage := tabListaCliente;
  if pcDetalle.ActivePage = tabListaCliente then
  begin
    if swClieLista = 0 then
    begin
      FormClienteLista.Parent := tabListaCliente;
      FormClienteLista.Show();
      swClieLista := 1;
    end;
  end;
end;

procedure TFormCliente.RutAbrirReservaClie;
begin
  pcDetalle.ActivePage := tabReservaCliente;
  if pcDetalle.ActivePage = tabReservaCliente then
  begin
    if swClieReserva = 0 then
    begin
      FormClienteReserva.Parent := tabReservaCliente;
      FormClienteReserva.Show();
      swClieReserva := 1;
    end;
  end;
end;

procedure TFormCliente.pcDetalleChange(Sender: TObject);
begin

  if pcDetalle.ActivePage = tabConsuPendiente then
  begin
    if swConsuPdte = 0 then
    begin
      //FormMenuClienteConsuPdte.Parent := tabConsuPendiente;
      //FormMenuClienteConsuPdte.Show();
      swConsuPdte := 1;
    end;
  end;
end;



procedure TFormCliente.UniBitBtn2Click(Sender: TObject);
begin
 // if pcDetalle.ActivePage = tabFichaCliente then


end;

end.
