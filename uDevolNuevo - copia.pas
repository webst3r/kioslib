unit uDevolNuevo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox;

type
  TFormDevolNuevo = class(TUniForm)
    dsCabeNuevo: TDataSource;
    UniDBEdit1: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    btGrabar: TUniButton;
    btCancelar: TUniButton;
    UniLabel8: TUniLabel;
    UniDBEdit3: TUniDBEdit;
    UniLabel1: TUniLabel;
    UniDBEdit4: TUniDBEdit;
    procedure btGrabarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swEdit : Boolean;


  end;

function FormDevolNuevo: TFormDevolNuevo;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu;

function FormDevolNuevo: TFormDevolNuevo;
begin
  Result := TFormDevolNuevo(DMppal.GetFormInstance(TFormDevolNuevo));

end;


procedure TFormDevolNuevo.btGrabarClick(Sender: TObject);
begin
//  DMDevolucion.GrabarCabeNuevo;
  Self.Close;
end;

procedure TFormDevolNuevo.btCancelarClick(Sender: TObject);
begin
  //DMDevolucion.CancelarCabeNuevo;
  //if not swEdit then  DMDevolucion.RutBorrarDevolucion;//sw para controlar si entra a modificar o nuevo
  Self.Close;
end;

end.
