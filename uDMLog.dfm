object DMLog: TDMLog
  OldCreateOrder = False
  Height = 461
  Width = 804
  object sqlLog: TFDQuery
    Connection = DMppal.FDConnection2
    SQL.Strings = (
      'select FL.*'
      ', FLI.titulo'
      ''
      'from flog FL'
      'left join flogi FLI on (FLI.accion = FL.accion)'
      'where FL.fecha > '#39'01/01/0101'#39
      'order by 1,2,3,4'
      '')
    Left = 57
    Top = 75
    object sqlLogID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlLogUSUARIO: TStringField
      FieldName = 'USUARIO'
      Origin = 'USUARIO'
      Size = 40
    end
    object sqlLogFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlLogHORAINICIO: TTimeField
      FieldName = 'HORAINICIO'
      Origin = 'HORAINICIO'
      DisplayFormat = 'hh:mm:ss'
    end
    object sqlLogACCION: TIntegerField
      FieldName = 'ACCION'
      Origin = 'ACCION'
    end
    object sqlLogID_CONTROL: TIntegerField
      FieldName = 'ID_CONTROL'
      Origin = 'ID_CONTROL'
    end
    object sqlLogTITULO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TITULO'
      Origin = 'TITULO'
      ProviderFlags = []
      ReadOnly = True
      Size = 60
    end
  end
  object sqlLogUsuariosAcciones: TFDQuery
    Connection = DMppal.FDConnection2
    SQL.Strings = (
      'select FL.usuario, FL.accion '
      ', FLI.titulo'
      ',count(FL.accion) as Total'
      ''
      'from flog FL'
      'left join flogi FLI on (FLI.accion = FL.accion)'
      'where FL.fecha > '#39'01/01/0101'#39
      ''
      'group by fl.usuario, FL.accion, FLI.titulo'
      'order by 1,2')
    Left = 57
    Top = 131
    object sqlLogUsuariosAccionesUSUARIO: TStringField
      FieldName = 'USUARIO'
      Origin = 'USUARIO'
      Size = 40
    end
    object sqlLogUsuariosAccionesACCION: TIntegerField
      FieldName = 'ACCION'
      Origin = 'ACCION'
    end
    object sqlLogUsuariosAccionesTITULO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TITULO'
      Origin = 'TITULO'
      ProviderFlags = []
      ReadOnly = True
      Size = 60
    end
    object sqlLogUsuariosAccionesTOTAL: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlLogAcciones: TFDQuery
    Connection = DMppal.FDConnection2
    SQL.Strings = (
      'select FL.accion'
      ', FLI.titulo'
      ',count(FL.accion) as Total'
      ''
      'from flog FL'
      'left join flogi FLI on (FLI.accion = FL.accion)'
      'where FL.fecha > '#39'01/01/0101'#39
      ''
      'group by FL.accion, FLI.titulo'
      'order by 1,2')
    Left = 57
    Top = 195
    object sqlLogAccionesACCION: TIntegerField
      FieldName = 'ACCION'
      Origin = 'ACCION'
    end
    object sqlLogAccionesTITULO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TITULO'
      Origin = 'TITULO'
      ProviderFlags = []
      ReadOnly = True
      Size = 60
    end
    object sqlLogAccionesTOTAL: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
end
