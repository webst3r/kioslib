unit uDevolMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormDevolMenu = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaDevolucion: TUniTabSheet;
    tabAlbaranDevolucion: TUniTabSheet;
    tabNuevaDevolucion: TUniTabSheet;
    procedure btMenuClick(Sender: TObject);


  private



    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swDevoLista, swDevoAlbaran, swNuevaDevo : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;
    procedure RutAbrirArtiDevol;
    procedure RutAbrirAlbaranTab;
    procedure RutAbrirLista;
    procedure RutAbrirNuevo;
  end;

function FormDevolMenu: TFormDevolMenu;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolLista, uDevolFicha, uDevolNuevo, uDMDevol;

function FormDevolMenu: TFormDevolMenu;
begin
  Result := TFormDevolMenu(DMppal.GetFormInstance(TFormDevolMenu));

end;

procedure TFormDevolMenu.RutAbrirNuevo;
begin
{  pcDetalle.ActivePage := tabNuevaDevolucion;
  if pcDetalle.ActivePage = tabNuevaDevolucion then
  begin
    if swNuevaDevo = 0 then
    begin
      FormDevolucionNuevo.Parent := tabNuevaDevolucion;
     // FormDevolucionAlbaran.pcDetalle.ActivePage := FormDevolucionAlbaran.tabAlbaran;
      FormDevolucionNuevo.Show();
      swNuevaDevo := 1;
    end;
  end;
 }
end;

procedure TFormDevolMenu.RutAbrirAlbaranTab;
begin

  pcDetalle.ActivePage := tabAlbaranDevolucion;
  if pcDetalle.ActivePage = tabAlbaranDevolucion then
  begin
    if swDevoAlbaran = 0 then
    begin
      FormDevolFicha.Parent := tabAlbaranDevolucion;
      //Devolucion.RutAbrirAlbaran(TipoDoc);
      FormDevolFicha.Show();
      swDevoAlbaran := 1;
    end;
  end;

  FormDevolFicha.pBarrasA.SetFocus;
  FormDevolFicha.edMensajes.Text := '';
  FormDevolFicha.edMensajes.Color := clWindow;

end;
procedure TFormDevolMenu.RutAbrirArtiDevol;
begin
    FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;


    FormDevolMenu.Parent := FormMenu.tabDevolucion;
    FormDevolMenu.Align  := alClient;
    FormDevolMenu.Show();

    FormDevolMenu.pcDetalle.ActivePage := FormDevolMenu.tabAlbaranDevolucion;
    FormDevolFicha.Parent := FormDevolMenu.tabAlbaranDevolucion;
    FormDevolFicha.Align := alClient;
    FormDevolFicha.Show();
    FormMenu.swDevolucion := 1;
    FormDevolFicha.pBarrasA.SetFocus;




end;

procedure TFormDevolMenu.RutAbrirLista;
begin
  pcDetalle.ActivePage := tabListaDevolucion;
  if pcDetalle.ActivePage = tabListaDevolucion then
  begin
    if swDevoLista = 0 then
    begin
      FormDevolLista.Parent := tabListaDevolucion;
      //FormMenuDevolucionLista.pnlBtsMenu2.Parent := FormMenuDevolucionLista.pnlBtsLista;
      FormDevolLista.Show();
      swDevoLista := 1;
      FormDevolLista.btConsultaAbiertosClick(nil);
    end;
  end;
  DMPpal.RutCapturaGrid(FormDevolLista.Name, FormDevolLista.gridListaDevolucion, nil, 0, 0, 0);
end;

procedure TFormDevolMenu.btMenuClick(Sender: TObject);
begin
//  if pcDetalle.ActivePage = tabListaDevolucion   then FormDevolucionLista.pnlBtsLista.Visible     := not FormDevolucionLista.pnlBtsLista.Visible;
//  if pcDetalle.ActivePage = tabAlbaranDevolucion then FormDevolucionAlbaran.pnlBtsAlbaran.Visible := not FormDevolucionAlbaran.pnlBtsAlbaran.Visible;
 { if pcDetalle.ActivePage = tabListaDevolucion  then
  begin
    //FormMenuDevolucionLista.pnlBtsMenu2.Parent := pnlParentMenu;
   // FormMenuDevolucionLista.pnlBtsLista.Visible     := not FormMenuDevolucionLista.pnlBtsLista.Visible;
    FormMenuDevolucionLista.pnlBtsMenu2.Visible := True;
  end;
  }
end;

end.
