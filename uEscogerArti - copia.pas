unit uEscogerArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormEscogerArti = class(TUniForm)
    dsArticulo: TDataSource;
    ImgList: TUniNativeImageList;
    UniPanel1: TUniPanel;
    UniButton1: TUniButton;
    UniButton2: TUniButton;
    UniLabel1: TUniLabel;


  private


    { Private declarations }

  public
    { Public declarations }

    vParada,vTiempo,vFinalizado : Boolean;
    vNuevoUsu : Boolean;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    swModificar, swNuevo : Boolean;
  
  end;

function FormEscogerArti: TFormEscogerArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolFicha;

function FormEscogerArti: TFormEscogerArti;
begin
  Result := TFormEscogerArti(DMppal.GetFormInstance(TFormEscogerArti));

end;


end.

