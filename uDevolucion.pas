unit uDevolucion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList, uniButton;

type
  TFormDevolucion = class(TUniForm)
    pcDetalle: TUniPageControl;
    tabListaDevolucion: TUniTabSheet;
    tabAlbaranDevolucion: TUniTabSheet;
    pnlBotonera: TUniPanel;
    pnlBtsPesta�as: TUniPanel;
    btListaDevolucion: TUniBitBtn;
    btAlbaranDevolucion: TUniBitBtn;
    ImgNativeList: TUniNativeImageList;
    pnlBotonesGeneral: TUniPanel;
    btVolver: TUniBitBtn;
    btBuscar: TUniBitBtn;
    UniBitBtn2: TUniBitBtn;
    UniBitBtn3: TUniBitBtn;
    UniBitBtn4: TUniBitBtn;
    UniBitBtn5: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    UniBitBtn9: TUniBitBtn;
    UniButton1: TUniButton;
    btSalir: TUniBitBtn;

    procedure btListaDevolucionClick(Sender: TObject);

    procedure btAlbaranDevolucionClick(Sender: TObject);


  private
    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;

      swDevoLista, swDevoAlbaran : Integer;

    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

  end;

function FormDevolucion: TFormDevolucion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDevolucionLista, uDevolucionAlbaran;

function FormDevolucion: TFormDevolucion;
begin
  Result := TFormDevolucion(DMppal.GetFormInstance(TFormDevolucion));

end;

procedure TFormDevolucion.btAlbaranDevolucionClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabAlbaranDevolucion;
  if pcDetalle.ActivePage = tabAlbaranDevolucion then
  begin
    if swDevoAlbaran = 0 then
    begin
      FormDevolucionAlbaran.Parent := tabAlbaranDevolucion;
      FormDevolucionAlbaran.Show();
      swDevoAlbaran := 1;
    end;
  end;

end;

procedure TFormDevolucion.btListaDevolucionClick(Sender: TObject);
begin
  pcDetalle.ActivePage := tabListaDevolucion;
  if pcDetalle.ActivePage = tabListaDevolucion then
  begin
    if swDevoLista = 0 then
    begin
      FormDevolucionLista.Parent := tabListaDevolucion;
      FormDevolucionLista.Show();
      swDevoLista := 1;
    end;
  end;
end;






end.
