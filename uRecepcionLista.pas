unit uRecepcionLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormListaRecepcion = class(TUniForm)
    tmSession: TUniTimer;
    UniPanel2: TUniPanel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    edDesdeFEcha: TUniDBDateTimePicker;
    edHastaFecha: TUniDBDateTimePicker;
    UniSpeedButton8: TUniSpeedButton;
    gridListaRecepcion: TUniDBGrid;
    btAbrirFicha: TUniSpeedButton;
    dsListaRecepcion: TDataSource;
    lbText: TUniLabel;
    btConsultaAbiertos: TUniBitBtn;
    btConsultaCerrados: TUniBitBtn;
    btConsultaTodos: TUniBitBtn;
    procedure btAbrirFichaClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure gridListaRecepcionDrawColumnCell(Sender: TObject; ACol,
      ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure gridListaRecepcionColumnSummaryResult(Column: TUniDBGridColumn;
      GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
    procedure gridListaRecepcionColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);


  private


    { Private declarations }

  public
    { Public declarations }

    maxId : Integer;
    vParada,vTiempo,vFinalizado : Boolean;
    vStringUsu,vTipoBt, vTipoBt2 : string;
    vHora : Double;

    vTCantidad, VTCantiEnAlba, VTImpoBruto, VTImpoDto,
    VTValCoste, VTValCoste2, VTCosteTotal,
    VTImpoBase, VTImpoIVA,     VTImpoRE,    VTImpoTotLin : Double;


    procedure ProCalcularCabeceras;
    procedure RutAbrirAlbaranRece;
  end;

function FormListaRecepcion: TFormListaRecepcion;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uRecepcionAlbaran, uMenuRecepcion, uDMRecepcion;

function FormListaRecepcion: TFormListaRecepcion;
begin
  Result := TFormListaRecepcion(DMppal.GetFormInstance(TFormListaRecepcion));

end;


procedure TFormListaRecepcion.gridListaRecepcionColumnSummary(
  Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'CANTIENALBA') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:= 0.0;
    Column.AuxValue:= FloatToStrf(vTCantidad, ffNumber, 10,0);
  end else if SameText(Column.FieldName, 'CANTIDAD') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:= 0;
    Column.AuxValue:= FloatToStrf(VTCantiEnAlba,  ffNumber, 10,0);
  end else
  if SameText(Column.FieldName, 'VALCOSTE') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=FloatToStrf(VTValCoste, ffNumber, 10,2);
  end
  else if SameText(Column.FieldName, 'VALCOSTE2') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=FloatToStrf(VTValCoste2,  ffNumber, 10,2);
  end
  else if SameText(Column.FieldName, 'CosteTotal') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=FloatToStrf(VTCosteTotal, ffNumber, 10,2);
  end
end;

procedure TFormListaRecepcion.gridListaRecepcionColumnSummaryResult(
  Column: TUniDBGridColumn; GroupFieldValue: Variant; Attribs: TUniCellAttribs;
  var Result: string);
var
  I :Integer;
begin
  if SameText(Column.FieldName, 'CANTIENALBA') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr(',0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'CANTIDAD') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr(',0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'VALCOSTE') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr(',0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'VALCOSTE2') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr(',0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'CosteTotal') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr(',0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end
end;

procedure TFormListaRecepcion.gridListaRecepcionDrawColumnCell(Sender: TObject;
  ACol, ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if DMMenuRecepcion.sqlListaRecepcionCANTIDAD.AsInteger <> DMMenuRecepcion.sqlListaRecepcionCANTIENALBA.AsInteger then
  begin
    Attribs.Font.Color := clRed;
    Attribs.Font.Size  := 10;
  end;
end;

procedure TFormListaRecepcion.UniFormCreate(Sender: TObject);
begin
  edDesdeFecha.DateTime := now - 365;
  edhastaFecha.DateTime := now;
end;

procedure TFormListaRecepcion.btAbrirFichaClick(Sender: TObject);
begin
  RutAbrirAlbaranRece;

  DMMenuRecepcion.RutAbrirCabe1(' = ' + DMMenuRecepcion.sqlListaRecepcionIDSTOCABE.AsString,' = 0',edDesdeFecha.DateTime,edHastaFecha.DateTime,true);

  DMMenuRecepcion.RutAbrirArti(DMMenuRecepcion.sqlHisArtiID_ARTICULO.AsInteger);
  DMppal.swRecepcionFicha := self.Name;
end;

procedure TFormListaRecepcion.RutAbrirAlbaranRece;
begin
  FormMenuRecepcion.pcDetalle.ActivePage := FormMenuRecepcion.tabAlbaranRecepcion;
  FormAlbaranRecepcion.Parent := FormMenuRecepcion.tabAlbaranRecepcion;
  FormAlbaranRecepcion.Align  := alClient;
  FormAlbaranRecepcion.Show();
end;


procedure TFormListaRecepcion.ProCalcularCabeceras;
begin

  vTCantidad    := 0;
  VTCantiEnAlba := 0;
  VTValCoste    := 0;
  VTValCoste2   := 0;
  VTCosteTotal  := 0;

  VTImpoBruto   := 0;
  VTImpoDto     := 0;
  VTImpoBase    := 0;
  VTImpoIVA     := 0;
  VTImpoRE      := 0;
  VTImpoTotLin  := 0;

   DMMenuRecepcion.sqlListaRecepcion.First;
   while not DMMenuRecepcion.sqlListaRecepcion.Eof do
   begin
      vTCantidad   := vTCantidad    + DMMenuRecepcion.sqlListaRecepcion.FieldByName('CANTIDAD').AsFloat;
      VTCantiEnAlba:= VTCantiEnAlba + DMMenuRecepcion.sqlListaRecepcion.FieldByName('CANTIENALBA').AsFloat;
      VTValCoste   := VTValCoste    + DMMenuRecepcion.sqlListaRecepcion.FieldByName('VALCOSTE').AsFloat;
      VTValCoste2  := VTValCoste2   + DMMenuRecepcion.sqlListaRecepcion.FieldByName('VALCOSTE2').AsFloat;
      VTCosteTotal := VTCosteTotal  + DMMenuRecepcion.sqlListaRecepcion.FieldByName('COSTETOTAL').AsFloat;

      VTImpoBruto  := VTImpoBruto   + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPOBRUTO').AsFloat;
      VTImpoDto    := VTImpoDto     + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPODTO').AsFloat;
      VTImpoBase   := VTImpoBase    + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPOBASE').AsFloat;
      VTImpoIVA    := VTImpoIVA     + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPOIVA').AsFloat;
      VTImpoRE     := VTImpoRE      + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPORE').AsFloat;
      VTImpoTotLin := VTImpoTotLin  + DMMenuRecepcion.sqlListaRecepcion.FieldByName('IMPOTOTLIN').AsFloat;
      DMMenuRecepcion.sqlListaRecepcion.next;
   end;
end;

end.
