unit ufrFicha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DB, uniGUIBaseClasses, uniURLFrame,
  unimURLFrame, ServerModule, uniButton;

type
  TFormfrFicha = class(TUniForm)
    UniURLFrame1: TUniURLFrame;
    btCerrarFR: TUniButton;
    procedure btCerrarFRClick(Sender: TObject);
    procedure UniFormBeforeShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    InvNum, vTipoInforme: string;
    vExportPDF : boolean;
    vModifica : Boolean;
  end;

function FormfrFicha: TFormfrFicha;

implementation

{$R *.dfm}

uses
  uDMppal, uniGUIApplication, uDMRecepcion, frDMKIOSLIB;

function FormfrFicha: TFormfrFicha;
begin
  Result := TFormfrFicha(DMppal.GetFormInstance(TFormfrFicha));
end;

procedure TFormfrFicha.btCerrarFRClick(Sender: TObject);
begin
  Close;
end;

procedure TFormfrFicha.UniFormBeforeShow(Sender: TObject);
var
  dm : TfrDMKioslib;
  RepUrl : string;
  vFile1, vFilePDF, vFolder: String;
  before, after : string;
begin
  dm := TfrDMKioslib.Create(nil);
  try
    if vModifica then
    RepUrl := dm.ModiReportPDF(InvNum)
    else
    if vExportPDF then
       RepUrl := dm.GenExportPDF(InvNum)
       else
       RepUrl := dm.GenReportPDF(InvNum,vTipoInforme);

  finally
    dm.Free;
  end;

  if vModifica = False then
  UniURLFrame1.URL := RepUrl;
//'/cache/sogemedi_exe/DQojKeaHOh101404154/$z$z$z/RIZICI180228131958823.pdf'


  vFile1   := UniServerModule.StartPath+RepUrl;

  before := vFile1;
  after  := stringreplace(before, '/', '\', [rfReplaceAll, rfIgnoreCase]);
  vFile1 := after;


  //-----------------------------------------------------------------------//
  //BLOQUEO HASTA DECIDIR LA RUTA EN LA QUE SE GUARDARAN LOS INFORMES Isaac//
  //-----------------------------------------------------------------------//

  {
  vFolder := UniServerModule.FilesFolderPath +'CRM\DOC\' + DMPedido.sqlFCabeID.AsString + '\' + '9' + '\';
  if DirectoryExists(vFolder) then else ForceDirectories(vFolder);

  vFilePDF := UniServerModule.FilesFolderPath +'CRM\DOC\' + DMPedido.sqlFCabeID.AsString + '\' + '9' + '\' + 'Info_' + DMPedido.sqlFCabeID.AsString + '_'
           +  FormatDateTime('yymmdd', now) + '_' + FormatDateTime('hhmmss', now) + '.pdf';

  }
//TEMPORAL PARA VER QUE FUNCIONA
  vFolder := UniServerModule.FilesFolderPath +'KIOSLIB\DOC\' + '1' + '\' + '9\' + vTipoInforme  + '\';
  if DirectoryExists(vFolder) then else ForceDirectories(vFolder);

  vFilePDF := UniServerModule.FilesFolderPath +'KIOSLIB\DOC\' + '1' + '\' + '9' + '\' + vTipoInforme +'\' + 'Info_' + vTipoInforme + '_'
           +  FormatDateTime('yymmdd', now) + '_' + FormatDateTime('hhmmss', now) + '.pdf';

//'C:\Users\Toni\Documents\Embarcadero\Studio\Projects\Sogemedi\Win32\Debug\files\UR\DOC\1\9\aaaa.pdf'
//  UniSession.SendFile(RepUrl, vFilePDF);

  CopyFile(pchar(vFile1), pChar(vFilePDF), False);


end;

end.












