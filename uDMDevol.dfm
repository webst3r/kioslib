object DMDevolucion: TDMDevolucion
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 578
  Width = 794
  object sqlCabeS2: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select'
      
        ' S.IDSTOCABE, S.SWTIPODOCU, S.NDOCSTOCK, S.FECHA, S.NALMACEN, S.' +
        'ID_PROVEEDOR, S.DOCTOPROVE, S.DOCTOPROVEFECHA, S.FECHACARGO, S.F' +
        'ECHAABONO,  S.NOMBRE, S.NIF, S.MENSAJEAVISO, S.SWDEVOLUCION'
      ', S.PAQUETE'
      ', P.REFEPROVEEDOR, P.RUTA'
      ' From FVSTOCABE S'
      ''
      'INNER JOIN FMPROVE P'
      'ON (S.ID_PROVEEDOR = P.ID_PROVEEDOR)'
      ''
      'where S.SWDEVOLUCION = 1'
      'and (S.SWTIPODOCU = 2 or S.SWTIPODOCU = 22)'
      'Order by S.IDSTOCABE')
    Left = 336
    Top = 24
    object sqlCabeS2IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeS2SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabeS2NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlCabeS2FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabeS2NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlCabeS2ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabeS2DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabeS2DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabeS2FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlCabeS2FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlCabeS2NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeS2NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeS2MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeS2SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlCabeS2PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlCabeS2REFEPROVEEDOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlCabeS2RUTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RUTA'
      Origin = 'RUTA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
  end
  object sqlCabe1: TFDQuery
    AfterScroll = sqlCabe1AfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FC.*'
      ', P.maxpaquete'
      ', (SELECT SUM(Devueltos) as TotalDevol'
      '   FROM FVHISARTI'
      '   WHERE IDSTOCABE = FC.idstocabe)'
      
        ', (Select Count(distinct FV.paquete) from FVHISARTI FV Where FV.' +
        'idstocabe = FC.idstocabe) as TotalPaquetes'
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where FC.IDSTOCABE > 0 AND FC.SWTIPODOCU = 2'
      'Order by FC.IDSTOCABE')
    Left = 112
    Top = 16
    object sqlCabe1IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabe1SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabe1NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlCabe1FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabe1NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlCabe1NALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlCabe1NALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlCabe1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabe1FABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlCabe1SWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlCabe1DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabe1DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabe1FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlCabe1FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlCabe1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabe1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabe1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabe1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlCabe1PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlCabe1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlCabe1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlCabe1SWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlCabe1DEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlCabe1DEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlCabe1DEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlCabe1DEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlCabe1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlCabe1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlCabe1SWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlCabe1SWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlCabe1SWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlCabe1SWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlCabe1SEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlCabe1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlCabe1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlCabe1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlCabe1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlCabe1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlCabe1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlCabe1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlCabe1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlCabe1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlCabe1MAXPAQUETE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
      ProviderFlags = []
    end
    object sqlCabe1REFEPROVEEDOR: TStringField
      FieldKind = fkLookup
      FieldName = 'REFEPROVEEDOR'
      LookupDataSet = sqlProveS
      LookupKeyFields = 'ID_PROVEEDOR'
      LookupResultField = 'REFEPROVEEDOR'
      KeyFields = 'ID_PROVEEDOR'
      Lookup = True
    end
    object sqlCabe1RUTA: TStringField
      FieldKind = fkLookup
      FieldName = 'RUTA'
      LookupDataSet = sqlProveS
      LookupKeyFields = 'ID_PROVEEDOR'
      LookupResultField = 'RUTA'
      KeyFields = 'ID_PROVEEDOR'
      Lookup = True
    end
    object sqlCabe1TextoPaquete: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TextoPaquete'
      Size = 50
    end
    object sqlCabe1TotalDevueltos: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TotalDevueltos'
    end
    object sqlCabe1TOTALDEVOL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALDEVOL'
      Origin = 'TOTALDEVOL'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlCabe1TOTALPAQUETES: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALPAQUETES'
      Origin = 'TOTALPAQUETES'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlLinea: TFDQuery
    OnCalcFields = sqlLineaCalcFields
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  H.*,A.BARRAS,A.DESCRIPCION,A.TBARRAS,'
      ''
      
        '(Select max(HC.FECHA) From FVHISARTI HC where HC.ID_ARTICULO = H' +
        '.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      
        '                                             and ((HC.SWACABADO ' +
        '= 0) or (HC.SWACABADO is NULL))'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as FechaCompra,'
      ''
      
        '(Select Sum(HC.CANTIDAD) From FVHISARTI HC where HC.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      '                                             ) as CompraHisto,'
      
        '(Select Sum(HS.CANTIDAD) From FVHISARTI HS  where  HS.ID_ARTICUL' +
        'O  = H.ID_ARTICULO   and HS.ADENDUM = H.ADENDUM  and (HS.clave =' +
        ' '#39'51'#39' or HS.clave = '#39'52'#39' or HS.clave = '#39'53'#39' ) ) as VentaHisto,'
      
        '(Select Sum(HD.CANTIDAD) From FVHISARTI HD  where  HD.ID_ARTICUL' +
        'O = H.ID_ARTICULO   and HD.ADENDUM = H.ADENDUM  and HD.clave = '#39 +
        '54'#39'    and HD.ID_HISARTI <> H.ID_HISARTI ) as DevolHisto,'
      
        '(Select Sum(HM.CANTIDAD) From FVHISARTI HM where  HM.ID_ARTICULO' +
        ' = H.ID_ARTICULO   and HM.ADENDUM = H.ADENDUM  and HM.clave = '#39'5' +
        '5'#39'   and HM.ID_HISARTI <> H.ID_HISARTI ) as MermaHisto,'
      'P.nombre '
      ''
      'From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ''
      ' where H.ID_HISARTI is not null'
      ' and H.SWCERRADO <> 1'
      ' and (A.swaltabaja = 0 or A.swaltabaja is null)'
      ' and A.swactivado = 1'
      ' and A.periodicidad = 5'
      ' and A.swcontrolfecha = 1'
      ' and H.clave = '#39'01'#39
      ' and H.Fecha ='#39'01/01/2000'#39)
    Left = 24
    Top = 88
    object sqlLineaID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlLineaSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlLineaFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlLineaHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlLineaCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlLineaID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlLineaADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlLineaID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlLineaREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlLineaIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlLineaIDDEVOLUCABE: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object sqlLineaPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlLineaID_HISARTI01: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object sqlLineaID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlLineaPARTIDA: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object sqlLineaNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlLineaID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlLineaID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlLineaSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlLineaNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlLineaNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlLineaNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlLineaDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlLineaVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlLineaMERMA: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object sqlLineaCARGO: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object sqlLineaABONO: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object sqlLineaRECLAMADO: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object sqlLineaCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlLineaCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlLineaCANTISUSCRI: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object sqlLineaPRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlLineaPRECIOCOMPRA2: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
      DisplayFormat = '0.00'
    end
    object sqlLineaPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = '0.000'
    end
    object sqlLineaPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
      DisplayFormat = '0.0000'
    end
    object sqlLineaPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
      DisplayFormat = '0.0000'
    end
    object sqlLineaPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
      DisplayFormat = '0.0000'
    end
    object sqlLineaPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
      DisplayFormat = '0.00'
    end
    object sqlLineaPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
      DisplayFormat = '0.00'
    end
    object sqlLineaSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object sqlLineaTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
    end
    object sqlLineaTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlLineaTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlLineaTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlLineaTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlLineaTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlLineaTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlLineaIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
      DisplayFormat = '0.00'
    end
    object sqlLineaIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      DisplayFormat = '0.00'
    end
    object sqlLineaENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlLineaSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlLineaVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
      DisplayFormat = '0.0000'
    end
    object sqlLineaVALORCOSTE2: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
      DisplayFormat = '0.0000'
    end
    object sqlLineaVALORCOSTETOT: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
      DisplayFormat = '0.0000'
    end
    object sqlLineaVALORCOSTETOT2: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
      DisplayFormat = '0.0000'
    end
    object sqlLineaVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlLineaVALORCOMPRA2: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
      DisplayFormat = '0.00'
    end
    object sqlLineaVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
      DisplayFormat = '0.00'
    end
    object sqlLineaVALORVENTA2: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
      DisplayFormat = '0.00'
    end
    object sqlLineaVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
      DisplayFormat = '0.00'
    end
    object sqlLineaTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object sqlLineaIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
      DisplayFormat = '0.00'
    end
    object sqlLineaMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
      DisplayFormat = '0.00'
    end
    object sqlLineaMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
      DisplayFormat = '0.00'
    end
    object sqlLineaENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
      DisplayFormat = '0.00'
    end
    object sqlLineaENCARTE2: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
      DisplayFormat = '0.00'
    end
    object sqlLineaTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlLineaNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlLineaSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlLineaSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlLineaSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlLineaSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlLineaSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlLineaSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlLineaSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlLineaSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlLineaSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlLineaSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlLineaSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlLineaTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlLineaFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlLineaFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlLineaFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlLineaFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlLineaSWRECLAMA: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object sqlLineaID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlLineaSWACABADO: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object sqlLineaBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlLineaDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlLineaTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaFECHACOMPRA: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHACOMPRA'
      Origin = 'FECHACOMPRA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaCOMPRAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'COMPRAHISTO'
      Origin = 'COMPRAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaVENTAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAHISTO'
      Origin = 'VENTAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaDEVOLHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLHISTO'
      Origin = 'DEVOLHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaMERMAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMAHISTO'
      Origin = 'MERMAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlLineaNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlLineaPrecioTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PrecioTotal'
      DisplayFormat = ',0.00'
      Calculated = True
    end
  end
  object sqlHistoAnteF: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  H.ID_ARTICULO, H.ADENDUM, A.BARRAS,A.DESCRIPCION,A.TBARR' +
        'AS, A.REFEPROVE, H.PRECIOVENTA, H.PRECIOVENTA2, H.FECHADEVOL,'
      'H.ID_PROVEEDOR,P.nombre,'
      'Count(*) as Registros,'
      'Sum(H.Cantidad) As Compras,'
      ''
      
        '(Select max(HC.FECHA) From FVHISARTI HC where HC.ID_ARTICULO = H' +
        '.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      
        '                                             and ((HC.SWACABADO ' +
        '= 0) or (HC.SWACABADO is NULL))'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as FechaCompra,'
      
        '(Select Sum(HC.CANTIDAD) From FVHISARTI HC where HC.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      
        '                                             and ((HC.SWACABADO ' +
        '= 0) or (HC.SWACABADO is NULL))'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as CompraHisto,'
      
        '(Select Sum(HS.CANTIDAD) From FVHISARTI HS where HS.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HS.ADENDUM = H.ADENDUM'
      
        '                                             and (HS.clave = '#39'51' +
        #39' or HS.clave = '#39'52'#39' or HS.clave = '#39'53'#39' )'
      
        '                                             and ((HS.SWACABADO ' +
        '= 0) or (HS.SWACABADO is NULL))'
      
        '                                             and coalesce(HS.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HS.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as VentaHisto,'
      
        '(Select Sum(HD.CANTIDAD) From FVHISARTI HD where HD.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HD.ADENDUM = H.ADENDUM'
      '                                             and HD.clave = '#39'54'#39
      
        '                                             and ((HD.SWACABADO ' +
        '= 0) or (HD.SWACABADO is NULL))'
      
        '                                             and HD.id_proveedor' +
        ' = H.id_proveedor'
      
        '                                             and coalesce(HD.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HD.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as DevolHisto,'
      
        '(Select Sum(HM.CANTIDAD) From FVHISARTI HM where HM.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HM.ADENDUM = H.ADENDUM'
      '                                             and HM.clave = '#39'55'#39
      
        '                                             and ((HM.SWACABADO ' +
        '= 0) or (HM.SWACABADO is NULL))'
      
        '                                             and coalesce(HM.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HM.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as MermaHisto'
      ''
      ''
      'From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      '       where H.ID_HISARTI is not null And H.ID_PROVEEDOR = 6'
      '        and ((H.SWACABADO = 0) or (H.SWACABADO is NULL))'
      '        and  (A.swaltabaja = 0 or A.swaltabaja is null)'
      '        and H.clave = '#39'01'#39
      
        '        and (    H.FECHADEVOL >=  '#39'12/11/2009'#39' or H.FECHAAVISO i' +
        's null  or H.IDDEVOLUCABE = 536 OR H.IDSTOCABE    = 536 )'
      '        and (H.SWDEVOLUCION < 3 OR H.SWDEVOLUCION IS NULL )'
      
        '        and (h.iddevolucabe = 0 or h.iddevolucabe is null or h.i' +
        'ddevolucabe = 536 )'
      ' Group by 1,2,3,4,5,6,7,8,9,10,11'
      'Order by H.ID_PROVEEDOR,A.DESCRIPCION')
    Left = 32
    Top = 168
    object sqlHistoAnteFID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHistoAnteFADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHistoAnteFBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlHistoAnteFDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoAnteFTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFREFEPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object sqlHistoAnteFPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object sqlHistoAnteFFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlHistoAnteFID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHistoAnteFNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoAnteFREGISTROS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'REGISTROS'
      Origin = 'REGISTROS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFCOMPRAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'COMPRAS'
      Origin = 'COMPRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFFECHACOMPRA: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHACOMPRA'
      Origin = 'FECHACOMPRA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFCOMPRAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'COMPRAHISTO'
      Origin = 'COMPRAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFVENTAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAHISTO'
      Origin = 'VENTAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFDEVOLHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLHISTO'
      Origin = 'DEVOLHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFMERMAHISTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMAHISTO'
      Origin = 'MERMAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFA_Devolver: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'A_Devolver'
    end
    object sqlHistoAnteFA_Merma: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'A_Merma'
    end
    object sqlHistoAnteFA_Stock: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'A_Stock'
    end
    object sqlHistoAnteFA_Ventas: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'A_Venta'
    end
    object sqlHistoAnteFDevueltos: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Devueltos'
    end
    object sqlHistoAnteFswPasado: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'swPasado'
    end
    object sqlHistoAnteFPrecioTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PrecioTotal'
      Calculated = True
    end
    object sqlHistoAnteFDevolTeorica: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DevolTeorica'
    end
  end
  object sqlHistoAnteFT: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  H.ID_HISARTI,H.ID_ARTICULO, H.ADENDUM, A.BARRAS,A.DESCRI' +
        'PCION,A.TBARRAS, A.REFEPROVE, H.PRECIOVENTA, H.PRECIOVENTA2, H.C' +
        'ANTIDAD, H.ID_PROVEEDOR,P.nombre'
      'From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      '       where H.ID_HISARTI = -2'
      '        and (H.SWCERRADO = 0 or H.SWCERRADO is null)'
      '        and  (A.swaltabaja = 0 or A.swaltabaja is null)'
      '        and (H.SWDEVOLUCION < 3 OR H.SWDEVOLUCION IS NULL )'
      'Order by H.ID_HISARTI,H.ID_PROVEEDOR,A.DESCRIPCION')
    Left = 136
    Top = 176
    object sqlHistoAnteFTID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHistoAnteFTID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHistoAnteFTADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHistoAnteFTBARRAS2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlHistoAnteFTDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoAnteFTTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFTREFEPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoAnteFTPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object sqlHistoAnteFTPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object sqlHistoAnteFTCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlHistoAnteFTID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHistoAnteFTNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object sqlHistoDevolver: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  H.*'
      ', A.BARRAS,A.DESCRIPCION,A.TBARRAS, P.NOMBRE'
      ',(Select Sum(HS.CANTIDAD) From FVHISARTI HS '
      'where HS.ID_ARTICULO = H.ID_ARTICULO '
      'and HS.ADENDUM = H.ADENDUM '
      'and (HS.clave >= '#39'51'#39' and HS.clave <= '#39'59'#39' ) ) as ADEVOLVER'
      ''
      'From FVHISARTI H'
      ''
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ''
      ' where H.ID_HISARTI is not null'
      ' and (H.SWPDTEPAGO = 2)'
      ' and (A.swaltabaja = 0 or A.swaltabaja is null)'
      ' and A.swactivado = 1'
      ' and H.clave = '#39'01'#39
      ' and H.Fecha >='#39'01/01/2000'#39
      ' and H.DEVUELTOS = 0'
      ''
      ''
      'Order by H.ID_PROVEEDOR,A.DESCRIPCION')
    Left = 136
    Top = 232
    object sqlHistoDevolverID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHistoDevolverSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlHistoDevolverFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHistoDevolverHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlHistoDevolverCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlHistoDevolverID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHistoDevolverADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHistoDevolverID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHistoDevolverREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlHistoDevolverIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlHistoDevolverIDDEVOLUCABE: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object sqlHistoDevolverPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHistoDevolverID_HISARTI01: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object sqlHistoDevolverID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlHistoDevolverPARTIDA: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object sqlHistoDevolverNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlHistoDevolverID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHistoDevolverID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlHistoDevolverSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlHistoDevolverNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlHistoDevolverNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlHistoDevolverNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlHistoDevolverDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlHistoDevolverVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlHistoDevolverMERMA: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object sqlHistoDevolverCARGO: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object sqlHistoDevolverABONO: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object sqlHistoDevolverRECLAMADO: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object sqlHistoDevolverCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlHistoDevolverCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlHistoDevolverCANTISUSCRI: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object sqlHistoDevolverPRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
    end
    object sqlHistoDevolverPRECIOCOMPRA2: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
    end
    object sqlHistoDevolverPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlHistoDevolverPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlHistoDevolverPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlHistoDevolverPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlHistoDevolverPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object sqlHistoDevolverPRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object sqlHistoDevolverSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object sqlHistoDevolverTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
    end
    object sqlHistoDevolverTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlHistoDevolverTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlHistoDevolverTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlHistoDevolverTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlHistoDevolverTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlHistoDevolverTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlHistoDevolverIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object sqlHistoDevolverIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object sqlHistoDevolverIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
    end
    object sqlHistoDevolverIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
    end
    object sqlHistoDevolverIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
    end
    object sqlHistoDevolverIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object sqlHistoDevolverIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object sqlHistoDevolverIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object sqlHistoDevolverIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
    end
    object sqlHistoDevolverENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlHistoDevolverSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlHistoDevolverVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
    end
    object sqlHistoDevolverVALORCOSTE2: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
    end
    object sqlHistoDevolverVALORCOSTETOT: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
    end
    object sqlHistoDevolverVALORCOSTETOT2: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
    end
    object sqlHistoDevolverVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
    end
    object sqlHistoDevolverVALORCOMPRA2: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
    end
    object sqlHistoDevolverVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
    end
    object sqlHistoDevolverVALORVENTA2: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
    end
    object sqlHistoDevolverVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
    end
    object sqlHistoDevolverTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object sqlHistoDevolverIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
    end
    object sqlHistoDevolverMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlHistoDevolverMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlHistoDevolverENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object sqlHistoDevolverENCARTE2: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
    end
    object sqlHistoDevolverTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlHistoDevolverNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHistoDevolverSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHistoDevolverSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlHistoDevolverSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlHistoDevolverSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHistoDevolverSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHistoDevolverSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHistoDevolverSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHistoDevolverSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHistoDevolverSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHistoDevolverSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHistoDevolverSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHistoDevolverTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlHistoDevolverFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlHistoDevolverFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlHistoDevolverFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHistoDevolverFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHistoDevolverSWRECLAMA: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object sqlHistoDevolverID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlHistoDevolverSWACABADO: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object sqlHistoDevolverBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlHistoDevolverDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoDevolverTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoDevolverNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoDevolverADEVOLVER: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ADEVOLVER'
      Origin = 'ADEVOLVER'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlHistoDevolverProve: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  H.*,A.BARRAS,A.DESCRIPCION,A.TBARRAS, P.NOMBRE'
      'From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ' where H.ID_HISARTI is not null'
      ' and (H.SWPDTEPAGO = 2)'
      ' and (A.swaltabaja = 0 or A.swaltabaja is null)'
      ' and A.swactivado = 1'
      ' and H.clave = '#39'01'#39
      ' and H.Fecha >='#39'01/01/2000'#39
      ' and H.DEVUELTOS = 0')
    Left = 136
    Top = 296
    object sqlHistoDevolverProveID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHistoDevolverProveSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlHistoDevolverProveFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHistoDevolverProveHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object sqlHistoDevolverProveCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlHistoDevolverProveID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHistoDevolverProveADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHistoDevolverProveID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHistoDevolverProveREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlHistoDevolverProveIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlHistoDevolverProveIDDEVOLUCABE: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object sqlHistoDevolverProvePAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHistoDevolverProveID_HISARTI01: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object sqlHistoDevolverProveID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlHistoDevolverProvePARTIDA: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object sqlHistoDevolverProveNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlHistoDevolverProveID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHistoDevolverProveID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlHistoDevolverProveSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlHistoDevolverProveNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlHistoDevolverProveNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlHistoDevolverProveNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlHistoDevolverProveDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlHistoDevolverProveVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlHistoDevolverProveMERMA: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object sqlHistoDevolverProveCARGO: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object sqlHistoDevolverProveABONO: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object sqlHistoDevolverProveRECLAMADO: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object sqlHistoDevolverProveCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlHistoDevolverProveCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlHistoDevolverProveCANTISUSCRI: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object sqlHistoDevolverProvePRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
    end
    object sqlHistoDevolverProvePRECIOCOMPRA2: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
    end
    object sqlHistoDevolverProvePRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlHistoDevolverProvePRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlHistoDevolverProvePRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlHistoDevolverProvePRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlHistoDevolverProvePRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object sqlHistoDevolverProvePRECIOVENTA2: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object sqlHistoDevolverProveSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object sqlHistoDevolverProveTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
    end
    object sqlHistoDevolverProveTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlHistoDevolverProveTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object sqlHistoDevolverProveTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object sqlHistoDevolverProveTIVA2: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlHistoDevolverProveTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlHistoDevolverProveTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlHistoDevolverProveIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object sqlHistoDevolverProveIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object sqlHistoDevolverProveIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
    end
    object sqlHistoDevolverProveIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
    end
    object sqlHistoDevolverProveIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
    end
    object sqlHistoDevolverProveIMPOBASE2: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object sqlHistoDevolverProveIMPOIVA2: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object sqlHistoDevolverProveIMPORE2: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object sqlHistoDevolverProveIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
    end
    object sqlHistoDevolverProveENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlHistoDevolverProveSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlHistoDevolverProveVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
    end
    object sqlHistoDevolverProveVALORCOSTE2: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
    end
    object sqlHistoDevolverProveVALORCOSTETOT: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
    end
    object sqlHistoDevolverProveVALORCOSTETOT2: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
    end
    object sqlHistoDevolverProveVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
    end
    object sqlHistoDevolverProveVALORCOMPRA2: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
    end
    object sqlHistoDevolverProveVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
    end
    object sqlHistoDevolverProveVALORVENTA2: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
    end
    object sqlHistoDevolverProveVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
    end
    object sqlHistoDevolverProveTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object sqlHistoDevolverProveIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
    end
    object sqlHistoDevolverProveMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlHistoDevolverProveMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlHistoDevolverProveENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object sqlHistoDevolverProveENCARTE2: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
    end
    object sqlHistoDevolverProveTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlHistoDevolverProveNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHistoDevolverProveSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHistoDevolverProveSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlHistoDevolverProveSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlHistoDevolverProveSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHistoDevolverProveSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHistoDevolverProveSWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHistoDevolverProveSWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHistoDevolverProveSWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHistoDevolverProveSWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHistoDevolverProveSWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHistoDevolverProveSEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHistoDevolverProveTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlHistoDevolverProveFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlHistoDevolverProveFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlHistoDevolverProveFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHistoDevolverProveFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHistoDevolverProveSWRECLAMA: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object sqlHistoDevolverProveID_HISARTIRE: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object sqlHistoDevolverProveSWACABADO: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object sqlHistoDevolverProveBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlHistoDevolverProveDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHistoDevolverProveTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlHistoDevolverProveNOMBRE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object sqlCabeSCap: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select '
      
        ' IDSTOCABE, SWTIPODOCU, NDOCSTOCK, FECHA, NALMACEN, ID_PROVEEDOR' +
        ', DOCTOPROVE, DOCTOPROVEFECHA, FECHACARGO,FECHAABONO, NOMBRE, NI' +
        'F, MENSAJEAVISO, SWDEVOLUCION'
      ' From FVSTOCABE'
      ' where IDSTOCABE = 0'
      ' Order by IDSTOCABE')
    Left = 360
    Top = 168
    object sqlCabeSCapIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeSCapSWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabeSCapNDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlCabeSCapFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabeSCapNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlCabeSCapID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabeSCapDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabeSCapDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabeSCapFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlCabeSCapFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlCabeSCapNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeSCapNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeSCapMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeSCapSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
  end
  object sqlProveS: TFDQuery
    AfterScroll = sqlProveSAfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select  A.ID_PROVEEDOR,A.NOMBRE,A.DIRECCION,A.POBLACION,A.PROVIN' +
        'CIA,A.CPOSTAL,A.CPROVINCIA'
      ', A.NIF,A.TELEFONO1,A.TELEFONO2,A.MOVIL'
      ', A.REFEPROVEEDOR, A.RUTA'
      ', A.MAXPAQUETE'
      ''
      'From FMPROVE A'
      'where ID_PROVEEDOR is not null'
      'Order by Nombre')
    Left = 592
    Top = 72
    object sqlProveSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProveSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProveSDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlProveSPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlProveSPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlProveSCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlProveSCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlProveSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProveSTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlProveSTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlProveSMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlProveSREFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlProveSRUTA: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object sqlProveSMAXPAQUETE: TIntegerField
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
    end
  end
  object sqlProve1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FMPROVE'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 112
    Top = 464
    object sqlProve1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProve1TPROVEEDOR: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
    object sqlProve1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlProve1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlProve1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlProve1NOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlProve1DIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlProve1DIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlProve1POBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlProve1PROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlProve1CPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlProve1CPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object sqlProve1CPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlProve1CONTACTO1NOMBRE: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object sqlProve1CONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlProve1CONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlProve1CONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlProve1TELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlProve1TELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlProve1MOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlProve1FAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlProve1EMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlProve1WEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlProve1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlProve1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlProve1BANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object sqlProve1AGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object sqlProve1DC: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object sqlProve1CUENTABANCO: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object sqlProve1TEFECTO: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object sqlProve1EFECTOS: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object sqlProve1FRECUENCIA: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object sqlProve1DIASPRIMERVTO: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object sqlProve1DIAFIJO1: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object sqlProve1DIAFIJO2: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object sqlProve1DIAFIJO3: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object sqlProve1TDTO: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object sqlProve1TPAGO: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object sqlProve1SWTFACTURACION: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object sqlProve1SWBLOQUEO: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object sqlProve1REFEPROVEEDOR: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object sqlProve1TRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlProve1RUTA: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object sqlProve1PROVEEDORHOSTING: TIntegerField
      FieldName = 'PROVEEDORHOSTING'
      Origin = 'PROVEEDORHOSTING'
    end
    object sqlProve1NOMBREHOSTING: TStringField
      FieldName = 'NOMBREHOSTING'
      Origin = 'NOMBREHOSTING'
      Size = 40
    end
    object sqlProve1ULTIMAFECHADESCARGA: TDateField
      FieldName = 'ULTIMAFECHADESCARGA'
      Origin = 'ULTIMAFECHADESCARGA'
    end
    object sqlProve1ULTIMAFECHAENVIO: TDateField
      FieldName = 'ULTIMAFECHAENVIO'
      Origin = 'ULTIMAFECHAENVIO'
    end
    object sqlProve1COLUMNASEXCEL: TStringField
      FieldName = 'COLUMNASEXCEL'
      Origin = 'COLUMNASEXCEL'
      Size = 30
    end
    object sqlProve1DESDEFILA: TIntegerField
      FieldName = 'DESDEFILA'
      Origin = 'DESDEFILA'
    end
    object sqlProve1SWRECIBIR: TSmallintField
      FieldName = 'SWRECIBIR'
      Origin = 'SWRECIBIR'
    end
    object sqlProve1SWENVIAR: TSmallintField
      FieldName = 'SWENVIAR'
      Origin = 'SWENVIAR'
    end
    object sqlProve1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlProve1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlProve1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlProve1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlProve1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlProve1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlProve1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlProve1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlEmpresa: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select * from  FGEMPRESA'
      'where NREFERENCIA = :NREFERENCIA')
    Left = 256
    Top = 168
    ParamData = <
      item
        Name = 'NREFERENCIA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlEmpresaNREFERENCIA: TIntegerField
      FieldName = 'NREFERENCIA'
      Origin = 'NREFERENCIA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlEmpresaNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlEmpresaNOMBRE2: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object sqlEmpresaTVIA: TStringField
      FieldName = 'TVIA'
      Origin = 'TVIA'
      FixedChar = True
      Size = 3
    end
    object sqlEmpresaVIANOMBRE: TStringField
      FieldName = 'VIANOMBRE'
      Origin = 'VIANOMBRE'
      Size = 30
    end
    object sqlEmpresaVIANUM: TStringField
      FieldName = 'VIANUM'
      Origin = 'VIANUM'
      Size = 5
    end
    object sqlEmpresaVIABLOC: TStringField
      FieldName = 'VIABLOC'
      Origin = 'VIABLOC'
      Size = 5
    end
    object sqlEmpresaVIABIS: TStringField
      FieldName = 'VIABIS'
      Origin = 'VIABIS'
      Size = 5
    end
    object sqlEmpresaVIAESCA: TStringField
      FieldName = 'VIAESCA'
      Origin = 'VIAESCA'
      Size = 5
    end
    object sqlEmpresaVIAPISO: TStringField
      FieldName = 'VIAPISO'
      Origin = 'VIAPISO'
      Size = 5
    end
    object sqlEmpresaVIAPUERTA: TStringField
      FieldName = 'VIAPUERTA'
      Origin = 'VIAPUERTA'
      Size = 5
    end
    object sqlEmpresaDIRECCION: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object sqlEmpresaDIRECCION2: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object sqlEmpresaPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object sqlEmpresaPROVINCIA: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object sqlEmpresaCPOSTAL: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object sqlEmpresaCPAIS: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object sqlEmpresaCPROVINCIA: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object sqlEmpresaNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlEmpresaCONTACTO1NOMRE: TStringField
      FieldName = 'CONTACTO1NOMRE'
      Origin = 'CONTACTO1NOMRE'
      Size = 30
    end
    object sqlEmpresaCONTACTO1CARGO: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object sqlEmpresaCONTACTO2NOMBRE: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object sqlEmpresaCONTACTO2CARGO: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object sqlEmpresaTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object sqlEmpresaTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object sqlEmpresaMOVIL: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object sqlEmpresaFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object sqlEmpresaEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object sqlEmpresaWEB: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object sqlEmpresaMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlEmpresaOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlEmpresaNACTIVIDAD: TIntegerField
      FieldName = 'NACTIVIDAD'
      Origin = 'NACTIVIDAD'
    end
    object sqlEmpresaNSECTOR: TIntegerField
      FieldName = 'NSECTOR'
      Origin = 'NSECTOR'
    end
    object sqlEmpresaTPCIVA1: TSingleField
      FieldName = 'TPCIVA1'
      Origin = 'TPCIVA1'
    end
    object sqlEmpresaTPCIVA2: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object sqlEmpresaTPCIVA3: TSingleField
      FieldName = 'TPCIVA3'
      Origin = 'TPCIVA3'
    end
    object sqlEmpresaTPCRE1: TSingleField
      FieldName = 'TPCRE1'
      Origin = 'TPCRE1'
    end
    object sqlEmpresaTPCRE2: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object sqlEmpresaTPCRE3: TSingleField
      FieldName = 'TPCRE3'
      Origin = 'TPCRE3'
    end
    object sqlEmpresaNPEDIDO: TIntegerField
      FieldName = 'NPEDIDO'
      Origin = 'NPEDIDO'
    end
    object sqlEmpresaNPEDIPROVE: TIntegerField
      FieldName = 'NPEDIPROVE'
      Origin = 'NPEDIPROVE'
    end
    object sqlEmpresaNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlEmpresaNALBASERVI: TIntegerField
      FieldName = 'NALBASERVI'
      Origin = 'NALBASERVI'
    end
    object sqlEmpresaNALBACOLECTOR: TIntegerField
      FieldName = 'NALBACOLECTOR'
      Origin = 'NALBACOLECTOR'
    end
    object sqlEmpresaNALBALMA: TIntegerField
      FieldName = 'NALBALMA'
      Origin = 'NALBALMA'
    end
    object sqlEmpresaNALBAREPARA: TIntegerField
      FieldName = 'NALBAREPARA'
      Origin = 'NALBAREPARA'
    end
    object sqlEmpresaNFACTURANORMAL: TIntegerField
      FieldName = 'NFACTURANORMAL'
      Origin = 'NFACTURANORMAL'
    end
    object sqlEmpresaNFACTURASERVI: TIntegerField
      FieldName = 'NFACTURASERVI'
      Origin = 'NFACTURASERVI'
    end
    object sqlEmpresaNFACTURACOLECTOR: TIntegerField
      FieldName = 'NFACTURACOLECTOR'
      Origin = 'NFACTURACOLECTOR'
    end
    object sqlEmpresaNFACTURATIENDA: TIntegerField
      FieldName = 'NFACTURATIENDA'
      Origin = 'NFACTURATIENDA'
    end
    object sqlEmpresaNFACTURAREPARA: TIntegerField
      FieldName = 'NFACTURAREPARA'
      Origin = 'NFACTURAREPARA'
    end
    object sqlEmpresaNUMTICKET: TIntegerField
      FieldName = 'NUMTICKET'
      Origin = 'NUMTICKET'
    end
    object sqlEmpresaNCOMPRA: TIntegerField
      FieldName = 'NCOMPRA'
      Origin = 'NCOMPRA'
    end
    object sqlEmpresaNTRASPASO: TIntegerField
      FieldName = 'NTRASPASO'
      Origin = 'NTRASPASO'
    end
    object sqlEmpresaREGISTROMERCANTIL: TStringField
      FieldName = 'REGISTROMERCANTIL'
      Origin = 'REGISTROMERCANTIL'
      Size = 200
    end
    object sqlEmpresaPIEFRANORMAL: TMemoField
      FieldName = 'PIEFRANORMAL'
      Origin = 'PIEFRANORMAL'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRASERVI: TMemoField
      FieldName = 'PIEFRASERVI'
      Origin = 'PIEFRASERVI'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRACOLLECTOR: TMemoField
      FieldName = 'PIEFRACOLLECTOR'
      Origin = 'PIEFRACOLLECTOR'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRATIENDA: TMemoField
      FieldName = 'PIEFRATIENDA'
      Origin = 'PIEFRATIENDA'
      BlobType = ftMemo
    end
    object sqlEmpresaPIEFRAREPARA: TMemoField
      FieldName = 'PIEFRAREPARA'
      Origin = 'PIEFRAREPARA'
      BlobType = ftMemo
    end
    object sqlEmpresaIMPTOPEFRAIDE: TSingleField
      FieldName = 'IMPTOPEFRAIDE'
      Origin = 'IMPTOPEFRAIDE'
    end
    object sqlEmpresaCTACONTAVTA1: TStringField
      FieldName = 'CTACONTAVTA1'
      Origin = 'CTACONTAVTA1'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA2: TStringField
      FieldName = 'CTACONTAVTA2'
      Origin = 'CTACONTAVTA2'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA3: TStringField
      FieldName = 'CTACONTAVTA3'
      Origin = 'CTACONTAVTA3'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA4: TStringField
      FieldName = 'CTACONTAVTA4'
      Origin = 'CTACONTAVTA4'
      Size = 9
    end
    object sqlEmpresaCTACONTAVTA5: TStringField
      FieldName = 'CTACONTAVTA5'
      Origin = 'CTACONTAVTA5'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA1: TStringField
      FieldName = 'CTACONTAIVA1'
      Origin = 'CTACONTAIVA1'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA2: TStringField
      FieldName = 'CTACONTAIVA2'
      Origin = 'CTACONTAIVA2'
      Size = 9
    end
    object sqlEmpresaCTACONTAIVA3: TStringField
      FieldName = 'CTACONTAIVA3'
      Origin = 'CTACONTAIVA3'
      Size = 9
    end
    object sqlEmpresaCTACONTARE1: TStringField
      FieldName = 'CTACONTARE1'
      Origin = 'CTACONTARE1'
      Size = 9
    end
    object sqlEmpresaCTACONTARE2: TStringField
      FieldName = 'CTACONTARE2'
      Origin = 'CTACONTARE2'
      Size = 9
    end
    object sqlEmpresaCTACONTARE3: TStringField
      FieldName = 'CTACONTARE3'
      Origin = 'CTACONTARE3'
      Size = 9
    end
    object sqlEmpresaTPCINCRECOMPRACESIO: TSingleField
      FieldName = 'TPCINCRECOMPRACESIO'
      Origin = 'TPCINCRECOMPRACESIO'
    end
    object sqlEmpresaTPCINCRECOMPRAVENTA: TSingleField
      FieldName = 'TPCINCRECOMPRAVENTA'
      Origin = 'TPCINCRECOMPRAVENTA'
    end
    object sqlEmpresaNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlEmpresaRUTAFTP: TStringField
      FieldName = 'RUTAFTP'
      Origin = 'RUTAFTP'
      Size = 80
    end
    object sqlEmpresaRUTAIMAGENFTP: TStringField
      FieldName = 'RUTAIMAGENFTP'
      Origin = 'RUTAIMAGENFTP'
      Size = 80
    end
    object sqlEmpresaRUTAIMAGENPC: TStringField
      FieldName = 'RUTAIMAGENPC'
      Origin = 'RUTAIMAGENPC'
      Size = 80
    end
    object sqlEmpresaRUTACAMPO: TIntegerField
      FieldName = 'RUTACAMPO'
      Origin = 'RUTACAMPO'
    end
    object sqlEmpresaRUTAAVISOSFTP: TStringField
      FieldName = 'RUTAAVISOSFTP'
      Origin = 'RUTAAVISOSFTP'
      Size = 80
    end
    object sqlEmpresaRUTAAVISOSPC: TStringField
      FieldName = 'RUTAAVISOSPC'
      Origin = 'RUTAAVISOSPC'
      Size = 80
    end
    object sqlEmpresaHOSTFTP: TStringField
      FieldName = 'HOSTFTP'
      Origin = 'HOSTFTP'
      Size = 80
    end
    object sqlEmpresaUSUFTP: TStringField
      FieldName = 'USUFTP'
      Origin = 'USUFTP'
    end
    object sqlEmpresaPASSFTP: TStringField
      FieldName = 'PASSFTP'
      Origin = 'PASSFTP'
    end
    object sqlEmpresaDIASRECEGEN: TSmallintField
      FieldName = 'DIASRECEGEN'
      Origin = 'DIASRECEGEN'
    end
    object sqlEmpresaTRANSPORTE: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object sqlEmpresaSWRECARGOE: TSmallintField
      FieldName = 'SWRECARGOE'
      Origin = 'SWRECARGOE'
    end
    object sqlEmpresaSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlEmpresaFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlEmpresaFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlEmpresaHORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlEmpresaFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlEmpresaHORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlEmpresaUSUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlEmpresaNOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlHistoAnte2: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  H.*,A.BARRAS,A.DESCRIPCION,A.TBARRAS,'
      ''
      
        '(Select max(HC.FECHA) From FVHISARTI HC where HC.ID_ARTICULO = H' +
        '.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      
        '                                             and ((HC.SWACABADO ' +
        '= 0) or (HC.SWACABADO is NULL))'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA,0) = coalesce(H.PRECIOVENTA,0)'
      
        '                                             and coalesce(HC.PRE' +
        'CIOVENTA2,0) = coalesce(H.PRECIOVENTA2,0)'
      '                                             ) as FechaCompra,'
      ''
      
        '(Select Sum(HC.CANTIDAD) From FVHISARTI HC where HC.ID_ARTICULO ' +
        '= H.ID_ARTICULO and HC.ADENDUM = H.ADENDUM'
      
        '                                             and HC.clave = '#39'01'#39 +
        ' and HC.id_proveedor = H.id_proveedor'
      '                                             ) as CompraHisto,'
      
        '(Select Sum(HS.CANTIDAD) From FVHISARTI HS  where  HS.ID_ARTICUL' +
        'O  = H.ID_ARTICULO   and HS.ADENDUM = H.ADENDUM  and (HS.clave =' +
        ' '#39'51'#39' or HS.clave = '#39'52'#39' or HS.clave = '#39'53'#39' ) ) as VentaHisto,'
      
        '(Select Sum(HD.CANTIDAD) From FVHISARTI HD  where  HD.ID_ARTICUL' +
        'O = H.ID_ARTICULO   and HD.ADENDUM = H.ADENDUM  and HD.clave = '#39 +
        '54'#39'    and HD.ID_HISARTI <> H.ID_HISARTI ) as DevolHisto,'
      
        '(Select Sum(HM.CANTIDAD) From FVHISARTI HM where  HM.ID_ARTICULO' +
        ' = H.ID_ARTICULO   and HM.ADENDUM = H.ADENDUM  and HM.clave = '#39'5' +
        '5'#39'   and HM.ID_HISARTI <> H.ID_HISARTI ) as MermaHisto,'
      'P.nombre '
      ''
      'From FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ''
      ' where H.ID_HISARTI is not null'
      ' and H.SWCERRADO <> 1'
      ' and (A.swaltabaja = 0 or A.swaltabaja is null)'
      ' and A.swactivado = 1'
      ' and A.periodicidad = 5'
      ' and A.swcontrolfecha = 1'
      ' and H.clave = '#39'01'#39
      ' and H.Fecha ='#39'01/01/2000'#39
      '')
    Left = 688
    Top = 480
    object IntegerField1: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object StringField1: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object DateField1: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object TimeField1: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object StringField2: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object IntegerField2: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object StringField3: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object IntegerField3: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object StringField4: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object IntegerField4: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object IntegerField5: TIntegerField
      FieldName = 'IDDEVOLUCABE'
      Origin = 'IDDEVOLUCABE'
    end
    object IntegerField6: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object IntegerField7: TIntegerField
      FieldName = 'ID_HISARTI01'
      Origin = 'ID_HISARTI01'
    end
    object IntegerField8: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object IntegerField9: TIntegerField
      FieldName = 'PARTIDA'
      Origin = 'PARTIDA'
    end
    object IntegerField10: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object IntegerField11: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object IntegerField12: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object IntegerField13: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object IntegerField14: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object IntegerField15: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object IntegerField16: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object SingleField1: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object SingleField2: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object SingleField3: TSingleField
      FieldName = 'MERMA'
      Origin = 'MERMA'
    end
    object SingleField4: TSingleField
      FieldName = 'CARGO'
      Origin = 'CARGO'
    end
    object SingleField5: TSingleField
      FieldName = 'ABONO'
      Origin = 'ABONO'
    end
    object SingleField6: TSingleField
      FieldName = 'RECLAMADO'
      Origin = 'RECLAMADO'
    end
    object SingleField7: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object SingleField8: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object SingleField9: TSingleField
      FieldName = 'CANTISUSCRI'
      Origin = 'CANTISUSCRI'
    end
    object SingleField10: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
    end
    object SingleField11: TSingleField
      FieldName = 'PRECIOCOMPRA2'
      Origin = 'PRECIOCOMPRA2'
    end
    object SingleField12: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object SingleField13: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object SingleField14: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object SingleField15: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object SingleField16: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
    end
    object SingleField17: TSingleField
      FieldName = 'PRECIOVENTA2'
      Origin = 'PRECIOVENTA2'
    end
    object SmallintField1: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object SingleField18: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
    end
    object SmallintField2: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object SingleField19: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
    end
    object SingleField20: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
    end
    object SmallintField3: TSmallintField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object SingleField21: TSingleField
      FieldName = 'TPCIVA2'
      Origin = 'TPCIVA2'
    end
    object SingleField22: TSingleField
      FieldName = 'TPCRE2'
      Origin = 'TPCRE2'
    end
    object SingleField23: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
    end
    object SingleField24: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
    end
    object SingleField25: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
    end
    object SingleField26: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
    end
    object SingleField27: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
    end
    object SingleField28: TSingleField
      FieldName = 'IMPOBASE2'
      Origin = 'IMPOBASE2'
    end
    object SingleField29: TSingleField
      FieldName = 'IMPOIVA2'
      Origin = 'IMPOIVA2'
    end
    object SingleField30: TSingleField
      FieldName = 'IMPORE2'
      Origin = 'IMPORE2'
    end
    object SingleField31: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
    end
    object SingleField32: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object SingleField33: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object SingleField34: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
    end
    object SingleField35: TSingleField
      FieldName = 'VALORCOSTE2'
      Origin = 'VALORCOSTE2'
    end
    object SingleField36: TSingleField
      FieldName = 'VALORCOSTETOT'
      Origin = 'VALORCOSTETOT'
    end
    object SingleField37: TSingleField
      FieldName = 'VALORCOSTETOT2'
      Origin = 'VALORCOSTETOT2'
    end
    object SingleField38: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
    end
    object SingleField39: TSingleField
      FieldName = 'VALORCOMPRA2'
      Origin = 'VALORCOMPRA2'
    end
    object SingleField40: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
    end
    object SingleField41: TSingleField
      FieldName = 'VALORVENTA2'
      Origin = 'VALORVENTA2'
    end
    object SingleField42: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
    end
    object SingleField43: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object SingleField44: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
    end
    object SingleField45: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object SingleField46: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object SingleField47: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object SingleField48: TSingleField
      FieldName = 'ENCARTE2'
      Origin = 'ENCARTE2'
    end
    object SmallintField4: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object IntegerField17: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object SmallintField5: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object SmallintField6: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object SmallintField7: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object SmallintField8: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object SmallintField9: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object SmallintField10: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object SmallintField11: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object SmallintField12: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object SmallintField13: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object SmallintField14: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object SmallintField15: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object SmallintField16: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object DateField2: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object DateField3: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object DateField4: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object DateField5: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object SmallintField17: TSmallintField
      FieldName = 'SWRECLAMA'
      Origin = 'SWRECLAMA'
    end
    object IntegerField18: TIntegerField
      FieldName = 'ID_HISARTIRE'
      Origin = 'ID_HISARTIRE'
    end
    object SmallintField18: TSmallintField
      FieldName = 'SWACABADO'
      Origin = 'SWACABADO'
    end
    object StringField5: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object StringField6: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object IntegerField19: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object DateField6: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'FECHACOMPRA'
      Origin = 'FECHACOMPRA'
      ProviderFlags = []
      ReadOnly = True
    end
    object FloatField1: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'COMPRAHISTO'
      Origin = 'COMPRAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FloatField2: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAHISTO'
      Origin = 'VENTAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FloatField3: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLHISTO'
      Origin = 'DEVOLHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object FloatField4: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMAHISTO'
      Origin = 'MERMAHISTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object StringField7: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object FloatField5: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PrecioTotal'
      Calculated = True
    end
  end
  object sqlListaDistri: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select * From FMPROVE'
      'where ID_PROVEEDOR is not null'
      'Order by ID_PROVEEDOR')
    Left = 24
    Top = 464
    object IntegerField20: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object SmallintField19: TSmallintField
      FieldName = 'TPROVEEDOR'
      Origin = 'TPROVEEDOR'
    end
    object StringField8: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object StringField9: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object IntegerField21: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object StringField10: TStringField
      FieldName = 'NOMBRE2'
      Origin = 'NOMBRE2'
      Size = 40
    end
    object StringField11: TStringField
      FieldName = 'DIRECCION'
      Origin = 'DIRECCION'
      Size = 40
    end
    object StringField12: TStringField
      FieldName = 'DIRECCION2'
      Origin = 'DIRECCION2'
      Size = 40
    end
    object StringField13: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 40
    end
    object StringField14: TStringField
      FieldName = 'PROVINCIA'
      Origin = 'PROVINCIA'
      Size = 40
    end
    object StringField15: TStringField
      FieldName = 'CPOSTAL'
      Origin = 'CPOSTAL'
      Size = 8
    end
    object StringField16: TStringField
      FieldName = 'CPAIS'
      Origin = 'CPAIS'
      FixedChar = True
      Size = 3
    end
    object StringField17: TStringField
      FieldName = 'CPROVINCIA'
      Origin = 'CPROVINCIA'
      FixedChar = True
      Size = 2
    end
    object StringField18: TStringField
      FieldName = 'CONTACTO1NOMBRE'
      Origin = 'CONTACTO1NOMBRE'
      Size = 30
    end
    object StringField19: TStringField
      FieldName = 'CONTACTO1CARGO'
      Origin = 'CONTACTO1CARGO'
    end
    object StringField20: TStringField
      FieldName = 'CONTACTO2NOMBRE'
      Origin = 'CONTACTO2NOMBRE'
      Size = 30
    end
    object StringField21: TStringField
      FieldName = 'CONTACTO2CARGO'
      Origin = 'CONTACTO2CARGO'
    end
    object StringField22: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 15
    end
    object StringField23: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 40
    end
    object StringField24: TStringField
      FieldName = 'MOVIL'
      Origin = 'MOVIL'
      Size = 15
    end
    object StringField25: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 15
    end
    object StringField26: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 80
    end
    object StringField27: TStringField
      FieldName = 'WEB'
      Origin = 'WEB'
      Size = 80
    end
    object StringField28: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object MemoField1: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object StringField29: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      Size = 5
    end
    object StringField30: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 5
    end
    object StringField31: TStringField
      FieldName = 'DC'
      Origin = 'DC'
      Size = 2
    end
    object StringField32: TStringField
      FieldName = 'CUENTABANCO'
      Origin = 'CUENTABANCO'
      Size = 10
    end
    object SmallintField20: TSmallintField
      FieldName = 'TEFECTO'
      Origin = 'TEFECTO'
    end
    object SmallintField21: TSmallintField
      FieldName = 'EFECTOS'
      Origin = 'EFECTOS'
    end
    object SmallintField22: TSmallintField
      FieldName = 'FRECUENCIA'
      Origin = 'FRECUENCIA'
    end
    object SmallintField23: TSmallintField
      FieldName = 'DIASPRIMERVTO'
      Origin = 'DIASPRIMERVTO'
    end
    object SmallintField24: TSmallintField
      FieldName = 'DIAFIJO1'
      Origin = 'DIAFIJO1'
    end
    object SmallintField25: TSmallintField
      FieldName = 'DIAFIJO2'
      Origin = 'DIAFIJO2'
    end
    object SmallintField26: TSmallintField
      FieldName = 'DIAFIJO3'
      Origin = 'DIAFIJO3'
    end
    object IntegerField22: TIntegerField
      FieldName = 'TDTO'
      Origin = 'TDTO'
    end
    object IntegerField23: TIntegerField
      FieldName = 'TPAGO'
      Origin = 'TPAGO'
    end
    object SmallintField27: TSmallintField
      FieldName = 'SWTFACTURACION'
      Origin = 'SWTFACTURACION'
    end
    object SmallintField28: TSmallintField
      FieldName = 'SWBLOQUEO'
      Origin = 'SWBLOQUEO'
    end
    object StringField33: TStringField
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
    end
    object SmallintField29: TSmallintField
      FieldName = 'TRANSPORTE'
      Origin = 'TRANSPORTE'
    end
    object StringField34: TStringField
      FieldName = 'RUTA'
      Origin = 'RUTA'
      Size = 10
    end
    object IntegerField24: TIntegerField
      FieldName = 'PROVEEDORHOSTING'
      Origin = 'PROVEEDORHOSTING'
    end
    object StringField35: TStringField
      FieldName = 'NOMBREHOSTING'
      Origin = 'NOMBREHOSTING'
      Size = 40
    end
    object DateField7: TDateField
      FieldName = 'ULTIMAFECHADESCARGA'
      Origin = 'ULTIMAFECHADESCARGA'
    end
    object DateField8: TDateField
      FieldName = 'ULTIMAFECHAENVIO'
      Origin = 'ULTIMAFECHAENVIO'
    end
    object StringField36: TStringField
      FieldName = 'COLUMNASEXCEL'
      Origin = 'COLUMNASEXCEL'
      Size = 30
    end
    object IntegerField25: TIntegerField
      FieldName = 'DESDEFILA'
      Origin = 'DESDEFILA'
    end
    object SmallintField30: TSmallintField
      FieldName = 'SWRECIBIR'
      Origin = 'SWRECIBIR'
    end
    object SmallintField31: TSmallintField
      FieldName = 'SWENVIAR'
      Origin = 'SWENVIAR'
    end
    object SmallintField32: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object DateField9: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object DateField10: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object TimeField2: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object DateField11: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object TimeField3: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object StringField37: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object StringField38: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
  end
  object sqlCabeNuevo: TFDQuery
    AfterScroll = sqlCabe1AfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select FC.IDSTOCABE, FC.SWTIPODOCU, FC.PAQUETE, FC.fecha, FC.swd' +
        'evolucion'
      ', FC.doctoprove, FC.doctoprovefecha'
      ', Fc.id_proveedor, FC.NOMBRE, FC.nif'
      ', P.maxpaquete'
      ''
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where IDSTOCABE > 0 AND SWTIPODOCU = 22'
      'Order by IDSTOCABE')
    Left = 592
    Top = 16
    object sqlCabeNuevoIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeNuevoSWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabeNuevoNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeNuevoPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlCabeNuevoFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabeNuevoDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabeNuevoDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabeNuevoID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabeNuevoNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeNuevoMAXPAQUETE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
      ProviderFlags = []
    end
    object sqlCabeNuevoSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
  end
  object SP_GEN_STOCABE: TFDStoredProc
    Connection = DMppal.FDConnection1
    StoredProcName = 'SP_GEN_STOCABE'
    Left = 720
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'NORDEN'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object sqlLineaTotal: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'SELECT SUM(Devueltos) as Total'
      'FROM FVHISARTI'
      'WHERE IDSTOCABE is not NULL'
      '')
    Left = 112
    Top = 88
    object sqlLineaTotalTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlHisto: TFDQuery
    Connection = DMppal.FDConnection1
    Left = 208
    Top = 88
  end
  object sqlHisArtiCabe1: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FC.*'
      ', P.maxpaquete'
      ''
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where FC.IDSTOCABE > 0 AND FC.SWTIPODOCU = 2'
      'Order by FC.IDSTOCABE')
    Left = 271
    Top = 28
    object sqlHisArtiCabe1IDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisArtiCabe1SWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlHisArtiCabe1NDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlHisArtiCabe1FECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHisArtiCabe1NALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHisArtiCabe1NALMACENORIGEN: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object sqlHisArtiCabe1NALMACENDESTINO: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object sqlHisArtiCabe1ID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHisArtiCabe1ID_DIRECCION: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object sqlHisArtiCabe1FABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object sqlHisArtiCabe1SWDOCTOPROVE: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object sqlHisArtiCabe1DOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlHisArtiCabe1DOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlHisArtiCabe1FECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlHisArtiCabe1FECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlHisArtiCabe1NOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlHisArtiCabe1NIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlHisArtiCabe1MENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlHisArtiCabe1OBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlHisArtiCabe1PAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlHisArtiCabe1SWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlHisArtiCabe1SWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisArtiCabe1SWFRATIENDA: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object sqlHisArtiCabe1DEVOLUSWTIPOINC: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object sqlHisArtiCabe1DEVOLUFECHASEL: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object sqlHisArtiCabe1DEVOLUDOCTOPROVE: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object sqlHisArtiCabe1DEVOLUIDSTOCABE: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object sqlHisArtiCabe1ID_FRAPROVE: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object sqlHisArtiCabe1SWMARCA: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object sqlHisArtiCabe1SWMARCA2: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object sqlHisArtiCabe1SWMARCA3: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object sqlHisArtiCabe1SWMARCA4: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object sqlHisArtiCabe1SWMARCA5: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object sqlHisArtiCabe1SEMANA: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object sqlHisArtiCabe1SWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlHisArtiCabe1FECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlHisArtiCabe1FECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlHisArtiCabe1HORAALTA: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object sqlHisArtiCabe1FECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object sqlHisArtiCabe1HORAULTI: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object sqlHisArtiCabe1USUULTI: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object sqlHisArtiCabe1NOTAULTI: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object sqlHisArtiCabe1MAXPAQUETE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlcabe1COPIA: TFDQuery
    AfterScroll = sqlCabe1AfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select FC.*'
      ', P.maxpaquete'
      ''
      'From FVSTOCABE FC'
      'left join FMPROVE P on (P.id_proveedor = FC.id_proveedor)'
      ''
      'where FC.IDSTOCABE > 0 AND FC.SWTIPODOCU = 2'
      'Order by FC.IDSTOCABE')
    Left = 192
    Top = 24
    object IntegerField26: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object SmallintField33: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object IntegerField27: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object DateField12: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object IntegerField28: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object IntegerField29: TIntegerField
      FieldName = 'NALMACENORIGEN'
      Origin = 'NALMACENORIGEN'
    end
    object IntegerField30: TIntegerField
      FieldName = 'NALMACENDESTINO'
      Origin = 'NALMACENDESTINO'
    end
    object IntegerField31: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object StringField39: TStringField
      FieldName = 'FABRICANTE'
      Origin = 'FABRICANTE'
    end
    object SmallintField34: TSmallintField
      FieldName = 'SWDOCTOPROVE'
      Origin = 'SWDOCTOPROVE'
    end
    object StringField40: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object DateField13: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object DateField14: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object DateField15: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object StringField41: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object StringField42: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object StringField43: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object MemoField2: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object IntegerField32: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object SmallintField35: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object SmallintField36: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object SmallintField37: TSmallintField
      FieldName = 'SWFRATIENDA'
      Origin = 'SWFRATIENDA'
    end
    object SmallintField38: TSmallintField
      FieldName = 'DEVOLUSWTIPOINC'
      Origin = 'DEVOLUSWTIPOINC'
    end
    object DateField16: TDateField
      FieldName = 'DEVOLUFECHASEL'
      Origin = 'DEVOLUFECHASEL'
    end
    object StringField44: TStringField
      FieldName = 'DEVOLUDOCTOPROVE'
      Origin = 'DEVOLUDOCTOPROVE'
    end
    object IntegerField33: TIntegerField
      FieldName = 'DEVOLUIDSTOCABE'
      Origin = 'DEVOLUIDSTOCABE'
    end
    object IntegerField34: TIntegerField
      FieldName = 'ID_FRAPROVE'
      Origin = 'ID_FRAPROVE'
    end
    object SmallintField39: TSmallintField
      FieldName = 'SWMARCA'
      Origin = 'SWMARCA'
    end
    object SmallintField40: TSmallintField
      FieldName = 'SWMARCA2'
      Origin = 'SWMARCA2'
    end
    object SmallintField41: TSmallintField
      FieldName = 'SWMARCA3'
      Origin = 'SWMARCA3'
    end
    object SmallintField42: TSmallintField
      FieldName = 'SWMARCA4'
      Origin = 'SWMARCA4'
    end
    object SmallintField43: TSmallintField
      FieldName = 'SWMARCA5'
      Origin = 'SWMARCA5'
    end
    object SmallintField44: TSmallintField
      FieldName = 'SEMANA'
      Origin = 'SEMANA'
    end
    object SmallintField45: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object DateField17: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object DateField18: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object TimeField4: TTimeField
      FieldName = 'HORAALTA'
      Origin = 'HORAALTA'
    end
    object DateField19: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
    object TimeField5: TTimeField
      FieldName = 'HORAULTI'
      Origin = 'HORAULTI'
    end
    object StringField45: TStringField
      FieldName = 'USUULTI'
      Origin = 'USUULTI'
    end
    object StringField46: TStringField
      FieldName = 'NOTAULTI'
      Origin = 'NOTAULTI'
      Size = 30
    end
    object IntegerField35: TIntegerField
      FieldName = 'ID_DIRECCION'
      Origin = 'ID_DIRECCION'
    end
    object IntegerField36: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXPAQUETE'
      Origin = 'MAXPAQUETE'
      ProviderFlags = []
    end
    object StringField47: TStringField
      FieldKind = fkLookup
      FieldName = 'REFEPROVEEDOR'
      LookupDataSet = sqlProveS
      LookupKeyFields = 'ID_PROVEEDOR'
      LookupResultField = 'REFEPROVEEDOR'
      KeyFields = 'ID_PROVEEDOR'
      Lookup = True
    end
    object StringField48: TStringField
      FieldKind = fkLookup
      FieldName = 'RUTA'
      LookupDataSet = sqlProveS
      LookupKeyFields = 'ID_PROVEEDOR'
      LookupResultField = 'RUTA'
      KeyFields = 'ID_PROVEEDOR'
      Lookup = True
    end
    object StringField49: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TextoPaquete'
      Size = 50
    end
    object FloatField6: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TotalDevueltos'
    end
  end
  object sqlCabeS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select'
      
        ' S.IDSTOCABE, S.SWTIPODOCU, S.NDOCSTOCK, S.FECHA, S.NALMACEN, S.' +
        'ID_PROVEEDOR, S.DOCTOPROVE, S.DOCTOPROVEFECHA, S.FECHACARGO, S.F' +
        'ECHAABONO,  S.NOMBRE, S.NIF, S.MENSAJEAVISO, S.SWDEVOLUCION'
      ', S.PAQUETE'
      ', P.REFEPROVEEDOR, P.RUTA'
      
        ', (Select Count(distinct FV.paquete) from FVHISARTI FV Where FV.' +
        'idstocabe = S.idstocabe) as TotalPaquetes'
      
        ', (Select Max(coalesce(FV.paquete,0)) from FVHISARTI FV Where FV' +
        '.idstocabe = S.idstocabe) as UltimoPaquete'
      
        ', (SELECT SUM(FV.Devueltos) FROM FVHISARTI FV Where FV.IDSTOCABE' +
        ' = S.IDSTOCABE) as TotalDevueltos'
      ''
      ' From FVSTOCABE S'
      ''
      'INNER JOIN FMPROVE P'
      'ON (S.ID_PROVEEDOR = P.ID_PROVEEDOR)'
      ''
      'where S.SWDEVOLUCION > 0'
      'and (S.SWTIPODOCU = 2 or S.SWTIPODOCU = 22)'
      'Order by S.fecha DESC')
    Left = 24
    Top = 24
    object sqlCabeSIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlCabeSSWTIPODOCU: TSmallintField
      FieldName = 'SWTIPODOCU'
      Origin = 'SWTIPODOCU'
    end
    object sqlCabeSNDOCSTOCK: TIntegerField
      FieldName = 'NDOCSTOCK'
      Origin = 'NDOCSTOCK'
    end
    object sqlCabeSFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlCabeSNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlCabeSID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlCabeSDOCTOPROVE: TStringField
      FieldName = 'DOCTOPROVE'
      Origin = 'DOCTOPROVE'
    end
    object sqlCabeSDOCTOPROVEFECHA: TDateField
      FieldName = 'DOCTOPROVEFECHA'
      Origin = 'DOCTOPROVEFECHA'
    end
    object sqlCabeSFECHACARGO: TDateField
      FieldName = 'FECHACARGO'
      Origin = 'FECHACARGO'
    end
    object sqlCabeSFECHAABONO: TDateField
      FieldName = 'FECHAABONO'
      Origin = 'FECHAABONO'
    end
    object sqlCabeSNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 40
    end
    object sqlCabeSNIF: TStringField
      FieldName = 'NIF'
      Origin = 'NIF'
      Size = 15
    end
    object sqlCabeSMENSAJEAVISO: TStringField
      FieldName = 'MENSAJEAVISO'
      Origin = 'MENSAJEAVISO'
      Size = 80
    end
    object sqlCabeSSWDEVOLUCION: TSmallintField
      FieldName = 'SWDEVOLUCION'
      Origin = 'SWDEVOLUCION'
    end
    object sqlCabeSPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlCabeSREFEPROVEEDOR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'REFEPROVEEDOR'
      Origin = 'REFEPROVEEDOR'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlCabeSRUTA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'RUTA'
      Origin = 'RUTA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object sqlCabeSTOTALPAQUETES: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALPAQUETES'
      Origin = 'TOTALPAQUETES'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlCabeSULTIMOPAQUETE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ULTIMOPAQUETE'
      Origin = 'ULTIMOPAQUETE'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlCabeSTOTALDEVUELTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTALDEVUELTOS'
      Origin = 'TOTALDEVUELTOS'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlPaquetes: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'select h.idstocabe, h.paquete, sum(h.devueltos) as Ejemplares'
      ',p.nombre as Distribuidora'
      ''
      'from fvhisarti h'
      'left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ''
      'where H.IDSTOCABE = 0'
      'And H.ID_PROVEEDOR = 0'
      'and H.CLAVE = '#39'54'#39' '
      ''
      'group by h.idstocabe, h.paquete, p.nombre')
    Left = 696
    Top = 216
    object sqlPaquetesIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlPaquetesPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
    object sqlPaquetesEJEMPLARES: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EJEMPLARES'
      Origin = 'EJEMPLARES'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlPaquetesDISTRIBUIDORA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUIDORA'
      Origin = 'DISTRIBUIDORA'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
  end
  object sqlArticulo: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select  * From  FMARTICULO'
      'where ID_ARTICULO = :ID_ARTICULO')
    Left = 256
    Top = 232
    ParamData = <
      item
        Name = 'ID_ARTICULO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object sqlArticuloID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArticuloTBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArticuloBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArticuloADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArticuloFECHAADENDUM: TDateField
      FieldName = 'FECHAADENDUM'
      Origin = 'FECHAADENDUM'
    end
    object sqlArticuloNADENDUM: TIntegerField
      FieldName = 'NADENDUM'
      Origin = 'NADENDUM'
    end
    object sqlArticuloDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArticuloDESCRIPCION2: TStringField
      FieldName = 'DESCRIPCION2'
      Origin = 'DESCRIPCION2'
    end
    object sqlArticuloIBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArticuloISBN: TStringField
      FieldName = 'ISBN'
      Origin = 'ISBN'
      Size = 17
    end
    object sqlArticuloEDITOR: TIntegerField
      FieldName = 'EDITOR'
      Origin = 'EDITOR'
    end
    object sqlArticuloPRECIO1: TSingleField
      FieldName = 'PRECIO1'
      Origin = 'PRECIO1'
    end
    object sqlArticuloPRECIO2: TSingleField
      FieldName = 'PRECIO2'
      Origin = 'PRECIO2'
    end
    object sqlArticuloPRECIO3: TSingleField
      FieldName = 'PRECIO3'
      Origin = 'PRECIO3'
    end
    object sqlArticuloPRECIO4: TSingleField
      FieldName = 'PRECIO4'
      Origin = 'PRECIO4'
    end
    object sqlArticuloPRECIO5: TSingleField
      FieldName = 'PRECIO5'
      Origin = 'PRECIO5'
    end
    object sqlArticuloPRECIO6: TSingleField
      FieldName = 'PRECIO6'
      Origin = 'PRECIO6'
    end
    object sqlArticuloPRECIO7: TSingleField
      FieldName = 'PRECIO7'
      Origin = 'PRECIO7'
    end
    object sqlArticuloTPCENCARTE1: TSingleField
      FieldName = 'TPCENCARTE1'
      Origin = 'TPCENCARTE1'
    end
    object sqlArticuloTPCENCARTE2: TSingleField
      FieldName = 'TPCENCARTE2'
      Origin = 'TPCENCARTE2'
    end
    object sqlArticuloTPCENCARTE3: TSingleField
      FieldName = 'TPCENCARTE3'
      Origin = 'TPCENCARTE3'
    end
    object sqlArticuloTPCENCARTE4: TSingleField
      FieldName = 'TPCENCARTE4'
      Origin = 'TPCENCARTE4'
    end
    object sqlArticuloTPCENCARTE5: TSingleField
      FieldName = 'TPCENCARTE5'
      Origin = 'TPCENCARTE5'
    end
    object sqlArticuloTPCENCARTE6: TSingleField
      FieldName = 'TPCENCARTE6'
      Origin = 'TPCENCARTE6'
    end
    object sqlArticuloTPCENCARTE7: TSingleField
      FieldName = 'TPCENCARTE7'
      Origin = 'TPCENCARTE7'
    end
    object sqlArticuloPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
    end
    object sqlArticuloPRECIOCOSTE2: TSingleField
      FieldName = 'PRECIOCOSTE2'
      Origin = 'PRECIOCOSTE2'
    end
    object sqlArticuloPRECIOCOSTETOT: TSingleField
      FieldName = 'PRECIOCOSTETOT'
      Origin = 'PRECIOCOSTETOT'
    end
    object sqlArticuloPRECIOCOSTETOT2: TSingleField
      FieldName = 'PRECIOCOSTETOT2'
      Origin = 'PRECIOCOSTETOT2'
    end
    object sqlArticuloMARGEN: TSingleField
      FieldName = 'MARGEN'
      Origin = 'MARGEN'
    end
    object sqlArticuloMARGEN2: TSingleField
      FieldName = 'MARGEN2'
      Origin = 'MARGEN2'
    end
    object sqlArticuloMARGENMAXIMO: TSingleField
      FieldName = 'MARGENMAXIMO'
      Origin = 'MARGENMAXIMO'
    end
    object sqlArticuloEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
    object sqlArticuloREFEPROVE: TStringField
      FieldName = 'REFEPROVE'
      Origin = 'REFEPROVE'
    end
    object sqlArticuloPERIODICIDAD: TIntegerField
      FieldName = 'PERIODICIDAD'
      Origin = 'PERIODICIDAD'
    end
    object sqlArticuloCADUCIDAD: TIntegerField
      FieldName = 'CADUCIDAD'
      Origin = 'CADUCIDAD'
    end
    object sqlArticuloTIVA: TIntegerField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlArticuloTIVA2: TIntegerField
      FieldName = 'TIVA2'
      Origin = 'TIVA2'
    end
    object sqlArticuloSWRECARGO: TSmallintField
      FieldName = 'SWRECARGO'
      Origin = 'SWRECARGO'
    end
    object sqlArticuloSWACTIVADO: TSmallintField
      FieldName = 'SWACTIVADO'
      Origin = 'SWACTIVADO'
    end
    object sqlArticuloSWINGRESO: TSmallintField
      FieldName = 'SWINGRESO'
      Origin = 'SWINGRESO'
    end
    object sqlArticuloSWCONTROLFECHA: TSmallintField
      FieldName = 'SWCONTROLFECHA'
      Origin = 'SWCONTROLFECHA'
    end
    object sqlArticuloSWTPRECIO: TSmallintField
      FieldName = 'SWTPRECIO'
      Origin = 'SWTPRECIO'
    end
    object sqlArticuloSWORIGEN: TIntegerField
      FieldName = 'SWORIGEN'
      Origin = 'SWORIGEN'
    end
    object sqlArticuloTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      Size = 1
    end
    object sqlArticuloTPRODUCTO: TIntegerField
      FieldName = 'TPRODUCTO'
      Origin = 'TPRODUCTO'
    end
    object sqlArticuloTCONTENIDO: TIntegerField
      FieldName = 'TCONTENIDO'
      Origin = 'TCONTENIDO'
    end
    object sqlArticuloTEMATICA: TIntegerField
      FieldName = 'TEMATICA'
      Origin = 'TEMATICA'
    end
    object sqlArticuloAUTOR: TIntegerField
      FieldName = 'AUTOR'
      Origin = 'AUTOR'
    end
    object sqlArticuloOBSERVACIONES: TMemoField
      FieldName = 'OBSERVACIONES'
      Origin = 'OBSERVACIONES'
      BlobType = ftMemo
    end
    object sqlArticuloSTOCKMINIMO: TSingleField
      FieldName = 'STOCKMINIMO'
      Origin = 'STOCKMINIMO'
    end
    object sqlArticuloSTOCKMAXIMO: TSingleField
      FieldName = 'STOCKMAXIMO'
      Origin = 'STOCKMAXIMO'
    end
    object sqlArticuloFAMILIA: TStringField
      FieldName = 'FAMILIA'
      Origin = 'FAMILIA'
      Size = 10
    end
    object sqlArticuloFAMILIAVENTA: TIntegerField
      FieldName = 'FAMILIAVENTA'
      Origin = 'FAMILIAVENTA'
    end
    object sqlArticuloORDENVENTA: TIntegerField
      FieldName = 'ORDENVENTA'
      Origin = 'ORDENVENTA'
    end
    object sqlArticuloSTOCKMINIMOESTANTE: TSingleField
      FieldName = 'STOCKMINIMOESTANTE'
      Origin = 'STOCKMINIMOESTANTE'
    end
    object sqlArticuloSTOCKMAXIMOESTANTE: TSingleField
      FieldName = 'STOCKMAXIMOESTANTE'
      Origin = 'STOCKMAXIMOESTANTE'
    end
    object sqlArticuloSTOCKVENTAESTANTE: TSingleField
      FieldName = 'STOCKVENTAESTANTE'
      Origin = 'STOCKVENTAESTANTE'
    end
    object sqlArticuloREPONERESTANTE: TSingleField
      FieldName = 'REPONERESTANTE'
      Origin = 'REPONERESTANTE'
    end
    object sqlArticuloUNIDADESCOMPRA: TSingleField
      FieldName = 'UNIDADESCOMPRA'
      Origin = 'UNIDADESCOMPRA'
    end
    object sqlArticuloUNIDADESVENTA: TSingleField
      FieldName = 'UNIDADESVENTA'
      Origin = 'UNIDADESVENTA'
    end
    object sqlArticuloIMAGEN: TStringField
      FieldName = 'IMAGEN'
      Origin = 'IMAGEN'
      Size = 80
    end
    object sqlArticuloCOD_ARTICU: TStringField
      FieldName = 'COD_ARTICU'
      Origin = 'COD_ARTICU'
      Size = 12
    end
    object sqlArticuloGRUPO: TIntegerField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
    end
    object sqlArticuloSUBGRUPO: TIntegerField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
    end
    object sqlArticuloSWALTABAJA: TSmallintField
      FieldName = 'SWALTABAJA'
      Origin = 'SWALTABAJA'
    end
    object sqlArticuloFECHABAJA: TDateField
      FieldName = 'FECHABAJA'
      Origin = 'FECHABAJA'
    end
    object sqlArticuloFECHAALTA: TDateField
      FieldName = 'FECHAALTA'
      Origin = 'FECHAALTA'
    end
    object sqlArticuloFECHAULTI: TDateField
      FieldName = 'FECHAULTI'
      Origin = 'FECHAULTI'
    end
  end
end
