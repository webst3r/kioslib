object FormAlbaran: TFormAlbaran
  Left = 0
  Top = 0
  ClientHeight = 532
  ClientWidth = 1008
  Caption = 'FormAlbaran'
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object UniPageControl3: TUniPageControl
    Left = 0
    Top = 0
    Width = 1008
    Height = 532
    Hint = ''
    ShowHint = True
    ActivePage = UniTabSheet7
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    ClientEvents.ExtEvents.Strings = (
      
        'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
        'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
        'Style("border-width", 0);'#13#10'}')
    TabOrder = 0
    object UniTabSheet7: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'UniTabSheet1'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
          'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
          'Style("border-width", 0);'#13#10'}')
      object UniPanel13: TUniPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 281
        Hint = ''
        ShowHint = True
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        ClientEvents.ExtEvents.Strings = (
          
            'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
            'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
            'Style("border-width", 0);'#13#10'}')
        Caption = ''
        object UniLabel47: TUniLabel
          Left = 68
          Top = -1
          Width = 95
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'C'#243'digo Art'#237'culo [F5]'
          TabOrder = 1
        end
        object UniLabel48: TUniLabel
          Left = 448
          Top = -1
          Width = 26
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Suscr'
          TabOrder = 2
        end
        object UniLabel49: TUniLabel
          Left = 302
          Top = -1
          Width = 64
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ejemplar [F7]'
          TabOrder = 3
        end
        object UniLabel50: TUniLabel
          Left = 389
          Top = -1
          Width = 23
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cant'
          TabOrder = 4
        end
        object edBarras: TUniDBEdit
          Left = 66
          Top = 13
          Width = 228
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 5
        end
        object edAdendum: TUniDBEdit
          Left = 294
          Top = 13
          Width = 75
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 6
        end
        object edCantiEnAlba: TUniDBEdit
          Left = 369
          Top = 13
          Width = 53
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 7
        end
        object edSuscripcion: TUniDBEdit
          Left = 422
          Top = 13
          Width = 53
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 8
        end
        object btSumarRecepcion: TUniSpeedButton
          Left = 475
          Top = 6
          Width = 46
          Height = 38
          Hint = ''
          ShowHint = True
          Caption = ''
          ParentColor = False
          Color = clWindow
          TabOrder = 9
        end
        object btRestarRecepcion: TUniSpeedButton
          Left = 520
          Top = 6
          Width = 46
          Height = 38
          Hint = ''
          ShowHint = True
          Caption = ''
          ParentColor = False
          Color = clWindow
          TabOrder = 10
        end
        object UniDBEdit50: TUniDBEdit
          Left = 66
          Top = 56
          Width = 99
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 11
        end
        object UniDBEdit51: TUniDBEdit
          Left = 165
          Top = 56
          Width = 51
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 12
        end
        object UniDBEdit52: TUniDBEdit
          Left = 216
          Top = 56
          Width = 306
          Height = 20
          Hint = ''
          ShowHint = True
          TabOrder = 13
        end
        object UniLabel52: TUniLabel
          Left = 524
          Top = 63
          Width = 38
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Importe'
          TabOrder = 14
        end
        object UniDBEdit53: TUniDBEdit
          Left = 222
          Top = 78
          Width = 257
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 15
        end
        object UniDBEdit54: TUniDBEdit
          Left = 140
          Top = 78
          Width = 82
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 16
        end
        object UniLabel53: TUniLabel
          Left = 79
          Top = 84
          Width = 59
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Ref.Dist[F3]'
          TabOrder = 17
        end
        object UniDBEdit55: TUniDBEdit
          Left = 479
          Top = 78
          Width = 87
          Height = 27
          Hint = ''
          ShowHint = True
          TabOrder = 18
        end
        object btPrior: TUniSpeedButton
          Left = 408
          Top = 122
          Width = 33
          Height = 33
          Hint = ''
          ShowHint = True
          Caption = ''
          ParentColor = False
          Color = clWindow
          TabOrder = 19
        end
        object btNext: TUniSpeedButton
          Left = 440
          Top = 122
          Width = 33
          Height = 33
          Hint = ''
          ShowHint = True
          Caption = ''
          ParentColor = False
          Color = clWindow
          TabOrder = 20
        end
        object btGrabaLinea: TUniBitBtn
          Left = 475
          Top = 137
          Width = 46
          Height = 38
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 21
        end
        object btCancelaLinea: TUniBitBtn
          Left = 520
          Top = 137
          Width = 46
          Height = 38
          Hint = ''
          ShowHint = True
          Caption = ''
          TabOrder = 22
        end
        object edAbono: TUniDBDateTimePicker
          Left = 341
          Top = 161
          Width = 92
          Height = 20
          Hint = ''
          ShowHint = True
          DateTime = 43371.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 23
        end
        object edCargo: TUniDBDateTimePicker
          Left = 248
          Top = 161
          Width = 92
          Height = 20
          Hint = ''
          ShowHint = True
          DateTime = 43371.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 24
        end
        object edAvisosDevol: TUniDBDateTimePicker
          Left = 155
          Top = 161
          Width = 92
          Height = 20
          Hint = ''
          ShowHint = True
          DateTime = 43371.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 25
        end
        object edDevolucion: TUniDBDateTimePicker
          Left = 61
          Top = 161
          Width = 92
          Height = 20
          Hint = ''
          ShowHint = True
          DateTime = 43371.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 26
        end
        object UniLabel54: TUniLabel
          Left = 73
          Top = 145
          Width = 52
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Devolucion'
          TabOrder = 27
        end
        object UniLabel55: TUniLabel
          Left = 164
          Top = 145
          Width = 57
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Avisos Dev.'
          TabOrder = 28
        end
        object UniLabel56: TUniLabel
          Left = 277
          Top = 145
          Width = 29
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Cargo'
          TabOrder = 29
        end
        object UniLabel57: TUniLabel
          Left = 365
          Top = 145
          Width = 31
          Height = 13
          Hint = ''
          ShowHint = True
          Caption = 'Abono'
          TabOrder = 30
        end
        object UniPanel19: TUniPanel
          Left = 779
          Top = 192
          Width = 181
          Height = 87
          Hint = ''
          ShowHint = True
          TabOrder = 31
          Caption = ''
          object UniLabel81: TUniLabel
            Left = 24
            Top = 23
            Width = 102
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Num Inferior al '#250'ltimo'
            TabOrder = 1
          end
          object UniPanel20: TUniPanel
            Left = 8
            Top = 39
            Width = 166
            Height = 36
            Hint = ''
            ShowHint = True
            TabOrder = 2
            Caption = ''
            object edNumInferior: TUniDBEdit
              Left = 5
              Top = 8
              Width = 21
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 1
            end
            object btNuminferiorOk: TUniBitBtn
              Left = 32
              Top = 6
              Width = 65
              Height = 25
              Hint = ''
              ShowHint = True
              Caption = 'btNuminferiorOk'
              TabOrder = 2
            end
            object btNumInferiorNo: TUniBitBtn
              Left = 97
              Top = 6
              Width = 65
              Height = 25
              Hint = ''
              ShowHint = True
              Caption = 'UniBitBtn34'
              TabOrder = 3
            end
          end
        end
        object UniPanel1: TUniPanel
          Left = 583
          Top = 192
          Width = 195
          Height = 87
          Hint = ''
          ShowHint = True
          TabOrder = 32
          Caption = ''
          object UniDBEdit89: TUniDBEdit
            Left = 65
            Top = 27
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 1
          end
          object UniLabel78: TUniLabel
            Left = 18
            Top = 13
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Venta'
            TabOrder = 2
          end
          object UniDBEdit90: TUniDBEdit
            Left = 1
            Top = 48
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 3
          end
          object UniDBEdit91: TUniDBEdit
            Left = 1
            Top = 27
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 4
          end
          object UniDBEdit92: TUniDBEdit
            Left = 65
            Top = 48
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 5
          end
          object UniDBEdit93: TUniDBEdit
            Left = 129
            Top = 49
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 6
          end
          object UniDBEdit94: TUniDBEdit
            Left = 129
            Top = 27
            Width = 64
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 7
          end
          object UniLabel79: TUniLabel
            Left = 85
            Top = 13
            Width = 28
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Coste'
            TabOrder = 8
          end
          object UniLabel80: TUniLabel
            Left = 131
            Top = 13
            Width = 59
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'V.Coste+RE'
            TabOrder = 9
          end
        end
        object UniPanel2: TUniPanel
          Left = -3
          Top = -6
          Width = 61
          Height = 281
          Hint = ''
          ShowHint = True
          TabOrder = 33
          Caption = ''
          object btNuevaLinea: TUniBitBtn
            Left = 3
            Top = 7
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Nuevo'
            TabOrder = 1
          end
          object btBorrarLinea: TUniBitBtn
            Left = 3
            Top = 53
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Anula'
            TabOrder = 2
          end
          object btReclamar: TUniBitBtn
            Left = 3
            Top = 99
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Reclamar'
            TabOrder = 3
          end
          object btResumenCargos: TUniBitBtn
            Left = 3
            Top = 145
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cargos'
            TabOrder = 4
          end
          object btPrecios: TUniBitBtn
            Left = 3
            Top = 191
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Pre Hoy'
            TabOrder = 5
          end
          object btEditarLinea: TUniBitBtn
            Left = 3
            Top = 237
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Modificar'
            TabOrder = 6
          end
        end
        object UniPanel3: TUniPanel
          Left = 60
          Top = 192
          Width = 522
          Height = 87
          Hint = ''
          ShowHint = True
          TabOrder = 34
          Caption = ''
          object UniLabel66: TUniLabel
            Left = 4
            Top = 5
            Width = 45
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Total PVP'
            TabOrder = 1
          end
          object UniDBEdit68: TUniDBEdit
            Left = 4
            Top = 20
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 2
          end
          object btTecNume: TUniBitBtn
            Left = 2
            Top = 44
            Width = 50
            Height = 35
            Hint = ''
            ShowHint = True
            Caption = 'Margen'
            TabOrder = 3
          end
          object btActualizaArticulo: TUniSpeedButton
            Left = 51
            Top = 54
            Width = 52
            Height = 25
            Hint = ''
            ShowHint = True
            Caption = 'Act. Arti'
            ParentColor = False
            Color = clWindow
            TabOrder = 4
          end
          object UniLabel67: TUniLabel
            Left = 81
            Top = 38
            Width = 19
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Pr 2'
            TabOrder = 5
          end
          object UniLabel68: TUniLabel
            Left = 81
            Top = 20
            Width = 19
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Pr 1'
            TabOrder = 6
          end
          object edPreu: TUniDBEdit
            Left = 104
            Top = 19
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 7
          end
          object edPreu2: TUniDBEdit
            Left = 104
            Top = 38
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 8
          end
          object edVentaCoste: TUniDBEdit
            Left = 104
            Top = 58
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 9
          end
          object edMargen: TUniDBEdit
            Left = 159
            Top = 19
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 10
          end
          object edMargen2: TUniDBEdit
            Left = 159
            Top = 38
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 11
          end
          object MargenCoste: TUniDBEdit
            Left = 159
            Top = 58
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 12
          end
          object edEncarte2: TUniDBEdit
            Left = 214
            Top = 38
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 13
          end
          object edEncarte1: TUniDBEdit
            Left = 214
            Top = 19
            Width = 55
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 14
          end
          object UniLabel69: TUniLabel
            Left = 110
            Top = 6
            Width = 41
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'PVP [F8]'
            TabOrder = 15
          end
          object UniLabel70: TUniLabel
            Left = 162
            Top = 4
            Width = 48
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Marg.[F9]'
            TabOrder = 16
          end
          object UniLabel71: TUniLabel
            Left = 221
            Top = 4
            Width = 37
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Encarte'
            TabOrder = 17
          end
          object edTIva2: TUniDBEdit
            Left = 269
            Top = 38
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 18
          end
          object edTIva1: TUniDBEdit
            Left = 269
            Top = 19
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 19
          end
          object UniLabel72: TUniLabel
            Left = 271
            Top = 4
            Width = 20
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Tipo'
            TabOrder = 20
          end
          object UniDBEdit79: TUniDBEdit
            Left = 296
            Top = 19
            Width = 45
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 21
          end
          object UniDBEdit80: TUniDBEdit
            Left = 296
            Top = 38
            Width = 45
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 22
          end
          object UniDBEdit83: TUniDBEdit
            Left = 341
            Top = 19
            Width = 42
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 23
          end
          object UniDBEdit84: TUniDBEdit
            Left = 341
            Top = 38
            Width = 42
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 24
          end
          object UniDBEdit81: TUniDBEdit
            Left = 384
            Top = 19
            Width = 67
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 25
          end
          object UniDBEdit82: TUniDBEdit
            Left = 384
            Top = 38
            Width = 67
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 26
          end
          object edCosteDire: TUniDBEdit
            Left = 384
            Top = 58
            Width = 67
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 27
          end
          object UniDBEdit86: TUniDBEdit
            Left = 451
            Top = 19
            Width = 67
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 28
          end
          object UniLabel73: TUniLabel
            Left = 299
            Top = 3
            Width = 39
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = '% I.V.A'
            TabOrder = 29
          end
          object UniLabel74: TUniLabel
            Left = 346
            Top = 4
            Width = 35
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = '% R.E.'
            TabOrder = 30
          end
          object UniLabel75: TUniLabel
            Left = 394
            Top = 3
            Width = 42
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Pr.Coste'
            TabOrder = 31
          end
          object UniLabel76: TUniLabel
            Left = 454
            Top = 3
            Width = 63
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Pr.Coste+RE'
            TabOrder = 32
          end
          object UniDBEdit87: TUniDBEdit
            Left = 451
            Top = 38
            Width = 67
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 33
          end
          object edSumarCoste: TUniDBEdit
            Left = 462
            Top = 58
            Width = 56
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 34
          end
          object UniLabel77: TUniLabel
            Left = 452
            Top = 65
            Width = 8
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = '+'
            TabOrder = 35
          end
          object UniSpeedButton34: TUniSpeedButton
            Left = 311
            Top = 58
            Width = 73
            Height = 23
            Hint = ''
            ShowHint = True
            Caption = 'In.Coste'
            ParentColor = False
            Color = clWindow
            TabOrder = 36
          end
        end
        object UniPanel4: TUniPanel
          Left = 567
          Top = -2
          Width = 150
          Height = 193
          Hint = ''
          ShowHint = True
          TabOrder = 35
          Caption = ''
          object btBorrar: TUniSpeedButton
            Left = 1
            Top = 1
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            ParentColor = False
            Color = clWindow
            TabOrder = 1
          end
          object btNuevoAdendum: TUniSpeedButton
            Left = 1
            Top = 40
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            ParentColor = False
            Color = clWindow
            TabOrder = 2
          end
          object btDirectoDevolucion: TUniSpeedButton
            Left = 50
            Top = 40
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            ParentColor = False
            Color = clWindow
            TabOrder = 3
          end
          object btRecuperaDevolucion: TUniSpeedButton
            Left = 99
            Top = 40
            Width = 50
            Height = 40
            Hint = ''
            ShowHint = True
            Caption = ''
            ParentColor = False
            Color = clWindow
            TabOrder = 4
          end
          object UniDBCheckBox4: TUniDBCheckBox
            Left = 59
            Top = 4
            Width = 64
            Height = 17
            Hint = ''
            ShowHint = True
            Caption = 'Art'#237'culo'
            TabOrder = 5
          end
          object UniLabel51: TUniLabel
            Left = 59
            Top = 23
            Width = 61
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'A devoluci'#243'n'
            TabOrder = 6
          end
          object edStock: TUniDBEdit
            Left = 98
            Top = 96
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 7
          end
          object edCantiReal: TUniDBEdit
            Left = 1
            Top = 96
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 8
          end
          object edReserva: TUniDBEdit
            Left = 49
            Top = 96
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 9
          end
          object UniLabel58: TUniLabel
            Left = 19
            Top = 80
            Width = 24
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Reci.'
            TabOrder = 10
          end
          object UniLabel59: TUniLabel
            Left = 59
            Top = 80
            Width = 32
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Reser.'
            TabOrder = 11
          end
          object UniLabel60: TUniLabel
            Left = 112
            Top = 80
            Width = 26
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Stock'
            TabOrder = 12
          end
          object edStockMaximo: TUniDBEdit
            Left = 1
            Top = 131
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 13
          end
          object UniDBEdit59: TUniDBEdit
            Left = 49
            Top = 131
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 14
          end
          object UniLabel61: TUniLabel
            Left = 5
            Top = 116
            Width = 37
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Servicio'
            TabOrder = 15
          end
          object UniLabel62: TUniLabel
            Left = 50
            Top = 116
            Width = 43
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Devolver'
            TabOrder = 16
          end
          object UniLabel64: TUniLabel
            Left = 5
            Top = 153
            Width = 42
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Compras'
            TabOrder = 17
          end
          object UniLabel63: TUniLabel
            Left = 58
            Top = 153
            Width = 31
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Devol.'
            TabOrder = 18
          end
          object edAcumCompras: TUniDBEdit
            Left = 1
            Top = 168
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 19
          end
          object edAcumDevol: TUniDBEdit
            Left = 49
            Top = 168
            Width = 48
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 20
          end
        end
        object UniPanel5: TUniPanel
          Left = 718
          Top = -1
          Width = 242
          Height = 192
          Hint = ''
          ShowHint = True
          TabOrder = 36
          Caption = ''
          object GridUltiCompra: TUniDBGrid
            Left = 1
            Top = 1
            Width = 240
            Height = 162
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alTop
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
          end
          object UniLabel65: TUniLabel
            Left = 78
            Top = 171
            Width = 24
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Total'
            TabOrder = 2
          end
          object UniDBEdit63: TUniDBEdit
            Left = 159
            Top = 168
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 3
          end
          object UniDBEdit64: TUniDBEdit
            Left = 186
            Top = 168
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 4
          end
          object UniDBEdit65: TUniDBEdit
            Left = 213
            Top = 168
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 5
          end
          object UniDBEdit66: TUniDBEdit
            Left = 131
            Top = 168
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 6
          end
          object UniDBEdit67: TUniDBEdit
            Left = 104
            Top = 168
            Width = 27
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 7
          end
        end
      end
      object UniPageControl4: TUniPageControl
        Left = 0
        Top = 281
        Width = 1000
        Height = 302
        Hint = ''
        ShowHint = True
        ActivePage = UniTabSheet9
        TabBarVisible = False
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        object UniTabSheet9: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Lineas'
          object GridLinea: TUniDBGrid
            Left = 0
            Top = 0
            Width = 992
            Height = 274
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
          end
        end
        object UniTabSheet10: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Resumen'
          object GridResumen: TUniDBGrid
            Left = 0
            Top = 0
            Width = 992
            Height = 274
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
          end
        end
      end
    end
    object UniTabSheet8: TUniTabSheet
      Hint = ''
      ShowHint = True
      Caption = 'UniTabSheet2'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'  Ext.get(se' +
          'nder.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sender.id).el.set' +
          'Style("border-width", 0);'#13#10'}')
      object UniPageControl1: TUniPageControl
        Left = 0
        Top = -2
        Width = 588
        Height = 369
        Hint = ''
        ShowHint = True
        ActivePage = tabDatosDocu
        TabOrder = 0
        object tabDatosDocu: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Datos Documento'
          object UniLabel2: TUniLabel
            Left = 25
            Top = 9
            Width = 54
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Documento'
            TabOrder = 0
          end
          object UniLabel3: TUniLabel
            Left = 128
            Top = 9
            Width = 70
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Fecha Entrega'
            TabOrder = 1
          end
          object UniLabel4: TUniLabel
            Left = 231
            Top = 9
            Width = 38
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Semana'
            TabOrder = 2
          end
          object UniLabel5: TUniLabel
            Left = 288
            Top = 31
            Width = 70
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Finalizado .....'
            TabOrder = 3
          end
          object UniDBEdit2: TUniDBEdit
            Left = 23
            Top = 27
            Width = 101
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 4
          end
          object UniDBEdit3: TUniDBEdit
            Left = 227
            Top = 27
            Width = 57
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 5
          end
          object UniDBDateTimePicker1: TUniDBDateTimePicker
            Left = 125
            Top = 27
            Width = 101
            Height = 20
            Hint = ''
            ShowHint = True
            DateTime = 43371.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 6
          end
          object UniButton1: TUniButton
            Left = 407
            Top = 22
            Width = 153
            Height = 25
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Fechas Articulos'
            TabOrder = 7
          end
          object UniGroupBox1: TUniGroupBox
            Left = 24
            Top = 56
            Width = 537
            Height = 121
            Hint = ''
            ShowHint = True
            Caption = 'Documento del Distribuidor'
            TabOrder = 8
            object UniLabel6: TUniLabel
              Left = 32
              Top = 24
              Width = 48
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'N.Albaran'
              TabOrder = 1
            end
            object UniLabel7: TUniLabel
              Left = 130
              Top = 24
              Width = 69
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha Albaran'
              TabOrder = 2
            end
            object UniLabel8: TUniLabel
              Left = 20
              Top = 66
              Width = 267
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Este Albaran ya existe **************************'
              TabOrder = 3
            end
            object UniLabel9: TUniLabel
              Left = 235
              Top = 24
              Width = 61
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha Cargo'
              TabOrder = 4
            end
            object UniLabel10: TUniLabel
              Left = 393
              Top = 25
              Width = 39
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Tipo Iva'
              TabOrder = 5
            end
            object UniLabel11: TUniLabel
              Left = 337
              Top = 25
              Width = 36
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Margen'
              TabOrder = 6
            end
            object UniLabel12: TUniLabel
              Left = 339
              Top = 10
              Width = 93
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Autom'#225'tico a todos'
              TabOrder = 7
            end
            object UniDBEdit4: TUniDBEdit
              Left = 11
              Top = 41
              Width = 101
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 8
            end
            object UniDBEdit5: TUniDBEdit
              Left = 388
              Top = 41
              Width = 55
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 9
            end
            object UniDBEdit6: TUniDBEdit
              Left = 332
              Top = 41
              Width = 55
              Height = 20
              Hint = ''
              ShowHint = True
              TabOrder = 10
            end
            object UniDBDateTimePicker2: TUniDBDateTimePicker
              Left = 113
              Top = 41
              Width = 101
              Height = 20
              Hint = ''
              ShowHint = True
              DateTime = 43371.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 11
            end
            object UniDBDateTimePicker3: TUniDBDateTimePicker
              Left = 216
              Top = 41
              Width = 101
              Height = 20
              Hint = ''
              ShowHint = True
              DateTime = 43371.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 12
            end
            object UniRadioGroup1: TUniRadioGroup
              Left = 448
              Top = 4
              Width = 87
              Height = 90
              Hint = ''
              ShowHint = True
              Items.Strings = (
                'Albaran'
                'Factura'
                'Deposito')
              Caption = ''
              TabOrder = 13
            end
            object UniDBCheckBox1: TUniDBCheckBox
              Left = 424
              Top = 98
              Width = 107
              Height = 17
              Hint = ''
              ShowHint = True
              Caption = 'Actualiza Articulos'
              TabOrder = 14
            end
          end
          object UniDBMemo1: TUniDBMemo
            Left = 24
            Top = 176
            Width = 537
            Height = 94
            Hint = ''
            ShowHint = True
            TabOrder = 9
          end
          object UniBitBtn1: TUniBitBtn
            Left = 23
            Top = 274
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Abrir'
            TabOrder = 10
          end
          object UniBitBtn3: TUniBitBtn
            Left = 79
            Top = 274
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Grabar'
            TabOrder = 11
          end
          object UniBitBtn4: TUniBitBtn
            Left = 135
            Top = 274
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cancelar'
            TabOrder = 12
          end
          object UniBitBtn5: TUniBitBtn
            Left = 191
            Top = 274
            Width = 55
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Anular'
            TabOrder = 13
          end
        end
        object tabDupliEntrada: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Duplicar Entradas'
          object UniPanel8: TUniPanel
            Left = 0
            Top = 289
            Width = 580
            Height = 52
            Hint = ''
            ShowHint = True
            Align = alBottom
            Anchors = [akLeft, akRight, akBottom]
            TabOrder = 0
            Caption = ''
            object UniLabel13: TUniLabel
              Left = 7
              Top = 8
              Width = 73
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'Fecha Localizar'
              TabOrder = 1
            end
            object UniLabel14: TUniLabel
              Left = 385
              Top = 6
              Width = 37
              Height = 13
              Hint = ''
              ShowHint = True
              Caption = 'N'#250'mero'
              TabOrder = 2
            end
            object UniBitBtn6: TUniBitBtn
              Left = 120
              Top = 4
              Width = 100
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Semana Pasada'
              TabOrder = 3
            end
            object UniBitBtn7: TUniBitBtn
              Left = 243
              Top = 4
              Width = 121
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'N'#250'mero Autom'#225'tico'
              TabOrder = 4
            end
            object UniBitBtn8: TUniBitBtn
              Left = 439
              Top = 6
              Width = 55
              Height = 42
              Hint = ''
              ShowHint = True
              Caption = 'Duplicar'
              TabOrder = 5
            end
            object UniDBDateTimePicker4: TUniDBDateTimePicker
              Left = 2
              Top = 24
              Width = 101
              Hint = ''
              ShowHint = True
              DateTime = 43371.000000000000000000
              DateFormat = 'dd/MM/yyyy'
              TimeFormat = 'HH:mm:ss'
              TabOrder = 6
            end
            object UniEdit1: TUniEdit
              Left = 380
              Top = 25
              Width = 52
              Height = 21
              Hint = ''
              ShowHint = True
              Text = 'UniEdit1'
              TabOrder = 7
            end
          end
          object UniDBGrid1: TUniDBGrid
            Left = 119
            Top = 0
            Width = 461
            Height = 289
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alClient
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 1
          end
          object UniDBGrid2: TUniDBGrid
            Left = 0
            Top = 0
            Width = 119
            Height = 289
            Hint = ''
            ShowHint = True
            LoadMask.Message = 'Loading data...'
            Align = alLeft
            Anchors = [akLeft, akTop, akBottom]
            TabOrder = 2
          end
        end
        object tabCambiarDistri: TUniTabSheet
          Hint = ''
          ShowHint = True
          Caption = 'Cambiar Distribuidora'
          object UniBitBtn9: TUniBitBtn
            Left = 15
            Top = 40
            Width = 126
            Height = 45
            Hint = ''
            ShowHint = True
            Caption = 'Nueva Distribuidora'
            TabOrder = 0
          end
          object UniBitBtn10: TUniBitBtn
            Left = 151
            Top = 109
            Width = 153
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cambiar Distribuidora'
            TabOrder = 1
          end
          object UniBitBtn11: TUniBitBtn
            Left = 315
            Top = 109
            Width = 152
            Height = 42
            Hint = ''
            ShowHint = True
            Caption = 'Cancelar'
            TabOrder = 2
          end
          object UniDBEdit7: TUniDBEdit
            Left = 145
            Top = 65
            Width = 47
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 3
          end
          object UniDBEdit8: TUniDBEdit
            Left = 192
            Top = 65
            Width = 288
            Height = 20
            Hint = ''
            ShowHint = True
            TabOrder = 4
          end
          object UniLabel15: TUniLabel
            Left = 147
            Top = 46
            Width = 94
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'Nueva Distribuidora'
            TabOrder = 5
          end
          object UniLabel16: TUniLabel
            Left = 259
            Top = 179
            Width = 87
            Height = 13
            Hint = ''
            ShowHint = True
            Caption = 'LabelProcesoDistri'
            TabOrder = 6
          end
        end
      end
    end
  end
  object tmSession: TUniTimer
    Interval = 180000
    ClientEvent.Strings = (
      'function(sender)'
      '{'
      ' '
      '}')
    Left = 1132
    Top = 777
  end
end
