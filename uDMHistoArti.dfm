object DMHistoArti: TDMHistoArti
  OldCreateOrder = False
  Height = 653
  Width = 978
  object sqlStock: TFDQuery
    AfterScroll = sqlStockAfterScroll
    Filtered = True
    Filter = 'Stock <> 0'
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'Select H.ID_ARTICULO, A.TBARRAS, A.BARRAS, H.ADENDUM, A.DESCRIPC' +
        'ION'
      ''
      ', (Select SUM(H1.Entradas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and clave = '#39'01'#39') as Entradas'
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and clave = '#39'51'#39') as Ventas'
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and clave = '#39'53'#39') as VentasAuto'
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and clave = '#39'54'#39') as Devoluciones'
      ', (Select SUM(H1.Salidas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and clave = '#39'55'#39') as Merma'
      ''
      ', (Select SUM(H1.Entradas - H1.Salidas) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um and H1.Clave between '#39'01'#39' and '#39'90'#39') as Stock'
      ''
      ', (Select Count(*) From FVHISARTI H1'
      
        '   where H1.ID_ARTICULO = H.ID_ARTICULO and H1.adendum = H.Adend' +
        'um  and H1.Clave between '#39'01'#39' and '#39'90'#39') as Movtos'
      ''
      'from FVHISARTI H'
      ''
      'left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ''
      'WHERE H.ID_ARTICULO IS NOT NULL '
      ''
      
        'group by H.ID_ARTICULO,A.TBARRAS, A.BARRAS, H.ADENDUM, A.DESCRIP' +
        'CION'
      
        'order by A.DESCRIPCION, H.ADENDUM, H.ID_ARTICULO,A.TBARRAS, A.BA' +
        'RRAS')
    Left = 39
    Top = 59
    object sqlStockID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlStockTBARRAS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockBARRAS: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      ProviderFlags = []
      ReadOnly = True
      Size = 13
    end
    object sqlStockADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlStockDESCRIPCION: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlStockENTRADAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockVENTAS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTAS'
      Origin = 'VENTAS'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockVENTASAUTO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VENTASAUTO'
      Origin = 'VENTASAUTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockDEVOLUCIONES: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DEVOLUCIONES'
      Origin = 'DEVOLUCIONES'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockMERMA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MERMA'
      Origin = 'MERMA'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockSTOCK: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'STOCK'
      Origin = 'STOCK'
      ProviderFlags = []
      ReadOnly = True
    end
    object sqlStockMOVTOS: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MOVTOS'
      Origin = 'MOVTOS'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object sqlHisArti: TFDQuery
    AfterScroll = sqlHisArtiAfterScroll
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      
        'select H.ID_HISARTI, H.SWES, H.FECHA, H.HORA, H.CLAVE, H.ID_ARTI' +
        'CULO, H.ADENDUM, H.ID_CLIENTE, H.SWTIPOFRA, H.NFACTURA'
      
        ', H.NALBARAN, H.NLINEA, H.DEVUELTOS, H.VENDIDOS, H.CANTIDAD, H.C' +
        'ANTIENALBA, H.PRECIOCOMPRA, H.PRECIOCOSTE, H.PRECIOVENTA'
      
        ', H.SWDTO, H.TPCDTO, H.TIVA, H.TPCIVA, H.TPCRE, H.IMPOBRUTO, H.I' +
        'MPODTO, H.IMPOBASE, H.IMPOIVA, H.IMPORE, H.IMPOTOTLIN, H.ENTRADA' +
        'S'
      
        ', H.SALIDAS, H.VALORCOSTE, H.VALORCOMPRA, H.VALORVENTA, H.VALORM' +
        'OVI, H.ID_PROVEEDOR, H.IDSTOCABE, H.TPCCOMISION, H.IMPOCOMISION'
      
        ', H.ENCARTE, H.ID_DIARIA, H.TURNO, H.NALMACEN, H.NRECLAMACION, H' +
        '.SWPDTEPAGO, H.SWESTADO, H.SWCERRADO, H.FECHAAVISO, H.FECHADEVOL'
      ', H.TIPOVENTA, H.PAQUETE'
      ', C.NOMBRE as NOMCLIE, P.NOMBRE as NOMPROVE'
      '  from FVHISARTI H'
      ' left outer join FMARTICULO A on (A.ID_ARTICULO = H.ID_ARTICULO)'
      ' left outer join FMPROVE P on (P.ID_PROVEEDOR = H.ID_PROVEEDOR)'
      ' left outer join FMCLIENTES C on (C.ID_CLIENTE = H.ID_CLIENTE)'
      ''
      'where H.ID_HISARTI is not null')
    Left = 103
    Top = 59
    object sqlHisArtiID_HISARTI: TIntegerField
      FieldName = 'ID_HISARTI'
      Origin = 'ID_HISARTI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlHisArtiSWES: TStringField
      FieldName = 'SWES'
      Origin = 'SWES'
      Size = 1
    end
    object sqlHisArtiFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object sqlHisArtiHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
      DisplayFormat = 'HH:MM'
    end
    object sqlHisArtiCLAVE: TStringField
      FieldName = 'CLAVE'
      Origin = 'CLAVE'
      Size = 2
    end
    object sqlHisArtiID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
    end
    object sqlHisArtiADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlHisArtiID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object sqlHisArtiSWTIPOFRA: TIntegerField
      FieldName = 'SWTIPOFRA'
      Origin = 'SWTIPOFRA'
    end
    object sqlHisArtiNFACTURA: TIntegerField
      FieldName = 'NFACTURA'
      Origin = 'NFACTURA'
    end
    object sqlHisArtiNALBARAN: TIntegerField
      FieldName = 'NALBARAN'
      Origin = 'NALBARAN'
    end
    object sqlHisArtiNLINEA: TIntegerField
      FieldName = 'NLINEA'
      Origin = 'NLINEA'
    end
    object sqlHisArtiDEVUELTOS: TSingleField
      FieldName = 'DEVUELTOS'
      Origin = 'DEVUELTOS'
    end
    object sqlHisArtiVENDIDOS: TSingleField
      FieldName = 'VENDIDOS'
      Origin = 'VENDIDOS'
    end
    object sqlHisArtiCANTIDAD: TSingleField
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object sqlHisArtiCANTIENALBA: TSingleField
      FieldName = 'CANTIENALBA'
      Origin = 'CANTIENALBA'
    end
    object sqlHisArtiPRECIOCOMPRA: TSingleField
      FieldName = 'PRECIOCOMPRA'
      Origin = 'PRECIOCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOCOSTE: TSingleField
      FieldName = 'PRECIOCOSTE'
      Origin = 'PRECIOCOSTE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiPRECIOVENTA: TSingleField
      FieldName = 'PRECIOVENTA'
      Origin = 'PRECIOVENTA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiSWDTO: TSmallintField
      FieldName = 'SWDTO'
      Origin = 'SWDTO'
    end
    object sqlHisArtiTPCDTO: TSingleField
      FieldName = 'TPCDTO'
      Origin = 'TPCDTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTIVA: TSmallintField
      FieldName = 'TIVA'
      Origin = 'TIVA'
    end
    object sqlHisArtiTPCIVA: TSingleField
      FieldName = 'TPCIVA'
      Origin = 'TPCIVA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiTPCRE: TSingleField
      FieldName = 'TPCRE'
      Origin = 'TPCRE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOBRUTO: TSingleField
      FieldName = 'IMPOBRUTO'
      Origin = 'IMPOBRUTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPODTO: TSingleField
      FieldName = 'IMPODTO'
      Origin = 'IMPODTO'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOBASE: TSingleField
      FieldName = 'IMPOBASE'
      Origin = 'IMPOBASE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOIVA: TSingleField
      FieldName = 'IMPOIVA'
      Origin = 'IMPOIVA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPORE: TSingleField
      FieldName = 'IMPORE'
      Origin = 'IMPORE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiIMPOTOTLIN: TSingleField
      FieldName = 'IMPOTOTLIN'
      Origin = 'IMPOTOTLIN'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiENTRADAS: TSingleField
      FieldName = 'ENTRADAS'
      Origin = 'ENTRADAS'
    end
    object sqlHisArtiSALIDAS: TSingleField
      FieldName = 'SALIDAS'
      Origin = 'SALIDAS'
    end
    object sqlHisArtiVALORCOSTE: TSingleField
      FieldName = 'VALORCOSTE'
      Origin = 'VALORCOSTE'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORCOMPRA: TSingleField
      FieldName = 'VALORCOMPRA'
      Origin = 'VALORCOMPRA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORVENTA: TSingleField
      FieldName = 'VALORVENTA'
      Origin = 'VALORVENTA'
      DisplayFormat = '0.00'
    end
    object sqlHisArtiVALORMOVI: TSingleField
      FieldName = 'VALORMOVI'
      Origin = 'VALORMOVI'
    end
    object sqlHisArtiID_PROVEEDOR: TIntegerField
      FieldName = 'ID_PROVEEDOR'
      Origin = 'ID_PROVEEDOR'
    end
    object sqlHisArtiIDSTOCABE: TIntegerField
      FieldName = 'IDSTOCABE'
      Origin = 'IDSTOCABE'
    end
    object sqlHisArtiTPCCOMISION: TSingleField
      FieldName = 'TPCCOMISION'
      Origin = 'TPCCOMISION'
    end
    object sqlHisArtiIMPOCOMISION: TSingleField
      FieldName = 'IMPOCOMISION'
      Origin = 'IMPOCOMISION'
    end
    object sqlHisArtiENCARTE: TSingleField
      FieldName = 'ENCARTE'
      Origin = 'ENCARTE'
    end
    object sqlHisArtiID_DIARIA: TIntegerField
      FieldName = 'ID_DIARIA'
      Origin = 'ID_DIARIA'
    end
    object sqlHisArtiTURNO: TSmallintField
      FieldName = 'TURNO'
      Origin = 'TURNO'
    end
    object sqlHisArtiNALMACEN: TIntegerField
      FieldName = 'NALMACEN'
      Origin = 'NALMACEN'
    end
    object sqlHisArtiNRECLAMACION: TIntegerField
      FieldName = 'NRECLAMACION'
      Origin = 'NRECLAMACION'
    end
    object sqlHisArtiSWPDTEPAGO: TSmallintField
      FieldName = 'SWPDTEPAGO'
      Origin = 'SWPDTEPAGO'
    end
    object sqlHisArtiSWESTADO: TSmallintField
      FieldName = 'SWESTADO'
      Origin = 'SWESTADO'
    end
    object sqlHisArtiSWCERRADO: TSmallintField
      FieldName = 'SWCERRADO'
      Origin = 'SWCERRADO'
    end
    object sqlHisArtiFECHAAVISO: TDateField
      FieldName = 'FECHAAVISO'
      Origin = 'FECHAAVISO'
    end
    object sqlHisArtiFECHADEVOL: TDateField
      FieldName = 'FECHADEVOL'
      Origin = 'FECHADEVOL'
    end
    object sqlHisArtiTIPOVENTA: TSmallintField
      FieldName = 'TIPOVENTA'
      Origin = 'TIPOVENTA'
    end
    object sqlHisArtiNOMCLIE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMCLIE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHisArtiNOMPROVE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMPROVE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object sqlHisArtiPAQUETE: TIntegerField
      FieldName = 'PAQUETE'
      Origin = 'PAQUETE'
    end
  end
  object sqlArticuloS: TFDQuery
    Connection = DMppal.FDConnection1
    SQL.Strings = (
      'Select    ID_ARTICULO,TBARRAS,BARRAS,ADENDUM,DESCRIPCION,IBS,'
      'EDITORIAL'
      ' From  FMARTICULO'
      ' where ID_ARTICULO is not null'
      ' and SWALTABAJA <> 1')
    Left = 167
    Top = 59
    object sqlArticuloSID_ARTICULO: TIntegerField
      FieldName = 'ID_ARTICULO'
      Origin = 'ID_ARTICULO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlArticuloSTBARRAS: TIntegerField
      FieldName = 'TBARRAS'
      Origin = 'TBARRAS'
    end
    object sqlArticuloSBARRAS: TStringField
      FieldName = 'BARRAS'
      Origin = 'BARRAS'
      Size = 13
    end
    object sqlArticuloSADENDUM: TStringField
      FieldName = 'ADENDUM'
      Origin = 'ADENDUM'
      Size = 5
    end
    object sqlArticuloSDESCRIPCION: TStringField
      FieldName = 'DESCRIPCION'
      Origin = 'DESCRIPCION'
      Size = 40
    end
    object sqlArticuloSIBS: TStringField
      FieldName = 'IBS'
      Origin = 'IBS'
      Size = 10
    end
    object sqlArticuloSEDITORIAL: TIntegerField
      FieldName = 'EDITORIAL'
      Origin = 'EDITORIAL'
    end
  end
end
