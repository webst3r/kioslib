unit uHistoArti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniImageList;

type
  TFormHistoArti = class(TUniForm)
    tmSession: TUniTimer;
    pnlBts: TUniPanel;
    GridHisto: TUniDBGrid;
    GridStock: TUniDBGrid;
    dsStock: TDataSource;
    edBarras: TUniDBEdit;
    edAdendum: TUniDBEdit;
    edDescripcion: TUniDBEdit;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    dsHisArti: TDataSource;
    lbArti: TUniLabel;
    btnSumaCabe: TUniButton;
    edDesdeFecha: TUniDateTimePicker;
    edHastaFecha: TUniDateTimePicker;
    btDevolucion: TUniButton;
    btRecepcion: TUniButton;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    btEjecutar: TUniButton;
    rgSeleStock: TUniRadioGroup;
    procedure UniFormCreate(Sender: TObject);
    procedure btnSumaCabeClick(Sender: TObject);
    procedure GridHistoDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure btEjecutarClick(Sender: TObject);
    procedure GridStockColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure GridStockColumnSummaryResult(Column: TUniDBGridColumn;
      GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
    procedure GridHistoColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure GridHistoColumnSummaryResult(Column: TUniDBGridColumn;
      GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
    procedure btDevolucionClick(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure rgSeleStockClick(Sender: TObject);


  private



    { Private declarations }

  public
    { Public declarations }
    vStringOrigen : String;



  end;

function FormHistoArti: TFormHistoArti;

implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uMenu, uDMMenuArti, uMenuArti, uDMHistoArti,
  uDevolFicha, uDevolMenu, uDevolLista,uDMDevol,
  uMenuBotones;

function FormHistoArti: TFormHistoArti;
begin
  Result := TFormHistoArti(DMppal.GetFormInstance(TFormHistoArti));

end;


procedure TFormHistoArti.btnSumaCabeClick(Sender: TObject);
begin
  DMHistoArti.vEntradas := 0;
  DMHistoArti.vSalidas  := 0;
  DMHistoArti.vStock    := 0;
  DMHistoArti.vImpoTotlin := 0;
  DMHistoArti.vValorCoste := 0;
  DMHistoArti.vValorCompra:= 0;
  DMHistoArti.vValorVenta := 0;
  DMHistoArti.vRegisH   := DMHistoArti.sqlHisArti.RecordCount;

  DMHistoArti.sqlHisArti.First;
  while not DMHistoArti.sqlHisArti.EOF Do
  begin
      DMHistoArti.vEntradas := DMHistoArti.vEntradas + DMHistoArti.sqlHisArti.FieldByname('ENTRADAS').AsFloat;
      DMHistoArti.vSalidas  := DMHistoArti.vSalidas  + DMHistoArti.sqlHisArti.FieldByname('SALIDAS').AsFloat;
      DMHistoArti.vStock    := DMHistoArti.vStock    + (DMHistoArti.sqlHisArti.FieldByname('ENTRADAS').AsFloat
                             -  DMHistoArti.sqlHisArti.FieldByname('SALIDAS').AsFloat);
      DMHistoArti.vImpoTotlin  := DMHistoArti.vImpoTotlin  + DMHistoArti.sqlHisArti.FieldByname('ImpoTotlin').AsFloat;
      DMHistoArti.vValorCoste  := DMHistoArti.vValorCoste  + DMHistoArti.sqlHisArti.FieldByname('ValorCoste').AsFloat;
      DMHistoArti.vValorCompra  := DMHistoArti.vValorCompra  + DMHistoArti.sqlHisArti.FieldByname('ValorCompra').AsFloat;
      DMHistoArti.vValorVenta  := DMHistoArti.vValorVenta  + DMHistoArti.sqlHisArti.FieldByname('ValorVenta').AsFloat;

      DMHistoArti.sqlHisArti.Next;
  end;

end;

procedure TFormHistoArti.GridHistoColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'FECHA') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:= 0.0;
    Column.AuxValue:= DMHistoArti.vStock;
  end else if SameText(Column.FieldName, 'HORA') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:= 0;
    Column.AuxValue:= DMHistoArti.vRegisH;
  end else
  if SameText(Column.FieldName, 'ENTRADAS') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end
  else if SameText(Column.FieldName, 'SALIDAS') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end
  else if SameText(Column.FieldName, 'IMPOTOTLIN') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end
  else if SameText(Column.FieldName, 'VALORCOSTE') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end
  else if SameText(Column.FieldName, 'VALORCOMPRA') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end
  else if SameText(Column.FieldName, 'VALORVENTA') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:=0.0;
    Column.AuxValue:=Column.AuxValue + Column.Field.AsFloat;
  end;
end;

procedure TFormHistoArti.GridHistoColumnSummaryResult(Column: TUniDBGridColumn;
  GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
var I : Integer;
begin
  if SameText(Column.FieldName, 'FECHA') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('Stk: ' + '0.0', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight

  end else if SameText(Column.FieldName, 'HORA') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('Reg: ' + '0.0', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'ENTRADAS') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.0', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'SALIDAS') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.0', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'IMPOTOTLIN') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'VALORCOSTE') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'VALORCOMPRA') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end else if SameText(Column.FieldName, 'VALORVENTA') then
  begin
    I := Column.AuxValue;
    Result:= FormatCurr('0.00', I);
    Attribs.Font.Style:=[fsBold];
    Align := alRight
  end;
end;

procedure TFormHistoArti.GridHistoDrawColumnCell(Sender: TObject; ACol,
  ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  {
  if Column.FieldName = 'SWES' then
  begin
    Attribs.Font.Style:=[fsBold];
    if Column.Field.AsString = 'E' then
    begin
      Attribs.Color:=clBlue;

    end else
      Attribs.Color:=clRed;
  end  }

  if DMHistoArti.sqlHisArtiSWES.AsString = 'E' then
     Attribs.Font.Color:=clBlue
  else
     Attribs.Font.Color:=clRed;


end;

procedure TFormHistoArti.GridStockColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  if SameText(Column.FieldName, 'ADENDUM') then
  begin
    if Column.AuxValue=NULL then Column.AuxValue:= '';
    Column.AuxValue := 'Total: ';
  end else
  begin
    if Column.AuxValue = NULL then Column.AuxValue:=0;
    Column.AuxValue    := Column.AuxValue + Column.Field.AsFloat;
  end;
end;

procedure TFormHistoArti.GridStockColumnSummaryResult(Column: TUniDBGridColumn;
  GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
  var
//  I : Integer;
  F : Real;
begin

  if SameText(Column.FieldName, 'ADENDUM') then
  begin
//    F := Column.AuxValue;
    Result:='Totales: ';
//    Result:=FormatCurr('0,0', F, FmtSettings);
    Attribs.Font.Style:=[fsBold];
    Align := alRight;
//    Attribs.Font.Color:=clGray;
  end else
  begin             //objetivo
    F := Column.AuxValue;
//    Result:='Total Cost: '+FormatCurr('0,0.00 ', F, FmtSettings) + FmtSettings.CurrencyString;
    Result:=FormatCurr('0', F, FmtSettings);
    Attribs.Font.Style:=[fsBold];
//    Attribs.Font.Style:=[fsBold];
//    Attribs.Font.Color:=clGray;
  end;
end;

procedure TFormHistoArti.rgSeleStockClick(Sender: TObject);
begin
  with DMHistoArti.sqlStock do
  begin
    Filtered := False;
    if rgSeleStock.ItemIndex = 0 then Filter := '';
    if rgSeleStock.ItemIndex = 1 then Filter := 'STOCK <> 0';
    if rgSeleStock.ItemIndex = 2 then Filter := 'STOCK > 0';

    Filtered := True;
  end;
end;

procedure TFormHistoArti.btDevolucionClick(Sender: TObject);
begin
  FormBotones.btDevolucionClick(nil);
  FormDevolMenu.RutAbrirAlbaranTab;
  DMDevolucion.RutAbrirAlbaran(' = ' + IntToStr(DMHistoArti.sqlHisArtiIDSTOCABE.AsInteger),' = 2 or SWTIPODOCU = 22 ', 0,edDesdeFecha.DateTime,edHastaFecha.DateTime,false);
  DMDevolucion.RutCambiarPaquete(DMHistoArti.sqlHisArtiPAQUETE.AsInteger);
  DMppal.swDevolucionFicha := Self.Name;
end;

procedure TFormHistoArti.btEjecutarClick(Sender: TObject);
begin
  DMHistoArti.ProSeleStock(DMHistoArti.vID_Articulo);
end;

procedure TFormHistoArti.UniFormCreate(Sender: TObject);
begin
  edDesdeFecha.DateTime := now - 365;
  edhastaFecha.DateTime := now;

  GridStock.Columns[0].Width := 70;

  with GridHisto do
  begin
    Columns[0].Width := 40;
    Columns[3].Width := 50;
    Columns[4].Width := 50;
    Columns[11].Width := 75;
  end;

  FormMenu.lbFormNombre.Caption := 'Historico';
end;



procedure TFormHistoArti.UniFormShow(Sender: TObject);
begin

end;

//--------------------------
//Eventos clic
//--------------------------

//--------------------------
//Fin Eventos clic
//--------------------------
end.
