unit uMenuDatos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, Data.DB, uniBasicGrid, uniDBGrid,
  uniPageControl, uniGUIBaseClasses, uniPanel, uniButton,

  Dialogs, Vcl.ExtCtrls,

  uniTreeView, uniImage, uniSplitter, uniMultiItem, uniComboBox,
  uniToolBar, uniImageList, uniStrUtils, uniBitBtn, uniDBComboBox,
  uniDBLookupComboBox, uniEdit, uniDBText, uniLabel, uniCheckBox, uniChart,
  uniDBEdit, uniMemo,
  uniDateTimePicker, uniListBox,// Uni,
  uniGUIFrame,
  uniDBListBox, uniDBLookupListBox, uniDBCheckBox, uniDBMemo, Vcl.Imaging.jpeg,
  uniScrollBox, Vcl.Menus, uniMainMenu, uniRadioGroup, uniDBRadioGroup,
  uniDBDateTimePicker, uniDBNavigator, uniURLFrame, uniHTMLFrame,   // DBAccess,
  uniGroupBox, uniTimer,
  Shellapi, uniFileUpload, uniSpeedButton;

type
  TFormNuevosDatos = class(TUniForm)
    UniSplitter1: TUniSplitter;
    pnlBts2: TUniPanel;
    UniPageControl1: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniDBLookupComboBox1: TUniDBLookupComboBox;
    UniLabel3: TUniLabel;
    UniDBEdit1: TUniDBEdit;
    UniLabel4: TUniLabel;
    UniLabel5: TUniLabel;
    UniDBEdit2: TUniDBEdit;
    UniTabSheet2: TUniTabSheet;
    UniTabSheet3: TUniTabSheet;
    procedure btCerrarMenuClick(Sender: TObject);
    procedure btClientesClick(Sender: TObject);
    procedure UniSpeedButton6Click(Sender: TObject);
    procedure UniSpeedButton8Click(Sender: TObject);
    procedure UniSpeedButton26Click(Sender: TObject);
    procedure UniSpeedButton27Click(Sender: TObject);



  private





    { Private declarations }
  public
    { Public declarations }

    FUrlDocumentacion :String;

    vUniForm : TUniForm;

    swOtraFicha, swFichaSareb1, swFichaActivo1, swFichaOcupantes1, swFichaContactos1, swFichaLanzamiento1,
    swFichaFoto1, swFichaDocumentacion1, swFichaInformes1  : Boolean;
    swReducirImagen : Boolean;

    swFotos, swDocumentos, swLanzamiento, swFichaUti, swFichaInformes, swFichaActivo, swFichaOcupantes, swFichaContactos, swFichaSareb : Integer;

    swActiveForm : string;

    vFiltro, vFiltroTexto,
    vFiltroEstado, vFiltroEstadoTexto,
    vFiltroFase,   vFiltroFaseTexto,
    vFiltroEncargo,   vFiltroEncargoTexto,
    swTipoFiltro,
    vResult, vFoto : String;

    vDesdeFecha, vHastaFecha : TDateTime;

    FFolderUploadFile, FFolderFile  : String;
    vComercialConsulta, vComercialConsulta1, vFabricante1, vFabricante1Sele : string;
    vCodigoComercial   : string;

    swConsultaAccion : boolean;

    swFormMenu : Integer;
    swVentas, swPantalla, swContactos, swConsultas, swAuxiliares,
    swListaUR, swFicha, swAlta, swUploadFile, swGestores,
    swComercial  : Integer;
    swTipoAlta : Integer;

    swAbrirTipos : Integer;
      swListaDet : Integer;

    vImage: TUniImage;
    vFormImage : String;

    swManteTPV, swCliente, swArticulo, swRecepcion, swDevolucion : Integer;

    procedure DCallBack(Sender: TComponent; Res: Integer);



  end;

function FormNuevosDatos: TFormNuevosDatos;




var
  vParada : Boolean;
  vMensajeTecnico : String;


implementation

{$R *.dfm}

uses
  uniGUIVars, uDMppal, uniGUIApplication, ServerModule,
  uArticulos, uManteTPV, uCliente, uMenuArti, uMenuRecepcion, uDevol, uDevolLista
  ,uDMDevol, uDMCliente, uMenu;

function FormNuevosDatos: TFormNuevosDatos;
begin
  Result := TFormNuevosDatos(DMppal.GetFormInstance(TFormNuevosDatos));
end;




procedure TFormNuevosDatos.DCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes : begin

              if vResult = '2' then
              begin
                //FormTrabajos.RutBorrarRegistro;
              end;

            end;
    mrNo  : vResult := 'mbNo';
  end;
end;

procedure TFormNuevosDatos.btCerrarMenuClick(Sender: TObject);
begin
  FormBotones.Close;
end;

{
procedure TFormBotones.UniSpeedButton1Click(Sender: TObject);
begin
  if FormMenu.pcDetalle.ActivePage = FormMenu.tabDevolucion then
  begin
    if FormDevolucion.pcDetalle.ActivePage = FormDevolucion.tabListaDevolucion then
       FormDevolucionLista.pnlBtsLista.Visible := not FormDevolucionLista.pnlBtsLista.Visible;

  end;

end;
 }
procedure TFormNuevosDatos.UniSpeedButton26Click(Sender: TObject);
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabRecepcion;

  if FormMenu.swRecepcion = 0 then
  begin
    FormMenuRecepcion.Parent := FormMenu.tabRecepcion;
    FormMenuRecepcion.Align  := alClient;
    FormMenuRecepcion.Show();
    FormMenu.swRecepcion := 1;
    DMppal.RutInicioForm;

  end;
  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabRecepcion.Caption;
end;

procedure TFormNuevosDatos.UniSpeedButton27Click(Sender: TObject);
begin
   FormMenu.pcDetalle.ActivePage := FormMenu.tabDevolucion;

  if FormMenu.swDevolucion = 0 then
  begin
    FormDevolucion.Parent := FormMenu.tabDevolucion;
    FormDevolucion.Align  := alClient;
    FormDevolucion.Show();
    FormMenu.swDevolucion := 1;
    DMppal.RutInicioForm;

  end;

  DMDevolucion.RutAbrirTablas;
  FormDevolucion.RutAbrirLista;

  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabDevolucion.Caption;
end;

procedure TFormNuevosDatos.btClientesClick(Sender: TObject);
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabCliente;

  if swCliente = 0 then
  begin
    FormCliente.Parent := FormMenu.tabCliente;
    FormCliente.Align  := alClient;
    FormCliente.Show();
    swCliente := 1;
    DMppal.RutInicioForm;

  end;
  DMCliente.RutAbrirTablas;
  FormCliente.RutAbrirListaClie;

  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabCliente.Caption;

end;

procedure TFormNuevosDatos.UniSpeedButton6Click(Sender: TObject);
begin
  FormMenu.pcDetalle.ActivePage := FormMenu.tabArticulos;

  if swArticulo = 0 then
  begin
    FormMenuArti.Parent := FormMenu.tabArticulos;
    FormMenuArti.Align  := alClient;
    FormMenuArti.Show();
    swArticulo := 1;
    DMppal.RutInicioForm;

  end;
  FormMenuArti.RutAbrirListaArti;
  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabArticulos.Caption;

end;

procedure TFormNuevosDatos.UniSpeedButton8Click(Sender: TObject);
begin

  FormMenu.pcDetalle.ActivePage := FormMenu.tabManteTPV;

  if swManteTPV = 0 then
  begin
    FormManteTPV.Parent := FormMenu.tabManteTPV;
    FormManteTPV.Align := alClient;
    FormManteTPV.Show();
    swManteTPV := 1;
    DMppal.RutInicioForm;

  end;

  btCerrarMenuClick(nil);
  FormMenu.lbFormNombre.Caption := FormMenu.tabManteTPV.Caption;

end;

initialization
  RegisterAppFormClass(TFormNuevosDatos);

end.
