unit uModificarDesc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  Data.DB, uniBasicGrid, uniDBGrid, uniLabel, uniDateTimePicker,
  uniDBDateTimePicker, uniButton, uniMemo, uniDBMemo, uniEdit, uniDBEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniCheckBox,
  uniGroupBox, uniRadioGroup, uniDBRadioGroup, uniDBNavigator, uniTimer,
  uniBitBtn, uniSpeedButton, uniListBox, uniImage, uniDBText, uniProgressBar,
  uniImageList, uniDBCheckBox,
    System.RegularExpressions;

type
  TFormEditarDesc = class(TUniForm)
    ImgList: TUniNativeImageList;
    UniPanel1: TUniPanel;
    btConfirmar: TUniButton;
    btCancelar: TUniButton;
    lbMensaje: TUniLabel;
    edDesc: TUniDBEdit;
    edCod: TUniDBEdit;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    dsArticulos: TDataSource;
    procedure btConfirmarClick(Sender: TObject);


  private





    { Private declarations }

  public
    { Public declarations }



  end;

function FormEditarDesc: TFormEditarDesc;

implementation

{$R *.dfm}

uses
  uniGUIVars, uConstVar, uDMppal, uniGUIApplication, ServerModule, uNuevoUsuario, uMenuBotones,
  uMenu, uDevolFicha, uDMMenuArti, uMenuArti, uListaArti, uDMDistribuidora, uDistribuidoraMenu, uDMDevol,
  uFichaArti, uDMModificarDesc;

function FormEditarDesc: TFormEditarDesc;
begin
  Result := TFormEditarDesc(DMppal.GetFormInstance(TFormEditarDesc));

end;






procedure TFormEditarDesc.btConfirmarClick(Sender: TObject);
begin
  DMDevolucion.sqlLinea.Edit;
  DMDevolucion.sqlArticulo.Edit;
  DMDevolucion.sqlLineaREFEPROVE.asString := DMModificarDesc.sqlArticulosREFEPROVE.asString;

  DMDevolucion.sqlArticuloDESCRIPCION.AsString := DMModificarDesc.sqlArticulosDESCRIPCION.AsString;
  DMDevolucion.sqlArticuloREFEPROVE.AsString   := DMModificarDesc.sqlArticulosREFEPROVE.AsString;


  DMDevolucion.sqlArticulo.Post;
  DMDevolucion.sqlLinea.Post;

  DMDevolucion.sqlLinea.RefreshRecord();
  DMDevolucion.sqlArticulo.Refresh;
 // DMModificarDesc.sqlArticulos.RefreshRecord();
  self.Close;


end;

end.

