object FormNuevoUsuario: TFormNuevoUsuario
  Left = 0
  Top = 0
  ClientHeight = 367
  ClientWidth = 642
  Caption = 'FormNuevoUsuario'
  OnShow = UniFormShow
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalle: TUniPageControl
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 636
    Height = 361
    Hint = ''
    ActivePage = tabConfiguracionDistri
    TabBarVisible = False
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    OnChange = pcDetalleChange
    object tabGrid: TUniTabSheet
      Hint = ''
      Caption = 'tabGrid'
      object gridUsuarios: TUniDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 34
        Width = 622
        Height = 296
        Hint = ''
        DataSource = dsUsuario
        WebOptions.Paged = False
        WebOptions.FetchAll = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        OnDblClick = gridUsuariosDblClick
        Columns = <
          item
            FieldName = 'ID'
            Title.Caption = 'Nombre'
            Width = 244
          end
          item
            FieldName = 'PASS'
            Title.Caption = 'Password'
            Width = 134
          end
          item
            FieldName = 'NUMEROUSUARIO'
            Title.Caption = 'Num.TPV'
            Width = 70
          end
          item
            FieldName = 'GRUPO'
            Title.Alignment = taCenter
            Title.Caption = 'Grupo'
            Width = 50
            Alignment = taCenter
          end
          item
            FieldName = 'FECHAPASSWORD'
            Title.Caption = 'Caducidad'
            Width = 87
          end
          item
            FieldName = 'ACTUALIZACIONAUTO'
            Title.Alignment = taCenter
            Title.Caption = 'Dias'
            Width = 50
            Alignment = taCenter
          end
          item
            FieldName = 'VISIBLE1'
            Title.Caption = 'BD General'
            Width = 64
          end>
      end
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 628
        Height = 31
        Hint = ''
        Align = alTop
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        BorderStyle = ubsNone
        Caption = ''
        object navUsu: TUniDBNavigator
          Left = 363
          Top = 3
          Width = 140
          Height = 25
          Hint = ''
          Visible = False
          DataSource = dsUsuario
          VisibleButtons = [nbFirst, nbLast, nbInsert, nbDelete, nbPost, nbCancel, nbRefresh]
          TabOrder = 1
        end
        object btSalir: TUniBitBtn
          Left = 581
          Top = 3
          Width = 39
          Height = 25
          Hint = ''
          Glyph.Data = {
            0E060000424D0E06000000000000360000002800000016000000160000000100
            180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F5FA8888BB2D2D8A151582
            1011840E0F8211117A29297B7C7CAAEDEDF3FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B7DA1919871D209E2D33
            C1232BC91820C7141AC5151AC01518B60C0D8F14146CA5A5C4FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFF9292C912128E3F48CE2F
            3BDA0913CE1E1FD62D2FDC2E2FDB1F1FD10104BE0D11C31417BB0607727979A8
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFC0C0E21314934E59DB
            2031DB1114D18785ECDFDEF3FCFBF4FDFCF4E3E3F49494ED1C1BC30507B71518
            C4060673AAAAC8FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFBFBFD1F209A4F59
            D52B40E20E13D2C2C0F2FFFFF2C8C8EB8784E48582E3C1C0ECFFFFF3D1D0F61F
            1EC10609B61418BC13136BEFEFF5FFFFFFFFFFFF0000FFFFFFFFFFFF9595D02A
            2EB05268EF0007D2A09DE9FFFFF28584E4080ACA0002BE0001BB0404BF7574DF
            FFFFF3B5B5ED0505AC1116C50B0D8F7B7BAAFFFFFFFFFFFF0000FFFFFFFFFFFF
            3939AB5561D7223AE72B2ED7FBFBF2B6B5E90001C8000EC4000FC2000CBE000A
            B70000B6A2A3E4FFFFF83E3EC50103B1171BB927277AFFFFFFFFFFFF0000FFFF
            FFFFFFFF2223A86476EB021EE25B5ED8FFFFF45A5DD60D14D01E2FD32A2DC92C
            2DC7111FC90306BC4949CBFFFFF8706FD20000AA1B1FC611117AFFFFFFFFFFFF
            0000FFFFFFFFFFFF2122AC6378F20016E36266D6F7F5F36B6DD6797DEE6968E2
            C5C3E8CCCBEA4241DA2F31E34748D0FAFAF77979D10000AA1C23C90F0F82FFFF
            FFFFFFFF0000FFFFFFFFFFFF2224AE677EF50018E95052D1FDFBF69191D78A89
            EB7776E2D1D1E9DFDFEC4C4CD83B3BE47273D3FFFFFA5C5CC60000AE2129CC10
            1183FFFFFFFFFFFF0000FFFFFFFFFFFF2526B0687DF15F78FA8A8CD8D2D1E8E3
            E3F16E6ECF7575DBCECEE7DADAEB4D4DD34949CDD9DAECE9E9F02E2DC91016CC
            2830CB151581FFFFFFFFFFFF0000FFFFFFFFFFFF3F3FB96772DEFBFFFFE2E2F3
            7B7BC5F2F2F8AAAAD85353BFD6D6EBDDDDED3A3ABB9C9CD2FFFFFB6C6CC42F2F
            DB3A3BF3292EC62D2D88FFFFFFFFFFFF0000FFFFFFFFFFFFA0A0DC373BBFF9FB
            FFFFFFFFC5C5E67F7FC08787C88989D5CFCFE7D9D9EA6060CD6F6FC87878BF47
            47D45555EC4647FC1618A28686BBFFFFFFFFFFFF0000FFFFFFFFFFFFFBFBFE27
            27B38D92E0FFFFFFFFFFFFD8D8EFD2D2F4A3A3DCD1D1E7DCDCEB7474CE8787ED
            6C6CE27473EE6768FB3D40D8191A85F7F7FBFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFCCCCED1416B0A9AEEBFFFFFFFFFFFFF8F8FFA9A9D7AFAFCEB7B7D17A7A
            CAA6A6F99494F48A89FC595CE60F118DBABADBFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFFFFFFFFFFFFFFABABE21314B08D93E0F6F8FEFFFFFFEBEAF59291BD86
            86BAB7B7EEBCBCFF9FA1FC5A60DB1012939292C8FFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECEEE2727B3373BBE8A8EDEC2C6F1
            D2D5F7C5C6F7A4A7F06B6FDB2C30B21E1E97BCBCDFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9EDB3E3E
            B92022AF1F21AD1E20AB1F21A83838AB9595CFFDFDFEFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
          Caption = ''
          TabOrder = 2
          OnClick = btSalirClick
        end
        object btAppend: TUniBitBtn
          Left = 3
          Top = 3
          Width = 30
          Height = 25
          Hint = ''
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0808080808080808080C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FF
            FFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF
            0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000
            0000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF00
            00FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0
            C080808080808080808080808080808080808080808000FF0000FF0000FF0080
            8080808080808080808080808080808080808080C0C0C0000000FFFFFF00FF00
            00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
            0000FF0000FF0000FF0000FF0000FF00808080000000FFFFFF00FF0000FF0000
            FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
            00FF0000FF0000FF0000FF00808080000000C0C0C0FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF00FF0000FF0000FF00FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFC0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FF
            FFFF00FF0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF
            0000FF0000FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000
            0000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF00
            00FF00808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF0080
            8080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF00FF0000FF0000FF00808080C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000}
          Caption = ''
          TabOrder = 3
          OnClick = btAppendClick
        end
        object btDelete: TUniBitBtn
          Left = 39
          Top = 3
          Width = 30
          Height = 25
          Hint = ''
          Glyph.Data = {
            16020000424D160200000000000076000000280000001A0000001A0000000100
            040000000000A001000000000000000000001000000000000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777788888888
            8888888887777700000077791111111111111111187777000000777911111111
            1111111118777700000077791111111111111111187777000000777911111111
            1111111118777700000077779999999999999999977777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000777777777777
            7777777777777700000077777777777777777777777777000000}
          Caption = ''
          TabOrder = 4
          OnClick = btDeleteClick
        end
        object btPost: TUniBitBtn
          Left = 75
          Top = 3
          Width = 30
          Height = 25
          Hint = ''
          Glyph.Data = {
            0E060000424D0E06000000000000360000002800000016000000160000000100
            180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA99E8E646136988776FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFF2F1EF65673D41AE48527735AA9E
            8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFB3AF9E4D823645DC7844
            C764516F2FB7B3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFEFEFD6D754749B457
            62E3965BE29246BE5D52692BCCCCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFD3D6C9457E
            2E5FDC887AE6A871E2A062E69948B859536E2EDADED2FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF7B
            8E5E4AAD5186EEB592ECBB85E8B17CE6AB67E9A046B252557230EBEEE7FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            ECF0E9407B2669DA8CA6F5CF83EBB088EEB88EECBB81EBB365EAA043A9465F7F
            41F6F8F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFF92AC8041A33EA5FAD18BEDB63BA83E4CBF5F83F1B993F1C483F0BB64
            EAA03FA43E688A4DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFF9FAF845822A6ED88C9EFCD03FAF464C7E2F427F2642B44B75EDAC
            90F6C983F2C15FE89A399A3081A36DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF0000FFFFFFB4CDAB399D3680F8BB49BB57488830F9FAF8E8EEE45990
            41349F3060E08D85F8C97FF7C859E69334942792B583FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF57984549D16D48CC653B8926E7EFE4FFFFFFFF
            FFFFFFFFFF7DA96D2B911D4DD16F78F8C079FDD053E1892F8E1EB2CEAAFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF2F97253EC74D348F25E1ECDEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFBAD3B3348E213ABB4967F4AE71FFD44EDF842C90
            18BFD9B9FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF449D39319523D0E4CCFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9F2E74C9A3A2CAB2B58EC9A6B
            FFDA49D876298F16DAEBD8FFFFFFFFFFFFFFFFFF0000FFFFFFF1F7F0EBF4EAFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7CB872
            229E1A47DC7769FFCE46D46F2F9620E9F4E8FFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFB4D8B026981937C85265FEBF3ECE60389D2EF9FCF9FFFFFF0000FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFE7F3E63DA53827B83655EF9433C44655B252FFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7BC37B1BAA1F3FD95B1AAD
            1CFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4DFB41B
            A61B28AC28FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
          Caption = ''
          TabOrder = 5
          OnClick = btPostClick
        end
        object btCancel: TUniBitBtn
          Left = 111
          Top = 3
          Width = 30
          Height = 25
          Hint = ''
          Glyph.Data = {
            0E060000424D0E06000000000000360000002800000016000000160000000100
            180000000000D8050000130B0000130B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFFFFFFFF9E9ED53030B82E2EB8A0A0D7FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFB0B0E32A2AC13030C58080D3FFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFA8A8D73737B83131F72727F42626B9A0A0D7FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFB5B5E32222BE4E4EF16262F93E3EC98C8CD5FFFF
            FFFFFFFF0000FFFFFF9797D03E3EB53737F70101F50000EF2121EE2222BC9898
            D3FFFFFFFFFFFFFFFFFFFFFFFFAAAADE1E1EBE3F3FEC3E3EEF4545F26D6DF845
            45C97E7ED1FFFFFF0000FFFFFF3E3EAD4242FA0101FB0000F10000EB0000E918
            18EE1C1CBCA1A1D6FFFFFFFFFFFFB6B6E11A1ABC3333EC3232E83B3BEA4545F0
            4E4EF78080FE4141C4FFFFFF0000FFFFFF3E3EA94848F00808FE0000F10000EC
            0000E60000E31111E71717BAA8A8DABCBCE21515B82323E62727E32F2FE53C3C
            EB4444F05555FA7D7DF73C3CC0FFFFFF0000FFFFFFC3C3E23C3CA74040EF0606
            F70000EB0000E70000E10000DE0A0AE40F0FB90F0FB41414E31B1BDF2525E031
            31E63A3AEA4949F46D6DF43636BDAAAAE1FFFFFF0000FFFFFFFFFFFFCACAE538
            38A83939F00606F30000E60000E20000DB0000D70303E00707E00F0FD81A1ADB
            2626E12F2FE53D3DEF6161F43333BDB5B5E3FFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFC2C2E13636A92F2FE90404EE0000E10000DC0000D60000D10202D10F0F
            D51A1ADB2525E03333EA5050EE2E2EBBAAAADEFFFFFFFFFFFFFFFFFF0000FFFF
            FFFFFFFFFFFFFFFFFFFFCACAE43131A82929E90404E60000D90000D40000CE00
            00CE0808D41616D92828E44242EB2828B8B5B5E1FFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4E92828A51919E31515E74646EE
            4D4DEF4E4EEF4A4AEE2D2DE52D2DE62020B4C1C1E5FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0DF2323A82222E76464
            FA8C8CFF8787FF8787FF8A8AFF7272FA3A3AEB1E1EB7ACACDCFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFBABADA2929A82929ED6A
            6AFB9898FF8F8FFF9898FF9898FF9090FF9595FF7D7DFB4A4AF02424B89E9ED6
            FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFB0B0D43030A93333EE
            7878FFA8A8FFA0A0FFACACFF7373F46C6CF2ABABFFA0A0FFA4A4FF8D8DFE5959
            F22D2DBA9595D3FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFBABAD83636A63A3A
            F48585FFBBBBFFAEAEFFBDBDFF8484F60A0ADF0505DC7B7BF4BCBCFFAFAFFFB6
            B6FF9D9DFE6B6BF83333BB9E9ED6FFFFFFFFFFFF0000FFFFFFB7B7D63B3BA043
            43F28E8EFFC8C8FFBDBDFFCECEFF9191FB1212E61111A91414A41515E28C8CF9
            CDCDFFBEBEFFC3C3FFAAAAFF7777F73737B89A9AD6FFFFFF0000FFFFFF42429E
            5050F29F9FFFDADAFFCECEFFDBDBFF9D9DFB1E1EE71616A8BDBDDDCECEE61A1A
            A72727E39B9BFAD9D9FECDCDFFD3D3FFBEBEFF8B8BF83E3EB8FFFFFF0000FFFF
            FF43439C4F4FF5ACACFFEEEEFFEEEEFFA8A8FD2424ED1C1CA8BBBBDAFFFFFFFF
            FFFFCCCCE51F1FA83434E9A9A9FCEBEBFFE7E7FFCBCBFF8C8CFA3E3EB7FFFFFF
            0000FFFFFFB3B3D33D3D9F4949F2B9B9FFB6B6FF2E2EEC2323A5B2B2D4FFFFFF
            FFFFFFFFFFFFFFFFFFC4C4E12323A94242E8BEBEFFD7D7FF7F7FF83A3AB79494
            D2FFFFFF0000FFFFFFFFFFFFBDBDD73838A04444F23838EF2A2AA0BCBCD9FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACAE42828A75656ED7171F73535B5A7
            A7D8FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFB5B5D439399E38389FBCBCD9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8E43131A92F2FAB
            9D9DD2FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
          Caption = ''
          TabOrder = 6
          OnClick = btCancelarClick
        end
      end
    end
    object tabModificar: TUniTabSheet
      Hint = ''
      Caption = 'tabModificar'
      object edNombre: TUniDBEdit
        Left = 89
        Top = 23
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ID'
        DataSource = dsUsuario
        TabOrder = 0
      end
      object UniLabel4: TUniLabel
        Left = 45
        Top = 26
        Width = 36
        Height = 13
        Hint = ''
        Caption = 'Usuario'
        TabOrder = 7
      end
      object UniLabel5: TUniLabel
        Left = 35
        Top = 55
        Width = 46
        Height = 13
        Hint = ''
        Caption = 'Password'
        TabOrder = 8
      end
      object btGrabarModificacion: TUniButton
        Left = 89
        Top = 248
        Width = 75
        Height = 25
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 5
        OnClick = btGrabarModificacionClick
      end
      object btCancelar: TUniButton
        Left = 174
        Top = 248
        Width = 75
        Height = 25
        Hint = ''
        Caption = 'Cancelar'
        TabOrder = 6
        OnClick = btCancelarClick
      end
      object UniLabel8: TUniLabel
        Left = 61
        Top = 146
        Width = 20
        Height = 13
        Hint = ''
        Caption = 'Dias'
        TabOrder = 9
      end
      object UniDBEdit3: TUniDBEdit
        Left = 89
        Top = 113
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'NUMEROUSUARIO'
        DataSource = dsUsuario
        TabOrder = 2
      end
      object UniDBDateTimePicker1: TUniDBDateTimePicker
        Left = 89
        Top = 173
        Width = 160
        Hint = ''
        DataField = 'FECHAPASSWORD'
        DataSource = dsUsuario
        DateTime = 43403.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 4
      end
      object UniDBEdit5: TUniDBEdit
        Left = 89
        Top = 143
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 3
      end
      object UniLabel3: TUniLabel
        Left = 19
        Top = 176
        Width = 62
        Height = 13
        Hint = ''
        Caption = 'Fecha Passw'
        TabOrder = 10
      end
      object UniDBEdit1: TUniDBEdit
        Left = 89
        Top = 83
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'GRUPO'
        DataSource = dsUsuario
        TabOrder = 1
      end
      object cbModificarPass: TUniDBCheckBox
        Left = 89
        Top = 53
        Width = 160
        Height = 17
        Hint = ''
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Modificar Password'
        TabOrder = 11
        OnClick = cbModificarPassClick
      end
      object UniLabel1: TUniLabel
        Left = 40
        Top = 115
        Width = 43
        Height = 13
        Hint = ''
        Caption = 'Num.TPV'
        TabOrder = 12
      end
      object UniLabel11: TUniLabel
        Left = 52
        Top = 86
        Width = 29
        Height = 13
        Hint = ''
        Caption = 'Grupo'
        TabOrder = 13
      end
      object pnlDatos: TUniPanel
        Left = 270
        Top = 81
        Width = 253
        Height = 190
        Hint = ''
        TabOrder = 14
        BorderStyle = ubsNone
        Caption = ''
        object UniLabel2: TUniLabel
          Left = 66
          Top = 124
          Width = 21
          Height = 13
          Hint = ''
          Caption = 'C.P.'
          TabOrder = 1
        end
        object UniLabel6: TUniLabel
          Left = 44
          Top = 65
          Width = 43
          Height = 13
          Hint = ''
          Caption = 'Direccion'
          TabOrder = 2
        end
        object UniDBEdit2: TUniDBEdit
          Left = 92
          Top = 62
          Width = 160
          Height = 22
          Hint = ''
          DataField = 'ACTUALIZACIONAUTO'
          DataSource = dsUsuario
          TabOrder = 3
        end
        object UniLabel7: TUniLabel
          Left = 69
          Top = 95
          Width = 18
          Height = 13
          Hint = ''
          Caption = 'Telf'
          TabOrder = 4
        end
        object UniDBEdit4: TUniDBEdit
          Left = 92
          Top = 92
          Width = 160
          Height = 22
          Hint = ''
          DataField = 'ACTUALIZACIONAUTO'
          DataSource = dsUsuario
          TabOrder = 5
        end
        object UniLabel12: TUniLabel
          Left = 50
          Top = 5
          Width = 37
          Height = 13
          Hint = ''
          Caption = 'Nombre'
          TabOrder = 6
        end
        object UniDBEdit6: TUniDBEdit
          Left = 92
          Top = 0
          Width = 160
          Height = 22
          Hint = ''
          DataField = 'NOMBRE'
          DataSource = dsUsuario
          TabOrder = 7
        end
        object UniLabel13: TUniLabel
          Left = 44
          Top = 35
          Width = 43
          Height = 13
          Hint = ''
          Caption = 'Nombre2'
          TabOrder = 8
        end
        object UniDBEdit7: TUniDBEdit
          Left = 93
          Top = 34
          Width = 160
          Height = 22
          Hint = ''
          DataField = 'NOMBRE'
          DataSource = dsUsuario
          TabOrder = 9
        end
        object UniDBEdit9: TUniDBEdit
          Left = 92
          Top = 121
          Width = 160
          Height = 22
          Hint = ''
          DataField = 'ACTUALIZACIONAUTO'
          DataSource = dsUsuario
          TabOrder = 10
        end
      end
      object UniDBEdit8: TUniDBEdit
        Left = 89
        Top = 203
        Width = 160
        Height = 22
        Hint = ''
        DataSource = dsUsuario
        TabOrder = 15
      end
      object UniLabel14: TUniLabel
        Left = 57
        Top = 206
        Width = 24
        Height = 13
        Hint = ''
        Caption = 'Email'
        TabOrder = 16
      end
      object pnlModiPass: TUniPanel
        Left = 270
        Top = 23
        Width = 253
        Height = 58
        Hint = ''
        Visible = False
        TabOrder = 17
        BorderStyle = ubsNone
        Caption = ''
        object UniLabel15: TUniLabel
          Left = 7
          Top = 5
          Width = 80
          Height = 13
          Hint = ''
          Caption = 'Nueva Password'
          TabOrder = 1
        end
        object edNuevaPass: TUniEdit
          Left = 92
          Top = 0
          Width = 160
          Hint = ''
          PasswordChar = '*'
          Text = ''
          TabOrder = 2
        end
        object edConfirmarPass: TUniEdit
          Left = 92
          Top = 30
          Width = 160
          Hint = ''
          PasswordChar = '*'
          Text = ''
          TabOrder = 3
        end
        object UniLabel16: TUniLabel
          Left = 40
          Top = 32
          Width = 47
          Height = 13
          Hint = ''
          Caption = 'Confirmar'
          TabOrder = 4
        end
      end
    end
    object tabModificarPassword: TUniTabSheet
      Hint = ''
      Caption = 'tabModificarPassword'
      object UniButton1: TUniButton
        Left = 104
        Top = 97
        Width = 75
        Height = 25
        Hint = ''
        Caption = 'Aceptar'
        TabOrder = 0
        OnClick = btGrabarModificacionClick
      end
      object UniButton2: TUniButton
        Left = 189
        Top = 97
        Width = 75
        Height = 25
        Hint = ''
        Caption = 'Cancelar'
        TabOrder = 1
        OnClick = btCancelarClick
      end
      object UniLabel9: TUniLabel
        Left = 28
        Top = 28
        Width = 80
        Height = 13
        Hint = ''
        Caption = 'Nuevo Password'
        TabOrder = 2
      end
      object UniEdit1: TUniEdit
        Left = 72
        Top = 25
        Width = 121
        Hint = ''
        PasswordChar = '*'
        Text = ''
        TabOrder = 3
      end
      object UniEdit2: TUniEdit
        Left = 72
        Top = 54
        Width = 121
        Hint = ''
        PasswordChar = '*'
        Text = ''
        TabOrder = 4
      end
      object UniLabel10: TUniLabel
        Left = 19
        Top = 53
        Width = 47
        Height = 13
        Hint = ''
        Caption = 'Confirmar'
        TabOrder = 5
      end
      object UniDBCheckBox1: TUniDBCheckBox
        Left = 101
        Top = 74
        Width = 63
        Height = 17
        Hint = ''
        DataField = 'TIPOPASSW'
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Encriptar'
        TabOrder = 6
      end
    end
    object tabNuevoUsuario: TUniTabSheet
      Hint = ''
      Caption = 'tabNuevoUsuario'
      object UniLabel17: TUniLabel
        Left = 5
        Top = 58
        Width = 46
        Height = 13
        Hint = ''
        Caption = 'Password'
        TabOrder = 0
      end
      object edPasswNueva: TUniEdit
        Left = 56
        Top = 53
        Width = 331
        Height = 26
        Hint = ''
        PasswordChar = '*'
        Text = ''
        ParentFont = False
        Font.Height = -16
        TabOrder = 2
      end
      object edPasswConfirmar: TUniEdit
        Left = 57
        Top = 87
        Width = 331
        Height = 26
        Hint = ''
        PasswordChar = '*'
        Text = ''
        ParentFont = False
        Font.Height = -16
        TabOrder = 3
      end
      object UniLabel18: TUniLabel
        Left = 4
        Top = 91
        Width = 47
        Height = 13
        Hint = ''
        Caption = 'Confirmar'
        TabOrder = 7
      end
      object UniLabel19: TUniLabel
        Left = 15
        Top = 16
        Width = 36
        Height = 13
        Hint = ''
        Caption = 'Usuario'
        TabOrder = 8
      end
      object UniLabel20: TUniLabel
        Left = 56
        Top = 35
        Width = 151
        Height = 13
        Hint = ''
        Caption = 'Se recomienda el uso del EMAIL'
        ParentFont = False
        Font.Style = [fsUnderline]
        TabOrder = 9
      end
      object btGrabarNuevoUsu: TUniButton
        Left = 267
        Top = 184
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 5
        Images = DMppal.ImgListPrincipal
        ImageIndex = 6
        OnClick = btGrabarNuevoUsuClick
      end
      object btSalirReg: TUniButton
        Left = 332
        Top = 184
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 6
        Images = DMppal.ImgListPrincipal
        ImageIndex = 5
        OnClick = btSalirRegClick
      end
      object UniLabel21: TUniLabel
        Left = 7
        Top = 127
        Width = 43
        Height = 13
        Hint = ''
        Caption = 'Num.TPV'
        TabOrder = 10
      end
      object edIDUsuario: TUniEdit
        Left = 56
        Top = 7
        Width = 331
        Height = 26
        Hint = ''
        Text = ''
        ParentFont = False
        Font.Height = -16
        TabOrder = 1
        ClearButton = True
        OnChange = edIDUsuarioChange
      end
      object edNumTPV: TUniEdit
        Left = 56
        Top = 123
        Width = 331
        Height = 26
        Hint = ''
        Text = ''
        ParentFont = False
        Font.Height = -16
        TabOrder = 4
        ReadOnly = True
      end
      object lbEmailValido: TUniLabel
        Left = 213
        Top = 34
        Width = 174
        Height = 13
        Hint = ''
        Visible = False
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'lbEmailValido'
        ParentFont = False
        TabOrder = 11
      end
      object rgUsoBD: TUniRadioGroup
        Left = 435
        Top = 262
        Width = 185
        Height = 60
        Hint = ''
        Visible = False
        Items.Strings = (
          'Base de Datos General'
          'Base de Datos Personal')
        ItemIndex = 0
        Caption = 'Base de Datos por Defecto'
        TabOrder = 12
      end
      object lbPassword: TUniLabel
        Left = 57
        Top = 151
        Width = 54
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'lbPassword'
        ParentFont = False
        TabOrder = 13
      end
    end
    object tabConfiguracionDistri: TUniTabSheet
      Hint = ''
      Caption = 'tabConfiguracionDistri'
      object UniLabel22: TUniLabel
        Left = 45
        Top = 24
        Width = 60
        Height = 13
        Hint = ''
        Caption = 'Distribuidora'
        TabOrder = 0
      end
      object UniLabel23: TUniLabel
        Left = 129
        Top = 24
        Width = 74
        Height = 13
        Hint = ''
        Caption = 'Punto de venta'
        TabOrder = 5
      end
      object UniLabel24: TUniLabel
        Left = 255
        Top = 24
        Width = 23
        Height = 13
        Hint = ''
        Caption = 'Ruta'
        TabOrder = 6
      end
      object UniLabel25: TUniLabel
        Left = 382
        Top = 24
        Width = 36
        Height = 13
        Hint = ''
        Caption = 'Margen'
        TabOrder = 7
      end
      object lbMarina: TUniLabel
        Left = 44
        Top = 50
        Width = 61
        Height = 13
        Hint = ''
        Caption = 'Marina Press'
        TabOrder = 8
      end
      object lbLogistica: TUniLabel
        Left = 13
        Top = 76
        Width = 92
        Height = 13
        Hint = ''
        Caption = 'Logistica de Medios'
        TabOrder = 9
      end
      object lbSade: TUniLabel
        Left = 43
        Top = 101
        Width = 62
        Height = 13
        Hint = ''
        Caption = 'Sade/Sabate'
        TabOrder = 10
      end
      object lbSgel: TUniLabel
        Left = 85
        Top = 127
        Width = 20
        Height = 13
        Hint = ''
        Caption = 'Sgel'
        TabOrder = 11
      end
      object edMarinaRuta: TUniEdit
        Left = 255
        Top = 50
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 1
        ClearButton = True
      end
      object edMarinaMargen: TUniEdit
        Left = 382
        Top = 50
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 2
        ClearButton = True
      end
      object UniLabel26: TUniLabel
        Left = 565
        Top = 180
        Width = 25
        Height = 13
        Hint = ''
        Visible = False
        Caption = 'Si/No'
        TabOrder = 12
      end
      object UniCheckBox1: TUniCheckBox
        Left = 565
        Top = 207
        Width = 40
        Height = 17
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 13
      end
      object UniCheckBox2: TUniCheckBox
        Left = 565
        Top = 239
        Width = 40
        Height = 17
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 14
      end
      object UniCheckBox3: TUniCheckBox
        Left = 565
        Top = 270
        Width = 40
        Height = 17
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 15
      end
      object UniCheckBox4: TUniCheckBox
        Left = 565
        Top = 302
        Width = 40
        Height = 17
        Hint = ''
        Visible = False
        Caption = ''
        TabOrder = 16
      end
      object btGrabarConfiguracion: TUniButton
        Left = 382
        Top = 180
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 3
        Images = DMppal.ImgListPrincipal
        ImageIndex = 6
        OnClick = btGrabarConfiguracionClick
      end
      object btCancelarConfiguracion: TUniButton
        Left = 448
        Top = 180
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 4
        Images = DMppal.ImgListPrincipal
        ImageIndex = 5
      end
      object edMarinaPuntoVenta: TUniEdit
        Left = 129
        Top = 50
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 17
        ClearButton = True
      end
      object edLogisticaPuntoVenta: TUniEdit
        Left = 129
        Top = 76
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 18
        ClearButton = True
      end
      object edLogisticaRuta: TUniEdit
        Left = 255
        Top = 75
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 19
        ClearButton = True
      end
      object edLogisticaMargen: TUniEdit
        Left = 382
        Top = 75
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 20
        ClearButton = True
      end
      object edSadePuntoVenta: TUniEdit
        Left = 129
        Top = 101
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 21
        ClearButton = True
      end
      object edSadeRuta: TUniEdit
        Left = 256
        Top = 101
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 22
        ClearButton = True
      end
      object edSadeMargen: TUniEdit
        Left = 382
        Top = 101
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 23
        ClearButton = True
      end
      object edSgelPuntoVenta: TUniEdit
        Left = 129
        Top = 127
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 24
        ClearButton = True
      end
      object edSgelRuta: TUniEdit
        Left = 255
        Top = 127
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 25
        ClearButton = True
      end
      object edSgelMargen: TUniEdit
        Left = 382
        Top = 127
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 26
        ClearButton = True
      end
    end
    object tabConfiguracionEmpresa: TUniTabSheet
      Hint = ''
      Caption = 'tabConfiguracionEmpresa'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 636
      ExplicitHeight = 361
      object UniLabel27: TUniLabel
        Left = 305
        Top = 39
        Width = 70
        Height = 13
        Hint = ''
        Caption = 'C.P./Poblaci'#243'n'
        TabOrder = 0
      end
      object UniLabel28: TUniLabel
        Left = 58
        Top = 135
        Width = 43
        Height = 13
        Hint = ''
        Caption = 'Direccion'
        TabOrder = 1
      end
      object UniDBEdit10: TUniDBEdit
        Left = 107
        Top = 136
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 2
      end
      object UniLabel29: TUniLabel
        Left = 357
        Top = 103
        Width = 18
        Height = 13
        Hint = ''
        Caption = 'Telf'
        TabOrder = 3
      end
      object UniDBEdit11: TUniDBEdit
        Left = 383
        Top = 103
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 4
      end
      object UniLabel30: TUniLabel
        Left = 64
        Top = 39
        Width = 37
        Height = 13
        Hint = ''
        Caption = 'Nombre'
        TabOrder = 5
      end
      object UniDBEdit12: TUniDBEdit
        Left = 107
        Top = 38
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'NOMBRE'
        DataSource = dsUsuario
        TabOrder = 6
      end
      object UniLabel31: TUniLabel
        Left = 15
        Top = 71
        Width = 86
        Height = 13
        Hint = ''
        Caption = 'Nombre Comercial'
        TabOrder = 7
      end
      object UniDBEdit13: TUniDBEdit
        Left = 107
        Top = 70
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'NOMBRE'
        DataSource = dsUsuario
        TabOrder = 8
      end
      object UniDBEdit14: TUniDBEdit
        Left = 383
        Top = 38
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 9
      end
      object UniLabel32: TUniLabel
        Left = 351
        Top = 135
        Width = 24
        Height = 13
        Hint = ''
        Caption = 'Movil'
        TabOrder = 10
      end
      object UniLabel33: TUniLabel
        Left = 84
        Top = 103
        Width = 17
        Height = 13
        Hint = ''
        Caption = 'NIF'
        TabOrder = 11
      end
      object UniDBEdit15: TUniDBEdit
        Left = 107
        Top = 102
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 12
      end
      object UniLabel34: TUniLabel
        Left = 351
        Top = 71
        Width = 24
        Height = 13
        Hint = ''
        Caption = 'Email'
        TabOrder = 13
      end
      object UniDBEdit16: TUniDBEdit
        Left = 383
        Top = 70
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 14
      end
      object UniDBEdit17: TUniDBEdit
        Left = 383
        Top = 136
        Width = 160
        Height = 22
        Hint = ''
        DataField = 'ACTUALIZACIONAUTO'
        DataSource = dsUsuario
        TabOrder = 15
      end
      object btGrabarDatosEmpresa: TUniButton
        Left = 390
        Top = 188
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 16
        Images = DMppal.ImgListPrincipal
        ImageIndex = 6
        OnClick = btGrabarConfiguracionClick
      end
      object btCancelarDatosEmpresa: TUniButton
        Left = 456
        Top = 188
        Width = 55
        Height = 42
        Cursor = crHandPoint
        Hint = ''
        Caption = ''
        TabOrder = 17
        Images = DMppal.ImgListPrincipal
        ImageIndex = 5
      end
      object UniLabel35: TUniLabel
        Left = 107
        Top = 9
        Width = 98
        Height = 13
        Hint = ''
        Caption = 'Datos de la Empresa'
        TabOrder = 18
      end
    end
  end
  object dsUsuario: TDataSource
    DataSet = DMppal.sqlUsu
    Left = 20
    Top = 283
  end
  object dsProve: TDataSource
    DataSet = DMppal.sqlProve
    Left = 175
    Top = 283
  end
end
