object FormReclamaciones: TFormReclamaciones
  Left = 0
  Top = 0
  Hint = 'Otras'
  ClientHeight = 600
  ClientWidth = 900
  Caption = ''
  OnShow = UniFormShow
  BorderStyle = bsNone
  Position = poDesigned
  OldCreateOrder = False
  ShowHint = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ClientEvents.ExtEvents.Strings = (
    
      'window.afterrender=function window.afterrender(sender, eOpts)'#13#10'{' +
      #13#10'  Ext.get(sender.id).el.setStyle("padding", 0);'#13#10'  Ext.get(sen' +
      'der.id).el.setStyle("border-width", 0);'#13#10'}')
  Layout = 'fit'
  PixelsPerInch = 96
  TextHeight = 13
  object pnltop: TUniPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 145
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Caption = ''
    object RGTipo: TUniRadioGroup
      Left = 3
      Top = 3
      Width = 406
      Height = 43
      Hint = ''
      ShowHint = True
      Items.Strings = (
        'Compras'
        'Devoluciones'
        'Todo')
      ItemIndex = 2
      Caption = 'Tipo de Consulta'
      TabOrder = 1
      Columns = 3
      Vertical = False
      OnClick = RGTipoClick
    end
    object RgDistribu: TUniRadioGroup
      Left = 3
      Top = 52
      Width = 99
      Height = 66
      Hint = ''
      ShowHint = True
      Items.Strings = (
        'Uno'
        'Todos')
      Caption = 'Distribuidor'
      TabOrder = 2
      OnClick = RgDistribuClick
    end
    object RgTReclama: TUniRadioGroup
      Left = 415
      Top = 3
      Width = 406
      Height = 43
      Hint = ''
      ShowHint = True
      Items.Strings = (
        'En curso'
        'Cerradas'
        'Todas')
      ItemIndex = 0
      Caption = 'Tipo de Reclamacion'
      TabOrder = 3
      Columns = 3
      Vertical = False
      OnClick = RgTReclamaClick
    end
    object UniPanel1: TUniPanel
      Left = 108
      Top = 52
      Width = 429
      Height = 56
      Hint = ''
      ShowHint = True
      TabOrder = 4
      Caption = ''
      object BtnBuscaDistribui: TUniButton
        Left = 4
        Top = 7
        Width = 70
        Height = 40
        Hint = ''
        ShowHint = True
        Caption = ''
        TabOrder = 1
        Images = DMppal.ImgListPrincipal
        ImageIndex = 23
      end
      object pDistriCodi: TUniDBEdit
        Left = 80
        Top = 25
        Width = 57
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 2
      end
      object pDistriNom: TUniDBEdit
        Left = 136
        Top = 25
        Width = 288
        Height = 22
        Hint = ''
        ShowHint = True
        TabOrder = 3
      end
    end
    object edDesdeFecha: TUniDateTimePicker
      Left = 616
      Top = 52
      Width = 101
      Hint = ''
      ShowHint = True
      DateTime = 43437.000000000000000000
      DateFormat = 'dd/MM/yy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
      OnExit = edDesdeFechaExit
    end
    object UniLabel1: TUniLabel
      Left = 548
      Top = 55
      Width = 62
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Desde Fecha'
      TabOrder = 6
    end
    object UniLabel2: TUniLabel
      Left = 550
      Top = 76
      Width = 60
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = 'Hasta Fecha'
      TabOrder = 7
    end
    object edHastaFecha: TUniDateTimePicker
      Left = 616
      Top = 73
      Width = 101
      Hint = ''
      ShowHint = True
      DateTime = 43437.000000000000000000
      DateFormat = 'dd/MM/yy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 8
    end
    object BtnSeleccionar: TUniBitBtn
      Left = 723
      Top = 52
      Width = 60
      Height = 43
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 9
      Images = DMppal.ImgListPrincipal
      ImageIndex = 3
      OnClick = BtnSeleccionarClick
    end
  end
  object UniPanel2: TUniPanel
    Left = 0
    Top = 145
    Width = 900
    Height = 16
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Caption = 'Articulos Reclamados'
  end
  object UniDBGrid1: TUniDBGrid
    Left = 0
    Top = 161
    Width = 900
    Height = 216
    Hint = ''
    ShowHint = True
    DataSource = dsReclama1
    LoadMask.Message = 'Loading data...'
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    Columns = <
      item
        FieldName = 'FECHARECLAMACION'
        Title.Caption = 'FechaReclama'
        Width = 73
      end
      item
        FieldName = 'FECHA'
        Title.Caption = 'Fecha'
        Width = 64
      end
      item
        FieldName = 'BARRAS'
        Title.Caption = 'Barras'
        Width = 82
      end
      item
        FieldName = 'ADENDUM'
        Title.Caption = 'Num'
        Width = 34
      end
      item
        FieldName = 'REFEPROVE'
        Title.Caption = 'Referencia'
        Width = 124
      end
      item
        FieldName = 'DESCRIPCION'
        Title.Caption = 'Descripcion'
        Width = 244
      end
      item
        FieldName = 'CANTIDAD'
        Title.Caption = 'Recibido'
        Width = 64
      end
      item
        FieldName = 'CANTIENALBA'
        Title.Caption = 'Albaran'
        Width = 64
      end
      item
        FieldName = 'SWESTADO'
        Title.Caption = 'Ind'
        Width = 64
      end
      item
        FieldName = 'NRECLAMACION'
        Title.Caption = 'N.Reclama'
        Width = 64
      end
      item
        FieldName = 'NOMPROVEEDOR'
        Title.Caption = 'Distribuidor'
        Width = 244
      end
      item
        FieldName = 'DOCTOPROVE'
        Title.Caption = 'Albaran'
        Width = 124
      end
      item
        FieldName = 'DOCTOPROVEFECHA'
        Title.Caption = 'FechaAlb'
        Width = 64
      end
      item
        FieldName = 'ID_PROVEEDOR'
        Title.Caption = 'N.Prove'
        Width = 64
      end>
  end
  object UniPanel4: TUniPanel
    Left = 0
    Top = 377
    Width = 900
    Height = 56
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    Caption = ''
    object BtnReclama: TUniButton
      Left = 723
      Top = 4
      Width = 60
      Height = 48
      Hint = ''
      ShowHint = True
      Caption = ''
      TabOrder = 1
      Images = DMppal.ImgListPrincipal
      ImageIndex = 40
    end
    object LblDada: TUniLabel
      Left = 3
      Top = 24
      Width = 12
      Height = 13
      Hint = ''
      ShowHint = True
      Caption = '...'
      TabOrder = 2
    end
  end
  object UniPanel3: TUniPanel
    Left = 0
    Top = 433
    Width = 900
    Height = 16
    Hint = ''
    ShowHint = True
    Align = alTop
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
    Caption = 'Reclamaciones Efectuadas'
  end
  object UniDBGrid2: TUniDBGrid
    Left = 0
    Top = 449
    Width = 900
    Height = 151
    Hint = ''
    ShowHint = True
    DataSource = dsReclamaS
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 5
    Columns = <
      item
        FieldName = 'FECHA'
        Title.Caption = 'Fecha'
        Width = 64
      end
      item
        FieldName = 'HORA'
        Title.Caption = 'Hora'
        Width = 64
      end
      item
        FieldName = 'TRECLAMACION'
        Title.Caption = 'TipoRec.'
        Width = 64
      end
      item
        FieldName = 'CONCEPTO'
        Title.Caption = 'Concepto'
        Width = 244
      end
      item
        FieldName = 'NRECLAMACION'
        Title.Caption = 'N.Orden'
        Width = 64
      end
      item
        FieldName = 'CANTIDAD'
        Title.Caption = 'Canti.Reclamada'
        Width = 85
      end
      item
        FieldName = 'CANTIENALBA'
        Title.Caption = 'CANTIENALBA'
        Width = 73
      end
      item
        FieldName = 'SWESTADO'
        Title.Caption = 'SWESTADO'
        Width = 64
      end
      item
        FieldName = 'DescriEstado'
        Title.Caption = 'DescriEstado'
        Width = 124
      end
      item
        FieldName = 'ID_RECLAMA'
        Title.Caption = 'ID_RECLAMA'
        Width = 68
      end>
  end
  object dsReclamaS: TDataSource
    DataSet = DMReclamaciones.sqlReclamaS
    Left = 840
    Top = 56
  end
  object dsReclama1: TDataSource
    DataSet = DMReclamaciones.sqlReclama1
    Left = 840
    Top = 8
  end
end
